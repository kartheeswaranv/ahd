package com.quikrservices.android.network;

import android.support.annotation.NonNull;
import com.quikr.android.api.QuikrNetwork;
import java.util.Set;

/**
 * Created by mohitkumar on 08/06/17.
 */

public class QuikrServicesNetwork {

    static Set<Long> sUserAuthenticationErrorCodes;
    static QuikrServicesContext sQuikrServicesContext;

    public static void initialize(@NonNull QuikrServicesContext quikrServicesContext) {
        sUserAuthenticationErrorCodes = quikrServicesContext.getUserTokenErrorCodes();
        sQuikrServicesContext = quikrServicesContext;
        QuikrNetwork.initialize(quikrServicesContext);
    }
}
