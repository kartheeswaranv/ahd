package com.quikrservices.android.network;

import com.quikr.android.api.QuikrContext;
import java.util.Set;

/**
 * Created by mohitkumar on 08/06/17.
 */

public interface QuikrServicesContext extends QuikrContext {
    void onUserTokenError();
    Set<Long> getUserTokenErrorCodes();
    APIGAErrorTracker getGAErrorTracker();
}
