package com.quikrservices.android.network;


/**
 * 
 * @author pkharche
 * Constants related to Feeds screen.
 *
 */
public class Constants {

	public interface HTTP_PARAMETERS {
        String OPF="opf";
		String VERSION = "version";
	}

	public interface HTTP_HEADERS {
		String CONNECTION = "Connection";
		String USER_AGENT = "User-Agent";
		String X_QUIKR_CLIENT = "x-quikr-client";
		String X_QUIKR_CLIENT_VERSION = "x-quikr-client-version";
		String X_QUIKR_CLIENT_SID="x-quikr-sid";
	}

    public interface ERROR_CODES{
        int SOCKET_TIMEOUT =1002;
        int CONNECTION_TIMEOUT=1003;
        int UNKNOWN_HOST = 1004;
        int UNKNOWN_SERVICE =1005;
        int NO_NETWORK = 1;

    }
	public static String BODY="body";
}
