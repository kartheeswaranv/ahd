package com.quikrservices.android.network;

import android.net.Uri;
import com.quikr.android.api.QuikrRequest;
import com.quikr.android.network.Method;
import com.quikr.android.network.NetworkException;
import com.quikr.android.network.NoConnectionException;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.GsonRequestBodyConverter;
import com.quikr.android.network.converter.GsonResponseBodyConverter;
import com.quikr.android.network.converter.RequestBodyConverter;
import com.quikr.android.network.converter.ResponseBodyConverter;
import com.quikr.android.network.converter.ToStringRequestBodyConverter;
import com.quikr.android.network.converter.ToStringResponseBodyConverter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.http.conn.ConnectTimeoutException;

/**
 * Created by mohit.kumar
 * This class can be used as a wrapper to build new request
 * using quikr network library.
 * It can handle additional stuffs like token expiry , error codes etc.
 */
public class QuikrNetworkRequest<T> {

    /**
     * Make a HTTP request and return a parsed object from JSON.
     *
     * @param method Type of Request (GET/POST..)
     * @param url URL of the request to make
     * @param type Relevant class object, for Gson's reflection [POJO for JSON Response parsing]
     * @param headers Map of request headers
     * @param params Map of request parameters
     * @param callBack QuikrNetworkResponse$Callback error/success response callback.
     */

    public QuikrNetworkRequest(int method, String url, Type type, Map<String, String> headers,
      Map<String, ? extends Object> params, Callback<T> callBack, String contentType, boolean isQDPApi) {

        this.isQdpApi = isQDPApi;
        this.method = method;
        this.url = url;
        this.type = type;
        this.headers = headers;
        this.params = params;
        this.callBack = callBack;
        this.contentType = contentType;
    }

    protected int method;
    final protected String url;
    protected String contentType;
    protected Type type;
    protected Map<String, String> headers;
    protected Map<String, ? extends Object> params;
    protected Callback<T> callBack;
    protected boolean isQdpApi;
    protected QuikrRequest mRequest;
    private ResponseBodyConverter<T> mConverter;


    public void execute() {
        final QuikrRequest.Builder request = new QuikrRequest.Builder();
        if (contentType == null) {
            contentType = "application/json";
        }
        request.setContentType(contentType);
        request.setQDP(isQdpApi);
        request.setMethod(getMappedMethod(method));
        request.appendBasicHeaders(true);
        if (method == RequestMethod.GET) {
            request.setUrl(appendParams(url, params));
        } else {
            request.setUrl(url);
            if (params != null && params.size() > 0) {

                if (params.containsKey(Constants.BODY)) {
                    request.setBody(params.get(Constants.BODY).toString(), new ToStringRequestBodyConverter<String>());
                } else if (contentType.contains(ContentType.X_WWW_URL_ENCODED)) {
                    request.setBody(params, new FormEncodingUrlBodyConverter<Map<String, ? extends Object>>());
                } else {
                    request.setBody(params, new GsonRequestBodyConverter<Map<String, ? extends Object>>());
                }
            }
        }

        if (headers != null) {
            request.addHeaders(headers);
        }
        mRequest = request.build();

        if(mConverter ==null) {
            ResponseBodyConverter converter = new GsonResponseBodyConverter(type);
            if (type == String.class) {
                converter = new ToStringResponseBodyConverter();
            }
            mConverter = converter;
        }
        mRequest.execute(new com.quikr.android.network.Callback<T>() {
            @Override
            public void onSuccess(Response<T> response) {

                if (callBack != null) {
                    callBack.onSuccess(response.getBody());
                }

                if (QuikrServicesNetwork.sQuikrServicesContext != null
                  && QuikrServicesNetwork.sQuikrServicesContext.getGAErrorTracker() != null) {
                    if (response == null || response.getBody() == null) {
                        QuikrServicesNetwork.sQuikrServicesContext.getGAErrorTracker().onEmptyResponse(url);
                    }
                }
            }

            @Override
            public void onError(NetworkException e) {
                String error;
                int statusCode = -1;

                Exception exception = e;
                if (e != null && e instanceof NoConnectionException) {
                    error = null;
                    statusCode = Constants.ERROR_CODES.NO_NETWORK;
                } else if (exception != null && exception instanceof SocketTimeoutException) {
                    error = null;
                    statusCode = Constants.ERROR_CODES.SOCKET_TIMEOUT;
                } else if (exception != null && exception instanceof ConnectTimeoutException) {
                    error = null;
                    statusCode = Constants.ERROR_CODES.CONNECTION_TIMEOUT;
                } else if (exception != null && exception instanceof UnknownHostException) {
                    error = null;
                    statusCode = Constants.ERROR_CODES.UNKNOWN_HOST;
                } else if (exception != null && exception instanceof UnknownServiceException) {
                    error = null;
                    statusCode = Constants.ERROR_CODES.UNKNOWN_SERVICE;
                } else if (e == null || e.getResponse() == null || e.getResponse().getBody() == null) {
                    error = null;
                } else {
                    statusCode = e.getResponse().getStatusCode();
                    error = e.getResponse().getBody().toString();
                }

                if (callBack != null) {
                    callBack.onError(statusCode, error);
                }

                Set<Long> unauthorizedTokenErrorCodes = QuikrServicesNetwork.sUserAuthenticationErrorCodes;
                if (QuikrServicesNetwork.sQuikrServicesContext != null
                  && unauthorizedTokenErrorCodes != null
                  && unauthorizedTokenErrorCodes.contains(Long.valueOf(statusCode))) {
                    QuikrServicesNetwork.sQuikrServicesContext.onUserTokenError();
                }

                if (QuikrServicesNetwork.sQuikrServicesContext != null
                  && QuikrServicesNetwork.sQuikrServicesContext.getGAErrorTracker() != null) {
                    QuikrServicesNetwork.sQuikrServicesContext.getGAErrorTracker().onAPIError(url, statusCode, error);
                }
            }
        }, mConverter);
    }

    public void cancel() {
        if (mRequest != null) {
            mRequest.cancel();
            mRequest = null;
        }

        callBack = null;
    }


    public void setConverter(ResponseBodyConverter<T> converter) {
        this.mConverter = converter;
    }

    public interface RequestMethod {
        int GET = 0;
        int POST = 1;
        int PUT = 2;
        int DELETE = 3;
        int HEAD = 4;
        int OPTIONS = 5;
        int TRACE = 6;
        int PATCH = 7;
    }

    private Method getMappedMethod(int method) {
        switch (method) {
            case 0:
                return Method.GET;
            case 1:
                return Method.POST;
            case 2:
                return Method.PUT;
            case 3:
                return Method.DELETE;
            case 4:
                return Method.HEAD;
            case 5:
                return Method.OPTIONS;
            case 6:
                return Method.TRACE;
            case 7:
                return Method.PATCH;
        }

        return Method.GET;
    }

    public interface Callback<T> {

        void onSuccess(T response);

        void onError(int errorCode, String errorMessage);
    }

    public static String appendParams(String url, Map<String, ? extends Object> params) {
        try {
            new URL(url);
            if (params == null) {
                return url;
            } else {
                Uri.Builder e = Uri.parse(url).buildUpon();
                Iterator iterator = params.entrySet().iterator();

                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    e.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
                }

                return e.build().toString();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Url is malformed");
        }
    }

    /**
     * Converts <code>params</code> into an application/x-www-form-urlencoded encoded string.
     */

    class FormEncodingUrlBodyConverter<T> extends RequestBodyConverter<T> {

        @Override
        public byte[] convert(T t) {
            return encodeParameters((Map<String, ?>) t);
        }

        private byte[] encodeParameters(Map<String, ?> params) {
            StringBuilder encodedParams = new StringBuilder();
            String paramsEncoding = "UTF-8";
            try {
                for (Map.Entry<String, ?> entry : params.entrySet()) {
                    encodedParams.append(URLEncoder.encode(entry.getKey().toString(), paramsEncoding));
                    encodedParams.append('=');
                    encodedParams.append(URLEncoder.encode(entry.getValue().toString(), paramsEncoding));
                    encodedParams.append('&');
                }
                return encodedParams.toString().getBytes(paramsEncoding);
            } catch (UnsupportedEncodingException uee) {
                throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
            }
        }
    }

    public interface ContentType {
        String JSON = String.format("application/json; charset=%s", new Object[] { "utf-8" });
        String X_WWW_URL_ENCODED = "application/x-www-form-urlencoded";
        String TEXT_PLAIN = "text/plain";
    }
}
