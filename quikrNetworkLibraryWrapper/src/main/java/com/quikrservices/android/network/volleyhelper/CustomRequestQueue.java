package com.quikrservices.android.network.volleyhelper;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ResponseDelivery;

/**
 * Created by mohit.kumar.
 * This custom class is added to set the retry policy for the all the requests instead setting it individually
 * for all requests.
 */
public class CustomRequestQueue extends RequestQueue {

    private static final int MAX_RETRY = 2; //Total Attempts is 3.
    private static final int INITIAL_TIMEOUT = 7500; //7.5secs.
    private static final float BACK_OFF_MULTIPLIER = 1.0F;
    /*Note as per Volley's retry policy time out is calculated as below :


    Timeout - 7500 millis, Num of Attempt - 3, Back Off Multiplier - 1

    Attempt 1:

    time = time + (time * Back Off Multiplier );
    time = 7500 + 7500 * 1 = 15000
    Socket Timeout = time;
    Request dispatched with Socket Timeout of 15 Secs

    Attempt 2:

    time = time + (time * Back Off Multiplier );
    time = 15000 + 15000 * 1 = 30000
    Socket Timeout = time;
    Request dispatched with Socket Timeout of 30 Secs

    Attempt 3:

    time = time + (time * Back Off Multiplier );
    time = 30000 + 30000 * 1 = 60000
    Socket Timeout = time;
    Request dispatched with Socket Timeout of 60 Secs*/

    public CustomRequestQueue(Cache cache, Network network, int threadPoolSize, ResponseDelivery delivery) {
        super(cache, network, threadPoolSize, delivery);
    }

    public CustomRequestQueue(Cache cache, Network network, int threadPoolSize) {
        super(cache, network, threadPoolSize);
    }

    public CustomRequestQueue(Cache cache, Network network) {
        super(cache, network);
    }

    @Override
    public Request add(Request request) {
        request.setRetryPolicy(new DefaultRetryPolicy(INITIAL_TIMEOUT, MAX_RETRY, BACK_OFF_MULTIPLIER));
        return super.add(request);
    }
}
