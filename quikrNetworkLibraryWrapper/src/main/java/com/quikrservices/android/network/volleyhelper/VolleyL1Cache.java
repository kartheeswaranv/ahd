package com.quikrservices.android.network.volleyhelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import com.android.volley.toolbox.ImageLoader;

/**
 * @author Gaurav Dingolia
 */
public class VolleyL1Cache implements ImageLoader.ImageCache {

    private LruCache<String, Bitmap> mCache;

    public VolleyL1Cache(Context context) {
        mCache = new LruCache<String, Bitmap>(1024) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / (1024 * 1024);
            }
        };
    }

    @Override
    public Bitmap getBitmap(String s) {
        return mCache.get(s);
    }

    @Override
    public void putBitmap(String s, Bitmap bitmap) {
        mCache.put(s, bitmap);
    }
}