package com.quikrservices.android.network.volleyhelper;

import android.util.Log;
import com.android.volley.Cache;
import com.android.volley.toolbox.DiskBasedCache;
import java.io.File;

/**
 * Created by vineeth on 2/8/17.
 * This class fix the app from getting crash with NegativeArraySizeException
 * <p>see https://groups.google.com/forum/#!topic/volley-users/0W-oI6za8VY </p>
 */

public class CustomDiskBasedCache extends DiskBasedCache {
    public CustomDiskBasedCache(File rootDirectory) {
        super(rootDirectory);
    }

    public CustomDiskBasedCache(File rootDirectory, int maxCacheSizeInBytes) {
        super(rootDirectory, maxCacheSizeInBytes);
    }

    @Override
    public synchronized Cache.Entry get(String key) {
        try {
            return super.get(key);
        } catch (NegativeArraySizeException e) {
            Log.d("DiskBasedCache", "Error in getting cached response" + e);
            remove(key);
            return null;
        }
    }
}
