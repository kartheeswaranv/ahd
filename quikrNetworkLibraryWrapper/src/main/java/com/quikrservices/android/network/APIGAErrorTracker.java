package com.quikrservices.android.network;

/**
 * Created by mohitkumar on 30/06/17.
 */

public interface APIGAErrorTracker {
    void onEmptyResponse(String url);
    void onAPIError(String url,int errorCode, String errorMessage);
}
