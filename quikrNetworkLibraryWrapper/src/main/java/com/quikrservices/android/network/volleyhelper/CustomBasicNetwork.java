package com.quikrservices.android.network.volleyhelper;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HttpStack;

/**
 * Created by vineeth
 *
 * This class will avoid passing 'If-Modified-Since' in header by disabling CacheEntry
 * This is a work around for avoiding '304 not modified' response code
 */
public class CustomBasicNetwork extends BasicNetwork {
    public CustomBasicNetwork(HttpStack httpStack) {
        super(httpStack);
    }

    @Override
    public NetworkResponse performRequest(Request<?> request) throws VolleyError {
        request.setCacheEntry(null);
        return super.performRequest(request);
    }
}
