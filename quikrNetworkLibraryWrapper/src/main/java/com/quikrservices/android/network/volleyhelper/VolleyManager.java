package com.quikrservices.android.network.volleyhelper;

import android.content.Context;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import java.io.File;
import javax.net.ssl.HttpsURLConnection;

public class VolleyManager {
    private static final int DEFAULT_DISK_USAGE_BYTES = 5 * 1024 * 1024; // 5 mb
    private volatile static VolleyManager mInstance;
    private static Context mCtx;
    private CustomRequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private VolleyManager(Context context) {
        mCtx = context.getApplicationContext();
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue, new VolleyL1Cache(mCtx));
    }

    public static VolleyManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (VolleyManager.class) {
                // Double check idiom
                if (mInstance == null) mInstance = new VolleyManager(context);
            }
        }
        return mInstance;
    }

    public static void destroy() {
        if (mInstance != null && mInstance.getRequestQueue() != null) {
            mInstance.getRequestQueue().cancelAll(mCtx);
        }
        mInstance = null;
    }

    private static CustomRequestQueue newRequestQueue(Context context) {
        // define cache folder
        File rootCache = context.getExternalCacheDir();
        if (rootCache == null) {
            rootCache = context.getCacheDir();
        }

        File cacheDir = rootCache;
        cacheDir.mkdirs();

        HttpStack stack = new HurlStack();
        Network network = new CustomBasicNetwork(stack);
        CustomDiskBasedCache diskBasedCache = new CustomDiskBasedCache(cacheDir, DEFAULT_DISK_USAGE_BYTES);
        CustomRequestQueue queue = new CustomRequestQueue(diskBasedCache, network);
        queue.start();

        return queue;
    }

    public CustomRequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = newRequestQueue(mCtx);
        }
        return mRequestQueue;
    }

    public CustomRequestQueue getCacheRequestQueue() {
        if (mRequestQueue == null) {
            // Instantiate the cache
            Cache cache = new DiskBasedCache(mCtx.getCacheDir(), 1024 * 1024 * 5); // 1MB cap

            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());

            // Instantiate the RequestQueue with the cache and network.
            mRequestQueue = new CustomRequestQueue(cache, network);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public void cancelAllRequestsForTag(Object tag) {
        getRequestQueue().cancelAll(tag);
    }

    static {
        HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());
    }
}
