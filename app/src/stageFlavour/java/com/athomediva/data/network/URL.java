package com.athomediva.data.network;

import java.util.HashSet;
import java.util.Set;
import www.zapluk.com.BuildConfig;

/**
 * Created by mohitkumar on 21/09/16.
 */
public class URL {

    public static Set<String> getEncryptionSupportedUrls() {
        Set<String> urls = new HashSet<>();
        urls.add("api.kvekers.com:8500");
        return urls;
    }

    public static String BASE_URL = "http://api.kvekers.com:8500";
    public static final String QDP_API_AUTH_ACCESS_TOKEN = "http://api.kvekers.com/app/auth/access_token";
    public static String VERSION = BuildConfig.ENVIRONMENT;
    public static final String BASE_IMAGE_UPLOAD_URL = "http://192.168.124.85";
    public static final String URL_VIEW_TERMS_OF_SERVICE = "http://192.168.124.51:4000/terms?noH=1&noF=1";
}
