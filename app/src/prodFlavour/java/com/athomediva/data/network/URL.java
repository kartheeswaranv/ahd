package com.athomediva.data.network;

import java.util.HashSet;
import java.util.Set;
import www.zapluk.com.BuildConfig;

/**
 * Created by mohitkumar on 21/09/16.
 */
public class URL {

    public static Set<String> getEncryptionSupportedUrls() {
        Set<String> urls = new HashSet<>();
        urls.add("api.quikr.com");
        return urls;
    }

    public static String BASE_URL = "https://api.quikr.com";
    public static final String QDP_API_AUTH_ACCESS_TOKEN  = "https://api.quikr.com/app/auth/access_token";
    public static String VERSION = BuildConfig.ENVIRONMENT;
    public static String BASE_IMAGE_UPLOAD_URL = "http://raven.kuikr.com";
    public static String URL_VIEW_TERMS_OF_SERVICE = "http://www.athomediva.com/terms?noH=1&noF=1";

}
