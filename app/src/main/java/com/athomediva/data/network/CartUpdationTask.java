package com.athomediva.data.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;

/**
 * Created by kartheeswaran on 14/06/17.
 */

public class CartUpdationTask {
    private static final String TAG = LogUtils.makeLogTag(CartUpdationTask.class);

    private Context mContext;
    private QuikrNetworkRequest mUpdateBookingReq;
    private DataManager mDataManager;
    private BookingSession mBookingSession;

    public CartUpdationTask(@NonNull Context context, @NonNull DataManager manager, @NonNull BookingSession session) {
        LogUtils.LOGD(TAG, "UpdateBookingTask : ");
        mContext = context;
        mDataManager = manager;
        mBookingSession = session;
    }

    public void execute(BookingRequestData data, QuikrNetworkRequest.Callback<APICartResponse> callback,
      NetworkErrorListener networkListener) {
        execute(data, callback, networkListener, false);
    }

    public void execute(BookingRequestData data, QuikrNetworkRequest.Callback<APICartResponse> callback,
      NetworkErrorListener networkListener, boolean launchNoNetwork) {
        LogUtils.LOGD(TAG, "execute : ");
        if (mUpdateBookingReq != null) {
            mUpdateBookingReq.cancel();
        }
        mUpdateBookingReq = mDataManager.getApiManager()
          .updateCart(data, mBookingSession.getUtmData(), new QuikrNetworkRequest.Callback<APICartResponse>() {
              @Override
              public void onSuccess(APICartResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (response != null) {
                      if (response.isSuccess()) {
                          LogUtils.LOGD(TAG, "onSuccess : Response Save to Session");
                          if (TextUtils.equals(data.getAction(), AppConstants.CART_ACTION.CREATE_CART.getAction())
                            || TextUtils.equals(data.getAction(), AppConstants.CART_ACTION.ADD_SERVICE.getAction())
                            || TextUtils.equals(data.getAction(), AppConstants.CART_ACTION.DELETE_SERVICE.getAction())) {
                              mBookingSession.updateCartResponse(response, data.getServiceId());
                          } else {
                              mBookingSession.updateCartResponse(response);
                          }
                      } else {
                          if (networkListener != null && response.getError() != null) {
                              networkListener.onNetworkError((int) response.getError().getCode(),
                                response.getError().getMessage(), new WeakReference<>(mUpdateBookingReq));
                              mDataManager.getAnalyticsManager()
                                .trackGAEventForAPIFail(data.getAction() + ":" + AppUrls.URL_UPDATE_CART,
                                  response.getError());
                          }
                      }
                  }
                  if (callback != null) {
                      callback.onSuccess(response);
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (networkListener != null) {
                      networkListener.onNetworkError(errorCode, errorMessage, new WeakReference<>(mUpdateBookingReq),
                        launchNoNetwork);
                  }

                  if (callback != null) {
                      callback.onError(errorCode, errorMessage);
                  }
              }
          });

        mUpdateBookingReq.execute();
    }

    public void cancel() {
        LogUtils.LOGD(TAG, "cancel : ");
        if (mUpdateBookingReq != null) {
            mUpdateBookingReq.cancel();
        }
    }
}
