package com.athomediva.data.network;

import android.support.annotation.NonNull;
import com.athomediva.data.managers.AnalyticsManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.logger.LogUtils;
import com.quikrservices.android.network.APIGAErrorTracker;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 30/06/17.
 */

public class AHDAPIGATracker implements APIGAErrorTracker {
    private String TAG = LogUtils.makeLogTag(AHDAPIGATracker.class.getSimpleName());
    private AnalyticsManager analyticsManager;

    public AHDAPIGATracker(@NonNull AnalyticsManager analyticsManager) {
        this.analyticsManager = analyticsManager;
    }

    @Override
    public void onEmptyResponse(String url) {
        LOGD(TAG, "onEmptyResponse: url =" + url);
        if (analyticsManager != null) {
            analyticsManager.trackGAEventForAPIFail(url, GATrackerContext.Label.RESPONSE_IS_EMPTY);
        }
    }

    @Override
    public void onAPIError(String url, int errorCode, String errorMessage) {
        LOGD(TAG, "onAPIError: url =" + url + " errorMessage =" + errorMessage);
        if (analyticsManager != null) {
            analyticsManager.trackGAEventForAPIFail(url, errorCode, errorMessage);
        }
    }
}
