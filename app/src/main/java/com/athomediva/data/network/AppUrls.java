package com.athomediva.data.network;

/**
 * Created by mohitkumar on 19/04/17.
 */

public class AppUrls extends URL {
    public static final String URL_VIEW_PRIVACY_POLICY = "http://www.athomediva.com/privacy.php";
    public static final String URL_GOOGLE_AUTOSUGGEST_PLACE_API =
      "https://maps.googleapis.com/maps/api/place/autocomplete/json";
    public static final String URL_GOOGLE_PLACE_BY_ID_API = "https://maps.googleapis.com/maps/api/place/details/json";
    public static final String URL_UPLOAD_IMAGE = BASE_IMAGE_UPLOAD_URL + "/servicesUpload";
    private static final String API_VERSION1 = "/ahd/v1/user";
    private static final String API_VERSION2 = "/ahd/v2/user";
    private static final String API_VERSION3 = "/ahd/v3";
    private static final String API_VERSION4 = "/ahd/v4";
    private static final String API_VERSION5 = "/ahd/v5";
    private static final String API_VERSION6 = "/ahd/v6";
    private static final String BASE_URL_V1 = BASE_URL + API_VERSION1;
    public static final String GET_WALLET_STATS = BASE_URL_V1 + "/getwalletstats";
    public static final String GET_BOUNDS = BASE_URL_V1 + "/getbounds";
    public static final String URL_UPDATE_BOOKING_MSG = BASE_URL_V1 + "/orderMsg";
    private static final String BASE_URL_V2 = BASE_URL + API_VERSION2;
    public static final String URL_USER_SIGN_IN = BASE_URL_V2 + "/signin";
    public static final String URL_USER_SIGNUP = BASE_URL_V2 + "/signup";
    public static final String URL_GEN_OTP = BASE_URL_V2 + "/getotp";
    public static final String URL_VERIFY_OTP = BASE_URL_V2 + "/verifyotp";
    //@Deprecated
    //public static final String URL_GET_USER_DETAILS = BASE_URL_V2 + "/getUserDetails";
    public static final String URL_GET_DEEPLINK_DATAS = BASE_URL_V2 + "/deeplink";
    private static final String BASE_URL_V3 = BASE_URL + API_VERSION3;
    public static final String GET_PROMOTIONAL_OFFERS = BASE_URL_V3 + "/offers/getPromotionalAds";
    public static final String URL_USER_LOGOUT = BASE_URL_V3 + "/logout";
    public static final String URL_GET_MY_WALLET = BASE_URL_V3 + "/myWallet";
    public static final String URL_UPDATE_PROFILE = BASE_URL_V3 + "/user/updateProfile";
    public static final String URL_UPDATE_PROFILE_PICTURE = BASE_URL_V3 + "/user/picUpload";
    public static final String URL_FEEDBACK_SUBMIT = BASE_URL_V3 + "/feedback/submit";
    public static final String URL_CREATE_CART = BASE_URL_V3 + "/createCart";
    //@Deprecated
    //public static final String URL_UPDATE_CART = BASE_URL_V3 + "/updateCart";
    //@Deprecated
    //public static final String URL_GET_TIMESLOTS = BASE_URL_V3 + "/getBookingTimings";
    public static final String URL_GET_BOOKINGS = BASE_URL_V3 + "/myOrders";
    public static final String URL_GET_BOOKING_DETAILS = BASE_URL_V3 + "/getOrderById";
    public static final String URL_CANCEL_BOOKING = BASE_URL_V3 + "/cancelOrder";
    //@Deprecated
    //public static final String URL_CONFIRM_BOOKING = BASE_URL_V3 + "/confirmOrder";
    public static final String URL_GET_CART_DETAILS = BASE_URL_V3 + "/cartDetails";
    public static final String URL_APP_REVIEW_SUBMIT = BASE_URL_V3 + "/appReview";
    public static final String URL_UPDATE_SPECIAL_DAYS = BASE_URL_V3 + "/user/updateDob";
    private static final String BASE_URL_V4 = BASE_URL + API_VERSION4;
    //deprecated v3 version but used in old version
    // payment pending error handling is added in v4 version of create order
    public static final String URL_CREATE_ORDER = BASE_URL_V4 + "/createOrder";
    public static final String URL_GET_INVITE_LIST = BASE_URL_V4 + "/referral/getList";
    public static final String URL_UPDATE_INVITE_LIST = BASE_URL_V4 + "/referral/addInvitation";
    public static final String URL_GET_SERVICE_AVAILABILITY = BASE_URL_V4 + "/map/isServicable";
    private static final String BASE_URL_V5 = BASE_URL + API_VERSION5;
    private static final String BASE_URL_V6 = BASE_URL + API_VERSION6;
    //@Deprecated
    //public static String URL_GET_CATEGORIES = BASE_URL_V3 + "/category/listCategories";
    public static String URL_GET_APP_METADATA = BASE_URL_V3 + "/getMetaDetails";
    public static String URL_ADD_ADDRESS = BASE_URL_V3 + "/user/addAddress";
    //http://192.168.124.51:4400/ahd/v4
    public static String URL_DELETE_ADDRESS = BASE_URL_V3 + "/user/deleteAddress";
    public static String URL_GET_CATEGORIES = BASE_URL_V5 + "/category/listCategories";
    public static String URL_GET_USER_DETAILS = BASE_URL_V5 + "/user/getUserDetails";
    public static String URL_GET_TIMESLOTS = BASE_URL_V5 + "/getBookingTimings";
    public static String URL_UPDATE_CART = BASE_URL_V5 + "/updateCart";
    public static String URL_CONFIRM_BOOKING = BASE_URL_V5 + "/confirmOrder";
    public static String URL_USER_AVAIL_CONFIRM = BASE_URL_V6 + "/user/confirmAvailability";
}
