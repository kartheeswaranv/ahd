package com.athomediva.data.network;

import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.athomediva.logger.LogUtils;
import com.quikrservices.android.network.volleyhelper.VolleyManager;
import hugo.weaving.DebugLog;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import com.athomediva.AHDApplication;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class MultiPartFileUploadRequest extends Request<String> {
    private static final String TAG = MultiPartFileUploadRequest.class.getSimpleName();
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    //to change the lib version
    HttpEntity httpentity;
    private String FILE_PART_NAME = "object";

    private final String mFilePart;
    private final Map<String, String> mStringPart;
    private String mimeType;
    private Context mContext;
    private MultipartProgressListener progressListener;
    private MultipartResponseCallback mResponseCallback;
    private long fileLength = 0L;
    private static final int MAX_RETRY = 2; //Total Attempts is 3.
    private static final int INITIAL_TIMEOUT = 7500; //7.5secs.
    private static final float BACK_OFF_MULTIPLIER = 1.0F;
    private int mRequestCode;

    @DebugLog
    public MultiPartFileUploadRequest(Context mContext, String url, Response.ErrorListener errorListener, String filePath,
      Map<String, String> mStringPart, String filePartName, String mimeType) {
        super(Method.POST, url, errorListener);
        LogUtils.LOGD(TAG, "MultiPartFileUploadRequest : " + url);

        this.mContext = mContext;
        this.mimeType = mimeType;
        if (filePartName != null) this.FILE_PART_NAME = filePartName;
        this.mFilePart = filePath;
        this.mStringPart = mStringPart;

        buildMultipartEntity();
        httpentity = entity.build();
        setRetryPolicy(new DefaultRetryPolicy(INITIAL_TIMEOUT, MAX_RETRY, BACK_OFF_MULTIPLIER));
    }

    private void buildMultipartEntity() {
        LogUtils.LOGD(TAG, "buildMultipartEntity : ");
        String newImage = mFilePart;

        File file = new File(newImage);
        fileLength = file.length();
        entity.addPart(FILE_PART_NAME, new FileBody(file, ContentType.create(mimeType), file.getName()));
        if (mStringPart != null) {
            for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
                entity.addTextBody(entry.getKey(), entry.getValue());
            }
        }
    }

    public void execute() {
        VolleyManager.getInstance(AHDApplication.get(mContext)).addToRequestQueue(this);
    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        LogUtils.LOGD(TAG, "getBody : ");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity.writeTo(new CountingOutputStream(bos, fileLength, progressListener, getTag()));
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        LogUtils.LOGD(TAG, "parseNetworkResponse : ");
        try {
            return Response.success(new String(response.data, "UTF-8"), getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected void deliverResponse(String response) {
        LOGD("MultiPart", "deliverResponse : response - " + response);
        if (mResponseCallback != null) {
            mResponseCallback.onResponse(getTag(), response, mRequestCode);
        }
    }

    public interface MultipartProgressListener {
        void transferred(Object obj, long transfered, int progress);
    }

    public interface MultipartResponseCallback {
        void onResponse(Object obj, String response, int requestCode);
    }

    public void setProgressListener(MultipartProgressListener listener) {
        progressListener = listener;
    }

    public void setResponseCallBack(MultipartResponseCallback listener) {
        mResponseCallback = listener;
    }

    public void setRequestCode(int requestCode) {
        this.mRequestCode = requestCode;
    }

    public static class CountingOutputStream extends FilterOutputStream {
        private final MultipartProgressListener progListener;
        private long transferred;
        private long fileLength;
        private Object tag;

        public CountingOutputStream(final OutputStream out, long fileLength, final MultipartProgressListener listener,
          Object tag) {
            super(out);
            this.fileLength = fileLength;
            this.progListener = listener;
            this.transferred = 0;
            this.tag = tag;
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            if (progListener != null) {
                this.transferred += len;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(tag, this.transferred, prog);
            }
        }

        @Override
        public void write(int b) throws IOException {
            out.write(b);
            if (progListener != null) {
                this.transferred++;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(tag, this.transferred, prog);
            }
        }
    }
}
