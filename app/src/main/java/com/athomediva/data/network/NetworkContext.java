package com.athomediva.data.network;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.session.UserSession;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.quikr.android.api.QDPMetaData;
import com.quikrservices.android.network.APIGAErrorTracker;
import com.quikrservices.android.network.Constants;
import com.quikrservices.android.network.QuikrServicesContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import www.zapluk.com.BuildConfig;

/**
 * Created by mohitkumar on 03/05/17.
 */

public class NetworkContext implements QuikrServicesContext {
    private final Map<String, String> mHeaders = new HashMap<>();
    private final Map<String, String> mParams = new HashMap<>();
    private final QDPMetaData mQDPMetaData = new QDPMetaData();
    private Context mContext;
    private PreferenceManager mPrefManager;
    private UserSession mUserSession;
    private APIGAErrorTracker mGAErrorTracker;
    private Set<String> mEncryptionSupportedUrls = new HashSet<>();

    {
        mQDPMetaData.setAppDeveloperEmail(BuildConfig.APP_DEVELOPER_EMAIL);
        mQDPMetaData.setAppId(BuildConfig.APP_ID);
        mQDPMetaData.setAppSecret(BuildConfig.APP_SECRET);
        mQDPMetaData.setTokenGenerationUrl(AppUrls.QDP_API_AUTH_ACCESS_TOKEN);
        mQDPMetaData.setBaseUrl(AppUrls.BASE_URL);

        mHeaders.put(Constants.HTTP_HEADERS.CONNECTION, "Keep-Alive");
        mHeaders.put(Constants.HTTP_HEADERS.USER_AGENT, "QuikrConsumer");
        mHeaders.put(Constants.HTTP_HEADERS.X_QUIKR_CLIENT, AppConstants.CLIENT_IDENTITY_HEADER_VALUE);
        mHeaders.put(Constants.HTTP_HEADERS.X_QUIKR_CLIENT_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        mParams.put(Constants.HTTP_PARAMETERS.OPF, "json");
        mEncryptionSupportedUrls = AppUrls.getEncryptionSupportedUrls();
    }

    public NetworkContext(@NonNull Context context, @NonNull PreferenceManager preferenceManager,
      @NonNull UserSession userSession, @NonNull APIGAErrorTracker errorTracker) {
        mContext = context;
        mPrefManager = preferenceManager;
        mUserSession = userSession;
        mGAErrorTracker = errorTracker;
    }

    @Override
    public Context getApplicationContext() {
        return mContext;
    }

    @Override
    public Map<String, String> getBasicHeaders(boolean isQDP) {
        if (!TextUtils.isEmpty(mPrefManager.getSessionToken())) {
            mHeaders.put(Constants.HTTP_HEADERS.X_QUIKR_CLIENT_SID, mPrefManager.getSessionToken());
        } else {
            mHeaders.remove(Constants.HTTP_HEADERS.X_QUIKR_CLIENT_SID);
        }
        return mHeaders;
    }

    @Override
    public Map<String, String> getBasicParams(boolean b) {
        return mParams;
    }

    @Override
    public String getQDPClientVersion() {
        return String.valueOf(BuildConfig.VERSION_CODE);
    }

    @Override
    public QDPMetaData getQDPMetaData() {
        return mQDPMetaData;
    }

    @Override
    public Map<String, Object> getContextProperties() {
        return null;
    }

    @Override
    public Set<String> getEncrytionSupportedUrls() {
        return mEncryptionSupportedUrls;
    }

    @Override
    public boolean isCompressionRequiredForParams(Map<String, String> map) {
        return false;
    }

    @Override
    public boolean isEncryptionEnabled() {
        return BuildConfig.ENCRYPTION_ENABLED;
    }

    @Override
    public Map<String, String> getUTMParams() {
        return null;
    }

    @Override
    public void onUserTokenError() {
        mUserSession.logout();
        Intent intent = new Intent(mContext, AppLauncherActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    public Set<Long> getUserTokenErrorCodes() {
        Set<Long> errorCodes = new HashSet<>();
        errorCodes.add(AppConstants.APICodes.ERROR_CODE.INVALID_ACCESS_TOKEN_ON_ERROR.getErrorCode());
        return errorCodes;
    }

    @Override
    public APIGAErrorTracker getGAErrorTracker() {
        return mGAErrorTracker;
    }
}
