package com.athomediva.data.network;

import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;

/**
 * Created by kartheeswaran on 19/06/17.
 */


public interface NetworkErrorListener {
    void onNetworkError(int errorCode, String errorMessage, WeakReference<QuikrNetworkRequest> networkRequest);

    void onNetworkError(int errorCode, String errorMessage, WeakReference<QuikrNetworkRequest> networkRequest,
      boolean launchNoNetwork);
}
