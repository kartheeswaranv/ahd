package com.athomediva.data.network.contracts;

/**
 * Created by mohitkumar on 19/09/16.
 */
public interface HttpConstants {

    interface BASE_DETAILS {
        String PARAM_MOBILE = "mobile";
        String PARAM_EMAILID = "email";
        String PARAM_DEVICE_DETAILS = "deviceDetails";
        String PARAM_GCM_REGID = "gpn";
        String PARAM_OS_VERSION = "osVersion";
        String PARAM_MODEL_NAME = "deviceName";
        String PARAM_DEVICE_RESOLUTION = "dimenssions";
        String PARAM_DEVICE_UID = "MID";
        String PARAM_NAME = "name";
    }

    interface GOOGLE_SUGGESTED_PLACES_PARAMS {
        String PARAM_TYPES = "types";
        String PARAM_KEY = "key";
        String PARAM_INPUT = "input";
        String PARAM_COMPONENTS = "components";
    }

    interface GOOGLE_PLACES_PARAMS extends GOOGLE_SUGGESTED_PLACES_PARAMS {
        String PARAM_PLACE_ID = "placeid";
    }

    interface SIGN_UP extends BASE_DETAILS {

        String PARAM_GENDER = "gender";
        String PARAM_REF_CODE = "refCode";
    }

    interface GENERATE_OTP extends BASE_DETAILS {

    }

    interface VERIFY_OTP extends BASE_DETAILS {
        String PARAM_OTP = "otp";
    }

    interface CATEGORIES {
        String PARAM_LAT = "lat";
        String PARAM_LONG = "lon";
        String PARAM_USE_MY_LOCATION = "useMyLocation";
    }

    interface ADD_ADDRESS {
        String PARAM_LAT = "lat";
        String PARAM_LONG = "lon";
        String PARAM_LABEL = "label";
        String PARAM_HOUSE_NAME = "houseNo";
        String PARAM_ADDRESS = "address";
        String PARAM_LANDMARK = "landmark";
        String PARAM_PINCODE = "pincode";
    }

    interface DELETE_ADDRESS {
        String PARAM_DELETE_ADDRESS = "addressId";
    }

    interface GENERAL_PARAMS {
        String PARAM_USER_ID = "userId";
        String PARAM_VERSION = "version";
        String PARAM_IOS = "isIos";
        String UTM_DATA = "utmData";
    }

    interface CATEGORIES_PARAMS {
        String PARAM_LATITUDE = "lat";
        String PARAM_LANGITUDE = "lng";
        String PARAM_APP_VERISON = "appVersion";
        String PARAM_USER_SELECTION = "userSelection";
        String PARAM_CAT_ID = "catId";
        String PARAM_SUB_CAT_ID = "subCatId";
    }

    interface COUPONS_PARAMS {
        String PARAM_COUPON_CODE = "coupon";
    }

    interface TIMINGS_PARAMS {
        String PARAM_FROM_VALUE = "fromVal";
        String PARAM_TO_VALUE = "toVal";
    }

    interface ORDER_PARAMS {
        String PARAM_ORDER_ID = "orderId";
        String PARAM_STATUS_TEXT = "statusText";
        String PARAM_PROF_ID = "profId";
        String PARAM_CANCEL_STATUS = "directCancel";
        String PARAM_MESSAGE = "message";
        String PARAM_CANCEL_REASON = "cancelReason";
    }

    interface UPDATE_BOOKING_PARAMS {
        String PARAM_BOOKING_ID = "bookingId";
        String PARAM_ACTION = "action";
        String PARAM_COUPON_CODE = "coupon";
        String PARAM_WALLET_TYPE = "walletType";
        String PARAM_IS_APPLIED = "isApplied";
        String PARAM_ADDRESS_ID = "addressId";
        String PARAM_DATE = "date";
        String PARAM_FROM = "from";
        String PARAM_To = "to";
        String PARAM_SPL_INSTR = "special_inst";
        String PARAM_LOC_LAT = "lat";
        String PARAM_LOC_LON = "lon";
        String PARAM_SELECTED_SERVICES = "cart";
        String PARAM_POOLING_ID = "poolId";
        String PARAM_BUCKET_ID = "bucketId";
    }

    interface REVIEWS_PARAMS {
        String PARAM_START = "start";
        String PARAM_END = "end";
    }

    interface WALLET_PARAMS {
        String PARAM_WALLET_CHECKED = "checked";
    }

    interface ADDRESS_PARAMS {
        String PARAM_CIRCLE_NAME = "cName";
    }

    interface BOOKING_PARAMS {
        String PARAM_BOOKING_TYPE = "type";
    }

    interface CUSTOMER_REVIEW {
        String PARAM_CUSTOMER_ID = "customerId";
    }

    interface PAYMENT_PARAMS {
        String PARAM_PAYMENT_TYPE = "payType";
    }

    interface PROFESSION_PARAMS {
        String PARAM_COUNT = "count";
    }

    interface CONFIRM_BOOKING extends BASE_DETAILS {
        String PARAM_MESSAGE = "message";
    }

    interface DEEPLINK_PARAMS {
        String PARAM_URL = "url";
    }

    interface FEEDBACK_PARAMS {
        String PARAM_COMMENTS = "comments";
    }

    interface APP_FEEDBACK_PARAMS {
        String PARAM_COMMENT = "comment";
        String PARAM_RATING = "rating";
    }

    interface UPDATE_PROFILE_PARAMS {
        String PARAM_USERNAME = "userName";
        String PARAM_EMAIL = "email";
        String PARAM_GENDER = "gender";
        String PARAM_MOBILE_NO = "mobileNum";
        String PARAM_IAMGE = "image";
        String PARAM_IMAGE_DENSITY = "image_density";
    }

    interface CART_PARAMS {
        String PARAM_CIRCLE_NAME = "circleName";
        String PARAM_SERVICE_ID = "serviceId";
        String PARAM_CART_ID = "cartId";
        String PARAM_ACTION = "action";
        String PARAM_ADDRESS_ID = "addressId";
        String PARAM_DATE = "date";
        String PARAM_FROM = "from";
        String PARAM_TO = "to";
        String PARAM_SLOT = "slot";
        String PARAM_BOOKING_ID = "bookingId";
        String PARAM_COUPON = "coupon";
        String PARAM_BOOKING_RESPONSE = "bookingResponse";
        String PARAM_LATITUDE = "latitude";
        String PARAM_LONGITUDE = "longitude";
        String PARAM_CIRCLE = "circle";
    }

    interface SPECIAL_OFFER_PARAMS {
        String PARAM_DATE_OF_BIRTH = "dob";
        String PARAM_ANNIVERSARY = "anniversary";
    }

    interface INVITE_PARAMS {
        String PARAM_INVITE_LIST = "inviteList";
        String PARAM_CONTACT_LIST = "contactList";
    }

    interface AVAILABILITY_PARAMS {
        String PARAM_IDENTIFIER = "identifier";
    }
}
