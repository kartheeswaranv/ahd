package com.athomediva.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.WalletInfo;
import com.athomediva.data.network.contracts.HttpConstants;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 19/04/17.
 */

public class AppConstants {
    public static final String AHD_EMAIL_ID = "athomediva@quikr.com";
    public static final String AHD_CUSTOMER_CARE_NUMBER = "08046333333";
    public static final int OTP_LENGTH = 6;
    public static final String SMS_SENDER = "Quikr";
    public static final String OTP_DELIMITER = "is ";
    public static final float BLUR_RADIUS = 5f;
    public static final String AHD_FB_LINK = "https://www.facebook.com/AtHomeDiva/";
    public static final String AHD_TWITTER_LINK = "https://twitter.com/AtHomeDiva";
    public static final String AHD_INSTAGRAM_LINK = "https://www.instagram.com/athomediva_official/";
    public static final String AHD_GOOGLEPLUS_LINK = "https://plus.google.com/+Athomediva";
    public static final int AUTO_VERIFY_TIMER = 60;
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final int MAX_SOCIAL_ITEMS = 4;
    public static final int GOOGLE_AUTOSUGGEST_QUERY_CHAR_THRESHOLD = 3;
    public static final String SPLASH_VIDEO_FILE_NAME = "launch_video.mp4";
    public static final int SPLASH_CLOSE_TIME = 4000;//ms
    public static final int LOGIN_SPLASH_LAUNCH_DELAY_TIME = 1000;
    public static final int CAMERA_DEFAULT_ZOOM = 15;
    private static final String TAG = LogUtils.makeLogTag(AppConstants.class.getSimpleName());
    public static String GCM_SENDER_ID = "101274829107";
    public static String CLIENT_IDENTITY_HEADER_VALUE = "ahd.android.consumer";
    public static long UPDATE_LATER_ASK_TIMESTAMP = 7 * 24 * 60 * 60 * 1000;

    public static int MINIMUM_STATE = 1;
    public static int MAXIMUM_STATE = 10;
    public static String[] SOCIAL_NETWORK_PACKAGE_PRIORITIZED_LIST = {
      "com.whatsapp", "com.google.android.talk", "com.facebook.orca", "com.google.android.apps.messaging",
      "com.google.android.gm", "com.facebook.katana", "com.google.android.apps.fireball", "com.bsb.hike"
    };

    public static float APP_LOW_RATING_RANGE = 3;
    public static float SERVICE_AVG_RATING_ENABLE_PLAYSTORE = 8;

    public static long PAYMENT_REDIRECT_DURATION = 5000;

    public static String UPDATE_MOBILE = "updateMobile";

    public static long BACK_PRESS_DURATION = 3000;

    public static String FCM_TOPIC_ALL_DEVICES = "/topics/alldevices";

    public static long CONTACT_FETCHING_DELAY = 1000;

    public static String PINCODE_PREFIX = "pincode-";

    public static LatLngBounds getIndiaLatLngBounds() {
        return new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));
    }

    public static List<TitleDescImageItem> getLoginPromoItem(Context context) {
        if (context == null) {
            LogUtils.LOGE(TAG, "context should not be null *****");
            return null;
        }

        List<TitleDescImageItem> titleDescImageItemList = new ArrayList<>();
        titleDescImageItemList.add(
          new TitleDescImageItem(R.drawable.ahd_pager_diva, context.getString(R.string.banner_1_title),
            context.getString(R.string.banner_1_desc)));
        titleDescImageItemList.add(
          new TitleDescImageItem(R.drawable.ahd_pager_female, context.getString(R.string.banner_2_title),
            context.getString(R.string.banner_2_desc)));
        titleDescImageItemList.add(
          new TitleDescImageItem(R.drawable.ahd_pager_howitworks, context.getString(R.string.banner_3_title),
            context.getString(R.string.banner_3_desc)));
        titleDescImageItemList.add(
          new TitleDescImageItem(R.drawable.ahd_pager_highlights, context.getString(R.string.banner_4_title),
            context.getString(R.string.banner_4_desc)));

        return titleDescImageItemList;
    }

    public static List<TitleDescImageItem> getAccountsItem(Context context, APIFullUserDetailsResponse response) {
        if (context == null) {
            LogUtils.LOGE(TAG, "context should not be null *****");
            return null;
        }

        List<TitleDescImageItem> list = new ArrayList<>();
        String walletAmount = "0";
        if (response != null && response.getData() != null && response.getData().getWallet() != null) {
            walletAmount = String.valueOf(response.getData().getWallet().getTotalAmount());
        }
        list.add(new TitleDescImageItem(R.drawable.ic_wallet, context.getString(R.string.ahd_my_wallet),
          UiUtils.getFormattedAmountByString(context, walletAmount)));
        list.add(new TitleDescImageItem(R.drawable.ahd_ic_addresses, context.getString(R.string.my_addresses)));

        list.add(new TitleDescImageItem(R.drawable.ic_offers_coupons, context.getString(R.string.ahd_offers_coupans)));
        list.add(new TitleDescImageItem(R.drawable.ic_invite_friends, context.getString(R.string.ahd_invite_a_friend)));
        list.add(new TitleDescImageItem(R.drawable.ahd_ic_call, context.getString(R.string.ahd_customer_service)));
        list.add(new TitleDescImageItem(R.drawable.ic_cancel_policy, context.getString(R.string.ahd_cancellation_policy)));
        list.add(new TitleDescImageItem(R.drawable.ahd_ic_rate, context.getString(R.string.ahd_rate_this_app)));
        list.add(new TitleDescImageItem(R.drawable.ic_logout_ahd, context.getString(R.string.ahd_logout)));

        return list;
    }

    public static List<TitleDescImageItem> getWalletBreakUpInfo(Context context, List<WalletInfo> walletInfoList) {
        List<TitleDescImageItem> titleDescImageItemList = new ArrayList<>();

        for (WalletInfo info : walletInfoList) {
            int res = 0;
            if (info.getName().equals(WALLET_BREAK_UP_INFO.CASH_CREDITS.getTitle())) {
                res = WALLET_BREAK_UP_INFO.CASH_CREDITS.getRes();
            } else if (info.getName().equals(WALLET_BREAK_UP_INFO.PROMOTIONAL_CREDITS.getTitle())) {
                res = WALLET_BREAK_UP_INFO.PROMOTIONAL_CREDITS.getRes();
            } else if (info.getName().equals(WALLET_BREAK_UP_INFO.REFERRAL_CREDITS.getTitle())) {
                res = WALLET_BREAK_UP_INFO.REFERRAL_CREDITS.getRes();
            } else if (info.getName().equals(WALLET_BREAK_UP_INFO.PAYMENT_PENDING.getTitle())) {
                res = WALLET_BREAK_UP_INFO.PAYMENT_PENDING.getRes();
            }
            titleDescImageItemList.add(
              new TitleDescImageItem(res, info.getName(), UiUtils.getFormattedAmount(context, info.getTotalAmount())));
        }

        return titleDescImageItemList;
    }

    public static EmptySectionItem getServicesNotAvailableItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.service_unavailable),
          context.getString(R.string.srvice_dont_provide_at_location_desc), R.drawable.ahd_img_location_blank,
          context.getString(R.string.change_location));
    }

    public static EmptySectionItem getLocationUnavailableItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.location_unavailable),
          context.getString(R.string.location_enable_desc_msg), R.drawable.ahd_img_location_blank,
          context.getString(R.string.search_location));
    }

    public static EmptySectionItem getEmptyAddressItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.empty_address_title),
          context.getString(R.string.add_Address_help_text), R.drawable.ahd_ic_address_blank,
          context.getString(R.string.add_address));
    }

    public static EmptySectionItem getEmptyOffersPageItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.empty_offers_title),
          context.getString(R.string.empty_offers_desc), R.drawable.ahd_ic_coupon_blank,
          context.getString(R.string.start_exploring));
    }

    public static EmptySectionItem getEmptyCartItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.mycart_empty_page_title),
          context.getString(R.string.mycart_empty_page_desc), R.drawable.ahd_ic_cart_blank,
          context.getString(R.string.mycart_empty_page_action));
    }

    public static EmptySectionItem getEmptyBookingItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.my_bookings_empty_title),
          context.getString(R.string.my_bookings_empty_desc), R.drawable.ahd_ic_booking_blank,
          context.getString(R.string.mycart_empty_page_action));
    }

    public static EmptySectionItem getEmptySuggestedAddressItem(@NonNull Context context, String query) {
        return new EmptySectionItem(context.getString(R.string.no_result_for, query),
          context.getString(R.string.change_the_keyword), R.drawable.ahd_ic_search_blank);
    }

    public static ArrayList<SpecialOfferDateModel> getSpecialOfferDateList(@NonNull Context context) {
        ArrayList<SpecialOfferDateModel> list = new ArrayList<>();
        SpecialOfferDateModel birthday = new SpecialOfferDateModel(SPECIAL_OFFER_DETAILS.BIRTHDAY.getType(),
          context.getString(SPECIAL_OFFER_DETAILS.BIRTHDAY.getHintRes()), SPECIAL_OFFER_DETAILS.BIRTHDAY.getApiParams());
        SpecialOfferDateModel anniversary = new SpecialOfferDateModel(SPECIAL_OFFER_DETAILS.ANNIVERSARY.getType(),
          context.getString(SPECIAL_OFFER_DETAILS.ANNIVERSARY.getHintRes()),
          SPECIAL_OFFER_DETAILS.ANNIVERSARY.getApiParams());
        /*birthday.setDisabled(true);
        birthday.setSelectedDateTxt("07/06/1988");

        anniversary.setDisabled(true);
        anniversary.setSelectedDateTxt("07/06/2017");*/
        list.add(birthday);
        list.add(anniversary);

        return list;
    }

    public static EmptySectionItem getNoNetworkItem(@NonNull Context context) {
        return new EmptySectionItem(context.getString(R.string.no_network_title),
          context.getString(R.string.no_network_desc), R.drawable.ahd_ic_network_blank,
          context.getString(R.string.no_network_action));
    }

    public static List<SocialSharingItem> getSocialLinks(Context context) {

        List<SocialSharingItem> list = new ArrayList<>();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_FB_LINK));
        list.add(new SocialSharingItem(context.getString(R.string.facebook),
          ContextCompat.getDrawable(context, R.drawable.ahd_ic_facebook), intent));
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_TWITTER_LINK));
        list.add(new SocialSharingItem(context.getString(R.string.twitter),
          ContextCompat.getDrawable(context, R.drawable.ahd_ic_twitter), intent));
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_GOOGLEPLUS_LINK));
        list.add(new SocialSharingItem(context.getString(R.string.google_plus),
          ContextCompat.getDrawable(context, R.drawable.ahd_ic_google), intent));
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_INSTAGRAM_LINK));
        list.add(new SocialSharingItem(context.getString(R.string.instagram),
          ContextCompat.getDrawable(context, R.drawable.ahd_ic_instagram), intent));
        return list;
    }

    enum WALLET_BREAK_UP_INFO {
        PROMOTIONAL_CREDITS("Promotional Credits", R.drawable.ic_offers_coupons),
        REFERRAL_CREDITS("Referral Credits", R.drawable.ic_invite_friends),
        CASH_CREDITS("Cash Credits", R.drawable.ahd_ic_rupe),
        PAYMENT_PENDING("Payment Pending", R.drawable.ahd_ic_payment_pending);

        private final int res;
        private final String title;

        WALLET_BREAK_UP_INFO(String title, int res) {
            this.title = title;
            this.res = res;
        }

        public int getRes() {
            return res;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public enum ORDER_SORT_TYPE {
        SORT_BY_CATEGORY(1),
        SORT_BY_AMOUNT(2);

        int type = 0;

        ORDER_SORT_TYPE(int value) {
            type = value;
        }

        public int getType() {
            return type;
        }
    }

    public enum SERVICE_SELECTION_TYPE {
        MULTI_SELECT(1),
        SINGLE_SELECT(0);
        int type = 0;

        SERVICE_SELECTION_TYPE(int value) {
            type = value;
        }

        public int getType() {
            return type;
        }
    }

    public enum BOOKING_ACTION {
        CREATE_BOOKING("createBooking"),
        UPDATE_SERVICES("servicesUpdate"),
        UPDATE_ADDRESS("address"),
        UPDATE_DATE_TIME("addDateTime"),
        UPDATE_WALLET("wallet"),
        UPDATE_COUPON("coupon"),
        BOOKING_SUMMARY("bookingSummary"),
        CONFIRM("confirmBooking");

        String mAction;

        BOOKING_ACTION(String action) {
            mAction = action;
        }

        public String getAction() {
            return mAction;
        }
    }

    public enum IMAGE_SIZE {
        SMALL(0.75f, "sm"),
        MEDIUM(1.0f, "md"),
        LARGE(1.5f, "lg"),
        XTRA_LARGE(2.0f, "xs");

        float densityValue;
        String key;

        IMAGE_SIZE(float density, String key) {
            densityValue = density;
            this.key = key;
        }

        public float getDensity() {
            return densityValue;
        }

        public String getKey() {
            return key;
        }
    }

    public enum CART_ACTION {
        //Locally Define this Action
        CREATE_CART("createCart"),

        ADD_SERVICE("addService"),
        // params serviceId,CartId
        DELETE_SERVICE("deleteService"),
        // params serviceId,CartId
        UPDATE_ADDRESS("addAddress"),
        // params cartId,selected address
        UPDATE_TIME("updateTimings"),
        //params cartId,bookingId,dateSlot
        UPDATE_WALLET("updateWallet"),
        //Params cartId,coupon
        UPDATE_COUPON("applyCoupon"),
        // Params cartId
        REMOVE_COUPON("removeCoupon");

        String action;

        CART_ACTION(String action) {
            this.action = action;
        }

        public String getAction() {
            return action;
        }
    }

    public enum ORDER_PROGRESS_STATUS {
        BOOKING_CONFIRMED(1),
        STYLIST_ON_WAY(2),
        START_SERVICE(3),
        // Payment Pending and Rate Service will come here
        END_SERVICE(4),
        BOOKING_COMPLETED(5);

        int status;

        ORDER_PROGRESS_STATUS(int value) {
            status = value;
        }

        public int getStatus() {
            return status;
        }
    }

    // ADD Type and Hint Text to show in the special dates item
    public enum SPECIAL_OFFER_DETAILS {
        BIRTHDAY(0, R.string.birthday_hint, HttpConstants.SPECIAL_OFFER_PARAMS.PARAM_DATE_OF_BIRTH),
        ANNIVERSARY(1, R.string.anniversary_hint, HttpConstants.SPECIAL_OFFER_PARAMS.PARAM_ANNIVERSARY);

        int hintRes;
        int type;
        String param;

        SPECIAL_OFFER_DETAILS(int type, int res, String param) {
            this.type = type;
            this.hintRes = res;
            this.param = param;
        }

        public int getHintRes() {
            return hintRes;
        }

        public int getType() {
            return type;
        }

        public String getApiParams() {
            return param;
        }
    }

    public enum INVITE_STATUS {
        NOT_INVITE(0, "Invite"),
        INVITED(1, "Invited"),
        REINVITE(3, "ReInvite"),
        ALREADY_MEMBER(2, "Already in Diva Family");

        int status;
        String title;

        INVITE_STATUS(int status, String title) {
            this.status = status;
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public int getStatus() {
            return status;
        }
    }

    public interface IMAGE_UPLOAD {
        String PARAM_BODY = "body";
        String PARAM_IMAGE = "image";
        String PARAM_TITLE = "title";
        String PARAM_CATEGORY = "category";
        String PARAM_CLIENT_ID = "clientId";
        String PARAM_IMAGE_SIZE = "image_density";
    }

    public interface IMAGE_UPLOAD_VALUES {
        String VALUE_CATEGORY = "ahd";
        String VALUE_CLIENT_ID = "profile";
    }

    public static class APICodes {

        public enum ERROR_CODE {
            NO_NETWORK(0),
            NETWORK_ERROR(1),
            OTHER_ERROR(2),
            TOKEN_EXPIRED(999),
            CLIENT_NOT_PRESENT_IN_HEADER(1001),
            MOBILE_NUMBER_ALREADY_REGISTERED(1005),
            INVALID_REFERRAL_CODE(1006),
            USER_BLOCKED(1009),
            NUMBER_NOT_REGISTERED(1010),
            VALIDATION_ERROR(1012),
            SERVICES_NOT_SERVED_IN_LOCATION(903),
            INVALID_ACCESS_TOKEN(9036),
            INVALID_ACCESS_TOKEN_ON_ERROR(403),
            // All Bookings are failed while confirming or any one of the booking is failed
            BOOKING_CONFIRM_FAILED(11131),
            UPDATE_MOBILE_WITH_OTP(3),
            STYLIST_NOT_AVAILABLE(9046),
            PAYMENT_PENDING_ERROR(11130),
            CANCEL_BOOKING_FAILURE(11140);

            private final long errorCode;

            ERROR_CODE(long errorCode) {
                this.errorCode = errorCode;
            }

            public long getErrorCode() {
                return errorCode;
            }

            @Override
            public String toString() {
                return super.toString();
            }
        }
    }
}
