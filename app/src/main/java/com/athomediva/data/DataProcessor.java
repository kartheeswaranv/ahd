package com.athomediva.data;

import android.content.Context;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Contact;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.InviteRequest;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.BookingService;
import com.athomediva.data.models.remote.BookingServiceGroup;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartServiceGroup;
import com.athomediva.data.models.remote.InvitedContact;
import com.athomediva.data.models.remote.Location;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.models.remote.RatingsConfig;
import com.athomediva.data.models.remote.Services;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import com.google.android.gms.maps.model.LatLng;
import hugo.weaving.DebugLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import rx.Single;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class DataProcessor {
    private final String TAG = LogUtils.makeLogTag(DataProcessor.class.getSimpleName());
    private final Context mContext;

    @Inject
    public DataProcessor(Context context) {
        mContext = context;
    }

    public static String getCouponMessage(Context context, InstantCoupon coupon) {

        String percentage = coupon.getPercentage() + "%";
        String amount = UiUtils.getFormattedAmount(context, coupon.getAmount());
        String message = context.getString(R.string.instant_offer_format_txt, percentage, amount);

        return message;
    }

    @DebugLog
    public Single<Consumer> getConsumerInfoObservable(Consumer consumer) {
        return Single.fromCallable(() -> {
            LOGD(TAG, "mConsumerInfoObservable call: current thread =" + Thread.currentThread().getName());
            return fillAdditionalConsumerInformation(consumer);
        });
    }

    @DebugLog
    @WorkerThread
    protected Consumer fillAdditionalConsumerInformation(Consumer consumer) throws IOException {
        LOGD(TAG, "fillAdditionalConsumerInformation: " + System.currentTimeMillis());
        String gcmRegID = AndroidHelper.getGcmId();
        consumer.getDevice().setGcmId(gcmRegID);
        LOGD(TAG, "fillAdditionalConsumerInformation: gcm id = " + gcmRegID + " " + System.currentTimeMillis());
        return consumer;
    }

    public List<LatLng> convertLocObjToLatLng(ArrayList<Location> locationList) {
        LogUtils.LOGD(TAG, "convertLocObjToLatLng : ");
        try {
            if (locationList != null && locationList.size() > 0) {
                ArrayList<LatLng> latLngList = new ArrayList<>();
                for (Location loc : locationList) {
                    latLngList.add(new LatLng(Double.valueOf(loc.getLat()), Double.valueOf(loc.getLng())));
                }

                return latLngList;
            }
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "convertLocObjToLatLng : " + e.getMessage());
            return Collections.emptyList();
        }
        return Collections.emptyList();
    }

    public List<RatingsModel> convertConfigToRating(ArrayList<RatingsConfig> ratingsConfigs) {
        LogUtils.LOGD(TAG, "convertConfigToRating : ");

        if (ratingsConfigs != null && ratingsConfigs.size() > 0) {
            List<RatingsModel> list = new ArrayList<>();
            for (RatingsConfig config : ratingsConfigs) {
                RatingsModel model =
                  new RatingsModel(config.getTitle(), config.getRating(), config.getOptional(), config.getMax());
                list.add(model);
            }
            return list;
        }

        return Collections.emptyList();
    }

    public int getQtyOfServiceInCart(APICartResponse response, Services service) {
        LogUtils.LOGD(TAG, "getQtyOfServiceInCart : ");
        if (response != null
          && response.isSuccess()
          && response.getData() != null
          && response.getData().getBuckets() != null) {
            for (CartBucketItem bucketItem : response.getData().getBuckets()) {
                return getServiceQty(bucketItem, service);
            }
        }
        return 0;
    }

    private int getServiceQty(CartBucketItem item, Services service) {
        LogUtils.LOGD(TAG, "hasServiceInBucket : ");
        if (item != null && item.getServicesGroupList() != null && !item.getServicesGroupList().isEmpty()) {
            for (CartServiceGroup groupItem : item.getServicesGroupList()) {
                int qty = getServiceQty(groupItem, service);
                if (qty != -1) {
                    return qty;
                }
            }
        }

        return 0;
    }

    private int getServiceQty(CartServiceGroup group, Services serv) {
        LogUtils.LOGD(TAG, "hasServiceInGroup : ");
        if (group != null && group.getServices() != null && !group.getServices().isEmpty()) {
            for (Services services : group.getServices()) {
                if (services.getServiceId() == serv.getServiceId()) {
                    return services.getQuantity();
                }
            }
        }

        return -1;
    }

    public ArrayList<Services> convertToCartService(ArrayList<BookingServiceGroup> groupList) {

        LogUtils.LOGD(TAG, "convertToCartService : ");
        if (groupList != null && groupList.size() > 0) {
            ArrayList<Services> allService = new ArrayList<>();
            for (BookingServiceGroup servGroup : groupList) {
                ArrayList<Services> item = getUpdateServicesWithGroupName(servGroup);
                if (item != null) {
                    allService.addAll(item);
                }
            }

            return allService;
        }

        return null;
    }

    private ArrayList<Services> getUpdateServicesWithGroupName(BookingServiceGroup group) {
        LogUtils.LOGD(TAG, "getUpdateServicesWithGroupName : ");

        if (group != null && group.getList() != null && !group.getList().isEmpty()) {
            ArrayList<Services> list = new ArrayList<>();
            for (BookingService service : group.getList()) {
                Services s = new Services(service.getName(), service.getDuration(), service.getQty(), service.getPrice());
                list.add(s);
            }
            if (list.size() > 0) {
                list.get(0).setGroupName(group.getCat_name());
                return list;
            }
        }

        return null;
    }

    public ArrayList<SpecialOfferDateModel> getUpdatedSpecialDates(ArrayList<SpecialOfferDateModel> list) {
        if (list != null && !list.isEmpty()) {
            ArrayList<SpecialOfferDateModel> temp = new ArrayList<>();
            for (SpecialOfferDateModel model : list) {
                if (!model.isDisabled() && !TextUtils.isEmpty(model.getSelectedDateTxt())) {
                    temp.add(model);
                }
            }
            return temp;
        }

        return null;
    }

    public ArrayList<SpecialOfferDateModel> createSpecialDatesFromUserDetails(LoggedInUserDetails details) {
        if (details != null) {
            ArrayList<SpecialOfferDateModel> temp = new ArrayList<>();
            SpecialOfferDateModel birthday =
              getModelForSpecialDate(AppConstants.SPECIAL_OFFER_DETAILS.BIRTHDAY, details.getDob());
            SpecialOfferDateModel anniversary =
              getModelForSpecialDate(AppConstants.SPECIAL_OFFER_DETAILS.ANNIVERSARY, details.getAnniversary());

            temp.add(birthday);
            temp.add(anniversary);
            return temp;
        }
        return null;
    }

    private SpecialOfferDateModel getModelForSpecialDate(AppConstants.SPECIAL_OFFER_DETAILS det, String date) {
        SpecialOfferDateModel model =
          new SpecialOfferDateModel(det.getType(), mContext.getString(det.getHintRes()), det.getApiParams());
        model.setSelectedDate(UiUtils.getMillisFromDate(date));
        model.setSelectedDateTxt(date);
        model.setDisabled(!TextUtils.isEmpty(date));
        return model;
    }

    public List<InvitedContact> getUpdatedContacts(List<InvitedContact> invitedListFromApi,
      List<InviteRequest> localInvitedList) {
        LogUtils.LOGD(TAG, "getUpdatedContacts : ");
        if (invitedListFromApi != null && !invitedListFromApi.isEmpty() && !localInvitedList.isEmpty()) {
            ArrayList<InvitedContact> contact = new ArrayList<>();
            for (int index = 0; index < localInvitedList.size(); index++) {
                int pos = getUpdatePosition(localInvitedList.get(index), invitedListFromApi);
                if (pos > -1) {
                    contact.add(invitedListFromApi.get(pos));
                }
            }

            return contact;
        }
        return invitedListFromApi;
    }

    public ArrayList<Contact> getRefreshItems(List<Contact> list, List<InvitedContact> invitedListFromApi,
      List<InviteRequest> inviteRequestList) {
        LogUtils.LOGD(TAG, "getRefreshItems : ");

        List<InvitedContact> invitedList = getUpdatedContacts(invitedListFromApi, inviteRequestList);

        return getContactsListToUpdate(list, invitedList);
    }

    private int getUpdatePosition(InviteRequest request, List<InvitedContact> inviteFromApi) {

        if (inviteFromApi != null && !inviteFromApi.isEmpty() && request != null) {
            InvitedContact contact = new InvitedContact(request.getMobile());
            int pos = inviteFromApi.indexOf(contact);
            return pos;
        }

        return -1;
    }

    public ArrayList<Contact> getContactsListToUpdate(List<Contact> list, List<InvitedContact> invitedList) {
        LogUtils.LOGD(TAG, "getContactsListToUpdate : ");

        if ((list != null && list.size() > 0) && (invitedList != null && invitedList.size() > 0)) {
            ArrayList<Contact> posList = new ArrayList<>();
            for (int index = 0; index < invitedList.size(); index++) {
                Contact c = new Contact(invitedList.get(index).getInvitedMobile(), null);
                int pos = list.indexOf(c);
                if (pos > -1) {
                    list.get(pos).setStatus(invitedList.get(index).getStatus());
                    list.get(pos).setChecked(false);
                    posList.add(list.get(pos));
                }
            }
            return posList;
        }

        return null;
    }

    public ArrayList<InviteRequest> getInviteRequestList(List<Contact> list) {
        LogUtils.LOGD(TAG, "getInviteRequestList : ");

        if (list != null && list.size() > 0) {
            ArrayList<InviteRequest> inviteList = new ArrayList<>();
            for (int index = 0; index < list.size(); index++) {
                if (list.get(index).getStatus() == AppConstants.INVITE_STATUS.NOT_INVITE.getStatus()
                  || list.get(index).getStatus() == AppConstants.INVITE_STATUS.REINVITE.getStatus()) {
                    InviteRequest c = new InviteRequest(list.get(index).getPhoneNo(), list.get(index).getStatus());
                    inviteList.add(c);
                }
            }
            return inviteList;
        }

        return null;
    }
}
