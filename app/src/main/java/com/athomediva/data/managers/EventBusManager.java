package com.athomediva.data.managers;

import android.content.Context;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by mohit kumar on 13/01/17.
 * Manager responsible for event management
 */
@Singleton
public class EventBusManager {

    @Inject
    public EventBusManager(Context context) {
    }

    public <T> void post(T event) {
        EventBus.getDefault().post(event);
    }

    public <T> void postSticky(T event) {
        EventBus.getDefault().postSticky(event);
    }

    public <T> void register(T listener) {
        EventBus.getDefault().register(listener);
    }

    public <T> void unregister(T listener) {
        EventBus.getDefault().unregister(listener);
    }
}
