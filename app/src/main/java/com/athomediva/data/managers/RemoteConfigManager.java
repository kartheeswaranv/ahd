package com.athomediva.data.managers;

import android.content.Context;
import com.athomediva.logger.LogUtils;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by mohitkumar on 30/03/17.
 */

@Singleton
public class RemoteConfigManager {
    private final String TAG = LogUtils.makeLogTag(RemoteConfigManager.class.getSimpleName());
    //private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private final Context mContext;

    @Inject
    public RemoteConfigManager(Context context) {
        //    mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mContext = context;
        //initialize();
    }

    //private void initialize() {
    //    logd(TAG, "initialize: ");
    //    FirebaseRemoteConfigSettings configSettings =
    //      new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG).build();
    //    mFirebaseRemoteConfig.setConfigSettings(configSettings);
    //    mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
    //    long cacheExpirationTime = getLong(KEY.KEY_CACHE_EXPIRATION_TIME);
    //    if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
    //        cacheExpirationTime = 0;
    //    }
    //    mFirebaseRemoteConfig.fetch(cacheExpirationTime).addOnCompleteListener(task -> {
    //        if (task.isSuccessful()) {
    //            logd(TAG, "initialize: fetch successful");
    //            mFirebaseRemoteConfig.activateFetched();
    //        } else {
    //            loge(TAG, "FirebaseRemoteConfigFetchException failed");
    //        }
    //    });
    //}
    //
    //public String getString(String key) {
    //    logd(TAG, "getString: ");
    //    return mFirebaseRemoteConfig.getString(key);
    //}
    //
    //public double getDouble(String key) {
    //    logd(TAG, "getDouble: ");
    //    return mFirebaseRemoteConfig.getDouble(key);
    //}
    //
    //public long getLong(String key) {
    //    logd(TAG, "getLong: ");
    //    return mFirebaseRemoteConfig.getLong(key);
    //}
    //
    ///**
    // * MAke sure you have the same key prevent in firebase remote config
    // */
    //interface KEY {
    //    String KEY_DEFAULT_DISCOUNT_PC = "default_discount_pc";
    //    String KEY_CACHE_EXPIRATION_TIME = "config_cache_expiration_time";
    //    String KEY_SSO_REDIRECT_URL = "sso_redirect_url";
    //}
}
