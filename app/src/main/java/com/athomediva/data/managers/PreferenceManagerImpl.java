package com.athomediva.data.managers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.models.remote.Referral;
import com.athomediva.data.sharedpref.AppLifeCycleData;
import com.athomediva.data.sharedpref.Encryption;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.data.sharedpref.SaveData;
import com.athomediva.data.sharedpref.UserData;
import com.athomediva.logger.LogUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import hugo.weaving.DebugLog;
import javax.inject.Inject;
import javax.inject.Singleton;
import www.zapluk.com.BuildConfig;

import static com.athomediva.data.sharedpref.PreferenceManager.SESSION.KEY_ENCRYPTION_ENABLE;
import static com.athomediva.data.sharedpref.PreferenceManager.SESSION.KEY_INSTANCE_ID;
import static com.athomediva.logger.LogUtils.LOGD;

/*
 * Created by mohitkumar on 13/01/17.
 */
@Singleton
public class PreferenceManagerImpl implements PreferenceManager {
    private final Context mContext;
    private final UserData mUserData;
    private final String TAG = LogUtils.makeLogTag(PreferenceManager.class.getSimpleName());

    @Inject
    public PreferenceManagerImpl(Context context) {
        mContext = context;
        mUserData = UserData.getInstance(mContext);
    }

    private String getAppInstanceId() {
        return getStringValue(KEY_INSTANCE_ID);
    }

    @Override
    public void saveInstanceId() {
        Factory.getSessionProfile(mContext, KEY_INSTANCE_ID.getSessionContext())
          .setValue(KEY_INSTANCE_ID.getKeyValue(), FirebaseInstanceId.getInstance().getId());
    }

    @Override
    public String getSessionToken() {
        String value = mUserData.getStringValue(PreferenceManager.SESSION.KEY_TOKEN.getKeyValue());
        LogUtils.LOGD(TAG, "getSessionToken : " + value);

        return getDecryptedString(value);
    }

    @Override
    public void setSessionToken(String token) {
        mUserData.setValue(PreferenceManager.SESSION.KEY_TOKEN.getKeyValue(), getEncryptedString(token));
    }

    @DebugLog
    @Override
    public void setLoggedInUserDetails(@NonNull LoggedInUserDetails details) {
        LOGD(TAG, "saveLoggedInUserDetails: ");

        mUserData.setValue(PreferenceManager.SESSION.KEY_USER_EMAIL.getKeyValue(), getEncryptedString(details.getEmail()));
        mUserData.setValue(PreferenceManager.SESSION.KEY_USER_GENDER.getKeyValue(), getEncryptedString(details.getGender()));
        mUserData.setValue(PreferenceManager.SESSION.KEY_USER_MOBILE_NUMBER.getKeyValue(),
          getEncryptedString(details.getMobile()));
        mUserData.setValue(PreferenceManager.SESSION.KEY_USER_NAME.getKeyValue(), getEncryptedString(details.getName()));
        mUserData.setValue(PreferenceManager.SESSION.KEY_USER_PROFILE_IMG.getKeyValue(),
          getEncryptedString(details.getImage()));
        mUserData.setValue(SESSION.KEY_DATE_OF_BIRTH.getKeyValue(), getEncryptedString(details.getDob()));
        mUserData.setValue(SESSION.KEY_ANNIVERSARY.getKeyValue(), getEncryptedString(details.getAnniversary()));
        if (details.getReferral() != null) {
            mUserData.setValue(PreferenceManager.SESSION.REF_TITLE.getKeyValue(),
              getEncryptedString(details.getReferral().getIfTitle()));
            mUserData.setValue(PreferenceManager.SESSION.REF_DESC.getKeyValue(),
              getEncryptedString(details.getReferral().getIfDesc()));
            mUserData.setValue(PreferenceManager.SESSION.REF_CODE.getKeyValue(),
              getEncryptedString(details.getReferral().getIfReferal()));
            mUserData.setValue(PreferenceManager.SESSION.REF_URL.getKeyValue(),
              getEncryptedString(details.getReferral().getIfRefUrl()));
            mUserData.setValue(PreferenceManager.SESSION.REF_BODY_CONTENT.getKeyValue(),
              getEncryptedString(details.getReferral().getIfContent()));
            mUserData.setValue(PreferenceManager.SESSION.REF_SUBJECT.getKeyValue(),
              getEncryptedString(details.getReferral().getIfSubject()));
        }
    }

    @Override
    public boolean isUserLoggedIn() {
        return !TextUtils.isEmpty(mUserData.getStringValue(PreferenceManager.SESSION.KEY_TOKEN.getKeyValue()));
    }

    @Override
    public void clearUserData() {
        mUserData.clearALL();
        Encryption.getInstance(getAppInstanceId()).clear();
    }

    @Override
    public void setValue(@NonNull PreferenceManager.SESSION key, int value) {
        Factory.getSessionProfile(mContext, key.getSessionContext()).setValue(key.getKeyValue(), value);
    }

    @Override
    public void setValue(@NonNull PreferenceManager.SESSION key, long value) {
        Factory.getSessionProfile(mContext, key.getSessionContext()).setValue(key.getKeyValue(), value);
    }

    @Override
    public void setValue(@NonNull PreferenceManager.SESSION key, String value) {
        Factory.getSessionProfile(mContext, key.getSessionContext()).setValue(key.getKeyValue(), value);
    }

    @Override
    public void setValue(@NonNull SESSION key, boolean value) {
        Factory.getSessionProfile(mContext, key.getSessionContext()).setValue(key.getKeyValue(), value);
    }

    @Override
    public int getIntValue(@NonNull PreferenceManager.SESSION key) {
        return Factory.getSessionProfile(mContext, key.getSessionContext()).getIntValue(key.getKeyValue());
    }

    @Override
    public long getLongValue(@NonNull PreferenceManager.SESSION key) {
        return Factory.getSessionProfile(mContext, key.getSessionContext()).getLongValue(key.getKeyValue());
    }

    @Override
    public String getStringValue(@NonNull PreferenceManager.SESSION key) {
        return Factory.getSessionProfile(mContext, key.getSessionContext()).getStringValue(key.getKeyValue());
    }

    @Override
    public boolean getBooleanValue(@NonNull SESSION key) {
        return Factory.getSessionProfile(mContext, key.getSessionContext()).getBooleanValue(key.getKeyValue());
    }

    @DebugLog
    @Override
    public LoggedInUserDetails getUserDetails() {
        LoggedInUserDetails userDetails = new LoggedInUserDetails();
        userDetails.setMobile(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_MOBILE_NUMBER.getKeyValue())));
        userDetails.setGender(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_GENDER.getKeyValue())));
        userDetails.setEmail(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_EMAIL.getKeyValue())));
        userDetails.setName(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_NAME.getKeyValue())));
        userDetails.setMobile(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_MOBILE_NUMBER.getKeyValue())));
        userDetails.setImage(getDecryptedString(mUserData.getStringValue(SESSION.KEY_USER_PROFILE_IMG.getKeyValue())));
        userDetails.setDob(getDecryptedString(mUserData.getStringValue(SESSION.KEY_DATE_OF_BIRTH.getKeyValue())));
        userDetails.setAnniversary(getDecryptedString(mUserData.getStringValue(SESSION.KEY_ANNIVERSARY.getKeyValue())));

        Referral referral = new Referral();
        referral.setIfTitle(getDecryptedString(mUserData.getStringValue(SESSION.REF_TITLE.getKeyValue())));
        referral.setIfDesc(getDecryptedString(mUserData.getStringValue(SESSION.REF_DESC.getKeyValue())));
        referral.setIfReferal(getDecryptedString(mUserData.getStringValue(SESSION.REF_CODE.getKeyValue())));
        referral.setIfRefUrl(getDecryptedString(mUserData.getStringValue(SESSION.REF_URL.getKeyValue())));
        referral.setIfContent(getDecryptedString(mUserData.getStringValue(SESSION.REF_BODY_CONTENT.getKeyValue())));
        referral.setIfSubject(getDecryptedString(mUserData.getStringValue(SESSION.REF_SUBJECT.getKeyValue())));

        userDetails.setReferral(referral);
        return userDetails;
    }

    static class Factory {
        public static SaveData getSessionProfile(Context context, PreferenceManager.SessionContext sessionContext) {
            if (sessionContext == PreferenceManager.SessionContext.APP_LIFECYCLE_CONTEXT) {
                return AppLifeCycleData.getInstance(context);
            } else if (sessionContext == PreferenceManager.SessionContext.USER_SESSION_CONTEXT) {
                return UserData.getInstance(context);
            }
            return null;
        }
    }

    private String getEncryptedString(String value) {
        if (BuildConfig.ENCRYPTION_ENABLED && getBooleanValue(KEY_ENCRYPTION_ENABLE) && !TextUtils.isEmpty(value)) {
            return getEncryption().encryptOrNull(value);
        }
        return value;
    }

    private String getDecryptedString(String value) {
        if (BuildConfig.ENCRYPTION_ENABLED && getBooleanValue(KEY_ENCRYPTION_ENABLE) && !TextUtils.isEmpty(value)) {
            return getEncryption().decryptOrNull(value);
        }
        return value;
    }

    private Encryption getEncryption() {
        return Encryption.getInstance(getAppInstanceId());
    }
}
