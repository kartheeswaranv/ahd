package com.athomediva.data.managers;

import android.content.Context;
import android.text.TextUtils;
import com.athomediva.data.models.remote.Error;
import com.athomediva.logger.LogUtils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.quikrservices.android.network.Constants;
import hugo.weaving.DebugLog;
import javax.inject.Inject;
import javax.inject.Singleton;
import www.zapluk.com.BuildConfig;
import www.zapluk.com.R;

/**
 * Created by mohit kumar on 13/01/17.
 * The manager is singleton and handles the analytics session
 */
@Singleton
public class AnalyticsManager implements GATrackerContext {
    public static final String TAG = LogUtils.makeLogTag(AnalyticsManager.class.getSimpleName());
    private final Tracker mTracker;
    private final int MAX_CHARACTER = 250;

    @DebugLog
    @Inject
    public AnalyticsManager(Context context) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        mTracker = analytics.newTracker(R.xml.analytics);
        mTracker.setAppName(context.getString(R.string.app_name));
        mTracker.setAppVersion((String.valueOf(BuildConfig.VERSION_CODE)));
        mTracker.setAppId(BuildConfig.APPLICATION_ID);
        if (BuildConfig.DEBUG || BuildConfig.STAGE_FLAVOUR) {
            analytics.setDryRun(true);//When dry run is set, hits will not be dispatched, but will still be logged
        }
    }

    @DebugLog
    @Override
    public void trackEventGA(String category, String action, String label) {
        HitBuilders.EventBuilder builder =
          new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).setCategory(category);
        mTracker.send(builder.build());
    }

    @DebugLog
    @Override
    public void trackScreenEventGA(String screenName, String category, String action) {
        mTracker.setScreenName(screenName);
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder().setCategory(category).setAction(action);
        mTracker.send(builder.build());
    }

    @DebugLog
    @Override
    public void trackScreenEventGA(String screenName, String category, String action, String label) {
        mTracker.setScreenName(screenName);
        HitBuilders.EventBuilder builder =
          new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label);
        mTracker.send(builder.build());
    }

    @DebugLog
    @Override
    public void trackGA(String screenName) {
        mTracker.setScreenName(screenName);
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder();
        mTracker.send(builder.build());
    }

    @DebugLog
    @Override
    public void trackGAEventForAPIFail(String action, long errorCode, String errorMsg) {
        // Label should have 500 bytes , so we get only 250 char
        // Api Error scenario we are sending long description

        if (!TextUtils.isEmpty(errorMsg)) {
            if (errorMsg.length() > MAX_CHARACTER) errorMsg = errorMsg.substring(0, MAX_CHARACTER);
        } else {
            errorMsg = Label.NO_ERROR_MSG;
        }

        if (errorCode == Constants.ERROR_CODES.NO_NETWORK) {
            errorMsg = Label.NO_NETWORK;
        }

        trackEventGA(Category.ERROR_API, action, errorMsg);
    }

    @DebugLog
    @Override
    public void trackGAEventForAPIFail(String action, Error error) {
        if (error != null) {
            trackGAEventForAPIFail(action, error.getCode(), error.getMessage());
        } else {
            trackEventGA(Category.ERROR_API, action, Label.NO_ERROR_MSG);
        }
    }

    @DebugLog
    @Override
    public void trackGAEventForAPIFail(String action, String label) {
        if (!TextUtils.isEmpty(label) && label.length() > MAX_CHARACTER) label = label.substring(0, MAX_CHARACTER);

        trackEventGA(Category.ERROR_API, action, label);
    }
}
