package com.athomediva.data.managers;

import android.content.Context;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.local.AddAddressItem;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Device;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.data.models.local.SelectedTimeSlot;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.local.Utm;
import com.athomediva.data.models.remote.APIAvailConfirmResponse;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.APICreateOrderResponse;
import com.athomediva.data.models.remote.APIDateTimeSlotResponse;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGenerateOtpResponse;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.models.remote.APIGetMetaDataResponse;
import com.athomediva.data.models.remote.APIGetPromotionalAdsResponse;
import com.athomediva.data.models.remote.APIGetWalletStatsResponse;
import com.athomediva.data.models.remote.APIGoogleAutoSuggestResponse;
import com.athomediva.data.models.remote.APIGooglePlaceByIDResponse;
import com.athomediva.data.models.remote.APIImageUploadResponse;
import com.athomediva.data.models.remote.APIInvitesResponse;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.models.remote.APIOldResponse;
import com.athomediva.data.models.remote.APISignInResponse;
import com.athomediva.data.models.remote.APISignUpResponse;
import com.athomediva.data.models.remote.APIVerifyOTPResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.BookingDetails;
import com.athomediva.data.models.remote.ConfirmBookingResponse;
import com.athomediva.data.models.remote.DeeplinkResponse;
import com.athomediva.data.models.remote.InviteRequest;
import com.athomediva.data.models.remote.MyBookingResponse;
import com.athomediva.data.models.remote.location.BoundsResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.network.contracts.HttpConstants;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.DataHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UTMUtils;
import com.athomediva.utils.UiUtils;
import com.quikrservices.android.network.Constants;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.json.JSONObject;
import www.zapluk.com.BuildConfig;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohit kumar on 13/01/17.
 * The Class Exposes all the apis
 */
@Singleton
public class APIManager {
    private static final String TAG = LogUtils.makeLogTag(APIManager.class.getSimpleName());
    private final Context mContext;

    @Inject
    public APIManager(Context context) {
        mContext = context;
    }

    public static QuikrNetworkRequest<DeeplinkResponse> getDeeplinkDatas(String url, String lat, String lng,
      QuikrNetworkRequest.Callback<DeeplinkResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.CATEGORIES_PARAMS.PARAM_LATITUDE, lat);
        params.put(HttpConstants.CATEGORIES_PARAMS.PARAM_LANGITUDE, lng);
        params.put(HttpConstants.DEEPLINK_PARAMS.PARAM_URL, url);
        QuikrNetworkRequest getDeeplinkDataReq =
          new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_GET_DEEPLINK_DATAS,
            DeeplinkResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
            BuildConfig.QDP_ENABLE);

        return getDeeplinkDataReq;
    }

    public static QuikrNetworkRequest<APIGenericResponse> submitRating(float rating, String comments,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.APP_FEEDBACK_PARAMS.PARAM_COMMENT, comments);
        params.put(HttpConstants.APP_FEEDBACK_PARAMS.PARAM_RATING, rating);

        QuikrNetworkRequest submitRatingReq =
          new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_APP_REVIEW_SUBMIT,
            APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
            BuildConfig.QDP_ENABLE);

        return submitRatingReq;
    }

    public static QuikrNetworkRequest<APIGenericResponse> updateSpecialDates(ArrayList<SpecialOfferDateModel> list,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        DataHelper.updateSpecialDateParams(list, params);
        QuikrNetworkRequest updateSpecialDatesReq =
          new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_SPECIAL_DAYS,
            APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
            BuildConfig.QDP_ENABLE);

        return updateSpecialDatesReq;
    }

    public QuikrNetworkRequest<APISignUpResponse> signUp(Consumer consumer, ArrayList<SpecialOfferDateModel> list,
      QuikrNetworkRequest.Callback<APISignUpResponse> responseCallback) {
        LOGD(TAG, " signUp()");
        JSONObject jsonData = new JSONObject();
        try {
            DataHelper.updateSpecialDateParams(list, jsonData);
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_NAME, consumer.getUserName());
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_EMAILID, consumer.getEmailID());
            jsonData.put(HttpConstants.SIGN_UP.PARAM_GENDER, consumer.getGender());
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_MOBILE, consumer.getUserNumber());
            jsonData.put(HttpConstants.SIGN_UP.PARAM_REF_CODE, consumer.getRefcode());
            jsonData.put(HttpConstants.SIGN_UP.PARAM_DEVICE_DETAILS, generateDeviceDetails(consumer.getDevice()));
        } catch (Exception e) {
            LOGE(TAG, "signUp: error while forming request body trace =" + e.getLocalizedMessage());
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.BODY, String.valueOf(jsonData));
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_USER_SIGNUP,
          APISignUpResponse.class, null, params, responseCallback, null, BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APISignInResponse> signIn(Consumer consumer,
      QuikrNetworkRequest.Callback<APISignInResponse> responseCallback) {
        LOGD(TAG, " signIn()");
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_EMAILID, consumer.getEmailID());
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_MOBILE, consumer.getUserNumber());
            jsonData.put(HttpConstants.SIGN_UP.PARAM_DEVICE_DETAILS, generateDeviceDetails(consumer.getDevice()));
        } catch (Exception e) {
            LOGE(TAG, "signIn: exception thrown " + e.getLocalizedMessage());
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.BODY, String.valueOf(jsonData));
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_USER_SIGN_IN,
          APISignInResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    private JSONObject generateDeviceDetails(Device device) throws Exception {
        JSONObject jsonData = new JSONObject();
        jsonData.put(HttpConstants.BASE_DETAILS.PARAM_GCM_REGID, device.getGcmId());
        jsonData.put(HttpConstants.BASE_DETAILS.PARAM_OS_VERSION, device.getVersionCode());
        jsonData.put(HttpConstants.BASE_DETAILS.PARAM_MODEL_NAME, device.getDeviceName());
        jsonData.put(HttpConstants.BASE_DETAILS.PARAM_DEVICE_RESOLUTION, device.getScreenDimension());
        jsonData.put(HttpConstants.BASE_DETAILS.PARAM_DEVICE_UID, device.getDeviceUid());
        return jsonData;
    }

    public QuikrNetworkRequest<APIGenerateOtpResponse> genOTP(Consumer consumer, boolean updateMobile,
      QuikrNetworkRequest.Callback<APIGenerateOtpResponse> responseCallback) {
        LOGD(TAG, " genOTP()");
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_EMAILID, consumer.getEmailID());
            jsonData.put(HttpConstants.BASE_DETAILS.PARAM_MOBILE, consumer.getUserNumber());

            if (updateMobile) {
                jsonData.put(HttpConstants.CART_PARAMS.PARAM_ACTION, AppConstants.UPDATE_MOBILE);
            }
        } catch (Exception e) {
            LOGE(TAG, "genOTP: exception " + e.getLocalizedMessage());
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.BODY, String.valueOf(jsonData));
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_GEN_OTP,
          APIGenerateOtpResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIVerifyOTPResponse> verifyOTP(Consumer consumer, String otp, boolean updateMobile,
      QuikrNetworkRequest.Callback<APIVerifyOTPResponse> responseCallback) {
        LOGD(TAG, " verifyOTP()");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.VERIFY_OTP.PARAM_OTP, otp);
        params.put(HttpConstants.BASE_DETAILS.PARAM_EMAILID, consumer.getEmailID());
        params.put(HttpConstants.BASE_DETAILS.PARAM_MOBILE, consumer.getUserNumber());
        if (updateMobile) {
            params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, AppConstants.UPDATE_MOBILE);
        }
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_VERIFY_OTP,
          APIVerifyOTPResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIFullUserDetailsResponse> getUserDetails(
      QuikrNetworkRequest.Callback<APIFullUserDetailsResponse> responseCallback) {
        LOGD(TAG, "getUserDetails: " + BuildConfig.QDP_ENABLE);
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_IMAGE_DENSITY, UiUtils.getDensityKey(mContext));
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_USER_DETAILS,
          APIFullUserDetailsResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGenericResponse> logoutUser(
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "logoutUser: ");
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_USER_LOGOUT,
          APIGenericResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGetWalletStatsResponse> getMyWalletDetails(
      QuikrNetworkRequest.Callback<APIGetWalletStatsResponse> responseCallback) {
        LOGD(TAG, "getMyWalletDetails : ");
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_MY_WALLET,
          APIGetWalletStatsResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGetPromotionalAdsResponse> getCouponDetails(boolean bookingSpecific,
      QuikrNetworkRequest.Callback<APIGetPromotionalAdsResponse> responseCallback) {
        LOGD(TAG, "getCouponDetails : ");
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.GET_PROMOTIONAL_OFFERS,
          APIGetPromotionalAdsResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGoogleAutoSuggestResponse> getSuggestedPlaces(String query,
      QuikrNetworkRequest.Callback<APIGoogleAutoSuggestResponse> responseCallback) {
        LOGD(TAG, "getSuggestedPlaces : ");
        HashMap<String, String> params = new HashMap<>();
        //params.put(HttpConstants.GOOGLE_SUGGESTED_PLACES_PARAMS.PARAM_TYPES, "geocode");
        params.put(HttpConstants.GOOGLE_SUGGESTED_PLACES_PARAMS.PARAM_KEY, BuildConfig.GOOGLE_AUTOSUGGEST_API_KEY);
        params.put(HttpConstants.GOOGLE_SUGGESTED_PLACES_PARAMS.PARAM_INPUT, query);
        params.put(HttpConstants.GOOGLE_SUGGESTED_PLACES_PARAMS.PARAM_COMPONENTS, "country:in");
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GOOGLE_AUTOSUGGEST_PLACE_API,
          APIGoogleAutoSuggestResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON, false);
    }

    public QuikrNetworkRequest<APIGooglePlaceByIDResponse> getPlaceByID(String placeID,
      QuikrNetworkRequest.Callback<APIGooglePlaceByIDResponse> responseCallback) {
        LOGD(TAG, "getPlaceByID : ");
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpConstants.GOOGLE_PLACES_PARAMS.PARAM_PLACE_ID, placeID);
        params.put(HttpConstants.GOOGLE_PLACES_PARAMS.PARAM_KEY, BuildConfig.GOOGLE_AUTOSUGGEST_API_KEY);
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GOOGLE_PLACE_BY_ID_API,
          APIGooglePlaceByIDResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON, false);
    }

    public QuikrNetworkRequest<BoundsResponse> getBoundsByCircle(String circleName,
      QuikrNetworkRequest.Callback<BoundsResponse> responseCallback) {
        LOGD(TAG, "getBoundsByCircle : ");
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpConstants.ADDRESS_PARAMS.PARAM_CIRCLE_NAME, circleName);
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.GET_BOUNDS, BoundsResponse.class,
          null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON, BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIListCategoriesResponse> getAllCategories(double latitude, double longitude,
      boolean getForCurrentLatLong, QuikrNetworkRequest.Callback<APIListCategoriesResponse> responseCallback) {
        LOGD(TAG, "getAllCategories : ");
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpConstants.CATEGORIES.PARAM_LAT, String.valueOf(latitude));
        params.put(HttpConstants.CATEGORIES.PARAM_LONG, String.valueOf(longitude));
        params.put(HttpConstants.CATEGORIES.PARAM_USE_MY_LOCATION, String.valueOf(getForCurrentLatLong));
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_CATEGORIES,
          APIListCategoriesResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGetMetaDataResponse> getAppMetaData(
      QuikrNetworkRequest.Callback<APIGetMetaDataResponse> responseCallback) {
        LOGD(TAG, "getAppMetaData: ");
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_APP_METADATA,
          APIGetMetaDataResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGenericResponse> addAddress(AddAddressItem item,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "addAddress: ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.ADD_ADDRESS.PARAM_LAT, item.getFullAddressEntity().getData().getLatLng().latitude);
        params.put(HttpConstants.ADD_ADDRESS.PARAM_LONG, item.getFullAddressEntity().getData().getLatLng().longitude);
        if (mContext.getResources()
          .getString(R.string.address_custom)
          .equalsIgnoreCase(item.getAddressTypeEntity().getData())) {
            params.put(HttpConstants.ADD_ADDRESS.PARAM_LABEL, item.getCustomAddressEntity().getData());
        } else {
            params.put(HttpConstants.ADD_ADDRESS.PARAM_LABEL, item.getAddressTypeEntity().getData());
        }
        params.put(HttpConstants.ADD_ADDRESS.PARAM_HOUSE_NAME, item.getBuildingEntity().getData());
        params.put(HttpConstants.ADD_ADDRESS.PARAM_ADDRESS, item.getFullAddressEntity().getData().getAddress());
        params.put(HttpConstants.ADD_ADDRESS.PARAM_LANDMARK, item.getLandmarkEntity().getData());
        if (!TextUtils.isEmpty(item.getFullAddressEntity().getData().getPinCode())) {
            params.put(HttpConstants.ADD_ADDRESS.PARAM_PINCODE, item.getFullAddressEntity().getData().getPinCode());
        }
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_ADD_ADDRESS,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGenericResponse> deleteAddress(Address item,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "deleteAddress: ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.DELETE_ADDRESS.PARAM_DELETE_ADDRESS, item.getAddressID());
        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_DELETE_ADDRESS,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGenericResponse> updateProfile(Consumer item, ArrayList<SpecialOfferDateModel> list,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "updateProfile: ");
        HashMap<String, Object> params = new HashMap<>();
        DataHelper.updateSpecialDateParams(list, params);
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_USERNAME, item.getUserName());
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_MOBILE_NO, item.getUserNumber());
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_EMAIL, item.getEmailID());
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_GENDER, item.getGender());

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_PROFILE,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIImageUploadResponse> updateProfilePicture(String imageName,
      QuikrNetworkRequest.Callback<APIImageUploadResponse> responseCallback) {
        LOGD(TAG, "updateProfilePicture: ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_IMAGE_DENSITY, UiUtils.getDensityKey(mContext));
        params.put(HttpConstants.UPDATE_PROFILE_PARAMS.PARAM_IAMGE, imageName);

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_PROFILE_PICTURE,
          APIImageUploadResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIGenericResponse> submtiFeedback(String bookingId, String comments, List<RatingsModel> list,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "submtiFeedback: ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.ORDER_PARAMS.PARAM_ORDER_ID, bookingId);
        params.put(HttpConstants.FEEDBACK_PARAMS.PARAM_COMMENTS, comments);
        if (list != null && list.size() > 0) {
            for (RatingsModel model : list) {
                params.put(model.getRatingKey(), model.getRating());
            }
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_FEEDBACK_SUBMIT,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICartResponse> createCart(String serviceId, String circle, Utm utm,
      QuikrNetworkRequest.Callback<APICartResponse> responseCallback) {
        LOGD(TAG, "createCart: ");
        HashMap<String, Object> params = createCartParams(serviceId, circle, utm);

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_CREATE_CART,
          APICartResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICartResponse> createCart(String serviceId, String circle, String lat, String lng, Utm utm,
      QuikrNetworkRequest.Callback<APICartResponse> responseCallback) {
        LOGD(TAG, "createCart: ");
        HashMap<String, Object> params = createCartParams(serviceId, circle, lat, lng, utm);

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_CREATE_CART,
          APICartResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICartResponse> updateCartServices(String action, String cartId, String serviceId,
      QuikrNetworkRequest.Callback<APICartResponse> responseCallback) {
        LOGD(TAG, "updateCartServices: ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
        params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
        params.put(HttpConstants.CART_PARAMS.PARAM_SERVICE_ID, serviceId);

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_CART,
          APICartResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICartResponse> updateCart(BookingRequestData data, Utm utm,
      QuikrNetworkRequest.Callback<APICartResponse> responseCallback) {
        LOGD(TAG, "updateCart: ");

        if (TextUtils.equals(data.getAction(), AppConstants.CART_ACTION.CREATE_CART.getAction())
          && data.getServiceId() > 0) {
            LogUtils.LOGD(TAG, "updateCart : Redirect to Create Cart api");
            return createCart(String.valueOf(data.getServiceId()), data.getCircle(), data.getLatitude(), data.getLongitude(),
              utm, responseCallback);
        }
        HashMap<String, Object> params = getUpdateCartParams(data);

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_CART,
          APICartResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIDateTimeSlotResponse> getTimeSlots(String cartId, String bookingId,
      QuikrNetworkRequest.Callback<APIDateTimeSlotResponse> responseCallback) {
        LOGD(TAG, "getTimeSlots: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(cartId) && !TextUtils.isEmpty(bookingId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
            params.put(HttpConstants.CART_PARAMS.PARAM_BOOKING_ID, bookingId);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_TIMESLOTS,
          APIDateTimeSlotResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICreateOrderResponse> createOrder(String cartId, Utm utm,
      QuikrNetworkRequest.Callback<APICreateOrderResponse> responseCallback) {
        LOGD(TAG, "createOrder: ");
        HashMap<String, Object> params = UTMUtils.getUTMParams(utm);
        if (!TextUtils.isEmpty(cartId)) {
            params.put(HttpConstants.BASE_DETAILS.PARAM_GCM_REGID, AndroidHelper.getGcmId());
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_CREATE_ORDER,
          APICreateOrderResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<MyBookingResponse> getMyBookings(
      QuikrNetworkRequest.Callback<MyBookingResponse> responseCallback) {
        LOGD(TAG, "createOrder: ");

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_BOOKINGS,
          MyBookingResponse.class, null, null, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<BookingDetails> getBookingDetails(String orderId,
      QuikrNetworkRequest.Callback<BookingDetails> responseCallback) {
        LOGD(TAG, "getBookingDetails: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(orderId)) {
            params.put(HttpConstants.ORDER_PARAMS.PARAM_ORDER_ID, orderId);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_BOOKING_DETAILS,
          BookingDetails.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIOldResponse> updateBookingMessage(String orderId, String message,
      QuikrNetworkRequest.Callback<APIOldResponse> responseCallback) {
        LOGD(TAG, "getBookingDetails: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(orderId) && !TextUtils.isEmpty(message)) {
            params.put(HttpConstants.ORDER_PARAMS.PARAM_ORDER_ID, orderId);
            params.put(HttpConstants.ORDER_PARAMS.PARAM_MESSAGE, message);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_BOOKING_MSG,
          APIOldResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    /*public QuikrNetworkRequest<APIGenericResponse> cancelBooking(String orderId, String reason,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "cancelBooking: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(orderId) && !TextUtils.isEmpty(reason)) {
            params.put(HttpConstants.ORDER_PARAMS.PARAM_ORDER_ID, orderId);
            params.put(HttpConstants.ORDER_PARAMS.PARAM_CANCEL_REASON, reason);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_CANCEL_BOOKING,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }*/

    public QuikrNetworkRequest<APIGenericResponse> cancelBooking(String orderId, String reason,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "cancelBooking: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(orderId)) {
            params.put(HttpConstants.ORDER_PARAMS.PARAM_ORDER_ID, orderId);
        }

        if (!TextUtils.isEmpty(reason)) {
            params.put(HttpConstants.ORDER_PARAMS.PARAM_CANCEL_REASON, reason);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_CANCEL_BOOKING,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<ConfirmBookingResponse> confirmBooking(String cartId,
      QuikrNetworkRequest.Callback<ConfirmBookingResponse> responseCallback) {
        LOGD(TAG, "cancelBooking: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(cartId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_CONFIRM_BOOKING,
          ConfirmBookingResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APICartResponse> getCartDetails(String cartId,
      QuikrNetworkRequest.Callback<APICartResponse> responseCallback) {
        LOGD(TAG, "getCartDetails: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(cartId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_GET_CART_DETAILS,
          APICartResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    private HashMap<String, Object> getUpdateCartParams(BookingRequestData data) {
        if (data != null && data.getAction() != null) {
            if (data.getAction().equals(AppConstants.CART_ACTION.UPDATE_ADDRESS.getAction())) {
                return updateAddressParams(data.getAction(), data.getCartId(), data.getAddressId(),
                  data.isBookingResNeeded());
            } else if (data.getAction().equals(AppConstants.CART_ACTION.ADD_SERVICE.getAction()) || data.getAction()
              .equals(AppConstants.CART_ACTION.DELETE_SERVICE.getAction())) {
                return updateCartServices(data.getAction(), String.valueOf(data.getServiceId()), data.getCartId());
            } else if (data.getAction().equals(AppConstants.CART_ACTION.UPDATE_TIME.getAction())) {
                return updateDateTimeParams(data.getAction(), data.getCartId(), data.getBookingId(), data.getScheduleTime());
            } else if (data.getAction().equals(AppConstants.CART_ACTION.UPDATE_COUPON.getAction())) {
                return updateCouponParams(data.getAction(), data.getCartId(), data.getCouponCode());
            } else if (data.getAction().equals(AppConstants.CART_ACTION.REMOVE_COUPON.getAction())) {
                return removeCouponParams(data.getAction(), data.getCartId());
            }
        } else {
            LogUtils.LOGD(TAG, "getUpdateCartParams : Action is empty");
        }

        return null;
    }

    private HashMap<String, Object> removeCouponParams(String action, String cartId) {
        LogUtils.LOGD(TAG, "updateCouponParams : ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(action) && !TextUtils.isEmpty(cartId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
            params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
        }

        return params;
    }

    private HashMap<String, Object> updateCouponParams(String action, String cartId, String coupon) {
        LogUtils.LOGD(TAG, "updateCouponParams : ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(action) && !TextUtils.isEmpty(cartId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
            params.put(HttpConstants.CART_PARAMS.PARAM_COUPON, coupon);
            params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
        }

        return params;
    }

    private HashMap<String, Object> updateAddressParams(String action, String cartId, String addressId,
      boolean isBookingResponse) {
        LogUtils.LOGD(TAG, "updateAddressParams : ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(action) && !TextUtils.isEmpty(addressId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
            params.put(HttpConstants.CART_PARAMS.PARAM_ADDRESS_ID, addressId);
            params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
            params.put(HttpConstants.CART_PARAMS.PARAM_BOOKING_RESPONSE, isBookingResponse);
        }

        return params;
    }

    private HashMap<String, Object> updateDateTimeParams(String action, String cartId, String bookingId,
      SelectedTimeSlot slot) {
        LogUtils.LOGD(TAG, "updateAddressParams : ");
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
        params.put(HttpConstants.CART_PARAMS.PARAM_BOOKING_ID, bookingId);
        params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
        params.put(HttpConstants.CART_PARAMS.PARAM_DATE, slot.getDate());
        params.put(HttpConstants.CART_PARAMS.PARAM_FROM, slot.getFrom());
        params.put(HttpConstants.CART_PARAMS.PARAM_TO, slot.getTo());
        params.put(HttpConstants.CART_PARAMS.PARAM_SLOT, slot.getSlot());
        params.put(HttpConstants.CART_PARAMS.PARAM_CIRCLE, slot.getCircle());

        return params;
    }

    private HashMap<String, Object> createCartParams(String serviceId, String circle, String lat, String lng, Utm utm) {
        LogUtils.LOGD(TAG, "createCartParams : ");
        HashMap<String, Object> params = UTMUtils.getUTMParams(utm);

        if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(serviceId) && !TextUtils.isEmpty(lng)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CIRCLE_NAME, circle);
            params.put(HttpConstants.CART_PARAMS.PARAM_LATITUDE, lat);
            params.put(HttpConstants.CART_PARAMS.PARAM_LONGITUDE, lng);
            params.put(HttpConstants.BASE_DETAILS.PARAM_GCM_REGID, AndroidHelper.getGcmId());
            params.put(HttpConstants.CART_PARAMS.PARAM_SERVICE_ID, serviceId);
        }

        return params;
    }

    private HashMap<String, Object> createCartParams(String serviceId, String circle, Utm utm) {
        LogUtils.LOGD(TAG, "createCartParams : ");
        HashMap<String, Object> params = UTMUtils.getUTMParams(utm);

        if (!TextUtils.isEmpty(circle) && !TextUtils.isEmpty(serviceId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_CIRCLE_NAME, circle);
            params.put(HttpConstants.BASE_DETAILS.PARAM_GCM_REGID, AndroidHelper.getGcmId());
            params.put(HttpConstants.CART_PARAMS.PARAM_SERVICE_ID, serviceId);
        }

        return params;
    }

    private HashMap<String, Object> updateCartServices(String action, String serviceId, String cartId) {
        LogUtils.LOGD(TAG, "createCartParams : ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(action) && !TextUtils.isEmpty(cartId) && !TextUtils.isEmpty(serviceId)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_ACTION, action);
            params.put(HttpConstants.CART_PARAMS.PARAM_CART_ID, cartId);
            params.put(HttpConstants.CART_PARAMS.PARAM_SERVICE_ID, serviceId);
        }
        return params;
    }

    public QuikrNetworkRequest<APIInvitesResponse> getInvitedList(String phonenos,
      QuikrNetworkRequest.Callback<APIInvitesResponse> responseCallback) {
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpConstants.INVITE_PARAMS.PARAM_CONTACT_LIST, phonenos);
        QuikrNetworkRequest inviteReq =
          new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_GET_INVITE_LIST,
            APIInvitesResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
            BuildConfig.QDP_ENABLE);

        return inviteReq;
    }

    public QuikrNetworkRequest<APIInvitesResponse> updateInviteList(ArrayList<InviteRequest> contacts,
      QuikrNetworkRequest.Callback<APIInvitesResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(HttpConstants.INVITE_PARAMS.PARAM_INVITE_LIST, contacts);
        QuikrNetworkRequest inviteReq =
          new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_UPDATE_INVITE_LIST,
            APIInvitesResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
            BuildConfig.QDP_ENABLE);

        return inviteReq;
    }

    public QuikrNetworkRequest<APIGenericResponse> checkServiceAvailability(String latitude, String longitude,
      QuikrNetworkRequest.Callback<APIGenericResponse> responseCallback) {
        LOGD(TAG, "cancelBooking: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
            params.put(HttpConstants.CART_PARAMS.PARAM_LATITUDE, latitude);
            params.put(HttpConstants.CART_PARAMS.PARAM_LONGITUDE, longitude);
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.POST, AppUrls.URL_GET_SERVICE_AVAILABILITY,
          APIGenericResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }

    public QuikrNetworkRequest<APIAvailConfirmResponse> availabilityConfirm(String orderId,
      QuikrNetworkRequest.Callback<APIAvailConfirmResponse> responseCallback) {
        LOGD(TAG, "availabilityConfirm: ");
        HashMap<String, Object> params = new HashMap<>();
        if (!TextUtils.isEmpty(orderId)) {
            params.put(HttpConstants.AVAILABILITY_PARAMS.PARAM_IDENTIFIER, orderId);
        } else {
            LogUtils.LOGD(TAG, "availabilityConfirm Req : Order Id is empty");
        }

        return new QuikrNetworkRequest<>(QuikrNetworkRequest.RequestMethod.GET, AppUrls.URL_USER_AVAIL_CONFIRM,
          APIAvailConfirmResponse.class, null, params, responseCallback, QuikrNetworkRequest.ContentType.JSON,
          BuildConfig.QDP_ENABLE);
    }
}
