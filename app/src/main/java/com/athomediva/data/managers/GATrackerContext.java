package com.athomediva.data.managers;

import com.athomediva.data.models.remote.Error;

/**
 * Created by kartheeswaran on 05/12/16.
 */

public interface GATrackerContext {
    void trackEventGA(String category, String action, String label);

    void trackGA(String gaCodeString);

    void trackScreenEventGA(String screenName, String category, String action);

    void trackScreenEventGA(String screenName, String category, String action, String label);

    void trackGAEventForAPIFail(String action, long errorCode, String errorMsg);

    void trackGAEventForAPIFail(String action, Error error);

    void trackGAEventForAPIFail(String action, String label);

    /**
     * Generic Screen names for each Activity/Screen
     */
    interface ScreenName {
        String APP_LAUNCH = "App launch";
        String LOGIN = "Login";
        String SIGNUP = "SignUp";
        String HOME_PAGE = "HomePage";
        String CATEGORY = "Category";
        String CART = "Cart";
        String SCHEDULING = "Scheduling";
        String WALLET = "Wallet";
        String COUPON_SCREEN = "Coupon Screen";
        String ADDRESS = "Address";
        String ADDRESS_MAP = "AddressMap";
        String SAVE_ADDRESS = "Save Address";
        String STYLIST_UNAVAILABLE = "Slylist unavailable";
        String BOOKINGS_CONFIRMED = "Bookings confirmed";
        String MY_UPCOMING_BOOKINGS = "My Bookings Upcoming";
        String MY_PAST_BOOKINGS = "My Bookings Past";
        String ACCOUNT = "Account";
        String PROFILE = "profile";
        String REFERAL = "Referal";
    }

    /**
     * Category
     */
    interface Category {

        String ATHOMEDIVA = "ahd";
        String ERROR_API = "ERROR_TYPE:API";
        String BOOKINGS = "BOOKINGS";
    }

    /**
     * Generic actions
     */
    interface Action {
        String LOGIN = "ahd_login";
        String SIGNUP = "ahd_signup";
        String HOME_PAGE = "ahd_hp";
        String CATEGORY = "ahd_category";
        String CART = "ahd_cart";
        String ADDRESS = "ahd_address";
        String ADDRESS_MAP = "ahd_addressmap";
        String STYLIST_UNAVAILABLE = "ahd_slylistunavailable";
        String BOOKING_CONFIRMED = "ahd_bookingconfirmed";
        String BOOKING_UPCOMING = "ahd_mybooking_upcoming";
        String BOOKING_PAST = "ahd_mybooking_past";
        String ACCOUNT = "ahd_account";
        String EDIT_PROFILE = "ahd_edit_profile";
        String WALLET = "ahd_wallet";
        String REFERAL = "ahd_referal";
        String CANCELLATION_POLICY = "ahd_cancellationpolicy";
        String TOS = "ahd_terms_conditions";
        String RATE_APP = "ahd_rateapp";
        String REVIEW_ORDER = "ahd_revieworder";

        String CONFIRM_BOOKING_IDS = "confirm_booking_ids";
        String CANCEL_BOOKING_ID = "cancel_booking_id";
    }

    interface Label {
        String SESSION_INIT = "ahd_session_initiate";
        String SESSION_END = "ahd_session_end";
        String LOGIN_PAGE_LOAD = "ahd_login_pageload";
        String SIGNUP_PAGE_LOAD = "ahd_signup_pageload";
        String HOME_PAGE_LOAD = "ahd_hp_pageload";
        String CATEGORY_PAGE_LOAD = "ahd_category_pageload";
        String CART_PAGE_LOAD = "ahd_cart_pageload";
        String ADDRESS_PAGE_LOAD = "ahd_address_pageload";
        String ADD_ADDRESS_PAGE_LOAD = "ahd_addaddress_pageload";
        String STYLIST_UNAVAIL_EVENT = "ahd_stlylistunavailable_event";
        String BOOKING_CONFIRM_PAGELOAD = "ahd_bookingconfirmed_pageload";
        String MYBOOKING_UPCOMING_PAGELOAD = "ahd_mybooking_upcoming_pageload";
        String MYBOOOKING_PAST_PAGELOAD = "ahd_mybooking_past_pageload";
        String ACCOUNT_PAGE_LOAD = "ahd_account_pageload";
        String EDIT_PROFILE_PAGE_LOAD = "ahd_edit_profile_pageload";
        String WALLET_PAGE_LOAD = "ahd_wallet_pageload";
        String REFERAL_PAGE_LOAD = "ahd_referal_pageload";
        String CANCELLATION_POLICY_PAGELOAD = "ahd_cancellationpolicy_pageload";
        String TOS_PAGELOAD = "ahd_terms_conditions_pageload";
        String RATE_APP_EVENT = "ahd_rateapp_event";
        String REVIEW_ORDER_PAGELOAD = "ahd_revieworder_pageload";

        String RESPONSE_IS_EMPTY = "Response is empty";
        String NO_ERROR_MSG = "No Error Message";
        String NO_NETWORK = "No Network";
        String META_DATA_IS_EMPTY = "Meta Data is empty";
    }
}
