package com.athomediva.data.managers;

import com.athomediva.data.CachedStorage;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.DataProcessor;
import com.athomediva.data.sharedpref.PreferenceManager;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by mohit kumar on 18/01/17.
 * Single data which provides all the corresponding managers.
 */

@Singleton
public class DataManager {

    private final EventBusManager mBusManager;
    private final APIManager mApiManager;
    private final AnalyticsManager mAnalyticsManager;
    private final PreferenceManager mPrefManager;
    private final CachedStorage mCacheStorage;
    private final CoreLogic mBookingValidator;
    private final DataProcessor mDataProcessor;
    private final RemoteConfigManager mRemoteConfigManager;

    @Inject
    public DataManager(EventBusManager eventBusManager, APIManager apiManager, AnalyticsManager analyticsManager,
      PreferenceManager preferenceManager, CachedStorage cachedStorage, CoreLogic bookingValidator,
      DataProcessor dataProcessor, RemoteConfigManager remoteConfigManager) {
        mBusManager = eventBusManager;
        mApiManager = apiManager;
        mAnalyticsManager = analyticsManager;
        mPrefManager = preferenceManager;
        mCacheStorage = cachedStorage;
        mBookingValidator = bookingValidator;
        mDataProcessor = dataProcessor;
        mRemoteConfigManager = remoteConfigManager;
    }

    public PreferenceManager getPrefManager() {
        return mPrefManager;
    }

    public EventBusManager getBusManager() {
        return mBusManager;
    }

    public APIManager getApiManager() {
        return mApiManager;
    }

    public AnalyticsManager getAnalyticsManager() {
        return mAnalyticsManager;
    }

    public CachedStorage getCacheStorage() {
        return mCacheStorage;
    }

    public CoreLogic getBookingValidator() {
        return mBookingValidator;
    }

    public DataProcessor getDataProcessor() {
        return mDataProcessor;
    }

    public RemoteConfigManager getRemoteConfigManager() {
        return mRemoteConfigManager;
    }
}
