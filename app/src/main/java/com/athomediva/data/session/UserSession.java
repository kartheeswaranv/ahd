package com.athomediva.data.session;

import android.content.Context;
import android.support.annotation.NonNull;
import com.athomediva.data.busevents.UserLogoutEvent;
import com.athomediva.data.managers.EventBusManager;
import com.athomediva.data.models.local.Device;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.injection.ApplicationContext;
import com.athomediva.logger.LogUtils;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

@Singleton
public class UserSession {
    private static final String TAG = LogUtils.makeLogTag(UserSession.class.getSimpleName());
    private final Context mContext;
    private final PreferenceManager mPreferenceManager;
    private final EventBusManager mEventBusManager;
    private Device mDevice;
    private List<Address> mUserAddressList;
    private boolean isNoAddressAddedForUser = false;

    @Inject
    public UserSession(@NonNull @ApplicationContext Context context, @NonNull PreferenceManager preferenceManager,
      @NonNull EventBusManager busManager) {
        LOGD(TAG, "UserSession: constructor called");
        mContext = context;
        mPreferenceManager = preferenceManager;
        mEventBusManager = busManager;
    }

    public void saveLoggedInUserDetails(LoggedInUserDetails loggedInUserDetails) {
        mPreferenceManager.setLoggedInUserDetails(loggedInUserDetails);
    }

    public String getUserToken() {
        return mPreferenceManager.getSessionToken();
    }

    public void setUserToken(String userToken) {
        mPreferenceManager.setSessionToken(userToken);
    }

    public LoggedInUserDetails getUser() {
        return mPreferenceManager.getUserDetails();
    }

    public Device getDevice() {
        if (mDevice == null) {
            mDevice = new Device();
            mDevice.setVersionCode(AndroidHelper.GetVersionCode());
            mDevice.setScreenDimension(AndroidHelper.getScreenDimensions(mContext));
            mDevice.setDeviceName(AndroidHelper.getDeviceName());
            mDevice.setDeviceUid(AndroidHelper.getMID(mContext));
        }

        return mDevice;
    }

    public void logout() {
        ToastSingleton.getInstance().showToast(mContext.getString(R.string.you_have_been_logged_out));
        mPreferenceManager.clearUserData();
        mEventBusManager.post(new UserLogoutEvent());
        if (mUserAddressList != null) mUserAddressList.clear();
        setUserToken(null);
    }

    public void updateDataFromResponse(APIFullUserDetailsResponse apiFullUserDetailsResponse) {
        if (apiFullUserDetailsResponse == null) return;
        if (apiFullUserDetailsResponse.getSuccess() && apiFullUserDetailsResponse.getData() != null) {
            saveLoggedInUserDetails(apiFullUserDetailsResponse.getData().getUserDetails());
            mUserAddressList = apiFullUserDetailsResponse.getData().getAddress();
            isNoAddressAddedForUser = (apiFullUserDetailsResponse.getData().getAddress() == null
              || apiFullUserDetailsResponse.getData().getAddress().size() == 0);
        } else {
            ToastSingleton.getInstance()
              .showToast(apiFullUserDetailsResponse.getError() == null ? mContext.getString(R.string.try_again)
                : apiFullUserDetailsResponse.getError().getMessage());
        }
    }

    public List<Address> getAddressList() {
        return mUserAddressList;
    }

    public boolean isNoAddressAddedForUser() {
        return isNoAddressAddedForUser;
    }
}
