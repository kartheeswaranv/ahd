package com.athomediva.data.session;

import android.content.Context;
import android.text.TextUtils;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.busevents.CartUpdateEvent;
import com.athomediva.data.busevents.SpecialDateUpdateEvent;
import com.athomediva.data.managers.EventBusManager;
import com.athomediva.data.models.local.Cart;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.local.Utm;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.APIGetMetaDataResponse;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.models.remote.MetaInfoCategories;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.logger.LogUtils;
import com.google.android.gms.maps.model.LatLng;
import javax.inject.Inject;
import javax.inject.Singleton;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */
@Singleton
public class BookingSession {
    private final String TAG = LogUtils.makeLogTag(BookingSession.class.getSimpleName());
    private Context mContext;
    private Cart mCart;
    private APIListCategoriesResponse mCategoryData;
    private APIGetMetaDataResponse mMetaData;
    private EventBusManager mBusManager;
    private Geolocation mCurrentGeolocation;
    private Utm mUtmData;
    private boolean isSpecialOfferPopupEnabled;

    @Inject
    public BookingSession(Context context, PreferenceManager preferenceManager, EventBusManager busManager) {
        LOGD(TAG, "BookingSession: constructor called");
        mContext = context;
        mBusManager = busManager;
    }

    public APIListCategoriesResponse getCategoryData() {
        return mCategoryData;
    }

    public void setCategoryData(APIListCategoriesResponse mCategoryData) {
        LOGD(TAG, "setCategoryData: storing category data in booking session");
        this.mCategoryData = mCategoryData;
    }

    public APIGetMetaDataResponse getMetaData() {
        return mMetaData;
    }

    public void setMetaData(APIGetMetaDataResponse mMetaData) {
        this.mMetaData = mMetaData;
    }

    public Geolocation getCurrentGeolocation() {
        return mCurrentGeolocation;
    }

    public void setCurrentGeolocation(Geolocation geolocation) {
        LOGD(TAG, "setCurrentGeolocation: ");
        mCurrentGeolocation = geolocation;
    }

    /**
     * Update the session geolocation with the updated categories mapped location coming from api.
     */
    public void updateLocationWithCategoriesLatLng(MetaInfoCategories metaInfoCategories) {
        if (metaInfoCategories != null) {
            if (mCurrentGeolocation == null || !TextUtils.equals(metaInfoCategories.getCircleName(),
              mCurrentGeolocation.getCircleName())) {
                //mCart = new Cart(mContext, metaInfoCategories.getCircleName());
                mCart = new Cart(mContext, metaInfoCategories.getCircleName(), metaInfoCategories.getLatitude(),
                  metaInfoCategories.getLongitude());
            }
            mCurrentGeolocation.setCircleName(metaInfoCategories.getCircleName());
            if (metaInfoCategories.getLastAddress() != null) {
                //means user is logged in and mapped address is different
                mCurrentGeolocation.setLatLng(new LatLng(Double.valueOf(metaInfoCategories.getLastAddress().getLat()),
                  Double.valueOf(metaInfoCategories.getLastAddress().getLng())));
                // Address Id needed for update the address in cart automatically
                mCurrentGeolocation.setUserAddressId(metaInfoCategories.getLastAddress().getAddressId());
                mCurrentGeolocation.setAddress(metaInfoCategories.getLastAddress().getOrderAddress());
            }
        }
    }

    public Cart getCart() {
        return mCart;
    }

    public int getCompleteState() {
        if (mMetaData != null && mMetaData.getData() != null) return mMetaData.getData().getProgressSteps();

        return 0;
    }

    /**
     * Trigger while updating the  service
     */
    public void updateCartResponse(APICartResponse response, long serviceId) {
        mCart.updateCartData(response);
        CartUpdateEvent event = new CartUpdateEvent();
        if (serviceId > 0) {
            int qty = CoreLogic.getQtyOfServiceInCart(response, serviceId);
            mCart.updateServices(serviceId, qty);
        }
        mBusManager.post(event);
    }

    /**
     * Trigger while updating the cart except the service updation
     * Becz Some Page dont wan't to listen the service updation
     */
    public void updateCartResponse(APICartResponse response) {
        mCart.updateCartData(response);
        CartUpdateEvent event = new CartUpdateEvent();
        mBusManager.post(event);
    }

    /**
     * Reset the data here.
     * 1. First time application launch
     * 2. Location Change
     * 3. Booking finished.
     * 4. User log out
     * 5. User login
     */
    public void resetSession() {
        mCategoryData = null;
        if (mCart != null) {
            mCart.resetCart();
        }
        CartUpdateEvent event = new CartUpdateEvent();
        mBusManager.post(event);
    }

    public void resetCachedResponse() {
        mCategoryData = null;
        if (mCart != null) {
            mCart.resetCart();
        }
        //reset the cart also
        CartUpdateEvent event = new CartUpdateEvent();
        mBusManager.post(event);
    }

    public void resetCategorySelection() {
        if (mCategoryData != null) {
            CoreLogic.resetCategoryData(mCategoryData);
            mCart.clearCartDetails();
        }
    }

    public void addUtmToSession(Utm utm) {
        LogUtils.LOGD(TAG, "addUtmToSession : ");
        mUtmData = utm;
    }

    public Utm getUtmData() {
        return mUtmData;
    }

    /**
     * OrdersToReview Should be clear while error
     * or feedback success
     * becz its locally stored in booking session
     * if we are not clearing it will ask the feedback
     * and block the user on the feedback page
     */
    public void clearOrdersToReview() {
        LogUtils.LOGD(TAG, "clearOrdersToReview : ");
        if (mMetaData != null && mMetaData.getData() != null && mMetaData.getData().getOrdersToReview() != null && !mMetaData
          .getData()
          .getOrdersToReview()
          .isEmpty()) {
            mMetaData.getData().getOrdersToReview().clear();
        }
    }

    public void setSpecialDatePopupEnabled(boolean enable) {
        isSpecialOfferPopupEnabled = enable;
        mBusManager.post(new SpecialDateUpdateEvent());
    }

    public boolean isSpecialOfferPopupEnabled() {
        return isSpecialOfferPopupEnabled;
    }

    public boolean isEncryptionDisabled() {
        if (mMetaData != null && mMetaData.getData() != null) return mMetaData.getData().isDisableEncryption();

        return false;
    }
}
