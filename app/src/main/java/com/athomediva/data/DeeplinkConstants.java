package com.athomediva.data;

import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 21/06/17.
 */

public class DeeplinkConstants {
    public static String KEY_DEEPLINK_URL = "key_deeplink_url";
    public static String KEY_ACTION = "key_action";

    public enum DEEPLINK_TYPE {
        HOME_PAGE(0),
        CATEGORY(1),
        CART_PAGE(2),
        MY_ORDERS(3),
        MY_ACCOUNTS(4),
        ORDER(5, true),
        FEEDBACK(6, true),
        INVITE_FRIEND(7, true),
        MY_WALLET(8, true),
        COUPONS_OFFERS(9, true),
        AVAILABILITY_CONFIRM(10),
        WEB_VIEW_LOAD(-1);

        int type = 0;
        boolean isLaunchLogin;

        DEEPLINK_TYPE(int value) {
            type = value;
        }

        DEEPLINK_TYPE(int value, boolean isLoginNeed) {
            type = value;
            isLaunchLogin = isLoginNeed;
        }

        public int getType() {
            return type;
        }

        public boolean isLoginNeed() {
            return isLaunchLogin;
        }
    }

    public enum DEEPLINK_PATHS {
        HOME_PAGE(-1, DEEPLINK_TYPE.HOME_PAGE, DEEPLINK_API.DEEPLINK_DATA),
        //  data Get it from Deeplink Data api
        CATEGORY(R.string.deeplink_category, DEEPLINK_TYPE.CATEGORY, DEEPLINK_API.DEEPLINK_DATA),
        //local path
        CART_PAGE(-1, DEEPLINK_TYPE.CART_PAGE, DEEPLINK_API.DEEPLINK_DATA),
        //  data Get it from Deeplink Data api
        FEEDBACK(R.string.deeplink_feedback, DEEPLINK_TYPE.FEEDBACK, DEEPLINK_API.META_DATA),
        // meta data api needed
        BOOKING_DETAILS(R.string.deeplink_order, DEEPLINK_TYPE.ORDER, DEEPLINK_API.META_DATA),
        // meta data api needed
        MY_ORDER(R.string.deeplink_myorders, DEEPLINK_TYPE.MY_ORDERS, DEEPLINK_API.NO_API),
        MY_ACCOUNT(R.string.deeplink_account, DEEPLINK_TYPE.MY_ACCOUNTS, DEEPLINK_API.NO_API),
        // no api needed
        INVITE_FRIEND(R.string.deeplink_invite, DEEPLINK_TYPE.INVITE_FRIEND, DEEPLINK_API.NO_API),

        MY_WALLET(R.string.deeplink_mywallet, DEEPLINK_TYPE.MY_WALLET, DEEPLINK_API.NO_API),

        COUPONS_OFFERS(R.string.deeplink_coupons, DEEPLINK_TYPE.COUPONS_OFFERS, DEEPLINK_API.NO_API),

        //meta data api needed

        AVAILABILITY_CONFIRM(R.string.deeplink_availability_confirm, DEEPLINK_TYPE.AVAILABILITY_CONFIRM,
          DEEPLINK_API.AVAILABILITY_CONFIRM_API);

        //MY_WALLET("/invite");

        int res;
        DEEPLINK_TYPE type;
        DEEPLINK_API api;

        DEEPLINK_PATHS(int res, DEEPLINK_TYPE type) {
            this.res = res;
            this.type = type;
        }

        DEEPLINK_PATHS(int res, DEEPLINK_TYPE type, DEEPLINK_API api) {
            this.res = res;
            this.type = type;
            this.api = api;
        }

        public int getPathRes() {
            return res;
        }

        public DEEPLINK_TYPE getDeeplinkType() {
            return type;
        }

        public DEEPLINK_API getDeeplinkApi() {
            return api;
        }

        ;
    }

    public enum DEEPLINK_API {
        DEEPLINK_DATA,
        META_DATA,
        USER_DETAILS,
        META_AND_USER_DET,
        AVAILABILITY_CONFIRM_API,
        NO_API;
    }

    public interface Deeplink {
        String DEEPLINK_BOOKING_PAGE = "http://www.athomediva.com/order?orderId=?";
        String DEEPLINK_FEEDPACK_PAGE = "http://www.athomediva.com/feedback?orderId=?";
        String DEEPLINK_CART_PAGE = "http://www.athomediva.com/cart?cartId=?";

        String DEEPLINK_MY_ORDERS = "http://www.athomediva.com/myorders";
        String DEEPLINK_MY_ACCOUNTS = "http://www.athomediva.com/myaccounts";
        String DEEPLINK_MY_WALLET = "http://www.athomediva.com/mywallet";
        String DEEPLINK_FEEDBACK = "http://www.athomediva.com/feedback?orderId=?";
        String DEEPLINK_COUPONS_OFFER = "http://www.athomediva.com/coupons_offers";
        String DEEPLINK_INVITE = "http://www.athomediva.com/invite";
    }

    // Deeplink path , Deeplink type, api type, callback

    public interface DEEPLINK_PARAMS {
        // UTM PARAMS
        String UTM_SOURCE = "utm_source";

        String UTM_MEDIUM = "utm_medium";

        String UTM_CAMPAIGN = "utm_campaign";

        String GCL_ID = "gclid";

        String KEYWORD = "kw";

        String PLATFORM = "platform";

        //QUERY PARAMS
        String ORDER_ID = "orderId";
        String CART_ID = "cartId";
        String CAT_ID = "catId";
        String AVAIL_CONFIRM_IDENTIFIER = "identifier";
    }
}
