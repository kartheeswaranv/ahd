package com.athomediva.data.models.local;

/**
 * Created by mohitkumar on 13/04/17.
 */

public class AddAddressItem {
    private final Entity<String> buildingName;
    private final Entity<String> landmark;
    private final Entity<String> addressType;
    private final Entity<String> customAddressName;
    private final Entity<Geolocation> fullAddress;

    public AddAddressItem() {
        buildingName = new Entity<>();
        landmark = new Entity<>();
        addressType = new Entity<>();
        fullAddress = new Entity<>();
        customAddressName = new Entity<>();
        fullAddress.setData(new Geolocation(0, 0));
    }

    public Entity<String> getBuildingEntity() {
        return buildingName;
    }

    public Entity<String> getLandmarkEntity() {
        return landmark;
    }

    public Entity<String> getAddressTypeEntity() {
        return addressType;
    }

    public Entity<Geolocation> getFullAddressEntity() {
        return fullAddress;
    }

    public Entity<String> getCustomAddressEntity() {
        return customAddressName;
    }
}
