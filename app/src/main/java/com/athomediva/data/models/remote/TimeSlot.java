package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 22/03/17.
 */

public class TimeSlot implements Parcelable {
    public static final Creator<TimeSlot> CREATOR = new Creator<TimeSlot>() {
        public TimeSlot createFromParcel(Parcel source) {
            return new TimeSlot(source);
        }

        public TimeSlot[] newArray(int size) {
            return new TimeSlot[size];
        }
    };
    private long from;
    private long to;
    private String displayFrom;
    private String displayTo;
    private String displayTime;
    private boolean isSelected;
    private String circle;

    protected TimeSlot(Parcel in) {
        this.displayFrom = in.readString();
        this.displayTo = in.readString();
        this.displayTime = in.readString();
        this.from = in.readLong();
        this.to = in.readLong();
        this.isSelected = in.readByte() != 0;
    }

    public TimeSlot() {

    }

    public String getCircle() {
        return circle;
    }

    public long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getTo() {
        return to;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public String getDisplayFrom() {
        return displayFrom;
    }

    public void setDisplayFrom(String displayFrom) {
        this.displayFrom = displayFrom;
    }

    public String getDisplayTo() {
        return displayTo;
    }

    public void setDisplayTo(String displayTo) {
        this.displayTo = displayTo;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            TimeSlot slot = (TimeSlot) o;
            if (slot.getFrom() == from && slot.getTo() == to) return true;
        }
        return false;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(displayFrom);
        parcel.writeString(displayTo);
        parcel.writeString(displayTime);
        parcel.writeLong(from);
        parcel.writeLong(to);
        parcel.writeByte(isSelected ? (byte) 1 : (byte) 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
