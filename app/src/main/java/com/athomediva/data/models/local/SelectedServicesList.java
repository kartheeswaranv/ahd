package com.athomediva.data.models.local;

import java.util.ArrayList;

/**
 * Created by kartheeswaran on 28/04/17.
 */

public class SelectedServicesList extends ArrayList<SelectedServiceReq> {

    @Override
    public boolean equals(Object o) {
        return isEqual((SelectedServicesList) o);
    }

    private boolean isEqual(SelectedServicesList array) {
        if (array != null && size() == array.size()) {
            for (int index = 0; index < size(); index++) {
                if (!this.get(index).equals(array.get(index))) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }
}
