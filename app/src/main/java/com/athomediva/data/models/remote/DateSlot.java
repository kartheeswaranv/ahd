package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 22/03/17.
 */

public class DateSlot implements Parcelable{
    private long date;
    private String displayDate;
    private ArrayList<TimeSlot> slots;
    private boolean isSelected;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public ArrayList<TimeSlot> getSlots() {
        return slots;
    }

    public void setSlots(ArrayList<TimeSlot> slots) {
        this.slots = slots;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        if(!isSelected)
            resetOtherDatesSelection();
    }

    public TimeSlot getSelectedTimeSlot() {
        if(slots != null && slots.size() >0) {
            for(TimeSlot slot : slots) {
                if(slot.isSelected())
                    return slot;
            }
        }

        return null;
    }

    private void resetOtherDatesSelection() {
        if(slots != null && slots.size() > 0) {
            for(TimeSlot sl :slots) {
                sl.setSelected(false);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if(o != null) {
            DateSlot slot = (DateSlot)o;
            if(slot.getDate() == date)
                return true;
        }
        return false;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(displayDate);
        parcel.writeByte(isSelected ? (byte) 1 : (byte) 0);
        parcel.writeLong(date);
        parcel.writeTypedList(slots);
    }

    protected DateSlot(Parcel in) {

        this.displayDate = in.readString();
        this.isSelected = in.readByte() != 0;
        this.date = in.readLong();
        this.slots = in.createTypedArrayList(TimeSlot.CREATOR);

    }


    public DateSlot(){

    }

    public static final Creator<DateSlot> CREATOR = new Creator<DateSlot>() {
        public DateSlot createFromParcel(Parcel source) {
            return new DateSlot(source);
        }

        public DateSlot[] newArray(int size) {
            return new DateSlot[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }
}
