package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class CartServiceGroup implements Parcelable {
    private String title;
    private ArrayList<Services> services;

    public String getTitle() {
        return title;
    }

    public ArrayList<Services> getServices() {
        return services;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeTypedList(this.services);
    }

    public CartServiceGroup() {
    }

    protected CartServiceGroup(Parcel in) {
        this.title = in.readString();
        this.services = in.createTypedArrayList(Services.CREATOR);
    }

    public static final Parcelable.Creator<CartServiceGroup> CREATOR = new Parcelable.Creator<CartServiceGroup>() {
        @Override
        public CartServiceGroup createFromParcel(Parcel source) {
            return new CartServiceGroup(source);
        }

        @Override
        public CartServiceGroup[] newArray(int size) {
            return new CartServiceGroup[size];
        }
    };
}
