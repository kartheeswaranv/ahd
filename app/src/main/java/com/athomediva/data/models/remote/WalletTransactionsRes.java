package com.athomediva.data.models.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class WalletTransactionsRes {

    @SerializedName("status") @Expose private Integer status;
    @SerializedName("message") @Expose private String message;
    @SerializedName("code") @Expose private String code;
    @SerializedName("walletdata") @Expose private List<WalletData> walletDataList = new ArrayList<>();
    @SerializedName("wallet_amount") @Expose private String walletAmt;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The walletdata
     */
    public List<WalletData> getWalletdata() {
        return walletDataList;
    }

    /**
     * @param walletdata The walletdata
     */
    public void setWalletdata(List<WalletData> walletdata) {
        this.walletDataList = walletdata;
    }

    /**
     * @return The walletAmt
     */
    public String getWalletAmt() {
        return walletAmt;
    }

    /**
     * @param walletAmt The wallet_amt
     */
    public void setWalletAmt(String walletAmt) {
        this.walletAmt = walletAmt;
    }

    public class WalletData {

        @SerializedName("desc") @Expose private String desc;
        @SerializedName("amt") @Expose private String amt;
        @SerializedName("date") @Expose private String date;

        /**
         * @return The desc
         */
        public String getDesc() {
            return desc;
        }

        /**
         * @param desc The desc
         */
        public void setDesc(String desc) {
            this.desc = desc;
        }

        /**
         * @return The amt
         */
        public String getAmt() {
            return amt;
        }

        /**
         * @param amt The amt
         */
        public void setAmt(String amt) {
            this.amt = amt;
        }

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }
    }
}