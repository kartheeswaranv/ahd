package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by mohitkumar on 24/04/17.
 */

public class APIGetPromotionalAdsResponse {
    public boolean isSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }

    private boolean success;
    private Data data;

    public static class Data {
        public ArrayList<OffersItem> getOffers() {
            return offers;
        }

        ArrayList<OffersItem> offers;
    }
}
