package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 19/06/17.
 */

public class WalletDetails implements Parcelable {
    private ArrayList<WalletInfo> wallet;
    private double total;
    private String color;

    public String getColor() {
        return color;
    }

    public ArrayList<WalletInfo> getWallet() {
        return wallet;
    }

    public double getTotalWalletAmt() {
        return total;
    }

    public WalletDetails() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.wallet);
        dest.writeDouble(this.total);
        dest.writeString(this.color);
    }

    protected WalletDetails(Parcel in) {
        this.wallet = in.createTypedArrayList(WalletInfo.CREATOR);
        this.total = in.readDouble();
        this.color = in.readString();
    }

    public static final Creator<WalletDetails> CREATOR = new Creator<WalletDetails>() {
        @Override
        public WalletDetails createFromParcel(Parcel source) {
            return new WalletDetails(source);
        }

        @Override
        public WalletDetails[] newArray(int size) {
            return new WalletDetails[size];
        }
    };
}
