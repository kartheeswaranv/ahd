package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 01/07/17.
 */

public class ConfirmBookingData implements Parcelable {
    public static final Creator<ConfirmBookingData> CREATOR = new Creator<ConfirmBookingData>() {
        @Override
        public ConfirmBookingData createFromParcel(Parcel source) {
            return new ConfirmBookingData(source);
        }

        @Override
        public ConfirmBookingData[] newArray(int size) {
            return new ConfirmBookingData[size];
        }
    };
    private ArrayList<ConfirmBookingInfo> bucketInfo;
    @SerializedName("thankyouHeader") private ArrayList<BookingDescInfo> bookingHeaderList;
    @SerializedName("thankyouFooter") private ArrayList<BookingDescInfo> bookingFooterList;
    @SerializedName("infoList") private ArrayList<BookingExtraInfo> bookingExtraInfo;
    private String paymentUrl;
    private String verifyPaymentUrl;
    private String totalAmountPayable;

    public ConfirmBookingData() {
    }

    protected ConfirmBookingData(Parcel in) {
        this.bucketInfo = in.createTypedArrayList(ConfirmBookingInfo.CREATOR);
        this.bookingHeaderList = in.createTypedArrayList(BookingDescInfo.CREATOR);
        this.bookingFooterList = in.createTypedArrayList(BookingDescInfo.CREATOR);
        this.bookingExtraInfo = in.createTypedArrayList(BookingExtraInfo.CREATOR);
        this.paymentUrl = in.readString();
        this.verifyPaymentUrl = in.readString();
        this.totalAmountPayable = in.readString();
    }

    public String getTotal() {
        return totalAmountPayable;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public String getVerifyPaymentUrl() {
        return verifyPaymentUrl;
    }

    public ArrayList<ConfirmBookingInfo> getBucketInfo() {
        return bucketInfo;
    }

    public ArrayList<BookingDescInfo> getBookingHeaders() {
        return bookingHeaderList;
    }

    public ArrayList<BookingExtraInfo> getBookingExtraInfo() {
        return bookingExtraInfo;
    }

    public ArrayList<BookingDescInfo> getBookingFooters() {
        return bookingFooterList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.bucketInfo);
        dest.writeTypedList(this.bookingHeaderList);
        dest.writeTypedList(this.bookingFooterList);
        dest.writeTypedList(this.bookingExtraInfo);
        dest.writeString(this.paymentUrl);
        dest.writeString(this.verifyPaymentUrl);
        dest.writeString(this.totalAmountPayable);
    }
}
