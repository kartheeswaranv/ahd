package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 19/06/17.
 */

public class MinimumOrderError implements Parcelable {
    private String title;
    private ArrayList<MinOrderErrorInfo> errorList;

    public String getTitle() {
        return title;
    }

    public ArrayList<MinOrderErrorInfo> getInfoList() {
        return errorList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeList(this.errorList);
    }

    public MinimumOrderError() {
    }

    protected MinimumOrderError(Parcel in) {
        this.title = in.readString();
        this.errorList = new ArrayList<MinOrderErrorInfo>();
        in.readList(this.errorList, MinOrderErrorInfo.class.getClassLoader());
    }

    public static final Parcelable.Creator<MinimumOrderError> CREATOR = new Parcelable.Creator<MinimumOrderError>() {
        @Override
        public MinimumOrderError createFromParcel(Parcel source) {
            return new MinimumOrderError(source);
        }

        @Override
        public MinimumOrderError[] newArray(int size) {
            return new MinimumOrderError[size];
        }
    };
}
