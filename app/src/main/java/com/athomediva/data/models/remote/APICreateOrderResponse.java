package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 05/07/17.
 */
public class APICreateOrderResponse implements Parcelable {
    private String apiVersion;
    private CreateOrderError error;
    private CartData data;
    private boolean success;

    public String getApiVersion() {
        return apiVersion;
    }

    public CreateOrderError getError() {
        return error;
    }

    public CartData getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.data, flags);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public APICreateOrderResponse() {
    }

    protected APICreateOrderResponse(Parcel in) {
        this.apiVersion = in.readString();
        this.error = in.readParcelable(CreateOrderError.class.getClassLoader());
        this.data = in.readParcelable(CartData.class.getClassLoader());
        this.success = in.readByte() != 0;
    }

    public static final Parcelable.Creator<APICreateOrderResponse> CREATOR =
      new Parcelable.Creator<APICreateOrderResponse>() {
          @Override
          public APICreateOrderResponse createFromParcel(Parcel source) {
              return new APICreateOrderResponse(source);
          }

          @Override
          public APICreateOrderResponse[] newArray(int size) {
              return new APICreateOrderResponse[size];
          }
      };
}
