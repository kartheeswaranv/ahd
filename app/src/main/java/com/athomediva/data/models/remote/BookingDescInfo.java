package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 30/06/17.
 */

public class BookingDescInfo implements Parcelable {
    private String title;
    private String description1;
    private String description2;
    private String description3;

    public String getTitle() {
        return title;
    }

    public String getDescription1() {
        return description1;
    }

    public String getDescription2() {
        return description2;
    }

    public String getDescription3() {
        return description3;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description1);
        dest.writeString(this.description2);
        dest.writeString(this.description3);
    }

    public BookingDescInfo() {
    }

    protected BookingDescInfo(Parcel in) {
        this.title = in.readString();
        this.description1 = in.readString();
        this.description2 = in.readString();
        this.description3 = in.readString();
    }

    public static final Parcelable.Creator<BookingDescInfo> CREATOR = new Parcelable.Creator<BookingDescInfo>() {
        @Override
        public BookingDescInfo createFromParcel(Parcel source) {
            return new BookingDescInfo(source);
        }

        @Override
        public BookingDescInfo[] newArray(int size) {
            return new BookingDescInfo[size];
        }
    };
}
