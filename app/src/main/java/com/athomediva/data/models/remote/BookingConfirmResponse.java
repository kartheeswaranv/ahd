package com.athomediva.data.models.remote;

import java.util.List;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class BookingConfirmResponse {

    private List<ConfirmBookingInfo> bookingList;

    public List<ConfirmBookingInfo> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<ConfirmBookingInfo> bookingList) {
        this.bookingList = bookingList;
    }
}
