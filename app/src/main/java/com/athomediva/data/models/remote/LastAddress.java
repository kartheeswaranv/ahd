package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class LastAddress {
    private String orderCircle;

    private String orderAddress;

    private String lng;

    private String orderAddressId;

    private String addressId;

    private String circleName;

    private String lat;

    public String getOrderCircle() {
        return orderCircle;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public String getLng() {
        return lng;
    }

    public String getOrderAddressId() {
        return orderAddressId;
    }

    public String getAddressId() {
        return addressId;
    }

    public String getCircleName() {
        return circleName;
    }

    public String getLat() {
        return lat;
    }
}