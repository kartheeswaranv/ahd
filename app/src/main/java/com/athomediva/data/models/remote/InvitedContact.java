package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 04/09/17.
 */

public class InvitedContact implements Parcelable {
    private String inviteId;
    private String invitedMobile;
    private int status;

    public InvitedContact(String mobile) {
        this.invitedMobile = mobile;
    }

    public InvitedContact(String mobile, String id, int status) {
        this.status = status;
        this.invitedMobile = mobile;
        this.inviteId = id;
    }

    public String getInviteId() {
        return inviteId;
    }

    public String getInvitedMobile() {
        return invitedMobile;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.inviteId);
        dest.writeString(this.invitedMobile);
        dest.writeInt(this.status);
    }

    public InvitedContact() {
    }

    protected InvitedContact(Parcel in) {
        this.inviteId = in.readString();
        this.invitedMobile = in.readString();
        this.status = in.readInt();
    }

    public static final Parcelable.Creator<InvitedContact> CREATOR = new Parcelable.Creator<InvitedContact>() {
        @Override
        public InvitedContact createFromParcel(Parcel source) {
            return new InvitedContact(source);
        }

        @Override
        public InvitedContact[] newArray(int size) {
            return new InvitedContact[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        InvitedContact contact = (InvitedContact) obj;

        if (contact != null) {
            return contact.getInvitedMobile().equals(getInvitedMobile());
        } else {
            return false;
        }
    }
}
