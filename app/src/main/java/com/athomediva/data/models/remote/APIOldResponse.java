package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 16/06/17.
 */

public class APIOldResponse {
    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
