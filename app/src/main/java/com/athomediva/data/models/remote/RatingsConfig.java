package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class RatingsConfig {
    public int getMax() {
        return max;
    }

    public boolean getOptional() {
        return optional;
    }

    private String title;

    private int max;

    private boolean optional;

    private String rating;

    public String getTitle() {
        return title;
    }

    public String getRating() {
        return rating;
    }

}