package com.athomediva.data.models.local;

import android.support.annotation.DrawableRes;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class EmptySectionItem {
    private final String sectionTitle;
    private final String sectionDescription;
    private final int drawableResourceID;
    private final String actionButtonTitle;

    public EmptySectionItem(String sectionTitle, String sectionDescription, @DrawableRes int drawableResourceID,
      String actionButtonTitle) {
        this.sectionTitle = sectionTitle;
        this.sectionDescription = sectionDescription;
        this.drawableResourceID = drawableResourceID;
        this.actionButtonTitle = actionButtonTitle;
    }

    public EmptySectionItem(String sectionTitle, String sectionDescription, @DrawableRes int drawableResourceID) {
        this.sectionTitle = sectionTitle;
        this.sectionDescription = sectionDescription;
        this.drawableResourceID = drawableResourceID;
        this.actionButtonTitle = null;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public String getSectionDescription() {
        return sectionDescription;
    }

    public int getDrawableResourceID() {
        return drawableResourceID;
    }

    public String getActionButtonTitle() {
        return actionButtonTitle;
    }
}
