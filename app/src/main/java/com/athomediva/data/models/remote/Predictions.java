package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 27/04/17.
 */

public class Predictions {
    private String id;

    private String place_id;

    private String description;

    private String reference;
    private StructuredFormatting structured_formatting;

    public StructuredFormatting getStructuredFormatting() {
        return structured_formatting;
    }

    public String getId() {
        return id;
    }

    public String getPlaceId() {
        return place_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}

