package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class StatusModel {
    private int status;
    private int maxStatus;
    private boolean enableArrow;
    private String message;
    private boolean isAnimationEnable;
    private int msgGravity;
    private long duration;
    private String statusTxtColor;
    private boolean isRated;
    private boolean isPayOnlineEnable;
    private boolean isCallEnable;
    private String titleText;
    private float rating;

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public boolean isRated() {
        return isRated;
    }

    public void setRated(boolean rated) {
        isRated = rated;
    }

    public boolean isPayOnlineEnable() {
        return isPayOnlineEnable;
    }

    public void setPayOnlineEnable(boolean payOnlineEnable) {
        isPayOnlineEnable = payOnlineEnable;
    }

    public String getStatusTxtColor() {
        return statusTxtColor;
    }

    public void setStatusTxtColor(String statusTxtColor) {
        this.statusTxtColor = statusTxtColor;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMaxStatus() {
        return maxStatus;
    }

    public void setMaxStatus(int maxStatus) {
        this.maxStatus = maxStatus;
    }

    public boolean isEnableArrow() {
        return enableArrow;
    }

    public void setEnableArrow(boolean enableArrow) {
        this.enableArrow = enableArrow;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isAnimationEnable() {
        return isAnimationEnable;
    }

    public void setAnimationEnable(boolean animationEnable) {
        isAnimationEnable = animationEnable;
    }

    public int getMsgGravity() {
        return msgGravity;
    }

    public void setMsgGravity(int msgGravity) {
        this.msgGravity = msgGravity;
    }

    public boolean isCallEnable() {
        return isCallEnable;
    }

    public void setCallEnable(boolean callEnable) {
        isCallEnable = callEnable;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
