package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class ConfirmBookingInfo implements Parcelable {
    private String bucketName;
    private String bookingId;
    private String scheduledOn;
    private String bookingIdText;
    private double amount;

    public double getAmount() {
        return amount;
    }

    public String getBookingIdText() {
        return bookingIdText;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getScheduleDate() {
        return scheduledOn;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduledOn = scheduleDate;
    }

    public ConfirmBookingInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bucketName);
        dest.writeString(this.bookingId);
        dest.writeString(this.scheduledOn);
        dest.writeString(this.bookingIdText);
        dest.writeDouble(this.amount);
    }

    protected ConfirmBookingInfo(Parcel in) {
        this.bucketName = in.readString();
        this.bookingId = in.readString();
        this.scheduledOn = in.readString();
        this.bookingIdText = in.readString();
        this.amount = in.readDouble();
    }

    public static final Creator<ConfirmBookingInfo> CREATOR = new Creator<ConfirmBookingInfo>() {
        @Override
        public ConfirmBookingInfo createFromParcel(Parcel source) {
            return new ConfirmBookingInfo(source);
        }

        @Override
        public ConfirmBookingInfo[] newArray(int size) {
            return new ConfirmBookingInfo[size];
        }
    };
}
