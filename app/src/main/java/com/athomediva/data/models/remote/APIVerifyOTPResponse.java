package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 20/09/16.
 */
public class APIVerifyOTPResponse {
    private String apiVersion;
    private Data data;
    private boolean success;
    private Error error;

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public boolean getSuccess() {
        return success;
    }

    public static class Data {
        private String message;
        private String token;
        private LoggedInUserDetails userDetails;

        public String getMessage() {
            return message;
        }

        public String getToken() {
            return token;
        }

        public LoggedInUserDetails getUserDetails() {
            return userDetails;
        }
    }
}