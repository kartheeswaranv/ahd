package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kartheeswaran on 16/06/17.
 */

public class BookingDetailsBtns implements Parcelable {

    @SerializedName("BTCBI") private boolean isConfirmBooking;
    @SerializedName("BTOMY") private boolean isStylistOnWay;
    @SerializedName("BTRC") private boolean isCustomerReached;
    @SerializedName("BTSS") private boolean isServiceStarted;
    @SerializedName("BTCA") private boolean isCancelled;
    @SerializedName("BTES") private boolean isServiceEnd;
    @SerializedName("BTRE") private boolean isReported;
    @SerializedName("BTCALL") private boolean isCallEnable;
    @SerializedName("BTSMS") private boolean isSmsEnable;
    @SerializedName("BTEND") private boolean isEnd;
    @SerializedName("BTAUTH") private boolean isAuthCodeEnable;
    @SerializedName("BTCOM") private boolean isCompleted;

    public boolean isConfirmBooking() {
        return isConfirmBooking;
    }

    public boolean isStylistOnWay() {
        return isStylistOnWay;
    }

    public boolean isCustomerReached() {
        return isCustomerReached;
    }

    public boolean isServiceStarted() {
        return isServiceStarted;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public boolean isServiceEnd() {
        return isServiceEnd;
    }

    public boolean isReported() {
        return isReported;
    }

    public boolean isCallEnable() {
        return isCallEnable;
    }

    public boolean isSmsEnable() {
        return isSmsEnable;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public boolean isAuthCodeEnable() {
        return isAuthCodeEnable;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isConfirmBooking ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isStylistOnWay ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCustomerReached ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isServiceStarted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCancelled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isServiceEnd ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isReported ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCallEnable ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isSmsEnable ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isEnd ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isAuthCodeEnable ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCompleted ? (byte) 1 : (byte) 0);
    }

    public BookingDetailsBtns() {
    }

    protected BookingDetailsBtns(Parcel in) {
        this.isConfirmBooking = in.readByte() != 0;
        this.isStylistOnWay = in.readByte() != 0;
        this.isCustomerReached = in.readByte() != 0;
        this.isServiceStarted = in.readByte() != 0;
        this.isCancelled = in.readByte() != 0;
        this.isServiceEnd = in.readByte() != 0;
        this.isReported = in.readByte() != 0;
        this.isCallEnable = in.readByte() != 0;
        this.isSmsEnable = in.readByte() != 0;
        this.isEnd = in.readByte() != 0;
        this.isAuthCodeEnable = in.readByte() != 0;
        this.isCompleted = in.readByte() != 0;
    }

    public static final Parcelable.Creator<BookingDetailsBtns> CREATOR = new Parcelable.Creator<BookingDetailsBtns>() {
        @Override
        public BookingDetailsBtns createFromParcel(Parcel source) {
            return new BookingDetailsBtns(source);
        }

        @Override
        public BookingDetailsBtns[] newArray(int size) {
            return new BookingDetailsBtns[size];
        }
    };
}
