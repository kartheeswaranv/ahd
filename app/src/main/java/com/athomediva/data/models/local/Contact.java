package com.athomediva.data.models.local;

import android.text.TextUtils;
import com.athomediva.data.AppConstants;

/**
 * Created by kartheeswaran on 29/08/17.
 */

public class Contact {
    private String phoneNo;
    private String name;
    private int status;
    private boolean isChecked;

    public Contact(String phoneNo, String name) {
        this.phoneNo = phoneNo;
        this.name = name;
        this.status = AppConstants.INVITE_STATUS.NOT_INVITE.getStatus();
    }

    public Contact(String phoneNo, String name, int status) {
        this.phoneNo = phoneNo;
        this.name = name;
        this.status = status;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        Contact c = (Contact) obj;
        if (c != null) {
            return TextUtils.equals(c.getPhoneNo(), phoneNo);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return this.getPhoneNo();
    }
}
