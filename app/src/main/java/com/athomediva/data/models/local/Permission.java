package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 29/11/16.
 */

public class Permission {
    private String permission;
    private boolean shouldShowRationale;
    private String message;

    public Permission(String permission) {
        this(permission, false, null);
    }

    public Permission(String permission, boolean showRationale, String message) {
        this.permission = permission;
        shouldShowRationale = showRationale;
        this.message = message;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public boolean isShouldShowInfoDialog() {
        return shouldShowRationale;
    }

    public void setInfoDialog(boolean shouldShowRationale) {
        this.shouldShowRationale = shouldShowRationale;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
