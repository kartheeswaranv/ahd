package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class Package {
    private String icon;

    private int maxQty;

    private int bucketId;

    private String durationText;

    private double strikeOffPrice;

    private String url;

    private String info;

    private ArrayList<String> infoList;

    private int duration;

    public int getMaxQty() {
        return maxQty;
    }

    public int getBucketId() {
        return bucketId;
    }

    public String getDurationText() {
        return durationText;
    }

    public double getStrikeOffPrice() {
        return strikeOffPrice;
    }

    public String getUrl() {
        return url;
    }

    public String getInfo() {
        return info;
    }

    public ArrayList<String> getInfoList() {
        return infoList;
    }

    public int getDuration() {
        return duration;
    }

    public double getPrice() {
        return price;
    }

    public long getServiceId() {
        return serviceId;
    }

    public double getPriceExcludeTax() {
        return priceExcludeTax;
    }

    public String getName() {
        return name;
    }

    private double price;

    private long serviceId;

    private double priceExcludeTax;

    private String name;

    public String getIcon() {
        return icon;
    }
}
