package com.athomediva.data.models.local;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mohitkumar on 26/04/17.
 */

public class Geolocation implements Parcelable {
    public static final Creator<Geolocation> CREATOR = new Creator<Geolocation>() {
        @Override
        public Geolocation createFromParcel(Parcel source) {
            return new Geolocation(source);
        }

        @Override
        public Geolocation[] newArray(int size) {
            return new Geolocation[size];
        }
    };
    private LatLng latLng;
    private String address;
    private String userAddressId;
    private String pinCode;
    private String circleName;

    public Geolocation(double latitude, double longitude) {
        latLng = new LatLng(latitude, longitude);
    }

    public Geolocation(String latitude, String longitude) {
        latLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
    }

    protected Geolocation(Parcel in) {
        this.latLng = in.readParcelable(LatLng.class.getClassLoader());
        this.address = in.readString();
        this.userAddressId = in.readString();
        this.circleName = in.readString();
        this.pinCode = in.readString();
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String code) {
        this.pinCode = code;
    }

    public String getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(String userAddressId) {
        this.userAddressId = userAddressId;
    }

    public LatLng getLocation() {
        return latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return latLng + "," + address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.latLng, flags);
        dest.writeString(this.address);
        dest.writeString(this.userAddressId);
        dest.writeString(this.circleName);
        dest.writeString(this.pinCode);
    }
}
