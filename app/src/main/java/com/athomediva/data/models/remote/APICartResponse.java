package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class APICartResponse implements Parcelable {
    private String apiVersion;
    private Error error;
    private CartData data;
    private boolean success;

    public String getApiVersion() {
        return apiVersion;
    }

    public Error getError() {
        return error;
    }

    public CartData getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.data, flags);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public APICartResponse() {
    }

    protected APICartResponse(Parcel in) {
        this.apiVersion = in.readString();
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.data = in.readParcelable(CartData.class.getClassLoader());
        this.success = in.readByte() != 0;
    }

    public static final Parcelable.Creator<APICartResponse> CREATOR = new Parcelable.Creator<APICartResponse>() {
        @Override
        public APICartResponse createFromParcel(Parcel source) {
            return new APICartResponse(source);
        }

        @Override
        public APICartResponse[] newArray(int size) {
            return new APICartResponse[size];
        }
    };
}
