package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by mohitkumar on 27/04/17.
 */

public class APIGoogleAutoSuggestResponse {
    private ArrayList<Predictions> predictions;

    private String status;

    public ArrayList<Predictions> getPredictions() {
        return predictions;
    }

    public String getStatus() {
        return status;
    }
}
