package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 14/06/17.
 */

public class BookingRequestData {
    private long serviceId;
    private String addressId;
    private String cartId;
    private String circle;
    private SelectedTimeSlot scheduleTime;
    private String couponCode;
    private String bookingId;
    private String action;
    private boolean isBookingResNeeded;
    private String latitude;
    private String longitude;

    public BookingRequestData(long serviceId, String circle) {
        this.serviceId = serviceId;
        this.circle = circle;
    }

    public BookingRequestData(long serviceId, String circle, String lat, String lng) {
        this.serviceId = serviceId;
        this.circle = circle;
        this.latitude = lat;
        this.longitude = lng;
    }

    public BookingRequestData(String action) {
        this.action = action;
    }

    public BookingRequestData() {
    }

    public boolean isBookingResNeeded() {
        return isBookingResNeeded;
    }

    public void setBookingResNeeded(boolean bookingResNeeded) {
        isBookingResNeeded = bookingResNeeded;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String address) {
        this.addressId = address;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public SelectedTimeSlot getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(SelectedTimeSlot scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
