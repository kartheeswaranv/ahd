package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 30/06/17.
 */

public class ConfirmBookingError implements Parcelable {
    private String message;
    private long code;
    private ConfirmBookingData data;

    public String getMessage() {
        return message;
    }

    public long getCode() {
        return code;
    }

    public ConfirmBookingData getData() {
        return data;
    }

    public ConfirmBookingError() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeLong(this.code);
        dest.writeParcelable(this.data, flags);
    }

    protected ConfirmBookingError(Parcel in) {
        this.message = in.readString();
        this.code = in.readLong();
        this.data = in.readParcelable(ConfirmBookingData.class.getClassLoader());
    }

    public static final Creator<ConfirmBookingError> CREATOR = new Creator<ConfirmBookingError>() {
        @Override
        public ConfirmBookingError createFromParcel(Parcel source) {
            return new ConfirmBookingError(source);
        }

        @Override
        public ConfirmBookingError[] newArray(int size) {
            return new ConfirmBookingError[size];
        }
    };
}
