package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 26/07/17.
 */

public class DateTimeSlotData implements Parcelable {

    private String timeConfigMsg;
    private ArrayList<DateSlot> dates;

    public String getTimeConfigMsg() {
        return timeConfigMsg;
    }

    public ArrayList<DateSlot> getDates() {
        return dates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.timeConfigMsg);
        dest.writeTypedList(this.dates);
    }

    public DateTimeSlotData() {
    }

    protected DateTimeSlotData(Parcel in) {
        this.timeConfigMsg = in.readString();
        this.dates = in.createTypedArrayList(DateSlot.CREATOR);
    }

    public static final Parcelable.Creator<DateTimeSlotData> CREATOR = new Parcelable.Creator<DateTimeSlotData>() {
        @Override
        public DateTimeSlotData createFromParcel(Parcel source) {
            return new DateTimeSlotData(source);
        }

        @Override
        public DateTimeSlotData[] newArray(int size) {
            return new DateTimeSlotData[size];
        }
    };
}
