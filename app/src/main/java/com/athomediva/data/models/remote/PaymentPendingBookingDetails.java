package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingBookingDetails implements Parcelable {
    private String title;
    @SerializedName("services") private PaymentPendingBookingData bookingData;

    public String getTitle() {
        return title;
    }

    public PaymentPendingBookingData getBookingData() {
        return bookingData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeParcelable(this.bookingData, flags);
    }

    public PaymentPendingBookingDetails() {
    }

    protected PaymentPendingBookingDetails(Parcel in) {
        this.title = in.readString();
        this.bookingData = in.readParcelable(PaymentPendingBookingData.class.getClassLoader());
    }

    public static final Parcelable.Creator<PaymentPendingBookingDetails> CREATOR =
      new Parcelable.Creator<PaymentPendingBookingDetails>() {
          @Override
          public PaymentPendingBookingDetails createFromParcel(Parcel source) {
              return new PaymentPendingBookingDetails(source);
          }

          @Override
          public PaymentPendingBookingDetails[] newArray(int size) {
              return new PaymentPendingBookingDetails[size];
          }
      };
}
