package com.athomediva.data.models.local;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by mohitkumar on 19/09/16.
 */
public class Consumer implements Parcelable {
    public static final Creator<Consumer> CREATOR = new Creator<Consumer>() {
        @Override
        public Consumer createFromParcel(Parcel source) {
            return new Consumer(source);
        }

        @Override
        public Consumer[] newArray(int size) {
            return new Consumer[size];
        }
    };
    private String userName;
    private String mobileNumber;
    private String refcode;
    private String gender;
    private String emailID;
    private Device device;
    private String accessToken;
    private String userId;
    private String image;

    public Consumer() {
    }

    protected Consumer(Parcel in) {
        this.userName = in.readString();
        this.mobileNumber = in.readString();
        this.refcode = in.readString();
        this.gender = in.readString();
        this.emailID = in.readString();
        this.device = in.readParcelable(Device.class.getClassLoader());
        this.accessToken = in.readString();
        this.userId = in.readString();
        this.image = in.readString();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userName);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.refcode);
        dest.writeString(this.gender);
        dest.writeString(this.emailID);
        dest.writeParcelable(this.device, flags);
        dest.writeString(this.accessToken);
        dest.writeString(this.userId);
        dest.writeString(this.image);
    }
}
