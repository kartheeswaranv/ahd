package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class CartHeaderDescription implements Parcelable {
    private String title;
    private ArrayList<DescriptionInfo> info;

    public String getTitle() {
        return title;
    }

    public ArrayList<DescriptionInfo> getInfo() {
        return info;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeTypedList(this.info);
    }

    public CartHeaderDescription() {
    }

    protected CartHeaderDescription(Parcel in) {
        this.title = in.readString();
        this.info = in.createTypedArrayList(DescriptionInfo.CREATOR);
    }

    public static final Parcelable.Creator<CartHeaderDescription> CREATOR = new Parcelable.Creator<CartHeaderDescription>() {
        @Override
        public CartHeaderDescription createFromParcel(Parcel source) {
            return new CartHeaderDescription(source);
        }

        @Override
        public CartHeaderDescription[] newArray(int size) {
            return new CartHeaderDescription[size];
        }
    };
}
