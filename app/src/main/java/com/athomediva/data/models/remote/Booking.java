package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class Booking implements Parcelable {

    private String paymentLink;
    private boolean toRate;
    private String userId;
    private String orderId;
    private String serviceName;
    private String stylistName;
    private String orderFinal;
    private int orderStatus;
    private String stylistId;
    private String scheduledDate;
    private String displayStatus;
    private String statusColor;
    private boolean isCancelled;
    private String bucketName;
    private String rateServiceTitleText;
    private String verifyPaymentUrl;

    public String getVerifyPaymentUrl() {
        return verifyPaymentUrl;
    }

    public String getRateServiceTitle() {
        return rateServiceTitleText;
    }

    public String getBucketName() {
        return bucketName;
    }

    public String getPaymentLink() {
        return paymentLink;
    }

    public boolean isOrderRated() {
        return toRate;
    }

    public String getUserId() {
        return userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getStylistName() {
        return stylistName;
    }

    public String getOrderFinal() {
        return orderFinal;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public String getStylistId() {
        return stylistId;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public Booking() {
    }

    public static Creator<Booking> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.paymentLink);
        dest.writeByte(this.toRate ? (byte) 1 : (byte) 0);
        dest.writeString(this.userId);
        dest.writeString(this.orderId);
        dest.writeString(this.serviceName);
        dest.writeString(this.stylistName);
        dest.writeString(this.orderFinal);
        dest.writeInt(this.orderStatus);
        dest.writeString(this.stylistId);
        dest.writeString(this.scheduledDate);
        dest.writeString(this.displayStatus);
        dest.writeString(this.statusColor);
        dest.writeByte(this.isCancelled ? (byte) 1 : (byte) 0);
        dest.writeString(this.bucketName);
        dest.writeString(this.rateServiceTitleText);
        dest.writeString(this.verifyPaymentUrl);
    }

    protected Booking(Parcel in) {
        this.paymentLink = in.readString();
        this.toRate = in.readByte() != 0;
        this.userId = in.readString();
        this.orderId = in.readString();
        this.serviceName = in.readString();
        this.stylistName = in.readString();
        this.orderFinal = in.readString();
        this.orderStatus = in.readInt();
        this.stylistId = in.readString();
        this.scheduledDate = in.readString();
        this.displayStatus = in.readString();
        this.statusColor = in.readString();
        this.isCancelled = in.readByte() != 0;
        this.bucketName = in.readString();
        this.rateServiceTitleText = in.readString();
        this.verifyPaymentUrl = in.readString();
    }

    public static final Creator<Booking> CREATOR = new Creator<Booking>() {
        @Override
        public Booking createFromParcel(Parcel source) {
            return new Booking(source);
        }

        @Override
        public Booking[] newArray(int size) {
            return new Booking[size];
        }
    };
}
