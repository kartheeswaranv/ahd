package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class Categories implements Parcelable {
    private long id;

    private String name;

    private String icon;

    private ArrayList<SubCategories> subcategories;

    public long getId() {
        return id;
    }

    public ArrayList<SubCategories> getSubcategories() {
        return subcategories;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {

        return name;
    }

    public Categories() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.icon);
        dest.writeTypedList(this.subcategories);
    }

    public Categories(long id, String name, String icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    protected Categories(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.icon = in.readString();
        this.subcategories = in.createTypedArrayList(SubCategories.CREATOR);
    }

    public static final Creator<Categories> CREATOR = new Creator<Categories>() {
        @Override
        public Categories createFromParcel(Parcel source) {
            return new Categories(source);
        }

        @Override
        public Categories[] newArray(int size) {
            return new Categories[size];
        }
    };
}