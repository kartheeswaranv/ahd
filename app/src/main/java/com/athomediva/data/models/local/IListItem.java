package com.athomediva.data.models.local;

import android.os.Parcelable;

/**
 * Created by mohit kumar on 20/01/17.
 * A common widget used for ui widgets like generic list view items.
 */

public interface IListItem extends Parcelable {
    String getName();
}
