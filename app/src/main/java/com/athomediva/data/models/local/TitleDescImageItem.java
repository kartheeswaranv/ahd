package com.athomediva.data.models.local;

/**
 * Created by mohitkumar on 08/04/17.
 */

public class TitleDescImageItem {

    private String title;
    private String endDescription;
    private int leftDrawable;

    public TitleDescImageItem(int leftDrawable, String title, String endDescription) {
        this.leftDrawable = leftDrawable;
        this.title = title;
        this.endDescription = endDescription;
    }

    public TitleDescImageItem(int leftDrawable, String title) {
        this.leftDrawable = leftDrawable;
        this.title = title;
    }

    public int getDrawable() {
        return leftDrawable;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return endDescription;
    }
}
