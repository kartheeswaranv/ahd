package com.athomediva.data.models.remote;

import com.athomediva.data.models.local.OptionModel;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class APIGetMetaDataResponse {
    private Data data;
    private boolean success;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    public static class Data {
        @SerializedName("ahdInstantDiscount") private InstantCoupon instantCoupon;

        private ArrayList<OrderInfo> ordersToReview;
        private int progressSteps;
        private ArrayList<OptionModel> cancelReasons;
        private ArrayList<RatingsConfig> ratingsConfig;
        private ArrayList<Offers> offers;
        private AppUpdate appUpdate;
        private ArrayList<Testimonials> testimonials;
        private String homeText;
        @SerializedName("ccNumber") private String customerCareNo;
        private boolean disableEncryption;
        private String inviteMsg;

        public InstantCoupon getInstantCoupon() {
            return instantCoupon;
        }

        public int getProgressSteps() {
            return progressSteps;
        }

        public ArrayList<OptionModel> getCancelReasons() {
            return cancelReasons;
        }

        public ArrayList<OrderInfo> getOrdersToReview() {
            return ordersToReview;
        }

        public ArrayList<RatingsConfig> getRatingsConfig() {
            return ratingsConfig;
        }

        public ArrayList<Offers> getOffers() {
            return offers;
        }

        public AppUpdate getAppUpdate() {
            return appUpdate;
        }

        public ArrayList<Testimonials> getTestimonials() {
            return testimonials;
        }

        public String getHomeText() {
            return homeText;
        }

        public String getCustomerCareNo() {
            return customerCareNo;
        }

        public boolean isDisableEncryption() {
            return disableEncryption;
        }

        public String getInviteMsg() {
            return inviteMsg;
        }
    }
}
