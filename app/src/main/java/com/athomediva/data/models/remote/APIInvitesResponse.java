package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/**
 * Created by kartheeswaran on 04/09/17.
 */

public class APIInvitesResponse implements Parcelable {
    private String apiVersion;
    private Error error;
    private InviteData data;
    private boolean success;

    public String getApiVersion() {
        return apiVersion;
    }

    public Error getError() {
        return error;
    }

    public InviteData getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.data, flags);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public APIInvitesResponse() {
    }

    protected APIInvitesResponse(Parcel in) {
        this.apiVersion = in.readString();
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.data = in.readParcelable(InviteData.class.getClassLoader());
        this.success = in.readByte() != 0;
    }

    public static final Parcelable.Creator<APIInvitesResponse> CREATOR = new Parcelable.Creator<APIInvitesResponse>() {
        @Override
        public APIInvitesResponse createFromParcel(Parcel source) {
            return new APIInvitesResponse(source);
        }

        @Override
        public APIInvitesResponse[] newArray(int size) {
            return new APIInvitesResponse[size];
        }
    };
}
