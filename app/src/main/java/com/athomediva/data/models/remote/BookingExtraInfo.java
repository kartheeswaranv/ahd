package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 14/03/18.
 */

public class BookingExtraInfo implements Parcelable {
    public static final Parcelable.Creator<BookingExtraInfo> CREATOR = new Parcelable.Creator<BookingExtraInfo>() {
        @Override
        public BookingExtraInfo createFromParcel(Parcel source) {
            return new BookingExtraInfo(source);
        }

        @Override
        public BookingExtraInfo[] newArray(int size) {
            return new BookingExtraInfo[size];
        }
    };
    private String info;
    private String color;

    public BookingExtraInfo() {
    }

    protected BookingExtraInfo(Parcel in) {
        this.info = in.readString();
        this.color = in.readString();
    }

    public String getInfo() {
        return info;
    }

    public String getColor() {
        return color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.info);
        dest.writeString(this.color);
    }
}
