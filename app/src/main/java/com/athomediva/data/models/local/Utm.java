package com.athomediva.data.models.local;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kartheeswaran on 29/06/17.
 */


public class Utm
{
    @SerializedName("source")
    public String source;
    @SerializedName("medium")
    public String medium;
    @SerializedName("name")
    public String campaign;

    public String gclId;

    public String keyword;

    public String uri;
}