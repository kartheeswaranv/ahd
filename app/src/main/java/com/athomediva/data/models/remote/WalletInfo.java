package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 11/04/17.
 */

public class WalletInfo implements Parcelable {
    private double totalAmount;
    private String name;

    public WalletInfo() {
    }

    protected WalletInfo(Parcel in) {
        this.totalAmount = in.readDouble();
        this.name = in.readString();
    }

    public static Creator<WalletInfo> getCREATOR() {
        return CREATOR;
    }

    public String getName() {
        return name;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.totalAmount);
        dest.writeString(this.name);
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static final Creator<WalletInfo> CREATOR = new Creator<WalletInfo>() {
        @Override
        public WalletInfo createFromParcel(Parcel source) {
            return new WalletInfo(source);
        }

        @Override
        public WalletInfo[] newArray(int size) {
            return new WalletInfo[size];
        }
    };
}
