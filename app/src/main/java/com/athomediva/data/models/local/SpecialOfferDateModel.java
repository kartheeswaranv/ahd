package com.athomediva.data.models.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public class SpecialOfferDateModel implements Parcelable {
    private int type;
    private String hint;
    private String selectedDateTxt;
    private String apiParam;
    private long selectedDate;
    private boolean isDisabled;

    public String getApiParam() {
        return apiParam;
    }

    public void setApiParam(String apiParam) {
        this.apiParam = apiParam;
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public SpecialOfferDateModel(int type, String hint, String apiParam) {
        this.type = type;
        this.hint = hint;
        this.apiParam = apiParam;
    }

    public int getType() {
        return type;
    }

    public String getHint() {
        return hint;
    }

    public String getSelectedDateTxt() {
        return selectedDateTxt;
    }

    public long getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDateTxt(String selectedDateTxt) {
        this.selectedDateTxt = selectedDateTxt;
    }

    public void setSelectedDate(long selectedDate) {
        this.selectedDate = selectedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.hint);
        dest.writeString(this.selectedDateTxt);
        dest.writeString(this.apiParam);
        dest.writeLong(this.selectedDate);
        dest.writeByte(this.isDisabled ? (byte) 1 : (byte) 0);
    }

    protected SpecialOfferDateModel(Parcel in) {
        this.type = in.readInt();
        this.hint = in.readString();
        this.selectedDateTxt = in.readString();
        this.apiParam = in.readString();
        this.selectedDate = in.readLong();
        this.isDisabled = in.readByte() != 0;
    }

    public static final Creator<SpecialOfferDateModel> CREATOR = new Creator<SpecialOfferDateModel>() {
        @Override
        public SpecialOfferDateModel createFromParcel(Parcel source) {
            return new SpecialOfferDateModel(source);
        }

        @Override
        public SpecialOfferDateModel[] newArray(int size) {
            return new SpecialOfferDateModel[size];
        }
    };
}
