package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 14/10/16.
 */

public class Address implements Parcelable {
    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
    private String lon;
    private String address;
    private String label;
    private String circle;
    private String apartmentNo;
    private String access;
    private String addressID;
    private String lat;

    public Address() {
    }

    protected Address(Parcel in) {
        this.lon = in.readString();
        this.address = in.readString();
        this.label = in.readString();
        this.circle = in.readString();
        this.apartmentNo = in.readString();
        this.access = in.readString();
        this.addressID = in.readString();
        this.lat = in.readString();
    }

    public String getLon() {
        return lon;
    }

    public String getAddress() {
        return address;
    }

    public String getLabel() {
        return label;
    }

    public String getCircle() {
        return circle;
    }

    public String getApartmentNo() {
        return apartmentNo;
    }

    public String getAccess() {
        return access;
    }

    public String getAddressID() {
        return addressID;
    }

    public String getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return "ClassPojo [lon = "
          + lon
          + ", address = "
          + address
          + ", label = "
          + label
          + ", circle = "
          + circle
          + ", apartmentNo = "
          + apartmentNo
          + ", access = "
          + access
          + ", addressID = "
          + addressID
          + ", lat = "
          + lat
          + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lon);
        dest.writeString(this.address);
        dest.writeString(this.label);
        dest.writeString(this.circle);
        dest.writeString(this.apartmentNo);
        dest.writeString(this.access);
        dest.writeString(this.addressID);
        dest.writeString(this.lat);
    }
}