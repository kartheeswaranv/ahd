package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class OrderInfo implements Parcelable {
    private long orderId;

    public long getOrderId() {
        return orderId;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    private boolean isMandatory;

    private String rateServiceTitleText;

    public String getRateServiceTitle() {
        return rateServiceTitleText;
    }

    public OrderInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.orderId);
        dest.writeByte(this.isMandatory ? (byte) 1 : (byte) 0);
        dest.writeString(this.rateServiceTitleText);
    }

    protected OrderInfo(Parcel in) {
        this.orderId = in.readLong();
        this.isMandatory = in.readByte() != 0;
        this.rateServiceTitleText = in.readString();
    }

    public static final Creator<OrderInfo> CREATOR = new Creator<OrderInfo>() {
        @Override
        public OrderInfo createFromParcel(Parcel source) {
            return new OrderInfo(source);
        }

        @Override
        public OrderInfo[] newArray(int size) {
            return new OrderInfo[size];
        }
    };
}
