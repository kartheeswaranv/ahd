package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 24/05/17.
 */

public class ConfirmBookingResponse implements Parcelable {
    private String apiVersion;
    private ConfirmBookingError error;
    private boolean success;
    private ConfirmBookingData data;

    public String getApiVersion() {
        return apiVersion;
    }

    public ConfirmBookingError getError() {
        return error;
    }

    public boolean isSuccess() {
        return success;
    }

    public ConfirmBookingData getData() {
        return data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeParcelable(this.error, flags);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.data, flags);
    }

    public ConfirmBookingResponse() {
    }

    protected ConfirmBookingResponse(Parcel in) {
        this.apiVersion = in.readString();
        this.error = in.readParcelable(ConfirmBookingError.class.getClassLoader());
        this.success = in.readByte() != 0;
        this.data = in.readParcelable(ConfirmBookingData.class.getClassLoader());
    }

    public static final Creator<ConfirmBookingResponse> CREATOR = new Creator<ConfirmBookingResponse>() {
        @Override
        public ConfirmBookingResponse createFromParcel(Parcel source) {
            return new ConfirmBookingResponse(source);
        }

        @Override
        public ConfirmBookingResponse[] newArray(int size) {
            return new ConfirmBookingResponse[size];
        }
    };
}
