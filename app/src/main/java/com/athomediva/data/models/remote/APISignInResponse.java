package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 20/09/16.
 */
public class APISignInResponse {

    private String apiVersion;
    private Error error;
    private Data data;
    private boolean success;

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public boolean getSuccess() {
        return success;
    }

    @Override
    public String toString() {
        return "ClassPojo [apiVersion = "
          + apiVersion
          + ", error = "
          + error
          + ", data = "
          + data
          + ", success = "
          + success
          + "]";
    }

    public static class Data {
        private String message;
        private int userStatus;

        public String getMessage() {
            return message;
        }

        public int getUserStatus() {
            return userStatus;
        }
    }
}