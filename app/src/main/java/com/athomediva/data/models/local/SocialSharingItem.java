package com.athomediva.data.models.local;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by mohitkumar on 11/04/17.
 */

public class SocialSharingItem {
    private String title;
    private Intent launchIntent;
    private Drawable drawable;

    public SocialSharingItem(String title, Drawable drawable, Intent launchIntent) {
        this.title = title;
        this.drawable = drawable;
        this.launchIntent = launchIntent;
    }

    public Intent getLaunchIntent() {
        return launchIntent;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public String getTitle() {
        return title;
    }
}
