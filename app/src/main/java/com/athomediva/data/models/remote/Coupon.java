package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 11/05/17.
 */
public class Coupon implements Parcelable {
    private String couponCode;
    private double couponAmount;
    private String color;
    private String couponMsg;

    public String getCouponMsg() {
        return couponMsg;
    }

    public String getColor() {
        return color;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public double getCouponAmount() {
        return couponAmount;
    }

    public void reset() {
        couponCode = "";
        couponAmount = 0.0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.couponCode);
        dest.writeDouble(this.couponAmount);
        dest.writeString(this.color);
        dest.writeString(this.couponMsg);
    }

    public Coupon() {
    }

    protected Coupon(Parcel in) {
        this.couponCode = in.readString();
        this.couponAmount = in.readDouble();
        this.color = in.readString();
        this.couponMsg = in.readString();
    }

    public static final Creator<Coupon> CREATOR = new Creator<Coupon>() {
        @Override
        public Coupon createFromParcel(Parcel source) {
            return new Coupon(source);
        }

        @Override
        public Coupon[] newArray(int size) {
            return new Coupon[size];
        }
    };
}