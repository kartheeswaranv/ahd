package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 20/09/16.
 */
public class APIGenerateOtpResponse {
    private String apiVersion;
    private Error error;
    private Data data;
    private boolean success;

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public boolean getSuccess() {
        return success;
    }

    public static class Data {
        private String message;

        public String getMessage() {
            return message;
        }
    }
}