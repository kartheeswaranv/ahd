package com.athomediva.data.models.remote.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.athomediva.data.models.remote.Error;
import com.athomediva.data.models.remote.Location;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 30/05/17.
 */

public class BoundsResponse implements Serializable
{
    private String apiVersion;
    private Error error;
    private boolean success;

    @SerializedName("data")
    private ArrayList<Address> addressList;
    @SerializedName("loc_tag_id")
    private ArrayList<String> tagIdList;
    private int status;
    private String message;

    private ArrayList<Location> markers;
    private ArrayList<ArrayList<Location>> bounds;
    @SerializedName("c_name")
    private String circleName;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }


    public ArrayList<Address> getAddressList() {
        return addressList;
    }


    public ArrayList<String> getTagIdList() {
        return tagIdList;
    }


    public ArrayList<ArrayList<Location>> getBounds() {
        return bounds;
    }


    public String getCircleName() {
        return circleName;
    }


    public static class Address implements Parcelable {
        @SerializedName("user_address_id")
        public int 	addressID;
        @SerializedName("user_id")
        public int 	userId;
        public String  lat;
        @SerializedName("lng")
        public String  lon;
        public String  address;
        public String  label;
        @SerializedName("location_tag_id")
        public String tagId;
        public String suggestion;
        public String access;
        @SerializedName("c_name")
        public String circle;

        public int getAddressID() {
            return addressID;
        }


        public int getUserId() {
            return userId;
        }


        public String getLat() {
            return lat;
        }


        public String getLon() {
            return lon;
        }


        public String getAddress() {
            return address;
        }


        public String getLabel() {
            return label;
        }


        public String getTagId() {
            return tagId;
        }


        public String getSuggestion() {
            return suggestion;
        }


        public String getAccess() {
            return access;
        }


        public String getCircle() {
            return circle;
        }


        protected Address(Parcel in) {
            addressID = in.readInt();
            userId = in.readInt();
            lat = in.readString();
            lon = in.readString();
            address = in.readString();
            label = in.readString();
            tagId = in.readString();
            suggestion = in.readString();
            access = in.readString();
            circle = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(addressID);
            dest.writeInt(userId);
            dest.writeString(lat);
            dest.writeString(lon);
            dest.writeString(address);
            dest.writeString(label);
            dest.writeString(tagId);
            dest.writeString(suggestion);
            dest.writeString(access);
            dest.writeString(circle);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
            @Override
            public Address createFromParcel(Parcel in) {
                return new Address(in);
            }

            @Override
            public Address[] newArray(int size) {
                return new Address[size];
            }
        };

    }
}
