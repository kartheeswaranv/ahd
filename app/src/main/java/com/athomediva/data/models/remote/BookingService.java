package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class BookingService {
    private String name;
    private String duration;
    private int qty;
    private double price;

    public String getName() {
        return name;
    }

    public String getDuration() {
        return duration;
    }

    public int getQty() {
        return qty;
    }

    public double getPrice() {
        return price;
    }
}
