package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class AppUpdate {
    private String title;
    private boolean isMandatory;

    public long getNewAppVersion() {
        return newAppVersion;
    }

    private long newAppVersion;

    public boolean getIsMandatory() {
        return isMandatory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}