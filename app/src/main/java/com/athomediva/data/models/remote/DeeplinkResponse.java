package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.athomediva.data.DeeplinkConstants;

/**
 * Created by kartheeswaran on 21/02/17.
 */

public class DeeplinkResponse implements Parcelable {
    private String apiVersion;
    private boolean success;
    private Error error;
    private Data data;

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(apiVersion);
        dest.writeByte(success ? (byte) 1 : (byte) 0);
        dest.writeParcelable(error, flags);
        dest.writeParcelable(data, flags);
    }

    public DeeplinkResponse(Parcel source) {
        this.apiVersion = source.readString();
        this.success = source.readByte() != 0;
        this.error = source.readParcelable(Error.class.getClassLoader());
        this.data = source.readParcelable(Data.class.getClassLoader());
    }

    public DeeplinkResponse() {
    }

    public static final Creator<DeeplinkResponse> CREATOR = new Creator<DeeplinkResponse>() {
        public DeeplinkResponse createFromParcel(Parcel source) {
            return new DeeplinkResponse(source);
        }

        public DeeplinkResponse[] newArray(int size) {
            return new DeeplinkResponse[size];
        }
    };

    public static class Data implements Parcelable {
        private String payload;
        private int type;

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }

        public int getDeeplinkType() {
            return type;
        }

        public void setDeeplinkType(int deeplinkType) {
            this.type = deeplinkType;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(payload);
            dest.writeInt(type);
        }

        public Data(Parcel source) {
            this.payload = source.readString();
            this.type = source.readInt();
        }

        public Data() {
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public DeeplinkConstants.DEEPLINK_TYPE getType() {
            if (DeeplinkConstants.DEEPLINK_TYPE.values() != null) {
                for (DeeplinkConstants.DEEPLINK_TYPE t : DeeplinkConstants.DEEPLINK_TYPE.values()) {
                    if (t.getType() == type) {
                        return t;
                    }
                }
            }
            return DeeplinkConstants.DEEPLINK_TYPE.HOME_PAGE;
        }
    }
}
