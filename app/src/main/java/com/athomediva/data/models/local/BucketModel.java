package com.athomediva.data.models.local;

import com.athomediva.data.AppConstants;
import com.athomediva.data.models.remote.BucketInfo;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.Services;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * Created by kartheeswaran on 28/04/17.
 */

public class BucketModel {
    private String mBookingId;
    private long mFromDateTime;
    private long mToDateTime;
    private ArrayList<Services> mServiceList = new ArrayList<>();
    private String specialInst;

    // Currently Cash only option is availiable so that
    // payment type  is hardcoded here
    private int paymentType = 1;
    private boolean isConfirmed;
    //private int mOrderFlow;
    private boolean errorInBooking;
    private BucketInfo mBucketInfo;
    private boolean isExpand;
    private DateSlot mSelectedDate;

    private WeakHashMap<String, HashMap<String, Object>> mBookingStepsCompletedMap = new WeakHashMap<>();

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public int getOrderFlow() {
        return mBucketInfo.getOrderFlow();
    }

    public int getBucketId() {
        return mBucketInfo.getId();
    }

    public String getSpecialInst() {
        return specialInst;
    }

    public void setSpecialInst(String specialInst) {
        this.specialInst = specialInst;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public long getFromTime() {
        return mFromDateTime;
    }

    public void setFromTime(long time) {
        mFromDateTime = time;
    }

    public long getToTime() {
        return mToDateTime;
    }

    public void setToTime(long time) {
        mToDateTime = time;
    }

    public DateSlot getSelectedDate() {
        return mSelectedDate;
    }

    public void setSelectedDate(DateSlot slot) {
        mSelectedDate = slot;
    }

    public boolean isErrorInBooking() {
        return errorInBooking;
    }

    public void setErrorInBooking(boolean errorInBooking) {
        this.errorInBooking = errorInBooking;
    }

    public BucketInfo getBucketInfo() {
        return mBucketInfo;
    }

    public void setBucketInfo(BucketInfo mBucketInfo) {
        this.mBucketInfo = mBucketInfo;
    }

    public SelectedServicesList getSelectedServices() {
        if (mServiceList != null && mServiceList.size() > 0) {
            SelectedServicesList mSelectedServices = new SelectedServicesList();
            for (Services order : mServiceList) {
                mSelectedServices.add(new SelectedServiceReq(String.valueOf(order.getServiceId()), order.getQuantity()));
            }

            return mSelectedServices;
        }
        return null;
    }

    public String getBookingId() {
        return mBookingId;
    }

    public void setBookingId(String id) {
        mBookingId = id;
    }

    public ArrayList<Services> getOrderList() {
        return mServiceList;
    }

    public void setOrderList(ArrayList<Services> mOrderList) {
        this.mServiceList = mOrderList;
    }

    public int getGroupCount(int groupId) {
        int count = 0;
        /*if (mServiceList != null && mServiceList.size() > 0) {
            for (Services model : mServiceList) {
                if (model.getGroupId() == groupId) {
                    count = count + model.getQuantity();
                }
            }
        }*/

        return count;
    }

    public int getServiceQuantity(int serviceId) {
        int count = 0;
        if (mServiceList != null && mServiceList.size() > 0) {
            for (Services model : mServiceList) {
                if (model.getServiceId() == serviceId) {
                    count = model.getQuantity();
                    break;
                }
            }
        }

        return count;
    }

    /**
     * The method check for the existing request.
     * Returns true is there is already an existing request for that particular request.
     */
    public boolean isOrderStepRequestExist(AppConstants.BOOKING_ACTION step, HashMap<String, Object> currentReq) {
        if (mBookingStepsCompletedMap.containsKey(step.getAction())) {
            HashMap<String, Object> storedReq = mBookingStepsCompletedMap.get(step.getAction());

            //check for null and then check for keyset. If not same return false.
            if (storedReq == null || currentReq == null || !storedReq.keySet().equals(currentReq.keySet())) {
                return false;
            }

            boolean exists = true;
            //Check for all key one by one. If all matches returns true
            for (final String key : storedReq.keySet()) {
                if (key != null && storedReq.containsKey(key) && !(storedReq.get(key) == null
                  && currentReq.get(key) == null)) {
                    //enter here if both key is not null
                    //if either of the key is null then exists is false
                    if (storedReq.get(key) == null || currentReq.get(key) == null || !storedReq.get(key)
                      .equals(currentReq.get(key))) {
                        exists = false;
                    }
                }
            }

            return exists;
        } else {
            return false;
        }
    }

    /**
     * Add the request for the successfull booking step
     */
    public void addToOrderStep(AppConstants.BOOKING_ACTION step, HashMap<String, Object> requestValues) {
        mBookingStepsCompletedMap.put(step.getAction(), requestValues);
    }

    public void clearSession() {
        mBookingStepsCompletedMap.clear();
        mBookingId = null;
        specialInst = null;
        clearDateTime();
        clearServices();
    }

    public void clearDateTime() {
        mFromDateTime = 0;
        mToDateTime = 0;
        mSelectedDate = null;
    }

    public void clearServices() {
        if (mServiceList != null) mServiceList.clear();
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public int getTotalAmount() {
        int amt = 0;
        if (mServiceList != null && mServiceList.size() > 0) {
            for (Services model : mServiceList) {
                amt = amt + model.getQuantity() * UiUtils.getAmountFromTxt(String.valueOf(model.getPrice()));
            }
        }
        return amt;
    }

    public String getBucketName() {
        if (mBucketInfo != null) {
            return mBucketInfo.getTitle();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Bucket : " + mBucketInfo.getId() + "  Total Amt : " + getTotalAmount();
    }
}
