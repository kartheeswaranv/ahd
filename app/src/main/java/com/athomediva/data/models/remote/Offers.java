package com.athomediva.data.models.remote;

import android.os.Parcel;
import com.athomediva.data.models.local.IListItem;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class Offers implements IListItem {
    private String title;

    private String description;

    private String path;

    private String type;

    private String couponCode;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPath() {
        return path;
    }

    public String getType() {
        return type;
    }

    public String getCouponCode() {
        return couponCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.path);
        dest.writeString(this.type);
        dest.writeString(this.couponCode);
    }

    public Offers() {
    }

    protected Offers(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.path = in.readString();
        this.type = in.readString();
        this.couponCode = in.readString();
    }

    public static final Creator<Offers> CREATOR = new Creator<Offers>() {
        @Override
        public Offers createFromParcel(Parcel source) {
            return new Offers(source);
        }

        @Override
        public Offers[] newArray(int size) {
            return new Offers[size];
        }
    };

    @Override
    public String getName() {
        return title;
    }
}