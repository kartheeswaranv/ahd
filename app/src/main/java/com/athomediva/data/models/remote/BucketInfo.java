package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 28/04/17.
 */

public class BucketInfo {
    private String title;
    private String subTitle;
    private int id;
    private String info;
    private int orderFlow;
    private int minOrderAmount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getOrderFlow() {
        return orderFlow;
    }

    public void setOrderFlow(int orderFlow) {
        this.orderFlow = orderFlow;
    }

    public int getMinOrderAmt() {
        return minOrderAmount;
    }

    public void setMinOrderAmt(int minOrderAmt) {
        this.minOrderAmount = minOrderAmt;
    }
}