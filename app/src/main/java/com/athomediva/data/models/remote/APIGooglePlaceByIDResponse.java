package com.athomediva.data.models.remote;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by mohitkumar on 27/04/17.
 */

public class APIGooglePlaceByIDResponse {
    private String status;
    private Data result;

    public Data getResult() {
        return result;
    }

    public String getStatus() {
        return status;
    }

    public static class Data {
        private List<AddressComponents> address_components;
        private String formatted_address;
        private Geometry geometry;

        public String getFormatted_address() {
            return formatted_address;
        }

        public Geometry getGeometry() {
            return geometry;
        }

        public List<AddressComponents> getAddressComponents() {
            return address_components;
        }
    }

    public static class AddressComponents {
        @SerializedName("long_name") private String longName;

        @SerializedName("short_name") private String shortName;

        private List<String> types;

        public String getLongName() {
            return longName;
        }

        public String getShortName() {
            return shortName;
        }

        public List<String> getTypes() {
            return types;
        }
    }
}
