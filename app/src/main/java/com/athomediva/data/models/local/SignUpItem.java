package com.athomediva.data.models.local;

/**
 * Created by mohitkumar on 13/04/17.
 */

public class SignUpItem {
    private Entity<String> mobileNumber;
    private Entity<String> name;
    private Entity<String> email;
    private Entity<String> referralCode;
    private Entity<String> genderEntity;

    public SignUpItem() {
        mobileNumber = new Entity<>();
        name = new Entity<>();
        email = new Entity<>();
        referralCode = new Entity<>();
        genderEntity = new Entity<>("");
    }

    public Entity<String> getGenderEntity() {
        return genderEntity;
    }

    public void setGenderEntity(Entity<String> gender) {
        genderEntity = gender;
    }

    public Entity<String> getMobileNumberEntity() {
        return mobileNumber;
    }

    public void setMobileNumberEntity(Entity<String> mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Entity<String> getNameEntity() {
        return name;
    }

    public void setNameEntity(Entity<String> name) {
        this.name = name;
    }

    public Entity<String> getEmailEntity() {
        return email;
    }

    public void setEmailEntity(Entity<String> email) {
        this.email = email;
    }

    public Entity<String> getReferralCodeEntity() {
        return referralCode;
    }

    public void setReferralCodeEntity(Entity<String> referralCode) {
        this.referralCode = referralCode;
    }
}
