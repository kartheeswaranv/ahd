package com.athomediva.data.models.remote;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by kartheeswaran on 18/05/17.
 */

public class PaymentInfo implements Serializable {
    private String title;
    private String amt;
    private String key;
    private int sortBy;
    private String color;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getSortOrder() {
        return sortBy;
    }

    public String getColor() {
        return color;
    }

    public static class PaymentInfoComparator implements Comparator<PaymentInfo> {
        @Override
        public int compare(PaymentInfo t1, PaymentInfo t2) {
            if (t1 != null && t2 != null) {
                if (t1.sortBy == t2.sortBy) {
                    return 0;
                } else if (t1.sortBy > t2.sortBy) {
                    return 1;
                } else {
                    return -1;
                }
            }
            return 0;
        }
    }
}
