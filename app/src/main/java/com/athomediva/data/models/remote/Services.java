package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class Services implements Parcelable {

    private long duration;

    private double price;

    private long serviceId;

    private int maxQty;

    private double priceExcludeTax;

    private String durationText;

    private String name;

    private double strikeOffPrice;

    private String info;

    private ArrayList<String> infoList;

    private String icon;

    private int bucketId;

    private int quantity;

    private String groupName;

    private String saveText;

    private boolean freeService;

    public boolean isFreeServices() {
        return freeService;
    }

    public String getSaveText() {
        return saveText;
    }

    public long getDuration() {
        return duration;
    }

    public double getPrice() {
        return price;
    }

    public long getServiceId() {
        return serviceId;
    }

    public int getMaxQty() {
        return maxQty;
    }

    public double getPriceExcludeTax() {
        return priceExcludeTax;
    }

    public String getDurationText() {
        return durationText;
    }

    public String getName() {
        return name;
    }

    public double getStrikeOffPrice() {
        return strikeOffPrice;
    }

    public String getInfo() {
        return info;
    }

    public ArrayList<String> getInfoList() {
        return infoList;
    }

    public String getIcon() {
        return icon;
    }

    public int getBucketId() {
        return bucketId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int count) {
        this.quantity = count;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Services() {
    }

    public Services(long serviceId) {
        this.serviceId = serviceId;
    }

    public Services(String name, String duration, int qty, double amt) {
        this.name = name;
        this.durationText = duration;
        this.quantity = qty;
        this.price = amt;
    }

    @Override
    public boolean equals(Object obj) {
        Services serv = (Services) obj;
        if (serv != null && serv instanceof Services) {
            return serv.getServiceId() == serviceId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) serviceId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.duration);
        dest.writeDouble(this.price);
        dest.writeLong(this.serviceId);
        dest.writeInt(this.maxQty);
        dest.writeDouble(this.priceExcludeTax);
        dest.writeString(this.durationText);
        dest.writeString(this.name);
        dest.writeDouble(this.strikeOffPrice);
        dest.writeString(this.info);
        dest.writeStringList(this.infoList);
        dest.writeString(this.icon);
        dest.writeInt(this.bucketId);
        dest.writeInt(this.quantity);
        dest.writeString(this.groupName);
        dest.writeString(this.saveText);
        dest.writeByte(this.freeService ? (byte) 1 : (byte) 0);
    }

    protected Services(Parcel in) {
        this.duration = in.readLong();
        this.price = in.readDouble();
        this.serviceId = in.readLong();
        this.maxQty = in.readInt();
        this.priceExcludeTax = in.readDouble();
        this.durationText = in.readString();
        this.name = in.readString();
        this.strikeOffPrice = in.readDouble();
        this.info = in.readString();
        this.infoList = in.createStringArrayList();
        this.icon = in.readString();
        this.bucketId = in.readInt();
        this.quantity = in.readInt();
        this.groupName = in.readString();
        this.saveText = in.readString();
        this.freeService = in.readByte() != 0;
    }

    public static final Creator<Services> CREATOR = new Creator<Services>() {
        @Override
        public Services createFromParcel(Parcel source) {
            return new Services(source);
        }

        @Override
        public Services[] newArray(int size) {
            return new Services[size];
        }
    };
}