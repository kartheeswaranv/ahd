package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class CartData implements Parcelable {
    public static final Creator<CartData> CREATOR = new Creator<CartData>() {
        @Override
        public CartData createFromParcel(Parcel source) {
            return new CartData(source);
        }

        @Override
        public CartData[] newArray(int size) {
            return new CartData[size];
        }
    };
    private String cartId;
    private ArrayList<CartBucketItem> buckets;
    private CartMetaInfo meta;
    private Address address;
    private Coupon coupon;
    private WalletDetails walletDetails;
    @SerializedName("totalPayable") private String total;
    private String subTotal;
    private String couponErrorMsg;
    private MinimumOrderError minOrderInfo;
    private boolean pendingStatus;
    private double paymentPending;
    private String pendingMsg;
    private String priceChange;

    public CartData() {
    }

    protected CartData(Parcel in) {
        this.cartId = in.readString();
        this.buckets = in.createTypedArrayList(CartBucketItem.CREATOR);
        this.meta = in.readParcelable(CartMetaInfo.class.getClassLoader());
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.coupon = in.readParcelable(Coupon.class.getClassLoader());
        this.walletDetails = in.readParcelable(WalletDetails.class.getClassLoader());
        this.total = in.readString();
        this.subTotal = in.readString();
        this.couponErrorMsg = in.readString();
        this.minOrderInfo = in.readParcelable(MinimumOrderError.class.getClassLoader());
        this.pendingStatus = in.readByte() != 0;
        this.paymentPending = in.readDouble();
        this.pendingMsg = in.readString();
        this.priceChange = in.readString();
    }

    public String getPriceChange() {
        return priceChange;
    }

    public boolean isPendingStatus() {
        return pendingStatus;
    }

    public double getPaymentPending() {
        return paymentPending;
    }

    public String getPendingMsg() {
        return pendingMsg;
    }

    public MinimumOrderError getMinOrderInfo() {
        return minOrderInfo;
    }

    public String getCouponErrorMsg() {
        return couponErrorMsg;
    }

    public String getTotal() {
        return total;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public WalletDetails getWallet() {
        return walletDetails;
    }

    public String getCartId() {
        return cartId;
    }

    public ArrayList<CartBucketItem> getBuckets() {
        return buckets;
    }

    public CartMetaInfo getMeta() {
        return meta;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cartId);
        dest.writeTypedList(this.buckets);
        dest.writeParcelable(this.meta, flags);
        dest.writeParcelable(this.address, flags);
        dest.writeParcelable(this.coupon, flags);
        dest.writeParcelable(this.walletDetails, flags);
        dest.writeString(this.total);
        dest.writeString(this.subTotal);
        dest.writeString(this.couponErrorMsg);
        dest.writeParcelable(this.minOrderInfo, flags);
        dest.writeByte(this.pendingStatus ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.paymentPending);
        dest.writeString(this.pendingMsg);
        dest.writeString(this.priceChange);
    }
}
