package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 22/03/17.
 */

public class APIDateTimeSlotResponse implements Parcelable {
    private String apiVersion;
    private boolean success;
    private Error error;
    private DateTimeSlotData data;

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isSuccess() {
        return success;
    }

    public DateTimeSlotData getData() {
        return data;
    }

    public Error getError() {
        return error;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(apiVersion);
        parcel.writeByte(success ? (byte) 1 : (byte) 0);
        parcel.writeParcelable(data, i);
        parcel.writeParcelable(error, i);
    }

    protected APIDateTimeSlotResponse(Parcel in) {

        this.apiVersion = in.readString();
        this.success = in.readByte() != 0;
        this.data = in.readParcelable(DateTimeSlotData.class.getClassLoader());
        this.error = in.readParcelable(Error.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<APIDateTimeSlotResponse> CREATOR = new Creator<APIDateTimeSlotResponse>() {
        public APIDateTimeSlotResponse createFromParcel(Parcel source) {
            return new APIDateTimeSlotResponse(source);
        }

        public APIDateTimeSlotResponse[] newArray(int size) {
            return new APIDateTimeSlotResponse[size];
        }
    };
}
