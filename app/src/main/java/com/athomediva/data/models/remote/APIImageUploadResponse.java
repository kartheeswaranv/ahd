package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class APIImageUploadResponse {
    private boolean success;
    private Error error;
    private Data data;

    public boolean isSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        int status;
        String message;
        String image;

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public String getImage() {
            return image;
        }
    }
}
