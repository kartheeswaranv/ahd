package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 19/06/17.
 */

public class MinOrderErrorInfo {
    private String errorTitle;
    private String errorMsg;
    private double amount;

    public String getTitle() {
        return errorTitle;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public double getAmount() {
        return amount;
    }


}
