package com.athomediva.data.models.remote;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 05/04/17.
 */

public class CategoryResponse {
    private ArrayList<Categories> categories;

    public ArrayList<Categories> getCategories() {
        return categories;
    }
}
