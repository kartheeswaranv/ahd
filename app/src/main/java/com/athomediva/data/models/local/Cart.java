package com.athomediva.data.models.local;

import android.content.Context;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.CartData;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.MinimumOrderError;
import java.util.HashMap;

/**
 * Created by mohitkumar on 20/06/17.
 */

public class Cart {

    private Context mContext;
    private String mCartID;
    private String mCircleName;
    private MinimumOrderError mMinimumOrderError;
    private int mServiceCount;
    private double mAmount;
    private HashMap<Long, Integer> mServicesMap = new HashMap<>();
    private String latitude;
    private String longitude;

    public Cart(Context context, String circleName) {
        mContext = context;
        mCircleName = circleName;
    }

    public Cart(Context context, String circleName, String lat, String lng) {
        mContext = context;
        latitude = lat;
        mCircleName = circleName;
        longitude = lng;
    }

    public String getCartID() {
        return mCartID;
    }

    public String getCircleName() {
        return mCircleName;
    }

    public void resetCart() {
        mCartID = null;
        mServiceCount = 0;
        mMinimumOrderError = null;
        mServicesMap.clear();
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double mAmount) {
        this.mAmount = mAmount;
    }

    public int getServiceCount() {
        return mServiceCount;
    }

    public MinimumOrderError getMinimumOrderError() {
        return mMinimumOrderError;
    }

    public BookingRequestData updateServices(String action, long serviceId) {
        BookingRequestData data = new BookingRequestData();
        if (TextUtils.isEmpty(mCartID)) {
            data.setAction(AppConstants.CART_ACTION.CREATE_CART.getAction());
            data.setCircle(mCircleName);
            data.setLatitude(latitude);
            data.setLongitude(longitude);
        } else {
            data.setAction(action);
            data.setCartId(mCartID);
        }
        data.setServiceId(serviceId);

        return data;
    }

    public BookingRequestData updateAddress(String action, String addressId) {
        BookingRequestData data = new BookingRequestData();

        data.setAction(action);
        data.setCartId(mCartID);
        data.setAddressId(addressId);

        return data;
    }

    public BookingRequestData updateTimeSlot(String action, String bookingId, DateSlot timeSlot) {
        BookingRequestData data = new BookingRequestData();

        data.setAction(action);
        data.setCartId(mCartID);
        data.setBookingId(bookingId);
        SelectedTimeSlot slot = new SelectedTimeSlot(timeSlot.getDate(), timeSlot.getSelectedTimeSlot().getFrom(),
          timeSlot.getSelectedTimeSlot().getTo(), timeSlot.getSelectedTimeSlot().getDisplayTime(),
          timeSlot.getSelectedTimeSlot().getCircle());
        data.setScheduleTime(slot);

        return data;
    }

    public BookingRequestData updateCoupon(String action, String couponCode) {
        BookingRequestData data = new BookingRequestData();

        data.setAction(action);
        data.setCartId(mCartID);
        data.setCouponCode(couponCode);

        return data;
    }

    public BookingRequestData removeCoupon(String action) {
        BookingRequestData data = new BookingRequestData();
        data.setAction(action);
        data.setCartId(mCartID);
        return data;
    }

    public void updateCartData(APICartResponse response) {
        if (response != null && response.getData() != null) {
            updateCartData(response.getData());
        }
    }

    private void updateCartData(CartData data) {
        if (data != null) {
            mCartID = data.getCartId();
            if (data.getAddress() != null && !TextUtils.isEmpty(data.getAddress().getCircle())) {
                mCircleName = data.getAddress().getCircle();
                latitude = data.getAddress().getLat();
                longitude = data.getAddress().getLon();
            }

            mMinimumOrderError = data.getMinOrderInfo();

            if (data.getBuckets() != null) {
                updateAllCartServices(CoreLogic.getServicesMap(data.getBuckets()));
                mServiceCount = CoreLogic.getServicesCountInCart(data.getBuckets());
                mAmount = CoreLogic.getTotalAmount(data.getBuckets());
            } else {
                mServicesMap.clear();
                mServiceCount = 0;
                mAmount = 0;
            }
        }
    }

    public void updateServices(long serviceId, int count) {
        mServicesMap.put(serviceId, count);
    }

    public HashMap<Long, Integer> getServicesMap() {
        return mServicesMap;
    }

    public void updateAllCartServices(HashMap<Long, Integer> map) {
        if (map != null && !map.isEmpty()) {
            mServicesMap.clear();
            mServicesMap.putAll(map);
        } else {
            mServicesMap.clear();
        }
    }

    public void clearCartDetails() {
        mServicesMap.clear();
        mServiceCount = 0;
        mAmount = 0;
        mMinimumOrderError = null;
    }
}
