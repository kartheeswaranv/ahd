package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 15/06/17.
 */

public class MyBookingResponse implements Parcelable {
    private String apiVersion;
    private boolean success;
    private Error error;
    private Data data;

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public static class Data implements Parcelable {
        ArrayList<Booking> past;
        ArrayList<Booking> upcoming;
        ArrayList<Booking> recent;

        public ArrayList<Booking> getRecent() {
            return recent;
        }

        public ArrayList<Booking> getPast() {
            return past;
        }

        public ArrayList<Booking> getUpcoming() {
            return upcoming;
        }

        public Data() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.past);
            dest.writeTypedList(this.upcoming);
            dest.writeTypedList(this.recent);
        }

        protected Data(Parcel in) {
            this.past = in.createTypedArrayList(Booking.CREATOR);
            this.upcoming = in.createTypedArrayList(Booking.CREATOR);
            this.recent = in.createTypedArrayList(Booking.CREATOR);
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.data, flags);
    }

    public MyBookingResponse() {
    }

    protected MyBookingResponse(Parcel in) {
        this.apiVersion = in.readString();
        this.success = in.readByte() != 0;
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.data = in.readParcelable(Data.class.getClassLoader());
    }

    public static final Parcelable.Creator<MyBookingResponse> CREATOR = new Parcelable.Creator<MyBookingResponse>() {
        @Override
        public MyBookingResponse createFromParcel(Parcel source) {
            return new MyBookingResponse(source);
        }

        @Override
        public MyBookingResponse[] newArray(int size) {
            return new MyBookingResponse[size];
        }
    };
}
