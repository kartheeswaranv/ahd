package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 29/06/17.
 */

public class OffersItem {

    private String path;
    private String type;
    private String couponCode;
    private String title;
    private String description;

    public String getPath() {
        return path;
    }

    public String getType() {
        return type;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
