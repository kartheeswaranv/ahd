package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by kartheeswaran on 15/06/17.
 */

public class BookingServiceGroup {
    private String catName;
    private ArrayList<BookingService> list;

    public String getCat_name() {
        return catName;
    }

    public ArrayList<BookingService> getList() {
        return list;
    }
}
