package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class SubCategories implements Parcelable {
    private long id;
    private String icon;
    private String url;
    private ArrayList<Services> services;

    public long getId() {
        return id;
    }

    public ArrayList<Services> getServices() {
        return services;
    }

    private String name;

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.icon);
        dest.writeString(this.url);
        dest.writeList(this.services);
        dest.writeString(this.name);
    }

    public SubCategories() {
    }

    protected SubCategories(Parcel in) {
        this.id = in.readLong();
        this.icon = in.readString();
        this.url = in.readString();
        this.services = new ArrayList<Services>();
        in.readList(this.services, Services.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Parcelable.Creator<SubCategories> CREATOR = new Parcelable.Creator<SubCategories>() {
        @Override
        public SubCategories createFromParcel(Parcel source) {
            return new SubCategories(source);
        }

        @Override
        public SubCategories[] newArray(int size) {
            return new SubCategories[size];
        }
    };
}