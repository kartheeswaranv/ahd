package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 27/06/17.
 */

public class PackageModel implements Parcelable {
    private String icon;
    private ArrayList<Services> list;

    public String getIcon() {
        return icon;
    }

    public ArrayList<Services> getList() {
        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.icon);
        dest.writeTypedList(this.list);
    }

    public PackageModel() {
    }

    protected PackageModel(Parcel in) {
        this.icon = in.readString();
        this.list = in.createTypedArrayList(Services.CREATOR);
    }

    public static final Parcelable.Creator<PackageModel> CREATOR = new Parcelable.Creator<PackageModel>() {
        @Override
        public PackageModel createFromParcel(Parcel source) {
            return new PackageModel(source);
        }

        @Override
        public PackageModel[] newArray(int size) {
            return new PackageModel[size];
        }
    };
}
