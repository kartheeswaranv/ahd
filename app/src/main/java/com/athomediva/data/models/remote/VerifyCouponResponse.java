package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 11/05/17.
 */

public class VerifyCouponResponse {
    private boolean success;
    private Coupon data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public Coupon getData() {
        return data;
    }

    public Error getError() {
        return error;
    }

    public void setData(Coupon data) {
        this.data = data;
    }
}
