package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingBookingData implements Parcelable {
    private String bookingId;
    private String completedOn;

    public String getBookingId() {
        return bookingId;
    }

    public String getCompletedOn() {
        return completedOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bookingId);
        dest.writeString(this.completedOn);
    }

    public PaymentPendingBookingData() {
    }

    protected PaymentPendingBookingData(Parcel in) {
        this.bookingId = in.readString();
        this.completedOn = in.readString();
    }

    public static final Parcelable.Creator<PaymentPendingBookingData> CREATOR =
      new Parcelable.Creator<PaymentPendingBookingData>() {
          @Override
          public PaymentPendingBookingData createFromParcel(Parcel source) {
              return new PaymentPendingBookingData(source);
          }

          @Override
          public PaymentPendingBookingData[] newArray(int size) {
              return new PaymentPendingBookingData[size];
          }
      };
}
