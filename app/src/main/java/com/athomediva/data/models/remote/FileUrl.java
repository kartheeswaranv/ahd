package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class FileUrl {
    private String original;
    private String lg;
    private String md;
    private String xs;
    private String sm;

    public String getOriginal() {
        return original;
    }

    public String getLg() {
        return lg;
    }

    public String getMd() {
        return md;
    }

    public String getXs() {
        return xs;
    }

    public String getSm() {
        return sm;
    }
}
