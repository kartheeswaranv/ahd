package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 10/06/17.
 */

public class RatingsModel {
    private String title;
    private String ratingKey;
    private float rating;

    private boolean optional;
    private int maximumRating;

    public RatingsModel(String title, String key, boolean optional, int maxRating) {
        this.title = title;
        this.ratingKey = key;
        this.optional = optional;
        this.maximumRating = maxRating;
    }

    public String getTitle() {
        return title;
    }

    public String getRatingKey() {
        return ratingKey;
    }

    public float getRating() {
        return rating;
    }

    public boolean isOptional() {
        return optional;
    }

    public int getMaximumRating() {
        return maximumRating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
