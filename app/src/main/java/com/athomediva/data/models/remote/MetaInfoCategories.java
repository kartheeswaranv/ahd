package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class MetaInfoCategories {

    private LastAddress lastAddress;
    private String circleName;
    private String latitude;
    private String longitude;

    public LastAddress getLastAddress() {
        return lastAddress;
    }

    public String getCircleName() {
        return circleName;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
