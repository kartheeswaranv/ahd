package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class DescriptionInfo implements Parcelable {
    private String desc;
    private String icon;

    public String getDesc() {
        return desc;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.desc);
        dest.writeString(this.icon);
    }

    public DescriptionInfo() {
    }

    protected DescriptionInfo(Parcel in) {
        this.desc = in.readString();
        this.icon = in.readString();
    }

    public static final Parcelable.Creator<DescriptionInfo> CREATOR = new Parcelable.Creator<DescriptionInfo>() {
        @Override
        public DescriptionInfo createFromParcel(Parcel source) {
            return new DescriptionInfo(source);
        }

        @Override
        public DescriptionInfo[] newArray(int size) {
            return new DescriptionInfo[size];
        }
    };
}
