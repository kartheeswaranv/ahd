package com.athomediva.data.models.remote;

import com.athomediva.data.models.remote.Services;
import java.util.ArrayList;

public class DeeplinkData {

    private String categoryID;
    private boolean isPackage;
    private ArrayList<Services> services;
    private String cartId;

    public boolean isPackage() {
        return isPackage;
    }

    public String getCartId() {
        return cartId;
    }

    public String getCategoryId() {
        return categoryID;
    }

    public void setCategoryId(String categoryId) {
        this.categoryID = categoryId;
    }

    public ArrayList<Services> getServices() {
        return services;
    }
    public void setServices(ArrayList<Services> services) {
        this.services = services;
    }
}
