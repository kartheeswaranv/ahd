package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 14/10/16.
 */

public class Referral implements Parcelable {
    public static final Parcelable.Creator<Referral> CREATOR = new Parcelable.Creator<Referral>() {
        @Override
        public Referral createFromParcel(Parcel source) {
            return new Referral(source);
        }

        @Override
        public Referral[] newArray(int size) {
            return new Referral[size];
        }
    };
    private String ifReferal;

    public void setIfReferal(String ifReferal) {
        this.ifReferal = ifReferal;
    }

    public void setIfTitle(String ifTitle) {
        this.ifTitle = ifTitle;
    }

    public void setIfSubject(String ifSubject) {
        this.ifSubject = ifSubject;
    }

    public void setIfDesc(String ifDesc) {
        this.ifDesc = ifDesc;
    }

    public void setIfRefUrl(String ifRefUrl) {
        this.ifRefUrl = ifRefUrl;
    }

    public void setIfContent(String ifContent) {
        this.ifContent = ifContent;
    }

    private String ifTitle;
    private String ifSubject;
    private String ifDesc;
    private String ifRefUrl;
    private String ifContent;

    public Referral() {
    }

    protected Referral(Parcel in) {
        this.ifReferal = in.readString();
        this.ifTitle = in.readString();
        this.ifSubject = in.readString();
        this.ifDesc = in.readString();
        this.ifRefUrl = in.readString();
        this.ifContent = in.readString();
    }

    public String getIfReferal() {
        return ifReferal;
    }

    public String getIfTitle() {
        return ifTitle;
    }

    public String getIfSubject() {
        return ifSubject;
    }

    public String getIfDesc() {
        return ifDesc;
    }

    public String getIfRefUrl() {
        return ifRefUrl;
    }

    public String getIfContent() {
        return ifContent;
    }

    @Override
    public String toString() {
        return "ClassPojo [ifReferal = "
          + ifReferal
          + ", ifTitle = "
          + ifTitle
          + ", ifSubject = "
          + ifSubject
          + ", ifDesc = "
          + ifDesc
          + ", ifRefUrl = "
          + ifRefUrl
          + ", ifContent = "
          + ifContent
          + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ifReferal);
        dest.writeString(this.ifTitle);
        dest.writeString(this.ifSubject);
        dest.writeString(this.ifDesc);
        dest.writeString(this.ifRefUrl);
        dest.writeString(this.ifContent);
    }
}
