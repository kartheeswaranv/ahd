package com.athomediva.data.models.local;

/**
 * Created by mohitkumar on 09/03/17.
 */

public class Entity<T> {
    private T data;
    private boolean isValid = true;
    private String errorMessage = "";

    public Entity(T data) {
        this.data = data;
    }

    public Entity() {

    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        errorMessage = (valid ? "" : errorMessage);
        isValid = valid;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
