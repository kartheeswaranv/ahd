package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 27/04/17.
 */

public class StructuredFormatting {
    private String main_text;
    private String secondary_text;

    public String getMainText() {
        return main_text;
    }

    public String getSecondaryText() {
        return secondary_text;
    }
}
