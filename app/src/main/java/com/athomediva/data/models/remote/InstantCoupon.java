package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 15/06/18.
 */

public class InstantCoupon implements Parcelable {
    public static final Creator<InstantCoupon> CREATOR = new Creator<InstantCoupon>() {
        @Override
        public InstantCoupon createFromParcel(Parcel source) {
            return new InstantCoupon(source);
        }

        @Override
        public InstantCoupon[] newArray(int size) {
            return new InstantCoupon[size];
        }
    };
    private int percentage;
    private double amount;
    private String couponCode;
    private String validityMsg;

    public InstantCoupon() {
    }

    protected InstantCoupon(Parcel in) {
        this.percentage = in.readInt();
        this.amount = in.readDouble();
        this.couponCode = in.readString();
        this.validityMsg = in.readString();
    }

    public String getValidityMsg() {
        return validityMsg;
    }

    public int getPercentage() {
        return percentage;
    }

    public double getAmount() {
        return amount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.percentage);
        dest.writeDouble(this.amount);
        dest.writeString(this.couponCode);
        dest.writeString(this.validityMsg);
    }
}
