package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingData implements Parcelable {
    @SerializedName("responseHeader") private String headerInfo;
    @SerializedName("responseFooter") private String footerInfo;
    @SerializedName("PPAmount") private double pendingAmt;
    private String paymentUrl;
    private String verifyPaymentUrl;

    public String getVerifyPaymentUrl() {
        return verifyPaymentUrl;
    }

    private ArrayList<PaymentPendingBookingDetails> list;

    public String getHeaderInfo() {
        return headerInfo;
    }

    public String getFooterInfo() {
        return footerInfo;
    }

    public double getPendingAmt() {
        return pendingAmt;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public ArrayList<PaymentPendingBookingDetails> getList() {
        return list;
    }

    public PaymentPendingData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.headerInfo);
        dest.writeString(this.footerInfo);
        dest.writeDouble(this.pendingAmt);
        dest.writeString(this.paymentUrl);
        dest.writeString(this.verifyPaymentUrl);
        dest.writeTypedList(this.list);
    }

    protected PaymentPendingData(Parcel in) {
        this.headerInfo = in.readString();
        this.footerInfo = in.readString();
        this.pendingAmt = in.readDouble();
        this.paymentUrl = in.readString();
        this.verifyPaymentUrl = in.readString();
        this.list = in.createTypedArrayList(PaymentPendingBookingDetails.CREATOR);
    }

    public static final Creator<PaymentPendingData> CREATOR = new Creator<PaymentPendingData>() {
        @Override
        public PaymentPendingData createFromParcel(Parcel source) {
            return new PaymentPendingData(source);
        }

        @Override
        public PaymentPendingData[] newArray(int size) {
            return new PaymentPendingData[size];
        }
    };
}
