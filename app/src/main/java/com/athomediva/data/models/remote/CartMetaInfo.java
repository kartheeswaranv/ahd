package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class CartMetaInfo implements Parcelable {
    private String headerDescription;
    private CartHeaderDescription headerFullDescription;

    public String getHeaderDescription() {
        return headerDescription;
    }

    public CartHeaderDescription getHeaderFullDescription() {
        return headerFullDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.headerDescription);
        dest.writeParcelable(this.headerFullDescription, flags);
    }

    public CartMetaInfo() {
    }

    protected CartMetaInfo(Parcel in) {
        this.headerDescription = in.readString();
        this.headerFullDescription = in.readParcelable(CartHeaderDescription.class.getClassLoader());
    }

    public static final Parcelable.Creator<CartMetaInfo> CREATOR = new Parcelable.Creator<CartMetaInfo>() {
        @Override
        public CartMetaInfo createFromParcel(Parcel source) {
            return new CartMetaInfo(source);
        }

        @Override
        public CartMetaInfo[] newArray(int size) {
            return new CartMetaInfo[size];
        }
    };
}
