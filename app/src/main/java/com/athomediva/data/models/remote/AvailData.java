package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 31/01/18.
 */

public class AvailData {
    private String confirmedDate;
    private String confirmedBy;

    public String getConfirmedDate() {
        return confirmedDate;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }
}
