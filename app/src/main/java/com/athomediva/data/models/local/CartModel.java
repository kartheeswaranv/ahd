package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 19/04/17.
 */

public class CartModel {
    private int count;
    private double amount;

    public CartModel(int count, double amt) {
        this.count = count;
        this.amount = amt;
    }

    public int getCount() {
        return count;
    }

    public double getAmount() {
        return amount;
    }
}
