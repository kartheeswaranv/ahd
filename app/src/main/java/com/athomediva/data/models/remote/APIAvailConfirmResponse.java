package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 31/01/18.
 */

public class APIAvailConfirmResponse {
    private String apiVersion;
    private boolean success;
    private Data data;

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        private String orderId;
        private AvailData availability;

        public String getOrderId() {
            return orderId;
        }

        public AvailData getAvailability() {
            return availability;
        }
    }
}
