package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 15/06/17.
 */

public class BookingDetails implements Parcelable {

    public static final Creator<BookingDetails> CREATOR = new Creator<BookingDetails>() {
        @Override
        public BookingDetails createFromParcel(Parcel source) {
            return new BookingDetails(source);
        }

        @Override
        public BookingDetails[] newArray(int size) {
            return new BookingDetails[size];
        }
    };
    private String apiVersion;
    private boolean success;
    private Error error;
    private BookingDetailsData data;

    public BookingDetails() {
    }

    protected BookingDetails(Parcel in) {
        this.apiVersion = in.readString();
        this.success = in.readByte() != 0;
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.data = in.readParcelable(BookingDetailsData.class.getClassLoader());
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }

    public BookingDetailsData getData() {
        return data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiVersion);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.data, flags);
    }

    public static class BookingDetailsData implements Parcelable {
        public static final Creator<BookingDetailsData> CREATOR = new Creator<BookingDetailsData>() {
            @Override
            public BookingDetailsData createFromParcel(Parcel source) {
                return new BookingDetailsData(source);
            }

            @Override
            public BookingDetailsData[] newArray(int size) {
                return new BookingDetailsData[size];
            }
        };
        @SerializedName("buttons") BookingDetailsBtns configBtns;
        @SerializedName("lat") private String latitude;
        @SerializedName("long") private String longitude;
        @SerializedName("orderMsg") private String specReqMsg;
        @SerializedName("scheduledDate") private String orderDate;
        @SerializedName("orderStatus") private String orderDisplayStatus;
        @SerializedName("toRate") private boolean isRated;
        @SerializedName("serviceTime") private String duration;
        @SerializedName("cancelAppDisplay") private boolean cancelEnable;
        @SerializedName("displayCallBar") private boolean callEnable;
        @SerializedName("totals") private ArrayList<PaymentInfo> paymentInfoList;
        @SerializedName("services") private ArrayList<BookingServiceGroup> serviceGroupList;
        private String statusColor;
        private String bucketName;
        private String paymentType;
        @SerializedName("specialReq") private boolean specialReqEnable;
        @SerializedName("orderId") private String orderIdTxt;
        @SerializedName("id") private String bookingId;
        private String paymentLink;
        private int progressSteps;
        private String authCode;
        @SerializedName("pMobile") private String customerMobile;
        private String unverifyMsg;
        private String rateServiceTitleText;
        private String verifyPaymentUrl;
        private float rating;
        private Address address;
        private String cancelPenaltyMsg;

        public BookingDetailsData() {
        }

        protected BookingDetailsData(Parcel in) {
            this.configBtns = in.readParcelable(BookingDetailsBtns.class.getClassLoader());
            this.latitude = in.readString();
            this.longitude = in.readString();
            this.specReqMsg = in.readString();
            this.orderDate = in.readString();
            this.orderDisplayStatus = in.readString();
            this.isRated = in.readByte() != 0;
            this.duration = in.readString();
            this.cancelEnable = in.readByte() != 0;
            this.callEnable = in.readByte() != 0;
            this.paymentInfoList = new ArrayList<PaymentInfo>();
            in.readList(this.paymentInfoList, PaymentInfo.class.getClassLoader());
            this.serviceGroupList = new ArrayList<BookingServiceGroup>();
            in.readList(this.serviceGroupList, BookingServiceGroup.class.getClassLoader());
            this.statusColor = in.readString();
            this.bucketName = in.readString();
            this.paymentType = in.readString();
            this.specialReqEnable = in.readByte() != 0;
            this.orderIdTxt = in.readString();
            this.bookingId = in.readString();
            this.paymentLink = in.readString();
            this.progressSteps = in.readInt();
            this.authCode = in.readString();
            this.customerMobile = in.readString();
            this.unverifyMsg = in.readString();
            this.rateServiceTitleText = in.readString();
            this.verifyPaymentUrl = in.readString();
            this.rating = in.readFloat();
            this.address = in.readParcelable(Address.class.getClassLoader());
            this.cancelPenaltyMsg = in.readString();
        }

        public String getCancelPenaltyMsg() {
            return cancelPenaltyMsg;
        }

        public float getRating() {
            return rating;
        }

        public String getVerifyPaymentUrl() {
            return verifyPaymentUrl;
        }

        public String getRateServiceTitle() {
            return rateServiceTitleText;
        }

        public String getUnverifyMsg() {
            return unverifyMsg;
        }

        public boolean isSpecialReqEnable() {
            return specialReqEnable;
        }

        public String getOrderIdTxt() {
            return orderIdTxt;
        }

        public String getBookingId() {
            return bookingId;
        }

        public String getCustomerMobile() {
            return customerMobile;
        }

        public String getStatusColor() {
            return statusColor;
        }

        public String getBucketName() {
            return bucketName;
        }

        public String getPaymentLink() {
            return paymentLink;
        }

        public int getProgressSteps() {
            return progressSteps;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public String getSpecReqMsg() {
            return specReqMsg;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public String getOrderDisplayStatus() {
            return orderDisplayStatus;
        }

        public boolean isRated() {
            return isRated;
        }

        public String getAuthCode() {
            return authCode;
        }

        public String getDuration() {
            return duration;
        }

        public Address getAddress() {
            return address;
        }

        public boolean isCancelEnable() {
            return cancelEnable;
        }

        public boolean isCallEnable() {
            return callEnable;
        }

        public ArrayList<PaymentInfo> getPaymentInfoList() {
            return paymentInfoList;
        }

        public ArrayList<BookingServiceGroup> getServiceGroupList() {
            return serviceGroupList;
        }

        public BookingDetailsBtns getConfigBtns() {
            return configBtns;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.configBtns, flags);
            dest.writeString(this.latitude);
            dest.writeString(this.longitude);
            dest.writeString(this.specReqMsg);
            dest.writeString(this.orderDate);
            dest.writeString(this.orderDisplayStatus);
            dest.writeByte(this.isRated ? (byte) 1 : (byte) 0);
            dest.writeString(this.duration);
            dest.writeByte(this.cancelEnable ? (byte) 1 : (byte) 0);
            dest.writeByte(this.callEnable ? (byte) 1 : (byte) 0);
            dest.writeList(this.paymentInfoList);
            dest.writeList(this.serviceGroupList);
            dest.writeString(this.statusColor);
            dest.writeString(this.bucketName);
            dest.writeString(this.paymentType);
            dest.writeByte(this.specialReqEnable ? (byte) 1 : (byte) 0);
            dest.writeString(this.orderIdTxt);
            dest.writeString(this.bookingId);
            dest.writeString(this.paymentLink);
            dest.writeInt(this.progressSteps);
            dest.writeString(this.authCode);
            dest.writeString(this.customerMobile);
            dest.writeString(this.unverifyMsg);
            dest.writeString(this.rateServiceTitleText);
            dest.writeString(this.verifyPaymentUrl);
            dest.writeFloat(this.rating);
            dest.writeParcelable(this.address, flags);
            dest.writeString(this.cancelPenaltyMsg);
        }
    }
}
