package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class ImageUploadReponse {

    private String status;
    private String filename;
    private long filesize;
    private long statusCode;
    private FileUrl fileurl;

    public String getStatus() {
        return status;
    }

    public String getFilename() {
        return filename;
    }

    public long getFilesize() {
        return filesize;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public FileUrl getFileurl() {
        return fileurl;
    }
}
