package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class CreateOrderError implements Parcelable {
    private String message;
    private long code;
    private PaymentPendingData data;

    public String getMessage() {
        return message;
    }

    public long getCode() {
        return code;
    }

    public PaymentPendingData getData() {
        return data;
    }

    public CreateOrderError() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeLong(this.code);
        dest.writeParcelable(this.data, flags);
    }

    protected CreateOrderError(Parcel in) {
        this.message = in.readString();
        this.code = in.readLong();
        this.data = in.readParcelable(PaymentPendingData.class.getClassLoader());
    }

    public static final Creator<CreateOrderError> CREATOR = new Creator<CreateOrderError>() {
        @Override
        public CreateOrderError createFromParcel(Parcel source) {
            return new CreateOrderError(source);
        }

        @Override
        public CreateOrderError[] newArray(int size) {
            return new CreateOrderError[size];
        }
    };
}
