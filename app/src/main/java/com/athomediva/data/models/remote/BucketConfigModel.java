package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by kartheeswaran on 28/04/17.
 */

public class BucketConfigModel {
    // Order type is SORT_BY_CATEGORY or SORT_BY_AMOUNT
    private int orderType;
    private ArrayList<BucketInfo> buckets;

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public ArrayList<BucketInfo> getBuckets() {
        return buckets;
    }

    public void setBuckets(ArrayList<BucketInfo> buckets) {
        this.buckets = buckets;
    }
}

