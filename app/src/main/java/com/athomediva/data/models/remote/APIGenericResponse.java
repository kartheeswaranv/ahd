package com.athomediva.data.models.remote;

/**
 * Created by mohitkumar on 06/06/17.
 */

public class APIGenericResponse {

    private boolean success;
    private Error error;
    private Data data;
    public boolean isSuccess() {
        return success;
    }
    public Error getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        int status;
        int s_status;
        String message;

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public int getSStatus() {
            return s_status;
        }
    }

}
