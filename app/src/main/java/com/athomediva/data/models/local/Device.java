package com.athomediva.data.models.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 19/09/16.
 */
public class Device implements Parcelable {
    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel source) {
            return new Device(source);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
    private String gcmId;
    private String versionCode;
    private String versionName;
    private String screenDimension;
    private String deviceName;
    private String deviceUid;

    public Device() {
    }

    protected Device(Parcel in) {
        this.gcmId = in.readString();
        this.versionCode = in.readString();
        this.versionName = in.readString();
        this.screenDimension = in.readString();
        this.deviceName = in.readString();
        this.deviceUid = in.readString();
    }

    public String getDeviceUid() {
        return deviceUid;
    }

    public void setDeviceUid(String deviceUid) {
        this.deviceUid = deviceUid;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getScreenDimension() {
        return screenDimension;
    }

    public void setScreenDimension(String screenDimension) {
        this.screenDimension = screenDimension;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.gcmId);
        dest.writeString(this.versionCode);
        dest.writeString(this.versionName);
        dest.writeString(this.screenDimension);
        dest.writeString(this.deviceName);
        dest.writeString(this.deviceUid);
    }
}
