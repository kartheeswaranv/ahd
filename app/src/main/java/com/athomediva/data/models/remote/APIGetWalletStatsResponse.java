package com.athomediva.data.models.remote;

import java.util.ArrayList;

public class APIGetWalletStatsResponse {

    private boolean success;
    private String apiVersion;
    private Error error;
    private WalletDetails data;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public WalletDetails getData() {
        return data;
    }

    public void setData(WalletDetails data) {
        this.data = data;
    }

    public class Data {
        private ArrayList<WalletInfo> wallet;
        private double total;

        public ArrayList<WalletInfo> getWallet() {
            return wallet;
        }

        public double getTotalWalletAmt() {
            return total;
        }
    }
}
