package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 20/09/16.
 */
public class Error implements Parcelable {
    public static final Creator<Error> CREATOR = new Creator<Error>() {
        public Error createFromParcel(Parcel source) {
            return new Error(source);
        }

        public Error[] newArray(int size) {
            return new Error[size];
        }
    };
    private String message;
    private long code;

    public Error(Parcel source) {
        code = source.readLong();
        message = source.readString();
    }

    public String getMessage() {
        return message;
    }

    public long getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", code = " + code + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(code);
        dest.writeString(message);
    }
}