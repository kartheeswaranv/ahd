package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class Testimonials implements Parcelable {
    private String title;

    private String desc;

    private String address;

    private String img;

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getAddress() {
        return address;
    }

    public String getImg() {
        return img;
    }

    public String getUser() {
        return user;
    }

    private String user;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.desc);
        dest.writeString(this.address);
        dest.writeString(this.img);
        dest.writeString(this.user);
    }

    public Testimonials() {
    }

    protected Testimonials(Parcel in) {
        this.title = in.readString();
        this.desc = in.readString();
        this.address = in.readString();
        this.img = in.readString();
        this.user = in.readString();
    }

    public static final Parcelable.Creator<Testimonials> CREATOR = new Parcelable.Creator<Testimonials>() {
        @Override
        public Testimonials createFromParcel(Parcel source) {
            return new Testimonials(source);
        }

        @Override
        public Testimonials[] newArray(int size) {
            return new Testimonials[size];
        }
    };
}
