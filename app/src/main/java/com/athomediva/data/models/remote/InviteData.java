package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/**
 * Created by kartheeswaran on 22/09/17.
 */
public class InviteData implements Parcelable {
    private List<InvitedContact> invitedList;

    public List<InvitedContact> getInviteList() {
        return invitedList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.invitedList);
    }

    public InviteData() {
    }

    protected InviteData(Parcel in) {
        this.invitedList = in.createTypedArrayList(InvitedContact.CREATOR);
    }

    public static final Parcelable.Creator<InviteData> CREATOR = new Parcelable.Creator<InviteData>() {
        @Override
        public InviteData createFromParcel(Parcel source) {
            return new InviteData(source);
        }

        @Override
        public InviteData[] newArray(int size) {
            return new InviteData[size];
        }
    };
}
