package com.athomediva.data.models.local;

import android.view.View;

/**
 * Created by kartheeswaran on 05/06/17.
 */

public class InfoDialogModel {
    private String title;
    private String message;
    private View customView;

    public InfoDialogModel(String title, String msg, View customView) {
        this.title = title;
        this.message = msg;
        this.customView = customView;
    }

    public InfoDialogModel(String title, View customView) {
        this.title = title;
        this.customView = customView;
    }


    public InfoDialogModel(String title, String msg) {
        this.title = title;
        this.message = msg;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }


    public View getCustomView() {
        return customView;
    }
}
