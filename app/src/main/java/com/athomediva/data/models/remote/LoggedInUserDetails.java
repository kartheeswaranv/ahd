package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 21/09/16.
 */
public class LoggedInUserDetails implements Parcelable {
    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setReferral(Referral referral) {
        this.referral = referral;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    private String name;
    private String gender;
    private String mobile;
    private String image;
    private Referral referral;
    private String dob;
    private String anniversary;

    public LoggedInUserDetails() {
    }

    public String getEmail() {
        return email;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getMobile() {
        return mobile;
    }

    public Referral getReferral() {
        return referral;
    }

    public String getDob() {
        return dob;
    }

    public String getAnniversary() {
        return anniversary;
    }

    @Override
    public String toString() {
        return "ClassPojo [email = " + email + ", name = " + name + ", gender = " + gender + ", mobile = " + mobile + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.name);
        dest.writeString(this.gender);
        dest.writeString(this.mobile);
        dest.writeString(this.image);
        dest.writeParcelable(this.referral, flags);
        dest.writeString(this.dob);
        dest.writeString(this.anniversary);
    }

    protected LoggedInUserDetails(Parcel in) {
        this.email = in.readString();
        this.name = in.readString();
        this.gender = in.readString();
        this.mobile = in.readString();
        this.image = in.readString();
        this.referral = in.readParcelable(Referral.class.getClassLoader());
        this.dob = in.readString();
        this.anniversary = in.readString();
    }

    public static final Creator<LoggedInUserDetails> CREATOR = new Creator<LoggedInUserDetails>() {
        @Override
        public LoggedInUserDetails createFromParcel(Parcel source) {
            return new LoggedInUserDetails(source);
        }

        @Override
        public LoggedInUserDetails[] newArray(int size) {
            return new LoggedInUserDetails[size];
        }
    };
}
