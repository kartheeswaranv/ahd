package com.athomediva.data.models.local;

/**
 * Created by kartheeswaran on 15/06/17.
 */

public class SelectedTimeSlot {

    private long date;
    private long from;
    private long to;
    private String slot;
    private String circle;

    public SelectedTimeSlot(long selectedDate, long selectFrom, long selectTo, String slot, String circle) {
        this.date = selectedDate;
        this.from = selectFrom;
        this.to = selectTo;
        this.slot = slot;
        this.circle = circle;
    }

    public String getCircle() {
        return circle;
    }

    public long getDate() {
        return date;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public String getSlot() {
        return slot;
    }
}
