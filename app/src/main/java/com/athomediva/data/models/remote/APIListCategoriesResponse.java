package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by mohitkumar on 01/06/17.
 */

public class APIListCategoriesResponse {
    private Data data;
    private boolean success;

    public Error getError() {
        return error;
    }

    private Error error;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    public static class Data {
        private PackageModel packages;
        private ArrayList<Categories> categories;
        private ArrayList<SubCategories> feedbackCatg;
        private MetaInfoCategories meta;

        public PackageModel getPackages() {
            return packages;
        }

        public ArrayList<Categories> getCategories() {
            return categories;
        }

        public MetaInfoCategories getMeta() {
            return meta;
        }

        public ArrayList<SubCategories> getFeedbackCatg() {
            return feedbackCatg;
        }
    }
}
