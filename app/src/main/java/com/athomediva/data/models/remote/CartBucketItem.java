package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 12/06/17.
 */

public class CartBucketItem implements Parcelable {
    private String title;
    private String bookingId;
    private int servicesCount;
    private double total;
    private ArrayList<CartServiceGroup> serviceGroupList;
    private String scheduledDateTime;
    private boolean isExpanded;
    private TimeSlot slot;

    public TimeSlot getTimeSlot() {
        return slot;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getTitle() {
        return title;
    }

    public String getBookingId() {
        return bookingId;
    }

    public int getServiceCount() {
        return servicesCount;
    }

    public double getTotal() {
        return total;
    }

    public ArrayList<CartServiceGroup> getServicesGroupList() {
        return serviceGroupList;
    }

    public String getScheduledDateTime() {
        return scheduledDateTime;
    }

    public CartBucketItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.bookingId);
        dest.writeInt(this.servicesCount);
        dest.writeDouble(this.total);
        dest.writeTypedList(this.serviceGroupList);
        dest.writeString(this.scheduledDateTime);
        dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.slot, flags);
    }

    protected CartBucketItem(Parcel in) {
        this.title = in.readString();
        this.bookingId = in.readString();
        this.servicesCount = in.readInt();
        this.total = in.readDouble();
        this.serviceGroupList = in.createTypedArrayList(CartServiceGroup.CREATOR);
        this.scheduledDateTime = in.readString();
        this.isExpanded = in.readByte() != 0;
        this.slot = in.readParcelable(TimeSlot.class.getClassLoader());
    }

    public static final Creator<CartBucketItem> CREATOR = new Creator<CartBucketItem>() {
        @Override
        public CartBucketItem createFromParcel(Parcel source) {
            return new CartBucketItem(source);
        }

        @Override
        public CartBucketItem[] newArray(int size) {
            return new CartBucketItem[size];
        }
    };
}
