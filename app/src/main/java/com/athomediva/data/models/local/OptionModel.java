package com.athomediva.data.models.local;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kartheeswaran on 28/05/17.
 */

public class OptionModel implements Parcelable {
    @SerializedName("cancel_option") private String title;
    private boolean isSelected;
    private boolean isEditable;
    @SerializedName("option_id") private String id;
    private String text;

    public OptionModel(String id,String str, boolean isSelect, boolean isEdit) {
        this.id = id;
        title = str;
        isSelected = isSelect;
        isEditable = isEdit;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isEditable ? (byte) 1 : (byte) 0);
        dest.writeString(this.id);
        dest.writeString(this.text);
    }

    protected OptionModel(Parcel in) {
        this.title = in.readString();
        this.isSelected = in.readByte() != 0;
        this.isEditable = in.readByte() != 0;
        this.id = in.readString();
        this.text = in.readString();
    }

    public static final Creator<OptionModel> CREATOR = new Creator<OptionModel>() {
        @Override
        public OptionModel createFromParcel(Parcel source) {
            return new OptionModel(source);
        }

        @Override
        public OptionModel[] newArray(int size) {
            return new OptionModel[size];
        }
    };
}

