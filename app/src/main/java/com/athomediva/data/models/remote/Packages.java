package com.athomediva.data.models.remote;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class Packages implements Parcelable {

    public static final Creator<Packages> CREATOR = new Creator<Packages>() {
        @Override
        public Packages createFromParcel(Parcel source) {
            return new Packages(source);
        }

        @Override
        public Packages[] newArray(int size) {
            return new Packages[size];
        }
    };

    public Packages() {
    }

    protected Packages(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
