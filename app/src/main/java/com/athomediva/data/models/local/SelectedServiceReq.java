package com.athomediva.data.models.local;

import android.text.TextUtils;

/**
 * Created by kartheeswaran on 28/04/17.
 */

public class SelectedServiceReq {

    private String serviceId;
    private int quantity;

    public SelectedServiceReq(String sId, int qty) {
        this.serviceId = sId;
        this.quantity = qty;
    }

    @Override
    public boolean equals(Object o) {
        SelectedServiceReq req = (SelectedServiceReq) o;

        if (req != null) {
            return TextUtils.equals(serviceId, req.serviceId) && quantity == req.quantity;
        }
        return super.equals(o);
    }
}
