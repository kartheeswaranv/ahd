package com.athomediva.data.models.remote;

/**
 * Created by kartheeswaran on 05/09/17.
 */

public class InviteRequest {
    private String mobile;
    private int status;

    public InviteRequest(String mobile, int status) {
        this.mobile = mobile;
        this.status = status;
    }

    public String getMobile() {
        return mobile;
    }

    public int getStatus() {
        return status;
    }
}
