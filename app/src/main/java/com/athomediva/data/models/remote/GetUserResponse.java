package com.athomediva.data.models.remote;

import java.util.ArrayList;

/**
 * Created by mohitkumar on 14/10/16.
 */

public class GetUserResponse {
    private String apiVersion;
    private Data data;
    private Error error;
    private boolean success;

    public String getApiVersion() {
        return apiVersion;
    }

    public Data getData() {
        return data;
    }

    public boolean getSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }

    @Override
    public String toString() {
        return "ClassPojo [apiVersion = " + apiVersion + ", data = " + data + ", success = " + success + "]";
    }

    public static class Data {
        private String reviews;

        private ArrayList<Address> address;

        private LoggedInUserDetails userDetails;

        public ArrayList<Address> getAddress() {
            return address;
        }

        public LoggedInUserDetails getUserDetails() {
            return userDetails;
        }

        public String getReviews() {
            return reviews;
        }

        @Override
        public String toString() {
            return "ClassPojo [reviews = " + reviews + ", address = " + address + ", userDetails = " + userDetails + "]";
        }
    }
}