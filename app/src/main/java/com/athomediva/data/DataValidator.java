package com.athomediva.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.models.local.AddAddressItem;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartServiceGroup;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.session.UserSession;
import com.athomediva.logger.LogUtils;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 19/04/17.
 */

public class DataValidator {
    private static final String TAG = LogUtils.makeLogTag(DataValidator.class.getSimpleName());

    public static boolean isPhoneNumberValid(String phoneNumber) {
        return !(phoneNumber == null || phoneNumber.trim().length() < 10 || !phoneNumber.substring(0, 1).matches("[7-9]"));
    }

    public static boolean validateCustomerSignUpDetails(Context context, SignUpItem info) {
        boolean flag = true;

        boolean isValid = isPhoneNumberValid(info.getMobileNumberEntity().getData());
        info.getMobileNumberEntity().setValid(isValid);

        if (!isValid) {
            info.getMobileNumberEntity().setErrorMessage(context.getString(R.string.validation_mobile_number));
        }
        flag = flag && isValid;
        isValid = !TextUtils.isEmpty(info.getNameEntity().getData());
        info.getNameEntity().setValid(isValid);
        if (!isValid) {
            info.getNameEntity().setErrorMessage(context.getString(R.string.validation_name));
        }
        flag = flag && isValid;

        isValid = isValidEmail(info.getEmailEntity().getData());
        info.getEmailEntity().setValid(isValid);

        if (!isValid) {
            info.getEmailEntity().setErrorMessage(context.getString(R.string.validation_email));
        }
        flag = flag && isValid;
        isValid = !TextUtils.isEmpty(info.getGenderEntity().getData());
        info.getGenderEntity().setValid(isValid);
        if (!isValid) {
            info.getGenderEntity().setErrorMessage(context.getString(R.string.validation_gender));
        }
        flag = flag && isValid;
        return flag;
    }

    private static boolean isValidEmail(String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isUserSessionExist(@NonNull UserSession userSession) {
        return !TextUtils.isEmpty(userSession.getUserToken());
    }

    public static boolean validateAddAddressDetails(Context context, AddAddressItem info) {
        boolean flag = true;

        boolean isValid = !TextUtils.isEmpty(info.getBuildingEntity().getData());
        flag = flag && isValid;
        LOGD(TAG, "validateAddAddressDetails: info.getBuildingEntity() =" + isValid);
        info.getBuildingEntity().setValid(isValid);
        if (!isValid) {
            info.getBuildingEntity().setErrorMessage(context.getString(R.string.building_validation_msg));
        }

        isValid = !TextUtils.isEmpty(info.getLandmarkEntity().getData());
        flag = flag && isValid;
        LOGD(TAG, "validateAddAddressDetails: info.getLandmarkEntity() =" + isValid);
        info.getLandmarkEntity().setValid(isValid);
        if (!isValid) {
            info.getLandmarkEntity().setErrorMessage(context.getString(R.string.landmark_validation_msg));
        }

        isValid = !TextUtils.isEmpty(info.getAddressTypeEntity().getData());
        flag = flag && isValid;
        LOGD(TAG, "validateAddAddressDetails: info.getAddressTypeEntity() =" + isValid);
        info.getAddressTypeEntity().setValid(isValid);
        if (!isValid) {
            info.getAddressTypeEntity().setErrorMessage(context.getString(R.string.choose_address_msg));
        }

        //reset custom address entity and set it below
        info.getCustomAddressEntity().setValid(true);
        if (info.getAddressTypeEntity().getData() != null && info.getAddressTypeEntity()
          .getData()
          .equals(context.getString(R.string.address_custom))) {
            isValid = !TextUtils.isEmpty(info.getCustomAddressEntity().getData());
            flag = flag && isValid;
            LOGD(TAG, "validateAddAddressDetails: info.getCustomAddressEntity() =" + isValid);
            info.getCustomAddressEntity().setValid(isValid);
            if (!isValid) {
                info.getCustomAddressEntity().setErrorMessage(context.getString(R.string.custom_address_name));
            }
        }

        isValid = !TextUtils.isEmpty(info.getFullAddressEntity().getData().getAddress())
          && info.getFullAddressEntity().getData().getLocation() != null
          && info.getFullAddressEntity().getData().getLocation().latitude != 0
          && info.getFullAddressEntity().getData().getLocation().longitude != 0;
        flag = flag && isValid;
        LOGD(TAG, "validateAddAddressDetails: info.getFullAddressEntity =" + isValid);
        info.getFullAddressEntity().setValid(isValid);
        if (!isValid) {
            info.getFullAddressEntity().setErrorMessage(context.getString(R.string.enter_address_msg));
        }

        return flag;
    }

    //TODO understand
    public static boolean isCoordinatesInsideBound(LatLng currentLocation, List<LatLng> lngArrayList) {
        if (currentLocation == null) return false;

        if (lngArrayList == null || lngArrayList.size() < 3) return true;
        LatLng lastPoint = lngArrayList.get(lngArrayList.size() - 1);
        boolean isInside = false;
        double x = currentLocation.longitude;

        for (LatLng point : lngArrayList) {
            double x1 = lastPoint.longitude;
            double x2 = point.longitude;
            double dx = x2 - x1;

            if (Math.abs(dx) > 180.0) {
                // we have, most likely, just jumped the dateline (could do further validation to this effect if needed).  normalise the numbers.
                if (x > 0) {
                    while (x1 < 0) x1 += 360;
                    while (x2 < 0) x2 += 360;
                } else {
                    while (x1 > 0) x1 -= 360;
                    while (x2 > 0) x2 -= 360;
                }
                dx = x2 - x1;
            }

            if ((x1 <= x && x2 > x) || (x1 >= x && x2 < x)) {
                double grad = (point.latitude - lastPoint.latitude) / dx;
                double intersectAtLat = lastPoint.latitude + ((x - x1) * grad);

                if (intersectAtLat > currentLocation.latitude) isInside = !isInside;
            }
            lastPoint = point;
        }

        return isInside;
    }

    public static boolean isConsumerUpdated(Consumer consumer, Consumer editedConsumer) {
        LogUtils.LOGD(TAG, "isConsumerUpdated : ");
        if (consumer != null
          && editedConsumer != null
          && TextUtils.equals(consumer.getUserName(), editedConsumer.getUserName())
          && TextUtils.equals(consumer.getUserNumber(), editedConsumer.getUserNumber())
          && TextUtils.equals(consumer.getGender(), editedConsumer.getGender())
          && TextUtils.equals(consumer.getEmailID(), editedConsumer.getEmailID())) {
            return false;
        }

        return true;
    }

    public static boolean isServicesAdded(APICartResponse response, Services service) {
        LogUtils.LOGD(TAG, "isServicesAdded : ");
        if (response != null
          && response.isSuccess()
          && response.getData() != null
          && response.getData().getBuckets() != null) {
            for (CartBucketItem bucketItem : response.getData().getBuckets()) {
                if (hasServiceInBucket(bucketItem, service)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasServiceInBucket(CartBucketItem item, Services service) {
        LogUtils.LOGD(TAG, "hasServiceInBucket : ");
        if (item != null && item.getServicesGroupList() != null && !item.getServicesGroupList().isEmpty()) {
            for (CartServiceGroup groupItem : item.getServicesGroupList()) {
                if (hasServiceInGroup(groupItem, service)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean hasServiceInGroup(CartServiceGroup group, Services serv) {
        LogUtils.LOGD(TAG, "hasServiceInGroup : ");
        if (group != null && group.getServices() != null && !group.getServices().isEmpty()) {
            for (Services services : group.getServices()) {
                if (services.getServiceId() == serv.getServiceId()) {
                    serv.setQuantity(services.getQuantity());
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean hasChangesInSpecialDates(ArrayList<SpecialOfferDateModel> list) {
        LogUtils.LOGD(TAG, "hasChangesInSpecialDates : ");
        if (list != null && !list.isEmpty()) {
            for (SpecialOfferDateModel model : list) {
                if (!model.isDisabled() && !TextUtils.isEmpty(model.getSelectedDateTxt())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasAverageRating(List<RatingsModel> list) {
        if (list != null && list.size() > 0) {
            float rating = 0;
            for (RatingsModel model : list) {
                rating += model.getRating();
            }
            float avg = rating / list.size();
            if (avg >= AppConstants.SERVICE_AVG_RATING_ENABLE_PLAYSTORE) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSpecialDatesUpdated(LoggedInUserDetails details) {
        if (details != null && (TextUtils.isEmpty(details.getDob()) && TextUtils.isEmpty(details.getAnniversary()))) {
            return false;
        }
        return true;
    }
}
