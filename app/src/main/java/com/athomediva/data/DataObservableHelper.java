package com.athomediva.data;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.RequiresPermission;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.helper.DataHelper;
import com.athomediva.helper.LocationRequest;
import com.athomediva.logger.LogUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import rx.Single;
import rx.schedulers.Schedulers;

import static com.athomediva.data.AppConstants.PINCODE_PREFIX;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 26/04/17.
 */

public class DataObservableHelper {
    private static final String TAG = LogUtils.makeLogTag(DataObservableHelper.class.getSimpleName());

    /**
     * Return the observable to perform the operation.
     */

    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    public static Single<Geolocation> getCurrentAddressObservable(Context context) {

        return Single.create(singleSubscriber -> {
            LocationRequest locationRequest = new LocationRequest(context);
            LOGD(TAG, "getCurrentAddressObservable: thread name =" + Thread.currentThread());
            locationRequest.getLocation(new LocationRequest.LocationResponseListener() {
                @Override
                public void updateLocation(android.location.Location location) {
                    LOGD(TAG, "updateLocation: " + Thread.currentThread());
                    if (location != null) {

                        getAddressFromLocation(context, location.getLatitude(), location.getLongitude()).subscribeOn(
                          Schedulers.io()).subscribe(address -> {
                            Geolocation geolocation = new Geolocation(location.getLatitude(), location.getLongitude());
                            String pincode = DataHelper.getPinCodeFromAddress(address);
                            geolocation.setPinCode(pincode);
                            address = DataHelper.clearAddressPinCodePrefix(address, pincode);
                            geolocation.setAddress(address);

                            singleSubscriber.onSuccess(geolocation);
                        }, error -> singleSubscriber.onError(error));
                    } else {
                        singleSubscriber.onError(new Throwable("Request Failed"));
                    }
                    locationRequest.stopLocation();
                }

                @Override
                public void onError(LocationRequest.ERROR_CODE errorCode) {
                    LOGD(TAG, "onError: " + errorCode);
                    singleSubscriber.onError(new Throwable(errorCode.toString()));
                    locationRequest.stopLocation();
                }
            });
        });
    }

    public static Single<String> getAddressFromLocation(Context context, double latitude, double longitude) {
        LOGD(TAG, "getAddressFromLocation: latitude =" + latitude + " longitude =" + longitude);
        return Single.create(singleSubscriber -> singleSubscriber.onSuccess(getAddress(context, latitude, longitude)));
    }

    @WorkerThread
    private static String getAddress(Context context, double latitude, double longitude) {
        LOGD(TAG, "getAddress: " + Thread.currentThread());
        String fullAddress = null;
        try {
            List<Address> addressList;
            if (Geocoder.isPresent()) {
                LOGD(TAG, "getAddress: geocoder present");
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                addressList = geocoder.getFromLocation(latitude, longitude,
                  1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } else {
                LOGD(TAG, "getAddress: geocoder not present");
                addressList = getStringFromLocation(latitude, longitude);
            }

            if (addressList == null || addressList.size() == 0) {
                LOGE(TAG, "execute: addressList is null");
                return null;
            }

            Address addressItem = addressList.get(0);
            LOGD(TAG, "execute: original address from api = " + addressItem);
            String address = addressItem.getAddressLine(0);
            String city = addressItem.getLocality();
            //String state = addressItem.getAdminArea();
            if (addressItem.getMaxAddressLineIndex() == 0) {// When geo coder is not present this case gets hit
                fullAddress = addressItem.getAddressLine(0);
            } else {
                fullAddress = address + ", " + city;
            }
            String prefix = PINCODE_PREFIX + addressList.get(0).getPostalCode();
            if (!TextUtils.isEmpty(fullAddress)) fullAddress = fullAddress + prefix;
        } catch (JSONException | IOException e) {
            LOGE(TAG, "execute: Exception" + e.getLocalizedMessage());
        }

        LOGD(TAG, "execute: filtered address = " + fullAddress);
        return fullAddress;
    }

    private static List<Address> getStringFromLocation(double lat, double lng) throws IOException, JSONException {
        LOGD(TAG, "getStringFromLocation: for map api");
        String address = String.format(Locale.ENGLISH,
          "http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$f,%2$f&sensor=true&language=" + Locale.getDefault()
            .getCountry(), lat, lng);

        LOGD(TAG, "getStringFromLocation: url formed = " + address);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(address).build();

        Response response = client.newCall(request).execute();
        JSONObject jsonObject = new JSONObject(response.body().string());

        LOGD(TAG, "getStringFromLocation: jsonObject = " + jsonObject);
        List<Address> retList = new ArrayList<>();

        if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                JSONObject result = results.getJSONObject(i);
                String indiStr = result.getString("formatted_address");
                Address addr = new Address(Locale.getDefault());
                addr.setAddressLine(i, indiStr);
                retList.add(addr);
            }
        }

        return retList;
    }
}
