package com.athomediva.data;

import android.support.v7.util.DiffUtil;
import com.athomediva.data.models.remote.Services;
import java.util.List;

/**
 * Created by kartheeswaran on 20/06/17.
 */

public class ServicesDiffUtils extends DiffUtil.Callback{

    private final List<Services> oldList;
    private final List<Services> newList;

    public ServicesDiffUtils(List<Services> oldList, List<Services> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getServiceId() == newList.get(newItemPosition).getServiceId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Services oldItem = oldList.get(oldItemPosition);
        final Services newItem = newList.get(newItemPosition);

        return oldItem.getQuantity() == newItem.getQuantity();
    }

    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
