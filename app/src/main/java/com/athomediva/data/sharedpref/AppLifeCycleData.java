package com.athomediva.data.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mohitkumar on 17/10/16.
 */

public class AppLifeCycleData extends SaveData {
    private static final String PREF_NAME = "ahd_booking_session";
    private static volatile AppLifeCycleData sInstance;

    private AppLifeCycleData(SharedPreferences sf) {
        super(sf);
    }

    public static AppLifeCycleData getInstance(Context context) {
        if (sInstance == null) {
            synchronized (AppLifeCycleData.class) {
                if (sInstance == null) {
                    sInstance = new AppLifeCycleData(context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE));
                }
            }
        }
        return sInstance;
    }
}
