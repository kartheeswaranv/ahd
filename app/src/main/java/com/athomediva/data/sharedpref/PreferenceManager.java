package com.athomediva.data.sharedpref;

import android.support.annotation.NonNull;
import com.athomediva.data.models.remote.LoggedInUserDetails;

public interface PreferenceManager {

    void saveInstanceId();

    String getSessionToken();

    void setSessionToken(String token);

    void setLoggedInUserDetails(@NonNull LoggedInUserDetails details);

    LoggedInUserDetails getUserDetails();

    boolean isUserLoggedIn();

    void clearUserData();

    void setValue(@NonNull SESSION key, int value);

    void setValue(@NonNull SESSION key, long value);

    void setValue(@NonNull SESSION key, String value);

    void setValue(@NonNull SESSION key, boolean value);

    int getIntValue(@NonNull SESSION key);

    long getLongValue(@NonNull SESSION key);

    String getStringValue(@NonNull SESSION key);

    boolean getBooleanValue(@NonNull SESSION key);

    enum SessionContext {
        USER_SESSION_CONTEXT,
        APP_LIFECYCLE_CONTEXT,
    }

    enum SESSION {
        KEY_TOKEN("key_user_session_token", SessionContext.USER_SESSION_CONTEXT),
        KEY_USER_NAME("key_user_session_user_name", SessionContext.USER_SESSION_CONTEXT),
        KEY_USER_EMAIL("key_user_session_user_email", SessionContext.USER_SESSION_CONTEXT),
        KEY_USER_MOBILE_NUMBER("key_user_session_user_mobile_number", SessionContext.USER_SESSION_CONTEXT),
        KEY_USER_GENDER("key_user_session_gender", SessionContext.USER_SESSION_CONTEXT),
        KEY_USER_PROFILE_IMG("key_user_session_profile_url", SessionContext.USER_SESSION_CONTEXT),
        KEY_APP_LAT("key_user_session_lat_long", SessionContext.USER_SESSION_CONTEXT),
        KEY_APP_LONG("key_user_session_long_long", SessionContext.USER_SESSION_CONTEXT),
        REF_TITLE("key_user_session_ref_title", SessionContext.USER_SESSION_CONTEXT),
        REF_DESC("key_user_session_ref_desc", SessionContext.USER_SESSION_CONTEXT),
        REF_CODE("key_user_session_ref_code", SessionContext.USER_SESSION_CONTEXT),
        REF_URL("key_user_session_ref_url", SessionContext.USER_SESSION_CONTEXT),
        REF_BODY_CONTENT("key_user_session_ref_body_content", SessionContext.USER_SESSION_CONTEXT),
        REF_SUBJECT("key_user_session_ref_subject", SessionContext.USER_SESSION_CONTEXT),
        KEY_DATE_OF_BIRTH("key_user_session_dob", SessionContext.USER_SESSION_CONTEXT),
        KEY_ANNIVERSARY("key_user_session_anniversary", SessionContext.USER_SESSION_CONTEXT),

        KEY_USER_SKIPPED_LOGIN("key_user_skipped_login", SessionContext.APP_LIFECYCLE_CONTEXT),
        KEY_USER_SKIPPED_UPDATE_TIMESTAMP("key_user_skipped_update_timestamp", SessionContext.APP_LIFECYCLE_CONTEXT),
        KEY_INSTANCE_ID("key_instance_id", SessionContext.APP_LIFECYCLE_CONTEXT),
        KEY_ENCRYPTION_ENABLE("key_encryption_enable", SessionContext.APP_LIFECYCLE_CONTEXT);

        private final String keyValue;
        private final SessionContext sessionContext;

        SESSION(String keyValue, SessionContext sessionContext) {
            this.keyValue = keyValue;
            this.sessionContext = sessionContext;
        }

        public String getKeyValue() {
            return keyValue;
        }

        public SessionContext getSessionContext() {
            return sessionContext;
        }

        @Override
        public String toString() {
            return keyValue;
        }
    }
}
