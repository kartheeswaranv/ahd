package com.athomediva.data.sharedpref;

import android.content.SharedPreferences;

/**
 * Created by mohitkumar on 03/05/17.
 */

public abstract class SaveData {
    private final SharedPreferences sharedPRef;
    private final SharedPreferences.Editor editor;

    SaveData(SharedPreferences sf) {
        sharedPRef = sf;
        editor = sf.edit();
    }

    public void setValue(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public void setValue(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public void setValue(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void setValue(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public int getIntValue(String key) {
        return sharedPRef.getInt(key, -1);
    }

    public long getLongValue(String key) {
        return sharedPRef.getLong(key, -1);
    }

    public String getStringValue(String key) {
        return sharedPRef.getString(key, null);
    }

    public boolean getBooleanValue(String key) {
        return sharedPRef.getBoolean(key, false);
    }

    public void clearALL() {
        editor.clear().apply();
    }
}
