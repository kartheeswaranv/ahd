package com.athomediva.data.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mohitkumar on 17/10/16.
 */

public class UserData extends SaveData {
    private static final String PREF_NAME = "ahd_user_session";
    private static volatile UserData sInstance;

    private UserData(SharedPreferences sf) {
        super(sf);
    }

    public static UserData getInstance(Context context) {
        if (sInstance == null) {
            synchronized (UserData.class) {
                if (sInstance == null) {
                    sInstance = new UserData(context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE));
                }
            }
        }
        return sInstance;
    }
}
