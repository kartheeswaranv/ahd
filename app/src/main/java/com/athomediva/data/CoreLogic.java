package com.athomediva.data;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.models.local.BucketModel;
import com.athomediva.data.models.local.CartModel;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.data.models.local.Contact;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.data.models.local.OptionModel;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.data.models.remote.BucketInfo;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartServiceGroup;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.data.models.remote.TimeSlot;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class CoreLogic {
    private static final String TAG = CoreLogic.class.getSimpleName();

    /**
     * To Get the Services count for the list of services
     */
    public static int getServicesCount(List<Services> list) {
        int count = 0;
        if (list != null && list.size() > 0) {
            for (Services model : list) {
                count += model.getQuantity();
            }
        }
        return count;
    }

    /**
     * To get the total amount for the list of services
     */
    public static double getServicesAmt(List<Services> list) {
        double amt = 0;
        if (list != null && list.size() > 0) {
            for (Services model : list) {
                amt += model.getQuantity() * Double.valueOf(model.getPrice());
            }
        }
        return amt;
    }

    /**
     * To get the cart details
     * Cart details has only services count and amount
     * of the selected services
     * This object is used to update the cartwidget
     */
    public static CartModel getCartDetails(APICartResponse response) {
        int count = 0;
        double amt = 0;
        if (response != null && response.getData() != null) {
            /*if (session.getServiceList() != null && session.getServiceList().size() > 0) {
                for (Services model : session.getServiceList()) {
                    if (model.getQuantity() > 0) {
                        count += model.getQuantity();
                        amt += Double.valueOf(model.getPrice());
                    }
                }
            }*/
            if (response.getData().getBuckets() != null && response.getData().getBuckets().size() > 0) {
                for (CartBucketItem item : response.getData().getBuckets()) {
                    count += item.getServiceCount();
                    amt += item.getTotal();
                }
            }
        }
        if (count != 0 && amt != 0) {
            return new CartModel(count, amt);
        }

        return null;
    }

    public static DateSlot getSelectedDateTimeSlot(List<DateSlot> list) {
        if (list != null && list.size() > 0) {
            for (DateSlot slot : list) {
                if (slot.isSelected() && slot.getSelectedTimeSlot() != null) return slot;
            }
        }
        return null;
    }

    public static DateSlot getSelectedDateSlot(List<DateSlot> list) {
        if (list != null && list.size() > 0) {
            for (DateSlot slot : list) {
                if (slot.isSelected()) return slot;
            }
        }
        return null;
    }

    public static String getBucketsTitleTxt(List<BucketModel> list) {
        if (list != null && list.size() > 0) {
            String comma_separator = ", ";
            String and_separator = " & ";
            String txt = "";
            for (int index = 0; index < list.size(); index++) {
                BucketInfo info = list.get(index).getBucketInfo();
                String separator = "";
                if (list.size() > 1) {
                    if (index == list.size() - 2) {
                        separator = and_separator;
                    } else if (index == list.size() - 1) {

                    } else {
                        separator = comma_separator;
                    }
                }

                if (info != null && !TextUtils.isEmpty(info.getTitle())) {
                    txt += info.getTitle() + separator;
                }
            }

            LogUtils.LOGD(TAG, "getBucketsTitleTxt : " + txt);
            return txt + " BookingService";
        }
        return null;
    }

    public static List<Address> getFilterAddressByCircle(@NonNull List<Address> addressList, @NonNull String circleName) {
        LogUtils.LOGD(TAG, "getFilterAddressByCircle : ");

        if (addressList != null && !addressList.isEmpty()) {
            ArrayList<Address> list = new ArrayList<>();
            for (Address address : addressList) {
                if (circleName.equalsIgnoreCase(address.getCircle())) {
                    list.add(address);
                }
            }

            return list;
        }

        return Collections.emptyList();
    }

    public static int getSubCatPosition(List<SubCategories> list, SubCategories subCategories) {
        LogUtils.LOGD(TAG, "getSubCatPosition : ");
        int position = -1;
        if (list != null && list.size() > 0 && subCategories != null) {
            for (int index = 0; index < list.size(); index++) {
                if (list.get(index).getId() == subCategories.getId()) {
                    position = index;
                    break;
                }
            }
        }

        return position;
    }

    public static int getSubCatPosition(List<SubCategories> list, long subCatId) {
        LogUtils.LOGD(TAG, "getSubCatPosition : ");
        int position = -1;
        if (list != null && list.size() > 0 && subCatId != 0) {
            for (int index = 0; index < list.size(); index++) {
                if (list.get(index).getId() == subCatId) {
                    position = index;
                    break;
                }
            }
        }

        return position;
    }

    public static int getCategorytPosition(List<Categories> list, long subCatId) {
        LogUtils.LOGD(TAG, "getCategorytPosition : ");
        int position = -1;
        if (list != null && list.size() > 0 && subCatId != 0) {
            for (int index = 0; index < list.size(); index++) {
                position = getSubCatPosition(list.get(index).getSubcategories(), subCatId);
                if (position != -1) return index;
            }
        }

        return position;
    }

    public static int getCategoryPosition(List<Categories> list, long catId) {
        LogUtils.LOGD(TAG, "getCategorytPosition : ");
        int position = -1;
        if (list != null && list.size() > 0) {
            for (int index = 0; index < list.size(); index++) {
                if (list.get(index).getId() == catId) {
                    return index;
                }
            }
        }

        return position;
    }

    /**
     * The method returns true if
     * 1. User has not clicked later
     * 2. The update is being displayed first time.
     * 3. later timestamp is expired
     */
    public static boolean shouldShowOptionalUpdate(PreferenceManager preferenceManager) {
        long updateLaterTimeStamp =
          preferenceManager.getLongValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_UPDATE_TIMESTAMP);
        return updateLaterTimeStamp == -1 || updateLaterTimeStamp < System.currentTimeMillis();
    }

    public static int getSelectedOption(List<OptionModel> list) {
        LogUtils.LOGD(TAG, "getSelectedOption : ");
        if (list != null && list.size() > 0) {
            for (int index = 0; index < list.size(); index++) {
                if (list.get(index).isSelected()) {
                    return index;
                }
            }
        }

        return -1;
    }

    public static boolean isCartValid(APICartResponse mCartResponse) {
        LogUtils.LOGD(TAG, "isCartValid : ");
        if (mCartResponse != null && mCartResponse.isSuccess() && mCartResponse.getData() != null) {
            return true;
        }

        return false;
    }

    public static boolean hasBucketsInCart(APICartResponse mCartResponse) {
        LogUtils.LOGD(TAG, "hasBucketsInCart : ");
        if (mCartResponse != null
          && mCartResponse.isSuccess()
          && mCartResponse.getData() != null
          && mCartResponse.getData().getBuckets() != null
          && !mCartResponse.getData().getBuckets().isEmpty()) {
            return true;
        }

        return false;
    }

    public static boolean hasServicesInCart(APICartResponse mCartResponse) {
        LogUtils.LOGD(TAG, "hasServicesInCart : ");
        if (mCartResponse != null
          && mCartResponse.isSuccess()
          && mCartResponse.getData() != null
          && mCartResponse.getData().getBuckets() != null
          && !mCartResponse.getData().getBuckets().isEmpty()) {
            for (CartBucketItem item : mCartResponse.getData().getBuckets()) {
                if (item.getServicesGroupList() != null && !item.getServicesGroupList().isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String getCartId(APICartResponse mCartResponse) {
        if (mCartResponse != null && mCartResponse.isSuccess() && mCartResponse.getData() != null) {
            return mCartResponse.getData().getCartId();
        }

        return null;
    }

    public static MinimumOrderError getMinOrderError(APICartResponse response) {
        LogUtils.LOGD(TAG, "getMinOrderError : ");
        if (response != null && response.isSuccess() && response.getData() != null) {
            return response.getData().getMinOrderInfo();
        }
        return null;
    }

    public static void UpdatedServices(APICartResponse response, List<Services> mList) {
        if (mList != null && mList.size() > 0) {
            for (Services service : mList) {
                int qty = getQtyOfServiceInCart(response, service.getServiceId());
                if (qty != service.getQuantity()) {
                    service.setQuantity(qty);
                }
            }
        }
    }

    public static int updateServices(long servicesId, int count, List<Services> list) {
        if (list != null && list.size() > 0) {
            Services ser = new Services(servicesId);
            int index = list.indexOf(ser);
            if (index != -1) {
                list.get(index).setQuantity(count);
            }
            return index;
        }
        return -1;
    }

    public static int getQtyOfServiceInCart(APICartResponse response, long id) {
        LogUtils.LOGD(TAG, "getQtyOfServiceInCart : ");
        if (response != null
          && response.isSuccess()
          && response.getData() != null
          && response.getData().getBuckets() != null) {
            for (CartBucketItem bucketItem : response.getData().getBuckets()) {
                int qty = getServiceQty(bucketItem, id);
                if (qty != -1) return qty;
            }
        }
        return 0;
    }

    public static int getServicesCountInCart(List<CartBucketItem> list) {
        int count = 0;
        if (list != null && !list.isEmpty()) {
            for (CartBucketItem bucketItem : list) {
                count += bucketItem.getServiceCount();
            }
        }

        return count;
    }

    public static double getTotalAmount(List<CartBucketItem> list) {
        double amt = 0;
        if (list != null && !list.isEmpty()) {
            for (CartBucketItem bucketItem : list) {
                amt += bucketItem.getTotal();
            }
        }

        return amt;
    }

    private static int getServiceQty(CartBucketItem item, long sId) {
        LogUtils.LOGD(TAG, "hasServiceInBucket : ");
        if (item != null && item.getServicesGroupList() != null && !item.getServicesGroupList().isEmpty()) {
            for (CartServiceGroup groupItem : item.getServicesGroupList()) {
                int qty = getServiceQty(groupItem, sId);
                if (qty != -1) {
                    return qty;
                }
            }
        }

        return -1;
    }

    private static int getServiceQty(CartServiceGroup group, long id) {
        LogUtils.LOGD(TAG, "hasServiceInGroup : ");
        if (group != null && group.getServices() != null && !group.getServices().isEmpty()) {
            for (Services services : group.getServices()) {
                if (services.getServiceId() == id) {
                    return services.getQuantity();
                }
            }
        }

        return -1;
    }

    public static void setExpandFlag(ArrayList<CartBucketItem> responseList, ArrayList<CartBucketItem> previousItem) {
        LogUtils.LOGD(TAG, "setExpandFlag : ");
        if (responseList != null && !responseList.isEmpty() && previousItem != null && !previousItem.isEmpty()) {
            for (CartBucketItem item : responseList) {
                if (isExpanded(item, previousItem)) {
                    item.setExpanded(true);
                }
            }
        }
    }

    public static boolean isExpanded(CartBucketItem item, ArrayList<CartBucketItem> list) {
        LogUtils.LOGD(TAG, "isExpanded : ");
        if (item != null && list != null && list.size() > 0) {
            for (CartBucketItem bucket : list) {
                if (TextUtils.equals(item.getBookingId(), bucket.getBookingId())) {
                    return bucket.isExpanded();
                }
            }
        }

        return false;
    }

    public static ArrayList<Booking> filterByCompletedState(ArrayList<Booking> list, int maxState) {
        LogUtils.LOGD(TAG, "isExpanded : ");
        if (list != null && list.size() > 0) {
            ArrayList<Booking> filterList = new ArrayList<>();
            for (Booking booking : list) {
                if (booking.getOrderStatus() == maxState && booking.isOrderRated()) {
                    filterList.add(booking);
                }
            }

            return filterList;
        }

        return null;
    }

    public static ArrayList<Booking> filterByCompletedState(ArrayList<Booking> list) {
        LogUtils.LOGD(TAG, "isExpanded : ");
        if (list != null && list.size() > 0) {
            ArrayList<Booking> filterList = new ArrayList<>();
            for (Booking booking : list) {
                if (booking.isOrderRated()) {
                    filterList.add(booking);
                }
            }

            return filterList;
        }

        return null;
    }

    public static ArrayList<Booking> getRecentBookings(ArrayList<Booking> past, ArrayList<Booking> upcoming) {
        ArrayList<Booking> wholeFilterList = CoreLogic.filterByCompletedState(past);
        if (wholeFilterList != null) {
            ArrayList<Booking> list1 = CoreLogic.filterByCompletedState(upcoming);
            if (list1 != null) {
                wholeFilterList.addAll(list1);
            }
        }

        return wholeFilterList;
    }

    public static HashMap<Long, Integer> getServicesMap(ArrayList<CartBucketItem> list) {
        if (list != null && list.size() > 0) {
            HashMap<Long, Integer> servicesMap = new HashMap<>();
            for (CartBucketItem item : list) {
                if (item.getServicesGroupList() != null && !item.getServicesGroupList().isEmpty()) {
                    updateServicesInMap(item.getServicesGroupList(), servicesMap);
                }
            }

            return servicesMap;
        }
        return null;
    }

    private static void updateServicesInMap(ArrayList<CartServiceGroup> list, HashMap map) {
        if (list != null && !list.isEmpty()) {
            for (CartServiceGroup group : list) {
                if (group.getServices() != null && !group.getServices().isEmpty()) {
                    for (Services serv : group.getServices()) {
                        map.put(serv.getServiceId(), serv.getQuantity());
                    }
                }
            }
        }
    }

    public static void resetCategoryData(APIListCategoriesResponse response) {
        if (response != null && response.getData() != null) {
            if (response.getData().getCategories() != null && !response.getData().getCategories().isEmpty()) {
                for (Categories cat : response.getData().getCategories()) {
                    clearServicesInSubCat(cat.getSubcategories());
                }
            }

            if (response.getData().getPackages() != null
              && response.getData().getPackages() != null
              && response.getData().getPackages().getList() != null
              && !response.getData().getPackages().getList().isEmpty()) {
                clearServicesList(response.getData().getPackages().getList());
            }
        }
    }

    private static void clearAllCategoriesServices(ArrayList<Categories> catList) {
        if (catList != null && !catList.isEmpty()) {
            for (Categories cat : catList) {
                clearServicesInSubCat(cat.getSubcategories());
            }
        }
    }

    private static void clearServicesInSubCat(ArrayList<SubCategories> subCatList) {
        if (subCatList != null && !subCatList.isEmpty()) {
            for (SubCategories subcat : subCatList) {
                clearServicesList(subcat.getServices());
            }
        }
    }

    private static void clearServicesList(ArrayList<Services> servList) {
        if (servList != null && !servList.isEmpty()) {
            for (Services serv : servList) {
                serv.setQuantity(0);
            }
        }
    }

    public static void updateServices(HashMap<Long, Integer> map, List<Services> list) {
        if (list != null && list.size() > 0) {
            for (Services serv : list) {
                Integer value = map.get(serv.getServiceId());
                if (value == null) {
                    serv.setQuantity(0);
                } else {
                    serv.setQuantity(value);
                }
            }
        }
    }

    public static DateSlot updatePreselectedSlot(List<DateSlot> list, TimeSlot slot) {
        if (list != null && !list.isEmpty()) {
            for (DateSlot dateSlot : list) {
                boolean timeSlotMatch = updateTimeSlot(dateSlot.getSlots(), slot);

                if (timeSlotMatch) {
                    dateSlot.setSelected(true);
                    return dateSlot;
                }
            }
        }

        return null;
    }

    private static boolean updateTimeSlot(List<TimeSlot> list, TimeSlot slot) {
        if (list != null && !list.isEmpty()) {
            for (TimeSlot timeslot : list) {
                if (timeslot.getFrom() == slot.getFrom() && timeslot.getTo() == slot.getTo()) {
                    timeslot.setSelected(true);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * get BookingIds String from Confirm booking infos
     * for GA
     */
    public static String getbookingIdsForGA(ArrayList<ConfirmBookingInfo> list) {
        if (list != null && !list.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            for (int index = 0; index < list.size(); index++) {
                builder.append(list.get(index).getBookingId());
                if (index < list.size() - 1) builder.append("_");
            }

            return builder.toString();
        }

        return null;
    }

    public static String getAllMobileNos(List<Contact> list) {
        if (list != null && !list.isEmpty()) {
            return Arrays.toString(list.toArray());
        }

        return null;
    }
}
