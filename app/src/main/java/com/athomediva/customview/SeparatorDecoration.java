package com.athomediva.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class SeparatorDecoration extends RecyclerView.ItemDecoration {

    private final Paint mPaint, mWhitePaint;
    private float marginLeft, marginRight;

    /**
     * Create a decoration that draws a line in the given color and width between the items in the view.
     *
     * @param context a context to access the resources.
     * @param color the color of the separator to draw.
     * @param heightDp the height of the separator in dp.
     */
    public SeparatorDecoration(@NonNull Context context, @ColorInt int color,
      @FloatRange(from = 0, fromInclusive = false) float heightDp) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(heightDp);
        mWhitePaint = null;
    }

    public SeparatorDecoration(@NonNull Context context, @ColorInt int color,
      @FloatRange(from = 0, fromInclusive = false) float heightDp, float marginLeft, float marginRight) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(heightDp);
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        mWhitePaint = new Paint(); // can be brawn based on bg color.provide a way to pass bg color
        mWhitePaint.setColor(Color.WHITE);
        mWhitePaint.setStrokeWidth(heightDp);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();

        // we want to retrieve the position in the list
        final int position = params.getViewAdapterPosition();

        // and add a separator to any view but the last one
        if (position < state.getItemCount() - 1) {
            outRect.set(0, 0, 0, (int) mPaint.getStrokeWidth()); // left, top, right, bottom
        } else {
            outRect.setEmpty(); // 0, 0, 0, 0
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        // we set the stroke width before, so as to correctly draw the line we have to offset by width / 2
        final int offset = (int) (mPaint.getStrokeWidth() / 2);

        // this will iterate over every visible view
        for (int i = 0; i < parent.getChildCount(); i++) {
            // get the view
            final View view = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();

            // get the position
            final int position = params.getViewAdapterPosition();

            // and finally draw the separator
            if (position < state.getItemCount()) {
                if (mWhitePaint != null) {
                    c.drawLine(view.getLeft(), view.getBottom() + offset, view.getRight(), view.getBottom() + offset,
                      mWhitePaint);
                }
                c.drawLine(view.getLeft() + marginLeft, view.getBottom() + offset, view.getRight() - marginRight,
                  view.getBottom() + offset, mPaint);
            }
        }
    }
}