package com.athomediva.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by kartheeswaran on 19/07/17.
 */

public class HorizontalSeparatorDecoration extends RecyclerView.ItemDecoration {

    private final Paint mPaint, mWhitePaint;
    private float marginTop, marginBottom;
    private float marginStartAndEnd;

    /**
     * Create a decoration that draws a line in the given color and width between the items in the view.
     *
     * @param context a context to access the resources.
     * @param color the color of the separator to draw.
     * @param heightDp the height of the separator in dp.
     */
    public HorizontalSeparatorDecoration(@NonNull Context context, @ColorInt int color,
      @FloatRange(from = 0, fromInclusive = false) float heightDp) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(heightDp);
        mWhitePaint = null;
    }

    public HorizontalSeparatorDecoration(@NonNull Context context, @ColorInt int color,
      @FloatRange(from = 0, fromInclusive = false) float heightDp, float marginTop, float marginBottom) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(heightDp);
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
        mWhitePaint = new Paint(); // can be brawn based on bg color.provide a way to pass bg color
        mWhitePaint.setColor(Color.WHITE);
        mWhitePaint.setStrokeWidth(heightDp);
    }

    public HorizontalSeparatorDecoration(@NonNull Context context, @ColorInt int color,
      @FloatRange(from = 0, fromInclusive = false) float heightDp, float marginStartAndEnd) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(heightDp);
        this.marginStartAndEnd = marginStartAndEnd;
        mWhitePaint = null;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();

        // we want to retrieve the position in the list
        final int position = params.getViewAdapterPosition();

        // and add a separator to any view but the last one
        if (position < state.getItemCount()) {
            if (position == 0) {
                outRect.set((int) marginStartAndEnd, 0, (int) mPaint.getStrokeWidth(), 0);
            } else if (position == state.getItemCount() - 1) {
                outRect.set(0, 0, (int) marginStartAndEnd, 0);
            } else {
                outRect.set(0, 0, (int) mPaint.getStrokeWidth(), 0); // left, top, right, bottom
            }
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        // we set the stroke width before, so as to correctly draw the line we have to offset by width / 2
        final int offset = (int) (mPaint.getStrokeWidth() / 2);

        // this will iterate over every visible view
        for (int i = 0; i < parent.getChildCount(); i++) {
            // get the view
            final View view = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();

            // get the position
            final int position = params.getViewAdapterPosition();

            // and finally draw the separator
            if (position < state.getItemCount()) {
                if (mWhitePaint != null) {
                    c.drawLine(view.getRight() + offset, view.getTop(), view.getRight() + offset, view.getBottom(),
                      mWhitePaint);
                }
                c.drawLine(view.getRight() + offset, view.getTop() + marginTop, view.getRight() + offset,
                  view.getBottom() - marginBottom, mPaint);
            }
        }
    }
}
