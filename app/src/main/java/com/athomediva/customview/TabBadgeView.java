package com.athomediva.customview;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import java.util.Locale;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 17/04/17.
 */

public class TabBadgeView extends LinearLayout {
    private static final String TAG = TabBadgeView.class.getSimpleName();

    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.badge_count) TextView mCountView;

    public TabBadgeView(Context context) {
        super(context);
    }

    public TabBadgeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TabBadgeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        LogUtils.LOGD(TAG, "onFinishInflate : ");
    }

    @DebugLog
    public void updateView(String title, int count) {
        LogUtils.LOGD(TAG, "updateView : ");
        mTitle.setText(title);
        if (count > 0) {
            mCountView.setVisibility(View.VISIBLE);
            mCountView.setText(String.valueOf(count));
        } else {
            mCountView.setVisibility(View.GONE);
        }
    }

    @DebugLog
    public void setTextAllCaps(boolean value) {
        LogUtils.LOGD(TAG, "setTextAllCaps : ");
        Locale locale = getResources().getConfiguration().locale;
        if (value) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                mTitle.setAllCaps(true);
            } else {
                mTitle.setText(mTitle.getText().toString().toUpperCase(locale));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                mTitle.setAllCaps(false);
            } else {
                mTitle.setText(mTitle.getText().toString().toLowerCase(locale));
            }
        }
    }

    @DebugLog
    public void setSelected(boolean value) {
        LogUtils.LOGD(TAG, "setSelected : ");
        if (value) {
            mTitle.setSelected(true);
            mCountView.setSelected(true);
        } else {
            mTitle.setSelected(false);
            mCountView.setSelected(false);
        }
    }
}
