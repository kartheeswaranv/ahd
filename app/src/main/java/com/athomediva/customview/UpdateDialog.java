package com.athomediva.customview;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.athomediva.data.AppConstants;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.AHDApplication;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohit on 1/30/17.
 */

public class UpdateDialog extends Dialog {
    private static final String TAG = LogUtils.makeLogTag(UpdateDialog.class.getSimpleName());

    public UpdateDialog(Context context) {
        super(context);
    }

    public void showDialog(boolean isMandatory, String desc) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ahd_layout_update_dialog);
        TextViewCustom descView = (TextViewCustom) findViewById(R.id.desc);
        descView.setText(desc);
        TextViewCustom updateLater = (TextViewCustom) findViewById(R.id.update_later);
        final PreferenceManager prefMgr = AHDApplication.get(getContext()).getComponent().preferencesManager();
        long updateLaterTimeStamp = prefMgr.getLongValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_UPDATE_TIMESTAMP);
        LOGD(TAG,
          "showDialog: updateLaterTimeStamp =" + updateLaterTimeStamp + "currentTimestamp =" + System.currentTimeMillis());
        if (!isMandatory) {
            updateLater.setVisibility(View.VISIBLE);
            updateLater.setOnClickListener(v -> {
                long nextTimeStamp = System.currentTimeMillis() + AppConstants.UPDATE_LATER_ASK_TIMESTAMP;
                LOGD(TAG, "showDialog: next time update ask timestamp = " + nextTimeStamp);
                prefMgr.setValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_UPDATE_TIMESTAMP, nextTimeStamp);
                dismiss();
            });
        } else {
            updateLater.setVisibility(View.GONE);
        }

        TextViewCustom updateNow = (TextViewCustom) findViewById(R.id.update_now);

        updateNow.setOnClickListener(v -> AndroidHelper.goToPlayStore(getContext()));
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
            }
            return false;
        });
        show();
    }
}
