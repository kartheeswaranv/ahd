package com.athomediva.customview;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;
import com.athomediva.data.models.local.Entity;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 19/04/17.
 */

public class CustomTextInputLayout extends TextInputLayout {

    private TextWatcher mTextValidator = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (getEditText().isFocused() && getEditText().getText().toString().trim().length() > 0) {
                setErrorEnabled(false);
                setError("");
            }
        }
    };

    public CustomTextInputLayout(Context context) {
        super(context);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView() {
        getEditText().removeTextChangedListener(mTextValidator);
        getEditText().addTextChangedListener(mTextValidator);
        setTypeface(UiUtils.getTypeface(getContext(), getContext().getString(R.string.ubuntu_regular_font)));
        getEditText().setTypeface(UiUtils.getTypeface(getContext(), getContext().getString(R.string.ubuntu_regular_font)));
    }

    @Override
    protected void onDetachedFromWindow() {
        if (getEditText() != null) getEditText().removeTextChangedListener(mTextValidator);
        super.onDetachedFromWindow();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    @Override
    public void setError(@Nullable CharSequence error) {
        if (!TextUtils.isEmpty(error)) {
            SpannableString s = new SpannableString(error);
            s.setSpan(
              new TypefaceSpan(UiUtils.getFontByName(getContext(), getContext().getString(R.string.ubuntu_regular_font))), 0,
              s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            super.setError(s);
        } else {
            super.setError(error);
        }
    }

    class TypefaceSpan extends MetricAffectingSpan {
        private Typeface mTypeface;

        public TypefaceSpan(Typeface typeface) {
            mTypeface = typeface;
        }

        @Override
        public void updateMeasureState(TextPaint p) {
            p.setTypeface(mTypeface);
            p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }

        @Override
        public void updateDrawState(TextPaint tp) {
            tp.setTypeface(mTypeface);
            tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }
    }

    class EntityTextWatcher implements TextWatcher {
        private Entity entity;

        public EntityTextWatcher(Entity entity) {
            this.entity = entity;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            entity.setData(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}

