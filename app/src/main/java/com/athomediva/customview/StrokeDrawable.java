package com.athomediva.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.StateListDrawable;
import android.util.TypedValue;
import com.athomediva.utils.UiUtils;

/**
 * Created by kartheeswaran on 27/05/17.
 */

public class StrokeDrawable extends StateListDrawable {
    private int mColor = 0;
    private int mHeight = 0;
    private int mWidth = 0;
    private int mStrokeWidth = 0;
    private Context mContext;
    private int startX = 0;

    public StrokeDrawable(Context context, int color, int strokeWidth) {
        mColor = color;
        mContext = context;
        mStrokeWidth = strokeWidth;

        startX = UiUtils.dpToPx(1);
        invalidateSelf();
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public void setColorFilter(int color, PorterDuff.Mode mode) {
        super.setColorFilter(color, mode);
    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public int getAlpha() {
        return super.getAlpha();
    }

    @Override
    public void draw(Canvas canvas) {
        getHeightAndWidth();
        mHeight = canvas.getHeight();
        mWidth = canvas.getWidth();
        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        Paint strokePaint = new Paint();
        strokePaint.setColor(mColor);
        Rect rect = new Rect(startX, 0, mWidth, mHeight - (mStrokeWidth));
        Rect strokeRect = new Rect(startX, mHeight - (mStrokeWidth), mWidth, mHeight);
        canvas.drawRect(rect, paint);
        canvas.drawRect(strokeRect, strokePaint);
    }

    @Override
    public int getOpacity() {
        return 0;
    }

    private void getHeightAndWidth() {
        mHeight = getIntrinsicHeight();
        mWidth = getIntrinsicWidth();
    }
}
