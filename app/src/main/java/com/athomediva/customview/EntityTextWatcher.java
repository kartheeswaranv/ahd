package com.athomediva.customview;

import android.text.Editable;
import android.text.TextWatcher;
import com.athomediva.data.models.local.Entity;

/**
 * Created by mohitkumar on 20/04/17.
 */

public class EntityTextWatcher implements TextWatcher {
    private final Entity entity;

    public EntityTextWatcher(Entity entity) {
        this.entity = entity;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        entity.setData(s.toString());
        entity.setValid(true);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
