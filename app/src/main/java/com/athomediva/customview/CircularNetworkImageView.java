package com.athomediva.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class CircularNetworkImageView extends NetworkImageView {
    private static final String TAG = CircularNetworkImageView.class.getSimpleName();
    Context mContext;

    public CircularNetworkImageView(Context context) {
        super(context);
        mContext = context;
    }

    public CircularNetworkImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        mContext = context;
    }

    public CircularNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        if (bm == null) return;
        RoundedBitmapDrawable drawable = getCircularBmp(bm);
        if(drawable!= null) {
            setImageDrawable(drawable);
        }
    }

    /**
     * Creates a circular bitmap and uses whichever dimension is smaller to determine the width
     * <br/>Also constrains the circle to the leftmost part of the image
     *
     * @return bitmap
     */

    private RoundedBitmapDrawable getCircularBmp(Bitmap bitmap) {
        LogUtils.LOGD(TAG,"getCircularBmp : ");
        int mImagWidth = getMeasuredWidth();
        int mImagHeight = getMeasuredHeight();

        if(mImagWidth > 0 && mImagHeight > 0) {

            RoundedBitmapDrawable d = RoundedBitmapDrawableFactory.create(mContext.getResources(),
              Bitmap.createScaledBitmap(bitmap, mImagWidth, mImagHeight, false));
            d.setCornerRadius(d.getIntrinsicHeight() / 2);
            d.setAntiAlias(true);

            return d;
        }

        return null;
    }
}