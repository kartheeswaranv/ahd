package com.athomediva.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 28/04/17.
 */

public class EditTextCustom extends android.support.v7.widget.AppCompatEditText {
    public EditTextCustom(Context context) {
        super(context);
    }

    public EditTextCustom(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        initView(context, attrs, defStyle);
    }

    public EditTextCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0);
    }

    public void initView(final Context context, AttributeSet attrs, int defStyle) {
        String type = "";
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewCustom, defStyle, 0);
            type = ta.getString(R.styleable.TextViewCustom_ttfName);
            ta.recycle();
        }

        if (type == null) {
            type = getContext().getString(R.string.ubuntu_regular_font);
        }
        setTypeface(UiUtils.getFontByName(context, type));
    }

    public void setTtfName(String name) {
        if (!TextUtils.isEmpty(name)) {
            setTypeface(UiUtils.getFontByName(getContext(), name));
        }
    }
}
