package com.athomediva.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class ExpandableTextView extends TextViewCustom {
    private final String TAG = LogUtils.makeLogTag(ExpandableTextView.class.getSimpleName());
    private final String DEFAULT_EXPAND_MORE_TEXT = "More>>";
    private final int DEFAULT_EXPAND_TEXT_COLOR = R.color.colorPrimary;
    private String expandMoreText = DEFAULT_EXPAND_MORE_TEXT;
    private int expandTextColor = DEFAULT_EXPAND_TEXT_COLOR;
    private int mMaxChar = 30;
    private String mPrefixText;
    private boolean mSpanAtEnd = false;

    private ClickableSpan mClickableSpan;

    public ExpandableTextView(Context context) {
        super(context);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView, defStyleAttr, 0);
        mMaxChar = a.getInt(R.styleable.ExpandableTextView_maxCharacter, mMaxChar);
        expandMoreText = a.getString(R.styleable.ExpandableTextView_expandMoreText);
        mPrefixText = a.getString(R.styleable.ExpandableTextView_dot_prefix);
        mSpanAtEnd = a.getBoolean(R.styleable.ExpandableTextView_moreTextAtEnd, mSpanAtEnd);
        expandTextColor =
          a.getColor(R.styleable.ExpandableTextView_expandTextColor, getResources().getColor(DEFAULT_EXPAND_TEXT_COLOR));
        a.recycle();
        setMovementMethod(LinkMovementMethod.getInstance());
        updateView();
    }

    public void setSpanText(String txt) {
        LogUtils.LOGD(TAG, "setSpanText : ");
        expandMoreText = txt;
    }

    public void setMaxCharacter(int character) {
        LogUtils.LOGD(TAG, "setMaxCharacter : ");
        mMaxChar = character;
    }

    public void setExpandMoreText(String expandMoreText) {
        this.expandMoreText = expandMoreText;
        invalidate();
    }

    public void setExpandTextColor(int expandTextColor) {
        this.expandTextColor = expandTextColor;
        invalidate();
    }

    public void setClickableSpan(ClickableSpan span) {
        mClickableSpan = span;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        updateView();
        super.onLayout(changed, left, top, right, bottom);
    }

    private void updateView() {
        LogUtils.LOGD(TAG, "updateView : ");
        if (!TextUtils.isEmpty(this.getText())) {
            SpannableStringBuilder builder = getSpannableString(this.getText().toString(), expandMoreText, expandTextColor);
            if (builder != null) {
                setText(builder, BufferType.SPANNABLE);
            }
        }
    }

    @DebugLog
    private SpannableStringBuilder getSpannableString(String text, String spanableText, int color) {
        LogUtils.LOGD(TAG, "getSpannableString : " + text + "  " + mMaxChar);
        if (text.length() > 0 && !text.equals(spanableText)) {
            text = getTextAfterPrefixAdded(text, spanableText);

            SpannableStringBuilder ssb = new SpannableStringBuilder();
            ssb.append(text);
            ssb.setSpan(new ClickSpan(), text.indexOf(spanableText), (text.indexOf(spanableText)) + spanableText.length(),
              0);
            //Remove Color span if color is 0
            if (color != 0) {
                ssb.setSpan(new ForegroundColorSpan(color), (text.indexOf(spanableText)),
                  (text.indexOf(spanableText)) + spanableText.length(), 0);
            }
            LogUtils.LOGD(TAG, "getSpannableString : " + ssb.toString());
            return ssb;
        }
        return null;
    }

    @DebugLog
    private String getTextAfterPrefixAdded(String text, String spannableText) {
        LogUtils.LOGD(TAG, "getTextAfterPrefixAdded : " + spannableText);
        if (!mSpanAtEnd && text.length() > mMaxChar) {
            text = text.substring(0, mMaxChar);

            if (!TextUtils.isEmpty(mPrefixText)) {
                text = text.concat("... ");
            }
        }

        if (!TextUtils.isEmpty(spannableText) && text.indexOf(spannableText) == -1) {
            text = text.concat(" " + spannableText);
        }

        text = Html.fromHtml(text).toString();

        return text;
    }

    public class ClickSpan extends ClickableSpan

    {
        @Override
        public void onClick(View view) {
            if (mClickableSpan != null) {
                mClickableSpan.onClick(view);
            }
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            //ds.setColor(ds.linkColor);
            ds.setUnderlineText(false);
        }
    }
}
