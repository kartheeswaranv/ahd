package com.athomediva.customview;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import www.zapluk.com.R;

/**
 * Created by mohit on 1/30/17.
 */

public class ProgressDialog extends Dialog {
    private final Context mContext;

    public ProgressDialog(Context context) {
        super(context);
        mContext = context;
    }

    public void showDialog(String desc) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ahd_layout_progress_dialog);
        TextViewCustom descView = (TextViewCustom) findViewById(R.id.loading_desc);
        if (TextUtils.isEmpty(desc)) {
            descView.setVisibility(View.GONE);
        } else {
            descView.setText(desc);
        }
        setCanceledOnTouchOutside(false);
        getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
            }
            return false;
        });
        show();
    }
}
