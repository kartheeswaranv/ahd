package com.athomediva.customview;

import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.view.animation.AlphaAnimation;
import com.athomediva.logger.LogUtils;

/**
 * Created by kartheeswaran on 12/04/17.
 */

public class ToolbarAnimationController implements AppBarLayout.OnOffsetChangedListener {
    private static final String TAG = ToolbarAnimationController.class.getSimpleName();

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.3f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.7f;

    private static final int ALPHA_ANIMATIONS_DURATION = 100;

    private boolean mIsTheTitleContainerVisible = true;

    private View mView1, mView2;
    private float percentage;

    public ToolbarAnimationController(View view1, View view2) {
        LogUtils.LOGD(TAG, "ToolbarAnimationController : ");
        mView1 = view1;
        mView2 = view2;
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation =
          (visibility == View.VISIBLE) ? new AlphaAnimation(0f, 1f) : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private void handleToolbarVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                //mToolbarLocation.setBackground(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
                startAlphaAnimation(mView1, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = false;
            }
        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mView1, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        if (percentage == 0.0) {
            mView1.setVisibility(View.INVISIBLE);
        }
        handleToolbarVisibility(percentage);
    }

    public boolean isToolbarVisible() {
        return percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS;
    }
}
