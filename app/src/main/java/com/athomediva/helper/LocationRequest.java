package com.athomediva.helper;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import com.athomediva.logger.LogUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import hugo.weaving.DebugLog;

import static com.athomediva.helper.LocationRequest.ERROR_CODE.LOCATION_IS_NULL;
import static com.athomediva.helper.LocationRequest.ERROR_CODE.PROVIDER_NOT_ENABLED;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

public class LocationRequest
  implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final String TAG = LogUtils.makeLogTag(LocationRequest.class.getSimpleName());
    private GoogleApiClient mGoogleApiClient;
    private final Context mContext;
    private LocationResponseListener mLocationResponseListener;
    private LocationManager locationManager;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 5 * 1000;
    private final android.location.LocationListener locationListener = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            LOGD(TAG, "onLocationChanged: location" + location);
            mLocationResponseListener.updateLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            LOGD(TAG, "onStatusChanged: ");
        }

        @Override
        public void onProviderEnabled(String provider) {
            LOGD(TAG, "onProviderEnabled: ");
        }

        @Override
        public void onProviderDisabled(String provider) {
            LOGD(TAG, "onProviderDisabled: ");
        }
    };

    private final HandlerThread mLooperThread = new HandlerThread("LocationRequest");

    public LocationRequest(Context ctx) {
        mContext = ctx;
    }

    private void setLocationChangeListener(LocationResponseListener listener) {
        mLocationResponseListener = listener;
    }

    @DebugLog
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        LOGD(TAG, "onConnectionFailed: ");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mLocationResponseListener.onError(LOCATION_IS_NULL);
    }

    @DebugLog
    @Override
    public void onConnected(Bundle bundle) {
        LOGD(TAG, "onConnected: ");
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location != null) {
                mLocationResponseListener.updateLocation(location);
            } else {
                LOGD(TAG, "onConnected: location obtained is null. Lets try with Location Manager");
                getLocationFromLocationManager();
            }
        } catch (SecurityException e) {
            LOGE(TAG, "onConnected: SecurityException **** " + e.getLocalizedMessage());
            mLocationResponseListener.onError(ERROR_CODE.PERMISSION_EXCEPTION);
        }
    }

    private void getLocationFromLocationManager() throws SecurityException {
        mLooperThread.start();
        LOGD(TAG, "getLocationFromLocationManager: ");
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
            mLocationResponseListener.onError(PROVIDER_NOT_ENABLED);
        } else {
            Location location = null;
            // First get location from Network Provider
            if (isNetworkEnabled) {
                LOGD(TAG, "getLocationFromLocationManager Network Enabled");
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                  MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener, mLooperThread.getLooper());
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    LOGD(TAG, "getLocationFromLocationManager: network location =" + location);
                    mLocationResponseListener.updateLocation(location);
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                      MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener, mLooperThread.getLooper());
                    LOGD(TAG, "getLocationFromLocationManager: GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            LOGD(TAG, "getLocationFromLocationManager: gps location =" + location);
                            mLocationResponseListener.updateLocation(location);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        LOGD(TAG, "onConnectionSuspended: ");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location loc) {
        LOGD(TAG, "onLocationChanged: loc =" + loc);
        mLocationResponseListener.updateLocation(loc);
    }

    @DebugLog
    public void getLocation(LocationResponseListener listener) {
        LOGD(TAG, "getLocation: ");
        try {
            setLocationChangeListener(listener);
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext) == ConnectionResult.SUCCESS) {
                LOGD(TAG, "getLocation: ");
                mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this)
                  .addOnConnectionFailedListener(this)
                  .addApi(LocationServices.API)
                  .build();
                if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
                    LOGD(TAG, "getLocation: !mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()");
                    mGoogleApiClient.connect();
                }
            } else {
                LOGE(TAG, "getLocation: google services not installed. Lets try with location manager ");
                getLocationFromLocationManager();
            }
        } catch (Exception ex) {
            LOGE(TAG, "getLocation: " + ex.getLocalizedMessage());
            mLocationResponseListener.onError(LOCATION_IS_NULL);
            stopLocation();
        }
    }

    /**
     * Please call this method once the job is completed
     */
    @DebugLog
    public void stopLocation() {
        LOGD(TAG, "stopLocation: ");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        mLooperThread.quit();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }

    public enum ERROR_CODE {
        PLAY_SERVICES_NOT_AVAILABLE(0),
        PERMISSION_EXCEPTION(1),
        PROVIDER_NOT_ENABLED(2),
        LOCATION_IS_NULL(3);
        private final long errorCode;

        ERROR_CODE(long errorCode) {
            this.errorCode = errorCode;
        }

        public long getErrorCode() {
            return errorCode;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public interface LocationResponseListener {
        void updateLocation(Location location);

        void onError(ERROR_CODE errorCode);
    }
}
