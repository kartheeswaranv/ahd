package com.athomediva.helper;

import android.content.Context;
import android.net.Uri;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.logger.LogUtils;
import org.apache.http.client.utils.URIBuilder;
import www.zapluk.com.R;

import static com.athomediva.data.DeeplinkConstants.DEEPLINK_PARAMS.CART_ID;

/**
 * Created by kartheeswaran on 23/08/17.
 */

public class DeeplinkHelper {
    private static final String TAG = DeeplinkHelper.class.getSimpleName();

    public static Uri getUpdatedSubcatUri(Context context, Uri uri, long catId) {
        LogUtils.LOGD(TAG, "getUpdatedUri : ");
        uri = uri.buildUpon()
          .path(context.getString(R.string.deeplink_category))
          .appendQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.CAT_ID, String.valueOf(catId))
          .build();
        return uri;
    }

    public static Uri getUpdatedCartUri(Uri uri, String cartId) {
        LogUtils.LOGD(TAG, "getUpdatedUri : ");
        uri = uri.buildUpon().appendQueryParameter(CART_ID, cartId).build();
        return uri;
    }

    public static Uri getBookingDetailsUri(String orderId) {
        LogUtils.LOGD(TAG, "getUpdatedUri : ");
        Uri uri = Uri.parse("http://www.athomediva.com/order?orderId=" + orderId);

        return uri;
    }
}
