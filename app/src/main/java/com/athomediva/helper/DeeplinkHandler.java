package com.athomediva.helper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.DataValidator;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.busevents.CategoryLoadEvent;
import com.athomediva.data.managers.APIManager;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIAvailConfirmResponse;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGetMetaDataResponse;
import com.athomediva.data.models.remote.DeeplinkData;
import com.athomediva.data.models.remote.DeeplinkResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UTMUtils;
import com.google.gson.Gson;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import www.zapluk.com.R;

import static com.athomediva.data.DeeplinkConstants.DEEPLINK_PARAMS.AVAIL_CONFIRM_IDENTIFIER;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 22/08/17.
 */

public class DeeplinkHandler {
    private static final String TAG = LogUtils.makeLogTag(DeeplinkHandler.class.getSimpleName());
    private final int LOGIN_REQUEST_CODE = 3200;
    private WeakReference<Activity> mActivity;
    private DataManager mDataManager;
    private QuikrNetworkRequest mGetMetaDataReq;
    private QuikrNetworkRequest mGetDeeplinkDataReq;
    private QuikrNetworkRequest mGetUserDetailsReq;
    private QuikrNetworkRequest mConfirmAvailReq;
    private BookingSession mBookingSession;
    private DeeplinkCallbacks mCallbacks;
    private Uri mDeeplinkUri;
    private Context mContext;
    private UserSession mUserSession;

    public DeeplinkHandler(@NonNull WeakReference<Activity> activity, @NonNull DataManager dataManager,
      @NonNull BookingSession session, Bundle bundle, @NonNull UserSession userSession, DeeplinkCallbacks callbacks) {
        LOGD(TAG, "DeeplinkHandler : " + activity);
        mActivity = activity;
        mContext = mActivity.get().getApplicationContext();
        mDataManager = dataManager;
        mCallbacks = callbacks;
        mBookingSession = session;
        mUserSession = userSession;
    }

    public void setCallbackListener(DeeplinkCallbacks callbacks) {
        mCallbacks = callbacks;
    }

    public void handleDeeplinkBundle(Bundle bundle) {
        handleBundle(bundle);
    }

    private void fetchMetaData(DeeplinkConstants.DEEPLINK_TYPE type) {
        LogUtils.LOGD(TAG, "fetchMetaData : ");

        if (mGetMetaDataReq != null) mGetMetaDataReq.cancel();

        mGetMetaDataReq =
          mDataManager.getApiManager().getAppMetaData(new QuikrNetworkRequest.Callback<APIGetMetaDataResponse>() {
              @Override
              public void onSuccess(APIGetMetaDataResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  mBookingSession.setMetaData(response);
                  launchDeeplinkPage(type);
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
              }
          });

        mGetMetaDataReq.execute();
    }

    private void getUserDetails(DeeplinkConstants.DEEPLINK_TYPE type) {
        LogUtils.LOGD(TAG, "getUserDetails : ");

        if (mGetUserDetailsReq != null) mGetUserDetailsReq.cancel();

        mGetUserDetailsReq =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  mUserSession.updateDataFromResponse(response);
                  launchDeeplinkPage(type);
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
              }
          });

        mGetUserDetailsReq.execute();
    }

    private void confirmAvailabilitySubmit(DeeplinkConstants.DEEPLINK_TYPE type, String identifier) {
        LogUtils.LOGD(TAG, "confirmAvailabilitySubmit : ");

        if (mConfirmAvailReq != null) mConfirmAvailReq.cancel();

        mConfirmAvailReq = mDataManager.getApiManager()
          .availabilityConfirm(identifier, new QuikrNetworkRequest.Callback<APIAvailConfirmResponse>() {
              @Override
              public void onSuccess(APIAvailConfirmResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (response != null && response.isSuccess() && response.getData() != null) {
                      if (mCallbacks != null) {
                          mCallbacks.onSuccess(type, mDeeplinkUri);
                      }
                      mDeeplinkUri = DeeplinkHelper.getBookingDetailsUri(response.getData().getOrderId());
                      fetchdatasForDeeplink();
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : " + errorMessage);
              }
          });

        mConfirmAvailReq.execute();
    }

    public void getDeeplinkDatas(Uri path) {
        LOGD(TAG, "getDeeplinkDatas : ");
        if (path != null
          && !TextUtils.isEmpty(path.toString())
          && mBookingSession.getCurrentGeolocation() != null
          && mBookingSession.getCurrentGeolocation().getLatLng() != null) {
            if (mGetDeeplinkDataReq != null) mGetDeeplinkDataReq.cancel();

            mGetDeeplinkDataReq = APIManager.getDeeplinkDatas(path.toString(),
              String.valueOf(mBookingSession.getCurrentGeolocation().getLatLng().latitude),
              String.valueOf(mBookingSession.getCurrentGeolocation().getLatLng().longitude),
              new QuikrNetworkRequest.Callback<DeeplinkResponse>() {
                  @Override
                  public void onSuccess(DeeplinkResponse response) {
                      LOGD(TAG, "onSuccess : " + response);
                      handleDeepLinkResponse(response);
                  }

                  @Override
                  public void onError(int errorCode, String errorMessage) {
                      LOGD(TAG, "onError : " + errorMessage);
                  }
              });

            mGetDeeplinkDataReq.execute();
        } else {
            LOGD(TAG, "getDeeplinkDatasForUrl : Path or Location  is empty");
        }
    }

    public void handleDeepLinkResponse(DeeplinkResponse response) {
        LOGD(TAG, "handleDeepLinkResponse : ");
        if (response != null) {
            if (response.isSuccess() && response.getData() != null) {
                LOGD(TAG, "handleDeepLinkResponse :    success ");
                try {
                    DeeplinkData data = new Gson().fromJson(response.getData().getPayload(), DeeplinkData.class);
                    updateUriBasedOnResponse(response.getData().getType(), data);
                    launchDeeplinkPage(response.getData().getType());
                } catch (Exception e) {
                    e.printStackTrace();
                    ToastSingleton.getInstance().showToast(e.getMessage());
                }
            } else {
                launchDeeplinkPage(DeeplinkConstants.DEEPLINK_TYPE.WEB_VIEW_LOAD);
                LOGD(TAG, "handleDeepLinkResponse :   Failed");
            }
        }
    }

    private void updateUriBasedOnResponse(DeeplinkConstants.DEEPLINK_TYPE type, DeeplinkData data) {
        LOGD(TAG, "handleDeeplinkResponse : ");
        if (type == DeeplinkConstants.DEEPLINK_TYPE.CART_PAGE) {
            mDeeplinkUri = DeeplinkHelper.getUpdatedCartUri(mDeeplinkUri, data.getCartId());
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.CATEGORY) {
            if (!TextUtils.isEmpty(data.getCategoryId())
              && mBookingSession != null
              && mBookingSession.getCategoryData() != null
              && mBookingSession.getCategoryData().getData() != null) {
                long catId = Long.parseLong(data.getCategoryId());
                mDeeplinkUri = DeeplinkHelper.getUpdatedSubcatUri(mContext, mDeeplinkUri, catId);
            }
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.HOME_PAGE) {
            LOGD(TAG, "handleDeeplinkResponse : ");
        } else {
            ToastSingleton.getInstance().showToast("Deepling url Not Matching ");
        }
    }

    private void handleBundle(Bundle bundle) {
        LOGD(TAG, "handleBundle: ");
        if (bundle != null) {
            Uri uri = AndroidHelper.getDeepLinkUrl(bundle);
            if (uri != null) {
                mBookingSession.addUtmToSession(UTMUtils.getExtractUtmParams(uri));
                mDeeplinkUri = uri;
                if (mBookingSession.getCategoryData() != null) {
                    fetchdatasForDeeplink();
                }
            }
        }
    }

    private DeeplinkConstants.DEEPLINK_PATHS getDeepLinkPath(Uri uri) {
        LogUtils.LOGD(TAG, "getDeepLinkPath : ");
        for (DeeplinkConstants.DEEPLINK_PATHS path : DeeplinkConstants.DEEPLINK_PATHS.values()) {
            if (path.getPathRes() != -1) {
                String pathStr = mContext.getString(path.getPathRes());
                if (pathStr.replace("/", "").equalsIgnoreCase(uri.getLastPathSegment())) {
                    return path;
                }
            }
        }
        return DeeplinkConstants.DEEPLINK_PATHS.HOME_PAGE;
    }

    private void launchDeeplinkPage(DeeplinkConstants.DEEPLINK_TYPE type) {
        LogUtils.LOGD(TAG, "launchDeeplinkPage : " + mDeeplinkUri);

        if (mDeeplinkUri == null || !isViewValid()) {
            return;
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.AVAILABILITY_CONFIRM) {
            LogUtils.LOGD(TAG, "launchDeeplinkPage: Availability Confirm");
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.WEB_VIEW_LOAD) {
            LogUtils.LOGD(TAG, "launchDeeplinkPage : WEb View ");
            launchWebView(mDeeplinkUri.toString());
            mDeeplinkUri = null;
            return;
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.HOME_PAGE
          || type == DeeplinkConstants.DEEPLINK_TYPE.MY_ORDERS
          || type == DeeplinkConstants.DEEPLINK_TYPE.MY_ACCOUNTS) {
            LogUtils.LOGD(TAG, "launchDeeplinkPage : ");
            if (mCallbacks != null) mCallbacks.onSuccess(type, mDeeplinkUri);
            mDeeplinkUri = null;
            return;
        }

        LogUtils.LOGD(TAG, "launchDeeplinkPage  Uri: " + mDeeplinkUri.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setPackage(mContext.getPackageName());
        String scheme = mDeeplinkUri.getScheme();
        try {
            mDeeplinkUri = mDeeplinkUri.buildUpon().scheme(mContext.getString(R.string.internal_deeplink_scheme)).build();
            intent.setData(mDeeplinkUri);
            mActivity.get().startActivity(intent);
        } catch (ActivityNotFoundException e) {
            LOGD(TAG, "launchWebView : ");
            mDeeplinkUri = mDeeplinkUri.buildUpon().scheme(scheme).build();
            launchWebView(mDeeplinkUri.toString());
        }
        // Once We launch the page We need to Clear the deeplink uri
        mDeeplinkUri = null;
    }

    private void launchWebView(String url) {
        LogUtils.LOGD(TAG, "launchWebView : " + url);
        if (url == null || !isViewValid()) return;
        Bundle b = new Bundle();
        b.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.app_name));
        b.putString(WebViewFragment.PARAM_URL, mDeeplinkUri.toString());
        b.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
        Intent webViewIntent = new Intent(mActivity.get(), FragmentContainerActivity.class);
        webViewIntent.putExtras(b);
        mActivity.get().startActivity(webViewIntent);
    }

    public void onEventMainThread(CategoryLoadEvent event) {
        LOGD(TAG, "onEventMainThread : Category Load Event");
        // Get the deeplink datas on CategoryLoad if category data is not available
        fetchdatasForDeeplink();
    }

    private void fetchdatasForDeeplink() {
        LogUtils.LOGD(TAG, "fetchdatasForDeeplink : ");
        if (mDeeplinkUri == null || !isViewValid()) return;
        DeeplinkConstants.DEEPLINK_PATHS path = getDeepLinkPath(mDeeplinkUri);

        if (path.getDeeplinkType().isLoginNeed() && !DataValidator.isUserSessionExist(mUserSession)) {
            launchLoginPage(LOGIN_REQUEST_CODE);
        } else if (path.getDeeplinkApi() == DeeplinkConstants.DEEPLINK_API.DEEPLINK_DATA) {
            getDeeplinkDatas(mDeeplinkUri);
        } else if (path.getDeeplinkApi() == DeeplinkConstants.DEEPLINK_API.META_DATA) {
            fetchMetaData(path.getDeeplinkType());
        } else if (path.getDeeplinkApi() == DeeplinkConstants.DEEPLINK_API.AVAILABILITY_CONFIRM_API) {
            String encryptedOrderId = mDeeplinkUri.getQueryParameter(AVAIL_CONFIRM_IDENTIFIER);
            LogUtils.LOGD(TAG, "fetchdatasForDeeplink: Encrypted Id is " + encryptedOrderId);
            if (!TextUtils.isEmpty(encryptedOrderId)) {
                confirmAvailabilitySubmit(path.getDeeplinkType(), encryptedOrderId);
            } else {
                LogUtils.LOGD(TAG, "fetchdatasForDeeplink: Encrypted Id is empty");
            }
        } else {
            launchDeeplinkPage(path.getDeeplinkType());
        }
    }

    private boolean isViewValid() {
        if (mActivity != null && mActivity.get() != null) return true;

        return false;
    }

    private void launchLoginPage(int requestCode) {
        LOGD(TAG, "launchLoginPage: ");
        if (!isViewValid()) return;
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppLauncherContract.PARAM_LAUNCHED_FOR_LOGIN, true);
        Intent intent = new Intent(mActivity.get(), AppLauncherActivity.class);
        intent.putExtras(bundle);
        mActivity.get().startActivityForResult(intent, requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            fetchdatasForDeeplink();
        }
    }

    public void destroy() {
        LogUtils.LOGD(TAG, "destroy : ");
        if (mGetMetaDataReq != null) {
            mGetMetaDataReq.cancel();
        }

        if (mGetDeeplinkDataReq != null) {
            mGetDeeplinkDataReq.cancel();
        }

        if (mGetUserDetailsReq != null) {
            mGetUserDetailsReq.cancel();
        }
        mDeeplinkUri = null;
        mCallbacks = null;
    }

    public interface DeeplinkCallbacks {
        void onSuccess(DeeplinkConstants.DEEPLINK_TYPE type, Uri uri);
    }
}
