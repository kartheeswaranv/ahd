package com.athomediva.helper;

import android.widget.Toast;
import com.athomediva.AHDApplication;

public class ToastSingleton {
    private static ToastSingleton mInstance;

    private static Toast mSingleToast;

    public static ToastSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new ToastSingleton();
            mSingleToast = Toast.makeText(AHDApplication.sContext, "", Toast.LENGTH_LONG);
        }
        return mInstance;
    }

    public void showToast(String iMessage) {
        if (mSingleToast != null) {
            mSingleToast.setText(iMessage);
            mSingleToast.show();
        }
    }

    public void showToast(int iMessage) {
        if (mSingleToast != null) {
            mSingleToast.setText(AHDApplication.sContext.getString(iMessage));
            mSingleToast.show();
        }
    }
}
