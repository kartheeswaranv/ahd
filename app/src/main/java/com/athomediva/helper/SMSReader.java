package com.athomediva.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.athomediva.data.AppConstants;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 */
public abstract class SMSReader extends BroadcastReceiver {
    public static final String SMS_ORIGIN = AppConstants.SMS_SENDER;
    public static final String OTP_DELIMITER = AppConstants.OTP_DELIMITER;
    private static final String TAG = SMSReader.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object aPdusObj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    LOGD(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);

                    // if the SMS is not from our gateway, ignore the message
                    if (!senderAddress.toLowerCase().contains(SMS_ORIGIN.toLowerCase())) {
                        return;
                    }

                    // verification code from sms
                    String verificationCode = getVerificationCode(message);

                    if (verificationCode.length() == AppConstants.OTP_LENGTH) {
                        GotVerifyCode(verificationCode);
                    }
                    LOGD(TAG, "OTP received: " + verificationCode);
                }
            }
        } catch (Exception e) {
            LOGE(TAG, "Exception: " + e.getMessage());
        }
    }

    protected abstract void GotVerifyCode(String Code);

    /**
     * Getting the OTP from sms message body
     * ':' is the separator of OTP from the message
     */
    private String getVerificationCode(String message) {
        String code = null;
        int index = message.indexOf(OTP_DELIMITER) + 1;

        if (index != -1) {
            int start = index + 2;

            code = message.substring(start, start + AppConstants.OTP_LENGTH);
            return code;
        }

        return code;
    }
}