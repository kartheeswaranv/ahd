package com.athomediva.helper;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import com.athomediva.data.models.local.Contact;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import rx.Observer;
import rx.Subscriber;

/**
 * Created by kartheeswaran on 29/08/17.
 */

public class RxContacts {
    private static final String TAG = LogUtils.makeLogTag(RxContacts.class.getSimpleName());

    private static final String REGEXP_MOBILE_NO = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    private static final int CONTACT_EMIT_LIMIT = 10;

    private ContentResolver mResolver;

    public static rx.Observable<List<Contact>> fetch(@NonNull final Context context) {
        return rx.Observable.create(new rx.Observable.OnSubscribe<List<Contact>>() {
            @Override
            public void call(Subscriber<? super List<Contact>> subscriber) {
                new RxContacts(context).fetch(subscriber);
            }
        });
    }

    private RxContacts(@NonNull Context context) {
        LogUtils.LOGD(TAG, "RxContacts : ");
        mResolver = context.getContentResolver();
    }

    private void fetch(Observer emitter) {
        LogUtils.LOGD(TAG, "fetch : ");
        LinkedHashMap<String, Contact> contacts = new LinkedHashMap<>();
        Cursor cursor = createCursor();
        if (cursor == null || cursor.getCount() == 0) {
            emitter.onError(new Throwable("No Data Found"));
        }
        cursor.moveToFirst();
        int displayNamePrimaryColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int phoneNumberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        LogUtils.LOGD(TAG, "fetch : Count " + cursor.getCount());
        while (!cursor.isAfterLast()) {

            String displayName = getDisplayName(cursor, displayNamePrimaryColumnIndex);
            String phoneNumber = getPhoneNumber(cursor, phoneNumberIndex);

            if (phoneNumber.length() >= 10 && phoneNumber.matches(REGEXP_MOBILE_NO)) {
                phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
                if (!contacts.containsKey(phoneNumber)) {
                    Contact contact1 = new Contact(phoneNumber, displayName);
                    contacts.put(phoneNumber, contact1);
                }
            }

            doNext(contacts, emitter, cursor.isLast());

            cursor.moveToNext();
        }
        cursor.close();

        emitter.onCompleted();
    }

    private Cursor createCursor() {
        LogUtils.LOGD(TAG, "createCursor : ");
        Cursor cursorPhone = mResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[] {
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
          },

          "LENGTH(" + ContactsContract.CommonDataKinds.Phone.NUMBER + ") >=10 ", null,
          ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");

        return cursorPhone;
    }

    private String getPhoneNumber(Cursor cursor, int index) {
        if (cursor == null) return null;
        String phoneNumber = cursor.getString(index);
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            // Remove all whitespaces, Special Characters
            phoneNumber = phoneNumber.replaceAll("\\s+", "");
            phoneNumber = phoneNumber.replaceAll("\\(", "");
            phoneNumber = phoneNumber.replaceAll("\\)", "");
            phoneNumber = phoneNumber.replaceAll("-", "");
        }
        return phoneNumber;
    }

    private String getDisplayName(Cursor cursor, int index) {
        if (cursor == null) return null;
        String displayName = cursor.getString(index);
        return displayName;
    }

    private void doNext(HashMap<String, Contact> contacts, Observer emitter, boolean isLast) {
        LogUtils.LOGD(TAG, "doNext : ");
        if (contacts != null && (contacts.size() % CONTACT_EMIT_LIMIT == 0 || isLast)) {
            emitter.onNext(new ArrayList<>(contacts.values()));
        }
    }
}