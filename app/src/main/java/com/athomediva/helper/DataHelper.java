package com.athomediva.helper;

import android.text.TextUtils;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APIGooglePlaceByIDResponse;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.TimeSlot;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

import static com.athomediva.data.AppConstants.PINCODE_PREFIX;

/**
 * Created by kartheeswaran on 02/05/17.
 */
//TODO move it to data processor
public class DataHelper {
    private static final String TAG = DataHelper.class.getSimpleName();

    public static void handleDateSelection(List<DateSlot> slots, DateSlot selectDate) {
        LogUtils.LOGD(TAG, "handleDateSelection : ");
        if (slots != null && slots.size() > 0 && selectDate != null) {
            for (DateSlot date : slots) {
                if (date.equals(selectDate)) {
                    date.setSelected(true);
                } else {
                    date.setSelected(false);
                    clearTimeSelection(date.getSelectedTimeSlot());
                }
            }
        }
    }

    public static void handleTimeSelection(List<TimeSlot> slots, TimeSlot timeSlot) {
        LogUtils.LOGD(TAG, "handleTimeSelection : ");
        if (slots != null && slots.size() > 0 && timeSlot != null) {
            for (TimeSlot slot : slots) {
                if (slot.equals(timeSlot)) {
                    slot.setSelected(true);
                } else {
                    slot.setSelected(false);
                }
            }
        }
    }

    public static void clearTimeSelection(TimeSlot slot) {
        LogUtils.LOGD(TAG, "clearTimeSelection : ");
        if (slot != null) {
            slot.setSelected(false);
        }
    }

    public static void updateSpecialDateParams(ArrayList<SpecialOfferDateModel> list, HashMap<String, Object> params) {
        LogUtils.LOGD(TAG, "updateSpecialDateParams : ");
        if (list == null || params == null) return;

        for (SpecialOfferDateModel model : list) {
            params.put(model.getApiParam(), UiUtils.getFormatDateForSpecialOffer(model.getSelectedDate()));
        }
    }

    public static void updateSpecialDateParams(ArrayList<SpecialOfferDateModel> list, JSONObject params) {
        LogUtils.LOGD(TAG, "updateSpecialDateParams : ");
        if (list == null || params == null) return;

        try {
            for (SpecialOfferDateModel model : list) {
                params.put(model.getApiParam(), UiUtils.getFormatDateForSpecialOffer(model.getSelectedDate()));
            }
        } catch (JSONException e) {
            LogUtils.LOGD(TAG, "updateSpecialDateParams :Json Exception ");
        }
    }

    public static String getPinCodeFromAddress(String address) {

        if (!TextUtils.isEmpty(address) && address.lastIndexOf(PINCODE_PREFIX) > -1) {
            String pincode =
              address.substring(address.lastIndexOf(PINCODE_PREFIX) + PINCODE_PREFIX.length(), address.length());
            LogUtils.LOGD(TAG, "getPinCodeFromAddress: PinCode " + pincode);
            return pincode;
        }
        LogUtils.LOGD(TAG, "getPinCodeFromAddress: PinCode null");
        return null;
    }

    public static String clearAddressPinCodePrefix(String address, String pinCode) {
        if (!TextUtils.isEmpty(address) && address.lastIndexOf(PINCODE_PREFIX + pinCode) > -1) {
            String trimeaddress = address.substring(address.lastIndexOf(PINCODE_PREFIX + pinCode));
            LogUtils.LOGD(TAG, "clearAddressPinCodePrefix: Trim Add " + trimeaddress);
            String addTrim = address.replace(trimeaddress, "");
            LogUtils.LOGD(TAG, "clearAddressPinCodePrefix: " + addTrim);

            return addTrim;
        }

        return null;
    }

    public static String getPinCodeFromAddressComponent(List<APIGooglePlaceByIDResponse.AddressComponents> components) {
        if (components != null && components.size() > 0) {
            for (APIGooglePlaceByIDResponse.AddressComponents comp : components) {
                if (comp.getTypes() != null && comp.getTypes().size() > 0) {
                    LogUtils.LOGD(TAG, "getPinCodeFromAddressComponent: " + comp.getTypes().toString());
                    if (comp.getTypes().toString().contains("postal_code")) {
                        String pinCode = comp.getLongName();
                        return pinCode;
                    }
                }
            }
        }

        return null;
    }
}
