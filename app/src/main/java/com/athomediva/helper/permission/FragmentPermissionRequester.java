package com.athomediva.helper.permission;

import android.support.v4.app.Fragment;
import com.athomediva.helper.AndroidHelper;
import java.lang.ref.WeakReference;

/**
 * Created by mohitkumar on 02/05/17.
 */

public class FragmentPermissionRequester implements PermissionManager.PermissionExecutor {
    private final WeakReference<Fragment> weakReference;

    public FragmentPermissionRequester(WeakReference<Fragment> weakReference) {
        this.weakReference = weakReference;
    }

    @Override
    public void requestPermission(String[] arrayPermission, int reqCode) {
        if (weakReference == null || weakReference.get() == null || weakReference.get().getActivity() == null) {
            return;
        }

        weakReference.get().requestPermissions(arrayPermission, reqCode);
    }

    @Override
    public void requestSettingsPage(int reqCode) {
        if (weakReference == null || weakReference.get() == null || weakReference.get().getActivity() == null) {
            return;
        }
        weakReference.get().startActivityForResult(AndroidHelper.getAppPermissionSettingsIntent(), reqCode);
    }
}
