package com.athomediva.helper.permission;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import com.athomediva.data.models.local.Permission;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import www.zapluk.com.R;

import static com.athomediva.helper.AndroidHelper.getDeniedPermission;
import static com.athomediva.helper.AndroidHelper.getMappedPermissionFromAndroidPermission;
import static com.athomediva.helper.AndroidHelper.getPermissionList;
import static com.athomediva.helper.AndroidHelper.getPermissions;
import static com.athomediva.helper.AndroidHelper.isAllPermissionGranted;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 27/11/16.
 * This Helper class is used to handle all permission related stuff
 * If permissions process is done, it will send call back to caller.
 * Otherwise it will request permissions
 *
 * Permission that have showInfoDialog flag as true, Show Settings options in
 * the Rationale dialog to enable the particular permission if it is always denied
 * If this dialog is cancelled, then exit the app
 */

public class PermissionManager implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = LogUtils.makeLogTag(PermissionManager.class.getSimpleName());
    private final int PERMISSION_REQUEST_CODE = 12345;
    private final int SETTINGS_REQUEST_CODE = 12346;
    private final WeakReference<Activity> mActivity;
    private final Context mContext;
    private final PermissionExecutor mPermissionRequester;
    private PermissionCallback mCallback;
    private boolean isRequestRunning;
    private ArrayList<Permission> mPermissionList;
    private AlertDialog mDialog;

    public PermissionManager(@NonNull WeakReference<Activity> activity, @NonNull PermissionExecutor permissionRequester) {
        LOGD(TAG, "PermissionManager : " + activity);
        mActivity = activity;
        mContext = activity.get().getApplicationContext();
        mPermissionRequester = permissionRequester;
    }

    /**
     * To check all permissions and request permissions if it is not granted.
     * This is exposed outside world and everyone should pass their permission object.
     * Client should pass the cancel settings listener if they want to explicitly handle the
     * permissions denied dismissDialog behaviour
     */
    public void checkAndRequestForPermissions(@NonNull ArrayList<Permission> permissions,
      @NonNull PermissionCallback callback) {
        LOGD(TAG, "checkAndRequestForPermissions : " + permissions + " Callback " + callback);

        if (callback == null || !isSessionValid()) return;

        String[] permissionArr = getPermissions(permissions);
        mPermissionList = permissions;
        checkAndExecutePermission(permissionArr, callback);
    }

    private void checkAndExecutePermission(String[] permissions, PermissionCallback callback) {
        if (!AndroidHelper.IsMarshmallowVersion()) {
            callback.onPermissionResult(true, permissions, null);
            return;
        }

        isRequestRunning = true;
        mCallback = callback;
        boolean isAllPermissionGranted = isAllPermissionGranted(mContext, permissions);
        if (!isAllPermissionGranted) {
            mPermissionRequester.requestPermission(permissions, PERMISSION_REQUEST_CODE);
        } else {
            setPermissionResult(isAllPermissionGranted, permissions, null);
        }
    }

    private boolean isSessionValid() {
        return !(mActivity == null || mActivity.get() == null || mPermissionRequester == null);
    }

    private boolean isPermissionCallbackValid() {
        return mCallback != null && isSessionValid() && isRequestRunning;
    }

    /**
     * This is callback method  to handle the permission results coming from Presenter or Activity
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult : "
          + requestCode
          + "  Permissions "
          + Arrays.toString(permissions)
          + "  Result "
          + Arrays.toString(grantResults));
        boolean isShowRationaleDialog = false;
        if (!isPermissionCallbackValid()) return;

        if (requestCode == PERMISSION_REQUEST_CODE) {
            boolean isAllAreGranted = isAllPermissionGranted(permissions, grantResults);
            String[] grantedPermission = getPermissionList(PackageManager.PERMISSION_GRANTED, permissions, grantResults);
            String[] deniedPermission = getPermissionList(PackageManager.PERMISSION_DENIED, permissions, grantResults);
            if (!isAllAreGranted) {
                String deniedPermissionItem = getDeniedPermission(permissions, grantResults);
                Permission permission = getMappedPermissionFromAndroidPermission(mPermissionList, deniedPermissionItem);
                if (permission.isShouldShowInfoDialog()) {
                    isShowRationaleDialog = true;
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity.get(), deniedPermissionItem)) {
                        showRationaleMessageDialog(permission.getMessage());
                    } else {
                        showPermissionSettingsDialog(permission.getMessage());
                    }
                }
            }
            LOGD(TAG, "onRequestPermissionsResult : Show Rationale Dialog " + isShowRationaleDialog);
            if (!isShowRationaleDialog) {
                setPermissionResult(isAllAreGranted, grantedPermission, deniedPermission);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: reqCode = " + requestCode);

        if (requestCode == SETTINGS_REQUEST_CODE) checkForPermissionAndSendCallback();
    }

    private void showPermissionSettingsDialog(String message) {
        LOGD(TAG, "showPermissionSettingsDialog : ");
        dismissDialog();
        if (mActivity == null || mActivity.get() == null) return;

        mDialog = new AlertDialog.Builder(mActivity.get()).create();
        mDialog.setMessage(message);
        mDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getString(R.string.settings_permission_btn),
          (dialogInterface, i) -> mPermissionRequester.requestSettingsPage(SETTINGS_REQUEST_CODE));
        mDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getString(R.string.cancel_permission_btn),
          (dialogInterface, i) -> {
              checkForPermissionAndSendCallback();
              dismissDialog();
          });
        mDialog.show();
    }

    private void showRationaleMessageDialog(String message) {
        LOGD(TAG, "showRationaleMessageDialog : ");
        dismissDialog();
        if (mActivity == null || mActivity.get() == null) return;
        mDialog = new AlertDialog.Builder(mActivity.get()).create();
        mDialog.setMessage(message);
        mDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getString(R.string.ok), (dialogInterface, i) -> {
            checkForPermissionAndSendCallback();
            dismissDialog();
        });

        mDialog.setOnDismissListener(dialog -> resetPermissionRequest());
        mDialog.show();
    }

    //TODO pass the exact permission filter failed and granted
    private void checkForPermissionAndSendCallback() {
        if (!isPermissionCallbackValid()) return;

        String[] permissionArr = getPermissions(mPermissionList);
        boolean isAllPermissionGranted = isAllPermissionGranted(mContext, permissionArr);
        if (isAllPermissionGranted) {
            setPermissionResult(isAllPermissionGranted, permissionArr, null);
        } else {
            setPermissionResult(isAllPermissionGranted, null, permissionArr);
        }
    }

    private void setPermissionResult(boolean granted, String[] grantedPerm, String[] deniedPermission) {
        LOGD(TAG, "setPermissionResult: ");
        if (mCallback != null) mCallback.onPermissionResult(granted, grantedPerm, deniedPermission);
        resetPermissionRequest();
    }

    private void resetPermissionRequest() {
        mCallback = null;
        isRequestRunning = false;
    }

    private void dismissDialog() {
        LOGD(TAG, "dismissDialog : ");
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public interface PermissionCallback {
        void onPermissionResult(boolean isAllAreGranted, String[] grantedPermission, String[] deniedPermission);
    }

    interface PermissionExecutor {
        void requestPermission(String[] arrayPermission, int reqCode);

        void requestSettingsPage(int reqCode);
    }
}
