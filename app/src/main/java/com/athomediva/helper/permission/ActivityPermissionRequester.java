package com.athomediva.helper.permission;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import com.athomediva.helper.AndroidHelper;
import java.lang.ref.WeakReference;

/**
 * Created by mohitkumar on 02/05/17.
 */

public class ActivityPermissionRequester implements PermissionManager.PermissionExecutor {
    private final WeakReference<Activity> weakReference;

    public ActivityPermissionRequester(WeakReference<Activity> weakReference) {
        this.weakReference = weakReference;
    }

    @Override
    public void requestPermission(String[] arrayPermission, int reqCode) {
        if (weakReference == null || weakReference.get() == null) {
            return;
        }
        ActivityCompat.requestPermissions(weakReference.get(), arrayPermission, reqCode);
    }

    @Override
    public void requestSettingsPage(int reqCode) {
        if (weakReference == null || weakReference.get() == null) {
            return;
        }
        weakReference.get().startActivityForResult(AndroidHelper.getAppPermissionSettingsIntent(), reqCode);
    }
}