package com.athomediva.helper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 03/05/17.
 */

public class AnimationHelper {

    public static TranslateAnimation getLoginBottomAnimation() {
        TranslateAnimation bottomSlideUpAnimation =
          new TranslateAnimation(Animation.ABSOLUTE, 0f, Animation.ABSOLUTE, 0f, Animation.RELATIVE_TO_SELF, 0.2f,
            Animation.RELATIVE_TO_SELF, 0f);
        bottomSlideUpAnimation.setDuration(300);
        return bottomSlideUpAnimation;
    }

    public static TranslateAnimation getPromoSlideUpAnimation() {
        TranslateAnimation bottomSlideUpAnimation =
          new TranslateAnimation(Animation.ABSOLUTE, 0f, Animation.ABSOLUTE, 0f, Animation.RELATIVE_TO_PARENT, 0.1f,
            Animation.RELATIVE_TO_SELF, 0f);
        bottomSlideUpAnimation.setDuration(300);
        return bottomSlideUpAnimation;
    }

    public static TranslateAnimation getToolbarSlideDownAnimation() {
        TranslateAnimation bottomSlideUpAnimation =
          new TranslateAnimation(Animation.ABSOLUTE, 0f, Animation.ABSOLUTE, 0f, Animation.RELATIVE_TO_PARENT, -1.0f,
            Animation.RELATIVE_TO_SELF, 0f);
        bottomSlideUpAnimation.setDuration(300);
        return bottomSlideUpAnimation;
    }

    /**
     * Animate Arrow view based on the rotation start and end angle
     */
    public static void rotateViewAnimation(View view, float rotationStart, float rotationEnd) {
        rotateViewAnimation(view, rotationStart, rotationEnd, null);
    }

    public static void rotateViewAnimation(View view, float rotationStart, float rotationEnd,
      Animation.AnimationListener listener) {
        AnimationSet animSet = new AnimationSet(true);
        animSet.setInterpolator(new DecelerateInterpolator());
        animSet.setFillAfter(true);
        animSet.setFillEnabled(true);
        TypedValue outValue = new TypedValue();
        view.getContext().getResources().getValue(R.dimen.view_rotation_pivotX, outValue, true);
        final RotateAnimation animRotate =
          new RotateAnimation(rotationStart, rotationEnd, RotateAnimation.RELATIVE_TO_SELF, outValue.getFloat(),
            RotateAnimation.RELATIVE_TO_SELF, outValue.getFloat());

        animRotate.setDuration(view.getContext().getResources().getInteger(R.integer.view_rotation_animation));
        animRotate.setFillAfter(true);
        animSet.addAnimation(animRotate);
        if (listener != null) {
            animSet.setAnimationListener(listener);
        }

        view.startAnimation(animSet);
    }

    public static ValueAnimator expandAnimation(View view, int duration, Animator.AnimatorListener listener) {
        final boolean expand = view.getVisibility() != View.VISIBLE;

        int prevHeight = view.getHeight();
        int height = 0;
        if (expand) {
            int measureSpecParams = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
            view.measure(measureSpecParams, measureSpecParams);
            height = view.getMeasuredHeight();
        }

        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, height);
        valueAnimator.addUpdateListener(animation -> {
            if (view != null) {
                view.getLayoutParams().height = (int) animation.getAnimatedValue();
                view.requestLayout();
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view == null) return;
                if (expand) {
                    view.setVisibility(View.VISIBLE);
                }
                if (listener != null) {
                    listener.onAnimationStart(animation);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view == null) return;

                if (!expand) {
                    view.setVisibility(View.INVISIBLE);
                }

                if (listener != null) {
                    listener.onAnimationEnd(animation);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();

        return valueAnimator;
    }

    public static void flipAnimation(Context context, View view, AnimatorListenerAdapter listener) {

        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "rotationY", 0.0f, 180.0f);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "rotationY", 180.0f, 0.0f);
        anim.setDuration(600);
        anim1.setDuration(1000);

        AnimatorSet set = new AnimatorSet();
        set.addListener(new AnimatorListenerAdapter() {

            private boolean mCanceled;

            @Override
            public void onAnimationStart(Animator animation) {
                mCanceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCanceled = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!mCanceled) {
                    animation.start();
                }

                /*if(listener != null) {
                    listener.onAnimationEnd(animation);
                }*/
            }
        });
        set.playSequentially(anim, anim1);
        set.start();
    }
}
