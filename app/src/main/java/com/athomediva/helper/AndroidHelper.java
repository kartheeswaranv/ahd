package com.athomediva.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.provider.MediaStore;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.models.remote.Referral;
import com.athomediva.logger.LogUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import www.zapluk.com.BuildConfig;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 19/04/17.
 */

public class AndroidHelper {

    private static final String TAG = LogUtils.makeLogTag(AndroidHelper.class.getSimpleName());

    @DebugLog
    public static void sendEmail(Context context, String subject, String[] TO) {
        if (TO == null || TO.length < 1) {
            TO = new String[1];
            TO[0] = "";
        }
        ResolveInfo best = null;
        StringBuilder result = new StringBuilder();
        for (String string : TO) {
            result.append(string);
            result.append(",");
        }

        Intent intent = new Intent(Intent.ACTION_VIEW,
          Uri.parse("mailto:" + (result.length() > 0 ? result.substring(0, result.length() - 1) : "")));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_EMAIL, TO);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        try {
            for (final ResolveInfo info : matches)
                if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase()
                  .contains("gmail")) {
                    best = info;
                }
            if (best != null) {
                intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
            }
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ToastSingleton.getInstance().showToast("There is no email client installed.");
        }
    }

    @DebugLog
    public static boolean isOTPPermissionGranted(Context context) {
        ArrayList<String> list = new ArrayList<>();
        list.add(Manifest.permission.READ_SMS);
        list.add(Manifest.permission.RECEIVE_SMS);
        return isAllPermissionGranted(context, list.toArray(new String[list.size()]));
    }

    @DebugLog
    public static boolean copyTextToClipboard(Context context, String value) {
        ClipData myClip = ClipData.newPlainText("text", value);
        ClipboardManager myClipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        myClipboard.setPrimaryClip(myClip);
        return true;
    }

    @DebugLog
    public static List<SocialSharingItem> getMessageApps(Context context, Referral referral) {
        List<SocialSharingItem> itemList = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();
        // Get activities that can handle the intent
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");

        List<ResolveInfo> activitiesOld = packageManager.queryIntentActivities(intent, 0);
        // Check if 1 or more were returned
        if (activitiesOld == null || activitiesOld.size() <= 0) return null;

        int max =
          activitiesOld.size() > AppConstants.MAX_SOCIAL_ITEMS ? AppConstants.MAX_SOCIAL_ITEMS : activitiesOld.size();

        List<ResolveInfo> activities = reorderPackageBasedOnPriority(activitiesOld);
        for (int i = 0; i < max; i++) {
            ResolveInfo info = activities.get(i);
            Intent launchIntent = new Intent(Intent.ACTION_SEND);
            launchIntent.setType("text/plain");
            launchIntent.putExtra(android.content.Intent.EXTRA_TEXT, referral.getIfContent() + referral.getIfRefUrl());
            intent.putExtra(Intent.EXTRA_SUBJECT, referral.getIfSubject());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
            itemList.add(
              new SocialSharingItem(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), launchIntent));
        }

        if (activities.size() > AppConstants.MAX_SOCIAL_ITEMS) {
            Intent launchIntent = new Intent(Intent.ACTION_SEND);
            launchIntent.setType("text/plain");
            launchIntent.putExtra(android.content.Intent.EXTRA_TEXT, referral.getIfContent() + referral.getIfRefUrl());
            intent.putExtra(Intent.EXTRA_SUBJECT, referral.getIfSubject());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Intent chooser = Intent.createChooser(launchIntent, context.getString(R.string.share_with_firends));
            itemList.add(new SocialSharingItem(context.getString(R.string.more),
              ContextCompat.getDrawable(context, R.drawable.ahd_ic_more_horiz), chooser));
            return itemList;
        }

        return itemList;
    }

    private static List<ResolveInfo> reorderPackageBasedOnPriority(List<ResolveInfo> activities) {
        LOGD(TAG, "reorderPackageBasedOnPriority: ");
        List<ResolveInfo> prioritizedList = new ArrayList<>();

        for (String packageName : AppConstants.SOCIAL_NETWORK_PACKAGE_PRIORITIZED_LIST) {
            if (prioritizedList.size() < AppConstants.MAX_SOCIAL_ITEMS) {
                ResolveInfo info = isPackageNamePresent(packageName, activities);
                if (info != null) {
                    prioritizedList.add(info);
                    activities.remove(info);
                }
            }
        }

        prioritizedList.addAll(activities);
        return prioritizedList;
    }

    private static ResolveInfo isPackageNamePresent(String packageName, List<ResolveInfo> activities) {
        for (ResolveInfo resolveInfo : activities) {
            if (packageName.equalsIgnoreCase(resolveInfo.activityInfo.packageName)) {
                return resolveInfo;
            }
        }

        return null;
    }

    public static void launchViewForAction(@NonNull Context context, @NonNull Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        LOGD(TAG, "launchViewForAction: context=" + context);
        context.startActivity(intent);
    }

    @DebugLog
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static Bitmap blur(Context context, Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(AppConstants.BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    @DebugLog
    public static Bitmap getRoundedBitmap(Bitmap bitmap, int width, int height) {
        if (width > height) width = height;
        Bitmap circleBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);

        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(width / 2, height / 2, width / 2, paint);
        return circleBitmap;
    }

    public static void callToPhoneNumber(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    //move email to common method
    public static void sendSupportEmail(Context context) {
        String[] TO = { AppConstants.AHD_EMAIL_ID };
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("vnd.android.cursor.item/email");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.email_support));
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Intent chooserIntent = Intent.createChooser(emailIntent, context.getString(R.string.sending_email));
            chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(chooserIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            ToastSingleton.getInstance().showToast(context.getString(R.string.no_email_client_installed));
        }
    }

    @DebugLog
    public static boolean isLocationEnabledInSettings(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders =
              Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static Intent getGPSSettingsIntent(Context context) {
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        return new Intent(action);
    }

    public static ArrayList<Permission> getLocationPermissionList(Context context, boolean mandatory) {
        ArrayList<Permission> locationList = new ArrayList<>();
        locationList.add(new Permission(Manifest.permission.ACCESS_FINE_LOCATION, mandatory,
          context.getString(R.string.perm_location_msg)));
        locationList.add(new Permission(Manifest.permission.ACCESS_COARSE_LOCATION, mandatory,
          context.getString(R.string.perm_location_msg)));
        return locationList;
    }

    @DebugLog
    public static boolean isLocationPermissionGranted(Context context) {
        ArrayList<String> list = new ArrayList<>();
        list.add(Manifest.permission.ACCESS_FINE_LOCATION);
        list.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        return isAllPermissionGranted(context, list.toArray(new String[list.size()]));
    }

    /**
     * Get the list of granted or denied permissions  based on the type
     */
    public static String[] getPermissionList(int type, String[] permission, int[] grantResult) {
        LOGD(TAG, "getPermissionList : ");
        if (permission != null && grantResult != null) {
            ArrayList<String> list = new ArrayList<>();
            for (int index = 0; index < permission.length; index++) {
                if (grantResult[index] == type) {
                    list.add(permission[index]);
                }
            }

            return list.toArray(new String[list.size()]);
        }

        return null;
    }

    /**
     * Get Permission Obj  if it is available in mPermissionList
     */
    public static Permission getMappedPermissionFromAndroidPermission(ArrayList<Permission> permissionList, String perm) {
        LOGD(TAG, "getMappedPermissionFromAndroidPermission : ");
        Permission permission = new Permission(perm);
        if (permissionList != null) {
            for (Permission p : permissionList) {
                if (p.getPermission().equals(perm)) {
                    return p;
                }
            }
        }
        return permission;
    }

    /**
     * To check all android permissions are granted based on the permissions object
     */
    public static String[] getPermissions(ArrayList<Permission> permissions) {
        LOGD(TAG, "getPermissions : ");
        if (permissions != null) {
            String[] list = new String[permissions.size()];
            for (int index = 0; index < permissions.size(); index++) {
                list[index] = permissions.get(index).getPermission();
            }
            return list;
        }

        return null;
    }

    /**
     * To check all permissions are granted based on the permissions
     */
    public static boolean isAllPermissionGranted(@NonNull Context context, String[] permissions) {
        LOGD(TAG, "isAllPermissionGranted : " + Arrays.toString(permissions));
        if (permissions == null) return true;

        boolean isAllPermissionGranted = true;
        for (String permission : permissions) {
            if (!isPermissionGranted(context, permission)) {
                isAllPermissionGranted = false;
                break;
            }
        }
        LOGD(TAG, "isAllPermissionGranted : " + isAllPermissionGranted);
        return isAllPermissionGranted;
    }

    /**
     * To check all permissions are granted based on the result of OnRequestPermissionResult
     */

    public static boolean isAllPermissionGranted(String[] permissions, int[] result) {
        LOGD(TAG, "isAllPermissionGranted : " + Arrays.toString(permissions) + " Results " + Arrays.toString(result));
        boolean isAllPermissionGranted = true;
        if (permissions != null) {
            for (int index = 0; index < permissions.length; index++) {
                if (result[index] != PackageManager.PERMISSION_GRANTED) {
                    isAllPermissionGranted = false;
                    break;
                }
            }
        }
        return isAllPermissionGranted;
    }

    /**
     * To get all Denied Permission based on the result onRequestPermissionResult
     */
    public static String getDeniedPermission(String[] permissions, int[] result) {
        LOGD(TAG, "isAllPermissionGranted : " + Arrays.toString(permissions) + " Results " + Arrays.toString(result));
        if (permissions != null) {
            for (int index = 0; index < permissions.length; index++) {
                if (result[index] != PackageManager.PERMISSION_GRANTED) {
                    return permissions[index];
                }
            }
        }
        return null;
    }

    public static boolean isPermissionGranted(@NonNull Context context, String permission) {
        return context.checkPermission(permission, android.os.Process.myPid(), Process.myUid())
          == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean IsMarshmallowVersion() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static String GetVersionCode() {
        //return android.os.Build.VERSION.SDK_INT;
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release + ")";
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getScreenDimensions(Context cont) {
        Display display = ((WindowManager) cont.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        String screenmode;
        DisplayMetrics dm = cont.getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        int screenSize = cont.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                screenmode = "L";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                screenmode = "N";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                screenmode = "S";
                break;
            default:
                screenmode = "O";
                break;
        }
        return width + " X " + height + "  ( " + density + " - " + String.format("%.2f", screenInches) + ") -" + screenmode;
    }

    public static String getMID(Context context) {
         /*Retrive Device ID*/
        String mid = null;
        try {
            TelephonyManager phonespecs = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            mid = phonespecs.getDeviceId();
            String macAddr, androidId;
            if (mid == null) {
                WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInf = wifiMan.getConnectionInfo();
                macAddr = wifiInf.getMacAddress();
                androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(),
                  android.provider.Settings.Secure.ANDROID_ID);
                UUID deviceUuid = new UUID(androidId.hashCode(), macAddr.hashCode());
                mid = deviceUuid.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mid;
    }

    public static void replaceFragmentWithFadeInAnimation(@NonNull FragmentManager fragmentManager, int container,
      @NonNull Fragment fragment, @NonNull String tag) {
        fragmentManager.beginTransaction()
          .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
          .replace(container, fragment, tag)
          .commitAllowingStateLoss();
    }

    public static void replaceFragment(@NonNull FragmentManager fragmentManager, int container, @NonNull Fragment fragment,
      @NonNull String tag) {
        fragmentManager.beginTransaction().replace(container, fragment, tag).commitAllowingStateLoss();
    }

    public static Intent getAppPermissionSettingsIntent() {
        Intent myAppSettings =
          new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);

        return myAppSettings;
    }

    public static void goToPlayStore(Context context) {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(
              new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            Intent intent =
              new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    @DebugLog
    public static Intent getCameraIntent(Uri outputImageUri) {
        LOGD(TAG, "getCameraIntent : ");
        Intent captureImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        captureImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageUri);
        return captureImageIntent;
    }

    public static Intent getImagePickerIntent(Context context) {
        String googlePickerActivity = getGooglePhotoPickerActivity(context);
        if (googlePickerActivity == null) {
            Intent getIntent = null;
            getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            getIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            Intent chooserIntent = Intent.createChooser(getIntent, "Pick an image");
            return chooserIntent;
        } else {
            Intent handle;
            handle = new Intent();
            handle.setAction(Intent.ACTION_PICK);
            handle.setType("image/*");
            handle.setComponent(new ComponentName(GOOGLE_PHOTOS_PACKAGE_NAME, googlePickerActivity));
            return handle;
        }
    }

    private static final String GOOGLE_PHOTOS_PACKAGE_NAME = "com.google.android.apps.photos";
    private static final String GooglePhotoPreference = "GooglePhotoPreference";
    private static SharedPreferences mSharedpreferences;
    private static final String GooglePhotoPickerActivity = "PhotoPickerActivity";

    public static void setPhotoPickerActivity(Context context) {
        int i = 0;
        Intent handle = new Intent();
        handle.setAction(Intent.ACTION_PICK);
        handle.setType("image/*");
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(handle, 0);
        mSharedpreferences = context.getSharedPreferences(GooglePhotoPreference, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        if (Build.VERSION.SDK_INT >= 16 && isGooglePhotosInstalled(context)) {
            for (i = 0; i < resolveInfoList.size(); i++) {
                if (mSharedpreferences.getString(GooglePhotoPickerActivity, null) != null) break;
                if (resolveInfoList.get(i) != null) {
                    String packageName = resolveInfoList.get(i).activityInfo.packageName;
                    if (GOOGLE_PHOTOS_PACKAGE_NAME.equals(packageName)) {
                        String activityName = resolveInfoList.get(i).activityInfo.name;
                        editor.putString(GooglePhotoPickerActivity, activityName);
                        editor.commit();
                        break;
                    }
                }
            }
        } else {
            editor.putString(GooglePhotoPickerActivity, null);
            editor.commit();
        }
    }

    private static boolean isGooglePhotosInstalled(Context context) {
        PackageManager packageManager = context.getPackageManager();

        try {
            boolean isPackageExist =
              packageManager.getPackageInfo(GOOGLE_PHOTOS_PACKAGE_NAME, PackageManager.GET_ACTIVITIES) != null;
            ApplicationInfo appInfo = packageManager.getApplicationInfo(GOOGLE_PHOTOS_PACKAGE_NAME, 0);
            return isPackageExist && appInfo.enabled;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String getGooglePhotoPickerActivity(Context context) {
        if (!isGooglePhotosInstalled(context)) return null;
        if (mSharedpreferences == null) {
            setPhotoPickerActivity(context);
        }
        return mSharedpreferences.getString(GooglePhotoPickerActivity, null);
    }

    public static Bundle getDeeplinkBundle(Intent intent) {
        LogUtils.LOGD(TAG, "getDeeplinkBundle : ");
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                bundle = new Bundle();
            }

            bundle.putParcelable(DeeplinkConstants.KEY_DEEPLINK_URL, intent.getData());
            bundle.putString(DeeplinkConstants.KEY_ACTION, intent.getAction());

            return bundle;
        }

        return null;
    }

    public static Intent getDeepLinkIntent(Context mContext, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setPackage(mContext.getPackageName());
        intent.setData(Uri.parse(url));
        return intent;
    }

    public static Uri getDeepLinkUrl(Bundle bundle) {
        Uri uri = null;
        if (bundle != null && bundle.containsKey(DeeplinkConstants.KEY_ACTION) && bundle.containsKey(
          DeeplinkConstants.KEY_DEEPLINK_URL) && TextUtils.equals(bundle.getString(DeeplinkConstants.KEY_ACTION),
          Intent.ACTION_VIEW)) {
            uri = bundle.getParcelable(DeeplinkConstants.KEY_DEEPLINK_URL);
        }

        return uri;
    }

    public static Intent getCallIntent(String mobileNo) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + mobileNo));

        return intent;
    }

    public static String getGcmId() {
        LogUtils.LOGD(TAG, "getGcmId : ");
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static boolean isNetworkAvailable(Context context) {
        LogUtils.LOGD(TAG, "isNetworkAvailable : ");
        ConnectivityManager connectivityManager =
          (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        return false;
    }
}
