package com.athomediva.helper;

import android.app.Activity;
import android.os.Build;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.athomediva.data.models.remote.TimeSlot;
import java.util.List;

/**
 * Created by kartheeswaran on 19/04/17.
 */

public class UIHelper {

    public static void setStatusColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(activity.getResources().getColor(color));
        }
    }

    public static void setTranslucentStatus(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * Hide View if the txt value is empty
     * otherwise display the text
     */
    public static void hideViewIfEmptyTxt(TextView txt, String value) {
        if (TextUtils.isEmpty(value)) {
            txt.setVisibility(View.GONE);
        } else {
            txt.setVisibility(View.VISIBLE);
            txt.setText(value);
        }
    }

    public static void createTimePopupMenu(PopupMenu popup, List<TimeSlot> list) {
        if (popup != null) {
            popup.getMenu().clear();
            for (int index = 0; index < list.size(); index++) {
                popup.getMenu().add(Menu.NONE, index, Menu.NONE, list.get(index).getDisplayTime());
            }
        }
    }

    public static void createAddressTypePopupMenu(PopupMenu popup, List<String> list) {
        if (popup != null) {
            popup.getMenu().clear();
            for (int index = 0; index < list.size(); index++) {
                popup.getMenu().add(Menu.NONE, index, Menu.NONE, list.get(index));
            }
        }
    }

    public static void scrollToViewPosition(final ScrollView scroll, final View view) {

        if (scroll == null || view == null) return;

        scroll.postDelayed(new Runnable() {
            @Override
            public void run() {
                int height = view.getHeight();
                scroll.smoothScrollTo(0, (view.getBottom()) + height);
            }
        }, 400);
    }

    /**
     * Reset View Height to WrapContent if already set some fixed height
     * or measure the height duraing animation.
     */
    public static void resetViewHeightToWrapContent(View view) {
        if (view != null) {
            if (view.getLayoutParams() != null) {
                if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                    view.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                    view.requestLayout();
                } else if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
                    view.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    view.requestLayout();
                } else if (view.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                    view.getLayoutParams().height = FrameLayout.LayoutParams.WRAP_CONTENT;
                    view.requestLayout();
                }
            }
        }
    }
}
