package com.athomediva.injection.component;

import android.content.Context;
import com.athomediva.data.managers.APIManager;
import com.athomediva.data.managers.AnalyticsManager;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.EventBusManager;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.injection.ApplicationContext;
import com.athomediva.injection.module.ApplicationModule;
import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    APIManager apiManager();

    PreferenceManager preferencesManager();

    BookingSession getSession();

    EventBusManager eventBus();

    AnalyticsManager analyticsManager();

    DataManager dataManager();

    UserSession userSession();
}
