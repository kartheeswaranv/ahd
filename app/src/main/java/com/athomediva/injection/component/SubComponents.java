package com.athomediva.injection.component;

import com.athomediva.injection.PerActivity;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.address.AddAddressFragment;
import com.athomediva.ui.address.MyAddressFragment;
import com.athomediva.ui.bookingpage.BookingCancelFragment;
import com.athomediva.ui.bookingpage.BookingDetailsActivity;
import com.athomediva.ui.bookingpage.BookingFailureFragment;
import com.athomediva.ui.bookingpage.BookingSuccessFragment;
import com.athomediva.ui.bookingpage.PaymentPendingFragment;
import com.athomediva.ui.bookingpage.ReviewOrderActivity;
import com.athomediva.ui.feedback.FeedbackActivity;
import com.athomediva.ui.feedback.FeedbackFragment;
import com.athomediva.ui.feedback.FeedbackSuccessFragment;
import com.athomediva.ui.home.HomePageFragment;
import com.athomediva.ui.home.SpecialOfferDialogFragment;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.athomediva.ui.login.LoginFragment;
import com.athomediva.ui.login.SignUpActivity;
import com.athomediva.ui.map.LocationPickerActivity;
import com.athomediva.ui.map.MapLocationPickerActivity;
import com.athomediva.ui.myaccount.EditProfileActivity;
import com.athomediva.ui.myaccount.MyAccountFragment;
import com.athomediva.ui.mybookings.MyBookingFragment;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.mycart.ScheduleDialogFragment;
import com.athomediva.ui.mywallet.MyWalletActivity;
import com.athomediva.ui.offers.InstantCouponActivity;
import com.athomediva.ui.offers.OffersAndCouponsActivity;
import com.athomediva.ui.packages.PackagesFragment;
import com.athomediva.ui.ratings.RateUsActivity;
import com.athomediva.ui.services.ServicesFragment;
import com.athomediva.ui.social.ContactSyncActivity;
import com.athomediva.ui.social.InviteFriendActivity;
import com.athomediva.ui.subcategory.SubCategoryActivity;
import com.athomediva.ui.support.CustomerCareFragment;
import com.athomediva.ui.verification.OTPVerificationActivity;
import dagger.Subcomponent;

/**
 * Created by mohitkumar on 23/01/17.
 */

public interface SubComponents {

    @PerActivity
    @Subcomponent(modules = { ModuleManager.AppLauncherModule.class })
    interface AppLauncherComponent {
        void inject(AppLauncherActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.HomePageModule.class })
    interface HomePageComponent {
        void inject(HomePageFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.AHDMainActivityModule.class })
    interface AHDMainActivityComponent {
        void inject(AHDMainActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.SubCatActModule.class })
    interface SubCatActivityComponent {
        void inject(SubCategoryActivity subCategoryActivity);
    }

    @Subcomponent(modules = { ModuleManager.LoginModule.class })
    interface LoginFragmentComponent {
        void inject(LoginFragment activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.OTPVerificationModule.class })
    interface OTPVerificationComponent {
        void inject(OTPVerificationActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.SignUpModule.class })
    interface SignUpComponent {
        void inject(SignUpActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.MyBookingsModule.class })
    interface MyBookingsComponent {
        void inject(MyBookingFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.MyAccountsModule.class })
    interface MyAccountsComponent {
        void inject(MyAccountFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.OffersAndCouponsModule.class })
    interface OffersAndCouponsComponent {
        void inject(OffersAndCouponsActivity fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.CustomerServicesModule.class })
    interface CustomerServicesComponent {
        void inject(CustomerCareFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.ServicesPageModule.class })
    interface ServicesComponent {
        void inject(ServicesFragment fragment);
    }

    @Subcomponent(modules = { ModuleManager.MyAddressModule.class })
    interface MyAddressComponent {
        void inject(MyAddressFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.AddAddressModule.class })
    interface AddAddressComponent {
        void inject(AddAddressFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.PackageModule.class })
    interface PackageComponent {
        void inject(PackagesFragment fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.MyWalletModule.class })
    interface MyWalletComponent {
        void inject(MyWalletActivity fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.InviteFriendsModule.class })
    interface InviteFriendsComponent {
        void inject(InviteFriendActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.MyCartModule.class })
    interface MyCartComponent {
        void inject(MyCartActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.ScheduleModule.class })
    interface ScheduleComponent {
        void inject(ScheduleDialogFragment dialogFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.LocationPickerModule.class })
    interface LocationPickerComponent {
        void inject(LocationPickerActivity fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.ReviewOrderModule.class })
    interface ReviewOrderComponent {
        void inject(ReviewOrderActivity fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.MapLocationPickerModule.class })
    interface MapLocationPickerComponent {
        void inject(MapLocationPickerActivity fragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.BookingDetailsModule.class })
    interface BookingDetailsComponent {
        void inject(BookingDetailsActivity bookingDetailsActivity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.BookingSuccessModule.class })
    interface BookingSuccessComponent {
        void inject(BookingSuccessFragment bookingSuccessFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.FeedbackModule.class })
    interface FeedbackModuleComponent {
        void inject(FeedbackFragment feedbackFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.FeedbackSuccessModule.class })
    interface FeedbackSuccessModule {
        void inject(FeedbackSuccessFragment feedbackSuccessFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.CancelBookingModule.class })
    interface CancelBookingModule {
        void inject(BookingCancelFragment cancelFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.EditProfileModule.class })
    interface EditProfileModule {
        void inject(EditProfileActivity editProfileActivity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.BookingFailureModule.class })
    interface BookingFailureModule {
        void inject(BookingFailureFragment bookingFailureFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.PaymentPendingModule.class })
    interface PaymentPendingModule {
        void inject(PaymentPendingFragment paymentPendingFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.RateUsModule.class })
    interface RateUsModule {
        void inject(RateUsActivity rateUsActivity);
    }

    @Subcomponent(modules = { ModuleManager.FeedbackActModule.class })
    interface FeedbackActModule {
        void inject(FeedbackActivity feedbackActivity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.SpecialOfferModule.class })
    interface SpecialOfferModule {
        void inject(SpecialOfferDialogFragment dialogFragment);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.ContactSyncModule.class })
    interface ContactSyncModule {
        void inject(ContactSyncActivity activity);
    }

    @PerActivity
    @Subcomponent(modules = { ModuleManager.InstantCouponModule.class })
    interface InstantCouponModule {
        void inject(InstantCouponActivity instantCouponActivity);
    }
}

