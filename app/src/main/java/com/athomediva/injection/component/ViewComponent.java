package com.athomediva.injection.component;

import com.athomediva.injection.PerActivity;
import com.athomediva.injection.module.ActivityModule;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.ui.BaseActivity;
import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ViewComponent {
    void inject(BaseActivity activity);

    SubComponents.AppLauncherComponent plus(ModuleManager.AppLauncherModule appLauncherModule);

    SubComponents.HomePageComponent plus(ModuleManager.HomePageModule homePageModule);

    SubComponents.AHDMainActivityComponent plus(ModuleManager.AHDMainActivityModule ahdMainActivityModule);

    SubComponents.SubCatActivityComponent plus(ModuleManager.SubCatActModule subCatActModule);

    SubComponents.LoginFragmentComponent plus(ModuleManager.LoginModule loginActivityModule);

    SubComponents.OTPVerificationComponent plus(ModuleManager.OTPVerificationModule otpVerificationModule);

    SubComponents.SignUpComponent plus(ModuleManager.SignUpModule signUpModule);

    SubComponents.MyBookingsComponent plus(ModuleManager.MyBookingsModule myBookingsModule);

    SubComponents.MyAccountsComponent plus(ModuleManager.MyAccountsModule myAccountsModule);

    SubComponents.OffersAndCouponsComponent plus(ModuleManager.OffersAndCouponsModule offersAndCouponsModule);

    SubComponents.CustomerServicesComponent plus(ModuleManager.CustomerServicesModule customerServicesModule);

    SubComponents.ServicesComponent plus(ModuleManager.ServicesPageModule servicesModule);

    SubComponents.MyAddressComponent plus(ModuleManager.MyAddressModule myAddressModule);

    SubComponents.AddAddressComponent plus(ModuleManager.AddAddressModule addAddressModule);

    SubComponents.PackageComponent plus(ModuleManager.PackageModule packageModule);

    SubComponents.MyWalletComponent plus(ModuleManager.MyWalletModule myWalletModule);

    SubComponents.InviteFriendsComponent plus(ModuleManager.InviteFriendsModule inviteFriendsModule);

    SubComponents.MyCartComponent plus(ModuleManager.MyCartModule myCartModule);

    SubComponents.ScheduleComponent plus(ModuleManager.ScheduleModule scheduleModule);

    SubComponents.ReviewOrderComponent plus(ModuleManager.ReviewOrderModule reviewOrderModule);

    SubComponents.LocationPickerComponent plus(ModuleManager.LocationPickerModule locationPickerModule);

    SubComponents.MapLocationPickerComponent plus(ModuleManager.MapLocationPickerModule mapLocationPickerModule);

    SubComponents.BookingDetailsComponent plus(ModuleManager.BookingDetailsModule bookingDetailsModule);

    SubComponents.BookingSuccessComponent plus(ModuleManager.BookingSuccessModule bookingSuccessModule);

    SubComponents.FeedbackModuleComponent plus(ModuleManager.FeedbackModule feedbackModule);

    SubComponents.FeedbackSuccessModule plus(ModuleManager.FeedbackSuccessModule feedbackSuccessModule);

    SubComponents.CancelBookingModule plus(ModuleManager.CancelBookingModule cancelBookingModule);

    SubComponents.EditProfileModule plus(ModuleManager.EditProfileModule editProfileModule);

    SubComponents.BookingFailureModule plus(ModuleManager.BookingFailureModule bookingFailure);

    SubComponents.PaymentPendingModule plus(ModuleManager.PaymentPendingModule paymentPendingModule);

    SubComponents.RateUsModule plus(ModuleManager.RateUsModule rateUsModule);

    SubComponents.FeedbackActModule plus(ModuleManager.FeedbackActModule feedbackActModule);

    SubComponents.SpecialOfferModule plus(ModuleManager.SpecialOfferModule specialOfferModule);

    SubComponents.ContactSyncModule plus(ModuleManager.ContactSyncModule contactSyncModule);

    SubComponents.InstantCouponModule plus(ModuleManager.InstantCouponModule instantCouponModule);
}

