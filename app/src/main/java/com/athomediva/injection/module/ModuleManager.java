package com.athomediva.injection.module;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.permission.ActivityPermissionRequester;
import com.athomediva.helper.permission.FragmentPermissionRequester;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.injection.ApplicationContext;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.AddAddressPageContract;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.BookingFailureContract;
import com.athomediva.mvpcontract.BookingSuccessContract;
import com.athomediva.mvpcontract.CancelBookingContract;
import com.athomediva.mvpcontract.ContactSyncContract;
import com.athomediva.mvpcontract.CustomerServicesContract;
import com.athomediva.mvpcontract.EditProfileContract;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.FeedbackPageContract;
import com.athomediva.mvpcontract.FeedbackSuccessContract;
import com.athomediva.mvpcontract.HomePageContract;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.mvpcontract.InviteFriendsContract;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.athomediva.mvpcontract.LoginPageContract;
import com.athomediva.mvpcontract.MapLocationPickerContract;
import com.athomediva.mvpcontract.MyAccountPageContract;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.mvpcontract.MyBookingsPageContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.MyWalletContract;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.athomediva.mvpcontract.OffersViewContract;
import com.athomediva.mvpcontract.PackagesContract;
import com.athomediva.mvpcontract.PaymentPendingContract;
import com.athomediva.mvpcontract.RateUsContract;
import com.athomediva.mvpcontract.ReviewOrderContract;
import com.athomediva.mvpcontract.ScheduleContract;
import com.athomediva.mvpcontract.ServicesContract;
import com.athomediva.mvpcontract.SignUpPageContract;
import com.athomediva.mvpcontract.SpecialOfferContract;
import com.athomediva.mvpcontract.SubcatSectionContract;
import com.athomediva.presenter.AHDMainPresenter;
import com.athomediva.presenter.AddAddressPresenter;
import com.athomediva.presenter.AppLauncherPresenter;
import com.athomediva.presenter.BookingDetailsPresenter;
import com.athomediva.presenter.BookingFailurePresenter;
import com.athomediva.presenter.BookingSuccessPresenter;
import com.athomediva.presenter.CancelBookingPresenter;
import com.athomediva.presenter.ContactSyncPresenter;
import com.athomediva.presenter.CustomerServicesPresenter;
import com.athomediva.presenter.EditProfilePresenter;
import com.athomediva.presenter.FeedbackActPresenter;
import com.athomediva.presenter.FeedbackPagePresenter;
import com.athomediva.presenter.FeedbackSuccessPresenter;
import com.athomediva.presenter.HomePagePresenter;
import com.athomediva.presenter.InstantOfferPresenter;
import com.athomediva.presenter.InviteFriendsPresenter;
import com.athomediva.presenter.LocationPickerPresenter;
import com.athomediva.presenter.LoginPresenter;
import com.athomediva.presenter.MapLocationPickerPresenter;
import com.athomediva.presenter.MyAccountsPresenter;
import com.athomediva.presenter.MyAddressPresenter;
import com.athomediva.presenter.MyBookingPresenter;
import com.athomediva.presenter.MyCartPresenter;
import com.athomediva.presenter.MyWalletPresenter;
import com.athomediva.presenter.OTPVerificationPresenter;
import com.athomediva.presenter.OffersAndCouponPresenter;
import com.athomediva.presenter.PackagePresenter;
import com.athomediva.presenter.PaymentPendingPresenter;
import com.athomediva.presenter.RateUsPresenter;
import com.athomediva.presenter.ReviewOrderPresenter;
import com.athomediva.presenter.SchedulePresenter;
import com.athomediva.presenter.ServicesPagePresenter;
import com.athomediva.presenter.SignUpPresenter;
import com.athomediva.presenter.SpecialOfferPresenter;
import com.athomediva.presenter.SubcatPresenter;
import dagger.Module;
import dagger.Provides;
import java.lang.ref.WeakReference;

/**
 * Created by mohit kumar on 23/01/17.
 */

public class ModuleManager {

    @Module
    public static class AppLauncherModule {
        private final Bundle bundle;

        public AppLauncherModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        AppLauncherContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          Activity activity, @NonNull BookingSession bookingSession) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new AppLauncherPresenter(dataManager, context, permissionManager, bundle, bookingSession);
        }
    }

    @Module
    public static class AHDMainActivityModule {

        private final Bundle bundle;
        private final Bundle saveInstance;

        /*public AHDMainActivityModule(Bundle bundle) {
            this.bundle = bundle;
        }*/

        public AHDMainActivityModule(Intent intent) {
            this.bundle = AndroidHelper.getDeeplinkBundle(intent);
            this.saveInstance = null;
        }

        public AHDMainActivityModule(Intent intent, Bundle saveInstance) {
            this.bundle = AndroidHelper.getDeeplinkBundle(intent);
            this.saveInstance = saveInstance;
        }

        @Provides
        AHDMainViewContract.Presenter providesPresenter(@NonNull Activity activity, DataManager dataManager,
          @ApplicationContext Context context, @NonNull UserSession userSession, @NonNull BookingSession bookingSession) {
            WeakReference<Activity> weakReference = new WeakReference<Activity>(activity);
            return new AHDMainPresenter(weakReference, dataManager, context, bookingSession, userSession, bundle,
              saveInstance);
        }
    }

    @Module
    public static class SubCatActModule {
        private final Bundle bundle;

        /*public SubCatActModule(Bundle b) {
            bundle = b;
        }*/
        public SubCatActModule(Intent intent) {
            bundle = AndroidHelper.getDeeplinkBundle(intent);
        }

        @Provides
        SubcatSectionContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession bookingSession,
          @ApplicationContext Context context) {
            return new SubcatPresenter(dataManager, bookingSession, context, bundle);
        }
    }

    @Module
    public static class LoginModule {
        private final Bundle bundle;
        private final WeakReference fragWeakReference;

        public LoginModule(Bundle bundle, Fragment fragment) {
            this.bundle = bundle;
            fragWeakReference = new WeakReference(fragment);
        }

        @Provides
        LoginPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull Activity activity, @NonNull UserSession userSession) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new FragmentPermissionRequester(fragWeakReference));

            return new LoginPresenter(dataManager, context, permissionManager, bundle, userSession);
        }
    }

    @Module
    public static class OTPVerificationModule {
        private final Bundle bundle;

        public OTPVerificationModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        OTPVerificationContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new OTPVerificationPresenter(dataManager, context, permissionManager, bundle);
        }
    }

    @Module
    public static class SignUpModule {
        private final Bundle bundle;

        public SignUpModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        SignUpPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new SignUpPresenter(dataManager, context, bundle);
        }
    }

    @Module
    public static class HomePageModule {
        private final Bundle bundle;
        private final WeakReference<Fragment> fragmentWeakReference;

        public HomePageModule(Bundle bundle, Fragment frag) {
            this.bundle = bundle;
            fragmentWeakReference = new WeakReference<>(frag);
        }

        @Provides
        HomePageContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession bookingSession,
          @ApplicationContext Context context, Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new FragmentPermissionRequester(fragmentWeakReference));
            CartUpdationTask task = new CartUpdationTask(context, dataManager, bookingSession);
            return new HomePagePresenter(dataManager, bookingSession, context, permissionManager, task);
        }
    }

    @Module
    public static class MyAccountsModule {
        private final Bundle bundle;

        public MyAccountsModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        MyAccountPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull UserSession userSession, @NonNull BookingSession bookingSession) {
            return new MyAccountsPresenter(dataManager, context, userSession, bookingSession);
        }
    }

    @Module
    public static class MyBookingsModule {
        private final Bundle bundle;

        public MyBookingsModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        MyBookingsPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new MyBookingPresenter(dataManager, context);
        }
    }

    @Module
    public static class OffersAndCouponsModule {
        private final Bundle bundle;

        public OffersAndCouponsModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        OffersViewContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new OffersAndCouponPresenter(dataManager, context);
        }
    }

    @Module
    public static class CustomerServicesModule {
        private final Bundle bundle;

        public CustomerServicesModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        CustomerServicesContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new CustomerServicesPresenter(dataManager, context);
        }
    }

    @Module
    public static class ServicesPageModule {
        private final Bundle bundle;

        public ServicesPageModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        ServicesContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            CartUpdationTask task = new CartUpdationTask(context, dataManager, session);
            return new ServicesPagePresenter(dataManager, session, context, task, bundle);
        }
    }

    @Module
    public static class PackageModule {
        private final Bundle bundle;

        public PackageModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        PackagesContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            CartUpdationTask task = new CartUpdationTask(context, dataManager, session);
            return new PackagePresenter(dataManager, session, context, task, bundle);
        }
    }

    @Module
    public static class MyAddressModule {
        private final Bundle bundle;

        public MyAddressModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        MyAddressPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull UserSession userSession) {
            return new MyAddressPresenter(dataManager, context, bundle, userSession);
        }
    }

    @Module
    public static class AddAddressModule {
        private final Bundle bundle;

        public AddAddressModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        AddAddressPageContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new AddAddressPresenter(dataManager, context, bundle);
        }
    }

    @Module
    public static class LocationPickerModule {
        private final Bundle bundle;

        public LocationPickerModule(@NonNull Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        LocationPickerContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull UserSession session, @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));

            return new LocationPickerPresenter(dataManager, context, session, permissionManager, bundle);
        }
    }

    @Module
    public static class MapLocationPickerModule {
        private final Bundle bundle;

        public MapLocationPickerModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        MapLocationPickerContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));

            return new MapLocationPickerPresenter(dataManager, context, permissionManager, bundle);
        }
    }

    @Module
    public static class MyWalletModule {
        private final Bundle bundle;

        public MyWalletModule(Bundle arguments) {
            this.bundle = arguments;
        }

        @Provides
        MyWalletContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context) {
            return new MyWalletPresenter(dataManager, context, bundle);
        }
    }

    @Module
    public static class InviteFriendsModule {
        private final Bundle bundle;

        public InviteFriendsModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        InviteFriendsContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull UserSession userSession, @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new InviteFriendsPresenter(dataManager, context, userSession, permissionManager);
        }
    }

    @Module
    public static class MyCartModule {
        private final Bundle bundle;

        public MyCartModule(Intent intent) {
            bundle = AndroidHelper.getDeeplinkBundle(intent);
        }

        @Provides
        MyCartContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession bookingSession,
          @ApplicationContext Context context, @NonNull UserSession userSession) {
            CartUpdationTask task = new CartUpdationTask(context, dataManager, bookingSession);
            return new MyCartPresenter(dataManager, bookingSession, context, userSession, task, bundle);
        }
    }

    @Module
    public static class ScheduleModule {
        private final Bundle bundle;

        public ScheduleModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        ScheduleContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new SchedulePresenter(dataManager, session, context, bundle);
        }
    }

    @Module
    public static class ReviewOrderModule {
        private Bundle bundle;

        public ReviewOrderModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        ReviewOrderContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            CartUpdationTask task = new CartUpdationTask(context, dataManager, session);
            return new ReviewOrderPresenter(dataManager, session, context, task, bundle);
        }
    }

    @Module
    public static class BookingDetailsModule {
        private Bundle bundle;

        /*public BookingDetailsModule(Bundle bundle) {
            this.bundle = bundle;
        }*/

        public BookingDetailsModule(Intent intent) {
            bundle = AndroidHelper.getDeeplinkBundle(intent);
        }

        @Provides
        BookingDetailsContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context, @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new BookingDetailsPresenter(dataManager, session, context, permissionManager, bundle);
        }
    }

    @Module
    public static class BookingSuccessModule {
        private Bundle bundle;

        public BookingSuccessModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        BookingSuccessContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new BookingSuccessPresenter(dataManager, context, session, bundle);
        }
    }

    @Module
    public static class BookingFailureModule {
        private Bundle bundle;

        public BookingFailureModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        BookingFailureContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new BookingFailurePresenter(dataManager, context, session, bundle);
        }
    }

    @Module
    public static class FeedbackModule {
        private Bundle bundle;

        public FeedbackModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        FeedbackPageContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new FeedbackPagePresenter(dataManager, session, context, bundle);
        }
    }

    @Module
    public static class FeedbackSuccessModule {
        private Bundle bundle;

        public FeedbackSuccessModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        FeedbackSuccessContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new FeedbackSuccessPresenter(dataManager, session, context, bundle);
        }
    }

    @Module
    public static class CancelBookingModule {
        private Bundle bundle;

        public CancelBookingModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        CancelBookingContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new CancelBookingPresenter(dataManager, session, context, bundle);
        }
    }

    @Module
    public static class EditProfileModule {
        private final Bundle bundle;

        public EditProfileModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        EditProfileContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull UserSession userSession, @NonNull Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new EditProfilePresenter(dataManager, context, userSession, permissionManager, bundle);
        }
    }

    @Module
    public static class PaymentPendingModule {
        private final Bundle bundle;

        public PaymentPendingModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        PaymentPendingContract.Presenter providesPresenter(DataManager dataManager, @ApplicationContext Context context,
          @NonNull BookingSession session, Activity activity) {
            WeakReference activityWeakRef = new WeakReference(activity);
            PermissionManager permissionManager =
              new PermissionManager(activityWeakRef, new ActivityPermissionRequester(activityWeakRef));
            return new PaymentPendingPresenter(dataManager, context, permissionManager, bundle, session);
        }
    }

    @Module
    public static class RateUsModule {
        private final Bundle bundle;

        public RateUsModule(Bundle b) {
            bundle = b;
        }

        @Provides
        RateUsContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession bookingSession,
          @ApplicationContext Context context) {
            return new RateUsPresenter(dataManager, context);
        }
    }

    @Module
    public static class FeedbackActModule {
        private Bundle bundle;

        public FeedbackActModule(Bundle b) {
            bundle = b;
        }

        @Provides
        FeedbackActContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new FeedbackActPresenter(dataManager, session, context, bundle);
        }
    }

    @Module
    public static class SpecialOfferModule {
        private final Bundle bundle;

        public SpecialOfferModule(Bundle bundle) {
            this.bundle = bundle;
        }

        @Provides
        SpecialOfferContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context, @NonNull UserSession userSession) {
            return new SpecialOfferPresenter(dataManager, context, userSession);
        }
    }

    @Module
    public static class ContactSyncModule {
        private Bundle bundle;

        public ContactSyncModule(Bundle b) {
            bundle = b;
        }

        @Provides
        ContactSyncContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession session,
          @ApplicationContext Context context) {
            return new ContactSyncPresenter(dataManager, context, session, bundle);
        }
    }

    @Module
    public static class InstantCouponModule {
        private final Bundle bundle;

        public InstantCouponModule(Bundle b) {
            bundle = b;
        }

        @Provides
        InstantCouponContract.Presenter providesPresenter(DataManager dataManager, @NonNull BookingSession bookingSession,
          @ApplicationContext Context context) {
            return new InstantOfferPresenter(dataManager, context, bundle);
        }
    }
}
