package com.athomediva.injection.module;

import android.app.Activity;
import android.content.Context;
import com.athomediva.injection.ActivityContext;
import dagger.Module;
import dagger.Provides;
import java.lang.ref.WeakReference;

@Module
public class ActivityModule {
    private final WeakReference<Activity> mActivity;

    public ActivityModule(Activity activity) {
        mActivity = new WeakReference<>(activity);
    }

    @Provides
    Activity provideActivity() {
        return mActivity.get();
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity.get();
    }
}