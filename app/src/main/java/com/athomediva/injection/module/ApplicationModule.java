package com.athomediva.injection.module;

import android.app.Application;
import android.content.Context;
import com.athomediva.data.CachedStorage;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.DataProcessor;
import com.athomediva.data.managers.APIManager;
import com.athomediva.data.managers.AnalyticsManager;
import com.athomediva.data.managers.EventBusManager;
import com.athomediva.data.managers.PreferenceManagerImpl;
import com.athomediva.data.managers.RemoteConfigManager;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.injection.ApplicationContext;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Provide application-level dependencies. Mainly singleton object that can be injected from
 * anywhere in the app.
 */
@Module
public class ApplicationModule {
    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    EventBusManager provideEventBus(@ApplicationContext Context context) {
        return new EventBusManager(context);
    }

    @Provides
    @Singleton
    APIManager provideAPIManager(@ApplicationContext Context context) {
        return new APIManager(context);
    }

    @Provides
    @Singleton
    BookingSession provideBookingSession(@ApplicationContext Context context, CachedStorage cachedStorage,
      DataProcessor dataProcessor, PreferenceManager preferenceManager, EventBusManager eventBusManager) {
        return new BookingSession(context, preferenceManager,eventBusManager);
    }

    @Provides
    @Singleton
    AnalyticsManager provideAnalyticsManager(@ApplicationContext Context context) {
        return new AnalyticsManager(context);
    }

    @Provides
    @Singleton
    PreferenceManager providePreferencesManager(@ApplicationContext Context context) {
        return new PreferenceManagerImpl(context);
    }

    @Provides
    @Singleton
    CachedStorage providesCachedStorage(@ApplicationContext Context context) {
        return new CachedStorage();
    }

    @Provides
    @Singleton
    CoreLogic providesBookingValidator(@ApplicationContext Context context) {
        return new CoreLogic();
    }

    @Provides
    @Singleton
    DataProcessor providesDataProcessor(@ApplicationContext Context context, CachedStorage cachedStorage) {
        return new DataProcessor(context);
    }

    @Provides
    @Singleton
    RemoteConfigManager providesRemoteConfigManager(@ApplicationContext Context context) {
        return new RemoteConfigManager(context);
    }

    @Provides
    @Singleton
    UserSession providesUserSession(@ApplicationContext Context context, PreferenceManager preferenceManager,
      EventBusManager busManager) {
        return new UserSession(context, preferenceManager, busManager);
    }
}
