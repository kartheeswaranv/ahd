package com.athomediva.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 06/06/17.
 */

public class FcmMessagingService extends FirebaseMessagingService {
    private final static String TAG = LogUtils.makeLogTag(FcmMessagingService.class.getSimpleName());

    interface PUSH {
        String PBO = "PBCO";        //CUSTOMER BOOKED CONFIRMATION..
        String CAS = "CAS";        //CUSTOMER APPOINTMENT STARTED..
        String SPN = "SPN";        //OFFERS..
        String TYPE = "type";
    }

    interface EXTRA {
        String MSG = "msg";
        String OFFER_IMAGE = "offer_image";
        String TITLE = "title";
        String SUBTITLE = "subtitle";
        String SUBJECT = "subject";
        String DEEPLINK = "deeplink";
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        LOGD(TAG, "onMessageReceived: " + Thread.currentThread().getName());

        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        //
        LOGD(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            LOGD(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            LOGD(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData() != null) {
            String type = remoteMessage.getData().get(PUSH.TYPE);
            LOGD(TAG, "onMessageReceived: type =" + type);
            /*if (type == null) {
                LOGE(TAG, "onMessageReceived: type is null");
                return;
            }
            if (PUSH.SPN.equalsIgnoreCase(type)) {
                LOGD(TAG, "onMessageReceived: type is spn");
                String imageUri = remoteMessage.getData().get(EXTRA.OFFER_IMAGE);
                offersNotification(remoteMessage.getData(), getBitmapFromUrl(imageUri));
            } else if (PUSH.CAS.equalsIgnoreCase(type) || PUSH.PBO.equalsIgnoreCase(type)) {
                bookingNotifications(remoteMessage.getData());
            } else {
                LOGE(TAG, "onMessageReceived: type not matched");
            }*/
            handleNotification(remoteMessage.getData());
        }
    }

    @WorkerThread
    private Bitmap getBitmapFromUrl(String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) return null;
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            LOGE(TAG, "getBitmapFromUrl: " + e.getLocalizedMessage());
            return null;
        }
    }

    private void offersNotification(Map<String, String> data, Bitmap bitmap) {
        LOGD(TAG, "offersNotification: ");
        String title = data.get(EXTRA.TITLE);
        String message = data.get(EXTRA.MSG);
        String deeplink = data.get(EXTRA.DEEPLINK);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext()).setAutoCancel(true)
          .setContentTitle(title)
          .setContentText(message)
          .setSmallIcon(R.drawable.ic_launcher);
        builder.setPriority(Notification.PRIORITY_MAX);

        if (bitmap != null) {
            NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
            bigPicStyle.bigPicture(bitmap);
            bigPicStyle.setBigContentTitle(title);
            bigPicStyle.setSummaryText(message);
            builder.setStyle(bigPicStyle);
        }

        Intent intent;

        if (TextUtils.isEmpty(deeplink)) {
            intent = new Intent(this, AppLauncherActivity.class);
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(deeplink));
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(10006, builder.build());
    }

    private int randInt() {
        int min = 0;
        int max = 100;
        Random rand = new Random();
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        return rand.nextInt((max - min) + 1) + min;
    }

    private void bookingNotifications(@NonNull Map<String, String> extras) {
        LOGD(TAG, "bookingNotifications: ");
        String title = extras.get(EXTRA.TITLE);
        String subtitle = extras.get(EXTRA.SUBTITLE);
        String subject = extras.get(EXTRA.SUBJECT);
        String deeplink = extras.get(EXTRA.DEEPLINK);
        Intent intent;

        if (TextUtils.isEmpty(deeplink)) {
            intent = new Intent(this, AppLauncherActivity.class);
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(deeplink));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        int requestID = randInt();
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext()).setAutoCancel(true)
          .setContentTitle(title)
          .setSmallIcon(R.drawable.ic_launcher)
          .setContentText(subject);
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(subtitle);
        mBuilder.setStyle(bigText);
        mBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        playDefaultNotificationSound();
        notificationManager.notify(requestID, mBuilder.build());
    }

    private void playDefaultNotificationSound() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }

    private void handleNotification(@NonNull Map<String, String> data) {
        LOGD(TAG, "offersNotification: ");
        if (data == null) return;
        String title = data.get(EXTRA.TITLE);
        // Message is not used right now
        String message = data.get(EXTRA.MSG);
        // Instead of Msg subtitle is using by api
        String subTitle = data.get(EXTRA.SUBTITLE);
        String deeplink = data.get(EXTRA.DEEPLINK);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext()).setAutoCancel(true)
          .setContentTitle(title)
          .setContentText(subTitle)
          .setSmallIcon(R.drawable.ic_launcher);
        builder.setPriority(Notification.PRIORITY_MAX);

        String imageUrl = data.get(EXTRA.OFFER_IMAGE);
        Bitmap bitmap = getBitmapFromUrl(imageUrl);
        if (bitmap != null) {
            NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
            bigPicStyle.bigPicture(bitmap);
            bigPicStyle.setBigContentTitle(title);
            bigPicStyle.setSummaryText(subTitle);
            builder.setStyle(bigPicStyle);
        } else if (!TextUtils.isEmpty(subTitle)) {
            NotificationCompat.BigTextStyle bigPicStyle = new NotificationCompat.BigTextStyle();
            bigPicStyle.bigText(subTitle);
            builder.setStyle(bigPicStyle);
        }

        Intent intent;

        // If it is start with http then it should launch via internal deeplink
        // otherwise just set the data to the intent and launch
        if (TextUtils.isEmpty(deeplink)) {
            intent = new Intent(this, AppLauncherActivity.class);
        } else if (deeplink.startsWith("http")) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setPackage(this.getPackageName());
            intent.setData(Uri.parse(deeplink));
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(deeplink));
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(10006, builder.build());
    }
}
