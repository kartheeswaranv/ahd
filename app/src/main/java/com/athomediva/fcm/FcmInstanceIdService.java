package com.athomediva.fcm;

import com.athomediva.logger.LogUtils;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 06/06/17.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = LogUtils.makeLogTag(FcmInstanceIdService.class.getSimpleName());

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        LOGD(TAG, "onTokenRefresh: ");
    }
}
