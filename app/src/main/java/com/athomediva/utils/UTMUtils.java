package com.athomediva.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.models.local.Utm;
import com.athomediva.logger.LogUtils;
import java.util.HashMap;

/**
 * Created by kartheeswaran on 29/06/17.
 */

public class UTMUtils {
    private static final String TAG = UTMUtils.class.getSimpleName();

    public static Utm extractUtmParams(Intent i, Activity activity) {
        LogUtils.LOGD(TAG, "extractUtmParams : ");
        // Extract UTM parameters
        final Utm utm = getUTMValuesFromIntent(i);
        final boolean utmDetailsFound = hasValidData(utm);

        return utm;
    }

    public static Utm getExtractUtmParams(Uri uri) {
        LogUtils.LOGD(TAG, "extractUtmParams : Uri " + uri);
        Utm utm = getUTMValuesFromUri(uri);
        return utm;
    }

    public static boolean hasValidData(Utm utm) {
        return (utm != null) && (!TextUtils.isEmpty(utm.source) || !TextUtils.isEmpty(utm.campaign) || !TextUtils.isEmpty(
          utm.medium));
    }

    public static Utm getUTMValuesFromIntent(Intent i) {
        LogUtils.LOGD(TAG, "getUTMValuesFromIntent : ");
        Uri uri = i.getData();
        Utm utm = getUTMValuesFromUri(uri);

        if (!hasValidData(utm)) {
            utm = getUTMValuesFromExtras(i.getExtras());
        }
        return utm;
    }

    public static Utm getUTMValuesFromUri(Uri uri) {
        LogUtils.LOGD(TAG, "getUTMValuesFromUri : ");
        Utm utm = new Utm();
        //https://examplepetstore.com/promo?utm_source=newsletter&utm_medium=email&utm_campaign=promotion
        utm.source = uri == null ? null : uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.UTM_SOURCE);
        utm.medium = uri == null ? null : uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.UTM_MEDIUM);
        utm.campaign = uri == null ? null : uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.UTM_CAMPAIGN);
        utm.gclId = uri == null ? null : uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.GCL_ID);
        utm.keyword = uri == null ? null : uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.KEYWORD);
        utm.uri = uri == null ? null : uri.toString();

        return utm;
    }

    public static Utm getUTMValuesFromExtras(Bundle extras) {
        LogUtils.LOGD(TAG, "getUTMValuesFromExtras : ");
        Utm utm = new Utm();
        if (extras != null) {
            utm.source = extras.getString(DeeplinkConstants.DEEPLINK_PARAMS.UTM_SOURCE, null);
            utm.medium = extras.getString(DeeplinkConstants.DEEPLINK_PARAMS.UTM_MEDIUM, null);
            utm.campaign = extras.getString(DeeplinkConstants.DEEPLINK_PARAMS.UTM_CAMPAIGN, null);
            utm.gclId = extras.getString(DeeplinkConstants.DEEPLINK_PARAMS.GCL_ID);
            utm.keyword = extras.getString(DeeplinkConstants.DEEPLINK_PARAMS.KEYWORD);
        }
        return utm;
    }

    public static HashMap<String, Object> getUTMParams(Utm utm) {
        LogUtils.LOGD(TAG, "getUTMParams : ");
        if (utm != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_SOURCE, TextUtils.isEmpty(utm.source) ? "direct" : utm.source);
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_MEDIUM, utm.medium);
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_CAMPAIGN, utm.campaign);
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.GCL_ID, utm.gclId);
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.KEYWORD, utm.keyword);
            params.put(DeeplinkConstants.DEEPLINK_PARAMS.PLATFORM, "Android");
            return params;
        }
        return getUTMParams();
    }

    public static HashMap<String, Object> getUTMParams() {
        HashMap<String, Object> params = new HashMap<>();
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_SOURCE, "direct");
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_MEDIUM, null);
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.UTM_CAMPAIGN, null);
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.GCL_ID, null);
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.KEYWORD, null);
        params.put(DeeplinkConstants.DEEPLINK_PARAMS.PLATFORM, "Android");

        return params;
    }
}
