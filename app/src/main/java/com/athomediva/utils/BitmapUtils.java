package com.athomediva.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class BitmapUtils {
    private static final String TAG = BitmapUtils.class.getSimpleName();

    private BitmapUtils() {
    }

    public static boolean isContentUri(Uri uri) {
        return "content".equalsIgnoreCase(uri.getScheme());
    }

    public static Bitmap resizeBitmapByScale(Bitmap bitmap, float scale, boolean recycle) {
        int width = Math.round((float) bitmap.getWidth() * scale);
        int height = Math.round((float) bitmap.getHeight() * scale);
        if (width == bitmap.getWidth() && height == bitmap.getHeight()) {
            return bitmap;
        } else {
            Bitmap target = Bitmap.createBitmap(width, height, getConfig(bitmap));
            Canvas canvas = new Canvas(target);
            canvas.scale(scale, scale);
            Paint paint = new Paint(6);
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, paint);
            if (recycle) {
                bitmap.recycle();
            }

            return target;
        }
    }

    private static Bitmap.Config getConfig(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }

        return config;
    }

    public static Bitmap rotateBitmap(Bitmap source, float rotation, boolean recycle) {
        if (rotation == 0.0F) {
            return source;
        } else {
            int w = source.getWidth();
            int h = source.getHeight();
            Matrix m = new Matrix();
            m.postRotate(rotation);
            Bitmap bitmap = Bitmap.createBitmap(source, 0, 0, w, h, m, true);
            if (recycle) {
                source.recycle();
            }

            return bitmap;
        }
    }

    public static Bitmap decodeFile(String path, int minSideLength) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = computeSampleSizeLarger(options.outWidth, options.outHeight, minSideLength);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap decodeFile(FileDescriptor fd, int minSideLength) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = false;
        BitmapFactory.decodeFileDescriptor(fd, (Rect) null, options);
        options.inSampleSize = computeSampleSizeLarger(options.outWidth, options.outHeight, minSideLength);
        options.inDither = true;
        options.inJustDecodeBounds = false;
        Log.d("BitmapUtils", "Sample size: " + options.inSampleSize);
        return BitmapFactory.decodeFileDescriptor(fd, (Rect) null, options);
    }

    public static int computeSampleSizeLarger(int w, int h, int minSideLength) {
        int initialSize = Math.max(w / minSideLength, h / minSideLength);
        return initialSize <= 1 ? 1 : (initialSize <= 8 ? prevPowerOf2(initialSize) : initialSize / 8 * 8);
    }

    public static int prevPowerOf2(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        } else {
            return Integer.highestOneBit(n);
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap decodeUri(Context context, Uri uri, int minSideLength) throws IOException {
        if (uri == null) {
            return null;
        } else {
            Bitmap bitmap;
            if (isContentUri(uri)) {
                ParcelFileDescriptor file = context.getContentResolver().openFileDescriptor(uri, "r");
                if (file == null) {
                    return null;
                }

                bitmap = decodeUri(file.getFileDescriptor(), minSideLength);
                file.close();
            } else {
                File file1 = new File(uri.getPath());
                FileInputStream in = new FileInputStream(file1);
                bitmap = decodeUri(in.getFD(), minSideLength);
                in.close();
            }

            return bitmap;
        }
    }

    public static Bitmap decodeUri(FileDescriptor fd, int minSideLength) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = false;
        BitmapFactory.decodeFileDescriptor(fd, (Rect) null, options);
        options.inSampleSize = computeSampleSizeLarger(options.outWidth, options.outHeight, minSideLength);
        options.inDither = true;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFileDescriptor(fd, (Rect) null, options);
    }
}
