package com.athomediva.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.Gravity;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.local.StatusModel;
import com.athomediva.data.models.remote.CartServiceGroup;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class UiUtils {
    private static final String UBUNTU_REGULAR = "UbuntuRegular.ttf";
    private static final String UBUNTU_BOLD = "UbuntuBold.ttf";
    private static final String UBUNTU_MEDIUM = "UbuntuMedium.ttf";
    private static String TAG = LogUtils.makeLogTag(UiUtils.class.getSimpleName());
    private static Typeface UbuntuRegularFont;
    private static Typeface UbuntuBoldFont;
    private static Typeface UbuntuMediumFont;

    public static Typeface getUbuntuBoldFont(Context context) {
        if (UbuntuBoldFont == null) {
            UbuntuBoldFont = Typeface.createFromAsset(context.getAssets(), UBUNTU_BOLD);
        }
        return UbuntuBoldFont;
    }

    public static Typeface getUbuntuMediumFont(Context context) {
        if (UbuntuMediumFont == null) {
            UbuntuMediumFont = Typeface.createFromAsset(context.getAssets(), UBUNTU_MEDIUM);
        }

        return UbuntuMediumFont;
    }

    public static Typeface getUbuntuRegularFont(Context context) {
        if (UbuntuRegularFont == null) {
            UbuntuRegularFont = Typeface.createFromAsset(context.getAssets(), UBUNTU_REGULAR);
        }
        return UbuntuRegularFont;
    }

    public static Typeface getFontByName(Context context, String fontTTFName) {
        if (fontTTFName == null || context == null) {
            return null;
        }
        switch (fontTTFName) {
            case UBUNTU_REGULAR:
                return getUbuntuRegularFont(context);
            case UBUNTU_BOLD:
                return getUbuntuBoldFont(context);
            case UBUNTU_MEDIUM:
                return getUbuntuMediumFont(context);
        }

        return null;
    }

    public static Typeface getTypeface(Context context, String name) {

        return Typeface.createFromAsset(context.getAssets(), name);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static String getFormattedAmount(@NonNull Context context, float amount) {
        if (context == null) return "";
        String value = new DecimalFormat("##,##,##0").format(amount);
        return context.getString(R.string.rs_symbol, value);
    }

    public static String getFormattedAmountByString(@NonNull Context context, String amount) {
        if (context == null) return "";
        try {
            String value = new DecimalFormat("##,##,##0").format(Float.valueOf(amount));
            return context.getString(R.string.rs_symbol, value);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, " number format exception" + e.getMessage());
            return "";
        }
    }

    public static String getFormattedDateForBookingPage(Context context, DateSlot slot) {
        LogUtils.LOGD(TAG, "getFormattedDateForBookingPage : ");
        String str = null;
        String timeSeperator = context.getString(R.string.booking_time_seperator);
        String dateSeperator = context.getString(R.string.booking_date_seperator);
        DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        if (slot != null && slot.getDisplayDate() != null) {
            str = format.format(slot.getDate() * 1000);
            if (slot.getSelectedTimeSlot() != null) {
                str +=
                  dateSeperator + slot.getSelectedTimeSlot().getDisplayFrom() + timeSeperator + slot.getSelectedTimeSlot()
                    .getDisplayTo();
            }
        }

        return str;
    }

    public static String getFormatedWeekDay(long milliseconds) {
        LogUtils.LOGD(TAG, "getFormatedDay : ");

        Time time = new Time();
        time.set(System.currentTimeMillis());

        int thenYear = time.year;
        int thenMonth = time.month;
        int thenMonthDay = time.monthDay;

        time.set(milliseconds);
        if ((thenYear == time.year) && (thenMonth == time.month) && (thenMonthDay == time.monthDay)) {
            return "TODAY";
        } else if ((thenYear == time.year) && (thenMonth == time.month) && (thenMonthDay + 1 == time.monthDay)) {
            return "TOM";
        } else {
            SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");
            Date netDate = (new Date(milliseconds));

            dayFormat.applyPattern("EEE");
            String day = dayFormat.format(netDate);
            return day;
        }
    }

    public static String getFormatDay(long timestamp) {
        LogUtils.LOGD(TAG, "getDay : ");
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
            Date netDate = (new Date(timestamp));
            df.applyPattern("dd");
            String day = df.format(netDate);
            return day;
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "getDay : error");
        }

        return null;
    }

    public static String getFormatDate(long timestamp) {
        LogUtils.LOGD(TAG, "getDay : ");
        if (timestamp <= 0) return null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date netDate = (new Date(timestamp));
            String date = df.format(netDate);
            return date;
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "getDay : error");
        }

        return null;
    }

    public static String getFormatDateForSpecialOffer(long timestamp) {
        LogUtils.LOGD(TAG, "getDay : ");
        if (timestamp <= 0) return null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timestamp));
            String date = df.format(netDate);
            return date;
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "getDay : error");
        }

        return null;
    }

    public static long getMillisFromDate(String date) {
        LogUtils.LOGD(TAG, "getDay : ");
        if (TextUtils.isEmpty(date)) return 0;
        try {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            Date formatDate = df.parse(date);
            return formatDate.getTime();
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "getDay : error");
        }

        return 0;
    }

    public static int getAmountFromTxt(String amountTxt) {
        int value = 0;
        if (!TextUtils.isEmpty(amountTxt)) {
            try {
                value = Integer.valueOf(amountTxt.replace("&#8377;", ""));
            } catch (Exception e) {

            }
        }
        return value;
    }

    public static String getNameFromBookingList(Context context, List<ConfirmBookingInfo> infoList) {
        LogUtils.LOGD(TAG, "getNameFromBookingList : ");
        if (infoList == null) return null;

        StringBuilder builder = new StringBuilder();
        String andSeperator = context.getString(R.string.and_seperator);
        String commaSeperator = context.getString(R.string.comma_seperator);
        String replaceText = context.getString(R.string.services);
        for (int index = 0; index < infoList.size(); index++) {

            builder.append(infoList.get(index).getBucketName().replace(replaceText, "").trim());
            if (infoList.size() > 1 && index != infoList.size() - 1) {
                if (index != infoList.size() - 2) {
                    builder.append(commaSeperator);
                } else {
                    builder.append(andSeperator);
                }
            }
        }

        return builder.toString();
    }

    public static Spanned getHtmlFormatText(String text) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY); // for 24 api and more
        } else {
            return Html.fromHtml(text); // or for older api
        }
    }

    public static String getFormattedDateTime(String date, String time) {
        String format = date + ", " + time;
        return format;
    }

    public static String getPercentageOfPrice(double originalPrice, double offerPrice) {
        if (originalPrice == offerPrice || originalPrice == 0) return null;
        double value = 100 - ((offerPrice / originalPrice) * 100);

        String str = new DecimalFormat("##,##,##0").format(value);
        return str;
    }

    public static String getFormattedAmount(@NonNull Context context, double amount) {
        if (context == null) return "";
        String value = new DecimalFormat("##,##,##0.##").format(amount);
        return context.getString(R.string.rs_symbol, value);
    }

    public static String removeLineSeperator(String line) {
        if (!TextUtils.isEmpty(line)) {
            line = line.replace("\n", "");
        }

        return line;
    }

    public static String getDensityKey(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        for (AppConstants.IMAGE_SIZE size : AppConstants.IMAGE_SIZE.values()) {
            if (size.getDensity() == density) {
                return size.getKey();
            }
        }

        return null;
    }

    public static List<Services> getAllServices(ArrayList<CartServiceGroup> groupList) {
        LogUtils.LOGD(TAG, "getAllServices : ");
        if (groupList != null && groupList.size() > 0) {
            List<Services> list = new ArrayList<>();
            for (CartServiceGroup group : groupList) {
                if (group.getServices() != null && group.getServices().size() > 0) {
                    group.getServices().get(0).setGroupName(group.getTitle());
                    list.addAll(group.getServices());
                }
            }
            return list;
        }

        return null;
    }

    public static StatusModel generateStatusModel(Context context, int status, String statusMessage,
      int maximumProgressStatus, String color) {
        LogUtils.LOGD(TAG, "generateStatusModel : ");
        StatusModel model = new StatusModel();
        model.setDuration(context.getResources().getInteger(R.integer.status_view_anim_duration));
        model.setMaxStatus(maximumProgressStatus);
        // Set 0 to display only status msg
        model.setMessage(statusMessage);
        model.setStatus(status);
        model.setStatusTxtColor(color);
        if (status > 0) {
            model.setMsgGravity(Gravity.CENTER);
            //model.setMsgGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            model.setAnimationEnable(true);
            model.setEnableArrow(true);
        } else {
            model.setAnimationEnable(false);
            model.setEnableArrow(false);
            model.setMsgGravity(Gravity.CENTER);
        }

        if (status == 1 || status == maximumProgressStatus) {
            model.setEnableArrow(true);
        }
        /*if (status == 1) {
            model.setAnimationEnable(true);
        }*/
        return model;
    }

    public static boolean isValidColorCode(String value) {
        if (!TextUtils.isEmpty(value) && value.startsWith("#") && (value.length() == 7 || value.length() == 9)) return true;

        return false;
    }

    public static Drawable getTintDrawable(Context context, int res, int color) {
        LogUtils.LOGD(TAG, "getTintDrawable : ");

        Drawable d = (ContextCompat.getDrawable(context, res));
        if (d != null) {
            Drawable drawable = DrawableCompat.wrap(d.mutate());
            DrawableCompat.setTint(drawable, color);
        }
        return d;
    }

    /**
     * Set Tint color to drawable
     * If Drawable is background drawable then u must call that view.invalidate()
     * after this method
     */
    public static void setTintToDrawable(Context context, Drawable drawable, int color) {
        LogUtils.LOGD(TAG, "getTintDrawable : ");
        if (drawable != null) {
            Drawable d = DrawableCompat.wrap(drawable.mutate());
            DrawableCompat.setTint(d, color);
        }
    }
}
