package com.athomediva.utils;

/**
 * Created by kartheeswaran on 06/06/17.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RSRuntimeException;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.content.ContextCompat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by vineeth on 4/20/17.
 */

public class FileUtils {

    public static File getCacheDir(Context context) {
        File temp = null;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED && android.os.Environment.getExternalStorageState()
          .equals(android.os.Environment.MEDIA_MOUNTED)) {
            temp = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

            boolean ifDirExistedOrCreated = temp.exists() || temp.mkdirs();
            boolean dirWritable = ifDirExistedOrCreated && temp.canWrite();

            if (!dirWritable) {
                temp = null;
            }
        }

        if (temp == null) {
            temp = context.getCacheDir();
        }

        return temp;
    }

    public static File getAudioDir(Context context) {
        File temp = null;
        final boolean extStoragePermission =
          ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED;
        if (extStoragePermission && android.os.Environment.getExternalStorageState()
          .equals(android.os.Environment.MEDIA_MOUNTED)) {
            temp = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC);

            boolean ifDirExistedOrCreated = temp.exists() || temp.mkdirs();
            boolean dirWritable = ifDirExistedOrCreated && temp.canWrite();

            if (!dirWritable) {
                temp = null;
            }
        }

        if (temp == null) {
            temp = context.getCacheDir();
        }

        return temp;
    }

    public static File getDocsDir(Context context) {
        File cacheDir = null;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED && android.os.Environment.getExternalStorageState()
          .equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);

            boolean ifDirExistedOrCreated = cacheDir.exists() || cacheDir.mkdirs();
            boolean dirWritable = ifDirExistedOrCreated && cacheDir.canWrite();

            if (!dirWritable) {
                cacheDir = null;
            }
        }

        if (cacheDir == null) {
            cacheDir = context.getCacheDir();
        }

        return cacheDir;
    }

    public static File getCameraDir(Context context) {
        File temp = null;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED && android.os.Environment.getExternalStorageState()
          .equals(android.os.Environment.MEDIA_MOUNTED)) {
            temp = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

            boolean ifDirExistedOrCreated = temp.exists() || temp.mkdirs();
            boolean dirWritable = ifDirExistedOrCreated && temp.canWrite();

            if (!dirWritable) {
                temp = null;
            }
        }

        if (temp == null) {
            temp = context.getCacheDir();
        }

        return temp;
    }

    public static File createTempFile(Context context, final boolean isToCrop) throws IOException {
        File tempFile = new File(FileUtils.getCameraDir(context), (isToCrop ? "temp_pic_cropped.jpg" : "temp_pic.jpg"));
        if (tempFile.exists() && tempFile.isFile()) tempFile.delete();
        tempFile.createNewFile();
        return tempFile;
    }

    public static File createFile(Context context, final boolean isToCrop) throws IOException {
        File tempFile = new File(FileUtils.getCameraDir(context), (isToCrop ? System.currentTimeMillis() +"_temp_pic_cropped.jpg" : System.currentTimeMillis() +"_temp_pic.jpg"));
        if (tempFile.exists() && tempFile.isFile()) tempFile.delete();
        tempFile.createNewFile();
        return tempFile;
    }

    public static void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1) {
                    break;
                }
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static Bitmap getBitmap(File f) {
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(f);
            return BitmapFactory.decodeStream(fin);
        } catch (FileNotFoundException e) {
        } catch (OutOfMemoryError e) {

        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    public static int dpToPx(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    public static Bitmap getThumbNail(Context context, Uri uri, float width, float height) {
        Bitmap imageFile = null;//compressImageFile(context, uri);
        try {
            imageFile = BitmapUtils.decodeUri(context, uri, 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ThumbnailUtils.extractThumbnail(imageFile, (int)width, (int)height);
    }

    public static File compressImageFile(Context context, final Uri imageFileUri) {
        FileOutputStream fileOutputStream = null;
        try {
            Bitmap bitmap = BitmapUtils.decodeUri(context, imageFileUri, 400);

            File f = createTempFile(context, false);
            if (!f.exists()) {
                f.createNewFile();
            }
            fileOutputStream = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            return f;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static long getFileSizeInMB(File file) {
        if (file == null) {
            return 0;
        }
        return file.length() / (1024 * 1024);
    }

    @SuppressLint("NewApi")
    public static Bitmap blur(Context context, Bitmap image, String url) {

        //String blurredBitmapUrl = getBlurredBitmapUrlForCachingKey(url);

        //Bitmap cachedBitmap = VolleyL1Cache.getVolleyL1Cache(QuikrApplication.context).getBitmap(blurredBitmapUrl);

        //if(cachedBitmap != null)
        //{
        //    LogUtils.LOGD("FileUtils", "Returned cached bitmap "+blurredBitmapUrl);
        //    return cachedBitmap;
        //}

        if (image.getConfig() == null || image.getWidth() == 0
          || image.getHeight() == 0) {
            //probably an empty image that will cause exception in renderscript
            // http://192.168.124.64:8080/browse/ANDROID-9531
            return image;
        }

        if (image.getWidth() >= image.getHeight()){
            image = Bitmap.createBitmap(
              image,
              image.getWidth()/2 - image.getHeight()/2,
              0,
              image.getHeight(),
              image.getHeight()
            );

        }else{
            image = Bitmap.createBitmap(
              image,
              0,
              image.getHeight()/2 - image.getWidth()/2,
              image.getWidth(),
              image.getWidth()
            );
        }

        try {
            Bitmap outBitmap = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);

            RenderScript rs = RenderScript.create(context.getApplicationContext());
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));

            Allocation tmpIn = Allocation.createFromBitmap(rs, image);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outBitmap);

            theIntrinsic.setRadius(2.0f);

            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);

            tmpOut.copyTo(outBitmap);

            //VolleyL1Cache.getVolleyL1Cache(QuikrApplication.context).putBitmap(blurredBitmapUrl, outBitmap);

            rs.destroy();

            return outBitmap;

        }catch (RSRuntimeException e){

            //VolleyL1Cache.getVolleyL1Cache(QuikrApplication.context).putBitmap(blurredBitmapUrl, image);
            return image;
        }

    }

    public static String getFilePathFromUri(Context context, Uri uri) {
        File file = FileUtils.compressImageFile(context, uri);
        String filePath = null;
        if (file != null) {
            LOGD("FileUtils", "getFilePathFromUri : file absolute path - " + file.getAbsolutePath());
            filePath = file.getAbsolutePath();
        }
        return filePath;
    }
}

