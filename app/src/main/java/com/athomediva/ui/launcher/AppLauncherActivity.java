package com.athomediva.ui.launcher;

import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Surface;
import android.view.TextureView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.login.LoginFragment;
import hugo.weaving.DebugLog;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

public class AppLauncherActivity extends BaseActivity implements AppLauncherContract.View {
    private final String TAG = LogUtils.makeLogTag(AppLauncherActivity.class.getSimpleName());
    @BindView(R.id.video_play_back_video) TextureView mVideoView;
    @Inject AppLauncherContract.Presenter mPresenter;
    private MediaPlayer mMediaPlayer;
    private Surface mVideoSurface;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_activity_app_launcher);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.AppLauncherModule(getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LOGD(TAG, "initView: ");
        mVideoView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                LOGD(TAG, "onSurfaceTextureAvailable: ");
                mVideoSurface = new Surface(surfaceTexture);
                mPresenter.onReadyForPlayback();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                mVideoSurface = null;
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });

        if (mVideoView.getSurfaceTexture() != null) {
            mVideoSurface = new Surface(mVideoView.getSurfaceTexture());
            mPresenter.onReadyForPlayback();
        }
    }

    @DebugLog
    private void playVideo(String fileName) throws Exception {
        LOGD(TAG, "playVideo: ");
        if (mVideoSurface == null) {
            LOGE(TAG, "playVideo: video surface is null ***** ");
            return;
        }

        if (mMediaPlayer == null || !mMediaPlayer.isPlaying()) {
            AssetFileDescriptor afd = getAssets().openFd(fileName);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mMediaPlayer.setSurface(mVideoSurface);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(mediaPlayer -> mediaPlayer.start());
        } else {
            LOGE(TAG, "playVideo: media is already playing ");
        }
    }

    @DebugLog
    @Override
    public void startVideoPlayback(String fileName) throws Exception {
        LOGD(TAG, "startVideoPlayback: ");
        playVideo(fileName);
    }

    @DebugLog
    @Override
    public void showLoginPageView(Bundle bundle) {
        LOGD(TAG, "showLoginPageView: ");
        if (getSupportFragmentManager().findFragmentByTag(LoginFragment.TAG) == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.login_container, LoginFragment.newInstance(bundle), LoginFragment.TAG)
              .commitAllowingStateLoss();
        }
    }

    @Override
    protected void onDestroy() {
        LOGD(TAG, "onDestroy: ");
        mVideoView.setSurfaceTextureListener(null);
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed();
        super.onBackPressed();
    }
}
