package com.athomediva.ui.packages;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.HorizontalSeparatorDecoration;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 10/04/17.
 */

public class PackageSectionWidget extends RelativeLayout {
    private static final String TAG = PackageSectionWidget.class.getSimpleName();

    @BindView(R.id.package_txt) TextView mPackageTxt;
    @BindView(R.id.view_all) TextView mViewAll;
    @BindView(R.id.packages_list) RecyclerView mPackageList;
    private View.OnClickListener mPlusClickListener;
    private View.OnClickListener mMinusClickListener;
    private ClickableSpan mInfoClickListener;
    private View.OnClickListener mViewAllClick;
    private PackagesAdapter mAdapter;
    private List<Services> mPackagesList = new ArrayList<>();

    public PackageSectionWidget(Context context) {
        super(context);
    }

    public PackageSectionWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PackageSectionWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        ButterKnife.bind(this);
        mAdapter = new PackagesAdapter(getContext(), R.layout.ahd_package_item, mPackagesList);
        mPackageList.setAdapter(mAdapter);

        mPackageList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ((SimpleItemAnimator) mPackageList.getItemAnimator()).setSupportsChangeAnimations(false);
        HorizontalSeparatorDecoration decoration = new HorizontalSeparatorDecoration(getContext(), Color.TRANSPARENT,
          getResources().getDimension(R.dimen.item_space_horizontal_large),
          getResources().getDimension(R.dimen.item_space_horizontal_large));
        mPackageList.addItemDecoration(decoration);
    }

    @DebugLog
    public void updatePackages(ArrayList<Services> list) {
        LogUtils.LOGD(TAG, "updatePackages : " + list);
        mPackagesList.clear();
        if (list != null && !list.isEmpty()) {
            mPackagesList.addAll(list);
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void refreshPackages(List<Services> list) {
        LogUtils.LOGD(TAG, "refreshPackages : ");
        mPackagesList.clear();
        if (list != null && !list.isEmpty()) {
            mPackagesList.addAll(list);
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @DebugLog
    public void updatePackageItem(Services item) {
        LogUtils.LOGD(TAG, "updatePackageItem : ");
        if (mAdapter != null) mAdapter.notifyItemChanged(item);
    }

    @DebugLog
    public void setUpdateListener(OnClickListener plusClick, OnClickListener minusClick, ClickableSpan infoClick,
      OnClickListener viewAllClick) {
        LogUtils.LOGD(TAG, "setUpdateListener : ");
        mPlusClickListener = plusClick;
        mMinusClickListener = minusClick;
        mInfoClickListener = infoClick;
        mViewAllClick = viewAllClick;
        if (mViewAll != null) {
            mViewAll.setOnClickListener(mViewAllClick);
        }
        if (mAdapter != null) {
            mAdapter.setUpdateListener(mPlusClickListener, mMinusClickListener, mInfoClickListener);
        }
    }
}
