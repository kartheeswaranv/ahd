package com.athomediva.ui.packages;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import java.util.List;

/**
 * Created by kartheeswaran on 10/04/17.
 */

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.ViewHolder> {
    private static final String TAG = PackagesAdapter.class.getSimpleName();

    private final Context mContext;
    private final List<Services> mPackageList;
    private final int mLayoutRes;

    private View.OnClickListener mPlusListener, mMinusListener;
    private ClickableSpan mInfoListener;

    public PackagesAdapter(@NonNull Context context, int resId, @NonNull List<Services> list) {
        LogUtils.LOGD(TAG, "PackagesAdapter : ");
        mContext = context;
        mLayoutRes = resId;
        mPackageList = list;
        if (mPackageList != null) {
            LogUtils.LOGD(TAG, "PackagesAdapter : " + mPackageList.size());
        }
    }

    @DebugLog
    public void setUpdateListener(View.OnClickListener plusClickListener, View.OnClickListener minusClickListener,
      ClickableSpan infoClickListener) {
        LogUtils.LOGD(TAG, "setUpdateListener : ");
        mPlusListener = plusClickListener;
        mMinusListener = minusClickListener;
        mInfoListener = infoClickListener;
    }

    @Override
    public int getItemCount() {
        LogUtils.LOGD(TAG, "getItemCount : ");
        return (mPackageList != null ? mPackageList.size() : 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LogUtils.LOGD(TAG, "onCreateViewHolder : ");
        View view = LayoutInflater.from(mContext).inflate(mLayoutRes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogUtils.LOGD(TAG, "onBindViewHolder : ");
        Services item = mPackageList.get(position);
        if (item != null) {
            holder.mWidget.setTag(item);
            holder.mWidget.setListener(mPlusListener, mMinusListener, mInfoListener);
            holder.mWidget.updateView(item);
        }
    }

    public void notifyItemChanged(Services model) {
        if (model != null) {
            int index = mPackageList.indexOf(model);
            if (index != -1) {
                notifyItemChanged(index);
            }
        }
    }

    public void refreshPackages(List<Services> servicesList) {
        notifyDataSetChanged();
        //mPackageList
        /*if(mPackageList != null) {
            final ServicesDiffUtils diffCallback = new ServicesDiffUtils(mPackageList, servicesList);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback, true);
            mPackageList.clear();
            mPackageList.addAll(servicesList);
            diffResult.dispatchUpdatesTo(this);
        }*/
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        final PackageItemWidget mWidget;

        public ViewHolder(View itemView) {
            super(itemView);
            mWidget = (PackageItemWidget) itemView;
        }
    }
}
