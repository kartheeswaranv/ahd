package com.athomediva.ui.packages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.Services;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.PackagesContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.home.InfoAdapter;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class PackagesFragment extends BaseFragment<PackagesContract.Presenter, PackagesContract.View>
  implements PackagesContract.View {
    public static final String TAG = LogUtils.makeLogTag(PackagesFragment.class.getSimpleName());

    @BindView(R.id.packages_list) RecyclerView mPackagesListView;

    private PackagesAdapter mAdapter;
    @Inject PackagesContract.Presenter mPresenter;
    private InfoDialog mInfoDialog;
    private List<Services> mPackageList = new ArrayList<>();

    public static PackagesFragment newInstance(Bundle b) {
        PackagesFragment fragment = new PackagesFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.package_section_frag, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.PackageModule(getArguments())).inject(this);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View view) {
        LogUtils.LOGD(TAG, "initializeView : ");

        mAdapter = new PackagesAdapter(getContext(), R.layout.ahd_package_item_view, mPackageList);
        mAdapter.setUpdateListener(v -> mPresenter.onPlusClick((Services) v.getTag()),
          v -> mPresenter.onMinusClick((Services) v.getTag()), new ClickableSpan() {
              @Override
              public void onClick(View view) {
                  LogUtils.LOGD(TAG, "onClick : ");
                  Services serv = (Services) view.getTag();
                  mPresenter.onInfoClick(serv);
              }
          });
        mPackagesListView.setAdapter(mAdapter);
        mPackagesListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        ((SimpleItemAnimator) mPackagesListView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @DebugLog
    @Override
    public void updatePackages(List<Services> packageList) {
        LogUtils.LOGD(TAG, "updatePackages : ");
        mPackageList.clear();
        if(packageList != null && !packageList.isEmpty()) {
            mPackageList.addAll(packageList);
        }
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }

    }

    @DebugLog
    public void updatePackageItem(Services item) {
        LogUtils.LOGD(TAG, "updatePackageItem : ");
        if (mAdapter != null) {
            mAdapter.notifyItemChanged(item);
        }
    }

    @DebugLog
    public void showInfoDialog(String title, ArrayList<String> infoList) {
        LOGD(TAG, "showInfoDialog : ");
        if (TextUtils.isEmpty(title)) return;

        if(mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        View view = LayoutInflater.from(getContext()).inflate(R.layout.ahd_package_info_dialog, null);
        ExpandListView infoListView = (ExpandListView) view;
        infoListView.setAdapter(
          new InfoAdapter(getContext(), R.layout.ahd_dialog_info_item, infoList.toArray(new String[] {})));

        InfoDialogModel model = new InfoDialogModel(title, view);
        mInfoDialog = new InfoDialog(getContext(), model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view1 -> mInfoDialog.dismiss());
    }

    @Override
    public void onDestroyView() {
        LogUtils.LOGD(TAG,"onDestroyView : ");
        if(mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        super.onDestroyView();
    }
}
