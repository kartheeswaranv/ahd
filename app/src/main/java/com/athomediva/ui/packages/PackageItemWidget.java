package com.athomediva.ui.packages;

import android.content.Context;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandableTextView;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.remote.Services;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.services.CountWidget;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 22/04/17.
 */

public class PackageItemWidget extends RelativeLayout {
    private static final String TAG = PackageItemWidget.class.getSimpleName();

    @BindView(R.id.info) ExpandableTextView mInfoTxt;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.offer_info) TextView mOfferTxt;
    @BindView(R.id.amt) TextView mAmount;
    @BindView(R.id.discount_amt) TextView mDiscountAmt;
    @BindView(R.id.count_widget) CountWidget mCountWidget;
    @BindView(R.id.duration) TextView mDuration;

    private Services mPackageModel;

    private OnClickListener mPlusListener;
    private OnClickListener mMinusListener;
    private ClickableSpan mInfoListener;

    public PackageItemWidget(Context context) {
        super(context);
    }

    public PackageItemWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PackageItemWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setListener(OnClickListener onPlusClickListener, OnClickListener onMinusClickListener,
      ClickableSpan onInfoClickListener) {
        LogUtils.LOGD(TAG, "setListener : ");
        mPlusListener = onPlusClickListener;
        mMinusListener = onMinusClickListener;
        mInfoListener = onInfoClickListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
    }

    @DebugLog
    public void updateView(Services item) {
        LogUtils.LOGD(TAG, "updateView : ");
        mPackageModel = item;
        if (mPackageModel != null) {
            mCountWidget.setTag(mPackageModel);
            mInfoTxt.setTag(mPackageModel);
            if (mPackageModel.getInfoList() != null) {
                mInfoTxt.setClickableSpan(mInfoListener);
            }

            UIHelper.hideViewIfEmptyTxt(mTitle, mPackageModel.getName());
            UIHelper.hideViewIfEmptyTxt(mInfoTxt, UiUtils.removeLineSeperator(mPackageModel.getInfo()));
            if (TextUtils.isEmpty(mPackageModel.getSaveText())) {
                mOfferTxt.setVisibility(View.INVISIBLE);
            } else {
                mOfferTxt.setVisibility(View.VISIBLE);
                mOfferTxt.setText(mPackageModel.getSaveText());
            }

            if (mPackageModel.getDuration() > 0) {
                mDuration.setVisibility(View.VISIBLE);
                mDuration.setText(mPackageModel.getDuration() + " " + mPackageModel.getDurationText());
            } else {
                mDuration.setVisibility(View.INVISIBLE);
            }

            mInfoTxt.requestLayout();

            if (mPackageModel.getStrikeOffPrice() > 0) {
                mAmount.setVisibility(View.VISIBLE);
                mAmount.setText(UiUtils.getFormattedAmount(getContext(), mPackageModel.getStrikeOffPrice()));
            } else {
                mAmount.setVisibility(View.GONE);
            }
            mDiscountAmt.setText(UiUtils.getFormattedAmount(getContext(), mPackageModel.getPrice()));
            mCountWidget.setSelectionType(AppConstants.SERVICE_SELECTION_TYPE.SINGLE_SELECT.getType());
            mCountWidget.updateView(mPackageModel.getQuantity());
            mCountWidget.setUpdateListener(mPlusListener, mMinusListener);

            if (item.isFreeServices()) {
                mCountWidget.setVisibility(View.GONE);
            } else {
                mCountWidget.setVisibility(View.VISIBLE);
            }
        }
    }
}
