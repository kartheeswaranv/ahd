package com.athomediva.ui.mywallet;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MyWalletContract;
import com.athomediva.ui.BaseActivity;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 *
 * }
 */

public class MyWalletActivity extends BaseActivity implements MyWalletContract.View {
    public static final String TAG = LogUtils.makeLogTag(MyWalletActivity.class.getSimpleName());
    @BindView(R.id.root_layout) LinearLayout mRootLayout;
    @BindView(R.id.available_balance_amount) TextViewCustom mAvailableBalance;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @Inject MyWalletContract.Presenter mPresenter;

    /*public static MyWalletActivity newInstance(Bundle bundle) {
        MyWalletActivity fragment = new MyWalletActivity();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ahd_my_wallets_frag, container, false);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.MyWalletModule(getArguments())).inject(this);
        LOGD(TAG, "onCreateView: ");
        bindView(this, view);

        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.ahd_my_wallet));
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_my_wallets_activity);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.MyWalletModule(getIntent().getExtras())).inject(this);
        initViews();
        mPresenter.attachView(this);
        AHDApplication.get(this)
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.WALLET,
            GATrackerContext.Label.WALLET_PAGE_LOAD);
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.custom_title);
        title.setText(getString(R.string.ahd_my_wallet));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void updateWalletBreakupViews(String amount, List<TitleDescImageItem> walletBreakupList) {
        LOGD(TAG, "updateWalletBreakupViews: ");
        mRootLayout.removeAllViews();
        mAvailableBalance.setText(amount);
        if (walletBreakupList == null || walletBreakupList.size() == 0) return;

        int marginTopDp = getResources().getDimensionPixelSize(R.dimen.divider_height);
        int sectionHeight = getResources().getDimensionPixelSize(R.dimen.wallet_info_item_height);
        for (TitleDescImageItem item : walletBreakupList) {
            SectionWalletInfoWidget sectionWidget = (SectionWalletInfoWidget) this.getLayoutInflater()
              .inflate(R.layout.ahd_wallet_section_info_widget, mRootLayout, false);
            LinearLayout.LayoutParams params =
              new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, sectionHeight);
            params.setMargins(0, marginTopDp, 0, 0);
            sectionWidget.setLayoutParams(params);
            mRootLayout.addView(sectionWidget);
            sectionWidget.updateData(item);
        }
        View shadowView = this.getLayoutInflater().inflate(R.layout.ahd_generic_view_shadow, mRootLayout, false);
        mRootLayout.addView(shadowView);
    }
}
