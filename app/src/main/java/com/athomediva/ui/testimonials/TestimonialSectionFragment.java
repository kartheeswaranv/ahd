package com.athomediva.ui.testimonials;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Testimonials;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseFragment;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class TestimonialSectionFragment extends BaseFragment {
    public final static int FIRST_PAGE = 1;
    public static final String TAG = LogUtils.makeLogTag(TestimonialSectionFragment.class.getSimpleName());
    private static final String KEY_TESTIMONIALS = "key_testimonials";
    private static final String KEY_STATUS = "key_status";
    @BindView(R.id.status_view) TextViewCustom mStatusView;

    public static TestimonialSectionFragment newInstance(ArrayList<Testimonials> testimonialList, String status) {
        TestimonialSectionFragment fragment = new TestimonialSectionFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_TESTIMONIALS, testimonialList);
        bundle.putString(KEY_STATUS, status);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.ahd_testimonial_section_frag, container, false);
        bindView(this, view);
        return view;
    }

    @Override
    protected void initializeView(View view) {
        LOGD(TAG, "initializeView");
        mStatusView.setText(getArguments().getString(KEY_STATUS, ""));
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.testimonial_view_pager);

        List<Testimonials> testimonials = getArguments().getParcelableArrayList(KEY_TESTIMONIALS);
        if (testimonials == null || testimonials.isEmpty()) {
            viewPager.setVisibility(View.GONE);
            return;
        }

        CoverFlowCarouselAdapter adapter =
          new CoverFlowCarouselAdapter(getChildFragmentManager(), getArguments().getParcelableArrayList(KEY_TESTIMONIALS));
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, adapter);

        // Set current testimonial_item to the middle page so we can fling to both
        // directions left and right
        viewPager.setCurrentItem(FIRST_PAGE);

        // Necessary or the mViewPager will only have one extra page to show
        // make this at least however many pages you can see
        viewPager.setOffscreenPageLimit(3);

        // Set margin for pages as a negative number, so a part of next and
        // previous pages will be showed
        viewPager.setPageMargin(-1 * UiUtils.dpToPx(14));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }
}
