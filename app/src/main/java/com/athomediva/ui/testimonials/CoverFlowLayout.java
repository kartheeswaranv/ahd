package com.athomediva.ui.testimonials;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.CircularNetworkImageView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Testimonials;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class CoverFlowLayout extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(CoverFlowLayout.class.getSimpleName());
    @BindView(R.id.user_city) TextViewCustom mUserCity;
    @BindView(R.id.user_name) TextViewCustom mUserName;
    @BindView(R.id.user_comments) TextViewCustom mUserComments;
    @BindView(R.id.user_img_view) CircularNetworkImageView mUserImage;
    private float mScale = CoverFlowCarouselAdapter.BIG_SCALE;

    public CoverFlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CoverFlowLayout(Context context) {
        super(context);
    }

    public void setScaleBoth(float scale) {
        this.mScale = scale;// If you want to see the mScale every time you set
        invalidate();
        // mScale you need to have this line here,
        // invalidate() function will call onDraw(Canvas)
        // to redraw the view for you
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // The main mechanism to display mScale animation, you can customize it
        // as your needs
        int w = this.getWidth();
        int h = this.getHeight();
        canvas.scale(1, mScale, 0, h);
        super.onDraw(canvas);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void updateView(Testimonials testimonial) {
        LOGD(TAG, "updateView: ");
        if (testimonial != null) {
            mUserCity.setText(testimonial.getAddress());
            mUserName.setText(testimonial.getUser());
            mUserComments.setText(testimonial.getDesc());
            mUserImage.startLoading(testimonial.getImg());
        }
    }
}
