package com.athomediva.ui.testimonials;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.athomediva.data.models.remote.Testimonials;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.ui.testimonials.TestimonialSectionFragment.FIRST_PAGE;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class CoverFlowCarouselAdapter extends FragmentPagerAdapter implements ViewPager.PageTransformer {
    public final static float BIG_SCALE = 1.0f;
    private final static float SMALL_SCALE = 0.895f;
    private final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private final List<Testimonials> mTestimonialList;

    public CoverFlowCarouselAdapter(FragmentManager fragmentManager, List<Testimonials> testimonialList) {
        super(fragmentManager);
        mTestimonialList = testimonialList;
    }

    @Override
    public Fragment getItem(int position) {
        // make the first mViewPager bigger than others
        float mScale;
        if (position == FIRST_PAGE) {
            mScale = BIG_SCALE;
        } else {
            mScale = SMALL_SCALE;
        }

        return CoverFlowFragment.newInstance(mTestimonialList.get(position), mScale);
    }

    @Override
    public int getCount() {
        if (mTestimonialList == null) return 0;
        return mTestimonialList.size();
    }

    @Override
    public void transformPage(View page, float position) {
        CoverFlowLayout itemLayout = (CoverFlowLayout) page.findViewById(R.id.item_root);
        float scale = BIG_SCALE;
        if (position > 0) {
            scale = scale - position * DIFF_SCALE;
        } else {
            scale = scale + position * DIFF_SCALE;
        }
        if (scale < 0) scale = 0;
        itemLayout.setScaleBoth(scale);
    }
}
