package com.athomediva.ui.testimonials;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.Testimonials;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class CoverFlowFragment extends Fragment {
    private static final String KEY_TESTIMONIAL = "key_testimonial";
    private static final String KEY_SCALE = "key_scale";
    @BindView(R.id.item_root) CoverFlowLayout mRootLayout;

    public static Fragment newInstance(Testimonials testimonial, float scale) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_TESTIMONIAL, testimonial);
        bundle.putFloat(KEY_SCALE, scale);
        Fragment fragment = new CoverFlowFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ahd_testimonial_item, container, false);
        ButterKnife.bind(this, view);
        float scale = this.getArguments().getFloat(KEY_SCALE);
        Testimonials testimonial = getArguments().getParcelable(KEY_TESTIMONIAL);
        mRootLayout.setScaleBoth(scale);
        mRootLayout.updateView(testimonial);
        return view;
    }
}
