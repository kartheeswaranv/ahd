package com.athomediva.ui.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.SpecialOfferContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseDialogFragment;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public class SpecialOfferDialogFragment extends BaseDialogFragment<SpecialOfferContract.Presenter, SpecialOfferContract.View>
  implements SpecialOfferContract.View {
    public static final String TAG = LogUtils.makeLogTag(SpecialDatesFragment.class.getSimpleName());

    @BindView(R.id.submit) TextView mSubmitBtn;
    @BindView(R.id.skip) TextView mSkipBtn;
    @Inject SpecialOfferContract.Presenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ahd_special_offer_dialog, container, false);
        bindView(this, rootView);
        LogUtils.LOGD(TAG, "onCreateView : ");
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.SpecialOfferModule(getArguments()))
          .inject(this);
        return rootView;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mSubmitBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Submit");
            mPresenter.onSubmitClick();
        });
        mSkipBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Skip");
            mPresenter.onSkipClick();
        });
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.LOGD(TAG, "onCreate : ");
        setStyle(STYLE_NO_TITLE, R.style.AhdSpecialOfferDialog);
    }

    @Override
    public void dismissDialog() {
        LogUtils.LOGD(TAG, "dismissDialog : ");
        dismissAllowingStateLoss();
    }

    @Override
    public void initSpecialOfferView(ArrayList<SpecialOfferDateModel> list,
      SpecialDatesFragment.SpecialDatesChangeListener listener) {
        LogUtils.LOGD(TAG, "initSpecialOfferView : ");
        if (getChildFragmentManager().findFragmentByTag(SpecialDatesFragment.TAG) == null) {
            SpecialDatesFragment specialDateSectionFrag = SpecialDatesFragment.newInstance(list, false);
            specialDateSectionFrag.setDateChangeListener(listener);
            getChildFragmentManager().beginTransaction()
              .add(R.id.special_date_container, specialDateSectionFrag, SpecialDatesFragment.TAG)
              .commit();
        } else {
            LogUtils.LOGD(TAG, "initiateSpecialOfferView : Special Date Frag is already Added");
        }
    }
}