package com.athomediva.ui.home;

/**
 * Created by kartheeswaran on 23/04/17.
 */

public interface OnSectionItemSelectionListener {
    void onItemSelect(int group, int child);
}
