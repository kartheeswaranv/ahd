package com.athomediva.ui.home;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/06/17.
 */

public class InfoAdapter extends ArrayAdapter<String> {
    private static final String TAG = InfoAdapter.class.getSimpleName();

    private Context mContext;

    public InfoAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull String[] objects) {
        super(context, resource, objects);
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.ahd_dialog_info_item, null);
        TextView tv = (TextView) view.findViewById(R.id.info_txt);
        String obj = getItem(position);
        if (!TextUtils.isEmpty(UiUtils.removeLineSeperator(obj))) tv.setText(UiUtils.removeLineSeperator(obj));

        return view;
    }
}
