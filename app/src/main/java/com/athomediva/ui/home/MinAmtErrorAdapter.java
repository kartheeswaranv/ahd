package com.athomediva.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.MinOrderErrorInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 19/06/17.
 */

public class MinAmtErrorAdapter extends BaseAdapter {
    private static final String TAG = MinAmtErrorAdapter.class.getSimpleName();

    private ArrayList<MinOrderErrorInfo> mErrorList;
    private String mTitle;
    private Context mContext;

    public MinAmtErrorAdapter(Context context, String title, ArrayList<MinOrderErrorInfo> errorList) {
        LogUtils.LOGD(TAG, "MinAmtErrorAdapter : ");
        mContext = context;
        mErrorList = errorList;
        mTitle = title;
    }

    @Override
    public int getCount() {
        if (mErrorList == null) return 0;

        return mErrorList.size();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mErrorList.get(i);
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        Holder holder;
        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_minamt_error_info, null);
            holder.mTitle = (TextView) view.findViewById(R.id.title);
            holder.mAmount = (TextView) view.findViewById(R.id.amt);
            holder.mDesc = (TextView) view.findViewById(R.id.subtitle);
            holder.mErrorMsg = (TextView) view.findViewById(R.id.error_msg);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        MinOrderErrorInfo info = mErrorList.get(pos);
        if (info != null) {
            UIHelper.hideViewIfEmptyTxt(holder.mTitle, info.getTitle());
            UIHelper.hideViewIfEmptyTxt(holder.mAmount, UiUtils.getFormattedAmount(mContext, info.getAmount()));
            UIHelper.hideViewIfEmptyTxt(holder.mDesc, "(" + mTitle + ")");
            UIHelper.hideViewIfEmptyTxt(holder.mErrorMsg, info.getErrorMsg());
        }

        return view;
    }

    static class Holder {
        TextView mTitle;
        TextView mAmount;
        TextView mDesc;
        TextView mErrorMsg;
    }
}
