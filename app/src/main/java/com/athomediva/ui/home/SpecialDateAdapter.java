package com.athomediva.ui.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public class SpecialDateAdapter extends BaseAdapter {
    private static final String TAG = SpecialDateAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<SpecialOfferDateModel> mSpecialDateList;
    private View.OnClickListener mSpecialDateItemClickListener;

    public SpecialDateAdapter(Context context, ArrayList<SpecialOfferDateModel> list) {
        LogUtils.LOGD(TAG, "SpecialDateAdapter : ");
        mSpecialDateList = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        if (mSpecialDateList == null) return 0;

        return mSpecialDateList.size();
    }

    public void setItemClickListener(View.OnClickListener listener) {
        LogUtils.LOGD(TAG, "setItemClickListener : ");
        mSpecialDateItemClickListener = listener;
    }

    @Override
    public Object getItem(int position) {
        return mSpecialDateList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.ahd_special_offer_date_item, null);
            holder = new ViewHolder();
            holder.customTextInputLayout = (CustomTextInputLayout) convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SpecialOfferDateModel model = mSpecialDateList.get(position);
        if (model != null) {
            holder.customTextInputLayout.setHint(model.getHint());
            holder.customTextInputLayout.getEditText().setText(model.getSelectedDateTxt());
            holder.customTextInputLayout.setEnabled(!model.isDisabled());
            holder.customTextInputLayout.setTag(position);
            holder.customTextInputLayout.getEditText().setTag(position);
            holder.customTextInputLayout.setOnClickListener(mSpecialDateItemClickListener);
            holder.customTextInputLayout.getEditText().setOnClickListener(mSpecialDateItemClickListener);
        }

        setTintToDrawables(holder.customTextInputLayout, model.isDisabled());
        return convertView;
    }

    static class ViewHolder {
        CustomTextInputLayout customTextInputLayout;
    }

    private void setTintToDrawables(CustomTextInputLayout view, boolean disable) {
        LogUtils.LOGD(TAG, "setTintToDrawables : ");
        if (view == null) return;

        int color;
        if (disable) {
            color = R.color.ahd_grey_dark;
        } else {
            color = R.color.ahd_tuna;
        }

        // left, top, right, bottom drawables.
        Drawable[] drawables = view.getEditText().getCompoundDrawables();
        // get right drawable.
        if (drawables != null && drawables.length >= 2) {
            Drawable rightCompoundDrawable = drawables[2];
            UiUtils.setTintToDrawable(mContext, rightCompoundDrawable, ContextCompat.getColor(mContext, color));
        }
    }
}
