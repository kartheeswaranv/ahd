package com.athomediva.ui.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import com.quikr.android.network.NetworkImageView;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

class HomePageSubcatAdapter extends ArrayAdapter<SubCategories> {
    private static final String TAG = HomePageSubcatAdapter.class.getSimpleName();

    private final Context mContext;
    private List<SubCategories> mSubCatList;
    private List<SubCategories> mList = new ArrayList<>();
    private boolean isExpand;
    private String mMoreTxt;
    private int mMoreIconRes;
    private int mDefaultSize;

    public HomePageSubcatAdapter(Context context, List<SubCategories> list) {
        super(context, R.layout.ahd_home_page_service_item);
        LogUtils.LOGD(TAG, "HomePageSubcatAdapter : ");
        mContext = context;
        if (list != null) {
            mDefaultSize = list.size();
            update(list);
        }
    }

    public HomePageSubcatAdapter(Context context, List<SubCategories> list, int columns) {
        super(context, R.layout.ahd_home_page_service_item);
        LogUtils.LOGD(TAG, "HomePageSubcatAdapter : ");
        mContext = context;
        mDefaultSize = columns;
        update(list);
    }

    public void update(List<SubCategories> list) {
        LogUtils.LOGD(TAG, "update : ");
        mSubCatList = list;
        isExpand = mSubCatList.size() <= mDefaultSize;
        if (!isExpand) {
            collapse();
        } else {
            expand();
        }
        notifyDataSetChanged();
    }

    /**
     * Set Custom More Txt and Icon
     */
    @DebugLog
    public void setMoreItems(String moreTxt, int iconRes) {
        LogUtils.LOGD(TAG, "setMoreItems : ");
        mMoreTxt = moreTxt;
        mMoreIconRes = iconRes;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;

        if (!isExpand) {
            return mList.size() + 1;
        }

        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public SubCategories getItem(int position) {
        LogUtils.LOGD(TAG, "getItem : ");
        if (!isExpand && mList.size() - 1 < position) return null;
        return mList.get(position);
    }

    @DebugLog
    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        long startTime = System.currentTimeMillis();

        Holder holder = new Holder();
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.ahd_home_page_service_item, parent, false);
            holder.tv = (TextView) convertView.findViewById(R.id.name);
            holder.img = (NetworkImageView) convertView.findViewById(R.id.icon);

            holder.view = convertView;
            convertView.setTag(holder);

            final int size = (mContext.getResources().getDisplayMetrics().widthPixels / 4) - UiUtils.dpToPx(1);
            convertView.setLayoutParams(new AbsListView.LayoutParams(size, size));
        } else {
            holder = (Holder) convertView.getTag();
        }

        SubCategories section = getItem(position);
        if (section != null) {
            holder.tv.setText(section.getName());
            holder.img.setErrorImageResId(R.drawable.ahd_ic_cat_default);
            holder.img.startLoading(section.getIcon());
        }

        // Handles the last item (More Item ) click if list is collapse(isExpand = false)

        if (!isExpand && position == mList.size()) {
            handleMoreItem(holder);
        } else {
            holder.view.setClickable(false);
        }

        Log.d(TAG, "get View time = " + (System.currentTimeMillis() - startTime));
        return convertView;
    }

    private void handleMoreItem(Holder holder) {
        LogUtils.LOGD(TAG, "handlesMore : ");
        if (holder == null) return;
        holder.view.setOnClickListener(view -> {
            isExpand = true;
            expand();
        });
        if (!TextUtils.isEmpty(mMoreTxt)) {
            holder.tv.setText(mMoreTxt);
        } else {
            holder.tv.setText(mContext.getString(R.string.more));
        }

        if (mMoreIconRes == 0) {
            holder.img.setDefaultResId(R.drawable.ahd_ic_more);
        } else {
            holder.img.setImageResource(mMoreIconRes);
        }
    }

    private void expand() {
        LogUtils.LOGD(TAG, "expand : ");
        if (isExpand) {
            mList = mSubCatList;
            notifyDataSetChanged();
        }
    }

    private void collapse() {
        LogUtils.LOGD(TAG, "collapse : ");
        if (!isExpand && mSubCatList.size() > mDefaultSize) {
            mList = mSubCatList.subList(0, mDefaultSize - 1);
        } else {
            expand();
        }
    }

    public class Holder {
        TextView tv;
        NetworkImageView img;
        View view;
    }
}
