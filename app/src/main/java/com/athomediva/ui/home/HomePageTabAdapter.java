package com.athomediva.ui.home;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.athomediva.customview.CustomViewPager;
import com.athomediva.customview.ExpandableGridView;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 06/04/17.
 */

class HomePageTabAdapter extends PagerAdapter {
    private static final String TAG = HomePageTabAdapter.class.getSimpleName();

    private final Context mContext;
    private final List<Categories> mList;
    private OnSectionItemSelectionListener mListener;

    public HomePageTabAdapter(Context context, List<Categories> list) {
        LogUtils.LOGD(TAG, "HomePageTabAdapter : ");
        mContext = context;
        mList = list;
    }

    public void setSectionItemListener(OnSectionItemSelectionListener listener) {
        LogUtils.LOGD(TAG, "setSectionItemListener : ");
        mListener = listener;
    }

    @Override
    public int getCount() {
        if (mList == null) return 0;
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LogUtils.LOGD(TAG, "instantiateItem : ");
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.ahd_expand_collapse_grid, container, false);
        initView(container, itemView, position);
        itemView.setTag(position);
        return itemView;
    }

    private void initView(ViewGroup container, View itemView, int position) {
        LogUtils.LOGD(TAG, "initView : ");
        ExpandableGridView gridView = (ExpandableGridView) itemView.findViewById(R.id.category_grid);
        gridView.setAdapter(new HomePageSubcatAdapter(mContext, mList.get(position).getSubcategories(), 8));
        gridView.setExpanded(true);
        gridView.setOnItemClickListener(new Listener(position));
        container.addView(itemView);
        CustomViewPager pager = (CustomViewPager) container;
        pager.requestLayout();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        LogUtils.LOGD(TAG, "destroyItem : ");
        container.removeView((ViewGroup) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mList.get(position).getName();
    }

    private class Listener implements AdapterView.OnItemClickListener {
        int mGroup = -1;

        public Listener(int group) {
            mGroup = group;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
            LogUtils.LOGD(TAG, "onItemClick : ");
            if (mListener != null && mGroup != -1) {
                mListener.onItemSelect(mGroup, pos);
            } else {
                LogUtils.LOGD(TAG, "onItemClick : Invalid Group");
            }
        }
    }
}
