package com.athomediva.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.HorizontalSeparatorDecoration;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 18/06/17.
 */

public class RecentBookingsWidget extends RelativeLayout {
    private static final String TAG = RecentBookingsWidget.class.getSimpleName();

    @BindView(R.id.booking_list) RecyclerView mBookingsListView;

    private List<Booking> mBookingList = new ArrayList<>();
    private OnClickListener mItemClickListener;
    private OnClickListener mRateServiceListener;
    private OnClickListener mPayOnlineListener;
    private RecentBookingAdapter mAdapter;

    public RecentBookingsWidget(Context context) {
        super(context);
    }

    public RecentBookingsWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecentBookingsWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        mAdapter = new RecentBookingAdapter(getContext(), mBookingList);
        RecyclerView.LayoutManager mLayoutManager =
          new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mBookingsListView.setLayoutManager(mLayoutManager);
        HorizontalSeparatorDecoration decoration = new HorizontalSeparatorDecoration(getContext(), Color.TRANSPARENT,
          getResources().getDimension(R.dimen.item_space_horizontal_large), 0);
        mBookingsListView.addItemDecoration(decoration);
        mBookingsListView.setItemAnimator(new DefaultItemAnimator());
        mBookingsListView.setAdapter(mAdapter);
    }

    public void setOnItemClickListener(OnClickListener listener) {
        LogUtils.LOGD(TAG, "setOnItemClickListener : ");
        mItemClickListener = listener;
        if (mAdapter != null) {
            mAdapter.setItemClickListener(mItemClickListener);
        }
    }

    public void setActionListener(OnClickListener rateClick, OnClickListener payonlineClick) {
        LogUtils.LOGD(TAG, "setActionListener : ");
        mRateServiceListener = rateClick;
        mPayOnlineListener = payonlineClick;
        if (mAdapter != null) {
            mAdapter.setActionListener(mRateServiceListener, mPayOnlineListener);
        }
    }

    public void updateRecentBookings(List<Booking> list) {
        if (mBookingList != null) {
            mBookingList.clear();
            if (list != null) {
                mBookingList.addAll(list);
                mAdapter.resetItemWidth(mBookingList.size());
            }
        }
        mAdapter.notifyDataSetChanged();
    }
}
