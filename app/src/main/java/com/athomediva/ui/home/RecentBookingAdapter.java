package com.athomediva.ui.home;

/**
 * Created by kartheeswaran on 18/06/17.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class RecentBookingAdapter extends RecyclerView.Adapter<RecentBookingAdapter.ViewHolder> {
    private final String TAG = LogUtils.makeLogTag(RecentBookingAdapter.class.getSimpleName());
    private final List<Booking> mBookingList;
    private final Context mContext;
    private View.OnClickListener mListener;
    private View.OnClickListener mRateClickListener;
    private View.OnClickListener mPayOnlineListener;
    private int mItemWidth = 0;

    public RecentBookingAdapter(Context context, List<Booking> bookingList) {
        mBookingList = bookingList;
        mContext = context;
        if (mBookingList != null) resetItemWidth(mBookingList.size());
    }

    public void setItemClickListener(View.OnClickListener itemClick) {
        LogUtils.LOGD(TAG, "setItemClickListener : ");
        mListener = itemClick;
    }

    public void setActionListener(View.OnClickListener rateClick, View.OnClickListener payonlineClick) {
        LogUtils.LOGD(TAG, "setActionListener : ");
        mRateClickListener = rateClick;
        mPayOnlineListener = payonlineClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_recent_bookings_item, parent, false);
        itemView.getLayoutParams().width = mItemWidth;
        LOGD(TAG, "onCreateViewHolder: ");

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LOGD(TAG, "onBindViewHolder: ");
        Booking booking = mBookingList.get(position);
        holder.bind(mContext, booking, position);
        holder.mView.setTag(mBookingList.get(position));
        holder.mRateService.setTag(mBookingList.get(position));
        holder.mPayOnline.setTag(mBookingList.get(position));
        holder.mView.setOnClickListener(mListener);
        holder.mRateService.setOnClickListener(mRateClickListener);
        holder.mPayOnline.setOnClickListener(mPayOnlineListener);
        if (!TextUtils.isEmpty(booking.getStatusColor())) {
            holder.mBookingStatus.setTextColor(Color.parseColor(booking.getStatusColor()));
            GradientDrawable gd = (GradientDrawable) holder.mBookingStatus.getBackground().getCurrent();
            gd.setStroke(2, Color.parseColor(booking.getStatusColor()));
            holder.mBookingStatus.setBackground(gd);
        }
    }

    @Override
    public int getItemCount() {
        if (mBookingList == null) {
            return 0;
        } else {
            return mBookingList.size();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextViewCustom mTitleView;
        private final TextViewCustom mDescriptionView;
        private final TextViewCustom mAmountView;
        private final TextViewCustom mBookingStatus;
        private final TextViewCustom mRateService;
        private final TextViewCustom mPayOnline;
        private final View mView;
        private final ViewGroup mCTAView;
        //private final TextViewCustom mRateNow;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mTitleView = (TextViewCustom) itemView.findViewById(R.id.title);
            mDescriptionView = (TextViewCustom) itemView.findViewById(R.id.sub_desc);
            mAmountView = (TextViewCustom) itemView.findViewById(R.id.amount);
            mBookingStatus = (TextViewCustom) itemView.findViewById(R.id.booking_status);
            mRateService = (TextViewCustom) itemView.findViewById(R.id.rate_service);
            mPayOnline = (TextViewCustom) itemView.findViewById(R.id.pay_online);
            mCTAView = (ViewGroup) itemView.findViewById(R.id.completed_cta_group);
        }

        public void bind(Context context, Booking booking, int pos) {
            mTitleView.setText(booking.getServiceName());
            mDescriptionView.setText(booking.getBucketName() + " " + booking.getScheduledDate());
            mAmountView.setText(UiUtils.getFormattedAmountByString(context, booking.getOrderFinal()));
            if (!TextUtils.isEmpty(booking.getDisplayStatus())) {
                mBookingStatus.setVisibility(View.VISIBLE);
                mBookingStatus.setText(booking.getDisplayStatus());
            } else {
                mBookingStatus.setVisibility(View.INVISIBLE);
            }

            if (!booking.isOrderRated() && TextUtils.isEmpty(booking.getPaymentLink())) {
                mCTAView.setVisibility(View.GONE);
            } else {
                mCTAView.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(booking.getPaymentLink())) {
                    mPayOnline.setVisibility(View.VISIBLE);
                } else {
                    mPayOnline.setVisibility(View.GONE);
                }

                if (booking.isOrderRated()) {
                    mRateService.setVisibility(View.VISIBLE);
                } else {
                    mRateService.setVisibility(View.GONE);
                }
            }
        }
    }

    public void resetItemWidth(int size) {
        LogUtils.LOGD(TAG, "resetWidth : ");
        if (size > 1) {
            mItemWidth = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        } else {
            mItemWidth = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 1f);
        }
    }
}

