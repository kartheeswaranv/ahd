package com.athomediva.ui.home;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseFragment;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.Calendar;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public class SpecialDatesFragment extends BaseFragment {
    public static final String TAG = LogUtils.makeLogTag(SpecialDatesFragment.class.getSimpleName());

    public static final String KEY_SPECIAL_EVENTS = "key_special_events";
    public static final String KEY_TITLE_ENABLE = "key_title_enable";

    @BindView(R.id.title) TextView mTitleView;
    @BindView(R.id.event_date_list) ExpandListView mSpecialOfferDatesView;

    private DatePickerDialog mDatePickerDialog;

    private ArrayList<SpecialOfferDateModel> mSpecialDateList = new ArrayList<>();
    private boolean mIsTitleEnable;
    private SpecialDateAdapter mAdapter;

    private SpecialDatesChangeListener mListener;

    public interface SpecialDatesChangeListener {
        void onSpecialDateChange(SpecialOfferDateModel changeItem, ArrayList<SpecialOfferDateModel> list);
    }

    public static SpecialDatesFragment newInstance(ArrayList<SpecialOfferDateModel> specialDateList, boolean titleEnable) {
        SpecialDatesFragment fragment = new SpecialDatesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_SPECIAL_EVENTS, specialDateList);
        bundle.putBoolean(KEY_TITLE_ENABLE, titleEnable);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.ahd_speical_offer_fragment, container, false);
        bindView(this, rootView);
        getExtras();
        LogUtils.LOGD(TAG, "onCreateView : ");
        return rootView;
    }

    private void getExtras() {
        LogUtils.LOGD(TAG, "getExtras : ");
        if (getArguments() != null) {
            ArrayList<SpecialOfferDateModel> list = getArguments().getParcelableArrayList(KEY_SPECIAL_EVENTS);
            if (list != null && !list.isEmpty()) {
                mSpecialDateList.addAll(list);
            }
            mIsTitleEnable = getArguments().getBoolean(KEY_TITLE_ENABLE);
        }
    }

    public void setDateChangeListener(SpecialDatesChangeListener listener) {
        mListener = listener;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mSpecialOfferDatesView.setExpanded(true);
        mAdapter = new SpecialDateAdapter(getContext(), mSpecialDateList);
        mSpecialOfferDatesView.setAdapter(mAdapter);
        mAdapter.setItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "onClick :Item Click of Special Date ");
                Integer position = (Integer) v.getTag();
                if (position != null) {
                    SpecialOfferDateModel model = mSpecialDateList.get(position);
                    if (model != null) {
                        showDatePickerView(model);
                    }
                }
            }
        });

        if (mIsTitleEnable) {
            mTitleView.setVisibility(View.VISIBLE);
        } else {
            mTitleView.setVisibility(View.GONE);
        }
    }

    public void updateSpecialDateView(ArrayList<SpecialOfferDateModel> list) {
        mSpecialDateList.clear();
        mSpecialDateList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }

    public void showDatePickerView(SpecialOfferDateModel model) {
        LogUtils.LOGD(TAG, "showDatePicker : ");

        if (model == null) return;

        if (mDatePickerDialog != null && mDatePickerDialog.isShowing()) mDatePickerDialog.dismiss();

        // If Selected date is 0 then set the current date in date picker
        // other wise set the selected date
        Calendar calendar = Calendar.getInstance();
        if (model.getSelectedDate() > 0) {
            calendar.setTimeInMillis(model.getSelectedDate());
        }

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        LogUtils.LOGD(TAG, "showDatePickerView : " + day + "/" + month + "/" + year);
        mDatePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
            LogUtils.LOGD(TAG, "onDateSet : " + dayOfMonth + "/" + month1 + "/" + year1);
            // In Kitkat,Jellbean versions OnDateSet Called while Cancel the dialog also
            // So We need to check whether the view is shown or not
            //if it is then set otherwise leave it
            if (view.isShown()) {
                mDatePickerDialog.dismiss();
                calendar.set(year1, month1, dayOfMonth);
                model.setSelectedDate(calendar.getTimeInMillis());
                model.setSelectedDateTxt(UiUtils.getFormatDate(model.getSelectedDate()));
                mAdapter.notifyDataSetChanged();
                if (mListener != null) {
                    mListener.onSpecialDateChange(model, mSpecialDateList);
                }
            }
        }, year, month, day);

        mDatePickerDialog.show();
    }
}
