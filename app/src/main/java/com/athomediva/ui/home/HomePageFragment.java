package com.athomediva.ui.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import butterknife.BindView;
import com.athomediva.customview.ExpandListView;
import com.athomediva.customview.ToolbarAnimationController;
import com.athomediva.customview.UpdateDialog;
import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.Offers;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.data.models.remote.Testimonials;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.HomePageContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.location.CurrentLocationWidget;
import com.athomediva.ui.offers.OffersCarouselWidget;
import com.athomediva.ui.packages.PackageSectionWidget;
import com.athomediva.ui.testimonials.TestimonialSectionFragment;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import com.athomediva.ui.widgets.SectionPagerWidget;
import com.athomediva.ui.widgets.ToolbarCartWidget;
import com.athomediva.ui.widgets.ToolbarLocationWidget;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class HomePageFragment extends BaseFragment<HomePageContract.Presenter, HomePageContract.View>
  implements HomePageContract.View {
    public static final String TAG = LogUtils.makeLogTag(HomePageFragment.class.getSimpleName());
    @BindView(R.id.offers_carousel_widget) OffersCarouselWidget mOffersCarouselWidget;
    @BindView(R.id.current_location_widget) CurrentLocationWidget mCurrentLocationWidget;
    @BindView(R.id.toolbar_layout) ToolbarLocationWidget mToolbarLocation;
    @BindView(R.id.app_bar) AppBarLayout mAppBar;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.dynamic_container) LinearLayout mAHDServicesContainer;
    @BindView(R.id.recent_booking_widget) RecentBookingsWidget mRecentBookingWidget;
    @Inject HomePageContract.Presenter mPresenter;
    private PackageSectionWidget mPackagesSection;
    private ToolbarCartWidget mCartMenuWidget;
    private ToolbarAnimationController mToolbarAnimationControl;
    private UpdateDialog mUpdateDialog;
    private InfoDialog mInfoDialog;

    public static HomePageFragment newInstance(Bundle bundle) {
        HomePageFragment fragment = new HomePageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @DebugLog
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.home_page_frag, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.HomePageModule(getArguments(), this))
          .inject(this);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @DebugLog
    @Override
    protected void initializeView(View view) {
        LOGD(TAG, "initializeView : ");
        mOffersCarouselWidget.setOnCouponCodeClickListener(item -> {
            LOGD(TAG, "initializeSectionViews: on item click");
            if (item.getTag() != null && item.getTag() instanceof Offers) {
                mPresenter.onCodeCopyClick(((Offers) item.getTag()).getCouponCode());
            }
        });

        mCurrentLocationWidget.setOnClickListener(view1 -> mPresenter.onCurrentLocationClick());
        mToolbarLocation.setOnClickListener(v -> {
            if (mToolbarAnimationControl.isToolbarVisible()) {
                mPresenter.onCurrentLocationClick();
            }
        });
        getActivity().setTitle("");
        mToolbarAnimationControl = new ToolbarAnimationController(mToolbarLocation, mCurrentLocationWidget);
        mAppBar.addOnOffsetChangedListener(mToolbarAnimationControl);
        initToolbar();
        mToolbarLocation.setVisibility(View.INVISIBLE);

        mRecentBookingWidget.setOnItemClickListener(view14 -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onBookingClick((Booking) view14.getTag());
        });

        mRecentBookingWidget.setActionListener(view13 -> {
            LogUtils.LOGD(TAG, "onClick : Rate Service");
            mPresenter.onRateServiceClick((Booking) view13.getTag());
        }, view12 -> {
            LogUtils.LOGD(TAG, "onClick : Payonline ");
            mPresenter.onPayonlineClick((Booking) view12.getTag());
        });
    }

    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        if (mToolbar != null) {
            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
            }

            mToolbar.inflateMenu(R.menu.ahd_cart_menu);
            mCartMenuWidget = (ToolbarCartWidget) mToolbar.getMenu().findItem(R.id.action_cart).getActionView();
            mCartMenuWidget.setOnClickListener(view -> mPresenter.onViewCartClicked());
            mToolbar.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_cart:
                        LOGD(TAG, "onMenuItemClick : ");
                        mPresenter.onViewCartClicked();
                        return true;
                    default:
                        return false;
                }
            });
        }
    }

    @DebugLog
    @Override
    public void updateOffersView(List<Offers> offerList) {
        LOGD(TAG, "updateOffersView: ");
        mOffersCarouselWidget.updateView(offerList);
    }

    @DebugLog
    @Override
    public void updateCurrentLocationView(String locationName) {
        LOGD(TAG, "updateCurrentLocationView: ");
        mCurrentLocationWidget.updateView(locationName);
        mToolbarLocation.updateLocation(locationName);
    }

    @Override
    public void updateLocationViewVisibility(boolean show) {
        LOGD(TAG, "updateLocationViewVisibility: " + show);
        mToolbarLocation.updateVisibility(show);
        mCurrentLocationWidget.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void resetAllServicesContainer() {
        LOGD(TAG, "resetAllServicesContainer: ");
        mAHDServicesContainer.removeAllViews();
        mPackagesSection = null;
    }

    @DebugLog
    @Override
    public void addServicesSection(List<Categories> categoryList) {
        LOGD(TAG, "addServicesSection: ");
        SectionPagerWidget servicesWidget = (SectionPagerWidget) getActivity().getLayoutInflater()
          .inflate(R.layout.ahd_section_pager_widget, mAHDServicesContainer, false);
        HomePageTabAdapter tabAdapter = new HomePageTabAdapter(getActivity(), categoryList);
        tabAdapter.setSectionItemListener((group, child) -> {
            Categories category = categoryList.get(group);
            SubCategories subcat = (SubCategories) category.getSubcategories().get(child);
            LOGD(TAG, "onItemSelect : " + category + "  " + subcat);
            mPresenter.onCategoryItemClick(category, subcat);
        });
        servicesWidget.setAdapter(tabAdapter);
        mAHDServicesContainer.addView(servicesWidget);
    }

    @DebugLog
    @Override
    public void addPackagesSection(ArrayList<Services> packageItem) {
        LOGD(TAG, "addPackagesSection: ");
        mPackagesSection = (PackageSectionWidget) getActivity().getLayoutInflater()
          .inflate(R.layout.ahd_package_section_widget, mAHDServicesContainer, false);
        LinearLayout.LayoutParams params =
          new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, UiUtils.dpToPx(16), 0, 0);
        mPackagesSection.setLayoutParams(params);
        mPackagesSection.setUpdateListener(v -> mPresenter.onPlusClick((Services) v.getTag()),
          v -> mPresenter.onMinusClick((Services) v.getTag()), new ClickableSpan() {
              @Override
              public void onClick(View view) {
                  LOGD(TAG, "onClick : ");
                  Services serv = (Services) view.getTag();
                  if (serv != null && serv.getInfoList() != null) {
                      showInfoDialog(serv.getName(), serv.getInfoList());
                  }
              }
          }, v -> mPresenter.onViewAllClick());
        mPackagesSection.updatePackages(packageItem);
        mAHDServicesContainer.addView(mPackagesSection);
    }

    @DebugLog
    @Override
    public void updatePackagesView(Services item) {
        LOGD(TAG, "updatePackagesView: ");
        if (mPackagesSection != null) {
            mPackagesSection.updatePackageItem(item);
        } else {
            LOGE(TAG, "package view should not be null ****");
        }
    }

    @DebugLog
    @Override
    public void refreshPackages(List<Services> list) {
        LogUtils.LOGD(TAG, "refreshPackages : ");
        if (mPackagesSection != null && list != null) {
            mPackagesSection.refreshPackages(list);
        } else {
            LOGE(TAG, "package view should not be null ****");
        }
    }

    @DebugLog
    @Override
    public void addEmptyWidgetSection(EmptySectionItem item) {
        LOGD(TAG, "addEmptyWidgetSection: item name =" + item.getSectionTitle());
        GenericEmptyPageWidget emptyPageWidget = (GenericEmptyPageWidget) getActivity().getLayoutInflater()
          .inflate(R.layout.ahd_generic_empty_widget, mAHDServicesContainer, false);
        emptyPageWidget.setActionBtnClickListener(v -> mPresenter.onEmptyWidgetActionButtonClick(item));
        emptyPageWidget.updateData(item);
        emptyPageWidget.setBackgroundColor(ContextCompat.getColor(getActivity().getApplication(), R.color.white));
        mAHDServicesContainer.addView(emptyPageWidget);
    }

    @Override
    public void showForceUpdateView(boolean isMandatory, String message) {
        LOGD(TAG, "showForceUpdateView: ");
        if (mUpdateDialog != null && mUpdateDialog.isShowing()) {
            mUpdateDialog.dismiss();
        }
        mUpdateDialog = new UpdateDialog(getActivity());
        mUpdateDialog.showDialog(isMandatory, message);
    }

    @Override
    public void showCartEmptyWarningView(String title, String description, View.OnClickListener listener) {
        LOGD(TAG, "showCartEmptyWarningView: ");
        if (TextUtils.isEmpty(title)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(title, description);

        mInfoDialog = new InfoDialog(getContext(), model);

        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.proceed), view -> {
            mInfoDialog.dismiss();
            if (listener != null) {
                listener.onClick(view);
            }
        });
        mInfoDialog.setNegativeButton(getString(R.string.cancel_permission_btn), view1 -> mInfoDialog.dismiss());
    }

    @DebugLog
    @Override
    public void updateTestimonialsView(List<Testimonials> testimonialList, String status) {
        LOGD(TAG, "updateTestimonialsView: ");
        if (getChildFragmentManager().findFragmentByTag(TestimonialSectionFragment.TAG) == null) {
            getChildFragmentManager().beginTransaction()
              .replace(R.id.section_container_testimonials,
                TestimonialSectionFragment.newInstance((ArrayList<Testimonials>) testimonialList, status),
                TestimonialSectionFragment.TAG)
              .commitAllowingStateLoss();
        } else {
            LOGD(TAG, "updateTestimonialsView: fragment already present.");
        }
    }

    @DebugLog
    @Override
    public void updateCartMenu(int count) {
        LOGD(TAG, "updateCartMenu : ");
        mCartMenuWidget.updateCart(count);
    }

    @DebugLog
    public void showInfoDialog(String title, ArrayList<String> infoList) {
        LOGD(TAG, "showInfoDialog : ");
        if (TextUtils.isEmpty(title)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        View view = LayoutInflater.from(getContext()).inflate(R.layout.ahd_package_info_dialog, null);
        ExpandListView infoListView = (ExpandListView) view;
        infoListView.setAdapter(
          new InfoAdapter(getContext(), R.layout.ahd_dialog_info_item, infoList.toArray(new String[] {})));

        InfoDialogModel model = new InfoDialogModel(title, view);

        mInfoDialog = new InfoDialog(getContext(), model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view1 -> mInfoDialog.dismiss());
    }

    @Override
    public void updateRecentBookingsView(List<Booking> list) {
        LogUtils.LOGD(TAG, "updateRecentBookingsView : ");

        if (list != null && !list.isEmpty()) {
            mRecentBookingWidget.setVisibility(View.VISIBLE);
            mRecentBookingWidget.updateRecentBookings(list);
        } else {
            mRecentBookingWidget.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        if (mUpdateDialog != null && mUpdateDialog.isShowing()) {
            mUpdateDialog.dismiss();
        }

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        super.onDestroyView();
    }

    @Override
    public void launchSpecialDateOffer() {
        LogUtils.LOGD(TAG, "launchSpecialOfferDialog : ");
        if (getFragmentManager().findFragmentByTag(SpecialOfferDialogFragment.TAG) == null) {
            SpecialOfferDialogFragment fragment = new SpecialOfferDialogFragment();
            fragment.show(getFragmentManager(), SpecialOfferDialogFragment.TAG);
        } else {
            LogUtils.LOGD(TAG, "launchSpecialDateOffer : Already Dialog is open");
        }
    }
}
