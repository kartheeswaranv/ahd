package com.athomediva.ui.social;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandableTextView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.InviteFriendsContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.widgets.SocialNetworkingWidget;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class InviteFriendActivity extends BaseActivity implements InviteFriendsContract.View {
    public static final String TAG = LogUtils.makeLogTag(InviteFriendActivity.class.getSimpleName());
    @Inject InviteFriendsContract.Presenter mPresenter;
    @BindView(R.id.coupon_code) TextViewCustom mCouponCode;
    @BindView(R.id.coupon_copy_view) LinearLayout mCouponLayout;
    @BindView(R.id.ref_message) ExpandableTextView mRefMessage;
    @BindView(R.id.sharing_message_layout) SocialNetworkingWidget mSocialMessagingWidget;
    @BindView(R.id.social_sharing_layout) SocialNetworkingWidget mSocialSitesWidget;
    @BindView(R.id.sync_invite) TextView mSyncAndInviteBtn;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    private InfoDialog mInfoDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_invite_friends_activity);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.InviteFriendsModule(getIntent().getExtras())).inject(this);
        initViews();
        mPresenter.attachView(this);
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
        mRefMessage.setClickableSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                LogUtils.LOGD(TAG, "onClick : ");
                mPresenter.onReferalMoreClick();
            }
        });
        mCouponLayout.setOnClickListener(v -> mPresenter.onCouponCodeClicked());

        mSyncAndInviteBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Sync Invite ");
            mPresenter.onSyncContactClick();
        });
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.custom_title);
        title.setText(getString(R.string.ahd_invite_a_friend));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void updateSharingMessageView(String message) {
        LOGD(TAG, "updateSharingMessageView: message =" + message);
        mRefMessage.setText(message);
        mRefMessage.requestLayout();
    }

    @Override
    public void updateCouponCodeView(String couponCode) {
        LOGD(TAG, "updateCouponCodeView: coupon code " + couponCode);
        mCouponCode.setText(couponCode);
    }

    @Override
    public void updateMessageSharingViews(List<SocialSharingItem> itemList) {
        LOGD(TAG, "updateMessageSharingViews: ");
        mSocialMessagingWidget.updateWidget("Start Sharing Now", itemList,
          v -> mPresenter.onMessageSharingClick((SocialSharingItem) v.getTag()));
    }

    @Override
    public void updateSocialSharingViews(List<SocialSharingItem> itemList) {
        LOGD(TAG, "updateSocialSharingViews: ");
        mSocialSitesWidget.updateWidget("Let the World Know", itemList,
          v -> mPresenter.onSocialSharingClick((SocialSharingItem) v.getTag()));
    }

    @DebugLog
    @Override
    public void showReferalInfoDialog(String title, String desc) {
        LogUtils.LOGD(TAG, "showReferalInfoDialog : ");
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(desc)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) mInfoDialog.dismiss();

        InfoDialogModel model = new InfoDialogModel(title, desc);
        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view -> mInfoDialog.dismiss());
    }

    protected void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy : ");
        if (mInfoDialog != null && mInfoDialog.isShowing()) mInfoDialog.dismiss();
        super.onDestroy();
    }
}
