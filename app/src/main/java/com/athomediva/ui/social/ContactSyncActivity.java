package com.athomediva.ui.social;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.SeparatorDecoration;
import com.athomediva.data.models.local.Contact;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.ContactSyncContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 30/08/17.
 */

public class ContactSyncActivity extends BaseActivity implements ContactSyncContract.View {
    private static final String TAG = LogUtils.makeLogTag(ContactSyncActivity.class.getSimpleName());
    @BindView(R.id.contact_list) RecyclerView mContactListView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.multi_invite_cta) TextView mInviteBtn;
    @BindView(R.id.progress_layout) ViewGroup mProgressLayout;
    @BindView(R.id.progress) ProgressBar mProgressBar;
    @BindView(R.id.invite_msg) TextView mInviteMsgView;
    @BindView(R.id.invite_msg_layout) ViewGroup mInviteMsgLayout;

    private ContactAdapter mAdapter;
    @Inject ContactSyncContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_invite_contact_list);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.ContactSyncModule(getIntent().getExtras())).inject(this);
        initViews();
        mPresenter.attachView(this);
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initView : ");
        initToolbar();
        mAdapter = new ContactAdapter(this, new ContactAdapter.ContactClickCallback() {
            @Override
            public void onItemClick(Contact item) {
                LogUtils.LOGD(TAG, "onItemClick : ");
                mPresenter.onSingleRequestClick(item);
            }

            @Override
            public void onItemSelect(Contact item) {
                LogUtils.LOGD(TAG, "onItemSelect : ");
                mPresenter.onItemSelect(item);
            }
        });

        SeparatorDecoration decoration =
          new SeparatorDecoration(getApplicationContext(), ContextCompat.getColor(getApplicationContext(), R.color.divider),
            getResources().getDimension(R.dimen.divider_height), 0, 0);
        mContactListView.addItemDecoration(decoration);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mContactListView.setLayoutManager(mLayoutManager);
        mContactListView.setAdapter(mAdapter);
        mInviteBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Invite Btn ");
            mPresenter.onMultiRequestClick();
        });
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView textView = (TextView) mToolbar.findViewById(R.id.title);
        TextView subTitle = (TextView) mToolbar.findViewById(R.id.sub_title);
        subTitle.setVisibility(View.GONE);
        textView.setText(getString(R.string.sync_invite_cta));
        mToolbar.setNavigationOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Navigation back");
            hideKeyboard();
            finish();
        });

        super.setTitle("");
    }

    @Override
    public void updateContactCountView(int count) {
        LogUtils.LOGD(TAG, "updateContactCountView : ");
        if (mToolbar != null) {
            TextView textView = (TextView) mToolbar.findViewById(R.id.sub_title);
            textView.setVisibility(View.VISIBLE);
            textView.setText(getString(R.string.sync_invite_subtitle, String.valueOf(count)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ahd_search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        initSearchMenu(item);
        return super.onCreateOptionsMenu(menu);
    }

    private void initSearchMenu(MenuItem item) {
        LogUtils.LOGD(TAG, "initSearchMenu : ");
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint(getString(R.string.search_for_contacts));
        EditText edit = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        if (edit != null) {
            edit.setTextColor(ContextCompat.getColor(this, R.color.white));
            edit.setHintTextColor(ContextCompat.getColor(this, R.color.ahd_white_fifty_opacity));
            edit.setCursorVisible(true);
        }
        UiUtils.setTintToDrawable(this, item.getIcon(), ContextCompat.getColor(this, R.color.white));
        searchView.setOnCloseListener(() -> {
            LogUtils.LOGD(TAG, "onClose : Search");
            if (mAdapter != null) {
                mAdapter.getFilter().filter("");
            }
            item.collapseActionView();
            return false;
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                LogUtils.LOGD(TAG, "onQueryTextChange : " + newText);

                if (mAdapter != null) {
                    mAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }

    @Override
    public void updateData(List<Contact> list) {
        LogUtils.LOGD(TAG, "updateData : ");
        mAdapter.setContacts(list);
    }

    @Override
    public void refreshItems(ArrayList<Contact> list) {
        LogUtils.LOGD(TAG, "refreshItems : ");
        hideContactProgressView();
        if (list != null && !list.isEmpty()) {
            for (int index = 0; index < list.size(); index++)
                mAdapter.notifyItemChange(list.get(index));
            enableInviteView(false);
        }
    }

    @Override
    public void enableInviteView(boolean enable) {
        LogUtils.LOGD(TAG, "enableInviteView : " + enable);
        if (enable) {
            mInviteBtn.setVisibility(View.VISIBLE);
        } else {
            mInviteBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactProgressView() {
        LogUtils.LOGD(TAG, "showContactProgressView : ");
        if (mProgressLayout != null) {
            mProgressLayout.setVisibility(View.VISIBLE);
            mContactListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideContactProgressView() {
        LogUtils.LOGD(TAG, "hideContactProgressView : ");
        if (mProgressLayout != null && mContactListView != null) {
            mProgressLayout.setVisibility(View.GONE);
            mContactListView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateInviteMsgView(String msg) {
        LogUtils.LOGD(TAG, "updateInviteMsgView : " + msg);
        if (!TextUtils.isEmpty(msg)) {
            mInviteMsgLayout.setVisibility(View.VISIBLE);
            mInviteMsgView.setText(msg);
        } else {
            mInviteMsgLayout.setVisibility(View.GONE);
        }
    }

    protected void hideKeyboard() {
        LogUtils.LOGD(TAG, "hideKeyboard : ");
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
