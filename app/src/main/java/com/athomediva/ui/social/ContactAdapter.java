package com.athomediva.ui.social;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.athomediva.customview.TextDrawable;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.local.Contact;
import com.athomediva.helper.ColorGenerator;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 29/08/17.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> implements Filterable {
    private static final String TAG = ContactAdapter.class.getSimpleName();
    private Context mContext;
    private List<Contact> mContactList = new ArrayList<>();
    private List<Contact> mFilterList;
    private List<Contact> mOriginalList;
    private TextDrawable.IBuilder mDrawableBuilder;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private Filter mFilter;
    private ContactClickCallback mCallback;

    public interface ContactClickCallback {
        void onItemClick(Contact item);

        void onItemSelect(Contact item);
    }

    public ContactAdapter(Context context, ContactClickCallback ctaClickListener) {
        mContext = context;
        mCallback = ctaClickListener;
        mDrawableBuilder =
          TextDrawable.builder().beginConfig().fontSize(UiUtils.dpToPx(22)).withBorder(0).toUpperCase().endConfig().round();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView contactName;
        private TextView contactNo;
        private ImageView imageView;
        private ImageView checkIcon;
        private TextView mInviteCta;

        public ViewHolder(View view) {
            super(view);
            contactName = (TextView) view.findViewById(R.id.name);
            contactNo = (TextView) view.findViewById(R.id.phone);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            checkIcon = (ImageView) view.findViewById(R.id.check_icon);
            mInviteCta = (TextView) view.findViewById(R.id.invite_cta);
        }
    }

    public void setContacts(List<Contact> list) {
        mOriginalList = list;

        if (list != null) {
            mContactList.clear();
            mContactList.addAll(list);
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.ahd_invite_list_item, null, false);
        return new ViewHolder(view);
    }

    private Contact getItem(int pos) {
        if (mContactList != null) {
            return mContactList.get(pos);
        }

        return null;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) mFilter = new RecordFilter();
        return mFilter;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Contact item = getItem(position);
        if (holder != null) {
            holder.contactNo.setText(mContactList.get(position).getPhoneNo());
            holder.contactName.setText(mContactList.get(position).getName());

            updateCheckedState(holder, item);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // when the image is clicked, update the selected state
                    if (item.getStatus() == AppConstants.INVITE_STATUS.NOT_INVITE.getStatus()
                      || item.getStatus() == AppConstants.INVITE_STATUS.REINVITE.getStatus()) {
                        item.setChecked(!item.isChecked());
                        updateCheckedState(holder, item);
                        mCallback.onItemSelect(item);
                    }
                }
            });
            holder.mInviteCta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null) {
                        mCallback.onItemClick(item);
                    }
                }
            });
            updateCTA(holder, item);
        }
    }

    private void updateCheckedState(ViewHolder holder, Contact item) {
        if (item.isChecked()) {
            holder.imageView.setImageDrawable(
              mDrawableBuilder.build(" ", ContextCompat.getColor(mContext, R.color.ahd_black_light)));
            //holder.view.setBackgroundColor(HIGHLIGHT_COLOR);
            holder.imageView.invalidate();
            holder.checkIcon.setVisibility(View.VISIBLE);
        } else {
            TextDrawable drawable =
              mDrawableBuilder.build(String.valueOf(item.getName().charAt(0)), mColorGenerator.getColor(item.getName()));
            holder.imageView.setImageDrawable(drawable);
            holder.imageView.invalidate();
            //holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.checkIcon.setVisibility(View.GONE);
        }
    }

    private void updateCTA(ViewHolder holder, Contact item) {
        if (item.getStatus() == AppConstants.INVITE_STATUS.NOT_INVITE.getStatus()
          || item.getStatus() == AppConstants.INVITE_STATUS.REINVITE.getStatus()) {
            holder.mInviteCta.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ahd_button_theme_stroke));
            holder.mInviteCta.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            if (item.getStatus() == AppConstants.INVITE_STATUS.NOT_INVITE.getStatus()) {
                holder.mInviteCta.setText(AppConstants.INVITE_STATUS.NOT_INVITE.getTitle());
            } else {
                holder.mInviteCta.setText(AppConstants.INVITE_STATUS.REINVITE.getTitle());
            }
        } else if (item.getStatus() == AppConstants.INVITE_STATUS.INVITED.getStatus()) {
            holder.mInviteCta.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ahd_bg_blue_button_ripple));
            holder.mInviteCta.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.mInviteCta.setText(AppConstants.INVITE_STATUS.INVITED.getTitle());
        } else if (item.getStatus() == AppConstants.INVITE_STATUS.ALREADY_MEMBER.getStatus()) {
            holder.mInviteCta.setBackground(null);
            holder.mInviteCta.setTextColor(ContextCompat.getColor(mContext, R.color.ahd_grey_dark));
            holder.mInviteCta.setText(AppConstants.INVITE_STATUS.ALREADY_MEMBER.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if (mContactList == null) return 0;
        return mContactList.size();
    }

    private class RecordFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (mOriginalList == null || mOriginalList.isEmpty()) return results;
            //Implement filter logic
            // if edittext is null return the actual list
            if (constraint == null || constraint.length() == 0) {
                //No need for filter
                results.values = mOriginalList;
                results.count = mOriginalList.size();
            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                ArrayList<Contact> fRecords = new ArrayList<>();

                if (mOriginalList != null && !mOriginalList.isEmpty()) {
                    for (Contact s : mOriginalList) {
                        if (s.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            fRecords.add(s);
                        }
                    }
                    results.values = fRecords;
                    results.count = fRecords.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            //it set the data from filter to adapter list and refresh the recyclerview adapter
            mFilterList = (ArrayList<Contact>) results.values;
            mContactList.clear();
            if (mFilterList != null) {
                mContactList.addAll(mFilterList);
            }
            notifyDataSetChanged();
        }
    }

    public void notifyItemChange(Contact contact) {
        LogUtils.LOGD(TAG, "notifyItemChange : " + contact);
        int pos = mContactList.indexOf(contact);
        if (pos > -1) {
            notifyItemChanged(pos);
        }
    }
}
