package com.athomediva.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.athomediva.customview.ProgressDialog;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MVPView;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohit kumar on 13/01/17.
 * Base Fragment class to do all the common work of fragments
 */

public abstract class BaseFragment<P extends MVPPresenter, M extends MVPView> extends Fragment {
    private final String TAG = LogUtils.makeLogTag(BaseFragment.class.getSimpleName());
    private Unbinder mUnBinder;
    private ProgressDialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LOGD(TAG, "onCreateView: ");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LOGD(TAG, "onViewCreated: ");
        super.onViewCreated(view, savedInstanceState);
        initializeView(view);

        if (getPresenter() != null) {
            getPresenter().attachView((M) this);
        }
    }

    protected abstract MVPPresenter getPresenter();

    /**
     * Called by sub classes to bind the butter knife view.
     */
    protected void bindView(Object object, View view) {
        LOGD(TAG, "bindView: ");
        mUnBinder = ButterKnife.bind(object, view);
    }

    /**
     * Method to be implemented by the sub classes
     * to make a generic implementation of initializing view.
     * Will be called once when activity is created
     */
    protected abstract void initializeView(View rootView);

    protected void setTitle(String name) {
        LOGD(TAG, "setTitle: name =" + name);
        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setTitle(name);
            }
        }
    }

    /**
     * Base method to start the activity.
     */
    public void launchActivity(Bundle bundle, Class className) {
        LOGD(TAG, "launchActivity: ");
        Intent intent = new Intent(getActivity(), className);
        if (bundle != null) intent.putExtras(bundle);
        startActivity(intent);
    }

    public void launchActivity(Intent intent) {
        LOGD(TAG, "launchActivity: ");
        startActivity(intent);
    }

    public void launchActivityForResult(Bundle bundle, Class className, int resultCode) {
        LOGD(TAG, "launchActivityForResult: ");
        Intent intent = new Intent(getActivity(), className);
        intent.putExtras(bundle);
        startActivityForResult(intent, resultCode);
    }

    public void showProgressBar(String desc) {
        LOGD(TAG, "showProgressBar");
        showProgressBar(desc, false);
    }

    public void showProgressBar(String desc, boolean disableCancel) {
        LOGD(TAG, "showProgressBar");
        hideProgressBar();
        mDialog = new ProgressDialog(getActivity());
        mDialog.setCancelable(!disableCancel);
        mDialog.showDialog(desc);
    }

    public void hideProgressBar() {
        LOGD(TAG, "hideProgressBar");
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getPresenter() != null) {
            getPresenter().detachView();
        }
        LOGD(TAG, "onDestroyView: ");
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
    }

    public void finish() {
        LOGD(TAG, "finish: ");
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: ");
        super.onActivityResult(requestCode, resultCode, data);
        if (getPresenter() != null) {
            getPresenter().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult: ");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getPresenter() != null) {
            getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void finishViewWithResult(int resultCode, Bundle data) {
        LOGD(TAG, "setViewForResult: ");
        Intent resultIntent = new Intent();
        if (data != null) {
            resultIntent.putExtras(data);
        }
        getActivity().setResult(resultCode, resultIntent);
        finish();
    }

    public void finishViewWithResult(int resultCode) {
        LOGD(TAG, "setViewForResult: ");
        finishViewWithResult(resultCode, null);
    }

    public void launchActivityForResult(Intent intent, int reqCode) {
        LOGD(TAG, "launchActivityForResult: ");
        startActivityForResult(intent, reqCode);
    }
}
