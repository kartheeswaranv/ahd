package com.athomediva.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.athomediva.customview.ProgressDialog;
import com.athomediva.data.busevents.ForceUpdateEvent;
import com.athomediva.data.managers.EventBusManager;
import com.athomediva.injection.component.DaggerViewComponent;
import com.athomediva.injection.component.ViewComponent;
import com.athomediva.injection.module.ActivityModule;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import javax.inject.Inject;
import org.greenrobot.eventbus.Subscribe;
import com.athomediva.AHDApplication;

import static com.athomediva.logger.LogUtils.LOGD;

public abstract class BaseActivity extends AppCompatActivity {
    private final String TAG = LogUtils.makeLogTag(BaseActivity.class.getSimpleName());
    protected @Inject EventBusManager mEventBusManager;
    private ViewComponent mViewComponent;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewComponent().inject(this);
        mEventBusManager.register(this);
        LOGD(TAG, "onCreate: ");
    }

    protected abstract MVPPresenter getPresenter();

    public ViewComponent viewComponent() {
        if (mViewComponent == null) {
            mViewComponent = DaggerViewComponent.builder()
              .activityModule(new ActivityModule(this))
              .applicationComponent(AHDApplication.get(this).getComponent())
              .build();
        }
        return mViewComponent;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
          || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onDestroy() {
        LOGD(TAG, "onDestroy: ");
        mEventBusManager.unregister(this);
        mViewComponent = null;
        mEventBusManager = null;
        if (getPresenter() != null) {
            getPresenter().detachView();
        }
        super.onDestroy();
    }

    @Subscribe
    public void onEventMainThread(ForceUpdateEvent event) {
        LOGD(TAG, "onEvent ForceUpdateEvent");
    }

    /**
     * Base method to start the activity.
     */
    public void launchActivity(Bundle bundle, @NonNull Class className) {
        LOGD(TAG, "launchActivity: ");
        Intent intent = new Intent(this, className);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    public void launchActivity(Intent intent) {
        LOGD(TAG, "launchActivity: ");
        startActivity(intent);
    }

    public void launchActivityForResult(Bundle bundle, @NonNull Class className, int resultCode) {
        LOGD(TAG, "launchActivityForResult: ");
        Intent intent = new Intent(this, className);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, resultCode);
    }

    public void showProgressBar(String desc) {
        LOGD(TAG, "showProgressBar");
        showProgressBar(desc, false);
    }

    public void showProgressBar(String desc, boolean disableCancel) {
        LOGD(TAG, "showProgressBar");
        hideProgressBar();
        mDialog = new ProgressDialog(this);
        mDialog.setCancelable(!disableCancel);
        mDialog.showDialog(desc);
    }

    public void hideProgressBar() {
        LOGD(TAG, "hideProgressBar");
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LOGD(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void finish() {
        super.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (getPresenter() != null) {
            getPresenter().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LOGD(TAG, "onRequestPermissionsResult: ");
        if (getPresenter() != null) {
            getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void finishViewWithResult(int resultCode, @Nullable Bundle data) {
        LOGD(TAG, "setViewForResult: ");
        Intent resultIntent = new Intent();
        if (data != null) {
            resultIntent.putExtras(data);
        }
        setResult(resultCode, resultIntent);
        finish();
    }

    public void finishViewWithResult(int resultCode) {
        LOGD(TAG, "setViewForResult: ");
        finishViewWithResult(resultCode, null);
    }

    public void launchActivityForResult(Intent intent, int reqCode) {
        LOGD(TAG, "launchActivityForResult: ");
        startActivityForResult(intent, reqCode);
    }
}
