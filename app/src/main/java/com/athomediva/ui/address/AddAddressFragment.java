package com.athomediva.ui.address;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import butterknife.BindView;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.customview.EntityTextWatcher;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.AddAddressItem;
import com.athomediva.data.models.local.Entity;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AddAddressPageContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class AddAddressFragment extends BaseFragment<AddAddressPageContract.Presenter, AddAddressPageContract.View>
  implements AddAddressPageContract.View {
    public static final String TAG = LogUtils.makeLogTag(AddAddressFragment.class.getSimpleName());
    @Inject AddAddressPageContract.Presenter mPresenter;
    @BindView(R.id.building_name) CustomTextInputLayout mBuildingName;
    @BindView(R.id.landmark) CustomTextInputLayout mLandmark;
    @BindView(R.id.address_type) CustomTextInputLayout mAddressType;
    @BindView(R.id.full_address) CustomTextInputLayout mFullAddress;
    @BindView(R.id.custom_name) CustomTextInputLayout mCustomName;
    @BindView(R.id.action_btn) TextViewCustom mSaveBtn;
    private EntityTextWatcher mBuildingNameTW;
    private EntityTextWatcher mLandMarkTW;
    private EntityTextWatcher mCustomAddressTW;
    private PopupMenu mPopupMenu;

    public static AddAddressFragment newInstance(Bundle bundle) {
        AddAddressFragment fragment = new AddAddressFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.ahd_add_address_frag, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.AddAddressModule(getArguments())).inject(this);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.add_address));
        ((FragmentContainerActivity) getActivity()).setNavigationIcon(R.drawable.ahd_ic_close);
        ((FragmentContainerActivity) getActivity()).setNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtils.LOGD(TAG, "onClick : ");
                hideKeyboard();
                finish();
            }
        });
        mSaveBtn.setOnClickListener(v -> {
            hideKeyboard();
            mPresenter.onSaveClick();
        });
        mFullAddress.getEditText().setOnTouchListener((v, event) -> {
            LOGD(TAG, "mFullAddress click: ");
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mPresenter.onGetCurrentLocationClick();
            }
            return false;
        });
        mAddressType.getEditText().setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mPopupMenu.show();
            }
            return false;
        });

        mPopupMenu = new PopupMenu(getContext(), mAddressType, Gravity.LEFT | Gravity.BOTTOM);
        List<String> addressType = Arrays.asList(getContext().getResources().getStringArray(R.array.add_address_option));
        UIHelper.createAddressTypePopupMenu(mPopupMenu, addressType);
        mPopupMenu.setOnMenuItemClickListener(item -> {
            mPresenter.onAddressTypeClick(item.getTitle().toString());
            return false;
        });
    }

    @Override
    public void updateViews(AddAddressItem item) {
        LOGD(TAG, "updateViews: ");
        removeTextWatchers();
        updateBuildingView(item.getBuildingEntity());
        updateLandmarkView(item.getLandmarkEntity());
        updateAddressTypeView(item.getAddressTypeEntity());
        updateAddressView(item.getFullAddressEntity());
        updateCustomAddressView(item.getCustomAddressEntity());
        addTextWatchers(item);
    }

    @Override
    public void updateCustomViewVisibility(boolean show) {
        LOGD(TAG, "updateCustomViewVisibility: show=" + show);
        mCustomName.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void removeTextWatchers() {
        LOGD(TAG, "removeTextWatchers: ");
        mBuildingName.getEditText().removeTextChangedListener(mBuildingNameTW);
        mLandmark.getEditText().removeTextChangedListener(mLandMarkTW);
        mCustomName.getEditText().removeTextChangedListener(mCustomAddressTW);
    }

    private void addTextWatchers(AddAddressItem info) {
        LOGD(TAG, "addTextWatchers: ");
        mBuildingNameTW = new EntityTextWatcher(info.getBuildingEntity());
        mLandMarkTW = new EntityTextWatcher(info.getLandmarkEntity());
        mCustomAddressTW = new EntityTextWatcher(info.getCustomAddressEntity());
        mBuildingName.getEditText().addTextChangedListener(mBuildingNameTW);
        mLandmark.getEditText().addTextChangedListener(mLandMarkTW);
        mCustomName.getEditText().addTextChangedListener(mCustomAddressTW);
    }

    private void updateBuildingView(Entity<String> buildingEntity) {
        LOGD(TAG, "updateBuildingView: " + buildingEntity.isValid());
        if (buildingEntity.isValid()) {
            mBuildingName.getEditText().setText(buildingEntity.getData());
            mBuildingName.setErrorEnabled(false);
        } else {
            mBuildingName.setErrorEnabled(true);
            mBuildingName.setError(buildingEntity.getErrorMessage());
        }
    }

    private void updateLandmarkView(Entity<String> landmarkEntity) {
        LOGD(TAG, "updateLandmarkView: " + landmarkEntity.isValid());
        if (landmarkEntity.isValid()) {
            mLandmark.getEditText().setText(landmarkEntity.getData());
            mLandmark.setErrorEnabled(false);
        } else {
            mLandmark.setErrorEnabled(true);
            mLandmark.setError(landmarkEntity.getErrorMessage());
        }
    }

    private void updateAddressTypeView(Entity<String> addressTypeEntity) {
        LOGD(TAG, "updateAddressTypeView: " + addressTypeEntity.isValid());
        if (addressTypeEntity.isValid()) {
            mAddressType.getEditText().setText(addressTypeEntity.getData());
            mAddressType.setErrorEnabled(false);
        } else {
            mAddressType.setErrorEnabled(true);
            mAddressType.setError(addressTypeEntity.getErrorMessage());
        }
    }

    private void updateAddressView(Entity<Geolocation> addressEntity) {
        LOGD(TAG, "updateAddressView: " + addressEntity.isValid() + addressEntity.getData());
        if (addressEntity.isValid()) {
            mFullAddress.getEditText().setText(addressEntity.getData().getAddress());
            mFullAddress.setErrorEnabled(false);
        } else {
            mFullAddress.setErrorEnabled(true);
            mFullAddress.setError(addressEntity.getErrorMessage());
        }
    }

    private void updateCustomAddressView(Entity<String> addressEntity) {
        LOGD(TAG, "updateCustomAddressView: " + addressEntity.isValid() + addressEntity.getData());
        if (addressEntity.isValid()) {
            mCustomName.getEditText().setText(addressEntity.getData());
            mCustomName.setErrorEnabled(false);
        } else {
            mCustomName.setErrorEnabled(true);
            mCustomName.setError(addressEntity.getErrorMessage());
        }
    }

    @Override
    public void onDestroyView() {
        LOGD(TAG, "onDestroyView: ");
        removeTextWatchers();
        super.onDestroyView();
    }

    protected void hideKeyboard() {
        LogUtils.LOGD(TAG, "hideKeyboard : ");
        if (getActivity() == null) return;
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager =
              (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
