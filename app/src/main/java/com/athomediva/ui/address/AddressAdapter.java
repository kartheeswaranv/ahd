package com.athomediva.ui.address;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Address;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    private final String TAG = LogUtils.makeLogTag(AddressAdapter.class.getSimpleName());
    private final List<Address> mBookingList;
    private View.OnClickListener mClickListener;
    private View.OnClickListener mOptionMenuCLickListener;
    private boolean showOptionsMenu;

    public AddressAdapter(List<Address> bookingList,boolean showOptionsMenu) {
        mBookingList = bookingList;
        this.showOptionsMenu = showOptionsMenu;
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_address_item_widget, parent, false);
        LOGD(TAG, "onCreateViewHolder: ");
        return new AddressAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressAdapter.ViewHolder holder, int position) {
        LOGD(TAG, "onBindViewHolder: ");
        Address booking = mBookingList.get(position);
        holder.bind(booking);
    }

    @Override
    public int getItemCount() {
        if (mBookingList == null) {
            return 0;
        } else {
            return mBookingList.size();
        }
    }

    public void setItemClickListener(View.OnClickListener listener) {
        mClickListener = listener;
    }

    public void setOptionMenuClickListener(View.OnClickListener listener) {
        mOptionMenuCLickListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextViewCustom mTitleView;
        private final TextViewCustom mDescriptionView;
        private final ImageView mOptionsMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleView = (TextViewCustom) itemView.findViewById(R.id.address_title);
            mDescriptionView = (TextViewCustom) itemView.findViewById(R.id.full_address);
            mOptionsMenu = (ImageView) itemView.findViewById(R.id.options);
        }

        public void bind(Address address) {
            itemView.setTag(address);
            mOptionsMenu.setVisibility(showOptionsMenu?View.VISIBLE:View.GONE);
            mOptionsMenu.setTag(address);

            mOptionsMenu.setOnClickListener(v -> {
                if (mOptionMenuCLickListener != null) {
                    mOptionMenuCLickListener.onClick(mOptionsMenu);
                }
            });
            itemView.setOnClickListener(v -> {
                if (mClickListener != null) mClickListener.onClick(itemView);
            });
            mTitleView.setText(address.getLabel());
            mDescriptionView.setText(
              new StringBuilder().append(address.getApartmentNo()).append(", ").append(address.getAddress()));
        }
    }
}
