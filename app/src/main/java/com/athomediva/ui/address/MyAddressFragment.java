package com.athomediva.ui.address;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import com.athomediva.AHDApplication;
import com.athomediva.customview.SeparatorDecoration;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.remote.Address;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class MyAddressFragment extends BaseFragment<MyAddressPageContract.Presenter, MyAddressPageContract.View>
  implements MyAddressPageContract.View {
    public static final String TAG = LogUtils.makeLogTag(MyAddressFragment.class.getSimpleName());
    @Inject MyAddressPageContract.Presenter mPresenter;
    @BindView(R.id.list_view) RecyclerView mListView;
    @BindView(R.id.fab_add_address) FloatingActionButton mAddAddressFab;
    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptyLayout;

    public static MyAddressFragment newInstance(Bundle bundle) {
        MyAddressFragment fragment = new MyAddressFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.ahd_my_address_frag, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.MyAddressModule(getArguments())).inject(this);
        AHDApplication.get(getActivity())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.ADDRESS,
            GATrackerContext.Label.ADDRESS_PAGE_LOAD);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.my_addresses));
        mAddAddressFab.setOnClickListener(view -> mPresenter.onAddAddressClick());
        mEmptyLayout.updateData(AppConstants.getEmptyAddressItem(getContext()));
        mEmptyLayout.setActionBtnClickListener(v -> mPresenter.onAddAddressClick());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mListView.setLayoutManager(mLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.TRANSPARENT,
          getResources().getDimension(R.dimen.address_list_separator_height));
        mListView.addItemDecoration(decoration);
    }

    @Override
    public void updateAddressView(List<Address> itemList, boolean enableOption) {
        LOGD(TAG, "updateAddressView: ");
        AddressAdapter adapter = new AddressAdapter(itemList, enableOption);
        mListView.setAdapter(adapter);
        adapter.setItemClickListener(view -> {
            Address address = (Address) view.getTag();
            mPresenter.onOptionItemClick(address);
        });

        adapter.setOptionMenuClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), v, Gravity.END | Gravity.BOTTOM);
            List<String> addressType = Arrays.asList(getContext().getResources().getStringArray(R.array.my_address_option));
            UIHelper.createAddressTypePopupMenu(popupMenu, addressType);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                mPresenter.onDeleteAddressClick((Address) v.getTag());
                return false;
            });
        });
    }

    @Override
    public void updateEmptyPageVisibility(boolean show) {
        LOGD(TAG, "updateEmptyPageVisibility: ");
        mAddAddressFab.setVisibility(show ? View.GONE : View.VISIBLE);
        mEmptyLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
