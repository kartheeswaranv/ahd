package com.athomediva.ui.address;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class AddressItemWidget extends RelativeLayout {
    public AddressItemWidget(Context context) {
        super(context);
    }

    public AddressItemWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AddressItemWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AddressItemWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
