package com.athomediva.ui.webview;

import android.annotation.TargetApi;
import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import com.athomediva.AHDApplication;
import com.athomediva.data.AppConstants;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import java.util.HashMap;
import www.zapluk.com.BuildConfig;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class WebViewFragment extends BaseFragment {
    public static final String TAG = LogUtils.makeLogTag(WebViewFragment.class.getSimpleName());
    public static final String PARAM_TITLE = "param_title";
    public static final String PARAM_URL = "param_url";
    public static final String PARAM_INTERCEPT_URL = "param_intercept_url";
    public static final String PARAM_REDIRECTION_MSG = "param_direction_msg";
    public static final String PARAM_REDIRECTION_TIME = "param_direction_time";

    @BindView(R.id.web_view) WebView mWebView;

    private boolean isInterceptUrlMatch;
    private Handler mRedirectHandler;
    private Runnable mRedirectRunnable;
    private String mRedirectionMsg;
    private long mRedirectionTime;
    private String mWebUrl;
    private String mIncerceptUrl;

    public static WebViewFragment newInstance(Bundle bundle) {
        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ahd_web_view_frag, container, false);
        bindView(this, view);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        // get extras weburl ,intercept url ,redirection msg ,redirecting time
        getExtras();
        initializeHandler();
        HashMap<String, String> headers = new HashMap<>();

        headers.put(com.quikrservices.android.network.Constants.HTTP_HEADERS.X_QUIKR_CLIENT,
          AppConstants.CLIENT_IDENTITY_HEADER_VALUE);
        String sessionToken = AHDApplication.get(getActivity()).getComponent().preferencesManager().getSessionToken();
        if (!TextUtils.isEmpty(sessionToken)) {
            headers.put(com.quikrservices.android.network.Constants.HTTP_HEADERS.X_QUIKR_CLIENT_SID, sessionToken);
        }
        ((FragmentContainerActivity) getActivity()).setTitle(getArguments().getString(PARAM_TITLE));

        mWebView.setWebViewClient(new MyWebViewClient());
        enableWebViewSettings(mWebView);
        if (AndroidHelper.isNetworkAvailable(getContext())) {
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            mWebView.loadUrl(mWebUrl, headers);
        } else {
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            mWebView.loadUrl(mWebUrl, headers);
        }

        ((FragmentContainerActivity) getActivity()).setBackListener(() -> {
            LogUtils.LOGD(TAG, "initializeView : Back Press ");
            setResultDataAndFinish();
        });
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }

    @Override
    public void onDestroyView() {
        LogUtils.LOGD(TAG, "onDestroyView : ");
        if (mRedirectHandler != null && mRedirectRunnable != null) {
            mRedirectHandler.removeCallbacksAndMessages(null);
        }
        mWebView.destroy();
        super.onDestroyView();
    }

    private void enableWebViewSettings(WebView webView) {
        LogUtils.LOGD(TAG, "enableWebViewSettings : ");
        if (webView != null) {
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettings.setAllowContentAccess(false);
            webSettings.setAllowFileAccess(false);
            webSettings.setBuiltInZoomControls(false);
            webSettings.setDatabaseEnabled(false);
            webSettings.setDomStorageEnabled(true);
            webSettings.setGeolocationEnabled(true);
        }
    }

    private class MyWebViewClient extends WebViewClient {

        public MyWebViewClient() {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            handleInterceptUrl(url);
            return true;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            LogUtils.LOGD(TAG, "shouldInterceptRequest : ");
            handleInterceptUrl(request);
            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            LogUtils.LOGD(TAG, "shouldOverrideUrlLoading : ");
            handleInterceptUrl(request);
            return super.shouldOverrideUrlLoading(view, request);
        }

        private void handleInterceptUrl(String url) {
            LogUtils.LOGD(TAG, "handleInterceptUrl : Url ");
            if (url == null || TextUtils.isEmpty(mIncerceptUrl)) return;
            LogUtils.LOGD(TAG, "handleInterceptUrl : " + url);
            if (url.toString().startsWith(mIncerceptUrl)) {
                isInterceptUrlMatch = true;
                handleAutomaticRedirection(mRedirectionMsg, mRedirectionTime);
            }
        }

        @TargetApi((Build.VERSION_CODES.LOLLIPOP))
        private void handleInterceptUrl(WebResourceRequest request) {
            LogUtils.LOGD(TAG, "handleInterceptUrl : WebResources");
            if (request == null || TextUtils.isEmpty(mIncerceptUrl)) return;
            LogUtils.LOGD(TAG, "handleInterceptUrl : " + request.getUrl());
            if (request.getUrl() != null && request.getUrl().toString().startsWith(mIncerceptUrl)) {
                LogUtils.LOGD(TAG, "handleInterceptUrl : Matching url " + request.getUrl());
                isInterceptUrlMatch = true;
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            if (BuildConfig.DEBUG) {
                //super.onReceivedSslError(view, handler, error);
                handler.proceed();
            } else {
                super.onReceivedSslError(view, handler, error);
            }
        }
    }

    private void handleAutomaticRedirection(String msg, long timeInMilliSeconds) {
        LogUtils.LOGD(TAG, "handleAutomaticRedirection : " + timeInMilliSeconds);
        if (mRedirectHandler != null && mRedirectRunnable != null && timeInMilliSeconds > 0) {
            mRedirectHandler.removeCallbacks(mRedirectRunnable);
            mRedirectHandler.postDelayed(mRedirectRunnable, timeInMilliSeconds);
            if (!TextUtils.isEmpty(msg)) {
                ToastSingleton.getInstance().showToast(msg);
            } else {
                LogUtils.LOGD(TAG, "handleAutomaticRedirection : Msg is empty");
            }
        }
    }

    private void getExtras() {
        LogUtils.LOGD(TAG, "getExtras : ");
        if (getArguments() != null) {
            mWebUrl = getArguments().getString(PARAM_URL);
            mIncerceptUrl = getArguments().getString(PARAM_INTERCEPT_URL);
            mRedirectionMsg = getArguments().getString(PARAM_REDIRECTION_MSG);
            mRedirectionTime = getArguments().getLong(PARAM_REDIRECTION_TIME);
            LogUtils.LOGD(TAG, "getExtras : Web Url " + mWebUrl);
        }
    }

    private void initializeHandler() {
        mRedirectHandler = new Handler();
        mRedirectRunnable = () -> {
            LogUtils.LOGD(TAG, "Initialize handle run : Set the Result via Redirection");
            setResultDataAndFinish();
        };
    }

    private void setResultDataAndFinish() {
        LogUtils.LOGD(TAG, "setResultDataAndFinish : ");
        if (isInterceptUrlMatch) {
            finishViewWithResult(RESULT_OK);
        } else {
            finish();
        }
    }
}
