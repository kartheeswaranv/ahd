package com.athomediva.ui.feedback;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 10/06/17.
 */

public class RatingAdapter extends BaseAdapter {
    private static final String TAG = RatingAdapter.class.getSimpleName();

    private Context mContext;
    private List<RatingsModel> mRatingList;

    public RatingAdapter(Context context, List<RatingsModel> list) {
        LogUtils.LOGD(TAG, "RatingAdapter : ");
        mContext = context;
        mRatingList = list;
    }

    @Override
    public int getCount() {
        if (mRatingList == null) return 0;

        return mRatingList.size();
    }

    @Override
    public RatingsModel getItem(int i) {
        return mRatingList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_feedback_rating_view, null);
            holder.mRatingView = (RatingView) view;
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        RatingsModel model = mRatingList.get(pos);
        if (model != null) {
            holder.mRatingView.updateTitle(model.getTitle());
            holder.mRatingView.setRatingListener(ratingBar -> {
                LogUtils.LOGD(TAG, "onRatingChange : ");
                model.setRating(ratingBar.getRating());
            });
        }

        return view;
    }

    private class ViewHolder {
        RatingView mRatingView;
    }
}
