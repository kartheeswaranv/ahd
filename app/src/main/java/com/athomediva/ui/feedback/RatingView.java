package com.athomediva.ui.feedback;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class RatingView extends RelativeLayout {
    private static final String TAG = RatingView.class.getSimpleName();

    private TextView mRatingText;
    private ColorRatingBar mRatingBar;
    private TextView mDescView;

    private ColorRatingBar.ColorRatingChangeListener mListener;

    public RatingView(Context context) {
        super(context);
    }

    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    public void setRatingListener(ColorRatingBar.ColorRatingChangeListener listener) {
        LogUtils.LOGD(TAG,"setRatingListener : ");
        mListener = listener;
    }

    private void initView() {
        LogUtils.LOGD(TAG,"initView : ");
        mRatingText = (TextView)findViewById(R.id.rating_txt_view);
        mRatingBar = (ColorRatingBar)findViewById(R.id.rating_bar);
        mDescView = (TextView)findViewById(R.id.rating_desc_view);

        mRatingBar.setOnRatingChangeListener(ratingBar -> {
            mRatingText.setText(ratingBar.getRatingText());
            if(mListener != null) {
                mListener.onRatingChange(ratingBar);
            }
        });
    }

    public void updateTitle(String title) {
        LogUtils.LOGD(TAG,"updateTitle : ");
        if(!TextUtils.isEmpty(title))
            mDescView.setText(title);
    }
}
