package com.athomediva.ui.feedback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.customview.ExpandableGridView;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.FeedbackSuccessContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class FeedbackSuccessFragment extends BaseFragment<FeedbackSuccessContract.Presenter, FeedbackSuccessContract.View>
  implements FeedbackSuccessContract.View {
    private static final String TAG = LogUtils.makeLogTag(FeedbackSuccessFragment.class.getSimpleName());
    public static final String FRAG_TAG = FeedbackSuccessFragment.class.getCanonicalName();

    @Inject FeedbackSuccessContract.Presenter mPresenter;

    @BindView(R.id.referal_code) TextView mReferalCode;
    @BindView(R.id.copy) TextView mCopyToClipboard;
    @BindView(R.id.goto_home) TextView mGotoHomeBtn;
    @BindView(R.id.services_list) ExpandableGridView mExpandGridView;
    @BindView(R.id.facebook) ViewGroup mFaceBook;
    @BindView(R.id.twitter) ViewGroup mTwitter;
    @BindView(R.id.feedback_services_view) ViewGroup mSubcatView;
    @BindView(R.id.sharing_view) ViewGroup mSharingViewGroup;
    @BindView(R.id.rate_us_view) ViewGroup mRateViewGroup;
    @BindView(R.id.rate_us) TextView mRateUsPlaystore;

    public static FeedbackSuccessFragment newInstance(Bundle b) {
        FeedbackSuccessFragment fragment = new FeedbackSuccessFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.ahd_feedback_success, container, false);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.FeedbackSuccessModule(getArguments()))
          .inject(this);
        bindView(this, view);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mFaceBook.setOnClickListener(view -> mPresenter.onFacebookShareClick());
        mTwitter.setOnClickListener(view -> mPresenter.onTwitterShareClick());
        mCopyToClipboard.setOnClickListener(view -> mPresenter.onCopyToClipboardClick());
        mGotoHomeBtn.setOnClickListener(view -> mPresenter.onHomeClick());
        mExpandGridView.setOnItemClickListener(
          (adapterView, view, i, l) -> mPresenter.onSubcatItemClick((SubCategories) adapterView.getItemAtPosition(i)));

        mRateUsPlaystore.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onRateOnPlaystoreClick();
        });
    }

    @DebugLog
    @Override
    public void updateSubCatView(List<SubCategories> list) {
        LogUtils.LOGD(TAG, "updateSubCatView : " + list);
        mSubcatView.setVisibility(View.VISIBLE);
        mExpandGridView.setExpanded(true);
        mExpandGridView.setAdapter(new SubcatAdapter(getContext(), list));
    }

    @Override
    public void updateReferalView(String referralCode) {
        LogUtils.LOGD(TAG, "updateReferalView : ");
        if (!TextUtils.isEmpty(referralCode)) {
            mReferalCode.setText(referralCode);
        }
    }

    @Override
    public void updateSubTitle(String bucketName) {
        LogUtils.LOGD(TAG, "updateSubTitle : ");
        if (getActivity() != null) {
            ((FeedbackActivity) getActivity()).updateSubTitle(bucketName);
        }
    }

    @Override
    public void hideSubCatView() {
        LogUtils.LOGD(TAG, "hideSubCatView : ");
        if (mSubcatView != null) {
            mSubcatView.setVisibility(View.GONE);
        }
    }

    @Override
    public void enableRateUsView(boolean enable) {
        LogUtils.LOGD(TAG, "enableRateUsView : ");
        if (enable) {
            mRateViewGroup.setVisibility(View.VISIBLE);
            mSharingViewGroup.setVisibility(View.GONE);
        } else {
            mRateViewGroup.setVisibility(View.GONE);
            mSharingViewGroup.setVisibility(View.VISIBLE);
        }
    }
}
