package com.athomediva.ui.feedback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.data.models.remote.RatingsConfig;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.FeedbackPageContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class FeedbackFragment extends BaseFragment<FeedbackPageContract.Presenter, FeedbackPageContract.View>
  implements FeedbackPageContract.View {
    private static final String TAG = LogUtils.makeLogTag(FeedbackFragment.class.getSimpleName());
    public static final String FRAG_TAG = FeedbackFragment.class.getCanonicalName();

    @Inject FeedbackPageContract.Presenter mPresenter;

    @BindView(R.id.rating_list) ExpandListView mRatingListView;
    @BindView(R.id.submit) TextView mSubmitBtn;
    @BindView(R.id.comments) CustomTextInputLayout mComments;

    public static FeedbackFragment newInstance(Bundle b) {
        FeedbackFragment fragment = new FeedbackFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ahd_feedback_fragment, container, false);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.FeedbackModule(getArguments())).inject(this);
        bindView(this, view);
        LogUtils.LOGD(TAG, "onCreateView : ");
        return view;
    }

    @Override
    public void updateSubTitle(String bucketName) {
        LogUtils.LOGD(TAG, "updateSubTitle : ");
        if (getActivity() != null) {
            ((FeedbackActivity) getActivity()).updateSubTitle(bucketName);
        }
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mSubmitBtn.setOnClickListener(view -> {
            mPresenter.onSubmitClick(mComments.getEditText().getText().toString());
        });

        mRatingListView.setExpanded(true);
    }

    @Override
    public void updateRatingListView(List<RatingsModel> list) {
        LogUtils.LOGD(TAG, "updateRatingListView : ");
        if (list != null && list.size() > 0) {
            mRatingListView.setAdapter(new RatingAdapter(getContext(), list));
        }
    }

    @Override
    public void showValidationMessage(String message) {
        LogUtils.LOGD(TAG, "showValidationMessage : " + message);
        ToastSingleton.getInstance().showToast(message);
    }

    @Override
    public void enableBackPress(boolean enable) {
        LogUtils.LOGD(TAG, "enableBackPress : ");
        if (getActivity() != null) {
            ((FeedbackActivity) getActivity()).enableHomeBtn(enable);
        }
    }
}
