package com.athomediva.ui.feedback;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.utils.UiUtils;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class FeedbackActivity extends BaseActivity implements FeedbackActContract.View {
    private static final String TAG = LogUtils.makeLogTag(FeedbackActivity.class.getSimpleName());

    public static final String FRAG_TAG_NAME = "fragment_name";

    @BindView(R.id.fragment_container) FrameLayout mFragmentContainer;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    private TextView mTitle;
    private TextView mSubTitle;

    @Inject FeedbackActContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_feedback_activity);
        ButterKnife.bind(this);
        LOGD(TAG, "onCreate : ");
        viewComponent().plus(new ModuleManager.FeedbackActModule(AndroidHelper.getDeeplinkBundle(getIntent()))).inject(this);
        initToolbar();
        mPresenter.attachView(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (mPresenter != null) {
            mPresenter.onNewIntent(AndroidHelper.getDeeplinkBundle(intent));
        }
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        super.setTitle("");
        mTitle = (TextView) mToolbar.findViewById(R.id.title);

        mSubTitle = (TextView) mToolbar.findViewById(R.id.sub_title);
        mSubTitle.setVisibility(View.GONE);
        mToolbar.setNavigationOnClickListener(view -> {
            LOGD(TAG, "onClick : ");
            mPresenter.onBackPressed();
        });
    }

    @Override
    public void enableHomeBtn(boolean enable) {
        LogUtils.LOGD(TAG, "enableHomeBtn : ");
        mPresenter.onHomeBtnStatusChange(enable);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        }
    }

    @Override
    public void launchFragment(String tagName, Bundle b) {
        LOGD(TAG, "initializeFragment: tagName =" + tagName);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tagName);

        if (fragment == null) {
            LOGD(TAG, "onCreate: adding customer details fragment");
            String tag = "";
            if (tagName.equals(FeedbackSuccessFragment.FRAG_TAG)) {
                fragment = FeedbackSuccessFragment.newInstance(b);
                tag = FeedbackSuccessFragment.FRAG_TAG;
            } else if (tagName.equals(FeedbackFragment.FRAG_TAG)) {
                fragment = FeedbackFragment.newInstance(b);
                tag = FeedbackFragment.FRAG_TAG;
            }

            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, tag).commit();
            } else {
                LOGD(TAG, "onCreate:  no fragment found ");
            }
        }
    }

    @CallSuper
    public void updateTitle(String title) {
        LOGD(TAG, "setTitle: title Name =" + title);
        super.setTitle("");
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    public void updateSubTitle(String title) {
        LOGD(TAG, "setSubTitle : ");
        if (mSubTitle != null) {
            UIHelper.hideViewIfEmptyTxt(mSubTitle, title);
        }
    }

    public void setNavigationIcon(@DrawableRes int resource) {
        LOGD(TAG, "setNavigationIcon: ");

        Drawable d = UiUtils.getTintDrawable(getApplicationContext(), resource, Color.WHITE);
        if (d != null) {
            mToolbar.setNavigationIcon(d);
        }
        mToolbar.setNavigationOnClickListener(view -> {
            LOGD(TAG, "onClick : ");
            mPresenter.onBackPressed();
        });
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed();
    }
}
