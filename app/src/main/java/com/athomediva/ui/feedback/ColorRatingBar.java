package com.athomediva.ui.feedback;

/**
 * Created by kartheeswaran on 23/05/17.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import www.zapluk.com.R;

public class ColorRatingBar extends View {

    private float rating = 0f;
    private int numStars = 5;
    private int width = 0;
    private int height = 0;
    private int rectWidth = 0;
    private int rectHeight = 0;
    private int spacingForEachStar = 0;
    private int margin = 5;
    private int cornerRadius = 0;
    private float strokeWidth = 2f;
    private float stepSize = 1f;
    private String[] colorsList = null;
    private boolean isTouchDisable;

    private int mDefaultColor;

    ArrayList<RatingItem> mRectList = new ArrayList<>();
    ColorRatingChangeListener listener;

    public ColorRatingBar(Context context) {
        super(context);
    }

    public ColorRatingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorRatingBar);
        rectHeight= a.getDimensionPixelOffset(R.styleable.ColorRatingBar_box_height,50);
        rectWidth= a.getDimensionPixelOffset(R.styleable.ColorRatingBar_box_height, 50);
        spacingForEachStar = a.getDimensionPixelOffset(R.styleable.ColorRatingBar_box_space, 10);
        numStars = a.getInt(R.styleable.ColorRatingBar_numStars, 5);
        rating = a.getFloat(R.styleable.ColorRatingBar_rating, 0f);
        cornerRadius = a.getInt(R.styleable.ColorRatingBar_corner_radius, 5);
        strokeWidth = a.getFloat(R.styleable.ColorRatingBar_box_stroke, 2f);
        stepSize = a.getFloat(R.styleable.ColorRatingBar_step_size, 1f);
        colorsList = getResources().getStringArray(R.array.rating_colors);
        isTouchDisable =a.getBoolean(R.styleable.ColorRatingBar_disable_rating,false);
        a.recycle();
        invalidate();
        mDefaultColor = getResources().getColor(R.color.divider);
    }

    public ColorRatingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setNumStars(int stars) {
        numStars = stars;
    }

    public void setOnRatingChangeListener(ColorRatingChangeListener listener) {
        this.listener = listener;
    }

    private void initRects() {
        for(int index=0;index<numStars;index++) {
            int left = index*(rectWidth)+(index)*spacingForEachStar;
            int right = left+rectWidth;
            int top = margin;
            int bottom = top+rectHeight;

            Rect r= new Rect(left,top,right,bottom);
            RatingItem item = new RatingItem();
            item.rect = r;

            if(index< colorsList.length) {
                item.color = Color.parseColor(colorsList[index]);
            }

            mRectList.add(item);
        }

        //invalidate();
    }

    public void setRating(float rating) {
        this.rating = rating;
        invalidate();
    }

    public float getRating() {
        if(rating >=stepSize) {
            float rat = (rating);
            return rat;
        }
        return 0f;
    }

    public String getRatingText() {
        int value = (int)rating;
        return String.valueOf(value);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        if(width == 0) {
            width = getWidth();
            if(rectHeight == 0) {
                height = getHeight();
            } else {
                height = rectHeight;
            }

            rectHeight =height-2*margin;
            int totalSpace = width/6;
            // +1 for draw text with same box height
            spacingForEachStar = totalSpace/(numStars);
            // To Calculate the gap and Width of each box
            rectWidth = (5*(width/6))/(numStars);
            initRects();
        }
        Rect lastRect = null;
        for(RatingItem item:mRectList) {
            int color =item.color;
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeWidth(strokeWidth);
            paint.setColor(Color.parseColor("#d8d8d8"));
            RectF rf = new RectF(item.rect);

            canvas.drawRect(rf,paint);
            // fill rect based on rating
            Paint fillPaint = new Paint();
            fillPaint.setStyle(Paint.Style.FILL);
            fillPaint.setStrokeWidth(strokeWidth);
            fillPaint.setColor(color);
            float width = (float)item.rect.width();
            width = width*(item.rating/stepSize);
            Rect r = new Rect(item.rect.left,item.rect.top,item.rect.left+(int)width,item.rect.bottom);
            RectF rf1 =new RectF(r);
            canvas.drawRect(rf1,fillPaint);
        }


    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isTouchDisable) return false;



        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                calculateRatingonTouch((int) event.getX(), (int) event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                calculateRatingonTouch((int) event.getX(), (int) event.getY());
                break;
            case MotionEvent.ACTION_UP:
                break;
        }

        return true;
    }


    private void calculateRating(int x,int y) {
        for(int index=0;index<mRectList.size();index++) {
            if(mRectList.get(index).rect.contains(x,y)) {
                float r = (float)(x-mRectList.get(index).rect.left)/(float)mRectList.get(index).rect.width();
                if(r < .9 && index == 0) {
                    rating = 0f;
                } else {
                    rating = (float)((index)*stepSize);
                }
                break;
            }
        }

        if(x > mRectList.get(mRectList.size()-1).rect.right) {
            rating = (numStars)*stepSize;
        }
        updateList();
        invalidate();
    }

    /**
     * Calculate the rating on Touch
     * Get the index which maches with touch position
     */
    private void calculateRatingonTouch(int x,int y) {
        for(int index=0;index<mRectList.size();index++) {
            if(mRectList.get(index).rect.contains(x,y)) {
                rating = (index+1)*stepSize;
            }
        }

        if (x > mRectList.get(mRectList.size() - 1).rect.right) {
            rating = (numStars) * stepSize;
        }
        updateList();
        invalidate();
    }




    private void updateList() {
        for (int index = 0; index < mRectList.size(); index++) {
            if(((index+1)*stepSize) > rating) {
                mRectList.get(index).rating = rating-(float)((index+1)*stepSize);
                if(mRectList.get(index).rating < 0.0f) {
                    mRectList.get(index).rating = 0.0f;
                }
            } else {
                mRectList.get(index).rating = stepSize;
            }
        }

        if(listener != null) {
            listener.onRatingChange(this);
        }

    }

    private class RatingItem {
        Rect rect;
        int color;
        float rating = 0.0f;
    }

    public interface  ColorRatingChangeListener {
        public void onRatingChange(ColorRatingBar ratingBar);
    }
}

