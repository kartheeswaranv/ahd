package com.athomediva.ui.feedback;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import hugo.weaving.DebugLog;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class SubcatAdapter extends BaseAdapter {
    private static final String TAG = SubcatAdapter.class.getSimpleName();
    private final Context mContext;
    private List<SubCategories> mSubCatList;

    public SubcatAdapter(Context context, List<SubCategories> list) {
        LogUtils.LOGD(TAG, "SubcatAdapter : ");
        mContext = context;
        if (list != null) {
            mSubCatList = list;
        }
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mSubCatList == null) return 0;

        return mSubCatList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public SubCategories getItem(int position) {
        LogUtils.LOGD(TAG, "getItem : ");
        return mSubCatList.get(position);
    }

    @DebugLog
    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        LogUtils.LOGD(TAG, "getView : ");
        SubcatAdapter.Holder holder = new SubcatAdapter.Holder();
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.ahd_feedback_subcat_item, parent, false);
            holder.tv = (TextView) convertView.findViewById(R.id.name);
            holder.img = (NetworkImageView) convertView.findViewById(R.id.icon);

            holder.view = convertView;
            convertView.setTag(holder);

            final int size = (mContext.getResources().getDisplayMetrics().widthPixels / 2);
            convertView.setLayoutParams(new AbsListView.LayoutParams(size, AbsListView.LayoutParams.WRAP_CONTENT));
        } else {
            holder = (SubcatAdapter.Holder) convertView.getTag();
        }

        SubCategories model = mSubCatList.get(position);
        holder.img.setErrorImageResId(R.drawable.ahd_ic_cat_default);
        if (model != null) {
            holder.tv.setText(model.getName());
            if (!TextUtils.isEmpty(model.getIcon())) {
                holder.img.startLoading(model.getIcon());
            }
        }

        return convertView;
    }

    public class Holder {
        TextView tv;
        NetworkImageView img;
        View view;
    }
}
