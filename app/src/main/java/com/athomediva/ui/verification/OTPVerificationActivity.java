package com.athomediva.ui.verification;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.athomediva.ui.BaseActivity;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.data.AppConstants.AUTO_VERIFY_TIMER;
import static com.athomediva.logger.LogUtils.LOGD;

public class OTPVerificationActivity extends BaseActivity implements OTPVerificationContract.View {
    private final String TAG = LogUtils.makeLogTag(OTPVerificationActivity.class.getSimpleName());
    private final int MSG_RESET_TIMER = 0;
    private final int MSG_TIMER = 1;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.otp_layout) OTPLayout mOtpLayout;
    @BindView(R.id.phone_number_view) TextViewCustom mPhoneNumberView;
    @BindView(R.id.phone_edit_view) ImageView mEditPhoneNumberView;
    @BindView(R.id.auto_verify_view) LinearLayout mAutoVerifyLayout;
    @BindView(R.id.auto_verify_timer_view) TextViewCustom mAutoVerifyTimerText;
    private final Handler handler = new Handler(new Handler.Callback() {
        private int seconds = AUTO_VERIFY_TIMER;

        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == MSG_RESET_TIMER) {
                seconds = AUTO_VERIFY_TIMER;
            } else if (msg.what == MSG_TIMER) {
                if (seconds == 0) {
                    updateAutoVerificationView(false);
                    enableOTPMode(true);
                } else {
                    if (mAutoVerifyTimerText.getVisibility() == View.VISIBLE) {
                        LogUtils.LOGD(TAG, "handleMessage: seconds =" + seconds);
                        mAutoVerifyTimerText.setText(getString(R.string.auto_verifying_txt, seconds--));
                        handler.sendEmptyMessageDelayed(MSG_TIMER, 1000);
                    }
                }
            }
            return false;
        }
    });
    @BindView(R.id.mobile_number_verified_view) LinearLayout mMobileNumberVerifiedView;
    @BindView(R.id.resend_otp_view) TextViewCustom mResendOtpView;
    @Inject OTPVerificationContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_activity_otpverification);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.OTPVerificationModule(getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LOGD(TAG, "initializeView: ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(R.string.login_with_otp);
        mOtpLayout.setAutoFillListener(otp -> {
            LogUtils.LOGD(TAG, "initView: otp filled");
            mPresenter.otpEntered(otp);
        });
        mEditPhoneNumberView.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "mEditPhoneNumberView onclick ");
            mPresenter.editNumberClick();
        });

        mOtpLayout.setManualClickListener(v -> {
            LogUtils.LOGD(TAG, "manual view onclick: ");
            mPresenter.enterManualClick();
        });

        mResendOtpView.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "resend otp on click: ");
            mPresenter.resendOTPClick();
        });
    }

    @Override
    public void updateVerificationStatusView(boolean show) {
        LogUtils.LOGD(TAG, "updateVerificationStatusView: show =" + show);
        mMobileNumberVerifiedView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void fillOTPAutomatically(String otp) {
        LogUtils.LOGD(TAG, "fillOTPAutomatically: ");
        mOtpLayout.fillFromCodeAutomatically(otp);
    }

    @Override
    public void showOtpError(boolean isError, String otpErrorMessage) {
        LogUtils.LOGD(TAG, "showOtpError: ");
        if (isError) {
            mOtpLayout.showError(otpErrorMessage);
        } else {
            mOtpLayout.hideError();
        }
    }

    @Override
    public void updateAutoVerificationView(boolean start) {
        LogUtils.LOGD(TAG, "updateAutoVerificationView: start =" + start);
        mAutoVerifyLayout.setVisibility(start ? View.VISIBLE : View.INVISIBLE);
        if (start) {
            handler.sendEmptyMessage(MSG_RESET_TIMER);
            handler.removeCallbacksAndMessages(null);
            handler.sendEmptyMessage(MSG_TIMER);
        } else {
            handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void enableOTPMode(boolean show) {
        LogUtils.LOGD(TAG, "enableOTPMode: show =" + show);
        mOtpLayout.enableMode(show);
    }

    @Override
    public void updateMobileNumberView(String number) {
        LogUtils.LOGD(TAG, "updateMobileNumberView: number =" + number);
        mPhoneNumberView.setText(number);
    }

    @Override
    protected void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy: ");
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
