package com.athomediva.ui.verification;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.AppConstants;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 18/04/17.
 */

public class OTPLayout extends RelativeLayout implements TextView.OnEditorActionListener {
    private final String TAG = LogUtils.makeLogTag(OTPLayout.class.getSimpleName());
    private OTPFilledListener mOtpFilledListener;
    private TextViewCustom mManualView, mErrorView;
    private EditText[] editTexts;
    private OtpTextWatcher[] textWatchers;

    public OTPLayout(Context context) {
        super(context);
    }

    public OTPLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public OTPLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OTPLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initializeView();
    }

    public void enableMode(boolean modeManual) {
        LOGD(TAG, "enableMode: modeManual =" + modeManual);
        mManualView.setVisibility(modeManual ? View.GONE : View.VISIBLE);
        updateFocusableState(modeManual);
        if (modeManual) {
            editTexts[0].requestFocus();
            editTexts[0].requestFocusFromTouch();
            InputMethodManager inputMethodManager =
              (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(editTexts[0], InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void initializeView() {
        LOGD(TAG, "initializeView: ");
        editTexts = new EditText[AppConstants.OTP_LENGTH];
        textWatchers = new OtpTextWatcher[AppConstants.OTP_LENGTH];
        EditText pin = (EditText) findViewById(R.id.pin1);
        editTexts[0] = pin;
        pin = (EditText) findViewById(R.id.pin2);
        editTexts[1] = pin;
        pin = (EditText) findViewById(R.id.pin3);
        editTexts[2] = pin;
        pin = (EditText) findViewById(R.id.pin4);
        editTexts[3] = pin;
        pin = (EditText) findViewById(R.id.pin5);
        editTexts[4] = pin;
        pin = (EditText) findViewById(R.id.pin6);
        editTexts[5] = pin;

        Typeface ttf = UiUtils.getFontByName(getContext(), getContext().getString(R.string.ubuntu_medium_font));
        for (int i = 0; i < editTexts.length; i++) {
            textWatchers[i] = new OtpTextWatcher(editTexts[i], i == 0 ? null : editTexts[i - 1],
              i == editTexts.length - 1 ? null : editTexts[i + 1]);
            textWatchers[i].setIsLast(i == editTexts.length - 1);
            editTexts[i].addTextChangedListener(textWatchers[i]);
            editTexts[i].setOnEditorActionListener(this);
            editTexts[i].setTypeface(ttf);
        }
        updateFocusableState(false);
        mManualView = (TextViewCustom) findViewById(R.id.otp_manual_entry);
        mErrorView = (TextViewCustom) findViewById(R.id.otp_error);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        LOGD(TAG, "onEditorAction: ");
        triggerOtpVerification();
        return false;
    }

    public void setAutoFillListener(OTPFilledListener listener) {
        LOGD(TAG, "setAutoFillListener: ");
        mOtpFilledListener = listener;
    }

    public void setManualClickListener(View.OnClickListener listener) {
        LOGD(TAG, "setManualClickListener: ");
        mManualView.setOnClickListener(listener);
    }

    private void updateFocusableState(boolean state) {
        //state - true == enable click and touch
        //state - false == disable click and touch
        LOGD(TAG, "updateFocusableState: ");
        for (EditText editText : editTexts) {
            editText.setFocusable(state);
            editText.setFocusableInTouchMode(state);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        cleanupEditTexts();
        super.onDetachedFromWindow();
    }

    public void showError(String errorMessage) {
        LOGD(TAG, "showError: errorMessage =" + errorMessage);
        mErrorView.setVisibility(View.VISIBLE);
        mErrorView.setText(errorMessage);
        resetPins();
        editTexts[0].requestFocus();
        editTexts[0].requestFocusFromTouch();
    }

    public void hideError() {
        LOGD(TAG, "hideError: ");
        if (mErrorView != null) {
            mErrorView.setVisibility(View.INVISIBLE);
        }
    }

    public void fillFromCodeAutomatically(String otp) {
        LOGD(TAG, "fillFromCodeAutomatically: ");
        mManualView.setVisibility(View.GONE);
        cleanupEditTexts();
        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i].setText(String.valueOf(otp.charAt(i)));
        }
    }

    private void triggerOtpVerification() {
        LOGD(TAG, "triggerOtpVerification: ");
        StringBuilder otp = new StringBuilder();
        for (EditText editText : editTexts) {
            otp.append(editText.getText().toString());
        }
        if (otp.toString().length() == editTexts.length) {
            mManualView.setVisibility(View.GONE);
            mOtpFilledListener.onOtpFilled(otp.toString());
        }
    }

    private void cleanupEditTexts() {
        LOGD(TAG, "cleanupEditTexts: ");
        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i].removeTextChangedListener(textWatchers[i]);
            editTexts[i].setOnEditorActionListener(null);
            textWatchers[i].cleanup();
        }
    }

    private void resetPins() {
        LOGD(TAG, "resetPins: ");
        for (EditText editText : editTexts) {
            editText.setText("");
            editText.clearFocus();
        }
    }

    public interface OTPFilledListener {
        void onOtpFilled(String otp);
    }

    class OtpTextWatcher implements TextWatcher {
        final EditText mPrevious;
        EditText mCurrent;
        EditText mNext;
        boolean mIsLastDigit;

        public OtpTextWatcher(EditText current, EditText previous, EditText next) {
            mCurrent = current;
            mPrevious = previous;
            mNext = next;
        }

        public void setIsLast(boolean isLast) {
            mIsLastDigit = isLast;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().length() > 0) {
                if (mIsLastDigit) {
                    triggerOtpVerification();
                } else {
                    if (mCurrent.getText() != null && !mCurrent.getText().toString().isEmpty() && !mIsLastDigit) {
                        mCurrent.clearFocus();
                        mNext.requestFocus();
                    }
                }
            } else {
                if (mPrevious != null) {
                    mCurrent.clearFocus();
                    mPrevious.requestFocus();
                }
            }
        }

        public void cleanup() {
            mCurrent = null;
            mNext = null;
        }
    }
}
