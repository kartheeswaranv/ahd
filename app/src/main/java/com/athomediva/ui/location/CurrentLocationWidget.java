package com.athomediva.ui.location;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class CurrentLocationWidget extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(CurrentLocationWidget.class.getSimpleName());

    @BindView(R.id.location_title) TextViewCustom mLocationTitle;

    public CurrentLocationWidget(Context context) {
        super(context);
    }

    public CurrentLocationWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CurrentLocationWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CurrentLocationWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void updateView(String locationName) {
        LOGD(TAG, "updateView: locationName =" + locationName);
        mLocationTitle.setText(locationName);
    }
}
