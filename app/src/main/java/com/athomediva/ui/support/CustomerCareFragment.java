package com.athomediva.ui.support;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.CustomerServicesContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.widgets.SocialNetworkingWidget;
import com.athomediva.ui.widgets.TitleSubtitleIconWidget;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class CustomerCareFragment extends BaseFragment<CustomerServicesContract.Presenter, CustomerServicesContract.View>
  implements CustomerServicesContract.View {
    public static final String TAG = LogUtils.makeLogTag(CustomerCareFragment.class.getSimpleName());
    @BindView(R.id.call_layout) TitleSubtitleIconWidget mCallUsWidget;
    @BindView(R.id.email_layout) TitleSubtitleIconWidget mEmailWidget;
    @BindView(R.id.social_networking_widget) SocialNetworkingWidget mSocialNetworkingWidget;
    @Inject CustomerServicesContract.Presenter mPresenter;

    public static CustomerCareFragment newInstance(Bundle bundle) {
        CustomerCareFragment fragment = new CustomerCareFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ahd_customer_care_frag, container, false);
        LOGD(TAG, "onCreateView: ");
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.CustomerServicesModule(getArguments()))
          .inject(this);
        bindView(this, view);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.ahd_customer_care));
        mCallUsWidget.setOnClickListener(view -> mPresenter.onPhoneServicesClick());
        mEmailWidget.setOnClickListener(view -> mPresenter.onEmailServicesClick());
    }

    @Override
    public void updateCallUsView(TitleDescImageItem callItem) {
        LOGD(TAG, "updateCallUsView: ");
        mCallUsWidget.updateData(callItem);
    }

    @Override
    public void updateEmailView(TitleDescImageItem emailItem) {
        LOGD(TAG, "updateEmailView: ");
        mEmailWidget.updateData(emailItem);
    }

    @Override
    public void updateSocialNetworkingServices(String title, List<SocialSharingItem> itemList) {
        LOGD(TAG, "updateSocialNetworkingServices: ");
        mSocialNetworkingWidget.updateWidget(title, itemList,
          v -> mPresenter.onSocialServiceClick((SocialSharingItem) v.getTag()));
    }
}
