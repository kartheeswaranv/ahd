package com.athomediva.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.home.HomePageFragment;
import com.athomediva.ui.myaccount.MyAccountFragment;
import com.athomediva.ui.mybookings.MyBookingFragment;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

public class AHDMainActivity extends BaseActivity implements AHDMainViewContract.View {
    private final String TAG = LogUtils.makeLogTag(AHDMainActivity.class.getSimpleName());
    private final int HOME_NAV_ITEM = 0;
    private final int BOOKING_NAV_ITEM = 1;
    private final int ACCOUNT_NAV_ITEM = 2;
    @Inject AHDMainViewContract.Presenter mPresenter;
    @BindView(R.id.bottom_navigation) BottomNavigationView mBottomNavigationView;

    private InfoDialog mInfoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.activity_ahdmain);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.AHDMainActivityModule(getIntent(), savedInstanceState)).inject(this);
        initializeView();
        mPresenter.attachView(this);
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.HOME_PAGE,
            GATrackerContext.Label.HOME_PAGE_LOAD);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        LOGD(TAG, "onNewIntent: intent = " + intent);
        super.onNewIntent(intent);
        mPresenter.onNewIntent(AndroidHelper.getDeeplinkBundle(intent));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initializeView() {
        LOGD(TAG, "initializeView savedInstanceState =");
        mBottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            LOGD(TAG, "initializeView : testimonial_item title = " + item.getTitle());
            mPresenter.onNavigationItemClicked(item.getTitle().toString());
            return false;
        });

        if (getActionBar() != null) {
            getActionBar().hide();
        }
    }

    @Override
    public void showHomePage(boolean reload) {
        LOGD(TAG, "showHomePage: reload = " + reload);
        if (getSupportFragmentManager().findFragmentByTag(HomePageFragment.TAG) == null || reload) {
            //UIHelper.setTranslucentStatus(this);
            toggleNavView(HOME_NAV_ITEM);
            if (getSupportFragmentManager().findFragmentByTag(HomePageFragment.TAG) != null) {
                AndroidHelper.replaceFragment(getSupportFragmentManager(), R.id.container,
                  HomePageFragment.newInstance(null), HomePageFragment.TAG);
            } else {
                AndroidHelper.replaceFragment(getSupportFragmentManager(), R.id.container,
                  HomePageFragment.newInstance(null), HomePageFragment.TAG);
            }
            mBottomNavigationView.getMenu().findItem(R.id.action_home).setChecked(true);
        } else {
            LOGD(TAG, "showHomePage: fragment already loaded");
        }
    }

    @Override
    public void showBookingsPage() {
        LOGD(TAG, "showBookingsPage: ");
        if (getSupportFragmentManager().findFragmentByTag(MyBookingFragment.TAG) == null) {
            toggleNavView(BOOKING_NAV_ITEM);
            AndroidHelper.replaceFragment(getSupportFragmentManager(), R.id.container, MyBookingFragment.newInstance(null),
              MyBookingFragment.TAG);
            mBottomNavigationView.getMenu().findItem(R.id.action_booking).setChecked(true);
        } else {
            LOGD(TAG, "showBookingsPage: fragment already loaded");
        }
    }

    @Override
    public void showMyAccountsPage() {
        LOGD(TAG, "showMyAccountsPage: ");
        if (getSupportFragmentManager().findFragmentByTag(MyAccountFragment.TAG) == null) {
            toggleNavView(ACCOUNT_NAV_ITEM);
            AndroidHelper.replaceFragment(getSupportFragmentManager(), R.id.container, MyAccountFragment.newInstance(null),
              MyAccountFragment.TAG);
            mBottomNavigationView.getMenu().findItem(R.id.action_my_account).setChecked(true);
        } else {
            LOGD(TAG, "showMyAccountsPage: fragment already loaded");
        }
    }

    private void toggleNavView(int selectedItem) {
        mBottomNavigationView.getMenu().getItem(HOME_NAV_ITEM).setChecked(selectedItem == HOME_NAV_ITEM);
        mBottomNavigationView.getMenu().getItem(BOOKING_NAV_ITEM).setChecked(selectedItem == BOOKING_NAV_ITEM);
        mBottomNavigationView.getMenu().getItem(ACCOUNT_NAV_ITEM).setChecked(selectedItem == ACCOUNT_NAV_ITEM);
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed();
        //super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPresenter != null) {
            mPresenter.onSaveInstance(outState);
        }
    }

    public void showAvailabilityConfirmDialog() {
        ToastSingleton.getInstance().showToast(getString(R.string.availability_success_msg));
        /*if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(null, getString(R.string.availability_success_msg));

        mInfoDialog = new InfoDialog(this, model);

        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view -> {
            mInfoDialog.dismiss();
        });*/
    }
}
