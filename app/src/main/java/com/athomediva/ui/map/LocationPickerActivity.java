package com.athomediva.ui.map;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.EditTextCustom;
import com.athomediva.customview.SeparatorDecoration;
import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.Predictions;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.address.AddressAdapter;
import com.athomediva.ui.widgets.AutoSuggestPlacesAdapter;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class LocationPickerActivity extends BaseActivity implements LocationPickerContract.View {
    private static final String TAG = LogUtils.makeLogTag(LocationPickerActivity.class.getSimpleName());
    @Inject LocationPickerContract.Presenter mPresenter;
    @BindView(R.id.detect_my_location) RelativeLayout mDetectLocation;
    @BindView(R.id.saved_address_layout) LinearLayout mSavedAddressLayout;
    @BindView(R.id.saved_address_list_view) RecyclerView mSavedAddressListView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.autocomplete_text_view) EditTextCustom mAutoCompleteView;
    @BindView(R.id.suggested_results_layout) RelativeLayout mSuggestedLocationLayout;
    @BindView(R.id.suggested_list_view) RecyclerView mSuggestedLocationListView;
    @BindView(R.id.close_suggestion) ImageView mCloseButton;
    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptySearchResultLayout;
    private AutoSuggestPlacesAdapter mSuggestedLocationAdapter;
    private TextWatcher mSearchTextWatcher;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_location_picker_activity);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.LocationPickerModule(getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
    }

    @DebugLog
    private void initView() {
        LOGD(TAG, "initializeView: ");
        initToolbar();
        mDetectLocation.setOnClickListener(v -> mPresenter.onGetCurrentLocationClick());
        mSearchTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                LOGD(TAG, "afterTextChanged: " + s.toString());
                mSuggestedLocationLayout.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                mCloseButton.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                mPresenter.onSearchQuery(s.toString());
            }
        };

        mCloseButton.setOnClickListener(v -> mAutoCompleteView.setText(""));

        mAutoCompleteView.addTextChangedListener(mSearchTextWatcher);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mSuggestedLocationListView.setLayoutManager(layoutManager);
        mSuggestedLocationListView.setItemAnimator(new DefaultItemAnimator());
        SeparatorDecoration decoration = new SeparatorDecoration(getApplicationContext(),
          ContextCompat.getColor(getApplicationContext(), R.color.ahd_place_divider_color),
          getResources().getDimension(R.dimen.divider_height),
          getResources().getDimension(R.dimen.location_picker_left_margin),
          getResources().getDimension(R.dimen.location_picker_right_margin));
        mSuggestedLocationListView.addItemDecoration(decoration);
        mSuggestedLocationAdapter = new AutoSuggestPlacesAdapter(getApplicationContext(), new ArrayList<>());
        mSuggestedLocationAdapter.setItemClickListener(v -> mPresenter.onSearchItemSelected((Predictions) v.getTag()));
        mSuggestedLocationListView.setAdapter(mSuggestedLocationAdapter);
    }

    /**
     * Initialize Toolbar Views
     */
    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @DebugLog
    @Override
    public void updateAddressView(List<Address> itemList) {
        LOGD(TAG, "updateAddressView: ");
        AddressAdapter adapter = new AddressAdapter(itemList,false);
        adapter.setItemClickListener(v -> mPresenter.onUserAddressClick((Address) v.getTag()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mSavedAddressListView.setLayoutManager(mLayoutManager);
        mSavedAddressListView.setItemAnimator(new DefaultItemAnimator());
        mSavedAddressListView.setAdapter(adapter);
    }

    @Override
    public void updateSavedAddressVisibility(boolean show) {
        LOGD(TAG, "updateSavedAddressVisibility: show" + show);
        mSavedAddressLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateCurrentLocationVisibility(boolean show) {
        LOGD(TAG, "updateCurrentLocationVisibility: show =" + show);
        mDetectLocation.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateAutoCompleteTextViewResult(List<Predictions> addresses) {
        LOGD(TAG,
          "updateAutoCompleteTextViewResult: addresses size =" + String.valueOf(addresses == null ? 0 : addresses.size()));
        mSuggestedLocationAdapter.setDataAndNotifyDataSetChanged(addresses);
    }

    @Override
    public void updateSuggestedAddressVisibility(boolean show) {
        LOGD(TAG, "updateSuggestedAddressVisibility: show =" + show);
        mSuggestedLocationLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateEmptyPageVisibility(EmptySectionItem item, boolean show) {
        LOGD(TAG, "updateEmptyPageVisibility: show =" + show);
        mEmptySearchResultLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show) {
            mEmptySearchResultLayout.updateData(item);
        }
    }

    @Override
    public void openKeyBoard() {
        LOGD(TAG, "openKeyBoard: ");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    protected void onDestroy() {
        LOGD(TAG, "onDestroy: ");
        mAutoCompleteView.removeTextChangedListener(mSearchTextWatcher);
        super.onDestroy();
    }
}
