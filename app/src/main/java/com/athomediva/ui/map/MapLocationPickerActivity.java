package com.athomediva.ui.map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MapLocationPickerContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.utils.UiUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

public class MapLocationPickerActivity extends BaseActivity implements OnMapReadyCallback, MapLocationPickerContract.View {
    private static final String TAG = LogUtils.makeLogTag(MapLocationPickerActivity.class.getSimpleName());
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @Inject MapLocationPickerContract.Presenter mPresenter;
    @BindView(R.id.search_bar) TextViewCustom mSearchBar;
    @BindView(R.id.search_layout) CardView mSearchLayout;
    @BindView(R.id.current_location_fab) FloatingActionButton mCurrentLocationFab;
    @BindView(R.id.use_this_location) TextViewCustom mUseThisLocation;
    @BindView(R.id.location_not_supported_error) LinearLayout mErrorBoundsLocation;
    private GoogleMap mMap;
    private final GoogleMap.OnCameraIdleListener mCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
        @Override
        public void onCameraIdle() {
            LOGD(TAG, "onCameraIdle: ");
            if (mMap != null && mPresenter != null) {
                LOGD(TAG, "onCameraIdle: lat lng =" + mMap.getCameraPosition().target);
                mPresenter.onCameraFocusedForLocation(mMap.getCameraPosition().target);
            }
        }
    };

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_activity_map_location_picker);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.MapLocationPickerModule(getIntent().getExtras())).inject(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initView();
        mPresenter.attachView(this);
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.ADDRESS_MAP,
            GATrackerContext.Label.ADD_ADDRESS_PAGE_LOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @DebugLog
    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(R.string.select_address);
        mSearchLayout.setOnClickListener(v -> mPresenter.onSearchClicked());
        mCurrentLocationFab.setOnClickListener(v -> mPresenter.onGetCurrentLocationClick());
        mUseThisLocation.setOnClickListener(v -> mPresenter.onUseThisLocationClick((Geolocation) mUseThisLocation.getTag()));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LOGD(TAG, "onMapReady: ");
        mMap = googleMap;
        mPresenter.onMapReady();
    }

    @Override
    public void updateSearchView(Geolocation geolocation) {
        LOGD(TAG, "updateSearchView: ");
        mSearchBar.setText(geolocation.getAddress());
        mUseThisLocation.setTag(geolocation);
    }

    @Override
    public void updateErrorView(boolean show) {
        LOGD(TAG, "updateErrorView: show =" + show);
        int visibility = mErrorBoundsLocation.getVisibility();
        if (visibility == View.GONE && show) {
            mErrorBoundsLocation.setVisibility(View.VISIBLE);
            mUseThisLocation.setVisibility(View.GONE);
            mErrorBoundsLocation.animate().translationY(0);
        } else if (visibility == View.VISIBLE && !show) {
            mErrorBoundsLocation.animate().translationY(mErrorBoundsLocation.getHeight()).withEndAction(() -> {
                LOGD(TAG, "onAnimationEnd: ");
                mErrorBoundsLocation.setVisibility(View.GONE);
                mUseThisLocation.setVisibility(View.VISIBLE);
            });
        }
    }

    @DebugLog
    @Override
    public void moveCameraToPosition(Geolocation geolocation) {
        LOGD(TAG, "moveCameraToPosition: geolocation =" + geolocation);
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(AppConstants.CAMERA_DEFAULT_ZOOM);
        if (geolocation != null && geolocation.getLocation() != null) {
            LatLng coordinate = geolocation.getLocation();
            builder.target(coordinate);
        }
        resetCameraListenerForMovingCameraProgrammatically();
        if (mMap != null) mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
    }

    @DebugLog
    @Override
    public void initializeBounds(LatLngBounds bounds) {
        LOGD(TAG, "initializeBounds: ");
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = UiUtils.dpToPx(309);
        resetCameraListenerForMovingCameraProgrammatically();
        if (mMap == null) return;
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, 0));
    }

    private void resetCameraListenerForMovingCameraProgrammatically() {
        if (mMap == null) return;
        mMap.setOnCameraIdleListener(null);
        mMap.setOnMapClickListener(null);
        mMap.setOnCameraIdleListener(() -> {
            mMap.setOnCameraIdleListener(mCameraIdleListener);
            mMap.setOnMapClickListener(latLng -> {
                LOGD(TAG, "onMapClick: lat lng =" + latLng);
                // Clears the previously touched position
                if (latLng != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            });
        });
    }

    @DebugLog
    @Override
    public void updateCurrentLocation(boolean currentLocationEnabled) {
        boolean enableLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
          != PackageManager.PERMISSION_GRANTED
          && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
          != PackageManager.PERMISSION_GRANTED;
        if (mMap != null) mMap.setMyLocationEnabled(!enableLocation && currentLocationEnabled);
    }

    @DebugLog
    @Override
    public void drawPolygonBounds(List<LatLng> bounds) {
        LOGD(TAG, "drawPolygonBounds: ");
        if (mMap == null) return;
        if (bounds != null && bounds.size() > 0) {
            mMap.addPolygon(new PolygonOptions().addAll(bounds)
              .strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
              .strokeWidth(getResources().getDimension(R.dimen.location_picker_stroke_width)));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : bounds) builder.include(latLng);
            resetCameraListenerForMovingCameraProgrammatically();
            if (mMap != null) {
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = UiUtils.dpToPx(309);
                resetCameraListenerForMovingCameraProgrammatically();
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, 0));
            }
        }
    }
}
