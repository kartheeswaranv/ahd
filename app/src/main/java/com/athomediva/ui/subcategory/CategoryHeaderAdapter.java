package com.athomediva.ui.subcategory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import hugo.weaving.DebugLog;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 18/04/17.
 */

public class CategoryHeaderAdapter extends RecyclerView.Adapter<CategoryHeaderAdapter.Holder> {
    private static final String TAG = CategoryHeaderAdapter.class.getSimpleName();

    private final Context mContext;
    private List<Categories> mCategoryList;
    private CategoryHeaderWidget.OnCategorySelectListener mListener;
    private int mItemwidth;

    @DebugLog
    public CategoryHeaderAdapter(Context context, List<Categories> list) {
        LogUtils.LOGD(TAG, "CategoryHeaderAdapter : ");
        mContext = context;
        mCategoryList = list;
        mItemwidth = mContext.getResources().getDisplayMetrics().widthPixels / 3;
    }

    @DebugLog
    public void updateAdapter(List<Categories> list) {
        LogUtils.LOGD(TAG, "updateAdapter : ");
        mCategoryList = list;
        notifyDataSetChanged();
    }

    @DebugLog
    public void setOnCategoryListener(CategoryHeaderWidget.OnCategorySelectListener listener) {
        LogUtils.LOGD(TAG, "setOnCategoryListener : ");
        mListener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LogUtils.LOGD(TAG, "onCreateViewHolder : ");
        View view = LayoutInflater.from(mContext).inflate(R.layout.ahd_category_header_item, parent, false);
        view.getLayoutParams().width = mItemwidth;
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        LogUtils.LOGD(TAG, "onBindViewHolder : ");
        Categories category = mCategoryList.get(position);
        if (category != null) {
            holder.title.setText(category.getName());

            holder.view.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onCategoryItemClick(category);
                }
            });
            holder.icon.setDefaultResId(R.drawable.ahd_ic_cat_facial);

            if (!TextUtils.isEmpty(category.getIcon())) {
                holder.icon.startLoading(category.getIcon());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mCategoryList == null) return 0;

        return mCategoryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        final TextView title;
        final NetworkImageView icon;
        final View view;

        public Holder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.name);
            icon = (NetworkImageView) itemView.findViewById(R.id.icon);
            view = itemView;
        }
    }
}

