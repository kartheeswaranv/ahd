package com.athomediva.ui.subcategory;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.local.CartModel;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.widgets.MinOrderErrorWidget;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 19/04/17.
 */

public class CartWidget extends RelativeLayout {
    private static final String TAG = CartWidget.class.getSimpleName();

    @BindView(R.id.services_selected_count) TextView mServiceCountView;
    @BindView(R.id.services_selected_amount) TextView mTotalView;
    @BindView(R.id.view_cart) TextView mViewCart;
    @BindView(R.id.icon) ImageView mIconView;
    @BindView(R.id.min_order_error) MinOrderErrorWidget mMinOrderErrorWidget;

    public CartWidget(Context context) {
        super(context);
    }

    public CartWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CartWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        UiUtils.setTintToDrawable(getContext(), mIconView.getDrawable(),
          ContextCompat.getColor(getContext(), R.color.cart_widget_bg_color));
    }

    public void setCartActionListener(OnClickListener listener) {
        LogUtils.LOGD(TAG, "setCartActionListener : ");
        OnClickListener mViewCartListener = listener;
        mViewCart.setOnClickListener(mViewCartListener);
    }

    public void updateCart(CartModel model) {
        LogUtils.LOGD(TAG, "updateCart : ");
        if (model != null) {
            String servicesCountTxt = String.valueOf(model.getCount());
            mServiceCountView.setText(Html.fromHtml(servicesCountTxt));
            mTotalView.setText(UiUtils.getFormattedAmount(getContext(), model.getAmount()));
        }
    }

    public void updateErrorInfo(MinimumOrderError error) {
        LogUtils.LOGD(TAG, "updateErrorInfo : ");
        if (error != null) {
            enableViewCart(false);
            mMinOrderErrorWidget.setVisibility(View.VISIBLE);
            mMinOrderErrorWidget.updateMinimumOrderInfo(error);
        } else {
            enableViewCart(true);
            mMinOrderErrorWidget.setVisibility(View.GONE);
        }
    }

    private void enableViewCart(boolean enable) {
        LogUtils.LOGD(TAG, "enableViewCart : ");
        mViewCart.setClickable(enable);
        if (enable) {
            UiUtils.setTintToDrawable(getContext(), mViewCart.getBackground(),
              ContextCompat.getColor(getContext(), R.color.colorPrimary));
            mViewCart.invalidate();
        } else {
            UiUtils.setTintToDrawable(getContext(), mViewCart.getBackground(),
              ContextCompat.getColor(getContext(), R.color.ahd_grey_dark));
            mViewCart.invalidate();
        }
    }
}
