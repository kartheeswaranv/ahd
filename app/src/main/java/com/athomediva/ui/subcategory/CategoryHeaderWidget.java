package com.athomediva.ui.subcategory;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 18/04/17.
 */

public class CategoryHeaderWidget extends FrameLayout {
    private static final String TAG = CategoryHeaderWidget.class.getSimpleName();
    private final List<Categories> mList = new ArrayList<>();
    @BindView(R.id.category_list) RecyclerView mCategoryList;
    private CategoryHeaderAdapter mAdapter;
    private OnCategorySelectListener mListener;

    public CategoryHeaderWidget(Context context) {
        super(context);
    }

    public CategoryHeaderWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CategoryHeaderWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        initView();
    }

    public void setItemClickListener(OnCategorySelectListener listener) {
        LogUtils.LOGD(TAG, "setItemClickListener : ");
        mListener = listener;
        if(mAdapter != null) {
            mAdapter.setOnCategoryListener(mListener);
        }
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mCategoryList.setLayoutManager(manager);
        mAdapter = new CategoryHeaderAdapter(getContext(), mList);
        mAdapter.setOnCategoryListener(mListener);
        mCategoryList.setAdapter(mAdapter);
    }

    @DebugLog
    public void updateCategories(List<Categories> catList) {
        LogUtils.LOGD(TAG, "updateCategories : ");
        if (catList != null && catList.size() > 0) {
            mAdapter.updateAdapter(catList);
        }
    }

    public interface OnCategorySelectListener {
        void onCategoryItemClick(Categories category);
    }
}
