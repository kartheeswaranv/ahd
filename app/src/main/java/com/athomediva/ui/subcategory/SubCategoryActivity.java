package com.athomediva.ui.subcategory;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.PagerSlidingTabStrip;
import com.athomediva.data.DataProcessor;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.CartModel;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.PackagesContract;
import com.athomediva.mvpcontract.SubcatSectionContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.offers.InstantCouponHeaderWidget;
import com.athomediva.ui.packages.PackagesFragment;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 16/04/17.
 */

public class SubCategoryActivity extends BaseActivity
  implements SubcatSectionContract.View, View.OnClickListener, CategoryHeaderWidget.OnCategorySelectListener {
    private static final String TAG = LogUtils.makeLogTag(SubCategoryActivity.class.getSimpleName());

    @Inject SubcatSectionContract.Presenter mPresenter;

    @BindView(R.id.tabs) PagerSlidingTabStrip mTabs;
    @BindView(R.id.subcat_pager) ViewPager mViewPager;
    @BindView(R.id.appToolbar) Toolbar mToolbar;
    @BindView(R.id.category_header) CategoryHeaderWidget mCategoryHeader;
    @BindView(R.id.cart_widget) CartWidget mCartWidget;
    @BindView(R.id.package_container) FrameLayout mPackageContainer;
    @BindView(R.id.shadow) View mShadowView;
    @BindView(R.id.instant_coupon_header_view) InstantCouponHeaderWidget mCouponHeaderView;

    private SubCatPager mPagerAdapter;

    private boolean isShowCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_activity_sub_category);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.SubCatActModule(getIntent())).inject(this);
        mPresenter.attachView(this);
        initViews();
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.CATEGORY,
            GATrackerContext.Label.CATEGORY_PAGE_LOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
        mCartWidget.setCartActionListener(view -> mPresenter.onViewCartClick());
        mCategoryHeader.setOnClickListener(this);
        mCategoryHeader.setItemClickListener(this);
    }

    /**
     * Initialize Toolbar Views
     */
    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.title);
        ImageView dropdown = (ImageView) mToolbar.findViewById(R.id.drop_down);
        UiUtils.setTintToDrawable(this, dropdown.getDrawable(), ContextCompat.getColor(this, R.color.white));
        mToolbar.setOnClickListener(this);
    }

    /**
     * Toolbar down arrow rotation animation
     */
    @DebugLog
    private void animateToolbar(boolean isCategoryOpen) {
        LogUtils.LOGD(TAG, "animateToolbar : ");
        TypedValue startAngle = new TypedValue();
        TypedValue endAngle = new TypedValue();
        getResources().getValue(R.dimen.subcat_rotation_start_angle, startAngle, false);
        getResources().getValue(R.dimen.subcat_rotation_end_angle, endAngle, false);
        ImageView dropdown = (ImageView) mToolbar.findViewById(R.id.drop_down);
        if (isCategoryOpen) {
            AnimationHelper.rotateViewAnimation(dropdown, startAngle.getFloat(), endAngle.getFloat());
        } else {
            AnimationHelper.rotateViewAnimation(dropdown, endAngle.getFloat(), startAngle.getFloat());
        }
    }

    @DebugLog
    @Override
    public void updateToolbar(String titleTxt) {
        LogUtils.LOGD(TAG, "updateToolbar : ");
        if (!TextUtils.isEmpty(titleTxt)) {
            TextView title = (TextView) mToolbar.findViewById(R.id.title);
            title.setText(titleTxt);
        }
    }

    @DebugLog
    @Override
    public void updateSubCats(List<SubCategories> serviceList, int pagePosition) {
        LogUtils.LOGD(TAG, "updateSubCats : ");
        mPackageContainer.setVisibility(View.GONE);
        mTabs.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.VISIBLE);
        mPagerAdapter = new SubCatPager(getSupportFragmentManager(), this, serviceList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabs.setViewPager(mViewPager);
        mViewPager.setCurrentItem(pagePosition);
        mPresenter.onSubCatClicked(pagePosition);
        mTabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mPresenter.onSubCatClicked(position);
            }

            @Override
            public void onPageSelected(int position) {
                mPresenter.onSubCatClicked(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @DebugLog
    @Override
    public void showCategories(List<Categories> list) {
        LogUtils.LOGD(TAG, "showCategories : ");
        if (list != null && list.size() > 0) {
            mCategoryHeader.setVisibility(View.VISIBLE);
            animateToolbar(true);
            mCategoryHeader.updateCategories(list);
        }
    }

    @Override
    public void launchPackageFragment(ArrayList<Services> list) {
        LogUtils.LOGD(TAG, "launchPackageFragment : ");
        mTabs.setVisibility(View.GONE);
        mViewPager.setVisibility(View.GONE);
        mPackageContainer.setVisibility(View.VISIBLE);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(PackagesContract.PARAM_PACKAGE_LIST, list);
        PackagesFragment fragment = PackagesFragment.newInstance(bundle);

        getSupportFragmentManager().beginTransaction()
          .replace(R.id.package_container, fragment, PackagesFragment.TAG)
          .commit();
    }

    @Override
    public void hideCategories() {
        LogUtils.LOGD(TAG, "hideCategories : ");
        isShowCategory = false;
        animateToolbar(false);
        mCategoryHeader.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.appToolbar:
                if (!isShowCategory) {
                    isShowCategory = true;
                    mPresenter.onToolbarClick();
                } else {
                    onOutsideClick();
                }
                break;
            case R.id.category_header:
                onOutsideClick();
                break;
        }
    }

    @Override
    public void onCategoryItemClick(Categories category) {
        mPresenter.onCategoryClick(category);
    }

    @DebugLog
    @Override
    public void updateCart(CartModel model) {
        LogUtils.LOGD(TAG, "updateCart : ");
        if (model != null && model.getCount() > 0) {
            mCartWidget.setVisibility(View.VISIBLE);
            mCartWidget.updateCart(model);
        } else {
            mCartWidget.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updateMinOrderError(MinimumOrderError error) {
        LogUtils.LOGD(TAG, "updateMinOrderError : ");
        if (mCartWidget != null) {
            mCartWidget.updateErrorInfo(error);
            if (error == null && mCartWidget.getVisibility() == View.VISIBLE) {
                mShadowView.setVisibility(View.VISIBLE);
            } else {
                mShadowView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void updateInstantCouponView(InstantCoupon coupon, boolean disableShadow) {
        LogUtils.LOGD(TAG, "updateInstantCouponView: ");
        if (coupon != null) {
            mCouponHeaderView.setVisibility(View.VISIBLE);
            mCouponHeaderView.setDisableShadow(disableShadow);
            mCouponHeaderView.enableEmptySpace(true);
            String amount = UiUtils.getFormattedAmount(this, coupon.getAmount());
            String percentage = coupon.getPercentage() + "%";
            String instant_coupon_msg = getString(R.string.instant_offer_text);
            mCouponHeaderView.setTag(coupon);

            String msg = DataProcessor.getCouponMessage(this, coupon);
            mCouponHeaderView.updateView(msg);
            mCouponHeaderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.LOGD(TAG, "onClick: Instant CouponClick");
                    InstantCoupon instantCoupon = (InstantCoupon) v.getTag();
                    mPresenter.onInstantCouponClick(instantCoupon);
                }
            });
        } else {
            mCouponHeaderView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateTabs() {
        LogUtils.LOGD(TAG, "updateTabs : ");
        if (mPagerAdapter != null && mTabs != null) {
            mPagerAdapter.notifyDataSetChanged();
            mViewPager.post(() -> {
                LogUtils.LOGD(TAG, " Update Tabs with count run : ");
                if (mTabs != null) {
                    mTabs.notifyDataSetChanged();
                }
            });
        }
    }

    private void onOutsideClick() {
        LogUtils.LOGD(TAG, "onOutsideClick : ");
        hideCategories();
    }
}
