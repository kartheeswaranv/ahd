package com.athomediva.ui.subcategory;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.athomediva.customview.PagerSlidingTabStrip;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.ServicesContract;
import com.athomediva.ui.home.OnSectionItemSelectionListener;
import com.athomediva.ui.services.ServicesFragment;
import hugo.weaving.DebugLog;
import java.util.List;

/**
 * Created by kartheeswaran on 16/04/17.
 */

class SubCatPager extends FragmentStatePagerAdapter implements PagerSlidingTabStrip.BadgeTabProvider {
    private static final String TAG = SubCatPager.class.getSimpleName();

    private List<SubCategories> mList;
    private OnSectionItemSelectionListener mListener;

    @DebugLog
    public SubCatPager(FragmentManager manager, Context context, List<SubCategories> list) {
        super(manager);
        mList = list;
        LogUtils.LOGD(TAG, "SubCatPager : ");
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    @Override
    public Fragment getItem(int position) {
        LogUtils.LOGD(TAG, "getItem : ");
        Bundle b = new Bundle();
        b.putInt(ServicesContract.PARAM_SELECT_POSITION, position);
        b.putParcelableArrayList(ServicesContract.PARAM_SELECTED_SERVICES, mList.get(position).getServices());
        return ServicesFragment.newInstance(b);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        LogUtils.LOGD(TAG, "getPageTitle : ");
        // Generate title based on item position
        return mList.get(position).getName();
    }

    @Override
    public int getCount(int position) {
        LogUtils.LOGD(TAG, "getCount : " + position);
        if (mList == null || mList.size() < position) return 0;
        return CoreLogic.getServicesCount(mList.get(position).getServices());
    }
}
