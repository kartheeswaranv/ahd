package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandListView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.WalletInfo;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 17/05/17.
 */

public class WalletListInfoView extends LinearLayout {
    private static final String TAG = WalletListInfoView.class.getSimpleName();

    @BindView(R.id.info_list) ExpandListView mListView;

    private View mFooterView;

    public WalletListInfoView(Context context) {
        super(context);
    }

    public WalletListInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WalletListInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        initViews();
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        mListView.setExpanded(true);
        mFooterView = LayoutInflater.from(getContext()).inflate(R.layout.ahd_wallet_info_item, null);
    }

    @DebugLog
    public void setInfoList(List<WalletInfo> list, String total) {
        LogUtils.LOGD(TAG, "setInfoList : ");
        if (list != null && !list.isEmpty()) {
            mListView.setAdapter(new WalletInfoAdapter(getContext(), R.layout.ahd_wallet_info_item, list));
            mListView.addFooterView(mFooterView);
            updateFooter(total);
        } else {
            LogUtils.LOGD(TAG, "setInfoList : Empty");
        }
    }

    private void updateFooter(String total) {
        LogUtils.LOGD(TAG, "addFooter : ");
        TextViewCustom name = (TextViewCustom) mFooterView.findViewById(R.id.wallet_name);
        name.setTtfName(getContext().getString(R.string.ubuntu_medium_font));
        name.setText(getContext().getString(R.string.Total));
        TextViewCustom amt = (TextViewCustom) mFooterView.findViewById(R.id.wallet_amount);
        amt.setTtfName(getContext().getString(R.string.ubuntu_medium_font));
        amt.setText(UiUtils.getFormattedAmountByString(getContext(), total));
    }
}
