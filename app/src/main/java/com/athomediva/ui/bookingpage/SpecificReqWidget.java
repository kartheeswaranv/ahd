package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.StrokeDrawable;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 15/05/17.
 */

public class SpecificReqWidget extends RelativeLayout {
    private static final String TAG = SpecificReqWidget.class.getSimpleName();

    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.spec_req_input) EditText mRequest;
    @BindView(R.id.specific_req_msg) TextView mMessage;
    @BindView(R.id.input_title) TextView mInputTitle;
    @BindView(R.id.specific_req_send) ImageView mSend;
    @BindView(R.id.specific_req_edit) ImageView mEdit;

    private boolean mOptionEnable;
    private TextListener mTextListener;
    private Drawable mDefaultDrawable;
    private Drawable mErrorDrawable;
    private Drawable mHighlightDrawable;
    private int mHintColor;
    private int mHintHighlightColor;
    private int mErrorColor;
    private int mStrokeWidth;
    private int mStrokeHighLightWidth;

    public SpecificReqWidget(Context context) {
        super(context);
    }

    public SpecificReqWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttr(context, attrs, 0);
    }

    public SpecificReqWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(context, attrs, defStyleAttr);
    }

    private void initAttr(Context context, AttributeSet attrs, int style) {
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.SpecificReqWidget, style, 0);
            mOptionEnable = ta.getBoolean(R.styleable.SpecificReqWidget_optionEnable, false);
            ta.recycle();
        }
        mErrorColor = ContextCompat.getColor(getContext(), R.color.validation_error);
        mHintColor = ContextCompat.getColor(getContext(), R.color.ahd_grey_dark);
        mHintHighlightColor = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        mStrokeWidth = UiUtils.dpToPx(1);
        mStrokeHighLightWidth = UiUtils.dpToPx(2);
        getDrawables();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        mEdit.setOnClickListener(view -> {
            mInputTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            mRequest.setEnabled(true);
            mRequest.requestFocus();
            mRequest.setBackgroundDrawable(mHighlightDrawable);
        });

        mRequest.setBackgroundDrawable(mDefaultDrawable);
        addFocusListener();

        mTextListener = new TextListener();
        setOptionEnable(mOptionEnable);
    }

    public void setOptionEnable(boolean value) {
        mOptionEnable = value;
        if (value) {
            mRequest.addTextChangedListener(mTextListener);
        } else {
            mRequest.removeTextChangedListener(mTextListener);
            LogUtils.LOGD(TAG, "onFinishInflate : Option Enable " + mOptionEnable);
        }
    }

    @DebugLog
    public void setSendClickListener(OnClickListener listener) {
        LogUtils.LOGD(TAG, "setSendClickListener : ");
        mSend.setOnClickListener(listener);
    }

    @DebugLog
    public void updateSpecificInfo(String details) {
        LogUtils.LOGD(TAG, "updateSpecificInfo : ");
        if (!TextUtils.isEmpty(details)) {
            mRequest.setText(details);
            mInputTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.ahd_grey_dark));
            mRequest.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.full_transparent));
            mEdit.setVisibility(View.VISIBLE);
            mSend.setVisibility(View.INVISIBLE);
            mRequest.setEnabled(false);
        } else {
            mRequest.setEnabled(true);
            mEdit.setVisibility(View.INVISIBLE);
            mSend.setVisibility(View.INVISIBLE);
            mRequest.setText("");
        }
    }

    private class TextListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing becz after text change handles all
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing becz after text change handles all
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable != null && !TextUtils.isEmpty(editable) && editable.length() > 1) {
                mSend.setVisibility(View.VISIBLE);
                mEdit.setVisibility(View.INVISIBLE);
            } else {
                mSend.setVisibility(View.INVISIBLE);
                mEdit.setVisibility(View.INVISIBLE);
            }
        }
    }

    public String getText() {
        if (!TextUtils.isEmpty(mRequest.getText())) {
            return mRequest.getText().toString();
        }
        return null;
    }

    private void getDrawables() {
        mErrorDrawable = new StrokeDrawable(getContext(), mErrorColor, mStrokeHighLightWidth);
        mHighlightDrawable = new StrokeDrawable(getContext(), mHintHighlightColor, mStrokeHighLightWidth);
        mDefaultDrawable = new StrokeDrawable(getContext(), mHintColor, mStrokeWidth);
    }

    private void addFocusListener() {
        if (mRequest != null) {
            mRequest.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        mInputTitle.setTextColor(mHintHighlightColor);
                        mRequest.setBackgroundDrawable(mHighlightDrawable);
                    } else {
                        mInputTitle.setTextColor(mHintColor);
                        mRequest.setBackgroundDrawable(mDefaultDrawable);
                    }
                }
            });
        }
    }

    private ColorStateList getHintColors() {
        int[][] states = new int[][] {
          new int[] { android.R.attr.state_focused }, // enabled
          new int[] { -android.R.attr.state_empty }, // disabled
        };

        int[] colors = new int[] {
          mHintHighlightColor, mHintColor
        };

        ColorStateList myList = new ColorStateList(states, colors);
        return myList;
    }
}
