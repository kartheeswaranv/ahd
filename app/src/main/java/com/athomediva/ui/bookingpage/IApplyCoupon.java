package com.athomediva.ui.bookingpage;

/**
 * Created by kartheeswaran on 11/05/17.
 */
public interface IApplyCoupon {

    /**
     * Triggers api call for verifying coupon code.
     * Method can be called for AUTO_APPLY or Manual(button click)
     */
    void onCouponApply(String couponCode);

    void onCouponRemove();
}
