package com.athomediva.ui.bookingpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.remote.PaymentPendingBookingDetails;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.PaymentPendingContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingFragment extends BaseFragment<PaymentPendingContract.Presenter, PaymentPendingContract.View>
  implements PaymentPendingContract.View {
    private static final String TAG = LogUtils.makeLogTag(PaymentPendingFragment.class.getSimpleName());
    public static final String FRAG_TAG = PaymentPendingFragment.class.getCanonicalName();

    @BindView(R.id.title_txt) TextView mTitle;
    @BindView(R.id.desc_txt) TextView mDesc;
    @BindView(R.id.footer_desc) TextView mFooterDesc;
    @BindView(R.id.booking_list) ExpandListView mBookingListView;
    @BindView(R.id.proceed_payment) TextView mProceed;
    @BindView(R.id.call_customer) TextView mCallCustomer;

    @Inject PaymentPendingContract.Presenter mPresenter;

    public static PaymentPendingFragment newInstance(Bundle bundle) {
        PaymentPendingFragment fragment = new PaymentPendingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.ahd_payment_pending_frag, container, false);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.PaymentPendingModule(getArguments()))
          .inject(this);
        bindView(this, view);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        initToolbar();
        mProceed.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onProceedClick();
        });

        mCallCustomer.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onCallClick();
        });
        mBookingListView.setExpanded(true);
        mBookingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                LogUtils.LOGD(TAG, "onItemClick : ");
            }
        });
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.payment_pending_title));
        ((FragmentContainerActivity) getActivity()).setNavigationIcon(R.drawable.ahd_ic_close);
    }

    @Override
    public void updateBookingListView(ArrayList<PaymentPendingBookingDetails> list) {
        LogUtils.LOGD(TAG, "updateBookingListView : ");

        mBookingListView.setAdapter(new PaymentPendingBookingAdapter(getContext(), list));
    }

    @Override
    public void updateTitle(String title) {
        LogUtils.LOGD(TAG, "updateTitle : ");
        if (!TextUtils.isEmpty(title)) mTitle.setText(getString(R.string.booking_failed_title, title));
    }

    @Override
    public void updateCTA(String cta) {
        LogUtils.LOGD(TAG, "updateCTA : ");
        UIHelper.hideViewIfEmptyTxt(mProceed, cta);
    }

    @Override
    public void updateFooterView(String footerMsg) {
        LogUtils.LOGD(TAG, "updateFooterView : ");
        UIHelper.hideViewIfEmptyTxt(mFooterDesc, footerMsg);
    }

    @Override
    public void updateHeaderView(String headerMsg) {
        LogUtils.LOGD(TAG, "updateHeaderView : ");
        UIHelper.hideViewIfEmptyTxt(mDesc, headerMsg);
    }
}
