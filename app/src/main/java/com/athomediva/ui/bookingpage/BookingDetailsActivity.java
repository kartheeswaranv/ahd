package com.athomediva.ui.bookingpage;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.local.StatusModel;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.PaymentInfo;
import com.athomediva.data.models.remote.Services;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.mycart.MyCartAddressWidget;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 17/05/17.
 */

public class BookingDetailsActivity extends BaseActivity implements BookingDetailsContract.View, View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(BookingDetailsActivity.class.getSimpleName());

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.services_group_list) ExpandListView mServicesListView;
    @BindView(R.id.payment_info_list) ExpandListView mPaymentInfoListView;
    @BindView(R.id.address) MyCartAddressWidget mAddressWidget;
    @BindView(R.id.specific_req_view) SpecificReqWidget mSpecificReqWidget;
    @BindView(R.id.cancel_booking) TextView mCancelBtn;
    @BindView(R.id.customer_care) TextView mCustomerCareBtn;
    @BindView(R.id.status_view) BookingStatusView mStatusView;
    @BindView(R.id.auth_code_value) TextView mAuthCodeTxt;
    @BindView(R.id.duration_value) TextView mDurationTxt;
    @BindView(R.id.bookingid_value) TextView mBookingIDTxt;
    @BindView(R.id.appointmnt_value) TextView mAppointment;
    @BindView(R.id.swipe_container) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.parent_view) ScrollView mScrollContainer;
    @Inject BookingDetailsContract.Presenter mPresenter;

    private TextView mSubTitle;
    private InfoDialog mInfoDialog;
    private View mPaymentModeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_activity_booking_details);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.BookingDetailsModule(getIntent())).inject(this);
        initViews();
        mPresenter.attachView(this);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
        mServicesListView.setExpanded(true);
        mPaymentInfoListView.setExpanded(true);
        mSpecificReqWidget.setOptionEnable(true);
        mCancelBtn.setOnClickListener(view -> mPresenter.onCancelClick());
        mCustomerCareBtn.setOnClickListener(view -> mPresenter.onCustomerCareClick());

        mSpecificReqWidget.setSendClickListener(view -> mPresenter.onSpecificReqSendClick(mSpecificReqWidget.getText()));
        mStatusView.setListener(view -> mPresenter.onRateServiceClick(), view -> mPresenter.onPayonlineClick());
        mStatusView.setCallListener(view -> mPresenter.onCustomerCareClick());
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            LogUtils.LOGD(TAG, "onRefresh : ");
            mPresenter.onRefreshClick();
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.title);
        title.setText(getString(R.string.ahd_booking_details_title));

        mSubTitle = (TextView) mToolbar.findViewById(R.id.sub_title);
    }

    @DebugLog
    @Override
    public void updateSubTitleView(String title) {
        LogUtils.LOGD(TAG, "updateSubTitle : ");
        if (!TextUtils.isEmpty(title)) {
            mSubTitle.setText(title);
            mSubTitle.setVisibility(View.VISIBLE);
        } else {
            mSubTitle.setVisibility(View.GONE);
        }
    }

    public void updateStatusView(StatusModel model) {
        LogUtils.LOGD(TAG, "updateStatusView : ");
        if (model != null) {
            mStatusView.setVisibility(View.VISIBLE);
            mStatusView.updateStatus(model);
        } else {
            mStatusView.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updateServicesView(List<Services> services) {
        LogUtils.LOGD(TAG, "updateServicesView : ");
        if (services != null && services.size() > 0) {
            mServicesListView.setVisibility(View.VISIBLE);
            mServicesListView.setAdapter(new BookingServiceGroupAdapter(this, services));
        } else {
            mServicesListView.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updatePaymentInfo(ArrayList<PaymentInfo> payments, String paymentMode) {
        LogUtils.LOGD(TAG, "updatePaymentInfo : ");
        if (payments != null && payments.size() > 0) {
            mPaymentInfoListView.setVisibility(View.VISIBLE);
            mPaymentInfoListView.setAdapter(new PaymentInfoAdapter(this, payments));
            addPaymentModeView(paymentMode);
        } else {
            mPaymentInfoListView.setVisibility(View.GONE);
        }
    }

    private void addPaymentModeView(String paymentMode) {
        LogUtils.LOGD(TAG, "addPaymentModeView : ");
        if (mPaymentInfoListView != null && !TextUtils.isEmpty(paymentMode)) {
            if(mPaymentModeView == null) {
                mPaymentModeView = LayoutInflater.from(this).inflate(R.layout.ahd_payment_info_item, null);

                TextView tvTitle = (TextView) mPaymentModeView.findViewById(R.id.name);
                TextView tvMode = (TextView) mPaymentModeView.findViewById(R.id.price);
                tvTitle.setText(getString(R.string.payment_mode));
                tvMode.setText(paymentMode);
                mPaymentInfoListView.addFooterView(mPaymentModeView);
            } else {
                //If Payment Mode View is already added, update the views
                TextView tvMode = (TextView) mPaymentModeView.findViewById(R.id.price);
                tvMode.setText(paymentMode);
                mPaymentModeView.invalidate();
            }
        }
    }

    @DebugLog
    @Override
    public void updateSpecificReqView(String reqMsg, boolean enable) {
        LogUtils.LOGD(TAG, "updateSpecificReqView : ");
        mSpecificReqWidget.updateSpecificInfo(reqMsg);
        if (!enable) {
            mSpecificReqWidget.setVisibility(View.GONE);
        } else {
            mSpecificReqWidget.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        LogUtils.LOGD(TAG, "onClick : ");
        switch (view.getId()) {
            case R.id.cancel_booking:
                mPresenter.onCancelClick();
                break;
            case R.id.customer_care:
                mPresenter.onCustomerCareClick();
                break;
        }
    }

    @Override
    public void updateAddressView(Address address) {
        LogUtils.LOGD(TAG, "updateAddressView : ");
        if (address != null) {
            mAddressWidget.setVisibility(View.VISIBLE);
            mAddressWidget.enableOptions(false);
            mAddressWidget.updateLocation(address);
        } else {
            mAddressWidget.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void showCancelDialog(String title, String msg) {
        LogUtils.LOGD(TAG, "showWalletInfo : ");

        if (TextUtils.isEmpty(msg)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(title, msg);
        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.dialog_yes), view -> {
            mInfoDialog.dismiss();
            mPresenter.onCancelConfirmClick();
        });

        mInfoDialog.setNegativeButton(getString(R.string.dialog_no), view -> mInfoDialog.dismiss());
    }

    @DebugLog
    @Override
    public void showCancelStatusDialog(String msg) {
        LogUtils.LOGD(TAG, "showWalletInfo : ");

        if (TextUtils.isEmpty(msg)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(null, msg);
        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view -> {
            mInfoDialog.dismiss();
            finishViewWithResult(RESULT_OK);
        });

        mInfoDialog.setOnCancelListener(dialogInterface -> {
            LogUtils.LOGD(TAG, "onCancel : ");
            finishViewWithResult(RESULT_OK);
        });
    }

    @Override
    public void showCTA(boolean cancel, boolean customer) {
        LogUtils.LOGD(TAG, "showCTA : ");
        showOrHideView(mCancelBtn, cancel);
        showOrHideView(mCustomerCareBtn, customer);
    }

    private void showOrHideView(View view, boolean value) {
        LogUtils.LOGD(TAG, "showOrHideView : ");
        if (value) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updateHeaderView(String date, String duration, String bookingId, String authCode) {
        LogUtils.LOGD(TAG, "updateHeaderView : ");
        UIHelper.hideViewIfEmptyTxt(mAppointment, date);
        UIHelper.hideViewIfEmptyTxt(mDurationTxt, duration);
        UIHelper.hideViewIfEmptyTxt(mBookingIDTxt, bookingId);
        UIHelper.hideViewIfEmptyTxt(mAuthCodeTxt, authCode);
    }

    @Override
    protected void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy : ");
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        super.onDestroy();
    }

    @Override
    public void hideSwipeRefreshView() {
        LogUtils.LOGD(TAG, "hideSwipeRefreshView : ");
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showContainerView() {
        LogUtils.LOGD(TAG, "showContainerView : ");
        if (mScrollContainer != null) {
            showOrHideView(mScrollContainer, true);
        }
    }
}
