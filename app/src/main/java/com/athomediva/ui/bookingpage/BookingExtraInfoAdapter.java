package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.BookingExtraInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 14/03/18.
 */

public class BookingExtraInfoAdapter extends BaseAdapter {
    private static final String TAG = BookingFailureDescAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<BookingExtraInfo> mInfoList;

    public BookingExtraInfoAdapter(Context context, ArrayList<BookingExtraInfo> infoList) {
        LogUtils.LOGD(TAG, "BookingFailureDescAdapter : ");
        mContext = context;
        mInfoList = infoList;
    }

    @Override
    public int getCount() {
        if (mInfoList == null) return 0;
        return mInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return mInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        Holder holder = null;
        BookingExtraInfo info = mInfoList.get(pos);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_booking_extra_info_item, null);
            holder = new Holder();
            holder.mTitle = (TextView) view.findViewById(R.id.title);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        if (info != null) {
            UIHelper.hideViewIfEmptyTxt(holder.mTitle, info.getInfo());
            try {
                if (TextUtils.isEmpty(info.getColor()) && info.getColor().startsWith("#")) {
                    holder.mTitle.setTextColor(Color.parseColor(info.getColor()));
                }
            } catch (Exception e) {
                LogUtils.LOGD(TAG, "getView: Color Code is not correct");
            }
        }

        return view;
    }

    private static class Holder {
        TextView mTitle;
    }
}

