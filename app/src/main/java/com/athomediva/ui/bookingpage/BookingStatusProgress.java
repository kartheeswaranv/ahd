package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Scroller;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

import static com.athomediva.data.AppConstants.MAXIMUM_STATE;
import static com.athomediva.data.AppConstants.MINIMUM_STATE;

/**
 * Created by kartheeswaran on 17/05/17.
 */
public class BookingStatusProgress extends View {
    private static final String TAG = BookingStatusProgress.class.getSimpleName();

    private static final int MIN_STATE_NUMBER = MINIMUM_STATE;
    private static final int MAX_STATE_NUMBER = MAXIMUM_STATE;

    private float mStateRadius;
    private float mStateSize;
    private float mStateLineThickness;
    private float mStateLineHighlightThickness;
    private float mStateNumberTextSize;
    private float mStateDescriptionSize;
    private float mStateCircleStroke;
    private float mIndicatorMargin;

    /**
     * width of one cell = stageWidth/noOfStates
     */
    private float mCellWidth;

    private float mCellHeight;

    /**
     * next cell(state) from previous cell
     */
    private float mNextCellWidth;

    /**
     * center of first cell(state)
     */
    private float mStartCenterX;

    /**
     * center of last cell(state)
     */
    private float mEndCenterX;

    private int mMaxStateNumber;
    private int mCurrentStateNumber;

    private int mAnimStartDelay;
    private int mAnimDuration;

    private float mSpacing;

    private static final float DEFAULT_TEXT_SIZE = 15f;
    private static final float DEFAULT_STATE_SIZE = 25f;

    /**
     * Paints for drawing
     */
    private Paint mBackgroundPaint;
    private Paint mForegroundPaint;
    private Paint mStrokePaint;

    private int mBackgroundColor;
    private int mForegroundColor;

    /**
     * animate inner line to current progress state
     */
    private Animator mAnimator;

    /**
     * tracks progress of line animator
     */
    private float mAnimStartXPos;
    private float mAnimEndXPos;

    private boolean mIsCurrentAnimStarted;

    private boolean mAnimateToCurrentProgressState;
    private boolean mEnableAllStatesCompleted;

    private StateProgressListener mListener;

    public interface StateProgressListener {
        void onAnimationStop(float animationStopX);
    }

    public BookingStatusProgress(Context context) {
        this(context, null, 0);
    }

    public BookingStatusProgress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BookingStatusProgress(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
        initializePainters();
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        /**
         * Setting default values.
         */
        initStateProgressBar();

        mStateDescriptionSize = convertSpToPixel(mStateDescriptionSize);
        mStateLineThickness = convertDpToPixel(mStateLineThickness);
        mStateLineHighlightThickness = convertDpToPixel(mStateLineHighlightThickness);
        mSpacing = convertDpToPixel(mSpacing);

        //mCheckFont = FontManager.getTypeface(context);

        if (attrs != null) {

            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BookingStatusProgress, defStyle, 0);

            mBackgroundColor = a.getColor(R.styleable.BookingStatusProgress_spb_stateBackgroundColor, mBackgroundColor);
            mForegroundColor = a.getColor(R.styleable.BookingStatusProgress_spb_stateForegroundColor, mForegroundColor);

            mCurrentStateNumber =
              a.getInteger(R.styleable.BookingStatusProgress_spb_currentStateNumber, mCurrentStateNumber);
            mMaxStateNumber = a.getInteger(R.styleable.BookingStatusProgress_spb_maxStateNumber, mMaxStateNumber);

            mStateSize = a.getDimension(R.styleable.BookingStatusProgress_spb_stateSize, mStateSize);
            mStateRadius = a.getDimension(R.styleable.BookingStatusProgress_spb_state_circle_size, mStateSize);
            mStateCircleStroke =
              a.getDimension(R.styleable.BookingStatusProgress_spb_state_circle_stroke, mStateCircleStroke);

            mStateNumberTextSize = a.getDimension(R.styleable.BookingStatusProgress_spb_stateTextSize, mStateNumberTextSize);
            mStateLineHighlightThickness =
              a.getDimension(R.styleable.BookingStatusProgress_spb_stateLineHighlightThickness, mStateLineThickness);
            mStateLineThickness =
              a.getDimension(R.styleable.BookingStatusProgress_spb_stateLineNormalThickness, mStateLineThickness);

            mAnimateToCurrentProgressState =
              a.getBoolean(R.styleable.BookingStatusProgress_spb_animateToCurrentProgressState,
                mAnimateToCurrentProgressState);
            mEnableAllStatesCompleted =
              a.getBoolean(R.styleable.BookingStatusProgress_spb_enableAllStatesCompleted, mEnableAllStatesCompleted);

            mAnimDuration = a.getInteger(R.styleable.BookingStatusProgress_spb_animationDuration, mAnimDuration);
            mAnimStartDelay = a.getInteger(R.styleable.BookingStatusProgress_spb_animationStartDelay, mAnimStartDelay);

            mIndicatorMargin = a.getDimension(R.styleable.BookingStatusProgress_spb_indicator_margin, mIndicatorMargin);

            if (!mAnimateToCurrentProgressState) {
                stopAnimation();
            }

            resolveStateSize();
            validateLineThickness(mStateLineThickness);
            validateStateNumber(mCurrentStateNumber);

            a.recycle();
        }
    }

    private void initializePainters() {

        mBackgroundPaint = setPaintAttributes(mStateLineThickness, mBackgroundColor);
        mForegroundPaint = setPaintAttributes(mStateLineHighlightThickness, mForegroundColor);
        mStrokePaint = setPaintAttributes(3, Color.WHITE);
    }

    private void validateLineThickness(float lineThickness) {
        float halvedStateSize = mStateSize / 2;

        if (lineThickness > halvedStateSize) {
            mStateLineThickness = halvedStateSize;
        }
    }

    private void validateStateSize() {
        if (mStateSize <= mStateNumberTextSize) {
            mStateSize = mStateNumberTextSize + mStateNumberTextSize / 2;
        }
    }

    public void setBackgroundColor(int backgroundColor) {
        mBackgroundColor = backgroundColor;
        mBackgroundPaint.setColor(mBackgroundColor);
        invalidate();
    }

    public void setCurrentStateNumber(int currentStateNumber) {
        validateStateNumber(currentStateNumber);
        mCurrentStateNumber = currentStateNumber;
        invalidate();
    }

    public void setMaxStateNumber(int maximumState) {
        mMaxStateNumber = maximumState;
        validateStateNumber(mCurrentStateNumber);
        invalidate();
    }

    public void enableAnimationToCurrentState(boolean animateToCurrentProgressState) {
        LogUtils.LOGD(TAG, "enableAnimationToCurrentState : ");
        this.mAnimateToCurrentProgressState = animateToCurrentProgressState;

        if (mAnimateToCurrentProgressState && mAnimator == null) {
            //startAnimator();
        }

        invalidate();
    }

    private void validateStateNumber(int stateNumber) {
        LogUtils.LOGD(TAG, "validateStateNumber : ");
        if (stateNumber > mMaxStateNumber) {
            /*throw new IllegalStateException(
              "State number (" + stateNumber + ") cannot be greater than total number of states " + mMaxStateNumber);*/
        }
    }

    private Paint setPaintAttributes(float strokeWidth, int color) {
        LogUtils.LOGD(TAG, "setPaintAttributes : ");
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(color);
        return paint;
    }

    private void initStateProgressBar() {
        LogUtils.LOGD(TAG, "initStateProgressBar : ");
        mStateSize = 0.0f;
        mStateLineThickness = 4.0f;
        mStateNumberTextSize = 0.0f;

        mCurrentStateNumber = 1;
        mMaxStateNumber = 5;

        mSpacing = 4.0f;

        mAnimateToCurrentProgressState = false;
        mEnableAllStatesCompleted = false;

        mAnimStartDelay = 100;
        mAnimDuration = 4000;
    }

    private void resolveStateSize() {
        LogUtils.LOGD(TAG, "resolveStateSize : ");
        resolveStateSize(mStateSize != 0, mStateNumberTextSize != 0);
    }

    private void resolveStateSize(boolean isStateSizeSet, boolean isStateTextSizeSet) {
        LogUtils.LOGD(TAG, "resolveStateSize : ");
        if (!isStateSizeSet && !isStateTextSizeSet) {
            mStateSize = convertDpToPixel(DEFAULT_STATE_SIZE);
            mStateNumberTextSize = convertSpToPixel(DEFAULT_TEXT_SIZE);
        } else if (isStateSizeSet && isStateTextSizeSet) {
            validateStateSize();
        } else if (!isStateSizeSet) {
            mStateSize = mStateNumberTextSize + mStateNumberTextSize / 2;
        } else {
            mStateNumberTextSize = mStateSize - (mStateSize * 0.375f);
        }
    }

    @DebugLog
    public void setAnimationDuration(int duration) {
        LogUtils.LOGD(TAG, "setAnimationDuration : ");
        mAnimDuration = duration;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        LogUtils.LOGD(TAG, "onSizeChanged : ");
        float circleSize = 2 * (mStateRadius + mStateCircleStroke);
        mCellWidth = (getWidth() - circleSize) / (mMaxStateNumber - 1);
        mNextCellWidth = mCellWidth;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        LogUtils.LOGD(TAG, "onDraw : ");
        drawState(canvas, mAnimateToCurrentProgressState);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //LogUtils.LOGD(TAG,"onMeasure : ");
        int height = getDesiredHeight();
        int width = MeasureSpec.getSize(widthMeasureSpec);

        setMeasuredDimension(width, height);

        mCellHeight = getCellHeight();
    }

    private int getDesiredHeight() {
        return (int) (2 * mStateRadius) + (int) (mSpacing);
    }

    private int getCellHeight() {
        return (int) (2 * mStateRadius) + (int) (mSpacing);
    }

    private void drawState(Canvas canvas, boolean animate) {
        LogUtils.LOGD(TAG, "drawState : ");
        setAnimatorStartEndCenterX();

        drawLines(canvas, mBackgroundPaint, mCurrentStateNumber - 1, mMaxStateNumber);
        drawCurrentStateJoiningLine(canvas);
        drawCircles(canvas, mBackgroundPaint, 0, mMaxStateNumber);
        if (animate) {
            drawCircles(canvas, mForegroundPaint, mAnimEndXPos);
        } else {
            drawCircles(canvas, mForegroundPaint, getEndXPos());
            if (mListener != null) mListener.onAnimationStop(getEndXPos());
        }
    }

    private void drawLines(Canvas canvas, Paint paint, int startIndex, int endIndex) {
        LogUtils.LOGD(TAG, "drawLines : ");
        float startCenterX;
        float endCenterX;

        float circleSize = (mStateCircleStroke + mStateRadius);
        if (endIndex > startIndex) {

            startCenterX = circleSize;
            endCenterX = (mCellWidth * endIndex) + (2 * circleSize);
            canvas.drawLine(startCenterX, mCellHeight / 2, endCenterX, mCellHeight / 2, paint);
        }
    }

    private void drawCircles(Canvas canvas, Paint paint, int startIndex, int endIndex) {
        LogUtils.LOGD(TAG, "drawCircles : ");
        float circleSize = ((mStateCircleStroke + mStateRadius));

        canvas.drawCircle(circleSize, mCellHeight / 2, mStateRadius + mStateCircleStroke, mStrokePaint);
        canvas.drawCircle(circleSize, mCellHeight / 2, mStateRadius, paint);
        for (int i = startIndex; i < endIndex; i++) {
            canvas.drawCircle(mCellWidth * (i + 1) + (circleSize), mCellHeight / 2, mStateRadius + mStateCircleStroke,
              mStrokePaint);
            canvas.drawCircle(mCellWidth * (i + 1) + (circleSize), mCellHeight / 2, mStateRadius, paint);
        }
    }

    private void drawCircles(Canvas canvas, Paint paint, float animEndX) {
        LogUtils.LOGD(TAG, "drawCircles : " + animEndX + "  " + mCellWidth);
        float circleSize = 2 * (mStateCircleStroke + mStateRadius);
        float value = (animEndX) / (mCellWidth);
        //int state = Math.round(value);
        drawCircles(canvas, paint, 0, (int) value);
    }

    private void drawCurrentStateJoiningLine(Canvas canvas) {
        LogUtils.LOGD(TAG, "drawCurrentStateJoiningLine : ");
        if (mAnimateToCurrentProgressState) {
            animateToCurrentState(canvas);
        } else {
            drawLineToCurrentState(canvas);
        }
    }

    private void drawLineToCurrentState(Canvas canvas) {
        LogUtils.LOGD(TAG, "drawLineToCurrentState : ");
        canvas.drawLine(mStartCenterX, mCellHeight / 2, mEndCenterX, mCellHeight / 2, mForegroundPaint);
        mNextCellWidth = mCellWidth;
        stopAnimation();
    }

    private void animateToCurrentState(Canvas canvas) {
        LogUtils.LOGD(TAG, "animateToCurrentState : ");
        if (!mIsCurrentAnimStarted) {
            mAnimStartXPos = mStartCenterX;
            mAnimEndXPos = mAnimStartXPos;
            mIsCurrentAnimStarted = true;
        }

        if (mAnimEndXPos < mStartCenterX || mStartCenterX > mEndCenterX) {
            stopAnimation();
            enableAnimationToCurrentState(false);
            invalidate();
        } else if (mAnimEndXPos <= mEndCenterX) {
            canvas.drawLine(mStartCenterX, mCellHeight / 2, mEndCenterX, mCellHeight / 2, mBackgroundPaint);
            canvas.drawLine(mStartCenterX, mCellHeight / 2, mAnimEndXPos, mCellHeight / 2, mForegroundPaint);

            mAnimStartXPos = mAnimEndXPos;
        } else {
            canvas.drawLine(mStartCenterX, mCellHeight / 2, mEndCenterX, mCellHeight / 2, mForegroundPaint);
        }

        mNextCellWidth = mCellWidth;
    }

    // To initiate StartCenterX and  EndCenterX
    private void setAnimatorStartEndCenterX() {
        LogUtils.LOGD(TAG, "setAnimatorStartEndCenterX : ");
        if (mCurrentStateNumber > MIN_STATE_NUMBER && mCurrentStateNumber < MAX_STATE_NUMBER + 1) {
            mStartCenterX = 2 * (mStateCircleStroke + mStateRadius);
            mNextCellWidth = (mCurrentStateNumber - 1) * mCellWidth;
            mEndCenterX = mNextCellWidth;
        } else {
            resetStateAnimationData();
        }
    }

    private void resetStateAnimationData() {
        LogUtils.LOGD(TAG, "resetStateAnimationData : ");
        if (mStartCenterX > 0 || mStartCenterX < 0) mStartCenterX = 0;
        if (mEndCenterX > 0 || mEndCenterX < 0) mEndCenterX = 0;
        if (mAnimEndXPos > 0 || mAnimEndXPos < 0) mAnimEndXPos = 0;
        if (mIsCurrentAnimStarted) mIsCurrentAnimStarted = false;
    }

    public void startAnimator() {
        LogUtils.LOGD(TAG, "startAnimator : ");
        if (mAnimator == null) {
            mAnimator = new Animator();
            mAnimator.start();
        } else {
            mAnimator.resume();
        }
    }

    public void stopAnimation() {
        LogUtils.LOGD(TAG, "stopAnimation : ");
        if (mAnimator != null) {
            mAnimator.stop();
        }
    }

    private class Animator implements Runnable {
        private Scroller mScroller;
        private boolean mRestartAnimation = false;

        public Animator() {
            LogUtils.LOGD(TAG, "Animator : ");
            mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
        }

        public void run() {
            if (mAnimator != this) return;

            if (mRestartAnimation) {
                LogUtils.LOGD(TAG, "run : ");
                initializeScroller(mScroller);
                mRestartAnimation = false;
            }

            boolean scrollRemains = mScroller.computeScrollOffset();
            // If Scroll Remains true draw it again upto reach the scroll end
            redraw(scrollRemains);
        }

        private void redraw(boolean animate) {
            mAnimStartXPos = mAnimEndXPos;
            mAnimEndXPos = mScroller.getCurrY();

            if (animate) {
                invalidate();
                post(this);
            } else {
                stop();
                enableAnimationToCurrentState(false);
            }
        }

        public void resume() {
            mRestartAnimation = true;
            mAnimateToCurrentProgressState = true;
            postDelayed(this, mAnimStartDelay);
        }

        public void start() {
            mRestartAnimation = true;
            postDelayed(this, mAnimStartDelay);
        }

        public void stop() {
            removeCallbacks(this);
            if (mListener != null) mListener.onAnimationStop(mAnimEndXPos - ((mStateRadius + mStateCircleStroke)));
            //mAnimator = null;
        }
    }

    private float convertDpToPixel(float dp) {
        LogUtils.LOGD(TAG, "convertDpToPixel : ");
        final float scale = getResources().getDisplayMetrics().density;
        return dp * scale;
    }

    private float convertSpToPixel(float sp) {
        LogUtils.LOGD(TAG, "convertSpToPixel : ");
        final float scale = getResources().getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.LOGD(TAG, "onAttachedToWindow : ");
        //startAnimator();
    }

    @Override
    protected void onDetachedFromWindow() {
        stopAnimation();
        LogUtils.LOGD(TAG, "onDetachedFromWindow : ");
        super.onDetachedFromWindow();
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        LogUtils.LOGD(TAG, "setVisibility : ");

        //startAnimator();
    }

    public void setStateListener(StateProgressListener listener) {
        LogUtils.LOGD(TAG, "setStateListener : ");
        mListener = listener;
    }

    private float getEndXPos() {
        int startX = (int) Math.floor((mStateCircleStroke + mStateRadius));
        float endX = ((mCurrentStateNumber - 1) * mCellWidth);
        return endX + startX;
    }

    private void initializeScroller(Scroller mScroller) {
        // To find the next integer value if its Floating value
        // becz scroll takes only int value but our calculation is based on float value
        int startX = (int) mAnimEndXPos;
        float endX = ((mCurrentStateNumber - 1) * mCellWidth);
        int endCenterX = (int) Math.floor(endX);
        mScroller.startScroll(0, startX, 0, (int) endCenterX - startX, mAnimDuration);
    }
}