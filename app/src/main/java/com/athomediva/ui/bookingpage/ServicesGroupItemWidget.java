package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/05/17.
 */

public class ServicesGroupItemWidget extends RelativeLayout {
    private static final String TAG = ServicesGroupItemWidget.class.getSimpleName();

    @BindView(R.id.group_title) TextView mGroupView;
    @BindView(R.id.service_title) TextView mServiceTitle;
    @BindView(R.id.count_view) TextView mCountView;
    @BindView(R.id.amount) TextView mAmountView;

    private boolean mHeaderEnable;

    public ServicesGroupItemWidget(Context context) {
        super(context);
    }

    public ServicesGroupItemWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServicesGroupItemWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        initViews();
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
    }

    @DebugLog
    public void enableHeader(boolean value) {
        LogUtils.LOGD(TAG, "enableHeader : ");
        mHeaderEnable = value;
        if (mHeaderEnable) {
            mGroupView.setVisibility(View.VISIBLE);
        } else {
            mGroupView.setVisibility(View.GONE);
        }
    }

    @DebugLog
    public void updateViews(Services model) {
        LogUtils.LOGD(TAG, "updateViews : ");
        if (model != null) {
            mGroupView.setText(model.getGroupName());
            mCountView.setText(
              getResources().getString(R.string.booking_service_count_txt, String.valueOf(model.getQuantity())));
            mServiceTitle.setText(model.getName());
            mAmountView.setText(UiUtils.getFormattedAmount(getContext(), model.getPrice()));
        }
    }
}
