package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.WalletInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 11/05/17.
 */

public class WalletWidget extends RelativeLayout {
    private static final String TAG = WalletWidget.class.getSimpleName();

    @BindView(R.id.wallet_checkbox) CheckBox mWalletCheckBox;
    @BindView(R.id.wallet_label) TextView mWalletLabel;
    @BindView(R.id.wallet_discount_price) TextView mWalletDiscountAmt;
    @BindView(R.id.wallet_info) ImageView mWalletInfo;
    @BindView(R.id.wallet_error_msg) TextView mWalletError;

    private OnClickListener mInfoClickListener;
    private CompoundButton.OnCheckedChangeListener mCheckListener;

    public WalletWidget(Context context) {
        super(context);
    }

    public WalletWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WalletWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setInfoClickListener(OnClickListener listener) {
        LogUtils.LOGD(TAG, "setInfoClickListener : ");
        mInfoClickListener = listener;
        mWalletInfo.setOnClickListener(mInfoClickListener);
    }

    public void setCheckListener(CompoundButton.OnCheckedChangeListener changeListener) {
        LogUtils.LOGD(TAG, "setCheckListener : ");
        mCheckListener = changeListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");

        mWalletCheckBox.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            updateWallet(isChecked);
            if (mCheckListener != null) mCheckListener.onCheckedChanged(compoundButton, isChecked);
        });
        updateWallet(true);
    }

    public void updateView(double info, String color) {
        LogUtils.LOGD(TAG, "updateView : ");
        String str =
          getContext().getString(R.string.ahd_amt_minus, String.valueOf(UiUtils.getFormattedAmount(getContext(), info)));
        mWalletDiscountAmt.setText(str);
        if (!TextUtils.isEmpty(color)) {
            mWalletDiscountAmt.setTextColor(Color.parseColor(color));
        }
    }

    private void updateWallet(boolean apply) {
        LogUtils.LOGD(TAG, "updateWallet : ");
        mWalletCheckBox.setChecked(apply);
        if (!apply) {
            mWalletDiscountAmt.setVisibility(View.INVISIBLE);
        } else {
            mWalletDiscountAmt.setVisibility(View.VISIBLE);
        }
    }

    public void updateWalletError(String error) {
        LogUtils.LOGD(TAG, "updateWallet : ");
        UIHelper.hideViewIfEmptyTxt(mWalletError, error);
    }
}
