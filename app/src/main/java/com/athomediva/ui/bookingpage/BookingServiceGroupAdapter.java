package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.mycart.ServiceGroupComparator;
import hugo.weaving.DebugLog;
import java.util.Collections;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/05/17.
 */

public class BookingServiceGroupAdapter extends BaseAdapter {
    private static final String TAG = BookingServiceGroupAdapter.class.getSimpleName();

    private Context mContext;
    private List<Services> mList;
    private String mGroupName;

    @DebugLog
    public BookingServiceGroupAdapter(Context context, List<Services> list) {
        LogUtils.LOGD(TAG, "MyCartServiceGroupAdapter : ");
        mContext = context;
        mList = list;
        /*if(mList != null && mList.size() > 0) {
            Collections.sort(mList, new ServiceGroupComparator());
        }*/
    }

    @Override
    public Object getItem(int i) {
        LogUtils.LOGD(TAG, "getItem : ");
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @DebugLog
    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        LogUtils.LOGD(TAG, "getView : ");
        ViewHolder holder;
        Services model = mList.get(pos);

        // Make the Group name as null If First Child Draw again
        if (pos == 0) {
            mGroupName = null;
        }
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_booking_services_group_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (BookingServiceGroupAdapter.ViewHolder) view.getTag();
        }

        if (model != null) {
            if (!TextUtils.isEmpty(model.getGroupName())) {
                holder.mWidget.enableHeader(true);
            } else {
                holder.mWidget.enableHeader(false);
            }
            holder.mWidget.updateViews(model);
        }

        return view;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        LogUtils.LOGD(TAG, "getCount : " + mList.size());
        return mList.size();
    }

    class ViewHolder {
        ServicesGroupItemWidget mWidget;

        public ViewHolder(View itemView) {
            mWidget = (ServicesGroupItemWidget) itemView;
        }
    }
}
