package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import java.util.ArrayList;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 30/06/17.
 */

public class BookingFailureDescAdapter extends BaseAdapter {
    private static final String TAG = BookingFailureDescAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<BookingDescInfo> mDescList;
    private View.OnClickListener mScheduleClickListener;
    public BookingFailureDescAdapter(Context context,ArrayList<BookingDescInfo> descInfoList,View.OnClickListener listener){
        LogUtils.LOGD(TAG,"BookingFailureDescAdapter : ");
        mContext = context;
        mDescList = descInfoList;
        mScheduleClickListener = listener;

    }

    @Override
    public int getCount() {
        if(mDescList == null)
        return 0;
        return mDescList.size();
    }

    @Override
    public Object getItem(int i) {
        return mDescList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        Holder holder = null;
        BookingDescInfo info  = mDescList.get(pos);
        if(view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_booking_failure_details,null);
            holder = new Holder();
            holder.mTitle = (TextView)view.findViewById(R.id.title);
            holder.mDesc1 = (TextView)view.findViewById(R.id.desc1);
            holder.mDesc2 = (TextView)view.findViewById(R.id.desc2);
            holder.mSchedule = (TextView)view.findViewById(R.id.schedule_btn);
            view.setTag(holder);
        } else {
            holder = (Holder)view.getTag();
        }

        if(info != null) {
            UIHelper.hideViewIfEmptyTxt(holder.mTitle,info.getTitle());
            UIHelper.hideViewIfEmptyTxt(holder.mDesc1,info.getDescription1());
            UIHelper.hideViewIfEmptyTxt(holder.mDesc2,info.getDescription2());
            holder.mSchedule.setTag(info);
            holder.mSchedule.setOnClickListener(mScheduleClickListener);
        }

        return view;
    }

    private static class Holder {
        TextView mTitle;
        TextView mDesc1;
        TextView mDesc2;
        TextView mSchedule;
    }
}
