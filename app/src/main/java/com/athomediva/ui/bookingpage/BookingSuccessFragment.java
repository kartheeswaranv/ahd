package com.athomediva.ui.bookingpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.AHDApplication;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.data.models.remote.BookingExtraInfo;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingSuccessContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.w3c.dom.Text;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class BookingSuccessFragment extends BaseFragment<BookingSuccessContract.Presenter, BookingSuccessContract.View>
  implements BookingSuccessContract.View {
    public static final String FRAG_TAG = BookingSuccessFragment.class.getCanonicalName();
    private static final String TAG = LogUtils.makeLogTag(BookingSuccessFragment.class.getSimpleName());
    @BindView(R.id.title_txt) TextView mTitle;
    //@BindView(R.id.desc_txt) TextView mDesc;
    @BindView(R.id.booking_list) ExpandListView mBookingListView;
    @BindView(R.id.root_layout) ViewGroup mRootLayout;
    @BindView(R.id.home_btn) TextView mHomeBtn;
    @BindView(R.id.footer_info_list) ExpandListView mFooterListView;
    @BindView(R.id.header_info_list) ExpandListView mHeaderListView;
    @BindView(R.id.extra_info_list) ExpandListView mBookingExtraInfoList;
    @BindView(R.id.pay_now) TextView mPaynowBtn;
    @BindView(R.id.total_amt) TextView mTotalAmtView;
    @BindView(R.id.pre_payment_view) ViewGroup mPrePaymentView;

    @Inject BookingSuccessContract.Presenter mPresenter;

    public static BookingSuccessFragment newInstance(Bundle bundle) {
        BookingSuccessFragment fragment = new BookingSuccessFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.ahd_booking_success_fragment, container, false);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.BookingSuccessModule(getArguments()))
          .inject(this);
        bindView(this, view);
        AHDApplication.get(getActivity())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.BOOKING_CONFIRMED,
            GATrackerContext.Label.BOOKING_CONFIRM_PAGELOAD);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        initToolbar();
        mHomeBtn.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onHomeClick();
        });
        mPaynowBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "Paynow OnClick onClick : ");
            mPresenter.onPayNowClick();
        });

        mBookingListView.setExpanded(true);
        mHeaderListView.setExpanded(true);
        mFooterListView.setExpanded(true);
        mBookingExtraInfoList.setExpanded(true);
        mBookingListView.setOnItemClickListener((adapterView, view, i, l) -> {
            LogUtils.LOGD(TAG, "initializeView : ");
            ConfirmBookingInfo info = (ConfirmBookingInfo) adapterView.getItemAtPosition(i);
            mPresenter.onBookingItemClick(info);
        });
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");

        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.booking_success_page_title));
        ((FragmentContainerActivity) getActivity()).setNavigationIcon(R.drawable.ahd_ic_close);
        ((FragmentContainerActivity) getActivity()).setNavigationClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onHomeClick();
        });

        ((FragmentContainerActivity) getActivity()).setBackListener(() -> {
            LogUtils.LOGD(TAG, "onBackPress : ");
            mPresenter.onHomeClick();
        });
    }

    @DebugLog
    @Override
    public void updateTitle(String userName) {
        LogUtils.LOGD(TAG, "updateTitle : ");
        if (!TextUtils.isEmpty(userName)) {
            mTitle.setText(getString(R.string.booking_success_title, userName));
        }
    }

    @DebugLog
    @Override
    public void updateHeaderInfoList(ArrayList<BookingDescInfo> list) {
        LogUtils.LOGD(TAG, "updateDesc : ");
        if (list != null && !list.isEmpty()) {
            mHeaderListView.setVisibility(View.VISIBLE);
            mHeaderListView.setAdapter(new BookingHeaderDescAdapter(getContext(), list));
        } else {
            mHeaderListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateFooterInfoList(ArrayList<BookingDescInfo> list) {
        LogUtils.LOGD(TAG, "updateTimeDesc : ");
        if (list != null && !list.isEmpty()) {
            mFooterListView.setVisibility(View.VISIBLE);
            mFooterListView.setAdapter(new BookingHeaderDescAdapter(getContext(), list));
        } else {
            mFooterListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateBookingExtraInfoList(ArrayList<BookingExtraInfo> list) {
        LogUtils.LOGD(TAG, "updateBookingExtraInfoList: ");
        if (list != null && list.size() > 0) {
            mBookingExtraInfoList.setVisibility(View.VISIBLE);
            mBookingExtraInfoList.setAdapter(new BookingExtraInfoAdapter(getContext(), list));
        } else {
            mBookingExtraInfoList.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updateBookingsList(List<ConfirmBookingInfo> bookingList) {
        LogUtils.LOGD(TAG, "updateBookingsList : ");
        mBookingListView.setAdapter(new ConfirmBookingAdapter(getContext(), bookingList));
    }

    @Override
    public void updateTotalAmountView(String amount) {
        LogUtils.LOGD(TAG, "updateTotatlAmountView : ");
        if (TextUtils.isEmpty(amount)) {
            mPrePaymentView.setVisibility(View.GONE);
        } else {
            mTotalAmtView.setText(amount);
            mPrePaymentView.setVisibility(View.VISIBLE);
        }
    }
}
