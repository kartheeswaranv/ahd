package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 19/05/17.
 */

public class BookingStatusArrowView extends View {
    private static final String TAG = BookingStatusArrowView.class.getSimpleName();
    private int mWidth = 0;
    private float mStartX = 0;
    private int mColor;

    public BookingStatusArrowView(Context context) {
        super(context);
    }

    public BookingStatusArrowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs, 0);
    }

    public BookingStatusArrowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs, defStyleAttr);
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LogUtils.LOGD(TAG, "initAttrs : ");
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BookingStatusArrowView, defStyleAttr, 0);
        mColor = a.getColor(R.styleable.BookingStatusArrowView_indicator_color, mColor);
        mWidth = (int) a.getDimension(R.styleable.BookingStatusArrowView_indicator_width, 0);
        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        LogUtils.LOGD(TAG, "onDraw : ");
        drawTriangle(canvas, (int) mStartX, 0, mWidth);
    }

    public void setX(float startX) {
        LogUtils.LOGD(TAG, "setX : ");
        mStartX = startX;
        invalidate();
    }

    public void drawTriangle(Canvas canvas, int x, int y, int width) {
        LogUtils.LOGD(TAG, "drawTriangle : ");
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(mColor);

        int halfWidth = width;

        Path path = new Path();
        path.moveTo(x, y); // Top
        path.lineTo(x - halfWidth, y + halfWidth); // Bottom left
        path.lineTo(x + halfWidth, y + halfWidth); // Bottom right
        path.lineTo(x, y); // Back to Top
        path.close();

        canvas.drawPath(path, paint);
    }
}
