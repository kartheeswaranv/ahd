package com.athomediva.ui.bookingpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.data.models.remote.BookingExtraInfo;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingFailureContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 30/06/17.
 */

public class BookingFailureFragment extends BaseFragment<BookingFailureContract.Presenter, BookingFailureContract.View>
  implements BookingFailureContract.View {
    public static final String FRAG_TAG = BookingFailureFragment.class.getCanonicalName();
    private static final String TAG = LogUtils.makeLogTag(BookingFailureFragment.class.getSimpleName());
    @BindView(R.id.success_img) ImageView mImgIcon;
    @BindView(R.id.title_txt) TextView mTitle;
    @BindView(R.id.desc_txt) TextView mDesc;
    @BindView(R.id.booking_header_info_list) ExpandListView mBookingHeaderInfoListView;
    @BindView(R.id.booking_header_info) RelativeLayout mBookingInfoView;
    @BindView(R.id.booking_list) ExpandListView mBookingListView;
    @BindView(R.id.booking_failure_list) ExpandListView mBookingFailureListView;
    @BindView(R.id.extra_info_list) ExpandListView mBookingExtraInfoList;
    @BindView(R.id.my_bookings) TextView mMyBookingCta;
    @BindView(R.id.pay_now) TextView mPaynowBtn;
    @BindView(R.id.total_amt) TextView mTotalAmtView;
    @BindView(R.id.pre_payment_view) ViewGroup mPrePaymentView;

    @Inject BookingFailureContract.Presenter mPresenter;
    private View.OnClickListener mScheduleClickListener;

    public static BookingFailureFragment newInstance(Bundle bundle) {
        BookingFailureFragment fragment = new BookingFailureFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = inflater.inflate(R.layout.ahd_booking_failure_fragment, container, false);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.BookingFailureModule(getArguments()))
          .inject(this);
        bindView(this, view);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        initToolbar();
        mBookingListView.setExpanded(true);
        mBookingFailureListView.setExpanded(true);
        mBookingHeaderInfoListView.setExpanded(true);
        mBookingListView.setOnItemClickListener((adapterView, view, i, l) -> {
            LogUtils.LOGD(TAG, "initializeView : ");
            ConfirmBookingInfo info = (ConfirmBookingInfo) adapterView.getItemAtPosition(i);
            mPresenter.onBookingItemClick(info);
        });
        mPaynowBtn.setOnClickListener(v -> {
            LogUtils.LOGD(TAG, "onClick : Paynow ");
            mPresenter.onPayNowClick();
        });

        mScheduleClickListener = view -> {
            LogUtils.LOGD(TAG, " On Reschedule onClick : ");
            BookingDescInfo info = (BookingDescInfo) view.getTag();
            mPresenter.onRescheduleClick(info);
        };
        mMyBookingCta.setOnClickListener(view -> mPresenter.onHomeClick());
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");

        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.booking_confirmation));
        ((FragmentContainerActivity) getActivity()).setNavigationIcon(R.drawable.ahd_ic_close);
        ((FragmentContainerActivity) getActivity()).setNavigationClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onHomeClick();
        });

        ((FragmentContainerActivity) getActivity()).setBackListener(() -> {
            LogUtils.LOGD(TAG, "onBackPress : ");
            mPresenter.onHomeClick();
        });
    }

    @DebugLog
    @Override
    public void updateTitle(String userName) {
        LogUtils.LOGD(TAG, "updateTitle : ");
        if (!TextUtils.isEmpty(userName)) {
            mTitle.setText(getString(R.string.booking_failed_title, userName));
        }
    }

    @DebugLog
    @Override
    public void updateDesc(String desc, boolean isAllBookingsFailed) {
        LogUtils.LOGD(TAG, "updateDesc : ");
        UIHelper.hideViewIfEmptyTxt(mDesc, desc);
        if (isAllBookingsFailed) {
            mImgIcon.setImageResource(R.drawable.ahd_ic_cross_circle);
        } else {
            mImgIcon.setImageResource(R.drawable.ic_tick);
        }
    }

    @DebugLog
    @Override
    public void updateBookingHeaderInfoList(ArrayList<BookingDescInfo> list) {
        LogUtils.LOGD(TAG, "updateHeaderInfoList : ");
        if (list != null && !list.isEmpty()) {
            mBookingInfoView.setVisibility(View.VISIBLE);
            mBookingHeaderInfoListView.setVisibility(View.VISIBLE);
            mBookingHeaderInfoListView.setAdapter(new BookingHeaderDescAdapter(getContext(), list));
        } else {
            mBookingInfoView.setVisibility(View.GONE);
            mBookingHeaderInfoListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateBookingExtraInfoList(ArrayList<BookingExtraInfo> list) {
        LogUtils.LOGD(TAG, "updateBookingExtraInfoList: ");
        if (list != null && list.size() > 0) {
            mBookingExtraInfoList.setVisibility(View.VISIBLE);
            mBookingExtraInfoList.setAdapter(new BookingExtraInfoAdapter(getContext(), list));
        } else {
            mBookingExtraInfoList.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void updateBookingsList(ArrayList<ConfirmBookingInfo> bookingList) {
        LogUtils.LOGD(TAG, "updateBookingsList : ");
        mBookingListView.setAdapter(new ConfirmBookingAdapter(getContext(), bookingList));
    }

    public void updateBookingFailureInfo(ArrayList<BookingDescInfo> failureList) {
        LogUtils.LOGD(TAG, "updateBookingFailureInfo : ");
        if (failureList != null && !failureList.isEmpty()) {
            mBookingFailureListView.setVisibility(View.VISIBLE);
            mBookingFailureListView.setAdapter(
              new BookingFailureDescAdapter(getContext(), failureList, mScheduleClickListener));
        } else {
            mBookingFailureListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateTotalAmountView(String amount) {
        LogUtils.LOGD(TAG, "updateTotatlAmountView : ");
        if (TextUtils.isEmpty(amount)) {
            mPrePaymentView.setVisibility(View.GONE);
        } else {
            mTotalAmtView.setText(amount);
            mPrePaymentView.setVisibility(View.VISIBLE);
        }
    }
}

