package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.PaymentPendingBookingDetails;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingBookingAdapter extends BaseAdapter {
    private static final String TAG = ConfirmBookingAdapter.class.getSimpleName();

    private Context mContext;
    private List<PaymentPendingBookingDetails> mList;

    public PaymentPendingBookingAdapter(Context context, List<PaymentPendingBookingDetails> list) {
        LogUtils.LOGD(TAG, "ConfirmBookingAdapter : ");
        mContext = context;
        mList = list;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        LogUtils.LOGD(TAG, "getView : ");
        PaymentPendingBookingDetails info = mList.get(pos);
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_pp_booking_item, viewGroup, false);
            holder.mBookingId = (TextView) view.findViewById(R.id.booking_id_value);
            holder.mScheduleDate = (TextView) view.findViewById(R.id.schedule_date_value);
            holder.mBucketName = (TextView) view.findViewById(R.id.title);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (info != null) {
            holder.mBucketName.setText(info.getTitle());
            if (info.getBookingData() != null) {
                holder.mBookingId.setText(info.getBookingData().getBookingId());
                holder.mScheduleDate.setText(info.getBookingData().getCompletedOn());
            }
        }

        return view;
    }

    private static class ViewHolder {
        TextView mBookingId;
        TextView mScheduleDate;
        TextView mBucketName;
    }
}
