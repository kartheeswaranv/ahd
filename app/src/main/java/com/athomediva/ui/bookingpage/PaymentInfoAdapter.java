package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.PaymentInfo;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 18/05/17.
 */

public class PaymentInfoAdapter extends BaseAdapter {
    private static final String TAG = PaymentInfoAdapter.class.getSimpleName();
    private Context mContext;
    private List<PaymentInfo> mBillingList;

    public PaymentInfoAdapter(Context context, List<PaymentInfo> list) {
        LogUtils.LOGD(TAG, "PaymentInfoAdapter : ");
        mContext = context;
        mBillingList = list;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mBillingList == null) return 0;
        return mBillingList.size();
    }

    @Override
    public Object getItem(int i) {
        return mBillingList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LogUtils.LOGD(TAG, "getView : ");
        ViewHolder mHolder;
        PaymentInfo billInfo = (PaymentInfo) getItem(position);
        if (convertView == null) {
            mHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.ahd_payment_info_item, null);

            mHolder.mName = (TextView) convertView.findViewById(R.id.name);

            mHolder.mPrice = (TextView) convertView.findViewById(R.id.price);
            if (UiUtils.isValidColorCode(billInfo.getColor())) {
                mHolder.mPrice.setTextColor(Color.parseColor(billInfo.getColor()));
            }
            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }

        if (billInfo != null) {
            mHolder.mName.setText(UiUtils.getHtmlFormatText(billInfo.getTitle()));
            mHolder.mPrice.setText(UiUtils.getFormattedAmountByString(mContext, billInfo.getAmt()));
        }
        return convertView;
    }

    private class ViewHolder {
        TextView mName;
        TextView mPrice;
    }
}