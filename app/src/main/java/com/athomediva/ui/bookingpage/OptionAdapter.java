package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.data.models.local.OptionModel;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 28/05/17.
 */

public class OptionAdapter extends ArrayAdapter<OptionModel> {
    public static final String TAG = OptionAdapter.class.getSimpleName();

    private Context mContext;
    private List<OptionModel> mList;
    private int mResId;
    private LayoutInflater mInflater;
    private InputMethodManager imm;

    public OptionAdapter(Context context, int resource, List<OptionModel> objects) {
        super(context, resource, objects);
        LogUtils.LOGD(TAG, "OptionsAdapter");
        mInflater = LayoutInflater.from(context);
        mList = objects;
        mContext = context;
        mResId = resource;
        imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public int getCount() {
        if (mList == null) return 0;
        return mList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LogUtils.LOGD(TAG, "getView");
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(mResId, null);
            holder.button = (RadioButton) convertView.findViewById(R.id.option);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        updateView(holder, mList.get(position));

        return convertView;
    }

    private void updateView(final ViewHolder holder, OptionModel model) {
        LogUtils.LOGD(TAG, "updateView " + model);
        if (model != null && holder != null) {
            holder.button.setChecked(model.isSelected());
            holder.title.setText(model.getTitle());

            if(model.isSelected()) {
                holder.title.setTextColor(ContextCompat.getColor(getContext(),R.color.ahd_tuna));
            } else {
                holder.title.setTextColor(ContextCompat.getColor(getContext(),R.color.tint_black_light));
            }
        }
    }

    class ViewHolder {
        RadioButton button;
        TextView title;
    }
}

