package com.athomediva.ui.bookingpage;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.ExpandListView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.DataProcessor;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.Coupon;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.WalletDetails;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.ReviewOrderContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.mycart.MyCartAddressWidget;
import com.athomediva.ui.offers.InstantCouponHeaderWidget;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 04/05/17.
 */

public class ReviewOrderActivity extends BaseActivity implements ReviewOrderContract.View {
    private static final String TAG = LogUtils.makeLogTag(ReviewOrderActivity.class.getSimpleName());

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.address) MyCartAddressWidget mAddressWidget;
    @BindView(R.id.bucket_list) ExpandListView mBucketListView;
    @BindView(R.id.subtotal_value) TextView mSubTotal;
    @BindView(R.id.total_value) TextView mTotal;
    @BindView(R.id.coupon_view) CouponWidget mCouponWidget;
    @BindView(R.id.wallet_view) WalletWidget mWalletWidget;
    @BindView(R.id.cart_edit) TextView mCartEdit;
    @BindView(R.id.specific_req_view) SpecificReqWidget mSpecificReq;
    @BindView(R.id.proceed) TextView mProceed;
    @BindView(R.id.payment_pending_amt) TextView mPaymentPendingAmt;
    @BindView(R.id.payment_pending_msg) TextView mPaymentPendingMsg;
    @BindView(R.id.payment_pending_view) ViewGroup mPaymentPendingView;
    @BindView(R.id.cashback_msg_layout) ViewGroup mCashbackLayout;
    @BindView(R.id.cashback_msg) TextViewCustom mCashbackMsg;
    @BindView(R.id.payment_divider) View mPaymentDivider;
    @BindView(R.id.scroll_view) ScrollView mScrollView;
    @BindView(R.id.instant_coupon_header_view) InstantCouponHeaderWidget mCouponHeaderView;
    @Inject ReviewOrderContract.Presenter mPresenter;

    private InfoDialog mInfoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_review_order_activity);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.ReviewOrderModule(getIntent().getExtras())).inject(this);
        mPresenter.attachView(this);
        initViews();
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.REVIEW_ORDER,
            GATrackerContext.Label.REVIEW_ORDER_PAGELOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
        mBucketListView.setExpanded(true);
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.title);
        title.setText(getString(R.string.review_order_title));
        TextView subTitle = (TextView) mToolbar.findViewById(R.id.sub_title);
        subTitle.setVisibility(View.GONE);
        mCartEdit.setOnClickListener(view -> mPresenter.onCartEditClick());
        mWalletWidget.setInfoClickListener(view -> mPresenter.onWalletInfoClick());

        mProceed.setOnClickListener(view -> mPresenter.onProceedClick());
        mAddressWidget.setListeners(null, view -> mPresenter.onAddressChangeClick());
        mAddressWidget.enableOptions(false);
        mCouponWidget.setCouponApplyListener(new IApplyCoupon() {
            @Override
            public void onCouponApply(String couponCode) {
                LogUtils.LOGD(TAG, "onCouponApply : ");
                mPresenter.onCouponApplyClick(couponCode);
            }

            @Override
            public void onCouponRemove() {
                LogUtils.LOGD(TAG, "onCouponRemove : ");
                mPresenter.onCouponClearClick();
            }
        });

        mCouponWidget.setFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                UIHelper.scrollToViewPosition(mScrollView, mCouponWidget);
            }
        });
    }

    @DebugLog
    @Override
    public void updateCouponView(Coupon coupon) {
        LogUtils.LOGD(TAG, "updateCoupons : ");
        mCouponWidget.updateView(coupon);
    }

    @DebugLog
    @Override
    public void setCouponError(String msg) {
        LogUtils.LOGD(TAG, "setCouponError : ");
        mCouponWidget.setCouponError(msg);
    }

    @DebugLog
    @Override
    public void updateWalletView(double wallet, String color) {
        LogUtils.LOGD(TAG, "updateWallets : ");
        if (wallet > 0) {
            mWalletWidget.setVisibility(View.VISIBLE);
            mWalletWidget.updateView(wallet, color);
        } else {
            mWalletWidget.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateWalletError(String error) {
        LogUtils.LOGD(TAG, "updateWalletErro : ");
        mWalletWidget.updateWalletError(error);
    }

    @DebugLog
    @Override
    public void updateServicesView(List<CartBucketItem> list) {
        LogUtils.LOGD(TAG, "updateServices : ");
        BucketAdapter mAdapter = new BucketAdapter(this, list);
        mBucketListView.setAdapter(mAdapter);
    }

    @DebugLog
    public void updatePaymentInfoView(String total, String subTotal) {
        LogUtils.LOGD(TAG, "updatePaymentInfo : ");
        mTotal.setText(UiUtils.getFormattedAmountByString(this, total));
        mSubTotal.setText(UiUtils.getFormattedAmountByString(this, subTotal));
    }

    @Override
    public void updatePaymentPendingView(boolean pendingStatus, double value, String msg) {
        LogUtils.LOGD(TAG, "updatePaymentPendingView : ");
        if (value > 0) {
            mPaymentPendingView.setVisibility(View.VISIBLE);
            mPaymentPendingAmt.setText(UiUtils.getFormattedAmount(this, value));
            UIHelper.hideViewIfEmptyTxt(mPaymentPendingMsg, msg);
        } else {
            mPaymentPendingView.setVisibility(View.GONE);
            LogUtils.LOGD(TAG, "updatePaymentPendingView : Payment Pending value is 0 ");
        }
        enableProceedView(!pendingStatus);
    }

    @DebugLog
    @Override
    public void updateAddressView(Address address) {
        LogUtils.LOGD(TAG, "updateAddressView : ");
        if (address != null) {
            mAddressWidget.updateLocation(address);
        }
    }

    @DebugLog
    public void showWalletInfoDialog(WalletDetails data) {
        LogUtils.LOGD(TAG, "showWalletInfo : ");

        if (data == null) {
            LogUtils.LOGD(TAG, "showWalletInfoDialog : Wallet Info is empty");
            return;
        }
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        View view = LayoutInflater.from(this).inflate(R.layout.ahd_wallet_info_dailog, null);
        if (view != null && data.getWallet() != null) {
            WalletListInfoView walletInfoView = (WalletListInfoView) view;
            walletInfoView.setInfoList(data.getWallet(), String.valueOf(data.getTotalWalletAmt()));
        }

        InfoDialogModel model = new InfoDialogModel(getString(R.string.applied_wallet), view);
        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view1 -> mInfoDialog.dismiss());
    }

    @DebugLog
    public void showRescheduleDialog(String message, boolean clearCart) {
        LogUtils.LOGD(TAG, "showRescheduleDialog : ");

        if (TextUtils.isEmpty(message)) {
            LogUtils.LOGD(TAG, "showRescheduleDialog :Message is empty");
            return;
        }
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(getString(R.string.booking_failed), message);
        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
        mInfoDialog.setNegativeButton(getString(R.string.cancel_permission_btn), view1 -> {
            mInfoDialog.dismiss();
            mPresenter.onRescheduleCancel(clearCart);
        });
        mInfoDialog.setPositiveButton(getString(R.string.booking_reschedule_cta), view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mInfoDialog.dismiss();
            mPresenter.onRecheduleConfirm();
        });
    }

    @DebugLog
    public void enableProceedView(boolean enable) {
        LogUtils.LOGD(TAG, "enableProceedView : ");
        mProceed.setEnabled(enable);
        if (enable) {
            mProceed.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else {
            mProceed.setBackgroundColor(ContextCompat.getColor(this, R.color.ahd_grey_dark));
        }
    }

    @Override
    public void updateCouponCashbackView(String cashback) {
        LogUtils.LOGD(TAG, "updateCouponCashbackView : ");
        if (TextUtils.isEmpty(cashback)) {
            mCashbackLayout.setVisibility(View.GONE);
            mPaymentDivider.setVisibility(View.VISIBLE);
        } else {
            mPaymentDivider.setVisibility(View.GONE);
            mCashbackLayout.setVisibility(View.VISIBLE);
            mCashbackMsg.setText(UiUtils.getHtmlFormatText(cashback));
        }
    }

    @Override
    public void updateInstantCouponView(InstantCoupon coupon, boolean disableShadow) {
        LogUtils.LOGD(TAG, "updateInstantCouponView: ");
        if (coupon != null) {
            mCouponHeaderView.setVisibility(View.VISIBLE);
            mCouponHeaderView.setDisableShadow(disableShadow);
            /*String amount = UiUtils.getFormattedAmount(this, coupon.getAmount());
            String percentage = coupon.getPercentage() + "%";
            String instant_coupon_msg = getString(R.string.instant_offer_text);*/
            mCouponHeaderView.setTag(coupon);
            String msg = DataProcessor.getCouponMessage(this, coupon);
            mCouponHeaderView.updateView(msg);
            mCouponHeaderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.LOGD(TAG, "onClick: Instant CouponClick");
                    InstantCoupon instantCoupon = (InstantCoupon) v.getTag();
                    mPresenter.onInstantCouponClick(instantCoupon);
                }
            });
        } else {
            mCouponHeaderView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy : ");
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        super.onDestroy();
    }
}
