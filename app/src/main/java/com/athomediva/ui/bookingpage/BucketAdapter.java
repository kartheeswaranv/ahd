package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/05/17.
 */

public class BucketAdapter extends BaseAdapter {
    private static final String TAG = BucketAdapter.class.getSimpleName();

    private Context mContext;
    private List<CartBucketItem> mList;

    public BucketAdapter(Context context, List<CartBucketItem> list) {
        LogUtils.LOGD(TAG, "BucketAdapter : ");
        mContext = context;
        mList = list;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LogUtils.LOGD(TAG, "getView : ");
        BucketAdapter.ViewHolder holder;
        CartBucketItem model = mList.get(i);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_booking_bucket_item, viewGroup, false);
            holder = new BucketAdapter.ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (BucketAdapter.ViewHolder) view.getTag();
        }

        if (model != null) {

            String date = mContext.getString(R.string.booking_service_count_txt, model.getScheduledDateTime());
            holder.mHeader.setText(model.getTitle() + date);
            holder.mListView.setAdapter(
              new BookingServiceGroupAdapter(mContext, UiUtils.getAllServices(model.getServicesGroupList())));
        }

        return view;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    public class ViewHolder {
        TextView mHeader;
        ExpandListView mListView;

        public ViewHolder(View itemView) {
            mHeader = (TextView) itemView.findViewById(R.id.header_title);
            mListView = (ExpandListView) itemView.findViewById(R.id.services_list);
            mListView.setExpanded(true);
        }
    }
}
