package com.athomediva.ui.bookingpage;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.local.StatusModel;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 19/05/17.
 */

public class BookingStatusView extends RelativeLayout {
    private static final String TAG = BookingStatusView.class.getSimpleName();

    @BindView(R.id.status_progress) BookingStatusProgress mStateProgressView;
    @BindView(R.id.status_arrow) BookingStatusArrowView mBookingStatusArrowView;
    @BindView(R.id.status_text) TextView mStatusTextView;
    @BindView(R.id.status_text_title) TextView mStatusTitle;
    @BindView(R.id.expand_layout) RelativeLayout mStatusLayout;
    @BindView(R.id.rate_service) TextView mRateService;
    @BindView(R.id.pay_online) TextView mPayonline;
    @BindView(R.id.call_now) TextView mCallNow;
    @BindView(R.id.rated_layout) ViewGroup mRatedLayout;
    @BindView(R.id.rated_text) TextView mRatedTxtView;

    @BindView(R.id.cta_layout) ViewGroup mCTAView;

    private StatusModel mStatus;

    private ValueAnimator mExpandAnimator;

    public BookingStatusView(Context context) {
        super(context);
    }

    public BookingStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BookingStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        mStateProgressView.setStateListener(animationStopX -> {
            mBookingStatusArrowView.setX(animationStopX);
            // To Animate the Status Layout
            if (mStatus != null) {
                enableArrowView(mStatus.isEnableArrow());
                if (mStatusLayout.getVisibility() == View.GONE) {
                    mExpandAnimator =
                      AnimationHelper.expandAnimation(mStatusLayout, (int) (mStatus.getDuration() / 2), null);
                }
            }
        });
    }

    public void setListener(OnClickListener rateListener, OnClickListener payonlineListener) {
        LogUtils.LOGD(TAG, "setStatusListener : ");
        if (mRateService != null) {
            mRateService.setOnClickListener(rateListener);
            mPayonline.setOnClickListener(payonlineListener);
        }
    }

    public void setCallListener(OnClickListener callListener) {
        LogUtils.LOGD(TAG, "setCallListener : ");
        if (mCallNow != null) {
            mCallNow.setOnClickListener(callListener);
        }
    }

    @DebugLog
    public void updateStatus(StatusModel status) {
        LogUtils.LOGD(TAG, "updateStatus : ");
        // Animate the progress and Expand animation if it is updated
        boolean isProgressStepUpdated = true;
        if (mStatus != null && (mStatus.getStatus() == status.getStatus())) {
            isProgressStepUpdated = false;
        }
        mStatus = status;

        if (mStatus.getStatus() != 0 && mStatus.getStatus() <= mStatus.getMaxStatus()) {
            mStateProgressView.setVisibility(View.VISIBLE);
            mStateProgressView.setMaxStateNumber(mStatus.getMaxStatus());
            mStateProgressView.setCurrentStateNumber(mStatus.getStatus());
            mStateProgressView.setAnimationDuration((int) mStatus.getDuration());
            if (isProgressStepUpdated) {
                enableAnimation(mStatus.isAnimationEnable());
            }
        } else {
            mStateProgressView.setVisibility(View.GONE);
        }
        updateStatusText(mStatus.getMessage(), mStatus.getMsgGravity(), mStatus.getStatusTxtColor());

        if (!status.isRated() && !status.isPayOnlineEnable() && !status.isCallEnable()) {
            mCTAView.setVisibility(View.GONE);
        } else {
            mCTAView.setVisibility(View.VISIBLE);
        }
        if (status.isRated()) {
            mRateService.setVisibility(View.VISIBLE);
        } else {
            mRateService.setVisibility(View.GONE);
        }

        if (status.isPayOnlineEnable()) {
            mPayonline.setVisibility(View.VISIBLE);
        } else {
            mPayonline.setVisibility(View.GONE);
        }

        if (status.isCallEnable()) {
            mCallNow.setVisibility(View.VISIBLE);
        } else {
            mCallNow.setVisibility(View.GONE);
        }
        enableRatedView(status.getRating());
        UIHelper.hideViewIfEmptyTxt(mStatusTitle, status.getTitleText());
        // If Animation is disable we need to redraw the view becz height will be differ
        // after hiding rate service or payonline
        UIHelper.resetViewHeightToWrapContent(mStatusLayout);
    }

    @DebugLog
    public void enableAnimation(boolean enable) {
        LogUtils.LOGD(TAG, "enableAnimation : ");
        if (enable) {
            mStatusLayout.setVisibility(View.GONE);
            mStateProgressView.startAnimator();
        } else {
            mStatusLayout.setVisibility(View.VISIBLE);
        }
    }

    @DebugLog
    public void enableArrowView(boolean enable) {
        LogUtils.LOGD(TAG, "enableArrowView : ");
        if (enable) {
            mBookingStatusArrowView.setVisibility(View.VISIBLE);
        } else {
            mBookingStatusArrowView.setVisibility(View.INVISIBLE);
        }
    }

    @DebugLog
    public void updateStatusText(String msg, int gravity, String color) {
        LogUtils.LOGD(TAG, "updateStatusText : ");
        if (!TextUtils.isEmpty(msg)) {
            mStatusTextView.setText(msg);
        }

        if (UiUtils.isValidColorCode(color)) {
            mStatusTextView.setTextColor(Color.parseColor(color));
        }

        if (gravity != 0) {
            mStatusTextView.setGravity(gravity);
        } else {
            mStatusTextView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        }
    }

    public void enableRatedView(float rating) {
        LogUtils.LOGD(TAG, "enableRatedView : ");
        if (rating > 0) {
            mRatedLayout.setVisibility(View.VISIBLE);
            mRatedTxtView.setText(getContext().getString(R.string.stylist_rate_txt, String.valueOf(rating)));
        } else {
            mRatedLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        LogUtils.LOGD(TAG, "onDetachedFromWindow : ");
        if (mExpandAnimator != null) {
            mExpandAnimator.cancel();
        }
        super.onDetachedFromWindow();
    }
}
