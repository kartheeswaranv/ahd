package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.WalletInfo;
import com.athomediva.helper.UIHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 14/05/17.
 */

public class WalletInfoAdapter extends ArrayAdapter<WalletInfo> {
    private static final String TAG = WalletInfoAdapter.class.getSimpleName();

    private Context mContext;
    private int mResId;
    private List<WalletInfo> mList;

    public WalletInfoAdapter(Context context, int resource, List<WalletInfo> objects) {
        super(context, resource);
        LogUtils.LOGD(TAG, "WalletAdapter : ");
        mContext = context;
        mResId = resource;
        mList = objects;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    @Nullable
    @Override
    public WalletInfo getItem(int position) {
        LogUtils.LOGD(TAG, "getItem : ");
        if (mList != null) {
            return mList.get(position);
        } else {
            return null;
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LogUtils.LOGD(TAG, "getView : ");
        WalletInfo info = getItem(position);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(mResId, null);
        }

        TextView walletName = (TextView) view.findViewById(R.id.wallet_name);
        TextView walletAmt = (TextView) view.findViewById(R.id.wallet_amount);
        if (info != null) {
            walletName.setText(info.getName());
            walletAmt.setText(UiUtils.getFormattedAmount(mContext, info.getTotalAmount()));
        } else {
            LogUtils.LOGD(TAG, "getView : WalletInfo Empty");
        }
        return view;
    }
}
