package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.data.models.local.OptionModel;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.CancelBookingContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.FragmentContainerActivity;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 28/05/17.
 */

public class BookingCancelFragment extends BaseFragment<CancelBookingContract.Presenter, CancelBookingContract.View>
  implements CancelBookingContract.View {
    private static final String TAG = LogUtils.makeLogTag(BookingCancelFragment.class.getSimpleName());
    public static final String FRAG_TAG = BookingCancelFragment.class.getCanonicalName();

    @Inject CancelBookingContract.Presenter mPresenter;

    @BindView(R.id.list) ListView mListView;
    @BindView(R.id.cancel_booking) TextView mCancelBooking;

    private OptionAdapter mAdapter;
    private View mEditOptionView;

    public static BookingCancelFragment newInstance(Bundle bundle) {
        BookingCancelFragment fragment = new BookingCancelFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ahd_cancel_booking_frag, container, false);
        LogUtils.LOGD(TAG, "onCreateView : ");

        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.CancelBookingModule(getArguments()))
          .inject(this);
        bindView(this, view);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        initToolbar();
        mCancelBooking.setOnClickListener(view -> mPresenter.onCancelClick());

        mListView.setOnItemClickListener((adapterView, view, i, l) -> mPresenter.onOptionItemClick(i));

        mEditOptionView = LayoutInflater.from(getContext()).inflate(R.layout.ahd_cancel_option_footer_view, null);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        ((FragmentContainerActivity) getActivity()).setTitle(getString(R.string.cancel_title));
        ((FragmentContainerActivity) getActivity()).setNavigationIcon(R.drawable.ahd_ic_close);
    }

    @Override
    public void updateCTAView(boolean enable) {
        LogUtils.LOGD(TAG, "updateCTAView : ");
        if (mCancelBooking != null) {
            mCancelBooking.setClickable(enable);
            if (enable) {
                mCancelBooking.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            } else {
                mCancelBooking.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.ahd_black_light));
            }
        }
    }

    @Override
    public void refreshAdapterView() {
        LogUtils.LOGD(TAG, "refreshAdapterView : ");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateOptionsView(List<OptionModel> list) {
        LogUtils.LOGD(TAG, "updateOptionsView : ");
        if (list != null && list.size() > 0) {
            mAdapter = new OptionAdapter(getContext(), R.layout.ahd_cancel_option_item, list);
            mListView.setAdapter(mAdapter);
        }
    }

    @DebugLog
    @Override
    public void showEditOptionView(OptionModel model) {
        LogUtils.LOGD(TAG, "showEditOption : ");
        if (mEditOptionView != null) {
            mListView.removeFooterView(mEditOptionView);
            TextInputLayout input = (TextInputLayout) mEditOptionView.findViewById(R.id.title_editor);
            input.getEditText().addTextChangedListener(new TxtWatcher(model));
            input.setHint(model.getTitle());
            // Clear Input if already is there
            input.getEditText().setText("");
            input.requestFocus();
            mListView.addFooterView(mEditOptionView);
            showKeyBoard();
        }
    }

    @Override
    public void hideEditOptionView() {
        LogUtils.LOGD(TAG, "hideEditOption : ");
        if (mListView != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mEditOptionView.getWindowToken(), 0);
            mListView.removeFooterView(mEditOptionView);
            mListView.invalidate();
        }
    }

    public void showKeyBoard() {
        LogUtils.LOGD(TAG, "showKeyBoard : ");
        if (mListView != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mEditOptionView, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private class TxtWatcher implements TextWatcher {
        private OptionModel mModel;

        public TxtWatcher(OptionModel model) {
            mModel = model;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s != null && !TextUtils.isEmpty(s.toString())) {
                mModel.setText(s.toString());
            }
        }
    }
}
