package com.athomediva.ui.bookingpage;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.Coupon;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 11/05/17.
 */
public class CouponWidget extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = CouponWidget.class.getSimpleName();
    private static final int COUPON_CODE_INPUT_MAXLENGTH = 15;

    @BindView(R.id.coupon_checkbox) CheckBox mCheckBox;
    @BindView(R.id.coupon_input_field) EditText mEditText;
    @BindView(R.id.have_coupon_label) TextView mHaveCouponLabel;
    @BindView(R.id.apply_btn) TextView mApplyBtn;
    @BindView(R.id.coupon_input_container) RelativeLayout mCouponInputContainer;
    @BindView(R.id.coupon_error_msg) TextView mErrorLabel;
    @BindView(R.id.coupon_applied_container) RelativeLayout mCouponAppliedContainer;
    @BindView(R.id.coupon_price_discount) TextView mCouponDiscount;
    @BindView(R.id.coupon_applied_label) TextView mCouponAppliedLabel;
    @BindView(R.id.coupon_edit) ImageView mCouponEdit;
    @BindView(R.id.coupon_edit_container) LinearLayout mCouponEditContainer;
    @BindView(R.id.line_separator) View mLineSeparator;

    private IApplyCoupon iApplyCoupon;
    private Coupon mCoupon;

    private boolean mIsAutoApply;

    public CouponWidget(Context context) {
        super(context);
    }

    public CouponWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CouponWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        initView();
    }

    public void setCouponApplyListener(IApplyCoupon listener) {
        LogUtils.LOGD(TAG, "setCouponApplyListener : ");
        iApplyCoupon = listener;
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        Drawable drawable = UiUtils.getTintDrawable(getContext(),R.drawable.ahd_ic_edit,ContextCompat.getColor(getContext(),R.color.colorPrimary));
        if(drawable != null) {
            mCouponEdit.setImageDrawable(drawable);
        }

        mApplyBtn.setOnClickListener(this);
        mCouponEditContainer.setOnClickListener(this);
        mHaveCouponLabel.setOnClickListener(this);

        mEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(COUPON_CODE_INPUT_MAXLENGTH) });

        mCheckBox.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            updateViewOnChecked(isChecked);
            if (!isChecked && iApplyCoupon != null) {
                iApplyCoupon.onCouponRemove();
            }
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    mApplyBtn.setVisibility(VISIBLE);
                } else {
                    mApplyBtn.setVisibility(GONE);
                }
                mLineSeparator.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mErrorLabel.setVisibility(GONE);
            }
        });
    }

    public void setFocusChangeListener(OnFocusChangeListener listener) {
        mEditText.setOnFocusChangeListener(listener);
    }

    private void updateViewOnChecked(boolean checked) {
        LogUtils.LOGD(TAG, "updateViewOnChecked : ");
        if (mCoupon != null && checked) {
            LogUtils.LOGD(TAG, "updateViewOnChecked :: RETURN");
            return;
        }
        if (checked) {
            mHaveCouponLabel.setVisibility(GONE);
            mCouponInputContainer.setVisibility(VISIBLE);
            mLineSeparator.setVisibility(VISIBLE);
            showSoftKeyBoard();
        } else {
            mHaveCouponLabel.setVisibility(VISIBLE);
            mCouponInputContainer.setVisibility(GONE);
            mLineSeparator.setVisibility(GONE);
            mCouponAppliedContainer.setVisibility(GONE);
            mEditText.setText("");
            mCoupon = null;
        }
    }

    private void updateView() {
        LogUtils.LOGD(TAG, "updateView : ");
        mCheckBox.setChecked(true);
        mHaveCouponLabel.setVisibility(GONE);
        mErrorLabel.setVisibility(GONE);
        mCouponInputContainer.setVisibility(GONE);
        mLineSeparator.setVisibility(GONE);
        mCouponAppliedContainer.setVisibility(VISIBLE);

        mCouponAppliedLabel.setText(getContext().getString(R.string.ahd_coupon_applied, mCoupon.getCouponCode()));
        String discountStr = getContext().getString(R.string.ahd_amt_minus,
          String.valueOf(UiUtils.getFormattedAmount(getContext(), mCoupon.getCouponAmount())));
        mCouponDiscount.setText(discountStr);
        if (!TextUtils.isEmpty(mCoupon.getColor())) {
            mCouponDiscount.setTextColor(Color.parseColor(mCoupon.getColor()));
        }
    }

    private void setError(int resId) {
        LogUtils.LOGD(TAG, "setError : ");
        setError(getResources().getString(resId));
    }

    @DebugLog
    private void setError(String message) {
        LogUtils.LOGD(TAG, "setError : ");
        if (mErrorLabel != null && mLineSeparator != null) {
            mErrorLabel.setVisibility(VISIBLE);
            mErrorLabel.setText(message);
            mLineSeparator.setBackgroundColor(getResources().getColor(R.color.validation_error));
        }
    }

    public void updateView(Coupon coupon) {
        mCoupon = coupon;
        updateView();
        LogUtils.LOGD(TAG, "handleApplyResponse mIsAutoApply " + mIsAutoApply);
    }

    public void setCouponError(String error) {
        LogUtils.LOGD(TAG, "setCouponError : ");
        if (!TextUtils.isEmpty(error)) {
            setError(error);
        }
    }

    public void applyCoupon() {
        LogUtils.LOGD(TAG, "onCoupon apply onClick");
        if (iApplyCoupon != null) {
            iApplyCoupon.onCouponApply(mEditText.getText().toString().trim());
        }
    }

    private void editAppliedCoupon() {
        LogUtils.LOGD(TAG, "editAppliedCoupon : ");
        mCouponInputContainer.setVisibility(VISIBLE);
        mLineSeparator.setVisibility(VISIBLE);
        mCouponAppliedContainer.setVisibility(GONE);
        mEditText.setText(mCoupon.getCouponCode());
        mEditText.setSelection(mCoupon.getCouponCode().length());
        showSoftKeyBoard();
    }

    private void showSoftKeyBoard() {
        LogUtils.LOGD(TAG, "showSoftKeyBoard : ");
        mEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.apply_btn) {
            applyCoupon();
        } else if (i == R.id.coupon_edit_container) {
            editAppliedCoupon();
        } else if (i == R.id.have_coupon_label) {
            mCheckBox.setChecked(!mCheckBox.isChecked());
        }
    }
}