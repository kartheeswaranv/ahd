package com.athomediva.ui.services;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import java.util.List;

/**
 * Created by kartheeswaran on 16/04/17.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private static final String TAG = ServicesAdapter.class.getSimpleName();

    private final Context mContext;
    private final List<Services> mList;
    private int mResId;
    private View.OnClickListener mPlusClickListener;
    private View.OnClickListener mMinusClickListener;
    private View.OnClickListener mInfoClickListener;

    public ServicesAdapter(Context context, List<Services> list) {
        LogUtils.LOGD(TAG, "ServicesAdapter : ");
        mContext = context;
        mList = list;
    }

    public ServicesAdapter(Context context, int resId, List<Services> list, View.OnClickListener plusListener,
      View.OnClickListener minusListener) {
        LogUtils.LOGD(TAG, "ServicesAdapter : ");
        mContext = context;
        mResId = resId;
        mList = list;
        mPlusClickListener = plusListener;
        mMinusClickListener = minusListener;
    }

    public void setInfoClickListener(View.OnClickListener listener) {
        LogUtils.LOGD(TAG, "setInfoClickListener : ");
        mInfoClickListener = listener;
    }

    @Override
    public int getItemCount() {
        LogUtils.LOGD(TAG, "getItemCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LogUtils.LOGD(TAG, "onCreateViewHolder : ");
        View view = LayoutInflater.from(mContext).inflate(mResId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Services model = mList.get(position);
        if (model != null) {
            holder.serviceItemWidget.updateView(model);
        }
    }

    public void notifyItemChanged(Services model) {
        LogUtils.LOGD(TAG, "notifyItemChanged : ");
        if (model != null) {
            int index = mList.indexOf(model);
            if (index != -1) {
                notifyItemChanged(index);
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final ServiceItemWidget serviceItemWidget;

        public ViewHolder(View itemView) {
            super(itemView);
            serviceItemWidget = (ServiceItemWidget) itemView;
            serviceItemWidget.setListeners(mPlusClickListener, mMinusClickListener);
            serviceItemWidget.setInfoListener(mInfoClickListener);
        }
    }
}
