package com.athomediva.ui.services;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.AppConstants;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 16/04/17.
 */

public class CountWidget extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = CountWidget.class.getSimpleName();

    @BindView(R.id.count) TextView mCountView;
    @BindView(R.id.plus) ImageView mPlusView;
    @BindView(R.id.minus) ImageView mMinusView;
    @BindView(R.id.add) ImageView mAddView;
    @BindView(R.id.count_layout) ViewGroup mCountGroupView;

    private int mSelectionType;
    private int mQuantity = 0;
    private float mStartAngle = 0f;
    private float mEndAngle = 0f;
    private View.OnClickListener mPlusClickListener, mMinusClickListener;

    public CountWidget(Context context) {
        super(context);
    }

    public CountWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs, 0);
    }

    public CountWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs, defStyleAttr);
    }

    private void initAttrs(Context context, AttributeSet attrs, int style) {
        LogUtils.LOGD(TAG, "initAttrs : ");
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewCustom, style, 0);
            mSelectionType =
              ta.getInt(R.styleable.CountWidget_selection_type, AppConstants.SERVICE_SELECTION_TYPE.MULTI_SELECT.getType());
            ta.recycle();
        }

        TypedValue startAngle = new TypedValue();
        TypedValue endAngle = new TypedValue();
        getResources().getValue(R.dimen.package_rotation_start_angle, startAngle, false);
        getResources().getValue(R.dimen.package_rotation_end_angle, endAngle, false);
        mStartAngle = startAngle.getFloat();
        mEndAngle = endAngle.getFloat();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        mAddView.setOnClickListener(this);
        mPlusView.setOnClickListener(this);
        mMinusView.setOnClickListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // While Detach Window ,it redraw the view
        // So we need to apply rotation animation
        if (mAddView != null && mSelectionType == AppConstants.SERVICE_SELECTION_TYPE.SINGLE_SELECT.getType()) {
            applyRotationAnimation();
        }
    }

    @Override
    public void onClick(View view) {
        LogUtils.LOGD(TAG, "onClick : ");
        switch (view.getId()) {
            case R.id.plus:
                if (mPlusClickListener != null) {
                    view.setTag(this.getTag());
                    mPlusClickListener.onClick(view);
                }
                break;
            case R.id.minus:
                if (mMinusClickListener != null) {
                    view.setTag(this.getTag());
                    mMinusClickListener.onClick(view);
                }
                break;
            case R.id.add:
                if (mQuantity == 0) {
                    if (mPlusClickListener != null) {
                        view.setTag(this.getTag());
                        mPlusClickListener.onClick(view);
                    }
                } else {
                    if (mMinusClickListener != null) {
                        view.setTag(this.getTag());
                        mMinusClickListener.onClick(view);
                    }
                }

                break;
        }
    }

    @DebugLog
    public void updateView(int qty) {
        LogUtils.LOGD(TAG, "updateView : ");
        mQuantity = qty;
        if (mQuantity <= 0) {
            mCountGroupView.setVisibility(View.GONE);
            mAddView.setVisibility(View.VISIBLE);
            if (mSelectionType == AppConstants.SERVICE_SELECTION_TYPE.SINGLE_SELECT.getType()) {
                applyRotationAnimation();
            }
        } else {
            if (mSelectionType == AppConstants.SERVICE_SELECTION_TYPE.MULTI_SELECT.getType()) {
                mCountGroupView.setVisibility(View.VISIBLE);
                mAddView.setVisibility(View.GONE);
                mCountView.setText(String.valueOf(mQuantity));
            } else {
                applyRotationAnimation();
            }
        }
    }

    @DebugLog
    public void setUpdateListener(View.OnClickListener pluUpdateListener, View.OnClickListener minOnClickListener) {
        LogUtils.LOGD(TAG, "setUpdateListener : ");
        mPlusClickListener = pluUpdateListener;
        mMinusClickListener = minOnClickListener;
    }

    @DebugLog
    public void setSelectionType(int type) {
        LogUtils.LOGD(TAG, "setSelectionType : ");
        mSelectionType = type;
    }

    private void changeDrawable() {
        LogUtils.LOGD(TAG, "changeDrawable : " + mQuantity);
        if (mQuantity == 1) {
            UiUtils.setTintToDrawable(getContext(),mAddView.getBackground(),ContextCompat.getColor(getContext(), R.color.ahd_grey_dark));
            UiUtils.setTintToDrawable(getContext(),mAddView.getDrawable(),ContextCompat.getColor(getContext(), R.color.ahd_grey_dark));
            mAddView.invalidate();
        } else {
            UiUtils.setTintToDrawable(getContext(),mAddView.getBackground(),ContextCompat.getColor(getContext(), R.color.colorPrimary));
            UiUtils.setTintToDrawable(getContext(),mAddView.getDrawable(),ContextCompat.getColor(getContext(), R.color.colorPrimary));
            mAddView.invalidate();
        }
    }

    private void applyRotationAnimation() {
        LogUtils.LOGD(TAG, "applyRotationAnimation : ");
        /*if (mQuantity == 1) {
            AnimationHelper.rotateViewAnimation(mAddView, mStartAngle, mEndAngle, new AnimListener());
        } else {
            AnimationHelper.rotateViewAnimation(mAddView, mEndAngle, mStartAngle, new AnimListener());
        }*/

        if(mQuantity ==1) {
            mAddView.setRotation(mEndAngle);
        } else {
            mAddView.setRotation(mStartAngle);
        }
        changeDrawable();
    }

    private class AnimListener implements Animation.AnimationListener {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            LogUtils.LOGD(TAG, "onAnimationEnd : ");
            changeDrawable();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}
