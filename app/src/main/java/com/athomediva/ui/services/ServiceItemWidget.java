package com.athomediva.ui.services;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 26/04/17.
 */

public class ServiceItemWidget extends RelativeLayout {
    private static final String TAG = ServiceItemWidget.class.getSimpleName();

    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.count_widget) CountWidget mCountWidget;
    @BindView(R.id.minutes) TextView mDuration;
    @BindView(R.id.amt) TextView mAmountTxt;
    @BindView(R.id.info) ImageView mInfoImg;

    private Services mModel;

    private View.OnClickListener mPlusClickListener, mMinusClickListener, mInfoClickListener;

    public ServiceItemWidget(Context context) {
        super(context);
    }

    public ServiceItemWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServiceItemWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
    }

    @DebugLog
    public void setListeners(OnClickListener plusClick, OnClickListener minusClick) {
        LogUtils.LOGD(TAG, "setListeners : ");
        mPlusClickListener = plusClick;
        mMinusClickListener = minusClick;
    }

    public void setInfoListener(OnClickListener listener) {
        LogUtils.LOGD(TAG, "setInfoListener : ");
        mInfoClickListener = listener;
    }

    @DebugLog
    public void updateView(Services model) {
        LogUtils.LOGD(TAG, "updateView : ");
        if (model != null) {
            mModel = model;
            mTitle.setText(model.getName());
            mAmountTxt.setText(UiUtils.getFormattedAmount(getContext(), model.getPrice()));
            mDuration.setText(model.getDuration() + " " + model.getDurationText());
            mCountWidget.updateView(model.getQuantity());
            mCountWidget.setSelectionType(AppConstants.SERVICE_SELECTION_TYPE.MULTI_SELECT.getType());
            mCountWidget.setTag(model);
            mInfoImg.setTag(model);
            mCountWidget.setUpdateListener(mPlusClickListener, mMinusClickListener);
            if (mInfoClickListener != null) {
                if (!TextUtils.isEmpty(model.getInfo())) {
                    mInfoImg.setVisibility(View.VISIBLE);
                    mInfoImg.setOnClickListener(mInfoClickListener);
                } else {
                    mInfoImg.setVisibility(View.GONE);
                }
            }

            // If it is Complementary services We dont want to show the count
            if (model.isFreeServices()) {
                mCountWidget.setVisibility(View.GONE);
            } else {
                mCountWidget.setVisibility(View.VISIBLE);
            }
        }
    }
}
