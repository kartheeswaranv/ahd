package com.athomediva.ui.services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import com.athomediva.customview.SeparatorDecoration;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.Services;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.ServicesContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.InfoDialog;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 16/04/17.
 */

public class ServicesFragment extends BaseFragment<ServicesContract.Presenter, ServicesContract.View>
  implements ServicesContract.View {
    private static final String TAG = LogUtils.makeLogTag(ServicesFragment.class.getSimpleName());

    @BindView(R.id.services_list) RecyclerView mServicesListView;
    @Inject ServicesContract.Presenter mPresenter;
    private ServicesAdapter mAdapter;
    private InfoDialog mInfoDialog;
    private List<Services> mServicesList = new ArrayList<>();

    public static ServicesFragment newInstance(Bundle b) {
        ServicesFragment fragment = new ServicesFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView : ");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.ahd_services_fragment, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.ServicesPageModule(getArguments()))
          .inject(this);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @DebugLog
    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mAdapter = new ServicesAdapter(getContext(), R.layout.ahd_services_item, mServicesList,
          view -> mPresenter.onPlusClick((Services) view.getTag()),
          view -> mPresenter.onMinusClick((Services) view.getTag()));
        mAdapter.setInfoClickListener(view -> {
            mPresenter.onInfoClick((Services) view.getTag());
        });
        SeparatorDecoration decoration =
          new SeparatorDecoration(getContext(), ContextCompat.getColor(getContext(), R.color.ahd_grey_background),
            getResources().getDimension(R.dimen.divider_height), 0, 0);

        mServicesListView.addItemDecoration(decoration);
        ((SimpleItemAnimator) mServicesListView.getItemAnimator()).setSupportsChangeAnimations(false);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mServicesListView.setLayoutManager(manager);
        mServicesListView.setAdapter(mAdapter);
        // Remove the Default Animation  while Notify ItemChange

    }

    @DebugLog
    @Override
    public void updateServices(List<Services> list) {
        LogUtils.LOGD(TAG, "updateServices : ");
        mServicesList.clear();
        if (list != null && !list.isEmpty()) {
            mServicesList.addAll(list);
        }
        if (mAdapter != null) mAdapter.notifyDataSetChanged();
    }

    @DebugLog
    @Override
    public void updateServiceItem(Services model) {
        LogUtils.LOGD(TAG, "updateServiceItem : ");
        if (mAdapter != null) {
            mAdapter.notifyItemChanged(model);
        }
    }

    @DebugLog
    public void showInfoDialog(String title, String info) {
        LOGD(TAG, "showInfoDialog : ");
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(info)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        InfoDialogModel model = new InfoDialogModel(title, info);
        mInfoDialog = new InfoDialog(getContext(), model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getString(R.string.ok), view -> mInfoDialog.dismiss());
    }

    @Override
    public void onDestroyView() {
        LogUtils.LOGD(TAG, "onDestroyView : ");
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        super.onDestroyView();
    }
}
