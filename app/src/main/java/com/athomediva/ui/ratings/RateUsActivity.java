package com.athomediva.ui.ratings;

import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.EditTextCustom;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.RateUsContract;
import com.athomediva.ui.BaseActivity;
import javax.inject.Inject;
import www.zapluk.com.R;

public class RateUsActivity extends BaseActivity implements RateUsContract.View {
    private static final String TAG = LogUtils.makeLogTag(RateUsActivity.class.getSimpleName());

    @BindView(R.id.txtRateUsTitle) TextView mTitle;
    @BindView(R.id.txtThankYou) TextView mHeaderInfo;
    @BindView(R.id.ratingBar1) AppCompatRatingBar mRatingBar;
    @BindView(R.id.rate_comments) EditTextCustom mComments;
    @BindView(R.id.rate_playstore) TextView mPlayStoreRate;
    @BindView(R.id.rateUsSubmit) TextView mSendFeedback;
    @BindView(R.id.not_now) TextView mNotNow;
    @BindView(R.id.footer_info) TextView mFooterInfo;

    @Inject RateUsContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_rate_us);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.RateUsModule(getIntent().getExtras())).inject(this);
        mPresenter.attachView(this);
        initView();
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        setDrawables();
        mRatingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            LogUtils.LOGD(TAG, "onRatingChanged : ");
            mPresenter.onRatingChange(v);
        });
        mSendFeedback.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onSubmitClick(mRatingBar.getRating(), mComments.getText().toString());
        });

        mPlayStoreRate.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mPresenter.onPlayStoreClick();
        });
        mNotNow.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : Finish ");
            finish();
        });
    }

    @Override
    public void updateView() {
        LogUtils.LOGD(TAG, "updateView : ");
        mHeaderInfo.setText(getString(R.string.rate_share_msg));
    }

    @Override
    public void enableLowRatingOptions(boolean enable) {
        LogUtils.LOGD(TAG, "enableLowRatingOptions : ");
        mFooterInfo.setVisibility(View.GONE);
        if (enable) {
            mHeaderInfo.setText(getString(R.string.app_rating_low_title));
            mComments.setVisibility(View.VISIBLE);
            mSendFeedback.setVisibility(View.VISIBLE);
            mNotNow.setVisibility(View.VISIBLE);

            mPlayStoreRate.setVisibility(View.GONE);
        } else {
            mHeaderInfo.setText(getString(R.string.rate_share_msg));
            mComments.setVisibility(View.GONE);
            mSendFeedback.setVisibility(View.GONE);
            mNotNow.setVisibility(View.GONE);

            mPlayStoreRate.setVisibility(View.VISIBLE);
        }
    }

    private void setDrawables() {
        LogUtils.LOGD(TAG, "setDrawables : ");
        if (mRatingBar != null && mRatingBar.getProgressDrawable() != null) {
            LayerDrawable drawable = (LayerDrawable) mRatingBar.getProgressDrawable();
            // Change the Progress drawable to themecolor, background and secondary progress
            // have no change
            if (drawable != null && drawable.getDrawable(2) != null) {
                DrawableCompat.setTint(DrawableCompat.wrap(drawable.getDrawable(2).mutate()),
                  ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }
    }
}