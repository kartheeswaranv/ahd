package com.athomediva.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.address.AddAddressFragment;
import com.athomediva.ui.address.MyAddressFragment;
import com.athomediva.ui.bookingpage.BookingCancelFragment;
import com.athomediva.ui.bookingpage.BookingFailureFragment;
import com.athomediva.ui.bookingpage.BookingSuccessFragment;
import com.athomediva.ui.bookingpage.PaymentPendingFragment;
import com.athomediva.ui.offers.OffersAndCouponsActivity;
import com.athomediva.ui.support.CustomerCareFragment;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohit kumar on 17/01/17.
 * A container which host any fragment from the tag passed.
 */

public class FragmentContainerActivity extends BaseActivity {

    public static final String PARAM_FRAG_TAG_NAME = "param_frag_tag_name";
    private final String TAG = LogUtils.makeLogTag(FragmentContainerActivity.class.getSimpleName());
    @BindView(R.id.toolbar) Toolbar mToolbar;

    public interface BackPressListener {
        void onBackPress();
    }

    private BackPressListener mBackListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_fragment_container);
        ButterKnife.bind(this);
        initializeView();

        final String tagName = getIntent().getStringExtra(PARAM_FRAG_TAG_NAME);
        if (TextUtils.isEmpty(tagName)) {
            LogUtils.LOGE(TAG, "onCreate: fragment tag is empty ***********");
            finish();
            return;
        }

        initializeFragment(tagName);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }

    private void initializeView() {
        LOGD(TAG, "initializeView: ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initializeFragment(String tagName) {
        LOGD(TAG, "initializeFragment: tagName =" + tagName);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tagName);

        if (fragment == null) {
            LOGD(TAG, "onCreate: adding customer details fragment");
            String tag = "";
            if (tagName.equals(CustomerCareFragment.TAG)) {
                fragment = CustomerCareFragment.newInstance(getIntent().getExtras());
                tag = OffersAndCouponsActivity.TAG;
            } else if (tagName.equals(WebViewFragment.TAG)) {
                fragment = WebViewFragment.newInstance(getIntent().getExtras());
                tag = WebViewFragment.TAG;
            } else if (tagName.equals(MyAddressFragment.TAG)) {
                fragment = MyAddressFragment.newInstance(getIntent().getExtras());
                tag = MyAddressFragment.TAG;
            } else if (tagName.equalsIgnoreCase(AddAddressFragment.TAG)) {
                fragment = AddAddressFragment.newInstance(getIntent().getExtras());
                tag = AddAddressFragment.TAG;
            } else if (tagName.equalsIgnoreCase(BookingSuccessFragment.FRAG_TAG)) {
                fragment = BookingSuccessFragment.newInstance(getIntent().getExtras());
                tag = BookingSuccessFragment.FRAG_TAG;
            } else if (tagName.equalsIgnoreCase(BookingFailureFragment.FRAG_TAG)) {
                fragment = BookingFailureFragment.newInstance(getIntent().getExtras());
                tag = BookingFailureFragment.FRAG_TAG;
            } else if (tagName.equalsIgnoreCase(BookingCancelFragment.FRAG_TAG)) {
                fragment = BookingCancelFragment.newInstance(getIntent().getExtras());
                tag = BookingCancelFragment.FRAG_TAG;
            } else if (tagName.equalsIgnoreCase(PaymentPendingFragment.FRAG_TAG)) {
                fragment = PaymentPendingFragment.newInstance(getIntent().getExtras());
                tag = PaymentPendingFragment.FRAG_TAG;
            }
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().add(R.id.container, fragment, tag).commit();
            } else {
                LOGD(TAG, "onCreate:  no fragment found ");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: ");
        super.onActivityResult(requestCode, resultCode, data);
    }

    @CallSuper
    public void setTitle(String title) {
        LOGD(TAG, "setTitle: title Name =" + title);
        super.setTitle("");
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(title);
    }

    public void setNavigationIcon(@DrawableRes int resource) {
        LOGD(TAG, "setNavigationIcon: ");
        Drawable d = UiUtils.getTintDrawable(getApplicationContext(), resource, ContextCompat.getColor(this, R.color.white));
        if (d != null) {
            mToolbar.setNavigationIcon(d);
        }
    }

    public void setNavigationClickListener(View.OnClickListener listener) {
        if (mToolbar != null) mToolbar.setNavigationOnClickListener(listener);
    }

    public void setBackListener(BackPressListener listener) {
        mBackListener = listener;
    }

    @Override
    public void onBackPressed() {
        if (mBackListener == null) {
            super.onBackPressed();
        } else {
            mBackListener.onBackPress();
        }
    }
}
