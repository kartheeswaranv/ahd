package com.athomediva.ui.offers;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.SeparatorDecoration;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.remote.OffersItem;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.OffersViewContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class OffersAndCouponsActivity extends BaseActivity implements OffersViewContract.View {
    public static final String TAG = LogUtils.makeLogTag(OffersAndCouponsActivity.class.getSimpleName());
    @BindView(R.id.list_view) RecyclerView mListView;
    @Inject OffersViewContract.Presenter mPresenter;
    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptyLayout;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_offers_and_coupons_activity);
        LogUtils.LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.OffersAndCouponsModule(getIntent().getExtras())).inject(this);
        initViews();
        mPresenter.attachView(this);
    }

    private void initViews() {
        LogUtils.LOGD(TAG, "initViews : ");
        initToolbar();
        mEmptyLayout.updateData(AppConstants.getEmptyOffersPageItem(this));
        mEmptyLayout.setActionBtnClickListener(v -> mPresenter.onEmptyPageActionBtnClick());
    }

    private void initToolbar() {
        LogUtils.LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.custom_title);
        title.setText(getString(R.string.ahd_offers_coupans));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void updateOffersView(ArrayList<OffersItem> offersList) {
        LOGD(TAG, "updateOffersView: ");
        OffersAndCouponsAdapter adapter = new OffersAndCouponsAdapter(this, offersList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mListView.setLayoutManager(mLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        SeparatorDecoration decoration = new SeparatorDecoration(this, Color.TRANSPARENT, UiUtils.dpToPx(12));
        mListView.addItemDecoration(decoration);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(adapter);
        adapter.setCouponCodeListener(v -> mPresenter.onOffersCopyToClipboardClick((OffersItem) v.getTag()));
    }

    @Override
    public void updateEmptyPageVisibility(boolean show) {
        LOGD(TAG, "updateEmptyPageVisibility: ");
        mEmptyLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
