package com.athomediva.ui.offers;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.utils.UiUtils;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 15/06/18.
 */

public class InstantCouponActivity extends BaseActivity implements InstantCouponContract.View {
    private static final String TAG = LogUtils.makeLogTag(InstantCouponActivity.class.getSimpleName());
    @Inject InstantCouponContract.Presenter mPresenter;
    private TextView mCouponCodeView;
    private TextView mUseCouponCodeView;
    private TextView mCancelCta;
    private TextView mPercentageView;
    private TextView mAmountView;
    private TextView mOfferView;
    private ViewGroup mCouponCodeViewGroup;
    private ImageView mOfferIcon;
    private TextView mOfferValidityMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_instant_coupon_activity);
        viewComponent().plus(new ModuleManager.InstantCouponModule(this.getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        mOfferValidityMsg = (TextView) findViewById(R.id.offer_validity_msg);
        mPercentageView = (TextView) findViewById(R.id.percentage);
        mAmountView = (TextView) findViewById(R.id.amount);
        mOfferView = (TextView) findViewById(R.id.offer_txt);
        mCouponCodeViewGroup = (ViewGroup) findViewById(R.id.coupon_copy_view);
        mCouponCodeView = (TextView) findViewById(R.id.coupon_code);
        mCancelCta = (TextView) findViewById(R.id.cancel_cta);
        mUseCouponCodeView = (TextView) findViewById(R.id.use_coupon_code);
        mOfferIcon = (ImageView) findViewById(R.id.offer_img);
        mCancelCta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "onClick: ");
                mPresenter.onCancelClick();
            }
        });

        mCouponCodeViewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "onClick: Copy CouponCode click");
                mPresenter.onCouponCodeClick();
            }
        });

        //AnimationHelper.flipAnimation(this,mOfferIcon);
    }

    @Override
    public void updateOfferView(String percentage, String amount) {
        mPercentageView.setText(percentage);
        mAmountView.setText(amount);
        String offerTxt = getString(R.string.instant_offer_format_txt, percentage, amount);
        if (Build.VERSION.SDK_INT >= 24) {
            mOfferView.setText(Html.fromHtml(offerTxt, Html.FROM_HTML_MODE_LEGACY));
        } else {
            mOfferView.setText(Html.fromHtml(offerTxt));
        }
    }

    @Override
    public void updateCouponView(String couponCode) {
        mCouponCodeView.setText(couponCode);
        mUseCouponCodeView.setText(getString(R.string.use_coupon_code, couponCode));
    }

    @Override
    public void updateValidationMsg(String msg) {
        LogUtils.LOGD(TAG, "updateValidationMsg: ");
        UIHelper.hideViewIfEmptyTxt(mOfferValidityMsg, msg);
        //mOfferValidityMsg.setText(msg);
    }
}