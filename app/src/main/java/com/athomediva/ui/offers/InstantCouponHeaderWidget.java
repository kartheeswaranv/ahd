package com.athomediva.ui.offers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.InfoDialog;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 15/06/18.
 */

public class InstantCouponHeaderWidget extends LinearLayout {
    private static final String TAG = InstantCouponHeaderWidget.class.getSimpleName();

    private TextView mTitle;
    private ImageView mInfoIcon;
    private View mShadow;
    private View mEmptySpace;

    private InfoDialog mInfoDialog;
    private Handler mHandler;
    private Runnable mRunnable;

    public InstantCouponHeaderWidget(Context context) {
        super(context);
    }

    public InstantCouponHeaderWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InstantCouponHeaderWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mHandler = new Handler();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        mTitle = (TextView) findViewById(R.id.title);
        mInfoIcon = (ImageView) findViewById(R.id.info_icon);
        mShadow = (View) findViewById(R.id.shadow);
        mEmptySpace = (View) findViewById(R.id.empty_space);
        UiUtils.setTintToDrawable(getContext(), mInfoIcon.getDrawable(),
          ContextCompat.getColor(getContext(), R.color.ahd_green));
        AnimationHelper.flipAnimation(getContext(), mInfoIcon, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //super.onAnimationEnd(animation);
                /*new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                },1000);*/
            }
        });

        /*mRunnable = new Runnable() {
            @Override
            public void run() {
                startAnimation();
            }
        };
        startAnimation();*/
    }

    private void startAnimation() {
        LogUtils.LOGD(TAG, "startAnimation: ");
        /*AnimationHelper.flipAnimation(getContext(), mInfoIcon, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mHandler.postDelayed(mRunnable, 1000);
            }
        });*/
    }

    public void setDisableShadow(boolean value) {
        LogUtils.LOGD(TAG, "setDisableShadow: ");
        if (!value) {
            mShadow.setVisibility(View.VISIBLE);
        } else {
            mShadow.setVisibility(View.GONE);
        }
    }

    public void updateView(String title) {
        LogUtils.LOGD(TAG, "updateView: " + title);

        if (Build.VERSION.SDK_INT >= 24) {
            mTitle.setText(Html.fromHtml(title, Html.FROM_HTML_MODE_LEGACY));
        } else {
            mTitle.setText(Html.fromHtml(title));
        }
    }

    public void enableEmptySpace(boolean value) {
        LogUtils.LOGD(TAG, "enableEmptySpace: ");
        if (mEmptySpace != null) {
            if (value) {
                mEmptySpace.setVisibility(View.VISIBLE);
            } else {
                mEmptySpace.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mInfoDialog != null) {
            mInfoDialog.cancel();
        }
        mHandler.removeCallbacks(mRunnable);
        super.onDetachedFromWindow();
    }
}