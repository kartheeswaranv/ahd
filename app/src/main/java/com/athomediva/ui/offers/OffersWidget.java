package com.athomediva.ui.offers;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class OffersWidget extends RelativeLayout {
    public OffersWidget(Context context) {
        super(context);
    }

    public OffersWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public OffersWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OffersWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
