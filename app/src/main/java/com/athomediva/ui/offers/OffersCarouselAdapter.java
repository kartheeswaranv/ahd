package com.athomediva.ui.offers;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Offers;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 06/04/17.
 */

public class OffersCarouselAdapter extends PagerAdapter {
    private final String TAG = LogUtils.makeLogTag(OffersCarouselAdapter.class.getSimpleName());
    private final Context mContext;
    private List<Offers> mOffersList;
    private View.OnClickListener mCouponCopyListener;

    public OffersCarouselAdapter(Context context, List<Offers> offersList) {
        mContext = context;
        mOffersList = offersList;
    }

    @Override
    public int getCount() {
        if (mOffersList == null) return 0;
        return mOffersList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LOGD(TAG, "instantiateItem: position =" + position);
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.ahd_offers_widget, container, false);
        NetworkImageView imageView = (NetworkImageView) itemView.findViewById(R.id.banner_img_view);
        Offers offerItem = mOffersList.get(position);
        imageView.startLoading(offerItem.getPath());
        container.addView(itemView);
        LinearLayout mCouponCopyView = (LinearLayout) itemView.findViewById(R.id.coupon_copy_view);
        if(TextUtils.isEmpty(offerItem.getCouponCode())){
            mCouponCopyView.setVisibility(View.INVISIBLE);
        }else{
            mCouponCopyView.setVisibility(View.VISIBLE);
            ((TextViewCustom)mCouponCopyView.findViewById(R.id.coupon_code)).setText(offerItem.getCouponCode());
            mCouponCopyView.setTag(offerItem);
            mCouponCopyView.setOnClickListener(view -> {
                if (mCouponCopyListener != null) {
                    mCouponCopyListener.onClick(mCouponCopyView);
                }
            });
        }

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        LOGD(TAG, "destroyItem: ");
        if (object instanceof OffersWidget) {
            LOGD(TAG, "destroyItem: instance of OffersWidget");
            container.removeView((OffersWidget) object);
        }
    }

    public void updateData(List<Offers> offerList) {
        LOGD(TAG, "updateData: ");
        mOffersList = offerList;
    }

    public void setCouponCodeClickListener(View.OnClickListener listener) {
        LOGD(TAG, "setCouponCodeClickListener: ");
        mCouponCopyListener = listener;
    }
}
