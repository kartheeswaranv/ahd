package com.athomediva.ui.offers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.OffersItem;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import java.util.ArrayList;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class OffersAndCouponsAdapter extends RecyclerView.Adapter<OffersAndCouponsAdapter.ViewHolder> {
    private static final String TAG =
      LogUtils.makeLogTag(com.athomediva.ui.mybookings.BookingsStatusAdapter.class.getSimpleName());
    private final List<OffersItem> mBookingList;
    private final Context mContext;
    private View.OnClickListener mCouponCodeListener;

    public OffersAndCouponsAdapter(Context context, ArrayList<OffersItem> bookingList) {
        mBookingList = bookingList;
        mContext = context;
    }

    @Override
    public OffersAndCouponsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_offers_coupons_item, parent, false);
        LOGD(TAG, "onCreateViewHolder: ");
        return new OffersAndCouponsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OffersAndCouponsAdapter.ViewHolder holder, int position) {
        LOGD(TAG, "onBindViewHolder: ");
        OffersItem booking = mBookingList.get(position);
        holder.bind(mContext, booking, position);
    }

    @Override
    public int getItemCount() {
        if (mBookingList == null) {
            return 0;
        } else {
            return mBookingList.size();
        }
    }

    public void setCouponCodeListener(View.OnClickListener couponCodeListener) {
        this.mCouponCodeListener = couponCodeListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final NetworkImageView mBanner;
        private final TextViewCustom mCouponDesc;
        private final TextViewCustom mCouponTitle;
        private final TextViewCustom mCouponCode;
        private final LinearLayout mCouponLayout;
        private final TextViewCustom mCopyCouponCodeText;

        public ViewHolder(View itemView) {
            super(itemView);
            mBanner = (NetworkImageView) itemView.findViewById(R.id.banner);
            mCouponDesc = (TextViewCustom) itemView.findViewById(R.id.coupon_desc);
            mCouponTitle = (TextViewCustom) itemView.findViewById(R.id.coupon_title);
            mCouponCode = (TextViewCustom) itemView.findViewById(R.id.coupon_code);
            mCouponLayout = (LinearLayout) itemView.findViewById(R.id.coupon_copy_view);
            mCopyCouponCodeText = (TextViewCustom) itemView.findViewById(R.id.copy_coupon_code);
        }

        public void bind(Context context, OffersItem coupon, int pos) {
            itemView.setTag(coupon);
            mCouponDesc.setText(coupon.getDescription());
            mCouponCode.setText(coupon.getCouponCode());
            mCouponTitle.setText(coupon.getTitle());
            mBanner.setDefaultResId(R.drawable.ahd_img_offer_default);
            mBanner.startLoadingwithAnimation(coupon.getPath(), null);
            mCouponLayout.setOnClickListener(v -> {
                if (mCouponCodeListener != null) {
                    mCouponCodeListener.onClick(itemView);
                }
            });

            if(!TextUtils.isEmpty(coupon.getCouponCode())){
                mCouponLayout.setVisibility(View.VISIBLE);
                mCopyCouponCodeText.setVisibility(View.VISIBLE);
            }else{
                mCouponLayout.setVisibility(View.INVISIBLE);
                mCopyCouponCodeText.setVisibility(View.INVISIBLE);
            }
        }
    }
}
