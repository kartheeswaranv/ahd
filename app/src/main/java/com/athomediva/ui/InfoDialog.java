package com.athomediva.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.widgets.InfoDialogWidget;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/06/17.
 */

public class InfoDialog extends Dialog {
    private static final String TAG = InfoDialog.class.getSimpleName();

    private InfoDialogModel mModel;
    private Context mContext;
    private InfoDialogWidget widget;

    public InfoDialog(Context context, InfoDialogModel model) {
        super(context);
        LogUtils.LOGD(TAG, "InfoDialog : ");
        mContext = context;
        mModel = model;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ahd_info_dialog_view);
        LogUtils.LOGD(TAG, "onCreate : ");
        widget = (InfoDialogWidget) findViewById(R.id.info_widget);
        widget.updateView(mModel);
    }

    public void setPositiveButton(String text, View.OnClickListener listener) {
        LogUtils.LOGD(TAG, "setPositiveButton : ");
        if (widget != null) {
            widget.setPositiveButton(text, listener);
            widget.requestLayout();
        }
    }

    public void setNegativeButton(String text, View.OnClickListener listener) {
        LogUtils.LOGD(TAG, "setNegativeButton : ");
        if (widget != null) {
            widget.setNegativeButton(text, listener);
            widget.requestLayout();
        }
    }
}
