package com.athomediva.ui.myaccount;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.customview.EntityTextWatcher;
import com.athomediva.customview.ExpandListView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.EditProfileContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.home.SpecialDatesFragment;
import com.athomediva.ui.widgets.EditProfileAppBar;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class EditProfileActivity extends BaseActivity implements EditProfileContract.View {
    private static final String TAG = LogUtils.makeLogTag(EditProfileActivity.class.getSimpleName());

    @BindView(R.id.sign_up_view) TextViewCustom mUpdateBtn;
    @BindView(R.id.female) RadioButton mFemaleRadioButton;
    @BindView(R.id.male) RadioButton mMaleRadioButton;
    @BindView(R.id.mobile_number) CustomTextInputLayout mMobileNumber;
    @BindView(R.id.name) CustomTextInputLayout mName;
    @BindView(R.id.email) CustomTextInputLayout mEmail;
    @BindView(R.id.gender_error) TextViewCustom mGenderError;

    @BindView(R.id.toolbar_layout) EditProfileAppBar mEditProfileAppBar;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.app_bar) AppBarLayout mAppBar;

    @Inject EditProfileContract.Presenter mPresenter;

    private InfoDialog mInfoDialog;

    private SignUpItem mSignUpData;
    private EntityTextWatcher mMobileNumberTextWatcher;
    private EntityTextWatcher mEmailTextWatcher;
    private EntityTextWatcher mNameTextWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_account_edit_activity);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.EditProfileModule(getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
        mPresenter.onActivityRecreate(savedInstanceState);
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.EDIT_PROFILE,
            GATrackerContext.Label.EDIT_PROFILE_PAGE_LOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LOGD(TAG, "initView: ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        mUpdateBtn.setOnClickListener(v -> mPresenter.updateClick(mSignUpData));
        mFemaleRadioButton.setTypeface(
          UiUtils.getFontByName(getApplicationContext(), getString(R.string.ubuntu_regular_font)));
        mMaleRadioButton.setTypeface(
          UiUtils.getFontByName(getApplicationContext(), getString(R.string.ubuntu_regular_font)));

        mEditProfileAppBar.setEditClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            showOptionDialog();
        });

        UIHelper.setTranslucentStatus(this);
    }

    private void addTextWatchers() {
        LogUtils.LOGD(TAG, "addTextWatchers : ");
        mMobileNumberTextWatcher = new EntityTextWatcher(mSignUpData.getMobileNumberEntity());
        mNameTextWatcher = new EntityTextWatcher(mSignUpData.getNameEntity());
        mEmailTextWatcher = new EntityTextWatcher(mSignUpData.getEmailEntity());
        mMobileNumber.getEditText().addTextChangedListener(mMobileNumberTextWatcher);
        mName.getEditText().addTextChangedListener(mNameTextWatcher);
        mEmail.getEditText().addTextChangedListener(mEmailTextWatcher);

        RadioGroup rb = (RadioGroup) findViewById(R.id.gender_group);
        rb.setOnCheckedChangeListener((group, checkedId) -> {
            LOGD(TAG, "check change: ");
            if (checkedId == mMaleRadioButton.getId()) {
                mSignUpData.getGenderEntity().setData(AppConstants.MALE);
            } else {
                mSignUpData.getGenderEntity().setData(AppConstants.FEMALE);
            }

            mGenderError.setVisibility(View.GONE);
        });
    }

    private void removeTextWatchers() {
        mMobileNumber.getEditText().removeTextChangedListener(mMobileNumberTextWatcher);
        mName.getEditText().removeTextChangedListener(mNameTextWatcher);
        mEmail.getEditText().removeTextChangedListener(mEmailTextWatcher);
    }

    @DebugLog
    @Override
    public void updateView(SignUpItem signUpData) {
        LOGD(TAG, "updateView: ");
        removeTextWatchers();
        mSignUpData = signUpData;
        updateView();
        addTextWatchers();
    }

    private void updateView() {
        LogUtils.LOGD(TAG, "updateView : ");

        mMobileNumber.getEditText().setText(mSignUpData.getMobileNumberEntity().getData());
        mName.getEditText().setText(mSignUpData.getNameEntity().getData());
        mEmail.getEditText().setText(mSignUpData.getEmailEntity().getData());
        mMaleRadioButton.setChecked(mSignUpData.getGenderEntity().getData() != null && mSignUpData.getGenderEntity()
          .getData()
          .equals(AppConstants.MALE));
        mFemaleRadioButton.setChecked(mSignUpData.getGenderEntity().getData() != null && mSignUpData.getGenderEntity()
          .getData()
          .equals(AppConstants.FEMALE));
        showValidationError(mSignUpData);
    }

    private void showValidationError(SignUpItem item) {
        LOGD(TAG, "showValidationError: ");
        if (!item.getMobileNumberEntity().isValid()) {
            mMobileNumber.setErrorEnabled(true);

            mMobileNumber.setError(item.getMobileNumberEntity().getErrorMessage());
        }

        if (!item.getNameEntity().isValid()) {
            mName.setErrorEnabled(true);
            mName.setError(item.getNameEntity().getErrorMessage());
        }

        if (!item.getEmailEntity().isValid()) {
            mEmail.setErrorEnabled(true);
            mEmail.setError(item.getEmailEntity().getErrorMessage());
        }

        if (!item.getGenderEntity().isValid()) {
            mGenderError.setVisibility(View.VISIBLE);
            mGenderError.setText(item.getGenderEntity().getErrorMessage());
        }
    }

    @Override
    protected void onDestroy() {
        cleanup();
        if (mInfoDialog != null) {
            mInfoDialog.dismiss();
        }
        super.onDestroy();
    }

    private void cleanup() {
        LogUtils.LOGD(TAG, "cleanup : ");
        mMobileNumber.getEditText().removeTextChangedListener(mMobileNumberTextWatcher);
        mName.getEditText().removeTextChangedListener(mNameTextWatcher);
        mEmail.getEditText().removeTextChangedListener(mEmailTextWatcher);
    }

    @Override
    public void updateImageView(String url) {
        LogUtils.LOGD(TAG, "updateImageView : ");
        mEditProfileAppBar.setData(url);
    }

    private void showOptionDialog() {
        LogUtils.LOGD(TAG, "showOptionDialog : ");

        View view = LayoutInflater.from(this).inflate(R.layout.ahd_package_info_dialog, null);

        ExpandListView listView = (ExpandListView) view;
        listView.setExpanded(true);
        listView.setAdapter(new ArrayAdapter<String>(this, R.layout.ahd_dialog_info_itemview,
          getResources().getStringArray(R.array.profile_photo_upload_option)));

        listView.setOnItemClickListener((adapterView, view1, i, l) -> {
            LogUtils.LOGD(TAG, "onItemClick : ");
            if (mInfoDialog != null) {
                mInfoDialog.dismiss();
            }
            mPresenter.onEditImgClick(i);
        });

        InfoDialogModel model = new InfoDialogModel(getString(R.string.profile_photo_upload_title), view);

        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        LogUtils.LOGD(TAG, "onSaveInstanceState : ");
        mPresenter.onSaveInstance(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void initSpecialOfferView(boolean titleEnable, ArrayList<SpecialOfferDateModel> list,
      SpecialDatesFragment.SpecialDatesChangeListener listener) {
        LogUtils.LOGD(TAG, "initSpecialOfferView : ");
        if (getSupportFragmentManager().findFragmentByTag(SpecialDatesFragment.TAG) == null) {
            SpecialDatesFragment specialDateSectionFrag = SpecialDatesFragment.newInstance(list, titleEnable);
            specialDateSectionFrag.setDateChangeListener(listener);
            getSupportFragmentManager().beginTransaction()
              .add(R.id.special_date_container, specialDateSectionFrag, SpecialDatesFragment.TAG)
              .commit();
        } else {
            LogUtils.LOGD(TAG, "initiateSpecialOfferView : Special Date Frag is already Added");
        }
    }
}
