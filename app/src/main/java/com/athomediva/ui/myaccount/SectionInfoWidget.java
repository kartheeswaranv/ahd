package com.athomediva.ui.myaccount;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.TitleDescImageItem;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 08/04/17.
 */

public class SectionInfoWidget extends RelativeLayout {
    public SectionInfoWidget(Context context) {
        super(context);
    }

    public SectionInfoWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SectionInfoWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SectionInfoWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void updateData(TitleDescImageItem item) {
        ImageView imageView = (ImageView) findViewById(R.id.left_drawable);
        imageView.setImageResource(item.getDrawable());

        TextViewCustom titleTextView = (TextViewCustom) findViewById(R.id.section_title);
        titleTextView.setText(item.getTitle());

        TextViewCustom descTextView = (TextViewCustom) findViewById(R.id.end_desc);
        descTextView.setText(item.getDescription());
    }
}
