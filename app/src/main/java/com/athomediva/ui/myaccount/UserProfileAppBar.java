package com.athomediva.ui.myaccount;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.athomediva.customview.CircularNetworkImageView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 09/04/17.
 */

public class UserProfileAppBar extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(UserProfileAppBar.class.getSimpleName());
    private TextViewCustom mUserName, mUserGender, mUserEmail, mUserPhoneNumber;
    private CircularNetworkImageView mUserNetworkImageView;
    private ImageView mDefaultImageView;
    private View mBackGroundColor;

    public UserProfileAppBar(Context context) {
        super(context);
    }

    public UserProfileAppBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserProfileAppBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mUserName = (TextViewCustom) findViewById(R.id.user_name);
        mUserGender = (TextViewCustom) findViewById(R.id.user_gender);
        mUserEmail = (TextViewCustom) findViewById(R.id.user_email);
        mUserPhoneNumber = (TextViewCustom) findViewById(R.id.user_phone_number);
        mUserNetworkImageView = (CircularNetworkImageView) findViewById(R.id.user_image_network_view);
        mDefaultImageView = (ImageView) findViewById(R.id.default_view);
        mBackGroundColor = findViewById(R.id.color_background_layout);
    }

    public void setData(LoggedInUserDetails user) {
        LOGD(TAG, "setData: ");
        if (user == null) return;
        mUserName.setText(user.getName());
        mUserGender.setText(user.getGender());
        mUserEmail.setText(user.getEmail());
        mUserPhoneNumber.setText(user.getMobile());
        if (user.getImage() != null) {
            mUserNetworkImageView.startLoadingwithAnimation(user.getImage(), new NetworkImageView.ImageCallback() {
                @Override
                public void onImageLoadComplete(Bitmap bitmap) {
                    LOGD(TAG, "onImageLoadComplete: ");
                    handleOnImageAfterNetworkCall(bitmap);
                }

                @Override
                public void onImageLoadError() {
                    LOGD(TAG, "onImageLoadError: ");
                    handleOnImageAfterNetworkCall(null);
                }
            });
        }
    }

    private void handleOnImageAfterNetworkCall(Bitmap bitmap) {
        LOGD(TAG, "handleOnImageSuccess: ");
        if (bitmap == null) {
            mDefaultImageView.setVisibility(View.VISIBLE);
            mUserNetworkImageView.setImageResource(0);
            mBackGroundColor.setAlpha(1.0f);
        } else {
            mDefaultImageView.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Bitmap croppedBitmap = AndroidHelper.blur(getContext(), bitmap);
                setBackground(new BitmapDrawable(getResources(), croppedBitmap));
                mBackGroundColor.setAlpha(.31f);
            }
        }
    }
}
