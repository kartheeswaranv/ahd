package com.athomediva.ui.myaccount;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import com.athomediva.AHDApplication;
import com.athomediva.customview.ToolbarAnimationController;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MyAccountPageContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.widgets.ToolbarAccountWidget;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class MyAccountFragment extends BaseFragment<MyAccountPageContract.Presenter, MyAccountPageContract.View>
  implements MyAccountPageContract.View {
    public static final String TAG = LogUtils.makeLogTag(MyAccountFragment.class.getSimpleName());
    @BindView(R.id.section_container) LinearLayout mSectionContainer;
    @BindView(R.id.user_background) UserProfileAppBar mUserProfileBackgroundView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.app_bar) AppBarLayout mAppBar;
    @BindView(R.id.toolbar_layout) ToolbarAccountWidget mToolbarWidget;
    @Inject MyAccountPageContract.Presenter mPresenter;

    public static MyAccountFragment newInstance(Bundle bundle) {
        MyAccountFragment fragment = new MyAccountFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.ahd_my_accounts_frag, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.MyAccountsModule(getArguments())).inject(this);
        AHDApplication.get(getActivity())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.ACCOUNT,
            GATrackerContext.Label.ACCOUNT_PAGE_LOAD);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        getActivity().setTitle("");
        mToolbar.inflateMenu(R.menu.ahd_profile_edit_menu);

        ViewGroup imgLayout = (ViewGroup) mToolbar.getMenu().findItem(R.id.action_edit).getActionView();
        ImageView img = (ImageView) imgLayout.findViewById(R.id.menu_edit);
        UiUtils.setTintToDrawable(getContext(),img.getDrawable(),ContextCompat.getColor(getContext(), R.color.white));
        img.setOnClickListener(view -> {
            LOGD(TAG, "onClick : ");
            mPresenter.onEditClicked();
        });

        ToolbarAnimationController toolbarAnimationController =
          new ToolbarAnimationController(mToolbarWidget, mSectionContainer);
        mAppBar.addOnOffsetChangedListener(toolbarAnimationController);
    }

    @DebugLog
    @Override
    public void initializeActionSections(List<TitleDescImageItem> sectionItemList) {
        LOGD(TAG, "initializeActionSections: ");
        mSectionContainer.removeAllViews();

        if (sectionItemList == null) return;
        int marginTopDp = getContext().getResources().getDimensionPixelSize(R.dimen.divider_height);
        int sectionHeight = getContext().getResources().getDimensionPixelSize(R.dimen.default_my_account_info_height);
        for (TitleDescImageItem item : sectionItemList) {
            SectionInfoWidget sectionWidget = (SectionInfoWidget) getActivity().getLayoutInflater()
              .inflate(R.layout.ahd_section_info_widget, mSectionContainer, false);
            LinearLayout.LayoutParams params =
              new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, sectionHeight);
            params.setMargins(0, marginTopDp, 0, 0);
            sectionWidget.setLayoutParams(params);
            mSectionContainer.addView(sectionWidget);
            sectionWidget.setOnClickListener(view -> mPresenter.onItemClick(item.getTitle()));
            sectionWidget.updateData(item);
        }
        View shadowView =
          getActivity().getLayoutInflater().inflate(R.layout.ahd_generic_view_shadow, mSectionContainer, false);
        mSectionContainer.addView(shadowView);
    }

    @Override
    public void updateUserProfileView(LoggedInUserDetails user) {
        LOGD(TAG, "updateUserProfileView: ");
        mUserProfileBackgroundView.setData(user);
        mToolbarWidget.updateData(user);
    }
}
