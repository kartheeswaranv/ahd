package com.athomediva.ui.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.athomediva.customview.CircularNetworkImageView;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import com.quikr.android.network.NetworkImageView;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 06/06/17.
 */
public class EditProfileAppBar extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(com.athomediva.ui.myaccount.UserProfileAppBar.class.getSimpleName());
    private CircularNetworkImageView mUserNetworkImageView;
    private ImageView mDefaultImageView;
    private ImageView mEditImg;
    private ViewGroup mEditLayout;
    private View mBackGroundColor;

    public EditProfileAppBar(Context context) {
        super(context);
    }

    public EditProfileAppBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditProfileAppBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mEditImg = (ImageView) findViewById(R.id.edit_img);
        mUserNetworkImageView = (CircularNetworkImageView) findViewById(R.id.user_image_network_view);
        mDefaultImageView = (ImageView) findViewById(R.id.default_view);
        mBackGroundColor = findViewById(R.id.color_background_layout);
        mEditLayout = (ViewGroup) findViewById(R.id.edit_layout);
        UiUtils.setTintToDrawable(getContext(),mEditImg.getDrawable(),Color.WHITE);
    }

    public void setEditClickListener(OnClickListener listener) {
        LOGD(TAG, "setEditClickListener : ");
        if (mEditLayout != null) {
            mEditLayout.setOnClickListener(listener);
        }
    }

    public void setData(String imageUrl) {
        LOGD(TAG, "setData: ");
        if (imageUrl == null) return;
        mUserNetworkImageView.startLoadingwithAnimation(imageUrl, new NetworkImageView.ImageCallback() {
            @Override
            public void onImageLoadComplete(Bitmap bitmap) {
                LOGD(TAG, "onImageLoadComplete: ");
                handleOnImageAfterNetworkCall(bitmap);
            }

            @Override
            public void onImageLoadError() {
                LOGD(TAG, "onImageLoadError: ");
                handleOnImageAfterNetworkCall(null);
            }
        });
    }

    private void handleOnImageAfterNetworkCall(Bitmap bitmap) {
        LOGD(TAG, "handleOnImageSuccess: ");
        if (bitmap == null) {
            mDefaultImageView.setVisibility(View.VISIBLE);
            mUserNetworkImageView.setImageResource(0);
            mBackGroundColor.setAlpha(1.0f);
        } else {
            mDefaultImageView.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Bitmap croppedBitmap = AndroidHelper.blur(getContext(), bitmap);
                setBackground(new BitmapDrawable(getResources(), croppedBitmap));
                mBackGroundColor.setAlpha(.31f);
            }
        }
    }
}
