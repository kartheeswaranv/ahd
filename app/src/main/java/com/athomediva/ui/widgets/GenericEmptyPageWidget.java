package com.athomediva.ui.widgets;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 12/04/17.
 */

public class GenericEmptyPageWidget extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(GenericEmptyPageWidget.class.getSimpleName());
    private TextViewCustom mTitle;
    private TextViewCustom mDescription;
    private TextViewCustom mActionButton;
    private ImageView mImageView;

    public GenericEmptyPageWidget(Context context) {
        super(context);
    }

    public GenericEmptyPageWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GenericEmptyPageWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GenericEmptyPageWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTitle = (TextViewCustom) findViewById(R.id.title);
        mDescription = (TextViewCustom) findViewById(R.id.description);
        mActionButton = (TextViewCustom) findViewById(R.id.action_btn);
        mImageView = (ImageView) findViewById(R.id.image_view);
    }

    public void updateData(EmptySectionItem item) {
        LOGD(TAG, "updateData: ");
        mTitle.setText(item.getSectionTitle());
        mDescription.setText(item.getSectionDescription());
        if (!TextUtils.isEmpty(item.getActionButtonTitle())) {
            mActionButton.setVisibility(View.VISIBLE);
            mActionButton.setText(item.getActionButtonTitle());
        } else {
            mActionButton.setVisibility(View.INVISIBLE);
        }
        mImageView.setImageResource(item.getDrawableResourceID());
    }

    public void setActionBtnClickListener(View.OnClickListener listener) {
        LOGD(TAG, "setActionBtnClickListener: ");
        mActionButton.setOnClickListener(listener);
    }
}
