package com.athomediva.ui.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.CustomViewPager;
import com.athomediva.customview.PagerSlidingTabStrip;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 07/04/17.
 */

public class SectionPagerWidget extends LinearLayout {
    private static final String TAG = SectionPagerWidget.class.getSimpleName();

    @BindView(R.id.tabs) PagerSlidingTabStrip mTabs;
    @BindView(R.id.pager) CustomViewPager mViewPager;

    private SectionPagerListener mListener;

    public SectionPagerWidget(Context context) {
        super(context);
    }

    public SectionPagerWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SectionPagerWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
        LOGD(TAG, "onFinishInflate : ");
    }

    public void setOnPageChangeListener(SectionPagerListener listener) {
        mListener = listener;
    }

    private void initView() {
        ButterKnife.bind(this);
    }

    public void setAdapter(PagerAdapter adapter) {
        LOGD(TAG, "setAdapter : ");
        PagerAdapter mViewPageAdapter = adapter;
        mViewPager.setAdapter(mViewPageAdapter);
        //mViewPager.setOffscreenPageLimit(mViewPageAdapter.getCount());
        mTabs.setViewPager(mViewPager);
        mTabs.setOnPageChangeListener(new PageChangeListener());
    }

    public interface SectionPagerListener {
        void onPageChangeListener(int position);
    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (mListener != null) {
                mListener.onPageChangeListener(position);
            }
        }

        @Override
        public void onPageSelected(int position) {
            LOGD(TAG, "onPageSelected: position ="+position);
            if (mListener != null) {
                mListener.onPageChangeListener(position);
            }

            mViewPager.measureCurrentView(mViewPager.findViewWithTag(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
