package com.athomediva.ui.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class SocialNetworkingAdapter extends RecyclerView.Adapter<SocialNetworkingAdapter.ViewHolder> {
    private final String TAG = LogUtils.makeLogTag(SocialNetworkingAdapter.class.getSimpleName());
    private final List<SocialSharingItem> mServicesList;
    private final Context mContext;
    private final View.OnClickListener mClickListener;

    public SocialNetworkingAdapter(@NonNull Context context, @NonNull List<SocialSharingItem> bookingList,
      @NonNull View.OnClickListener listener) {
        mServicesList = bookingList;
        mContext = context;
        mClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_social_networking_item, parent, false);
        LinearLayout.LayoutParams params =
          new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, UiUtils.dpToPx(24), 0);
        LOGD(TAG, "onCreateViewHolder: ");
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LOGD(TAG, "onBindViewHolder: ");
        SocialSharingItem service = mServicesList.get(position);
        holder.bind(mContext, service, position);
    }

    @Override
    public int getItemCount() {
        if (mServicesList == null) {
            return 0;
        } else {
            return mServicesList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mImageView;
        private final TextViewCustom mDescriptionView;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.image_view);
            mDescriptionView = (TextViewCustom) itemView.findViewById(R.id.service_desc);
        }

        public void bind(Context context, SocialSharingItem service, int pos) {
            if (service.getLaunchIntent() != null && service.getLaunchIntent().getComponent() != null) {
                LOGD(TAG, "onMessageSharingClick: item item.getLaunchIntent().getComponent().getClassName()"
                  + service.getLaunchIntent().getComponent().getClassName());
                LOGD(TAG, "onMessageSharingClick: item item.getLaunchIntent().getComponent().getPackageName()"
                  + service.getLaunchIntent().getComponent().getPackageName());
            }
            mImageView.setImageDrawable(service.getDrawable());
            mDescriptionView.setText(service.getTitle());
            itemView.setTag(service);
            itemView.setOnClickListener(v -> mClickListener.onClick(itemView));
        }
    }
}
