package com.athomediva.ui.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.CircularNetworkImageView;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.quikr.android.network.NetworkImageView;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 11/04/17.
 */

public class ToolbarAccountWidget extends RelativeLayout {
    private static final String TAG = ToolbarAccountWidget.class.getSimpleName();
    @BindView(R.id.toolbar_user_name) TextView mUserName;
    @BindView(R.id.toolbar_phone_number) TextView mUserPhoneNumber;
    @BindView(R.id.user_image_network_view) CircularNetworkImageView mUserNetworkImageView;
    @BindView(R.id.toolbar_img_view) ImageView mDefaultImageView;

    public ToolbarAccountWidget(Context context) {
        super(context);
    }

    public ToolbarAccountWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolbarAccountWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private void initView() {
        LOGD(TAG, "initView : ");
        ButterKnife.bind(this);
    }

    public void updateData(LoggedInUserDetails user) {
        LOGD(TAG, "updateLocation : ");
        LOGD(TAG, "setData: ");
        if (user == null) return;
        mUserName.setText(user.getName());
        mUserPhoneNumber.setText(user.getMobile());
        if (user.getImage() != null) {
            mUserNetworkImageView.startLoadingwithAnimation(user.getImage(), new NetworkImageView.ImageCallback() {
                @Override
                public void onImageLoadComplete(Bitmap bitmap) {
                    LOGD(TAG, "onImageLoadComplete: ");
                    handleOnImageAfterNetworkCall(bitmap);
                }

                @Override
                public void onImageLoadError() {
                    LOGD(TAG, "onImageLoadError: ");
                    handleOnImageAfterNetworkCall(null);
                }
            });
        }
    }

    private void handleOnImageAfterNetworkCall(Bitmap bitmap) {
        LOGD(TAG, "handleOnImageSuccess: ");
        if (bitmap == null) {
            mDefaultImageView.setVisibility(View.VISIBLE);
            mUserNetworkImageView.setImageBitmap(null);
        } else {
            mDefaultImageView.setVisibility(View.GONE);
            mUserNetworkImageView.setImageBitmap(bitmap);
            //mUserNetworkImageView.setBackground(new BitmapDrawable(getResources(), AndroidHelper.getRoundedBitmap(bitmap)));
        }
    }
}

