package com.athomediva.ui.widgets;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 11/04/17.
 */

public class ToolbarLocationWidget extends RelativeLayout {
    private static final String TAG = ToolbarLocationWidget.class.getSimpleName();

    @BindView(R.id.title) TextView mTitleView;
    @BindView(R.id.location_title) TextView mLocationView;
    @BindView(R.id.location_icon) ImageView mLocationImg;
    @BindView(R.id.drop_down) ImageView mDropDownImg;

    public ToolbarLocationWidget(Context context) {
        super(context);
    }

    public ToolbarLocationWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolbarLocationWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        ButterKnife.bind(this);
        UiUtils.setTintToDrawable(getContext(),mDropDownImg.getDrawable(),ContextCompat.getColor(getContext(), R.color.white));
        UiUtils.setTintToDrawable(getContext(),mLocationImg.getDrawable(),ContextCompat.getColor(getContext(), R.color.white));
    }

    public void updateLocation(String location) {
        LogUtils.LOGD(TAG, "updateLocation : ");
        mLocationView.setText(location);
    }

    public void updateVisibility(boolean show) {
        mTitleView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        mLocationView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        mLocationImg.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        mDropDownImg.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
}
