package com.athomediva.ui.widgets;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.logger.LogUtils;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 11/04/17.
 */

public class TitleSubtitleIconWidget extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(TitleSubtitleIconWidget.class.getSimpleName());
    private TextViewCustom mTitle;
    private TextViewCustom mDesc;
    private ImageView mIconImageView;

    public TitleSubtitleIconWidget(Context context) {
        super(context);
    }

    public TitleSubtitleIconWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleSubtitleIconWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TitleSubtitleIconWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTitle = (TextViewCustom) findViewById(R.id.title);
        mDesc = (TextViewCustom) findViewById(R.id.description);
        mIconImageView = (ImageView) findViewById(R.id.image_view);
    }

    public void updateData(TitleDescImageItem item) {
        mTitle.setText(item.getTitle());
        mDesc.setText(item.getDescription());
        mIconImageView.setImageResource(item.getDrawable());
    }
}
