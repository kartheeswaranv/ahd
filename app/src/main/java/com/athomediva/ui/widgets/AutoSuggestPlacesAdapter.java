package com.athomediva.ui.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Predictions;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by mohitkumar on 27/04/17.
 */

public class AutoSuggestPlacesAdapter extends RecyclerView.Adapter<AutoSuggestPlacesAdapter.ViewHolder> {
    private final String TAG = com.athomediva.logger.LogUtils.makeLogTag(AutoSuggestPlacesAdapter.class.getSimpleName());
    private List<Predictions> mPlacesList;
    private View.OnClickListener mClickListener;

    public AutoSuggestPlacesAdapter(Context context, List<Predictions> placesList) {
        mPlacesList = placesList;
    }

    @Override
    public AutoSuggestPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
          LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_location_suggestion_item, parent, false);
        return new AutoSuggestPlacesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AutoSuggestPlacesAdapter.ViewHolder holder, int position) {
        Predictions place = mPlacesList.get(position);
        holder.bind(place);
    }

    @Override
    public int getItemCount() {
        if (mPlacesList == null) {
            return 0;
        } else {
            return mPlacesList.size();
        }
    }

    public void setDataAndNotifyDataSetChanged(List<Predictions> list) {
        mPlacesList = list;
        notifyDataSetChanged();
    }

    public void setItemClickListener(View.OnClickListener listener) {
        mClickListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextViewCustom mTitleView;
        private final TextViewCustom mDescriptionView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleView = (TextViewCustom) itemView.findViewById(R.id.address_title);
            mDescriptionView = (TextViewCustom) itemView.findViewById(R.id.address_desc);
        }

        public void bind(Predictions address) {
            itemView.setTag(address);
            itemView.setOnClickListener(v -> {
                if (mClickListener != null) mClickListener.onClick(itemView);
            });

            if (address.getStructuredFormatting() != null) {
                mTitleView.setText(address.getStructuredFormatting().getMainText());
                mDescriptionView.setText(address.getStructuredFormatting().getSecondaryText());
            }
        }
    }
}