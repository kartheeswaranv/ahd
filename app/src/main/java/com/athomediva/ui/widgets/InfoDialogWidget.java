package com.athomediva.ui.widgets;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 05/06/17.
 */

public class InfoDialogWidget extends LinearLayout {
    private static final String TAG = InfoDialogWidget.class.getSimpleName();

    private TextView mInfoTitle;
    private LinearLayout mViewContainer;
    private TextView mMsgView;
    private TextView mPositiveBtn;
    private TextView mNegativeBtn;
    private InfoDialogModel mModel;

    public InfoDialogWidget(Context context) {
        super(context);
    }

    public InfoDialogWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InfoDialogWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG,"onFinishInflate : ");
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        mInfoTitle = (TextView) findViewById(R.id.title);
        mViewContainer = (LinearLayout) findViewById(R.id.container);
        mMsgView = (TextView) findViewById(R.id.message);
        mPositiveBtn = (TextView)findViewById(R.id.positive_btn);
        mNegativeBtn = (TextView)findViewById(R.id.negative_btn);
    }

    @DebugLog
    private void setCustomView(View view) {
        LogUtils.LOGD(TAG, "setCustomView : ");
        if (mViewContainer != null) {
            if (mViewContainer.getChildCount() > 0) mViewContainer.removeAllViews();

            LinearLayout.LayoutParams params =
              new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(params);
            mViewContainer.addView(view);
        }
    }

    @DebugLog
    public void updateView(InfoDialogModel model) {
        LogUtils.LOGD(TAG,"updateView : ");
        mModel = model;

        if(!TextUtils.isEmpty(mModel.getTitle())) {
            mInfoTitle.setVisibility(View.VISIBLE);
            mInfoTitle.setText(Html.fromHtml(mModel.getTitle()));
        } else {
            mInfoTitle.setVisibility(View.GONE);
        }

        if(mModel.getCustomView() != null) {
            mViewContainer.setVisibility(View.VISIBLE);
            setCustomView(mModel.getCustomView());
        } else {
            mViewContainer.setVisibility(View.GONE);
            LogUtils.LOGD(TAG,"updateView : No Custom View ");
        }

        if(!TextUtils.isEmpty(mModel.getMessage())) {
            mMsgView.setText(Html.fromHtml(mModel.getMessage()));
            mMsgView.setVisibility(View.VISIBLE);
        } else {
            mMsgView.setVisibility(View.GONE);
        }
    }

    public void setPositiveButton(String text,OnClickListener listener) {
        LogUtils.LOGD(TAG,"setPositiveButton : ");
        if(mPositiveBtn != null) {
            mPositiveBtn.setVisibility(View.VISIBLE);
            mPositiveBtn.setText(text);
            mPositiveBtn.setOnClickListener(listener);
        }
    }

    public void setNegativeButton(String text,OnClickListener listener) {
        LogUtils.LOGD(TAG,"setNegativeButton : ");
        if(mNegativeBtn != null) {
            mNegativeBtn.setVisibility(View.VISIBLE);
            mNegativeBtn.setText(text);
            mNegativeBtn.setOnClickListener(listener);
        }
    }
}
