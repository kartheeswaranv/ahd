package com.athomediva.ui.widgets;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 11/04/17.
 */

public class ToolbarCartWidget extends RelativeLayout {
    private static final String TAG = ToolbarCartWidget.class.getSimpleName();

    @BindView(R.id.count) TextView mCountView;
    @BindView(R.id.cart_img) ImageView mCartImg;

    public ToolbarCartWidget(Context context) {
        super(context);
    }

    public ToolbarCartWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolbarCartWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        ButterKnife.bind(this);
        UiUtils.setTintToDrawable(getContext(),mCartImg.getDrawable(),ContextCompat.getColor(getContext(), R.color.white));
        mCartImg.invalidate();
    }

    /**
     * To Update the Cart
     */
    @DebugLog
    public void updateCart(int count) {
        LogUtils.LOGD(TAG, "updateCart : ");
        if (count > 0) {
            mCountView.setVisibility(View.VISIBLE);
            mCountView.setText(String.valueOf(count));
        } else {
            mCountView.setVisibility(View.GONE);
        }
    }
}
