package com.athomediva.ui.widgets;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.MinOrderErrorInfo;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.home.MinAmtErrorAdapter;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 19/06/17.
 */

public class MinOrderErrorWidget extends RelativeLayout {
    private static final String TAG = MinOrderErrorWidget.class.getSimpleName();

    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.info_icon) ImageView mInfoIcon;

    private InfoDialog mInfoDialog;

    private MinimumOrderError mMinimumOrderError;

    public MinOrderErrorWidget(Context context) {
        super(context);
    }

    public MinOrderErrorWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MinOrderErrorWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        UiUtils.setTintToDrawable(getContext(), mInfoIcon.getDrawable(), Color.WHITE);
        mInfoIcon.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            if (mMinimumOrderError != null) {
                showInfoDialog(mMinimumOrderError.getTitle(), mMinimumOrderError.getInfoList());
            }
        });

        this.setOnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : ");
            if (mMinimumOrderError != null) {
                showInfoDialog(mMinimumOrderError.getTitle(), mMinimumOrderError.getInfoList());
            }
        });
    }

    @DebugLog
    public void updateMinimumOrderInfo(MinimumOrderError error) {
        LogUtils.LOGD(TAG, "updateMinimumOrderInfo : ");
        mMinimumOrderError = error;
    }

    private void showInfoDialog(String title, ArrayList<MinOrderErrorInfo> list) {
        LogUtils.LOGD(TAG, "showInfoDialog : ");

        if (TextUtils.isEmpty(title) || list == null) return;

        View view = LayoutInflater.from(getContext()).inflate(R.layout.ahd_package_info_dialog, null);

        ExpandListView listView = (ExpandListView) view;
        listView.setExpanded(true);
        listView.setAdapter(new MinAmtErrorAdapter(getContext(), title, list));

        listView.setOnItemClickListener((adapterView, view1, i, l) -> {
            LogUtils.LOGD(TAG, "onItemClick : ");
            if (mInfoDialog != null) {
                mInfoDialog.dismiss();
            }
        });

        InfoDialogModel model = new InfoDialogModel(title, view);

        mInfoDialog = new InfoDialog(getContext(), model);
        mInfoDialog.show();
        mInfoDialog.setPositiveButton(getContext().getString(R.string.ok), view12 -> {
            if (mInfoDialog != null) mInfoDialog.dismiss();
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mInfoDialog != null) {
            mInfoDialog.cancel();
        }
        super.onDetachedFromWindow();
    }
}
