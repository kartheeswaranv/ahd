package com.athomediva.ui.widgets;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 11/04/17.
 */

public class SocialNetworkingWidget extends LinearLayout {
    private final String TAG = LogUtils.makeLogTag(SocialNetworkingWidget.class.getSimpleName());
    private TextViewCustom mTitle;
    private RecyclerView mListView;

    public SocialNetworkingWidget(Context context) {
        super(context);
    }

    public SocialNetworkingWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SocialNetworkingWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SocialNetworkingWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTitle = (TextViewCustom) findViewById(R.id.title);
        mListView = (RecyclerView) findViewById(R.id.list_view);
    }

    public void updateWidget(String title, List<SocialSharingItem> itemList, View.OnClickListener listener) {
        LOGD(TAG, "updateWidget: ");
        mTitle.setText(title);
        SocialNetworkingAdapter mAdapter = new SocialNetworkingAdapter(getContext(), itemList, listener);
        RecyclerView.LayoutManager mLayoutManager =
          new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mListView.setLayoutManager(mLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(mAdapter);
    }
}
