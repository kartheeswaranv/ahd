package com.athomediva.ui.mycart;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.Services;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.services.ServiceItemWidget;
import hugo.weaving.DebugLog;
import java.util.Collections;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

class MyCartServiceGroupAdapter extends BaseAdapter {
    private static final String TAG = MyCartServiceGroupAdapter.class.getSimpleName();

    private Context mContext;
    private View.OnClickListener mPlusClickListsner;
    private View.OnClickListener mMinusClickListsner;
    private List<Services> mList;

    private String groupName;

    @DebugLog
    public MyCartServiceGroupAdapter(Context context, List<Services> list, View.OnClickListener plusListener,
      View.OnClickListener minusListener) {
        LogUtils.LOGD(TAG, "MyCartServiceGroupAdapter : ");
        mContext = context;
        mList = list;
        //Collections.sort(mList, new ServiceGroupComparator());
        mPlusClickListsner = plusListener;
        mMinusClickListsner = minusListener;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @DebugLog
    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        LogUtils.LOGD(TAG, "getView : ");
        ViewHolder holder;
        Services model = mList.get(pos);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_mycart_service_group, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (model != null) {

            /*if (!TextUtils.equals(groupName, model.getGroupName())) {
                groupName = model.getGroupName();
                holder.mTitle.setVisibility(View.VISIBLE);
            } else {
                holder.mTitle.setVisibility(View.GONE);
            }*/
            if (!TextUtils.isEmpty( model.getGroupName())) {
                holder.mTitle.setVisibility(View.VISIBLE);
            } else {
                holder.mTitle.setVisibility(View.GONE);
            }
            holder.mTitle.setText(model.getGroupName());
            holder.mServiceItem.setListeners(mPlusClickListsner, mMinusClickListsner);
            holder.mServiceItem.updateView(model);
        }

        return view;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        LogUtils.LOGD(TAG, "getCount : " + mList.size());
        return mList.size();
    }

    class ViewHolder {
        final TextView mTitle;
        final ServiceItemWidget mServiceItem;

        public ViewHolder(View itemView) {
            mTitle = (TextView) itemView.findViewById(R.id.group_title);
            mServiceItem = (ServiceItemWidget) itemView.findViewById(R.id.services_item);
        }
    }
}
