package com.athomediva.ui.mycart;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataProcessor;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.InfoDialogModel;
import com.athomediva.data.models.remote.APIDateTimeSlotResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartHeaderDescription;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.data.models.remote.Services;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.ScheduleContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.InfoDialog;
import com.athomediva.ui.offers.InstantCouponHeaderWidget;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import com.athomediva.ui.widgets.MinOrderErrorWidget;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 26/04/17.
 */

public class MyCartActivity extends BaseActivity implements MyCartContract.View {
    private static final String TAG = LogUtils.makeLogTag(MyCartActivity.class.getSimpleName());

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.address) MyCartAddressWidget mAddressWidget;
    @BindView(R.id.bucket_list) ExpandListView mBucketListView;
    @BindView(R.id.header_info) TextView mHeaderInfo;
    @BindView(R.id.header_info_icon) ImageView mHeaderInfoIcon;
    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptyLayout;
    @BindView(R.id.content_container) ViewGroup mContentView;
    @BindView(R.id.proceed) TextView mProceed;
    @BindView(R.id.info_header) ViewGroup mHeaderInfoView;
    @BindView(R.id.min_order_error) MinOrderErrorWidget mMinOrderErrorView;
    @BindView(R.id.instant_coupon_header_view) InstantCouponHeaderWidget mCouponHeaderView;

    @Inject MyCartContract.Presenter mPresenter;

    private MyCartBucketAdapter mAdapter;
    private InfoDialog mInfoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahd_activity_my_cart);
        LOGD(TAG, "onCreate : ");
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.MyCartModule(getIntent())).inject(this);
        mPresenter.attachView(this);
        initViews();
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.CART,
            GATrackerContext.Label.CART_PAGE_LOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        LOGD(TAG, "initViews : ");
        initToolbar();
        mBucketListView.setExpanded(true);
        mEmptyLayout.updateData(AppConstants.getEmptyCartItem(this));
        mEmptyLayout.setActionBtnClickListener(v -> mPresenter.onExploreServiceClick());
        mProceed.setOnClickListener(view -> mPresenter.onProceedClick());
        UiUtils.setTintToDrawable(this, mHeaderInfoIcon.getDrawable(), ContextCompat.getColor(this, R.color.ahd_tuna));
    }

    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.title);
        title.setText(getString(R.string.mycart_title));
        TextView subTitle = (TextView) mToolbar.findViewById(R.id.sub_title);
        subTitle.setVisibility(View.GONE);
    }

    @DebugLog
    @Override
    public void updateAddress(Address address) {
        LOGD(TAG, "updateAddress : ");
        mAddressWidget.setVisibility(View.VISIBLE);
        mAddressWidget.setListeners(view -> mPresenter.onAddAddressClick(), view -> mPresenter.onChangeAddressClick());
        mAddressWidget.updateLocation(address);
    }

    @DebugLog
    @Override
    public void updateBuckets(List<CartBucketItem> list) {
        LOGD(TAG, "updateBuckets : ");
        if (list != null && list.size() > 0) {
            mAdapter = new MyCartBucketAdapter(this, list, view -> mPresenter.onPlusClick((Services) view.getTag()),
              view -> mPresenter.onMinusClick((Services) view.getTag()));
            mAdapter.setScheduleListener(view -> mPresenter.onScheduleClick((CartBucketItem) view.getTag()),
              view -> mPresenter.onScheduleClick((CartBucketItem) view.getTag()));
            mBucketListView.setAdapter(mAdapter);
        }
    }

    @Override
    public void updateHeaderInfo(String title, CartHeaderDescription desc) {
        LOGD(TAG, "updateHeaderInfo : ");
        if (!TextUtils.isEmpty(title)) {
            mHeaderInfoView.setVisibility(View.VISIBLE);
            mHeaderInfo.setText(title);
            if (desc != null && desc.getInfo() != null && !desc.getInfo().isEmpty()) {
                mHeaderInfoIcon.setVisibility(View.VISIBLE);
                mHeaderInfoView.setOnClickListener(view -> infoDialog(desc));
            } else {
                mHeaderInfoIcon.setVisibility(View.GONE);
            }
        } else {
            mHeaderInfoView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateAdapter() {
        LOGD(TAG, "updateAdapter : ");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateEmptyPageVisibility(boolean show) {
        LOGD(TAG, "updateEmptyPageVisibility: ");
        mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
        mEmptyLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showScheduleDialog(CartBucketItem model, APIDateTimeSlotResponse response) {
        LOGD(TAG, "showScheduleDialog : ");
        if (model != null) {
            ScheduleDialogFragment fragment = new ScheduleDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ScheduleContract.PARAM_RESPONSE, response);
            bundle.putParcelable(ScheduleContract.PARAM_SELECTED_SLOT, model.getTimeSlot());
            fragment.setArguments(bundle);
            fragment.setDateSelectionListener(slot -> {
                mPresenter.onScheduleConfirm(model, slot);
                mAdapter.notifyDataSetChanged();
            });
            fragment.show(getSupportFragmentManager(), ScheduleDialogFragment.TAG);
        }
    }

    @DebugLog
    private void infoDialog(CartHeaderDescription description) {
        LOGD(TAG, "infoDialog : ");
        if (description == null || description.getInfo() == null) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }

        View view = LayoutInflater.from(this).inflate(R.layout.ahd_mycart_header_info, null);
        ExpandListView listview = (ExpandListView) view;
        listview.setExpanded(true);
        listview.setAdapter(new DescriptionAdapter(this, R.layout.ahd_split_booking_info, description.getInfo()));

        InfoDialogModel model = new InfoDialogModel(description.getTitle(), view);

        mInfoDialog = new InfoDialog(this, model);

        mInfoDialog.show();

        mInfoDialog.setPositiveButton(getString(R.string.done), view1 -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mInfoDialog.dismiss();
        });
    }

    @DebugLog
    public void enableProceedView(boolean enable) {
        LogUtils.LOGD(TAG, "enableProceedView : ");
        mProceed.setClickable(enable);
        if (enable) {
            mProceed.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else {
            mProceed.setBackgroundColor(ContextCompat.getColor(this, R.color.ahd_grey_dark));
        }
    }

    @DebugLog
    public void showOrHideMinOrderError(MinimumOrderError error) {
        LogUtils.LOGD(TAG, "updateMinOrderInfo : ");
        if (error != null) {
            mMinOrderErrorView.setVisibility(View.VISIBLE);
            mMinOrderErrorView.updateMinimumOrderInfo(error);
        } else {
            mMinOrderErrorView.setVisibility(View.GONE);
        }
    }

    @DebugLog
    @Override
    public void showErrorMessage(String message) {
        LogUtils.LOGD(TAG, "showErrorMessage : ");
        if (TextUtils.isEmpty(message)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        InfoDialogModel model = new InfoDialogModel(null, message);

        mInfoDialog = new InfoDialog(this, model);

        mInfoDialog.show();

        mInfoDialog.setPositiveButton(getString(R.string.ok), view1 -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mInfoDialog.dismiss();
        });
    }

    @DebugLog
    @Override
    public void showErrorMessage(String message, DialogInterface.OnDismissListener listener) {
        LogUtils.LOGD(TAG, "showErrorMessage : ");
        if (TextUtils.isEmpty(message)) return;

        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        InfoDialogModel model = new InfoDialogModel(null, message);

        mInfoDialog = new InfoDialog(this, model);
        mInfoDialog.setCancelable(false);
        mInfoDialog.show();

        mInfoDialog.setPositiveButton(getString(R.string.ok), view1 -> {
            LogUtils.LOGD(TAG, "onClick : ");
            mInfoDialog.dismiss();
        });

        mInfoDialog.setOnDismissListener(listener);
    }

    @Override
    public void showContainerView() {
        LogUtils.LOGD(TAG, "showContainerView : ");
        if (mContentView != null) {
            mContentView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateInstantCouponView(InstantCoupon coupon, boolean disableShadow) {
        LogUtils.LOGD(TAG, "updateInstantCouponView: ");
        if (coupon != null) {
            mCouponHeaderView.setVisibility(View.VISIBLE);
            mCouponHeaderView.setDisableShadow(disableShadow);
            String amount = UiUtils.getFormattedAmount(this, coupon.getAmount());
            String percentage = coupon.getPercentage() + "%";
            String instant_coupon_msg = getString(R.string.instant_offer_text);
            mCouponHeaderView.setTag(coupon);

            String msg = DataProcessor.getCouponMessage(this, coupon);
            mCouponHeaderView.updateView(msg);
            mCouponHeaderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.LOGD(TAG, "onClick: Instant CouponClick");
                    InstantCoupon instantCoupon = (InstantCoupon) v.getTag();
                    mPresenter.onInstantCouponClick(instantCoupon);
                }
            });
        } else {
            mCouponHeaderView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy : ");
        if (mInfoDialog != null && mInfoDialog.isShowing()) {
            mInfoDialog.dismiss();
        }
        super.onDestroy();
    }
}
