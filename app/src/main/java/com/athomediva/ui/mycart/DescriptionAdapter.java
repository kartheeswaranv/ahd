package com.athomediva.ui.mycart;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.athomediva.data.models.remote.DescriptionInfo;
import com.athomediva.logger.LogUtils;
import com.quikr.android.network.NetworkImageView;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 13/06/17.
 */

public class DescriptionAdapter extends ArrayAdapter<DescriptionInfo> {
    private static final String TAG = DescriptionAdapter.class.getSimpleName();

    private Context mContext;
    private int mResId;
    private List<DescriptionInfo> mList;

    public DescriptionAdapter(Context context, int resource, List<DescriptionInfo> objects) {
        super(context, resource);
        LogUtils.LOGD(TAG, "DescriptionAdapter : ");
        mContext = context;
        mResId = resource;
        mList = objects;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    @Nullable
    @Override
    public DescriptionInfo getItem(int position) {
        LogUtils.LOGD(TAG, "getItem : ");
        if (mList != null) {
            return mList.get(position);
        } else {
            return null;
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LogUtils.LOGD(TAG, "getView : ");
        DescriptionInfo info = getItem(position);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(mResId, null);
        }

        TextView desc = (TextView) view.findViewById(R.id.info);
        NetworkImageView icon = (NetworkImageView) view.findViewById(R.id.icon);
        if (info != null) {
            desc.setText(info.getDesc());
            icon.startLoading(info.getIcon());
        } else {
            LogUtils.LOGD(TAG, "getView : DescriptionInfo Empty");
        }
        return view;
    }
}
