package com.athomediva.ui.mycart;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.local.BucketModel;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class BucketViewWidget extends RelativeLayout {
    private static final String TAG = BucketViewWidget.class.getSimpleName();

    @BindView(R.id.bucket_title_view) MyCartBucketTitleWidget mTitleWidget;
    @BindView(R.id.bucket_schedule_view) MyCartScheduleWidget mScheduleWidget;
    @BindView(R.id.services_view) MyCartServicesWidget mServicesWidget;

    public BucketViewWidget(Context context) {
        super(context);
    }

    public BucketViewWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BucketViewWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        initView();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
    }

    @DebugLog
    public void setTitleClickListener(OnClickListener mTitleClick) {
        LogUtils.LOGD(TAG, "setTitleClickListener : ");
        OnClickListener mTitleClickListener = mTitleClick;
        mTitleWidget.setOnClickListener(mTitleClickListener);
    }

    @DebugLog
    public void setServicesListener(OnClickListener plusClick, OnClickListener minusClick) {
        LogUtils.LOGD(TAG, "setServicesListener : ");
        OnClickListener mPlusClickListener = plusClick;
        OnClickListener mMinusClickListener = minusClick;
        mServicesWidget.setListener(mPlusClickListener, mMinusClickListener);
    }

    @DebugLog
    public void setScheduleClickListener(OnClickListener scheduleNow, OnClickListener scheduleEdit) {
        LogUtils.LOGD(TAG, "setScheduleClickListener : ");
        OnClickListener mScheduleNowClick = scheduleNow;
        OnClickListener mScheduleEditClick = scheduleEdit;
        mScheduleWidget.setListeners(mScheduleNowClick, mScheduleEditClick);
    }

    @DebugLog
    public void updateBucket(CartBucketItem model) {
        LogUtils.LOGD(TAG, "updateBucket : ");
        mTitleWidget.updateBucket(model);
        if (model.isExpanded()) {
            mServicesWidget.setVisibility(View.VISIBLE);
            mServicesWidget.updateServices(UiUtils.getAllServices(model.getServicesGroupList()));
        } else {
            mServicesWidget.setVisibility(View.GONE);
        }
        mScheduleWidget.updateBucket(model);
    }
}
