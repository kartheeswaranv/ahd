package com.athomediva.ui.mycart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 30/04/17.
 */

public class DateSlotAdapter extends RecyclerView.Adapter<DateSlotAdapter.ViewHolder> {
    private static final String TAG = DateSlotAdapter.class.getSimpleName();

    private final Context mContext;
    private final List<DateSlot> mDateList;
    private final OnItemClickListener mListener;

    public DateSlotAdapter(Context context, List<DateSlot> dateList, OnItemClickListener listener) {
        mContext = context;
        mDateList = dateList;
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        if (mDateList == null) {
            return 0;
        } else {
            return mDateList.size();
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogUtils.LOGD(TAG, "onBindViewHolder : ");
        DateSlot slotInfo = mDateList.get(position);

        if (slotInfo != null) {
            long seconds = slotInfo.getDate() * 1000;
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(seconds);
            holder.mDate.setText(UiUtils.getFormatedWeekDay(seconds));
            holder.mDay.setText(UiUtils.getFormatDay(seconds));

            holder.mMainView.setOnClickListener(view -> mListener.onItemClick(position));
            holder.mDate.setSelected(slotInfo.isSelected());
            holder.mDay.setSelected(slotInfo.isSelected());
            holder.mMainView.setSelected(slotInfo.isSelected());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.ahd_date_slot_item, null);
        return new ViewHolder(view);
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mDay;
        final TextView mDate;
        final ViewGroup mMainView;

        public ViewHolder(View itemView) {
            super(itemView);
            mDate = (TextView) itemView.findViewById(R.id.DateName);
            mDay = (TextView) itemView.findViewById(R.id.DateLabel);
            mMainView = (ViewGroup) itemView.findViewById(R.id.DateLayer);
        }
    }
}
