package com.athomediva.ui.mycart;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.models.local.BucketModel;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class MyCartScheduleWidget extends RelativeLayout {
    private static final String TAG = MyCartScheduleWidget.class.getSimpleName();

    @BindView(R.id.schedule_now_view) ViewGroup mScheduleNowGroup;
    @BindView(R.id.schedule_edit_view) ViewGroup mScheduleEditGroup;
    @BindView(R.id.schedule_now) TextView mScheduleNow;
    @BindView(R.id.schedule_edit) TextView mScheduleEdit;
    @BindView(R.id.schedule_date) TextView mScheduleDate;

    public MyCartScheduleWidget(Context context) {
        super(context);
    }

    public MyCartScheduleWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCartScheduleWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
    }

    @DebugLog
    public void setListeners(OnClickListener scheduleNow, OnClickListener scheduleEdit) {
        LogUtils.LOGD(TAG, "setListeners : ");
        OnClickListener mScheduleNowClick = scheduleNow;
        OnClickListener mScheduleEditClick = scheduleEdit;
        mScheduleEdit.setOnClickListener(mScheduleEditClick);
        mScheduleNow.setOnClickListener(mScheduleNowClick);
    }

    @DebugLog
    public void updateBucket(CartBucketItem model) {
        LogUtils.LOGD(TAG, "updateBucket : ");
        mScheduleEdit.setTag(model);
        mScheduleNow.setTag(model);
        if (model.getScheduledDateTime() != null) {
            String date =
              model.getScheduledDateTime();//UiUtils.getFormattedDateForBookingPage(getContext(),model.getScheduledDateTime());
            mScheduleDate.setText(date);
            mScheduleEditGroup.setVisibility(View.VISIBLE);
            mScheduleNowGroup.setVisibility(View.GONE);
        } else {
            mScheduleEditGroup.setVisibility(View.GONE);
            mScheduleNowGroup.setVisibility(View.VISIBLE);
        }
    }
}
