package com.athomediva.ui.mycart;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.models.local.BucketModel;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class MyCartBucketTitleWidget extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = MyCartBucketTitleWidget.class.getSimpleName();

    @BindView(R.id.bucket_title) TextView mTitle;
    @BindView(R.id.bucket_services_count) TextView mCountView;
    @BindView(R.id.drop_down_icon) ImageView mImg;

    private float mStartAngle;
    private float mEndAngle;

    public MyCartBucketTitleWidget(Context context) {
        super(context);
    }

    public MyCartBucketTitleWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCartBucketTitleWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        TypedValue startAngle = new TypedValue();
        TypedValue endAngle = new TypedValue();
        getResources().getValue(R.dimen.subcat_rotation_start_angle, startAngle, false);
        getResources().getValue(R.dimen.subcat_rotation_end_angle, endAngle, false);
        mStartAngle = startAngle.getFloat();
        mEndAngle = endAngle.getFloat();
    }

    @DebugLog
    public void updateBucket(CartBucketItem model) {
        LogUtils.LOGD(TAG, "updateBucket : ");
        if (model != null) {
            mTitle.setText(model.getTitle());
            int value = model.getServiceCount();// CoreLogic.getServicesCount(model.getOrderList());
            if (value > 0) {
                mCountView.setVisibility(View.VISIBLE);
                mCountView.setText(String.valueOf(value));
            } else {
                mCountView.setVisibility(View.GONE);
            }

            /*if (!model.isExpanded()) {
                AnimationHelper.rotateViewAnimation(mImg, mEndAngle, mStartAngle);
            } else {
                AnimationHelper.rotateViewAnimation(mImg, mStartAngle, mEndAngle);
            }*/
            if(!model.isExpanded()) {
                mImg.setRotation(mStartAngle);
            } else {
                mImg.setRotation(mEndAngle);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.drop_down_icon) {
            /*if (isExpand()) {
                AnimationHelper.rotateViewAnimation(mImg, mEndAngle, mStartAngle);
            } else {
                AnimationHelper.rotateViewAnimation(mImg, mStartAngle, mEndAngle);
            }*/
        }
    }
}
