package com.athomediva.ui.mycart;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.customview.ExpandListView;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.models.remote.Services;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class MyCartServicesWidget extends RelativeLayout {
    private static final String TAG = MyCartServicesWidget.class.getSimpleName();

    @BindView(R.id.services_list) ExpandListView mListView;
    @BindView(R.id.total_amt) TextView mTotalView;

    private MyCartServiceGroupAdapter mAdapter;

    private OnClickListener mPlusClickListener, mMinusClickListener;

    private List<Services> mList;

    public MyCartServicesWidget(Context context) {
        super(context);
    }

    public MyCartServicesWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCartServicesWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
        mListView.setExpanded(true);
    }

    public void setListener(OnClickListener plusClick, OnClickListener minusClick) {
        LogUtils.LOGD(TAG, "setListener : ");
        mPlusClickListener = plusClick;
        mMinusClickListener = minusClick;
    }

    @DebugLog
    public void updateServices(List<Services> model) {
        LogUtils.LOGD(TAG, "updateServices : ");
        mList = model;
        mAdapter = new MyCartServiceGroupAdapter(getContext(), mList, mPlusClickListener, mMinusClickListener);
        mListView.setAdapter(mAdapter);
        updateTotalView();
    }

    @DebugLog
    private void updateTotalView() {
        LogUtils.LOGD(TAG, "updateTotalView : ");
        double amount = CoreLogic.getServicesAmt(mList);
        mTotalView.setText(UiUtils.getFormattedAmount(getContext(), amount));
    }
}
