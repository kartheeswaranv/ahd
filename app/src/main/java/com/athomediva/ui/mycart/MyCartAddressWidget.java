package com.athomediva.ui.mycart;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.remote.Address;
import com.athomediva.logger.LogUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 26/04/17.
 */

public class MyCartAddressWidget extends RelativeLayout {
    private static final String TAG = MyCartAddressWidget.class.getSimpleName();

    @BindView(R.id.add_address) ViewGroup mAddAddress;
    @BindView(R.id.address_content) ViewGroup mAddressContent;
    @BindView(R.id.address) TextView mAddressLabelView;
    @BindView(R.id.full_address) TextView mAddressView;
    @BindView(R.id.change_address) TextView mChangeAddress;

    private boolean mIsEnableOption = true;

    private OnClickListener mChangeAddClickListener, mNewAddClickListener;

    public MyCartAddressWidget(Context context) {
        super(context);
    }

    public MyCartAddressWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCartAddressWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtils.LOGD(TAG, "onFinishInflate : ");
        ButterKnife.bind(this);
    }

    @DebugLog
    public void setListeners(OnClickListener newAddressClick, OnClickListener changeAddClick) {
        LogUtils.LOGD(TAG, "setListeners : ");
        mChangeAddClickListener = changeAddClick;
        mNewAddClickListener = newAddressClick;
        mAddAddress.setOnClickListener(mNewAddClickListener);
        mChangeAddress.setOnClickListener(mChangeAddClickListener);
    }

    @DebugLog
    public void updateLocation(Address address) {
        LogUtils.LOGD(TAG, "updateLocation : ");

        if(address != null) {
            if (!TextUtils.isEmpty(address.getAddress())) {
                mAddressLabelView.setText(address.getLabel());
            }

            if(!TextUtils.isEmpty(address.getAddress())) {
                mAddressView.setText(address.getAddress());
            }

            mAddAddress.setVisibility(View.GONE);
            mAddressContent.setVisibility(View.VISIBLE);
            mChangeAddress.setVisibility(View.VISIBLE);

        } else {
            mAddressContent.setVisibility(View.GONE);
            mChangeAddress.setVisibility(View.GONE);
            mAddAddress.setVisibility(View.VISIBLE);
        }
        updateCTAS();

    }

    public void enableOptions(boolean value) {
        mIsEnableOption = value;
        updateCTAS();
    }

    private void updateCTAS() {
        if (!mIsEnableOption) {
            mAddAddress.setVisibility(View.GONE);
            mChangeAddress.setVisibility(View.GONE);
            mAddressContent.setVisibility(View.VISIBLE);
        }
    }
}
