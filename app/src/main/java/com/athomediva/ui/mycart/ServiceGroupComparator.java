package com.athomediva.ui.mycart;

import com.athomediva.data.models.remote.Services;
import java.util.Comparator;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class ServiceGroupComparator implements Comparator<Services> {

    private static final String TAG = ServiceGroupComparator.class.getSimpleName();


    public ServiceGroupComparator() {
    }

    @Override
    public int compare(Services lhs, Services rhs) {
        if (lhs.getGroupName() == null) {
            return -1;
        }
        if (rhs.getGroupName() == null) {
            return 1;
        }

        if (lhs.getGroupName().equalsIgnoreCase(rhs.getGroupName())) {
            return 0;
        }
        return lhs.getGroupName().compareTo(rhs.getGroupName());
    }
}
