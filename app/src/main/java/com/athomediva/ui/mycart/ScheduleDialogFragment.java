package com.athomediva.ui.mycart;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.customview.HorizontalSeparatorDecoration;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.TimeSlot;
import com.athomediva.helper.UIHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.ScheduleContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseDialogFragment;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 30/04/17.
 */

public class ScheduleDialogFragment extends BaseDialogFragment<ScheduleContract.Presenter, ScheduleContract.View>
  implements ScheduleContract.View, View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(ScheduleDialogFragment.class.getSimpleName());

    @BindView(R.id.time_slot_value) TextView mTimeSlotValue;
    @BindView(R.id.time_slot) TextView mTimeSlot;
    @BindView(R.id.date_list) RecyclerView mDateListView;
    @BindView(R.id.date_error) TextView mDateError;
    @BindView(R.id.time_layout) ViewGroup mTimeLayout;
    @BindView(R.id.schedule_btn) TextView mScheduleBtn;
    @BindView(R.id.close) ImageView mClose;
    @Inject ScheduleContract.Presenter mPresenter;
    private DateSlotAdapter mAdapter;
    private PopupMenu mPopupMenu;
    private DateSelectListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ahd_schedule_dialog, container, false);
        bindView(this, rootView);
        LogUtils.LOGD(TAG, "onCreateView : ");
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.ScheduleModule(getArguments())).inject(this);
        return rootView;
    }

    @Override
    protected void initializeView(View rootView) {
        LogUtils.LOGD(TAG, "initializeView : ");
        mDateListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ((SimpleItemAnimator) mDateListView.getItemAnimator()).setSupportsChangeAnimations(false);
        HorizontalSeparatorDecoration decoration = new HorizontalSeparatorDecoration(getContext(), Color.TRANSPARENT,
          getResources().getDimension(R.dimen.item_space_horizontal_small),
          getResources().getDimension(R.dimen.item_space_horizontal_large));
        mDateListView.addItemDecoration(decoration);
        getDialog().getWindow().setGravity(Gravity.BOTTOM);
        mPopupMenu = new PopupMenu(getContext(), mTimeSlot, Gravity.END | Gravity.BOTTOM);

        mClose.setOnClickListener(this);
        mTimeLayout.setOnClickListener(this);
        mScheduleBtn.setOnClickListener(this);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.LOGD(TAG, "onCreate : ");
        setStyle(STYLE_NO_TITLE, R.style.AhdDialogTheme);
    }

    public void setDateSelectionListener(DateSelectListener listener) {
        LogUtils.LOGD(TAG, "setDateSelectionListener : ");
        mListener = listener;
    }

    private void showPopup() {
        LogUtils.LOGD(TAG, "showPopup : ");
        if (mPopupMenu != null) {
            mPopupMenu.show();
        }
    }

    private void hidePopup() {
        LogUtils.LOGD(TAG, "hidePopup : ");
        if (mPopupMenu != null) {
            mPopupMenu.dismiss();
        }
    }

    @DebugLog
    @Override
    public void updateMenu(List<TimeSlot> slots) {
        LogUtils.LOGD(TAG, "updateMenu : ");
        mTimeSlot.setVisibility(View.VISIBLE);
        if (slots != null && slots.size() > 0) {
            mDateError.setVisibility(View.GONE);
            mTimeLayout.setVisibility(View.VISIBLE);
            mTimeSlotValue.setVisibility(View.GONE);
            UIHelper.createTimePopupMenu(mPopupMenu, slots);
            mPopupMenu.setOnMenuItemClickListener(new PopupItemClickListener(slots));
        } else {
            mDateError.setVisibility(View.VISIBLE);
            mTimeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void refreshDateList() {
        LogUtils.LOGD(TAG, "refreshDateList : ");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @DebugLog
    @Override
    public void updateDateList(List<DateSlot> list) {
        LogUtils.LOGD(TAG, "updateDateList : ");
        mAdapter = new DateSlotAdapter(getContext(), list, pos -> mPresenter.onDateSelection(list.get(pos)));
        mDateListView.setAdapter(mAdapter);
    }

    @DebugLog
    @Override
    public void enableScheduleBtn(boolean enable) {
        LogUtils.LOGD(TAG, "enableScheduleBtn : ");
        /*mScheduleBtn.setClickable(enable);
        if (enable) {
            mScheduleBtn.setBackgroundResource(R.drawable.ahd_bg_blue_rect);
        } else {
            mScheduleBtn.setBackgroundResource(R.drawable.ahd_grey_rect);
        }*/

        mScheduleBtn.setEnabled(enable);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.schedule_btn:
                mPresenter.onScheduleClick(mListener);
                break;
            case R.id.time_layout:
                showPopup();
                break;
            case R.id.close:
                dismissAllowingStateLoss();
                break;
        }
    }

    @DebugLog
    @Override
    public void updateTimeView(TimeSlot value) {
        LogUtils.LOGD(TAG, "updateTimeView : ");
        if (value != null) {
            mDateError.setVisibility(View.GONE);
            mTimeSlot.setTag(value);
            mTimeSlot.setVisibility(View.GONE);
            mTimeSlotValue.setVisibility(View.VISIBLE);
            mTimeSlotValue.setText(value.getDisplayTime());
        }
    }

    @Override
    public void dismissDialog() {
        LogUtils.LOGD(TAG, "dismissDialog : ");
        dismissAllowingStateLoss();
    }

    public interface DateSelectListener {
        void onDateSelect(DateSlot slot);
    }

    private class PopupItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private final List<TimeSlot> timeList;

        public PopupItemClickListener(List<TimeSlot> list) {
            timeList = list;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            LogUtils.LOGD(TAG, "onMenuItemClick : ");
            if (item.getItemId() > -1) {
                TimeSlot value = timeList.get(item.getItemId());
                mPresenter.onTimeSelection(value);
            }
            return false;
        }
    }
}
