package com.athomediva.ui.mycart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.athomediva.data.models.local.BucketModel;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 27/04/17.
 */

class MyCartBucketAdapter extends BaseAdapter {
    private static final String TAG = MyCartBucketAdapter.class.getSimpleName();

    private final Context mContext;
    private final List<CartBucketItem> mList;
    private final View.OnClickListener mPlusClickListener;
    private final View.OnClickListener mMinusClickListener;
    private View.OnClickListener mScheduleNowClickListener;
    private View.OnClickListener mEditClickListener;

    public MyCartBucketAdapter(Context context, List<CartBucketItem> list, View.OnClickListener plusListener,
      View.OnClickListener minusListener) {
        LogUtils.LOGD(TAG, "MyCartBucketAdapter : ");
        mContext = context;
        mList = list;
        mPlusClickListener = plusListener;
        mMinusClickListener = minusListener;
    }

    public void setScheduleListener(View.OnClickListener scheduleListener, View.OnClickListener scheduleEditListener) {
        mScheduleNowClickListener = scheduleListener;
        mEditClickListener = scheduleEditListener;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        CartBucketItem model = mList.get(i);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.ahd_mycart_bucket_view, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (model != null) {
            holder.bucketItemWidget.updateBucket(model);
            holder.bucketItemWidget.setTitleClickListener(view1 -> {
                if (model.isExpanded()) {
                    model.setExpanded(false);
                } else {
                    model.setExpanded(true);
                }
                notifyDataSetChanged();
            });
        }

        return view;
    }

    @Override
    public int getCount() {
        LogUtils.LOGD(TAG, "getCount : ");
        if (mList == null) return 0;
        return mList.size();
    }

    public class ViewHolder {
        final BucketViewWidget bucketItemWidget;

        public ViewHolder(View itemView) {
            bucketItemWidget = (BucketViewWidget) itemView;
            bucketItemWidget.setScheduleClickListener(mScheduleNowClickListener, mEditClickListener);
            bucketItemWidget.setServicesListener(mPlusClickListener, mMinusClickListener);
        }
    }
}
