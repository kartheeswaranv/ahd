package com.athomediva.ui.mybookings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.AppConstants;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class BookingByStatusFragment extends BaseFragment {
    private static final String TAG = LogUtils.makeLogTag(BookingByStatusFragment.class.getSimpleName());
    private static final String KEY_BOOKING_DATA = "key_booking_data";
    @BindView(R.id.list_view) RecyclerView mListView;
    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptyLayout;

    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mRateServiceListener;

    public static BookingByStatusFragment newInstance(ArrayList<Booking> bookingList) {
        BookingByStatusFragment fragment = new BookingByStatusFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_BOOKING_DATA, bookingList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ahd_booking_by_status_frag, container, false);
        LOGD(TAG, "onCreateView: ");
        ButterKnife.bind(this, view);
        return view;
    }

    @DebugLog
    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        mEmptyLayout.updateData(AppConstants.getEmptyBookingItem(getContext()));
        mEmptyLayout.setActionBtnClickListener(view -> {
            LOGD(TAG, "onClick : ");
            launchHomePage();
        });
        ArrayList<Booking> mBookingList = getArguments().getParcelableArrayList(KEY_BOOKING_DATA);
        updateView(mBookingList);
    }

    public void updateView(ArrayList<Booking> mBookingList) {
        LogUtils.LOGD(TAG, "updateView : ");
        if (mBookingList != null && !mBookingList.isEmpty()) {
            mListView.setVisibility(View.VISIBLE);
            mEmptyLayout.setVisibility(View.GONE);
            BookingsStatusAdapter adapter =
              new BookingsStatusAdapter(getContext(), mBookingList, mItemClickListener, mRateServiceListener);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mListView.setLayoutManager(mLayoutManager);
            mListView.setItemAnimator(new DefaultItemAnimator());
            mListView.setAdapter(adapter);
        } else {
            mListView.setVisibility(View.GONE);
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    public void setItemClickListener(View.OnClickListener listener) {
        LOGD(TAG, "setItemClickListener : ");
        mItemClickListener = listener;
    }

    public void setRateServiceListener(View.OnClickListener listener) {
        LOGD(TAG, "setRateServiceListener : ");
        mRateServiceListener = listener;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }

    private void launchHomePage() {
        LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(getActivity(), AHDMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        launchActivity(intent);
    }
}
