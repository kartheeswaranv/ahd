package com.athomediva.ui.mybookings;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.logger.LogUtils;
import com.athomediva.utils.UiUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 07/04/17.
 */

public class BookingsStatusAdapter extends RecyclerView.Adapter<BookingsStatusAdapter.ViewHolder> {
    private final String TAG = LogUtils.makeLogTag(BookingsStatusAdapter.class.getSimpleName());
    private final List<Booking> mBookingList;
    private final Context mContext;
    private View.OnClickListener mListener;
    private View.OnClickListener mRateClickListener;

    public BookingsStatusAdapter(Context context, List<Booking> bookingList, View.OnClickListener listener,
      View.OnClickListener rateClickListener) {
        mBookingList = bookingList;
        mContext = context;
        mListener = listener;
        mRateClickListener = rateClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ahd_my_booking_item, parent, false);
        LOGD(TAG, "onCreateViewHolder: ");
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LOGD(TAG, "onBindViewHolder: ");
        Booking booking = mBookingList.get(position);
        holder.bind(mContext, booking, position);
        holder.mView.setTag(mBookingList.get(position));
        holder.mRateService.setTag(mBookingList.get(position));
        holder.mView.setOnClickListener(mListener);
        holder.mRateService.setOnClickListener(mRateClickListener);
        if (!TextUtils.isEmpty(booking.getStatusColor())) {
            holder.mBookingStatus.setTextColor(Color.parseColor(booking.getStatusColor()));
            GradientDrawable gd = (GradientDrawable) holder.mBookingStatus.getBackground().getCurrent();
            gd.setStroke(2, Color.parseColor(booking.getStatusColor()));
            holder.mBookingStatus.setBackground(gd);
        }
    }

    @Override
    public int getItemCount() {
        if (mBookingList == null) {
            return 0;
        } else {
            return mBookingList.size();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextViewCustom mTitleView;
        private final TextViewCustom mDescriptionView;
        private final TextViewCustom mAmountView;
        private final TextViewCustom mBookingStatus;
        private final TextViewCustom mRateService;
        private final View mView;
        //private final TextViewCustom mRateNow;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mTitleView = (TextViewCustom) itemView.findViewById(R.id.title);
            mDescriptionView = (TextViewCustom) itemView.findViewById(R.id.sub_desc);
            mAmountView = (TextViewCustom) itemView.findViewById(R.id.amount);
            mBookingStatus = (TextViewCustom) itemView.findViewById(R.id.booking_status);
            mRateService = (TextViewCustom) itemView.findViewById(R.id.rate_my_service);
        }

        public void bind(Context context, Booking booking, int pos) {
            mTitleView.setText(booking.getServiceName());
            mDescriptionView.setText(booking.getBucketName() + " " + booking.getScheduledDate());
            mAmountView.setText(UiUtils.getFormattedAmountByString(context, booking.getOrderFinal()));
            if (!TextUtils.isEmpty(booking.getDisplayStatus())) {
                mBookingStatus.setVisibility(View.VISIBLE);
                mBookingStatus.setText(booking.getDisplayStatus());
            } else {
                mBookingStatus.setVisibility(View.INVISIBLE);
            }
            if (booking.isOrderRated()) {
                mRateService.setVisibility(View.VISIBLE);
            } else {
                mRateService.setVisibility(View.INVISIBLE);
            }
        }
    }
}
