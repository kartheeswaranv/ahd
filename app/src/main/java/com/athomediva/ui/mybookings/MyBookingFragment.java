package com.athomediva.ui.mybookings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.PagerSlidingTabStrip;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MyBookingsPageContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class MyBookingFragment extends BaseFragment implements MyBookingsPageContract.View {
    public static final String TAG = LogUtils.makeLogTag(MyBookingFragment.class.getSimpleName());
    @BindView(R.id.booking_view_pager) ViewPager mViewPager;
    @BindView(R.id.sliding_tab_layout) PagerSlidingTabStrip mPagerSlidingTrip;
    @Inject MyBookingsPageContract.Presenter mPresenter;
    @BindView(R.id.appToolbar) Toolbar mToolbar;
    @BindView(R.id.swipe_container) SwipeRefreshLayout mSwipeRefreshLayout;

    private BookingAdapter mBookingFragmentAdapter;

    private static View.OnClickListener mItemClickListener;
    private static View.OnClickListener mRateServiceListener;

    public static MyBookingFragment newInstance(Bundle bundle) {
        MyBookingFragment fragment = new MyBookingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ahd_my_bookings_frag, container, false);
        ButterKnife.bind(this, view);
        ((BaseActivity) getActivity()).viewComponent().plus(new ModuleManager.MyBookingsModule(getArguments())).inject(this);
        return view;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "setCustomActionBar");
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(R.string.my_bookings_title);
        mItemClickListener = view -> {
            Booking booking = (Booking) view.getTag();
            if (booking != null) {
                mPresenter.onItemClick(booking);
            }
        };

        mRateServiceListener = view -> {
            LOGD(TAG, "onClick : Rate BookingService");
            Booking booking = (Booking) view.getTag();
            if (booking != null) {
                mPresenter.onRateMyServiceClick(booking);
            }
        };
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                LOGD(TAG, "onPageSelected: position = " + position);
                updateGAForPos(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            LogUtils.LOGD(TAG, "onRefresh : ");
            mPresenter.onRefreshClick();
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }

    private void updateGAForPos(int position) {
        LOGD(TAG, "updateGAForPos: ");
        if (position == 0) {
            AHDApplication.get(getActivity())
              .getComponent()
              .analyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.BOOKING_UPCOMING,
                GATrackerContext.Label.MYBOOKING_UPCOMING_PAGELOAD);
        } else {
            AHDApplication.get(getActivity())
              .getComponent()
              .analyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.BOOKING_PAST,
                GATrackerContext.Label.MYBOOOKING_PAST_PAGELOAD);
        }
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @DebugLog
    @Override
    public void updateBookingsView(ArrayList<String> titleList, ArrayList<ArrayList<Booking>> bookingList) {
        LogUtils.LOGD(TAG, "updateBookingsView : ");
        if (mBookingFragmentAdapter != null) {
            mBookingFragmentAdapter.updateList(titleList, bookingList);
            mPagerSlidingTrip.notifyDataSetChanged();
        } else {
            mBookingFragmentAdapter = new BookingAdapter(getChildFragmentManager(), bookingList, titleList);
            mViewPager.setAdapter(mBookingFragmentAdapter);
            mPagerSlidingTrip.setViewPager(mViewPager);
        }
    }

    static class BookingAdapter extends FragmentStatePagerAdapter {
        private List<ArrayList<Booking>> mBookingStatusList;
        private List<String> mBookingStatusTitleList;
        private List<WeakReference<BookingByStatusFragment>> mFragmentList = new ArrayList<>();

        public BookingAdapter(FragmentManager fragmentManager, List<ArrayList<Booking>> bookingList,
          List<String> fragmentTitleList) {
            super(fragmentManager);
            this.mBookingStatusList = bookingList;
            this.mBookingStatusTitleList = fragmentTitleList;
        }

        public void updateList(List<String> titleList, List<ArrayList<Booking>> bookingList) {
            mBookingStatusList = bookingList;
            if (titleList != null) {
                mBookingStatusTitleList = titleList;
            }
            refreshFragments();
        }

        private void refreshFragments() {
            LogUtils.LOGD(TAG, "refreshFragments : ");
            for (int index = 0; index < mFragmentList.size(); index++) {
                if (mFragmentList.get(index) != null
                  && mFragmentList.get(index).get() != null
                  && mBookingStatusList.size() <= mFragmentList.size()) {
                    mFragmentList.get(index).get().updateView(mBookingStatusList.get(index));
                }
            }
        }

        @Override
        public Fragment getItem(int position) {
            BookingByStatusFragment fragment = BookingByStatusFragment.newInstance(mBookingStatusList.get(position));
            fragment.setItemClickListener(mItemClickListener);
            fragment.setRateServiceListener(mRateServiceListener);
            mFragmentList.add(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Override
        public int getCount() {
            if (mBookingStatusList == null) return 0;
            return mBookingStatusList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mBookingStatusTitleList.get(position);
        }
    }

    @Override
    public void hideSwipeRefreshView() {
        LogUtils.LOGD(TAG, "hideSwipeRefreshView : ");
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
