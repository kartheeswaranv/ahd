package com.athomediva.ui.login;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.AHDApplication;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.customview.EntityTextWatcher;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.SignUpPageContract;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.home.SpecialDatesFragment;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

public class SignUpActivity extends BaseActivity implements SignUpPageContract.View {

    private final String TAG = LogUtils.makeLogTag(SignUpActivity.class.getSimpleName());
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.t_o_s) TextViewCustom mTermsOfService;
    @BindView(R.id.privacy_policy_view) TextViewCustom mPrivacyPolicyView;
    @BindView(R.id.sign_up_view) TextViewCustom mSignUpActBtn;
    @BindView(R.id.female) RadioButton mFemaleRadioButton;
    @BindView(R.id.male) RadioButton mMaleRadioButton;
    @BindView(R.id.mobile_number) CustomTextInputLayout mMobileNumber;
    @BindView(R.id.name) CustomTextInputLayout mName;
    @BindView(R.id.email) CustomTextInputLayout mEmail;
    @BindView(R.id.ref_code) CustomTextInputLayout mRefCode;
    @BindView(R.id.gender_error) TextViewCustom mGenderError;
    @Inject SignUpPageContract.Presenter mPresenter;
    private SignUpItem mSignUpData;
    private EntityTextWatcher mMobileNumberTextWatcher, mEmailTextWatcher, mNameTextWatcher, mRefCodeTextWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGD(TAG, "onCreate: ");
        setContentView(R.layout.ahd_activity_signup);
        ButterKnife.bind(this);
        viewComponent().plus(new ModuleManager.SignUpModule(getIntent().getExtras())).inject(this);
        initView();
        mPresenter.attachView(this);
        AHDApplication.get(getApplicationContext())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.SIGNUP,
            GATrackerContext.Label.SIGNUP_PAGE_LOAD);
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    private void initView() {
        LOGD(TAG, "initView: ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(R.string.sign_up_page_title);
        mTermsOfService.setOnClickListener(v -> mPresenter.termsOfServiceClick());
        mPrivacyPolicyView.setOnClickListener(v -> mPresenter.privacyPolicyClick());
        mSignUpActBtn.setOnClickListener(v -> mPresenter.signUpClick(mSignUpData));
        mFemaleRadioButton.setTypeface(
          UiUtils.getFontByName(getApplicationContext(), getString(R.string.ubuntu_regular_font)));
        mMaleRadioButton.setTypeface(
          UiUtils.getFontByName(getApplicationContext(), getString(R.string.ubuntu_regular_font)));
    }

    private void addTextWatchers() {
        mMobileNumberTextWatcher = new EntityTextWatcher(mSignUpData.getMobileNumberEntity());
        mNameTextWatcher = new EntityTextWatcher(mSignUpData.getNameEntity());
        mEmailTextWatcher = new EntityTextWatcher(mSignUpData.getEmailEntity());
        mRefCodeTextWatcher = new EntityTextWatcher(mSignUpData.getReferralCodeEntity());
        mMobileNumber.getEditText().addTextChangedListener(mMobileNumberTextWatcher);
        mName.getEditText().addTextChangedListener(mNameTextWatcher);
        mEmail.getEditText().addTextChangedListener(mEmailTextWatcher);
        mRefCode.getEditText().addTextChangedListener(mRefCodeTextWatcher);

        RadioGroup rb = (RadioGroup) findViewById(R.id.gender_group);
        rb.setOnCheckedChangeListener((group, checkedId) -> {
            LOGD(TAG, "check change: ");
            if (checkedId == mMaleRadioButton.getId()) {
                mSignUpData.getGenderEntity().setData(AppConstants.MALE);
            } else {
                mSignUpData.getGenderEntity().setData(AppConstants.FEMALE);
            }

            mGenderError.setVisibility(View.GONE);
        });
    }

    @Override
    public void updateView(SignUpItem signUpData) {
        LOGD(TAG, "updateView: ");
        removeTextWatchers();
        mSignUpData = signUpData;
        updateView();
        addTextWatchers();
    }

    private void updateView() {
        mMobileNumber.getEditText().setText(mSignUpData.getMobileNumberEntity().getData());
        mName.getEditText().setText(mSignUpData.getNameEntity().getData());
        mEmail.getEditText().setText(mSignUpData.getEmailEntity().getData());
        mRefCode.getEditText().setText(mSignUpData.getReferralCodeEntity().getData());
        mMaleRadioButton.setChecked(mSignUpData.getGenderEntity().getData() != null && mSignUpData.getGenderEntity()
          .getData()
          .equals(AppConstants.MALE));
        mFemaleRadioButton.setChecked(mSignUpData.getGenderEntity().getData() != null && mSignUpData.getGenderEntity()
          .getData()
          .equals(AppConstants.FEMALE));

        showValidationError();
    }

    private void showValidationError() {
        LOGD(TAG, "showValidationError: ");
        if (!mSignUpData.getMobileNumberEntity().isValid()) {
            mMobileNumber.setErrorEnabled(true);
            mMobileNumber.setError(mSignUpData.getMobileNumberEntity().getErrorMessage());
        }

        if (!mSignUpData.getNameEntity().isValid()) {
            mName.setErrorEnabled(true);
            mName.setError(mSignUpData.getNameEntity().getErrorMessage());
        }

        if (!mSignUpData.getEmailEntity().isValid()) {
            mEmail.setErrorEnabled(true);
            mEmail.setError(mSignUpData.getEmailEntity().getErrorMessage());
        }

        if (!mSignUpData.getReferralCodeEntity().isValid()) {
            mRefCode.setErrorEnabled(true);
            mRefCode.setError(mSignUpData.getReferralCodeEntity().getErrorMessage());
        }

        if (!mSignUpData.getGenderEntity().isValid()) {
            mGenderError.setVisibility(View.VISIBLE);
            mGenderError.setText(mSignUpData.getGenderEntity().getErrorMessage());
        }
    }

    @Override
    protected void onDestroy() {
        removeTextWatchers();
        super.onDestroy();
    }

    private void removeTextWatchers() {
        mMobileNumber.getEditText().removeTextChangedListener(mMobileNumberTextWatcher);
        mName.getEditText().removeTextChangedListener(mNameTextWatcher);
        mEmail.getEditText().removeTextChangedListener(mEmailTextWatcher);
        mRefCode.getEditText().removeTextChangedListener(mRefCodeTextWatcher);
    }

    @Override
    public void initiateSpecialOfferView(ArrayList<SpecialOfferDateModel> list,
      SpecialDatesFragment.SpecialDatesChangeListener listener) {
        LogUtils.LOGD(TAG, "initSpecialOfferView : ");
        if (getSupportFragmentManager().findFragmentByTag(SpecialDatesFragment.TAG) == null) {
            SpecialDatesFragment specialDateSectionFrag = SpecialDatesFragment.newInstance(list, true);
            specialDateSectionFrag.setDateChangeListener(listener);
            getSupportFragmentManager().beginTransaction()
              .add(R.id.special_date_container, specialDateSectionFrag, SpecialDatesFragment.TAG)
              .commit();
        } else {
            LogUtils.LOGD(TAG, "initiateSpecialOfferView : Special Date Frag is already Added");
        }
    }
}
