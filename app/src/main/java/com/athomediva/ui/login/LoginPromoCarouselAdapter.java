package com.athomediva.ui.login;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.logger.LogUtils;
import com.athomediva.ui.offers.OffersWidget;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 06/04/17.
 */

class LoginPromoCarouselAdapter extends PagerAdapter {
    private final String TAG = LogUtils.makeLogTag(LoginPromoCarouselAdapter.class.getSimpleName());
    private final Context mContext;
    private List<TitleDescImageItem> mOffersList;

    public LoginPromoCarouselAdapter(Context context, List<TitleDescImageItem> offersList) {
        mContext = context;
        mOffersList = offersList;
    }

    @Override
    public int getCount() {
        if (mOffersList == null) return 0;
        return mOffersList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LOGD(TAG, "instantiateItem: position =" + position);
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.ahd_promo_widget, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.icon_view);
        TitleDescImageItem item = mOffersList.get(position);
        imageView.setImageResource(item.getDrawable());
        ((TextViewCustom) itemView.findViewById(R.id.title_view)).setText(item.getTitle());
        ((TextViewCustom) itemView.findViewById(R.id.description_view)).setText(item.getDescription());
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        LOGD(TAG, "destroyItem: ");
        if (object instanceof OffersWidget) {
            LOGD(TAG, "destroyItem: instance of OffersWidget");
            container.removeView((OffersWidget) object);
        }
    }

    public void updateData(List<TitleDescImageItem> offerList) {
        LOGD(TAG, "updateData: ");
        mOffersList = offerList;
    }
}
