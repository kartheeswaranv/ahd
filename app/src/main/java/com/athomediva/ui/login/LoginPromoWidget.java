package com.athomediva.ui.login;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by mohitkumar on 05/04/17.
 */

public class LoginPromoWidget extends RelativeLayout {
    public LoginPromoWidget(Context context) {
        super(context);
    }

    public LoginPromoWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginPromoWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LoginPromoWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
