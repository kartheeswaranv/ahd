package com.athomediva.ui.login;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.logger.LogUtils;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 06/04/17.
 */

public class LoginPromoCarouselWidget extends RelativeLayout {
    private final String TAG = LogUtils.makeLogTag(LoginPromoCarouselWidget.class.getSimpleName());
    @BindView(R.id.pager_introduction) ViewPager mCarouselViewPager;
    @BindView(R.id.viewPagerCountDots) LinearLayout mIndicator;
    private LoginPromoCarouselAdapter mCarouselAdapter;
    private ImageView[] dots;
    private List<TitleDescImageItem> mPromoList;
    private final ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            LOGD(TAG, "onPageSelected: position =" + position);
            position = position % (mPromoList.size());
            if (mPromoList != null || mPromoList.size() <= 1) {
                for (int i = 0; i < mPromoList.size(); i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ahd_non_selected_item_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ahd_selected_item_dot));
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public LoginPromoCarouselWidget(Context context) {
        super(context);
    }

    public LoginPromoCarouselWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginPromoCarouselWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LoginPromoCarouselWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LOGD(TAG, "onFinishInflate: ");
        ButterKnife.bind(this);
        mCarouselViewPager.addOnPageChangeListener(mPageChangeListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LOGD(TAG, "onDetachedFromWindow: ");
        mCarouselViewPager.removeOnPageChangeListener(mPageChangeListener);
    }

    /*
         * @param offerList
         */
    public void updateView(List<TitleDescImageItem> offerList) {
        LOGD(TAG, "updateView: offerList " + String.valueOf(offerList == null ? null : offerList.size()));
        if (mCarouselAdapter == null) {
            mCarouselAdapter = new LoginPromoCarouselAdapter(getContext(), offerList);
            mCarouselViewPager.setAdapter(mCarouselAdapter);
        } else {
            mCarouselAdapter.updateData(offerList);
            mCarouselAdapter.notifyDataSetChanged();
        }
        mCarouselViewPager.setCurrentItem(0);
        mPromoList = offerList;
        initializeDotsIndicator();
    }

    private void initializeDotsIndicator() {
        LOGD(TAG, "initializeDotsIndicator: ");
        if (mPromoList == null || mPromoList.size() <= 1) {
            LOGE(TAG, "initializeDotsIndicator: offers should not be null **** ");
            return;
        }
        dots = new ImageView[mPromoList.size()];

        for (int i = 0; i < mPromoList.size(); i++) {
            dots[i] = new ImageView(getContext());
            final int pos = i;
            dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ahd_non_selected_item_dot));
            dots[i].setOnClickListener(view -> {
                if (mCarouselViewPager != null) {
                    mCarouselViewPager.setCurrentItem(pos);
                }
            });
            LinearLayout.LayoutParams params =
              new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            int dpMargins = getContext().getResources().getDimensionPixelSize(R.dimen.ahd_carousel_dots_spacings);
            params.setMargins(dpMargins, 0, dpMargins, 0);

            mIndicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ahd_selected_item_dot));
    }

    public ViewGroup getPromoView() {
        return mCarouselViewPager;
    }
}
