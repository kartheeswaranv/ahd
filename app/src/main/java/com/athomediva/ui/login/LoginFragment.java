package com.athomediva.ui.login;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import butterknife.BindView;
import com.athomediva.AHDApplication;
import com.athomediva.customview.CustomTextInputLayout;
import com.athomediva.customview.TextViewCustom;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.helper.AnimationHelper;
import com.athomediva.injection.module.ModuleManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.LoginPageContract;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.BaseActivity;
import com.athomediva.ui.BaseFragment;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

public class LoginFragment extends BaseFragment<LoginPageContract.Presenter, LoginPageContract.View>
  implements LoginPageContract.View {

    public static final String TAG = LogUtils.makeLogTag(LoginFragment.class.getSimpleName());
    @BindView(R.id.toolbar_login) Toolbar mToolbar;
    @BindView(R.id.promo_carousel_widget) LoginPromoCarouselWidget mPromoCarousel;
    @BindView(R.id.continue_login) TextViewCustom mContinueLogin;
    @BindView(R.id.contact_us_view) TextViewCustom mContactUs;
    @BindView(R.id.mobile_number_edit_text) CustomTextInputLayout mEditTextMobNum;
    @Inject LoginPageContract.Presenter mPresenter;
    @BindView(R.id.login_scroll) NestedScrollView mBottomLayout;

    public static LoginFragment newInstance(Bundle b) {
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LOGD(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.ahd_frag_login, container, false);
        bindView(this, view);
        ((BaseActivity) getActivity()).viewComponent()
          .plus(new ModuleManager.LoginModule(getArguments(), this))
          .inject(this);
        AHDApplication.get(getActivity())
          .getComponent()
          .analyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.LOGIN,
            GATrackerContext.Label.LOGIN_PAGE_LOAD);
        return view;
    }

    @Override
    protected MVPPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeView(View rootView) {
        LOGD(TAG, "initializeView: ");
        initToolbar();
        super.setTitle("");
        setHasOptionsMenu(true);
        ((TextViewCustom) mToolbar.findViewById(R.id.custom_title)).setText(R.string.login_page_title);
        mContinueLogin.setOnClickListener(view -> mPresenter.login(mEditTextMobNum.getEditText().getText().toString()));
        mContactUs.setOnClickListener(view -> mPresenter.contactUs());
        mToolbar.setNavigationOnClickListener(v -> mPresenter.onNavigationClose());
        mToolbar.setVisibility(View.INVISIBLE);
        mPromoCarousel.getPromoView().setVisibility(View.INVISIBLE);
        TranslateAnimation bottomSlideUpAnimation = AnimationHelper.getLoginBottomAnimation();

        bottomSlideUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                LOGD(TAG, "onAnimationStart: ");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                LOGD(TAG, "onAnimationEnd: ");
                mPromoCarousel.getPromoView().startAnimation(AnimationHelper.getPromoSlideUpAnimation());
                mPromoCarousel.getPromoView().setVisibility(View.VISIBLE);
                mToolbar.setVisibility(View.VISIBLE);
                mToolbar.startAnimation(AnimationHelper.getToolbarSlideDownAnimation());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                LOGD(TAG, "onAnimationRepeat: ");
            }
        });

        mBottomLayout.startAnimation(bottomSlideUpAnimation);

        //Call Login While Action Done click on Keyboard
        mEditTextMobNum.getEditText().setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (mContinueLogin != null) {
                    mPresenter.login(mEditTextMobNum.getEditText().getText().toString());
                }
            }
            return false;
        });
    }

    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        inflateToolbar(true);
    }

    private void inflateToolbar(boolean enableSkipMenu) {
        LOGD(TAG, "inflateToolbar: enableSkipMenu =" + enableSkipMenu);
        mToolbar.inflateMenu(R.menu.ahd_login_menu);
        MenuItem skipMenu = mToolbar.getMenu().findItem(R.id.action_skip);
        skipMenu.getActionView().setOnClickListener(v -> mPresenter.skip());
        skipMenu.setVisible(enableSkipMenu);
    }

    @Override
    public void updatePromoCarousel(List<TitleDescImageItem> titleDescImageItemList) {
        LOGD(TAG, "updatePromoCarousel: ");
        if (titleDescImageItemList == null || titleDescImageItemList.isEmpty()) {
            LOGE(TAG, "list should not be empty");
            return;
        }
        mPromoCarousel.updateView(titleDescImageItemList);
    }

    @DebugLog
    @Override
    public void updateSkipMenuVisibility(boolean show) {
        LOGD(TAG, "updateSkipMenuVisibility: show" + show);
        mToolbar.getMenu().clear();
        inflateToolbar(show);
    }

    @Override
    public void updateNavigationButtonVisibility(boolean show) {
        LOGD(TAG, "updateNavigationButtonVisibility: show =" + show);
        if (!show) {
            mToolbar.setNavigationIcon(null);
        } else {
            Drawable closeIcon = UiUtils.getTintDrawable(getContext(), R.drawable.ahd_ic_close,
              ContextCompat.getColor(getContext(), R.color.white));
            if (closeIcon != null) {
                mToolbar.setNavigationIcon(closeIcon);
            }
        }
    }

    @DebugLog
    @Override
    public void showValidationError(boolean show, String message) {
        LOGD(TAG, "showValidationError: ");
        mEditTextMobNum.setErrorEnabled(show);
        mEditTextMobNum.setError(message);
    }
}
