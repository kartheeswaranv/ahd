package com.athomediva.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.athomediva.data.AppConstants;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.ui.widgets.GenericEmptyPageWidget;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 21/07/17.
 */

public class NoNetworkActivity extends BaseActivity {
    private static final String TAG = LogUtils.makeLogTag(NoNetworkActivity.class.getSimpleName());

    @BindView(R.id.empty_layout) GenericEmptyPageWidget mEmptyLayout;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_network);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        LogUtils.LOGD(TAG, "initView : ");
        initToolbar();
        mEmptyLayout.updateData(AppConstants.getNoNetworkItem(this));
        mEmptyLayout.setActionBtnClickListener(view -> {
            LogUtils.LOGD(TAG, "onClick : Try Again Click");
            if (AndroidHelper.isNetworkAvailable(NoNetworkActivity.this)) {
                finishViewWithResult(RESULT_OK);
            } else {
                LogUtils.LOGD(TAG, "onClick : Try Again Click No Network");
            }
        });
    }

    private void initToolbar() {
        LOGD(TAG, "initToolbar : ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.setTitle("");
        TextView title = (TextView) mToolbar.findViewById(R.id.custom_title);
        title.setText(getString(R.string.no_network_page_title));
    }

    @Override
    protected MVPPresenter getPresenter() {
        return null;
    }
}
