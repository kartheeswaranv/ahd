package com.athomediva.logger;

import android.util.Log;
import android.view.View;
import www.zapluk.com.BuildConfig;

public class LogUtils {

    private static final String LOG_PREFIX = "ahd_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    private LogUtils() {
    }

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }

        return LOG_PREFIX + str;
    }

    public static void logMeasureSpec(String tag, int widthMeasureSpec, int heightMeasureSpec) {

        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        String widthMode;

        switch (View.MeasureSpec.getMode(widthMeasureSpec)) {
            case View.MeasureSpec.EXACTLY:
                widthMode = "EXACTLY";
                break;
            case View.MeasureSpec.AT_MOST:
                widthMode = "AT_MOST";
                break;
            default:
                widthMode = "UNSPECIFIED";
        }

        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        String heightMode;

        switch (View.MeasureSpec.getMode(heightMeasureSpec)) {
            case View.MeasureSpec.EXACTLY:
                heightMode = "EXACTLY";
                break;
            case View.MeasureSpec.AT_MOST:
                heightMode = "AT_MOST";
                break;
            default:
                heightMode = "UNSPECIFIED";
        }

        LOGD(tag, "Width: (" + widthMode + ", " + widthSize + ")  Height: (" + heightMode + ", " + heightSize + ")");
    }

    public static void logViewMeasuredDimensions(String TAG, View view) {
        if (view == null) {
            LOGD(TAG, "View is null. Cannot log dimensions");
        }

        LOGD(TAG, new KeyValueLogBuilder().appendInt("Width", view.getMeasuredWidth())
          .appendInt("Height", view.getMeasuredHeight())
          .build());
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    public static void LOGD(final String tag, String message) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG) {
            Log.d(resolveTag(tag), message);
        }
    }

    public static void LOGD(final String tag, String message, Throwable cause) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG) {
            Log.d(resolveTag(tag), message, cause);
        }
    }

    public static void LOGV(final String tag, String message) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG) {
            Log.v(resolveTag(tag), message);
        }
    }

    public static void LOGV(final String tag, String message, Throwable cause) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG) {
            Log.v(resolveTag(tag), message, cause);
        }
    }

    private static String resolveTag(String tag) {
        if (tag.length() > 23) return tag.substring(0, 23);

        return tag;
    }

    public static void LOGI(final String tag, String message) {
        Log.i(resolveTag(tag), message);
    }

    public static void LOGI(final String tag, String message, Throwable cause) {
        Log.i(resolveTag(tag), message, cause);
    }

    public static void LOGW(final String tag, String message) {
        Log.w(resolveTag(tag), message);
    }

    public static void LOGW(final String tag, String message, Throwable cause) {
        Log.w(resolveTag(tag), message, cause);
    }

    public static void LOGE(final String tag, String message) {
        Log.e(resolveTag(tag), message);
    }

    public static void LOGE(final String tag, String message, Throwable cause) {
        Log.e(resolveTag(tag), message, cause);
    }

    /**
     * Static factory for {@link KeyValueLogBuilder}
     *
     * @return new instance of {@link KeyValueLogBuilder}
     */
    public static KeyValueLogBuilder newKeyValueLogBuilder() {
        return new KeyValueLogBuilder();
    }

    /**
     * @author Gaurav Dingolia
     *
     *         This is a helper class for chaining the key-value pair logs.
     */
    public static class KeyValueLogBuilder {
        private static final char KEY_VALUE_SEPARATOR = ':';
        private static final String ENTITY_SEPARATOR = ", ";

        private final StringBuilder messageBuilder = new StringBuilder();

        private StringBuilder getBuilder() {
            return messageBuilder.length() > 0 ? messageBuilder.append(ENTITY_SEPARATOR) : messageBuilder;
        }

        public KeyValueLogBuilder appendBoolean(String key, boolean value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public KeyValueLogBuilder appendInt(String key, int value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public KeyValueLogBuilder appendLong(String key, long value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public KeyValueLogBuilder appendFloat(String key, float value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public KeyValueLogBuilder appendDouble(String key, double value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public KeyValueLogBuilder append(String key, String value) {
            getBuilder().append(key).append(KEY_VALUE_SEPARATOR).append(value);
            return this;
        }

        public String build() {
            return messageBuilder.toString();
        }
    }
}
