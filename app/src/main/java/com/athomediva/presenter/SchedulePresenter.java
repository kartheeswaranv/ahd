package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.APIDateTimeSlotResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.DataHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.ScheduleContract;
import com.athomediva.ui.mycart.ScheduleDialogFragment;
import hugo.weaving.DebugLog;
import java.util.List;
import com.athomediva.data.models.remote.TimeSlot;

import static com.athomediva.mvpcontract.ScheduleContract.PARAM_RESPONSE;
import static com.athomediva.mvpcontract.ScheduleContract.PARAM_SELECTED_SLOT;

/**
 * Created by kartheeswaran on 02/05/17.
 */

public class SchedulePresenter extends BasePresenter<ScheduleContract.View> implements ScheduleContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(SchedulePresenter.class.getSimpleName());

    private APIDateTimeSlotResponse mResponse;
    private List<DateSlot> mDateList;
    private Bundle mBundle;
    private TimeSlot mSelectedTimeSlot;

    public SchedulePresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull Bundle bundle) {
        super(dataManager, context, session);
        LogUtils.LOGD(TAG, "ServicesPagePresenter : ");
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull ScheduleContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        if (mBundle != null) {
            mResponse = mBundle.getParcelable(PARAM_RESPONSE);
            mSelectedTimeSlot = mBundle.getParcelable(PARAM_SELECTED_SLOT);
        }
        if (mResponse != null
          && mResponse.getData() != null
          && mResponse.getData().getDates() != null
          && !mResponse.getData().getDates().isEmpty()) {
            mDateList = mResponse.getData().getDates();
            boolean defaultSelection = true;
            if (mSelectedTimeSlot != null) {
                DateSlot slot = CoreLogic.updatePreselectedSlot(mDateList, mSelectedTimeSlot);
                if (slot != null) {
                    defaultSelection = false;
                    onDateSelection(slot);
                }
            }
            if (defaultSelection) onDateSelection(mDateList.get(0));

            getMvpView().updateDateList(mDateList);
        }
    }

    @Override
    public void onScheduleClick(ScheduleDialogFragment.DateSelectListener listener) {
        LogUtils.LOGD(TAG, "onScheduleClick : ");
        DateSlot slot = CoreLogic.getSelectedDateTimeSlot(mDateList);
        if (slot != null) {
            if (listener != null) {
                listener.onDateSelect(slot);
            }
            getMvpView().dismissDialog();
        } else {
            LogUtils.LOGD(TAG, "Date Slot is not select : ");
        }
    }

    @DebugLog
    @Override
    public void onDateSelection(DateSlot selectedDate) {
        LogUtils.LOGD(TAG, "onDateSelection : ");
        if (selectedDate != null) {
            getMvpView().updateMenu(selectedDate.getSlots());
            getMvpView().enableScheduleBtn(false);
            if (selectedDate.getSelectedTimeSlot() != null) {
                updateSelectedTime(selectedDate.getSelectedTimeSlot());
            }
            DataHelper.handleDateSelection(mDateList, selectedDate);
            getMvpView().refreshDateList();
        }
    }

    @DebugLog
    @Override
    public void onTimeSelection(TimeSlot slot) {
        LogUtils.LOGD(TAG, "onTimeSelect : ");
        DataHelper.handleTimeSelection(CoreLogic.getSelectedDateSlot(mDateList).getSlots(), slot);
        updateSelectedTime(slot);
    }

    private void updateSelectedTime(TimeSlot slot) {
        LogUtils.LOGD(TAG, "updateSelectedTime : ");
        getMvpView().updateTimeView(slot);
        if (slot != null) {
            getMvpView().enableScheduleBtn(true);
        } else {
            getMvpView().enableScheduleBtn(false);
        }
    }
}
