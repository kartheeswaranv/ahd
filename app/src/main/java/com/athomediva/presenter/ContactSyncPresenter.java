package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.InviteRequest;
import com.athomediva.data.models.remote.APIInvitesResponse;
import com.athomediva.data.models.remote.InvitedContact;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.RxContacts;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.ContactSyncContract;
import com.athomediva.data.models.local.Contact;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 04/09/17.
 */

public class ContactSyncPresenter extends BasePresenter<ContactSyncContract.View> implements ContactSyncContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(ContactSyncPresenter.class.getSimpleName());

    private List<InvitedContact> mInvitesList;
    private List<Contact> mContactList = new ArrayList<>();
    private HashMap<String, Contact> mSelectedList = new HashMap<>();
    private QuikrNetworkRequest mGetInviteRequest;
    private QuikrNetworkRequest mUpdateInviteRequest;

    public ContactSyncPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull BookingSession session,
      @NonNull Bundle bundle) {
        super(dataManager, context, session);
        LogUtils.LOGD(TAG, "ContactSyncPresenter : ");
    }

    @Override
    public void attachView(@NonNull ContactSyncContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            getMvpView().updateInviteMsgView(mBookingSession.getMetaData().getData().getInviteMsg());
        } else {
            getMvpView().updateInviteMsgView(null);
        }

        fetchingContacts();
    }

    @Override
    public void onItemSelect(Contact contact) {
        int pos = getOriginalPos(contact);
        if (mSelectedList != null && pos < mContactList.size()) {
            Contact item = mContactList.get(pos);
            if (mSelectedList.containsKey(item.getPhoneNo()) && !item.isChecked()) {
                mSelectedList.remove(item.getPhoneNo());
            } else {
                mSelectedList.put(item.getPhoneNo(), item);
            }
        }
        if (mSelectedList != null && !mSelectedList.isEmpty()) {
            getMvpView().enableInviteView(true);
        } else {
            getMvpView().enableInviteView(false);
        }
    }

    @Override
    public void onSingleRequestClick(Contact contact) {
        LogUtils.LOGD(TAG, "onCTAClick : ");
        int pos = getOriginalPos(contact);
        if (pos > -1) {
            List<Contact> item = new ArrayList<>();
            item.add(mContactList.get(pos));
            multipleInvitationRequest(item);
        }
    }

    @Override
    public void onMultiRequestClick() {
        LogUtils.LOGD(TAG, "onMultiRequestClick : ");
        if (mSelectedList != null && mSelectedList.size() > 0) {
            multipleInvitationRequest(new ArrayList<>(mSelectedList.values()));
        }
    }

    public void multipleInvitationRequest(List<Contact> list) {
        LogUtils.LOGD(TAG, "onBulkInvitationClick : ");
        updateInvites(mDataManager.getDataProcessor().getInviteRequestList(list));
    }

    private void fetchInviteFriends() {
        LogUtils.LOGD(TAG, "inviteFriends : ");
        if (!isViewAttached()) return;

        if (mGetInviteRequest != null) mGetInviteRequest.cancel();

        String str = CoreLogic.getAllMobileNos(mContactList);

        LogUtils.LOGD(TAG, "fetchInviteFriends : MobileNos " + str);
        if (TextUtils.isEmpty(str)) return;

        str = str.replace("[", "").replace("]", "");

        mGetInviteRequest =
          mDataManager.getApiManager().getInvitedList(str, new QuikrNetworkRequest.Callback<APIInvitesResponse>() {
              @Override
              public void onSuccess(APIInvitesResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  if (response != null && response.isSuccess() && response.getData() != null) {
                      mInvitesList = response.getData().getInviteList();
                      getMvpView().refreshItems(
                        mDataManager.getDataProcessor().getContactsListToUpdate(mContactList, mInvitesList));
                  } else if (response != null && response.getError() != null) {
                      mDataManager.getAnalyticsManager()
                        .trackGAEventForAPIFail(AppUrls.URL_GET_INVITE_LIST, response.getError());
                  }
                  getMvpView().hideContactProgressView();
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  ToastSingleton.getInstance().showToast(mContext.getString(R.string.contact_sync_failure_msg));
                  if (!isViewAttached()) return;
                  getMvpView().hideContactProgressView();
              }
          });

        mGetInviteRequest.execute();
    }

    private void updateInvites(ArrayList<InviteRequest> contacts) {
        LogUtils.LOGD(TAG, "inviteFriends : ");
        if (!isViewAttached() || contacts == null || contacts.isEmpty()) return;

        if (mUpdateInviteRequest != null) mUpdateInviteRequest.cancel();

        getMvpView().showProgressBar("");

        mUpdateInviteRequest =
          mDataManager.getApiManager().updateInviteList(contacts, new QuikrNetworkRequest.Callback<APIInvitesResponse>() {
              @Override
              public void onSuccess(APIInvitesResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.isSuccess() && response.getData() != null) {
                      ToastSingleton.getInstance().showToast(mContext.getString(R.string.invite_success_msg));
                      mInvitesList = response.getData().getInviteList();
                      getMvpView().refreshItems(
                        mDataManager.getDataProcessor().getRefreshItems(mContactList, mInvitesList, contacts));
                  } else if (response != null && response.getError() != null) {
                      mDataManager.getAnalyticsManager()
                        .trackGAEventForAPIFail(AppUrls.URL_UPDATE_INVITE_LIST, response.getError());
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<QuikrNetworkRequest>(mUpdateInviteRequest));
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
              }
          });

        mUpdateInviteRequest.execute();
    }

    private void fetchingContacts() {
        if (!isViewAttached()) return;
        getMvpView().showContactProgressView();
        RxContacts.fetch(mContext).delay(AppConstants.CONTACT_FETCHING_DELAY, TimeUnit.MILLISECONDS)
          .onBackpressureDrop()
          .observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.io())
          .subscribe(contactFetching());
    }

    private Observer contactFetching() {

        return new Observer<List<Contact>>() {
            @Override
            public void onError(Throwable e) {
                LogUtils.LOGD(TAG, "onError : ");
                e.printStackTrace();
                ToastSingleton.getInstance().showToast(mContext.getString(R.string.contact_sync_failure_msg));
                if (!isViewAttached()) return;
                getMvpView().hideContactProgressView();
            }

            @Override
            public void onNext(List<Contact> contact) {
                LogUtils.LOGD(TAG, "onNext : ");
                if (contact != null && !contact.isEmpty()) mContactList = contact;
                getMvpView().updateData(mContactList);
            }

            @Override
            public void onCompleted() {
                LogUtils.LOGD(TAG, "onCompleted : ");
                if (!isViewAttached()) return;
                if (mContactList != null) {
                    getMvpView().updateContactCountView(mContactList.size());
                }
                fetchInviteFriends();
            }
        };
    }

    private int getOriginalPos(Contact item) {
        if (mContactList != null) {
            return mContactList.indexOf(item);
        }

        return -1;
    }
}
