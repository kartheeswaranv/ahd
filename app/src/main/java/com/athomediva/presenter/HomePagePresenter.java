package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.DataValidator;
import com.athomediva.data.busevents.CartUpdateEvent;
import com.athomediva.data.busevents.CategoryLoadEvent;
import com.athomediva.data.busevents.CategoryUpdateEvent;
import com.athomediva.data.busevents.SpecialDateUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.APIGetMetaDataResponse;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.models.remote.AppUpdate;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.MyBookingResponse;
import com.athomediva.data.models.remote.OrderInfo;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.HomePageContract;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.SubcatSectionContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.bookingpage.BookingDetailsActivity;
import com.athomediva.ui.feedback.FeedbackActivity;
import com.athomediva.ui.feedback.FeedbackFragment;
import com.athomediva.ui.map.LocationPickerActivity;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.subcategory.SubCategoryActivity;
import com.athomediva.ui.webview.WebViewFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 04/04/17.
 * 1. Hit the home page meta data api.
 * 2. Update the offers/testimonial and status
 * 3. Check if location is enabled and permissions is given
 * 4. If yes fetch the address and update the view.
 * 5. Hit the home page api with latest lat/long
 * 6. If service available inflate the page
 * 7. If service not available inflate service not available page.
 * 8. If permission or location not enabled launch the permission dialog to enable
 * 9. Once enabled hit the api again.
 **/

public class HomePagePresenter extends BaseGPSPresenter<HomePageContract.View> implements HomePageContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(HomePagePresenter.class.getSimpleName());
    private static final int LOCATION_PICKER_REQ_CODE = 1001;

    private static final int BOOKING_DETAILS_REQUEST = 201;
    private static final int FEEDBACK_REQUEST = 202;
    private static final int PAYMENT_REQUEST_CODE = 1002;

    private QuikrNetworkRequest mMetaDataRequest;
    private QuikrNetworkRequest mCategoriesRequest;
    private ArrayList<Categories> mCategoriesList;
    private ArrayList<Services> mPackageList;
    private CartUpdationTask mCartUpdateTask;
    private QuikrNetworkRequest mGetBookingsReq;

    public HomePagePresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull PermissionManager permissionHelper, @NonNull CartUpdationTask task) {
        super(dataManager, context, session, permissionHelper);
        mCartUpdateTask = task;
    }

    @Override
    public void attachView(@NonNull HomePageContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        mDataManager.getBusManager().register(this);
        if (mBookingSession.getCurrentGeolocation() != null) {
            LOGD(TAG, "attachView: location already there");
            onLocationUpdateViewUpdate(mBookingSession.getCurrentGeolocation(), false);
        } else {
            LOGD(TAG, "attachView: location not present. Lets fetch in");
            getLocationFromGPS(false);
        }

        getHomePageMetaData();
        // Update the Cart Count if cart is not empty
        if (mBookingSession.getCart() != null) {
            getMvpView().updateCartMenu(mBookingSession.getCart().getServiceCount());
        }
        if (mDataManager.getPrefManager().isUserLoggedIn()) {
            getRecentBookings();
        } else {
            LOGD(TAG, "attachView : User not login");
        }

        onEventMainThread(new CartUpdateEvent());
    }

    @Override
    public void onCurrentLocationClick() {
        LOGD(TAG, "onCurrentLocationClick: ");
        Bundle bundle = new Bundle();
        if (!isViewAttached()) return;
        getMvpView().launchActivityForResult(bundle, LocationPickerActivity.class, LOCATION_PICKER_REQ_CODE);
    }

    @Override
    public void onCodeCopyClick(String code) {
        LOGD(TAG, "onCodeCopyClick: code =" + code);
        AndroidHelper.copyTextToClipboard(mContext, code);
        ToastSingleton.getInstance().showToast(mContext.getString(R.string.coupan_code_copied_toast));
    }

    @Override
    public void onViewCartClicked() {
        LOGD(TAG, "onViewCartClicked: ");
        launchMyCartPage();
    }

    @Override
    public void onEmptyWidgetActionButtonClick(EmptySectionItem item) {
        LOGD(TAG, "onEmptyWidgetActionButtonClick: item clicked = " + item.getSectionTitle());
        if (item.getActionButtonTitle().equalsIgnoreCase(mContext.getString(R.string.enable_location))) {
            LOGD(TAG, "onEmptyWidgetActionButtonClick: enable location ");
            getLocationFromGPS(true);
        } else if (item.getActionButtonTitle().equalsIgnoreCase(mContext.getString(R.string.change_location))) {
            LOGD(TAG, "onEmptyWidgetActionButtonClick: change location");
            onCurrentLocationClick();
        } else if (item.getActionButtonTitle().equalsIgnoreCase(mContext.getString(R.string.search_location))) {
            LOGD(TAG, "onEmptyWidgetActionButtonClick: search location");
            onCurrentLocationClick();
        }
    }

    @Override
    public void onCategoryItemClick(Categories cat, SubCategories subCat) {
        LOGD(TAG, "onCategoryItemClick : ");
        launchSubCategoryPage(mCategoriesList.indexOf(cat), subCat);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (isViewAttached()) {
            getMvpView().hideProgressBar();
        }
        if (mCategoriesRequest != null) mCategoriesRequest.cancel();
        if (mMetaDataRequest != null) mMetaDataRequest.cancel();
        if (mCartUpdateTask != null) {
            mCartUpdateTask.cancel();
        }
        mDataManager.getBusManager().unregister(this);
        LOGD(TAG, "detachView: ");
    }

    @Override
    public void onViewAllClick() {
        LOGD(TAG, "onViewAllClick : ");
        launchPackagePage();
    }

    @Override
    public void onPlusClick(Services model) {
        LOGD(TAG, "onAddPackage : ");
        createOrUpdateCart(AppConstants.CART_ACTION.ADD_SERVICE, model);
    }

    @Override
    public void onMinusClick(Services model) {
        LOGD(TAG, "onRemovePackage : ");
        createOrUpdateCart(AppConstants.CART_ACTION.DELETE_SERVICE, model);
    }

    @DebugLog
    private void launchSubCategoryPage(int selectedPos, SubCategories subcat) {
        LOGD(TAG, "launchSubCategoryPage : ");
        if (!isViewAttached()) return;
        Bundle b = new Bundle();
        b.putLong(SubcatSectionContract.PARAM_SELECTED_SUBCAT, subcat.getId());
        b.putInt(SubcatSectionContract.PARAM_SELECTED_POS, selectedPos);
        getMvpView().launchActivity(b, SubCategoryActivity.class);
    }

    @DebugLog
    private void launchPackagePage() {
        LOGD(TAG, "launchPackagePage : ");
        if (!isViewAttached()) return;
        Bundle b = new Bundle();
        b.putBoolean(SubcatSectionContract.PARAM_IS_PACKAGE, true);
        getMvpView().launchActivity(b, SubCategoryActivity.class);
    }

    private void getHomePageMetaData() {
        if (mMetaDataRequest != null) {
            mMetaDataRequest.cancel();
        }

        //Update from local store meanwhile hit the api to get the latest
        handleMetaDataUpdate(mBookingSession.getMetaData());
        mMetaDataRequest =
          mDataManager.getApiManager().getAppMetaData(new QuikrNetworkRequest.Callback<APIGetMetaDataResponse>() {
              @Override
              public void onSuccess(APIGetMetaDataResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  mBookingSession.setMetaData(response);
                  handleMetaDataUpdate(response);
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGE(TAG, "onError: ");
                  //do not launch no network from here
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mMetaDataRequest), true);
              }
          });
        mMetaDataRequest.execute();
    }

    private void getCategories(boolean viaLocationPicker) {
        LOGD(TAG, "getCategories: ");
        if (mCategoriesRequest != null) {
            mCategoriesRequest.cancel();
        }

        if (mBookingSession.getCategoryData() != null) {
            LOGD(TAG, "getCategories: getting from cache");
            handleServicesSectionPageUiUpdate(mBookingSession.getCategoryData());
            return;
        }

        getMvpView().showProgressBar(mContext.getString(R.string.getting_services));
        mCategoriesRequest = mDataManager.getApiManager()
          .getAllCategories(mBookingSession.getCurrentGeolocation().getLocation().latitude,
            mBookingSession.getCurrentGeolocation().getLocation().longitude, viaLocationPicker,
            new QuikrNetworkRequest.Callback<APIListCategoriesResponse>() {
                @Override
                public void onSuccess(APIListCategoriesResponse response) {
                    LOGD(TAG, "onSuccess: ");
                    if (isViewAttached()) {
                        getMvpView().hideProgressBar();
                    }
                    handleServicesSectionPageUiUpdate(response);
                    triggerCatUpdateEvent();
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    LOGE(TAG, "onError: ");
                    onNetworkError(errorCode, errorMessage, new WeakReference<>(mCategoriesRequest), true);
                }
            });

        mCategoriesRequest.execute();
    }

    private void handleMetaDataUpdate(APIGetMetaDataResponse response) {
        LOGD(TAG, "handleMetaDataUpdate: ");

        if (!isViewAttached()) return;

        boolean forceUpdate = false;
        if (response != null && response.getData() != null && response.isSuccess()) {
            getMvpView().updateOffersView(response.getData().getOffers());
            getMvpView().updateTestimonialsView(response.getData().getTestimonials(), response.getData().getHomeText());
            if (response.getData().getAppUpdate() != null) {
                AppUpdate appUpdate = response.getData().getAppUpdate();
                boolean shouldShowOptionalUpdate = CoreLogic.shouldShowOptionalUpdate(mDataManager.getPrefManager());
                LOGD(TAG, "handleMetaDataUpdate: CoreLogic.shouldShowOptionalUpdate(mDataManager.getPrefManager()) "
                  + shouldShowOptionalUpdate);
                forceUpdate = appUpdate.getIsMandatory();
                if (appUpdate.getIsMandatory() || shouldShowOptionalUpdate) {
                    getMvpView().showForceUpdateView(appUpdate.getIsMandatory(), appUpdate.getTitle());
                }
            }
            handleReviewOrders(response.getData().getOrdersToReview(), forceUpdate);
        }
    }

    private void handleServicesSectionPageUiUpdate(APIListCategoriesResponse response) {
        LOGD(TAG, "handleServicesSectionPageUiUpdate: ");
        if (!isViewAttached() || response == null) return;
        getMvpView().resetAllServicesContainer();

        if (response.isSuccess() && response.getData() != null) {
            if (response.getData().getCategories() != null && !response.getData().getCategories().isEmpty()) {
                mCategoriesList = response.getData().getCategories();
                getMvpView().addServicesSection(mCategoriesList);
            }

            if (response.getData().getPackages() != null
              && response.getData().getPackages() != null
              && response.getData().getPackages().getList() != null
              && !response.getData().getPackages().getList().isEmpty()) {
                mPackageList = response.getData().getPackages().getList();
                getMvpView().addPackagesSection(mPackageList);
            }

            mBookingSession.updateLocationWithCategoriesLatLng(response.getData().getMeta());
            if (response.getData().getMeta() == null) {
                mDataManager.getAnalyticsManager()
                  .trackGAEventForAPIFail(AppUrls.URL_GET_CATEGORIES, GATrackerContext.Label.META_DATA_IS_EMPTY);
            }
            getMvpView().updateLocationViewVisibility(true);
            getMvpView().updateCurrentLocationView(mBookingSession.getCurrentGeolocation().getAddress());
            mBookingSession.setCategoryData(response);
        } else {
            if (response.getError() == null) return;
            long errorCode = response.getError().getCode();
            //Show locality not served view.
            if (errorCode == AppConstants.APICodes.ERROR_CODE.SERVICES_NOT_SERVED_IN_LOCATION.getErrorCode()) {
                getMvpView().addEmptyWidgetSection(AppConstants.getServicesNotAvailableItem(mContext));
            }
        }
    }

    @Override
    protected void onCurrentLocationObtained(Geolocation geolocation) {
        LOGD(TAG, "onCurrentLocationObtained: ");
        mBookingSession.resetCachedResponse();
        mBookingSession.setCurrentGeolocation(geolocation);
        if (!isViewAttached()) return;
        getMvpView().hideProgressBar();
        onLocationUpdateViewUpdate(geolocation, false);
    }

    @Override
    protected void onLocationFailed() {
        LOGD(TAG, "onLocationFailed: ");
        if (!isViewAttached()) return;
        getMvpView().hideProgressBar();
        onLocationUpdateViewUpdate(null, false);
    }

    private void onLocationUpdateViewUpdate(Geolocation geolocation, boolean viaLocationPicker) {
        LOGD(TAG, "onLocationUpdateViewUpdate: ");
        getMvpView().resetAllServicesContainer();
        if (geolocation != null) {
            LOGD(TAG, "onLocationUpdateViewUpdate: geolocation.getAddress() = " + geolocation.getAddress());
            getMvpView().updateLocationViewVisibility(true);
            getMvpView().updateCurrentLocationView(geolocation.getAddress());
            getCategories(viaLocationPicker);
        } else {
            getMvpView().addEmptyWidgetSection(AppConstants.getLocationUnavailableItem(mContext));
            getMvpView().updateLocationViewVisibility(false);
        }
    }

    @Override
    protected void onGPSLocationInProgress() {
        if (!isViewAttached()) return;
        getMvpView().showProgressBar(mContext.getString(R.string.getting_location));
        LOGD(TAG, "onGPSLocationInProgress: ");
    }

    @Override
    public void onRateServiceClick(Booking booking) {
        LOGD(TAG, "onRateServiceClick : ");
        if (booking != null) {
            Bundle b = new Bundle();
            b.putString(FeedbackActivity.FRAG_TAG_NAME, FeedbackFragment.FRAG_TAG);
            b.putString(FeedbackActContract.PARAM_SUBTITLE, booking.getRateServiceTitle());
            b.putString(FeedbackActContract.PARAM_BOOKING_ID, booking.getOrderId());
            getMvpView().launchActivityForResult(b, FeedbackActivity.class, FEEDBACK_REQUEST);
        }
    }

    @Override
    public void onBookingClick(Booking booking) {
        LOGD(TAG, "onBookingClick : ");
        if (booking != null) {
            Bundle b = new Bundle();
            b.putString(BookingDetailsContract.PARAM_BOOKING_ID, booking.getOrderId());
            getMvpView().launchActivityForResult(b, BookingDetailsActivity.class, BOOKING_DETAILS_REQUEST);
        }
    }

    @Override
    public void onPayonlineClick(Booking booking) {
        LOGD(TAG, "onPayonlineClick : ");
        if (booking != null && !TextUtils.isEmpty(booking.getPaymentLink())) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.pay_online_cta));
            bundle.putString(WebViewFragment.PARAM_URL, booking.getPaymentLink());
            bundle.putString(WebViewFragment.PARAM_INTERCEPT_URL, booking.getVerifyPaymentUrl());
            bundle.putLong(WebViewFragment.PARAM_REDIRECTION_TIME, AppConstants.PAYMENT_REDIRECT_DURATION);
            bundle.putString(WebViewFragment.PARAM_REDIRECTION_MSG,
              mContext.getString(R.string.payment_success_redirect_msg));
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOGD(TAG, "onActivityResult: requestCode" + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LOCATION_PICKER_REQ_CODE && data != null) {
                LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE");
                Geolocation geolocation = data.getParcelableExtra(LocationPickerContract.PARAM_SELECTED_GEOLOCATION);
                if (geolocation != null) {
                    LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE address =" + geolocation.getAddress());
                    LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE address user saved address id ="
                      + geolocation.getUserAddressId());

                    /**
                     * If we already have cart items and the location is changed we will show user warning first
                     * that cart will be emptied.
                     */
                    if (mBookingSession.getCart() != null && mBookingSession.getCart().getServiceCount() > 0) {
                        getMvpView().showCartEmptyWarningView(mContext.getString(R.string.cart_warning_title),
                          mContext.getString(R.string.cart_warning_message,
                            mBookingSession.getCurrentGeolocation().getAddress()), v -> {
                              LOGD(TAG, "onActivityResult: empty the cart");
                              updateViewAndSessionOnGeolocationChange(geolocation);
                          });
                    } else {
                        updateViewAndSessionOnGeolocationChange(geolocation);
                    }
                } else {
                    LOGE(TAG, "onActivityResult: geolocation is null ");
                }
            } else if (requestCode == BOOKING_DETAILS_REQUEST
              || requestCode == FEEDBACK_REQUEST
              || requestCode == PAYMENT_REQUEST_CODE) {
                LogUtils.LOGD(TAG, "onActivityResult : ");
                // Refresh the recent bookings coming from feedback ,booking details,payment
                // if we have have any changes we need to update
                getRecentBookings();
            }
        }
    }

    private void updateViewAndSessionOnGeolocationChange(Geolocation geolocation) {
        LOGD(TAG, "updateViewAndSessionOnGeolocationChange: ");
        mBookingSession.resetCachedResponse();
        mBookingSession.setCurrentGeolocation(geolocation);
        onLocationUpdateViewUpdate(geolocation, true);
    }

    private void launchMyCartPage() {
        LOGD(TAG, "launchMyCartPage : ");
        Bundle bundle = new Bundle();
        if (mBookingSession.getCart() != null) {
            bundle.putString(MyCartContract.PARAM_CART_ID, mBookingSession.getCart().getCartID());
        }
        getMvpView().launchActivity(bundle, MyCartActivity.class);
    }

    private void createOrUpdateCart(AppConstants.CART_ACTION action, Services service) {
        LOGD(TAG, "createOrUpdateCart : ");
        if (service == null || !isViewAttached() || mBookingSession.getCart() == null) return;

        if (mCartUpdateTask != null) mCartUpdateTask.cancel();

        getMvpView().showProgressBar("");

        BookingRequestData data = mBookingSession.getCart().updateServices(action.getAction(), service.getServiceId());
        mCartUpdateTask.execute(data, new QuikrNetworkRequest.Callback<APICartResponse>() {
            @Override
            public void onSuccess(APICartResponse response) {
                LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
            }
        }, this);
    }

    private void getRecentBookings() {
        LOGD(TAG, "getRecentBookings : ");
        if (!isViewAttached()) return;

        if (mGetBookingsReq != null) {
            mGetBookingsReq.cancel();
        }

        mGetBookingsReq = mDataManager.getApiManager().getMyBookings(new QuikrNetworkRequest.Callback<MyBookingResponse>() {
            @Override
            public void onSuccess(MyBookingResponse response) {
                LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                if (response != null) {
                    if (response.isSuccess() && response.getData() != null) {

                        getMvpView().updateRecentBookingsView(response.getData().getRecent());
                    } else if (!response.isSuccess() && response.getError() != null) {
                        mDataManager.getAnalyticsManager()
                          .trackGAEventForAPIFail(AppUrls.URL_GET_BOOKINGS, response.getError());
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetBookingsReq), true);
            }
        });
        mGetBookingsReq.execute();
    }

    public void updateServices() {
        LogUtils.LOGD(TAG, "updateServices : ");
        CoreLogic.updateServices(mBookingSession.getCart().getServicesMap(), mPackageList);
        getMvpView().refreshPackages(mPackageList);
    }

    private void triggerCatUpdateEvent() {
        LogUtils.LOGD(TAG, "triggerCatUpdateEvent : ");
        CategoryLoadEvent event = new CategoryLoadEvent();
        mDataManager.getBusManager().post(event);
    }

    private void handleReviewOrders(ArrayList<OrderInfo> ordersToReview, boolean forceUpdate) {
        LogUtils.LOGD(TAG, "handleReviewOrders : ");
        if (ordersToReview != null && !ordersToReview.isEmpty() && !forceUpdate) {
            LogUtils.LOGD(TAG, "handleReviewOrders : Orders To Review Page");
            launchFeedbackPage(ordersToReview.get(0));
        } else {
            LogUtils.LOGD(TAG, "handleReviewOrders : hide ReviewOrders  forceUpdate:" + forceUpdate);
        }
    }

    private void launchFeedbackPage(OrderInfo orderInfo) {
        LogUtils.LOGD(TAG, "launchFeedbackPage : ");
        if (orderInfo != null) {
            Bundle b = new Bundle();
            b.putString(FeedbackActivity.FRAG_TAG_NAME, FeedbackFragment.FRAG_TAG);
            b.putString(FeedbackActContract.PARAM_SUBTITLE, orderInfo.getRateServiceTitle());
            b.putString(FeedbackActContract.PARAM_BOOKING_ID, String.valueOf(orderInfo.getOrderId()));
            b.putBoolean(FeedbackActContract.PARAM_MANDATORY, orderInfo.isMandatory());
            getMvpView().launchActivityForResult(b, FeedbackActivity.class, FEEDBACK_REQUEST);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(final CartUpdateEvent event) {
        LOGD(TAG, "onEvent CartUpdateEvent Event");
        if (event != null && isViewAttached() && mBookingSession.getCart() != null) {
            getMvpView().updateCartMenu(mBookingSession.getCart().getServiceCount());
            updateServices();
        }
    }

    @Subscribe
    public void onEventMainThread(SpecialDateUpdateEvent event) {
        LOGD(TAG, "onEventMainThread : SpecialDateUpdateEvent");
        if (mDataManager.getPrefManager().isUserLoggedIn() && !DataValidator.isSpecialDatesUpdated(
          mDataManager.getPrefManager().getUserDetails()) && mBookingSession.isSpecialOfferPopupEnabled()) {
            LogUtils.LOGD(TAG, "attachView : Special Offer is shown");
            mBookingSession.setSpecialDatePopupEnabled(false);
            getMvpView().launchSpecialDateOffer();
        } else {
            LogUtils.LOGD(TAG, "attachView : Special Offer is hidden");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CategoryUpdateEvent event) {
        LOGD(TAG, "onEventMainThread : Category Load Event");
        // Refresh the selected category
        // First match the category Id
        if (mBookingSession.getCategoryData() != null) {
            handleServicesSectionPageUiUpdate(mBookingSession.getCategoryData());
        }
    }
}
