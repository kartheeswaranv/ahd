package com.athomediva.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.DataValidator;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.busevents.CategoryUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.APICreateOrderResponse;
import com.athomediva.data.models.remote.APIDateTimeSlotResponse;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartData;
import com.athomediva.data.models.remote.CreateOrderError;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.PaymentPendingContract;
import com.athomediva.mvpcontract.ReviewOrderContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.address.MyAddressFragment;
import com.athomediva.ui.bookingpage.PaymentPendingFragment;
import com.athomediva.ui.bookingpage.ReviewOrderActivity;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.athomediva.ui.offers.InstantCouponActivity;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;
import static com.athomediva.ui.FragmentContainerActivity.PARAM_FRAG_TAG_NAME;

/**
 * Created by kartheeswaran on 27/04/17.
 */

public class MyCartPresenter extends BasePresenter<MyCartContract.View> implements MyCartContract.Presenter {
    public static final int ADDRESS_REQUEST_CODE = 301;
    private static final String TAG = LogUtils.makeLogTag(MyCartPresenter.class.getSimpleName());
    private static final int LOGIN_REQ_FOR_SCHEDULE = 302;
    private static final int LOGIN_REQ_FOR_ADDRESS = 303;
    private static final int PAYMENT_REQUEST_CODE = 304;

    private Address mAddress;
    private QuikrNetworkRequest mGeUserDetails;
    private QuikrNetworkRequest mGetTimeSlots;
    private QuikrNetworkRequest mCreateOrderReq;
    private QuikrNetworkRequest mGetCartDetails;
    private QuikrNetworkRequest mCategoriesRequest;
    private UserSession mUserSession;
    private CartBucketItem mSelectedBucket;
    private CartData mCartData;
    private CartUpdationTask mCartUpdateTask;
    private Bundle mBundle;

    @Inject
    public MyCartPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull UserSession userSession, @NonNull CartUpdationTask task, @NonNull Bundle bundle) {
        super(dataManager, context, session);
        LOGD(TAG, "MyCartPresenter : ");
        mBundle = bundle;
        mCartUpdateTask = task;
        mUserSession = userSession;
    }

    @Override
    public void attachView(@NonNull MyCartContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView : ");
        mDataManager.getBusManager().register(this);
        if (mBundle != null && mBundle.containsKey(DeeplinkConstants.KEY_ACTION) && TextUtils.equals(
          mBundle.getString(DeeplinkConstants.KEY_ACTION), Intent.ACTION_VIEW)) {
            Uri uri = mBundle.getParcelable(DeeplinkConstants.KEY_DEEPLINK_URL);
            if (uri != null) {
                String cartId = uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.CART_ID);
                LogUtils.LOGD(TAG, "attachView : Deeplink Launch CartId " + cartId);
                getCartDetails(cartId);
            }
        } else if (mBundle != null && mBundle.containsKey(MyCartContract.PARAM_CART_ID) && !TextUtils.isEmpty(
          mBundle.getString(MyCartContract.PARAM_CART_ID))) {
            getCartDetails(mBundle.getString(MyCartContract.PARAM_CART_ID));
        } else if (mBookingSession.getCart() != null && !TextUtils.isEmpty(mBookingSession.getCart().getCartID())) {
            getCartDetails(mBookingSession.getCart().getCartID());
        } else {
            getMvpView().updateEmptyPageVisibility(true);
        }

        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            LogUtils.LOGD(TAG, "attachView:Update Instant Coupon ");
            getMvpView().updateInstantCouponView(mBookingSession.getMetaData().getData().getInstantCoupon(), false);
        }
    }

    @DebugLog
    @Override
    public void onScheduleConfirm(CartBucketItem model, DateSlot slot) {
        LOGD(TAG, "onEDitScheduleClick : ");
        if (slot == null || slot.getSelectedTimeSlot() == null || mBookingSession.getCart() == null) return;

        BookingRequestData data = mBookingSession.getCart()
          .updateTimeSlot(AppConstants.CART_ACTION.UPDATE_TIME.getAction(), model.getBookingId(), slot);
        updateCart(data);
    }

    @Override
    public void onScheduleClick(CartBucketItem model) {
        LogUtils.LOGD(TAG, "onScheduleClick : ");
        mSelectedBucket = model;
        if (mBookingSession.getCart() != null) {
            if (DataValidator.isUserSessionExist(mUserSession)) {
                fetchDateTimeSlots(mBookingSession.getCart().getCartID(), mSelectedBucket.getBookingId());
            } else {
                LogUtils.LOGD(TAG, "onScheduleClick : Force Login ");
                launchLoginPage(LOGIN_REQ_FOR_SCHEDULE);
            }
        } else {
            LogUtils.LOGD(TAG, "onScheduleClick : Cart is null");
        }
    }

    @DebugLog
    @Override

    public void onMinusClick(Services model) {
        LogUtils.LOGD(TAG, "onMinusClick : ");
        if (mBookingSession.getCart() != null) {
            BookingRequestData data = mBookingSession.getCart()
              .updateServices(AppConstants.CART_ACTION.DELETE_SERVICE.getAction(), model.getServiceId());
            updateCart(data);
        } else {
            LogUtils.LOGD(TAG, "onMinusClick : Cart is null");
        }
    }

    @DebugLog
    @Override
    public void onPlusClick(Services model) {
        LOGD(TAG, "onPlusClick : ");
        if (mBookingSession.getCart() != null) {
            BookingRequestData data = mBookingSession.getCart()
              .updateServices(AppConstants.CART_ACTION.ADD_SERVICE.getAction(), model.getServiceId());
            updateCart(data);
        } else {
            LogUtils.LOGD(TAG, "onPlusClick : Cart is null");
        }
    }

    @Override
    public void onAddAddressClick() {
        LOGD(TAG, "onAddAddressClick : ");
        if (DataValidator.isUserSessionExist(mUserSession)) {
            if (mUserSession.getAddressList() == null || mUserSession.getAddressList().isEmpty()) {
                getCompleteUserDetails();
            } else {
                launchAddressPage(mBookingSession.getCurrentGeolocation().getCircleName());
            }
        } else {
            LogUtils.LOGD(TAG, "onAddAddressClick : Force Login ");
            launchLoginPage(LOGIN_REQ_FOR_ADDRESS);
        }
    }

    @Override
    public void onChangeAddressClick() {
        LOGD(TAG, "onChangeAddressClick : ");
        launchAddressPage(mBookingSession.getCurrentGeolocation().getCircleName());
    }

    @Override
    public void onProceedClick() {
        LOGD(TAG, "onProceedClick : ");
        createOrder();
    }

    @Override
    public void onExploreServiceClick() {
        LOGD(TAG, "onExploreServiceClick : ");
        launchHomePage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADDRESS_REQUEST_CODE) {
                Address address = data.getParcelableExtra(MyAddressPageContract.PARAM_SELECTED_ADDRESS);
                if (address != null) {
                    mAddress = address;
                    if (isViewAttached() && mAddress != null && mBookingSession.getCart() != null) {
                        BookingRequestData reqData = mBookingSession.getCart()
                          .updateAddress(AppConstants.CART_ACTION.UPDATE_ADDRESS.getAction(), mAddress.getAddressID());
                        updateCart(reqData);

                        getMvpView().enableProceedView(isValidationSuccess());
                    } else {
                        LogUtils.LOGD(TAG, "onActivityResult : View is not valid");
                    }
                }
            } else if (requestCode == LOGIN_REQ_FOR_SCHEDULE) {
                LOGD(TAG, "onActivityResult: booking result obtained");
                if (mUserSession.getUser() != null && mBookingSession.getCart() != null) {
                    LOGD(TAG, "onActivityResult: user found");
                    fetchDateTimeSlots(mBookingSession.getCart().getCartID(), mSelectedBucket.getBookingId());
                }
            } else if (requestCode == LOGIN_REQ_FOR_ADDRESS) {
                LOGD(TAG, "onActivityResult: account result obtained");
                if (mUserSession.getUser() != null) {
                    LOGD(TAG, "onActivityResult: user found");
                    getCompleteUserDetails();
                }
            } else if (requestCode == PAYMENT_REQUEST_CODE) {
                LogUtils.LOGD(TAG, "onActivityResult : Payment paid");
                createOrder();
            }
        }
    }

    private void launchAddressPage(String circleName) {
        LogUtils.LOGD(TAG, "launchAddressPage : ");

        Bundle b = new Bundle();
        b.putString(PARAM_FRAG_TAG_NAME, MyAddressFragment.TAG);
        //b.putString(MyAddressPageContract.PARAM_CIRCLE_NAME, circleName);
        getMvpView().launchActivityForResult(b, FragmentContainerActivity.class, ADDRESS_REQUEST_CODE);
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mGeUserDetails != null) {
            mGeUserDetails.cancel();
        }

        if (!isViewAttached()) return;

        getMvpView().showProgressBar("Fetching Details");
        mGeUserDetails =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.getSuccess() && response.getData() != null) {
                          mUserSession.updateDataFromResponse(response);
                          if (mBookingSession.getCart() != null) {
                              launchAddressPage(mBookingSession.getCart().getCircleName());
                          }
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GET_USER_DETAILS, response.getError());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGeUserDetails));
              }
          });

        mGeUserDetails.execute();
    }

    @Override
    public void onInstantCouponClick(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "onInstantCouponClick: ");
        launchInstantCouponPage(coupon);
    }

    private void launchInstantCouponPage(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "launchInstantCouponPage: ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(InstantCouponContract.PARAM_COUPON_OBJ, coupon);
        getMvpView().launchActivity(bundle, InstantCouponActivity.class);
    }

    private void launchLoginPage(int requestCode) {
        LOGD(TAG, "launchLoginPage: ");
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppLauncherContract.PARAM_LAUNCHED_FOR_LOGIN, true);
        getMvpView().launchActivityForResult(bundle, AppLauncherActivity.class, requestCode);
    }

    private boolean isTimeScheduled() {
        LogUtils.LOGD(TAG, "isTimeScheduled : ");
        boolean scheduled = false;

        if (mCartData != null && mCartData.getBuckets() != null) {
            for (CartBucketItem item : mCartData.getBuckets()) {
                if (item != null && item.getScheduledDateTime() != null) {
                    scheduled = true;
                } else {
                    scheduled = false;
                    break;
                }
            }
        }

        return scheduled;
    }

    private boolean isValidationSuccess() {
        LogUtils.LOGD(TAG, "isValidationSuccess : ");
        if (mCartData != null
          && isTimeScheduled()
          && mCartData.getAddress() != null
          && mCartData.getMinOrderInfo() == null) {
            return true;
        }

        return false;
    }

    private void launchHomePage() {
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
    }

    private void handleCartResponse(APICartResponse response) {
        LogUtils.LOGD(TAG, "handleCartResponse : ");
        if (response != null && response.getData() != null) {
            getMvpView().showContainerView();
            if (CoreLogic.hasBucketsInCart(response) && mCartData != null) {
                CoreLogic.setExpandFlag(response.getData().getBuckets(), mCartData.getBuckets());
            } else {
                LogUtils.LOGD(TAG, "handleCartResponse : No Buckets in response");
            }
            mCartData = response.getData();

            if (!TextUtils.isEmpty(mCartData.getPriceChange())) {
                getMvpView().showErrorMessage(mCartData.getPriceChange(), new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        LogUtils.LOGD(TAG, "onDismiss: Price Change OK Click");
                        getCategories(response);
                    }
                });
            }

            if (CoreLogic.hasBucketsInCart(response)) {
                getMvpView().updateAddress(mCartData.getAddress());
                getMvpView().updateBuckets(mCartData.getBuckets());
                if (mCartData.getMeta() != null) {
                    getMvpView().updateHeaderInfo(mCartData.getMeta().getHeaderDescription(),
                      mCartData.getMeta().getHeaderFullDescription());
                } else {
                    // To hide the header
                    getMvpView().updateHeaderInfo(null, null);
                }
                getMvpView().showOrHideMinOrderError(mCartData.getMinOrderInfo());
                getMvpView().enableProceedView(isValidationSuccess());
            } else {
                getMvpView().updateEmptyPageVisibility(true);
            }
        }
    }

    private void updateCart(BookingRequestData data) {
        LogUtils.LOGD(TAG, "createOrUpdateCart : ");
        if (data == null || !isViewAttached()) return;

        if (mCartUpdateTask != null) mCartUpdateTask.cancel();

        getMvpView().showProgressBar("");

        mCartUpdateTask.execute(data, new QuikrNetworkRequest.Callback<APICartResponse>() {
            @Override
            public void onSuccess(APICartResponse response) {
                LogUtils.LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                handleCartResponse(response);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LogUtils.LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
            }
        }, this);
    }

    private void fetchDateTimeSlots(String cartId, String bookingId) {
        LogUtils.LOGD(TAG, "fetchDateTimeSlots : ");
        if (mGetTimeSlots != null) {
            mGetTimeSlots.cancel();
        }

        if (!isViewAttached()) return;
        getMvpView().showProgressBar("");
        mGetTimeSlots = mDataManager.getApiManager()
          .getTimeSlots(cartId, bookingId, new QuikrNetworkRequest.Callback<APIDateTimeSlotResponse>() {
              @Override
              public void onSuccess(APIDateTimeSlotResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();

                  if (response != null) {
                      if (response.isSuccess()) {
                          getMvpView().showScheduleDialog(mSelectedBucket, response);
                      } else if (response.getError() != null) {
                          if (response.getError().getCode()
                            == AppConstants.APICodes.ERROR_CODE.STYLIST_NOT_AVAILABLE.getErrorCode()) {
                              getMvpView().showErrorMessage(response.getError().getMessage());
                              mDataManager.getAnalyticsManager()
                                .trackEventGA(GATrackerContext.Category.ATHOMEDIVA,
                                  GATrackerContext.Action.STYLIST_UNAVAILABLE, GATrackerContext.Label.STYLIST_UNAVAIL_EVENT);
                          } else {
                              ToastSingleton.getInstance().showToast(response.getError().getMessage());
                          }
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GET_TIMESLOTS, response.getError());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetTimeSlots), true);
              }
          });
        mGetTimeSlots.execute();
    }

    private void createOrder() {
        LogUtils.LOGD(TAG, "createOrder : ");
        if (!isViewAttached() || mBookingSession.getCart() == null) return;

        if (mCreateOrderReq != null) mCreateOrderReq.cancel();

        getMvpView().showProgressBar("");

        mCreateOrderReq = mDataManager.getApiManager()
          .createOrder(mBookingSession.getCart().getCartID(), mBookingSession.getUtmData(),
            new QuikrNetworkRequest.Callback<APICreateOrderResponse>() {
                @Override
                public void onSuccess(APICreateOrderResponse response) {
                    LogUtils.LOGD(TAG, "onSuccess : ");
                    if (!isViewAttached()) return;
                    getMvpView().hideProgressBar();
                    if (response != null) {
                        if (response.isSuccess()) {
                            launchReviewOrderPage(response.getData());
                        } else {
                            if (response.getError() != null) {
                                if (response.getError().getCode()
                                  == AppConstants.APICodes.ERROR_CODE.PAYMENT_PENDING_ERROR.getErrorCode()) {
                                    launchPaymentPendingPage(response.getError());
                                }

                                mDataManager.getAnalyticsManager()
                                  .trackGAEventForAPIFail(AppUrls.URL_CREATE_ORDER, response.getError().getCode(),
                                    response.getError().getMessage());
                            }
                        }
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    LogUtils.LOGD(TAG, "onError : ");
                    if (!isViewAttached()) return;
                    onNetworkError(errorCode, errorMessage, new WeakReference<>(mCreateOrderReq), true);
                }
            });
        mCreateOrderReq.execute();
    }

    private void getCartDetails(String cartId) {
        LogUtils.LOGD(TAG, "createOrder : ");
        if (!isViewAttached() || TextUtils.isEmpty(cartId)) {
            LogUtils.LOGD(TAG, "getCartDetails : Cart is Empty ");
            getMvpView().updateEmptyPageVisibility(true);
            return;
        }

        if (mGetCartDetails != null) mGetCartDetails.cancel();

        getMvpView().showProgressBar("");

        mGetCartDetails =
          mDataManager.getApiManager().getCartDetails(cartId, new QuikrNetworkRequest.Callback<APICartResponse>() {
              @Override
              public void onSuccess(APICartResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.isSuccess()) {
                          mBookingSession.updateCartResponse(response);
                          if (response.getData() != null) {
                              mCartData = response.getData();
                          }
                          handleCartResponse(response);
                          //If User Address Id Is selected on home Page then
                          //we need to call the api to update the address

                          if (mCartData != null && mCartData.getAddress() == null) {
                              updateAddressOnLaunch();
                          } else {
                              LogUtils.LOGD(TAG, "onSuccess : Address is already Updated");
                          }
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GET_CART_DETAILS, response.getError().getCode(),
                              response.getError().getMessage());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetCartDetails), true);
              }
          });
        mGetCartDetails.execute();
    }

    private void getCategories(APICartResponse cartResponse) {
        LOGD(TAG, "getCategories: ");
        if (mCategoriesRequest != null) {
            mCategoriesRequest.cancel();
        }

        double lat = 0;
        double lang = 0;

        try {
            if (cartResponse != null && cartResponse.getData() != null && cartResponse.getData().getAddress() != null) {
                if (!TextUtils.isEmpty(cartResponse.getData().getAddress().getLat()) && !TextUtils.isEmpty(
                  cartResponse.getData().getAddress().getLon())) {
                    lat = Double.valueOf(cartResponse.getData().getAddress().getLat());
                    lang = Double.valueOf(cartResponse.getData().getAddress().getLon());
                }
            }
        } catch (Exception e) {

        } finally {
            if (lat == 0 && lang == 0) return;
        }

        getMvpView().showProgressBar(mContext.getString(R.string.getting_services));
        mCategoriesRequest = mDataManager.getApiManager()
          .getAllCategories(lat, lang, true, new QuikrNetworkRequest.Callback<APIListCategoriesResponse>() {
              @Override
              public void onSuccess(APIListCategoriesResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (isViewAttached()) {
                      getMvpView().hideProgressBar();

                      Geolocation geolocation = new Geolocation(cartResponse.getData().getAddress().getLat(),
                        cartResponse.getData().getAddress().getLon());
                      geolocation.setAddress(cartResponse.getData().getAddress().getAddress());
                      geolocation.setUserAddressId(cartResponse.getData().getAddress().getAddressID());
                      mBookingSession.setCurrentGeolocation(geolocation);
                      mBookingSession.setCategoryData(response);
                      triggerCategoryUpdateEvent();
                      mBookingSession.updateCartResponse(cartResponse);
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGE(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mCategoriesRequest), true);
              }
          });

        mCategoriesRequest.execute();
    }

    private void updateAddressOnLaunch() {
        LogUtils.LOGD(TAG, "updateAddressOnLaunch : ");
        if (mBookingSession.getCart() == null) {
            LogUtils.LOGD(TAG, "updateAddressOnLaunch : Cart is null");
            return;
        }

        if (mBookingSession.getCurrentGeolocation() != null && !TextUtils.isEmpty(
          mBookingSession.getCurrentGeolocation().getUserAddressId())) {
            BookingRequestData reqData = mBookingSession.getCart()
              .updateAddress(AppConstants.CART_ACTION.UPDATE_ADDRESS.getAction(),
                mBookingSession.getCurrentGeolocation().getUserAddressId());
            updateCart(reqData);
        } else {
            LogUtils.LOGD(TAG, "updateAddressOnLaunch : Address is not match with user address");
        }
    }

    private void launchReviewOrderPage(CartData response) {
        LogUtils.LOGD(TAG, "launchReviewOrderPage : ");
        if (response != null) {
            Bundle b = new Bundle();
            b.putParcelable(ReviewOrderContract.PARAM_CART_RESPONSE, response);
            getMvpView().launchActivity(b, ReviewOrderActivity.class);
            getMvpView().finish();
        }
    }

    private void launchPaymentPendingPage(CreateOrderError error) {
        LogUtils.LOGD(TAG, "launchPaymentPendingPage : ");
        if (error != null) {
            Bundle b = new Bundle();
            b.putParcelable(PaymentPendingContract.PARAM_RESPONSE, error);
            b.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, PaymentPendingFragment.FRAG_TAG);
            getMvpView().launchActivityForResult(b, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }

    private void triggerCategoryUpdateEvent() {
        LogUtils.LOGD(TAG, "triggerCategoryUpdateEvent: ");
        CategoryUpdateEvent event = new CategoryUpdateEvent();
        mDataManager.getBusManager().post(event);
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView : ");
        mDataManager.getBusManager().unregister(this);
        if (mCartUpdateTask != null) {
            mCartUpdateTask.cancel();
        }

        if (mGetTimeSlots != null) {
            mGetTimeSlots.cancel();
        }

        if (mGeUserDetails != null) {
            mGeUserDetails.cancel();
        }

        if (mCreateOrderReq != null) {
            mCreateOrderReq.cancel();
        }

        if (mGetCartDetails != null) {
            mGetCartDetails.cancel();
        }

        if (mCategoriesRequest != null) {
            mCategoriesRequest.cancel();
        }

        super.detachView();
    }
}
