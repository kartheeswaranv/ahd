package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.Referral;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.FeedbackSuccessContract;
import com.athomediva.mvpcontract.SubcatSectionContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.subcategory.SubCategoryActivity;
import hugo.weaving.DebugLog;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class FeedbackSuccessPresenter extends BasePresenter<FeedbackSuccessContract.View>
  implements FeedbackSuccessContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(FeedbackSuccessPresenter.class.getSimpleName());

    private Bundle mBundle;
    private String mBookingId;
    private String mBucketName;
    private boolean mEnableRate;

    public FeedbackSuccessPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session,
      @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context, session);
        LogUtils.LOGD(TAG, "FeedbackSuccessPresenter : ");
        mBundle = bundle;
        getExtras();
    }

    @Override
    public void attachView(@NonNull FeedbackSuccessContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        String refCode = getReferalCode();
        if (!TextUtils.isEmpty(refCode)) {
            getMvpView().updateReferalView(refCode);
        }
        getMvpView().updateSubTitle(mBucketName);
        getMvpView().enableRateUsView(mEnableRate);

        if (mBookingSession.getCategoryData() != null
          && mBookingSession.getCategoryData().getData() != null
          && mBookingSession.getCategoryData().getData().getFeedbackCatg() != null
          && !mBookingSession.getCategoryData().getData().getFeedbackCatg().isEmpty()) {
            List<SubCategories> list = mBookingSession.getCategoryData().getData().getFeedbackCatg();
            getMvpView().updateSubCatView(list);
        } else {
            LogUtils.LOGD(TAG, "attachView : ");
            getMvpView().hideSubCatView();
        }
    }

    @DebugLog
    @Override
    public void onSubcatItemClick(SubCategories model) {
        LogUtils.LOGD(TAG, "onSubcatItemClick : ");
        if (mBookingSession.getCategoryData() != null && mBookingSession.getCategoryData().getData() != null) {
            int catPos =
              CoreLogic.getCategorytPosition(mBookingSession.getCategoryData().getData().getCategories(), model.getId());
            launchSubCatPage(catPos, model.getId());
        }
    }

    @Override
    public void onHomeClick() {
        LogUtils.LOGD(TAG, "onHomeClick : ");
        launchHomePage();
    }

    @Override
    public void onFacebookShareClick() {
        LogUtils.LOGD(TAG, "onFacebookShareClick : ");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_FB_LINK));
        AndroidHelper.launchViewForAction(mContext, intent);
    }

    @Override
    public void onTwitterShareClick() {
        LogUtils.LOGD(TAG, "onTwitterShareClick : ");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.AHD_TWITTER_LINK));
        AndroidHelper.launchViewForAction(mContext, intent);
    }

    @Override
    public void onCopyToClipboardClick() {
        LogUtils.LOGD(TAG, "onCopyToClipboardClick : ");
        String refCode = getReferalCode();
        if (!TextUtils.isEmpty(refCode)) {
            AndroidHelper.copyTextToClipboard(mContext, refCode);
            ToastSingleton.getInstance().showToast(mContext.getString(R.string.referral_code_copied_toast));
        }
    }

    private void getExtras() {
        LogUtils.LOGD(TAG, "getExtras : ");
        if (mBundle != null) {
            mBucketName = mBundle.getString(FeedbackActContract.PARAM_SUBTITLE);
            mBookingId = mBundle.getString(FeedbackActContract.PARAM_BOOKING_ID);
            mEnableRate = mBundle.getBoolean(FeedbackActContract.PARAM_IS_ENABLE_RATE);
        }
    }

    private String getReferalCode() {
        LogUtils.LOGD(TAG, "getReferalCode : ");
        Referral ref = mDataManager.getPrefManager().getUserDetails().getReferral();
        if (ref != null && ref.getIfReferal() != null) return ref.getIfReferal();

        return null;
    }

    private void launchHomePage() {
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.putExtra(AHDMainViewContract.PARAM_NAVIGATION_TAG, mContext.getString(R.string.home));
        intent.putExtra(AHDMainViewContract.PARAM_RELOAD_HOME_PAG, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
        getMvpView().finish();
    }

    private void launchSubCatPage(int catPos, long subCatId) {
        LogUtils.LOGD(TAG, "launchSubCatPage : ");
        Bundle b = new Bundle();
        b.putLong(SubcatSectionContract.PARAM_SELECTED_SUBCAT, subCatId);
        b.putInt(SubcatSectionContract.PARAM_SELECTED_POS, catPos);
        getMvpView().launchActivity(b, SubCategoryActivity.class);
        getMvpView().finish();
    }

    @Override
    public void onRateOnPlaystoreClick() {
        LogUtils.LOGD(TAG, "onRateOnPlaystoreClick : ");
        AndroidHelper.goToPlayStore(mContext);
    }
}
