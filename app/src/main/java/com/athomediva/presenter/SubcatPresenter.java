package com.athomediva.presenter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.busevents.CartUpdateEvent;
import com.athomediva.data.busevents.CategoryUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.CartModel;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.SubcatSectionContract;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.offers.InstantCouponActivity;
import hugo.weaving.DebugLog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 17/04/17.
 */

public class SubcatPresenter extends BasePresenter<SubcatSectionContract.View> implements SubcatSectionContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(SubcatPresenter.class.getSimpleName());

    private final Bundle mBundle;

    private ArrayList<Categories> mCategoriesList;
    private ArrayList<Services> mPackages;
    private String mPackageIcon;
    private long mSelectedCategoryId;
    private long mSubCatId;
    private boolean mIsPackage;

    @Inject
    public SubcatPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull Bundle b) {
        super(dataManager, context, session);
        mBundle = b;
        LogUtils.LOGD(TAG, "SubcatPresenter : ");
    }

    @Override
    public void attachView(@NonNull SubcatSectionContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        mDataManager.getBusManager().register(this);
        getDatasFromSession();
        updateDatasFromBundle(mBundle);
        onEventMainThread(new CartUpdateEvent());
        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            LogUtils.LOGD(TAG, "attachView:Update Instant Coupon ");
            getMvpView().updateInstantCouponView(mBookingSession.getMetaData().getData().getInstantCoupon(), false);
        }
    }

    @Override
    public void detachView() {
        super.detachView();
        LogUtils.LOGD(TAG, "detachView : ");
        mDataManager.getBusManager().unregister(this);
    }

    private void updateDatasFromBundle(Bundle bundle) {
        LogUtils.LOGD(TAG, "fetchDatasFromBundle : ");
        int selectedCatPos = 0;
        long subcatId = -1;
        boolean isPackage = false;
        if (bundle != null) {
            Uri uri = AndroidHelper.getDeepLinkUrl(bundle);
            if (uri == null) {
                subcatId = bundle.getLong(SubcatSectionContract.PARAM_SELECTED_SUBCAT);
                selectedCatPos = bundle.getInt(SubcatSectionContract.PARAM_SELECTED_POS, 0);
                isPackage = bundle.getBoolean(SubcatSectionContract.PARAM_IS_PACKAGE);
            } else {
                String catId = uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.CAT_ID);
                if (!TextUtils.isEmpty(catId)) {
                    Long cat = Long.parseLong(catId);
                    selectedCatPos = CoreLogic.getCategoryPosition(mCategoriesList, cat);
                } else {
                    LogUtils.LOGD(TAG, "updateDatasFromBundle : No Parameters ");
                }
            }
        }

        updateView(selectedCatPos, subcatId, isPackage);
    }

    private void getDatasFromSession() {
        LogUtils.LOGD(TAG, "getDatasFromSession : ");

        if (mBookingSession.getCategoryData() != null && mBookingSession.getCategoryData().getData() != null) {
            if (mBookingSession.getCategoryData().getData().getCategories() != null) {
                mCategoriesList = mBookingSession.getCategoryData().getData().getCategories();
            }

            if (mBookingSession.getCategoryData().getData().getPackages() != null
              && mBookingSession.getCategoryData().getData().getPackages().getList() != null
              && !mBookingSession.getCategoryData().getData().getPackages().getList().isEmpty()) {
                mPackages = mBookingSession.getCategoryData().getData().getPackages().getList();
                mPackageIcon = mBookingSession.getCategoryData().getData().getPackages().getIcon();
            }
        }
    }

    private void updateView(int selectedCatPos, long subCatId, boolean ispackage) {
        mSubCatId = subCatId;
        if (ispackage) {
            if (mPackages != null && mPackages.size() > 0) {
                LogUtils.LOGD(TAG, "attachView : Package Update ");
                getMvpView().launchPackageFragment(mPackages);
            }
            getMvpView().updateToolbar(mContext.getString(R.string.packages));
        } else if (mCategoriesList != null && !mCategoriesList.isEmpty() && selectedCatPos < mCategoriesList.size()) {
            mSelectedCategoryId = mCategoriesList.get(selectedCatPos).getId();
            int pos = 0;
            if (subCatId != 0) {
                pos = CoreLogic.getSubCatPosition(mCategoriesList.get(selectedCatPos).getSubcategories(), subCatId);
            }

            getMvpView().updateSubCats(mCategoriesList.get(selectedCatPos).getSubcategories(), pos);
            getMvpView().updateToolbar(mCategoriesList.get(selectedCatPos).getName());
        }
    }

    @Override
    public void onViewCartClick() {
        LogUtils.LOGD(TAG, "onViewCartClick : ");

        if (mBookingSession.getCart() != null) {
            launchMyCartPage(mBookingSession.getCart().getCartID());
        } else {
            LogUtils.LOGD(TAG, "onViewCartClick : Cart is null");
        }
    }

    @DebugLog
    @Override
    public void onSubCatClicked(int group) {
        LogUtils.LOGD(TAG, "onSubCatClicked : ");
    }

    @DebugLog
    @Override
    public void onCategoryClick(Categories category) {
        LogUtils.LOGD(TAG, "onCategoryClick : ");
        if (category != null) {
            getMvpView().hideCategories();
            getMvpView().updateToolbar(category.getName());
            if (category.getId() == -1) {
                getMvpView().launchPackageFragment(mPackages);
            } else {
                getMvpView().updateSubCats(category.getSubcategories(), 0);
            }
        }
    }

    @Override
    public void onInstantCouponClick(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "onInstantCouponClick: ");
        launchInstantCouponPage(coupon);
    }

    private void launchInstantCouponPage(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "launchInstantCouponPage: ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(InstantCouponContract.PARAM_COUPON_OBJ, coupon);
        getMvpView().launchActivity(bundle, InstantCouponActivity.class);
    }

    @Override
    public void onServicesUpdate() {
        LogUtils.LOGD(TAG, "onServicesUpdate : ");
        if (mBookingSession.getCart() != null) {
            CartModel model =
              new CartModel(mBookingSession.getCart().getServiceCount(), mBookingSession.getCart().getAmount());
            getMvpView().updateCart(model);
            getMvpView().updateMinOrderError(mBookingSession.getCart().getMinimumOrderError());
        } else {
            LogUtils.LOGD(TAG, "onServicesUpdate : Cart is null");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(final CartUpdateEvent event) {
        LogUtils.LOGD(TAG, "onEvent CartUpdateEvent Event");
        if (event != null && isViewAttached()) {
            getMvpView().updateTabs();
            onServicesUpdate();
        }
    }

    @Override
    public void onToolbarClick() {
        LogUtils.LOGD(TAG, "onToolbarClick : ");
        if (mCategoriesList != null) {
            List<Categories> list = new ArrayList<>();
            list.addAll(mCategoriesList);
            list.add(new Categories(-1, mContext.getString(R.string.packages), mPackageIcon));
            getMvpView().showCategories(list);
        }
    }

    public void launchMyCartPage(String cartId) {
        LogUtils.LOGD(TAG, "launchMyCartPage : ");
        Bundle bundle = new Bundle();
        bundle.putString(MyCartContract.PARAM_CART_ID, cartId);
        getMvpView().launchActivityForResult(bundle, MyCartActivity.class, 100);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CategoryUpdateEvent event) {
        LOGD(TAG, "onEventMainThread : Category Load Event");
        // Refresh the selected category
        // First match the category Id
        if (mBookingSession != null && mBookingSession.getCategoryData() != null) {
            getDatasFromSession();
            updateDatasFromBundle(createBundleForParams());
        }
    }

    private Bundle createBundleForParams() {
        if (mBookingSession != null && mBookingSession.getCategoryData() != null) {
            Bundle b = new Bundle();
            //int subCatPos = CoreLogic.getSubCatPosition(mSubCatId)

            int categoryPos = CoreLogic.getCategoryPosition(mCategoriesList, mSubCatId);
            if (categoryPos == -1) {
                categoryPos = CoreLogic.getCategoryPosition(mCategoriesList, mSelectedCategoryId);
            } else {
                b.putLong(SubcatSectionContract.PARAM_SELECTED_SUBCAT, mSubCatId);
            }
            if (mIsPackage && (mPackages == null || mPackages.size() == 0)) {
                mIsPackage = false;
            }
            b.putBoolean(SubcatSectionContract.PARAM_IS_PACKAGE, mIsPackage);
            if (categoryPos > 0) {
                b.putInt(SubcatSectionContract.PARAM_SELECTED_POS, categoryPos);
            } else {
                b.putInt(SubcatSectionContract.PARAM_SELECTED_POS, 0);
            }

            return b;
        }
        return null;
    }
}

