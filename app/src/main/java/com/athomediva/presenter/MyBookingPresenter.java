package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.data.models.remote.MyBookingResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.MyBookingsPageContract;
import com.athomediva.ui.bookingpage.BookingDetailsActivity;
import com.athomediva.ui.feedback.FeedbackActivity;
import com.athomediva.ui.feedback.FeedbackFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class MyBookingPresenter extends BasePresenter<MyBookingsPageContract.View>
  implements MyBookingsPageContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(MyBookingPresenter.class.getSimpleName());

    private static final int BOOKING_DETAILS_REQUEST = 201;
    private static final int FEEDBACK_REQUEST = 202;

    private QuikrNetworkRequest mGetBookingsReq;
    private ArrayList<String> mTitleList;

    public MyBookingPresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        super(dataManager, context);
    }

    @Override
    public void attachView(@NonNull MyBookingsPageContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        // To initiate the bookings adapter with null values default
        // once the api success fill with real datas
        mTitleList = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.my_bookings_tabs)));
        getMvpView().updateBookingsView(mTitleList, getBookingsList(null));
        getAllBookings(false);
    }

    @Override
    public void onItemClick(Booking booking) {
        LogUtils.LOGD(TAG, "onItemClick : ");
        if (booking != null) {
            Bundle b = new Bundle();
            b.putString(BookingDetailsContract.PARAM_BOOKING_ID, booking.getOrderId());
            getMvpView().launchActivityForResult(b, BookingDetailsActivity.class, BOOKING_DETAILS_REQUEST);
        }
    }

    @Override
    public void onRateMyServiceClick(Booking booking) {
        LogUtils.LOGD(TAG, "onRateMyServiceClick : ");
        if (booking != null) {
            Bundle b = new Bundle();
            b.putString(FeedbackActivity.FRAG_TAG_NAME, FeedbackFragment.FRAG_TAG);
            b.putString(FeedbackActContract.PARAM_SUBTITLE, booking.getRateServiceTitle());
            b.putString(FeedbackActContract.PARAM_BOOKING_ID, booking.getOrderId());
            getMvpView().launchActivityForResult(b, FeedbackActivity.class, FEEDBACK_REQUEST);
        }
    }

    private void getAllBookings(boolean pullToRefresh) {
        LogUtils.LOGD(TAG, "getAllBookings : ");

        if (!isViewAttached()) return;

        if (mGetBookingsReq != null) {
            mGetBookingsReq.cancel();
        }

        if (!pullToRefresh) getMvpView().showProgressBar("");

        mGetBookingsReq = mDataManager.getApiManager().getMyBookings(new QuikrNetworkRequest.Callback<MyBookingResponse>() {
            @Override
            public void onSuccess(MyBookingResponse response) {
                LogUtils.LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                hideProgress(pullToRefresh);
                if (response != null) {
                    if (response.isSuccess() && response.getData() != null) {
                        getMvpView().updateBookingsView(mTitleList, getBookingsList(response.getData()));
                    } else if (response != null && !response.isSuccess() && response.getError() != null) {
                        mDataManager.getAnalyticsManager()
                          .trackGAEventForAPIFail(AppUrls.URL_GET_BOOKINGS, response.getError());

                        if (!TextUtils.isEmpty(response.getError().getMessage())) {
                            ToastSingleton.getInstance().showToast(response.getError().getMessage());
                        }
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                if (!isViewAttached()) return;
                onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetBookingsReq), true);
                hideProgress(pullToRefresh);
            }
        });
        mGetBookingsReq.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (resultCode == RESULT_OK && (requestCode == BOOKING_DETAILS_REQUEST || requestCode == FEEDBACK_REQUEST)) {
            if (isViewAttached()) {
                LogUtils.LOGD(TAG, "onActivityResult : Refresh All Bookings");
                getAllBookings(false);
            }
        }
    }

    @Override
    public void onRefreshClick() {
        LogUtils.LOGD(TAG, "onRefreshClick : ");
        getAllBookings(true);
    }

    @Override
    public void detachView() {
        if (isViewAttached()) {
            getMvpView().hideProgressBar();
        }
        if (mGetBookingsReq != null) mGetBookingsReq.cancel();
        super.detachView();
    }

    private void hideProgress(boolean pullToRefresh) {
        LogUtils.LOGD(TAG, "hideProgress : ");
        if (pullToRefresh) {
            getMvpView().hideSwipeRefreshView();
        } else {
            getMvpView().hideProgressBar();
        }
    }

    private ArrayList<ArrayList<Booking>> getBookingsList(MyBookingResponse.Data data) {
        LogUtils.LOGD(TAG, "getBookingsList : ");
        ArrayList<ArrayList<Booking>> combinedList = new ArrayList<>();
        if (data != null) {
            combinedList.add(data.getUpcoming());
            combinedList.add(data.getPast());
        } else {
            combinedList.add(null);
            combinedList.add(null);
        }

        return combinedList;
    }
}

