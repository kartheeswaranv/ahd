package com.athomediva.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIGetPromotionalAdsResponse;
import com.athomediva.data.models.remote.OffersItem;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.OffersViewContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

public class OffersAndCouponPresenter extends BasePresenter<OffersViewContract.View>
  implements OffersViewContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(OffersAndCouponPresenter.class.getSimpleName());
    private QuikrNetworkRequest mGetCouponsRequest;

    @Inject
    public OffersAndCouponPresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        super(dataManager, context);
    }

    @Override
    public void attachView(@NonNull OffersViewContract.View mvpView) {
        super.attachView(mvpView);
        getCoupons();
    }

    @Override
    public void onOffersCopyToClipboardClick(OffersItem offer) {
        LOGD(TAG, "onOffersCopyToClipboardClick: ");
        AndroidHelper.copyTextToClipboard(mContext, offer.getCouponCode());
        ToastSingleton.getInstance().showToast(mContext.getString(R.string.coupan_code_copied_toast));
    }

    @Override
    public void onEmptyPageActionBtnClick() {
        LOGD(TAG, "onEmptyPageActionBtnClick: ");
        getMvpView().finish();
    }

    private void getCoupons() {
        LOGD(TAG, "getCoupons: ");
        if (mGetCouponsRequest != null) {
            mGetCouponsRequest.cancel();
        }
        getMvpView().showProgressBar("");
        mGetCouponsRequest = mDataManager.getApiManager()
          .getCouponDetails(false, new QuikrNetworkRequest.Callback<APIGetPromotionalAdsResponse>() {
              @Override
              public void onSuccess(APIGetPromotionalAdsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.isSuccess()) {
                      if (response.getData() != null && response.getData().getOffers() != null && !response.getData()
                        .getOffers()
                        .isEmpty()) {
                          getMvpView().updateOffersView(response.getData().getOffers());
                      } else {
                          getMvpView().updateEmptyPageVisibility(true);
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetCouponsRequest), true);
              }
          });

        mGetCouponsRequest.execute();
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        if (mGetCouponsRequest != null) {
            mGetCouponsRequest.cancel();
        }
        super.detachView();
    }
}
