package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.APIManager;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.RateUsContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 07/07/17.
 */

public class RateUsPresenter extends BasePresenter<RateUsContract.View> implements RateUsContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(RateUsPresenter.class.getSimpleName());

    private Bundle mBundle;
    private QuikrNetworkRequest mRatingSubmitReq;

    public RateUsPresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        super(dataManager, context);
        LOGD(TAG, "ReviewOrderPresenter : ");
    }

    @Override
    public void attachView(@NonNull RateUsContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView : ");
    }

    @DebugLog
    @Override
    public void onSubmitClick(float rating, String comments) {
        LogUtils.LOGD(TAG, "onSubmitClick : ");
        if (rating <= AppConstants.APP_LOW_RATING_RANGE && TextUtils.isEmpty(comments)) {
            ToastSingleton.getInstance().showToast(mContext.getString(R.string.app_rating_validation_error));
        } else {
            submitApi(rating, comments);
        }
    }

    @DebugLog
    @Override
    public void onRatingChange(float rating) {
        LogUtils.LOGD(TAG, "onRatingChange : ");
        if (rating <= AppConstants.APP_LOW_RATING_RANGE) {
            getMvpView().enableLowRatingOptions(true);
        } else {
            getMvpView().enableLowRatingOptions(false);
        }
    }

    @Override
    public void onPlayStoreClick() {
        LogUtils.LOGD(TAG, "onPlayStoreClick : ");
        launchPlaystore();
    }

    private void launchPlaystore() {
        LogUtils.LOGD(TAG, "launchPlaystore : ");
        AndroidHelper.goToPlayStore(mContext);
        getMvpView().finish();
    }

    private void submitApi(float rating, String comments) {
        LogUtils.LOGD(TAG, "submitApi : ");
        if (!isViewAttached()) return;

        if (mRatingSubmitReq != null) {
            mRatingSubmitReq.cancel();
        }

        getMvpView().showProgressBar("");

        mRatingSubmitReq = APIManager.submitRating(rating, comments, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
            @Override
            public void onSuccess(APIGenericResponse response) {
                LogUtils.LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                if (response != null && response.isSuccess()) {
                    LogUtils.LOGD(TAG, "onSuccess : True");
                    if (response.getData() != null && !TextUtils.isEmpty(response.getData().getMessage())) {
                        ToastSingleton.getInstance().showToast(response.getData().getMessage());
                        getMvpView().finish();
                    }
                } else {
                    LogUtils.LOGD(TAG, "onSuccess : False ");
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LogUtils.LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                onNetworkError(errorCode, errorMessage, new WeakReference<QuikrNetworkRequest>(mRatingSubmitReq));
            }
        });
        mRatingSubmitReq.execute();
    }

    @Override
    public void detachView() {
        LogUtils.LOGD(TAG, "detachView : ");
        if (mRatingSubmitReq != null) {
            mRatingSubmitReq.cancel();
        }
        super.detachView();
    }
}
