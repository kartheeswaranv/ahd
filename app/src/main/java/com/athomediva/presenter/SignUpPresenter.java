package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Entity;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APISignUpResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.SignUpPageContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.home.SpecialDatesFragment;
import com.athomediva.ui.webview.WebViewFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.util.ArrayList;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 10/04/17.
 * Get the consumer and pre-fill items
 * update the view with the data
 * Sign up click do validation
 * show error if any
 * else hit the api to register the user.
 * if api success return to the called that user is registered so that they can proceed with otp verification
 * if failure
 * because of ref code show appropriate message
 * else show other errors
 * on click of tos and privacy policy launch the corresponding pages
 */

public class SignUpPresenter extends BasePresenter<SignUpPageContract.View>
  implements SignUpPageContract.Presenter, SpecialDatesFragment.SpecialDatesChangeListener {
    private final String TAG = LogUtils.makeLogTag(SignUpPresenter.class.getSimpleName());
    private final Consumer mConsumer;
    private final SignUpItem mFormItem;
    private QuikrNetworkRequest<APISignUpResponse> mSignUpRequest;
    private ArrayList<SpecialOfferDateModel> mSpecialOfferDateList;

    @Inject
    public SignUpPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context);
        mConsumer = bundle.getParcelable(SignUpPageContract.PARAM_CONSUMER);
        mFormItem = new SignUpItem();
        if (mConsumer != null) {
            mFormItem.setMobileNumberEntity(new Entity<>(mConsumer.getUserNumber()));
            mFormItem.setEmailEntity(new Entity<>(mConsumer.getEmailID()));
            mFormItem.setNameEntity(new Entity<>(mConsumer.getUserName()));
        }
    }

    @Override
    public void attachView(@NonNull SignUpPageContract.View mvpView) {
        LOGD(TAG, "attachView: ");
        super.attachView(mvpView);
        mSpecialOfferDateList = AppConstants.getSpecialOfferDateList(mContext);
        getMvpView().initiateSpecialOfferView(mSpecialOfferDateList, this);
        updateView();
    }

    @Override
    public void signUpClick(SignUpItem signUpItem) {
        LOGD(TAG, "signUpClick: ");
        if (DataValidator.validateCustomerSignUpDetails(mContext, signUpItem)) {
            mConsumer.setMobileNumber(signUpItem.getMobileNumberEntity().getData());
            mConsumer.setEmailID(signUpItem.getEmailEntity().getData());
            mConsumer.setUserName(signUpItem.getNameEntity().getData());
            mConsumer.setGender(signUpItem.getGenderEntity().getData());
            mConsumer.setRefcode(signUpItem.getReferralCodeEntity().getData());
            loginApiRequest(mConsumer);
        } else {
            updateView();
        }
    }

    @Override
    public void termsOfServiceClick() {
        LOGD(TAG, "termsOfServiceClick: ");
        Bundle bundle = new Bundle();
        bundle.putString(WebViewFragment.PARAM_TITLE, "Terms Of BookingService");
        bundle.putString(WebViewFragment.PARAM_URL, AppUrls.URL_VIEW_TERMS_OF_SERVICE);
        bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
        getMvpView().launchActivity(bundle, FragmentContainerActivity.class);
        mDataManager.getAnalyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.TOS,
            GATrackerContext.Label.TOS_PAGELOAD);
    }

    @Override
    public void privacyPolicyClick() {
        LOGD(TAG, "privacyPolicyClick: ");
        Bundle bundle = new Bundle();
        bundle.putString(WebViewFragment.PARAM_TITLE, "Privacy Policy");
        bundle.putString(WebViewFragment.PARAM_URL, AppUrls.URL_VIEW_PRIVACY_POLICY);
        bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
        getMvpView().launchActivity(bundle, FragmentContainerActivity.class);
    }

    private void updateView() {
        getMvpView().updateView(mFormItem);
    }

    private void loginApiRequest(final Consumer consumer) {
        LogUtils.LOGD(TAG, "loginApiRequest : ");
        if (!isViewAttached()) return;

        getMvpView().showProgressBar("Registering...");

        mSignUpRequest = mDataManager.getApiManager()
          .signUp(consumer, mDataManager.getDataProcessor().getUpdatedSpecialDates(mSpecialOfferDateList),
            new QuikrNetworkRequest.Callback<APISignUpResponse>() {
                @Override
                public void onSuccess(APISignUpResponse response) {
                    LOGD(TAG, "onSuccess");
                    if (!isViewAttached()) return;

                    getMvpView().hideProgressBar();
                    if (response != null) {
                        handleSignUpResponse(response, consumer);
                    } else {
                        LOGE(TAG, "response is null");
                        ToastSingleton.getInstance().showToast(R.string.try_again);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {

                    if (!isViewAttached()) return;
                    onNetworkError(errorCode, errorMessage, new WeakReference<>(mSignUpRequest), true);
                }
            });
        mSignUpRequest.execute();
    }

    private void handleSignUpResponse(APISignUpResponse response, Consumer consumer) {
        LOGD(TAG, "handleSignInResponse: ");
        if (!isViewAttached()) return;

        if (response == null) {
            LOGE(TAG, "handleSignUpResponse: null****");
            return;
        }
        if (response.getSuccess()) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(SignUpPageContract.PARAM_CONSUMER, consumer);
            getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
        } else {
            if (response.getError() != null
              && response.getError().getCode() == AppConstants.APICodes.ERROR_CODE.INVALID_REFERRAL_CODE.getErrorCode()) {
                mFormItem.getReferralCodeEntity().setValid(false);
                mFormItem.getReferralCodeEntity()
                  .setErrorMessage(response.getError().getMessage() == null ? mContext.getString(R.string.validation_ref)
                    : response.getError().getMessage());
                getMvpView().updateView(mFormItem);
            } else {
                mDataManager.getAnalyticsManager().trackGAEventForAPIFail(AppUrls.URL_USER_SIGNUP, response.getError());
                getMvpView().finish();
            }
        }
    }

    @Override
    public void detachView() {
        if (mSignUpRequest != null) {
            mSignUpRequest.cancel();
        }
        super.detachView();
    }

    @Override
    public void onSpecialDateChange(SpecialOfferDateModel changeItem, ArrayList<SpecialOfferDateModel> list) {
        LogUtils.LOGD(TAG, "onSpecialDateChange : ");
        mSpecialOfferDateList = list;
    }
}
