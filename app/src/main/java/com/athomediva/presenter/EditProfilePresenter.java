package com.athomediva.presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Entity;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.models.remote.APIImageUploadResponse;
import com.athomediva.data.models.remote.ImageUploadReponse;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.network.MultiPartFileUploadRequest;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.EditProfileContract;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.athomediva.ui.home.SpecialDatesFragment;
import com.athomediva.ui.verification.OTPVerificationActivity;
import com.athomediva.utils.FileUtils;
import com.google.gson.Gson;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public class EditProfilePresenter extends BasePresenter<EditProfileContract.View>
  implements EditProfileContract.Presenter, MultiPartFileUploadRequest.MultipartResponseCallback,
  SpecialDatesFragment.SpecialDatesChangeListener {
    private static final String TAG = LogUtils.makeLogTag(EditProfilePresenter.class.getSimpleName());

    private static final int CAMERA_PHOTO_REQUEST = 2201;
    private static final int GALLERY_PHOTO_REQUEST = 2202;
    private static final int OTP_VERIFICATION_REQUEST = 2203;

    private static final int OPTION_CAMERA = 0;
    private static final int OPTION_GALLERY = 1;
    private static final int OPTION_REMOVE = 2;

    private Consumer mConsumer;
    private Consumer mEditedConsumer;
    private SignUpItem mFormItem;
    private File mCameraPhotoFile;
    private QuikrNetworkRequest mUpdateRequest, mUpdatePicRequest;
    private MultiPartFileUploadRequest mFileUploadRequest;
    private PermissionManager mPermissionManager;
    private UserSession mUserSession;
    private boolean mIsImageUpdated;
    private ArrayList<SpecialOfferDateModel> mSpecialDateList;

    public EditProfilePresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull UserSession userSession,
      @NonNull PermissionManager permissionManager, @NonNull Bundle bundle) {
        super(dataManager, context);
        LOGD(TAG, "EditProfilePresenter : ");
        mUserSession = userSession;
        mPermissionManager = permissionManager;
        mConsumer = bundle.getParcelable(EditProfileContract.PARAM_CONSUMER);
        mFormItem = new SignUpItem();
        if (mConsumer != null) {
            mFormItem.setMobileNumberEntity(new Entity<>(mConsumer.getUserNumber()));
            mFormItem.setEmailEntity(new Entity<>(mConsumer.getEmailID()));
            mFormItem.setNameEntity(new Entity<>(mConsumer.getUserName()));
            mFormItem.setGenderEntity(new Entity<>(mConsumer.getGender()));
        }
    }

    @Override
    public void attachView(@NonNull EditProfileContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        if (!isViewAttached()) return;
        mSpecialDateList = mDataManager.getDataProcessor().createSpecialDatesFromUserDetails(mUserSession.getUser());
        getMvpView().initSpecialOfferView(!DataValidator.isSpecialDatesUpdated(mUserSession.getUser()), mSpecialDateList,
          this);
        getMvpView().updateView(mFormItem);
        getMvpView().updateImageView(mConsumer.getImage());
    }

    @Override
    public void updateClick(SignUpItem signUpItem) {
        LOGD(TAG, "updateClick : ");
        if (DataValidator.validateCustomerSignUpDetails(mContext, signUpItem)) {
            mEditedConsumer = new Consumer();
            mEditedConsumer.setMobileNumber(signUpItem.getMobileNumberEntity().getData());
            mEditedConsumer.setEmailID(signUpItem.getEmailEntity().getData());
            mEditedConsumer.setUserName(signUpItem.getNameEntity().getData());
            mEditedConsumer.setGender(signUpItem.getGenderEntity().getData());

            // Check whether special date is updated or consumer details updated
            // If Any one of them updated we need to call api to update
            // otherwise close the page
            if (DataValidator.isConsumerUpdated(mConsumer, mEditedConsumer) || DataValidator.hasChangesInSpecialDates(
              mSpecialDateList)) {
                updateProfileDatas();
            } else {
                setDataAndFinish(mIsImageUpdated);
                LogUtils.LOGD(TAG, "updateClick : No updation is there");
            }
        } else {
            getMvpView().updateView(signUpItem);
        }
    }

    @Override
    public void onEditImgClick(int option) {
        LOGD(TAG, "onEditImgClick : ");
        if (option == OPTION_CAMERA) {
            ArrayList<Permission> list = new ArrayList<>();
            list.add(new Permission(Manifest.permission.CAMERA));
            // Camera photos are stored  in EXTERNAL STORAGE
            list.add(new Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
            mPermissionManager.checkAndRequestForPermissions(list,
              (isAllAreGranted, grantedPermission, deniedPermission) -> {
                  if (isAllAreGranted) {
                      LogUtils.LOGD(TAG, "onEditImgClick : Permission Granted");
                      launchCameraIntent();
                  } else {
                      LogUtils.LOGD(TAG, "onEditImgClick : Permission Denied");
                  }
              });
        } else if (option == OPTION_GALLERY) {
            Intent intent = AndroidHelper.getImagePickerIntent(mContext);
            getMvpView().launchActivityForResult(intent, GALLERY_PHOTO_REQUEST);
        } else if (option == OPTION_REMOVE) {
            // Not Handling the Remove Option
            // TODO Confirm Whether api accept empty value for removing
            //updatePicture("");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (resultCode == RESULT_OK) {
            LogUtils.LOGD(TAG, "onActivityResult : RESULT OK ");
            getMvpView().showProgressBar("");
            if (requestCode == CAMERA_PHOTO_REQUEST) {
                if (mCameraPhotoFile != null) {
                    LOGD(TAG, "onActivityResult : " + mCameraPhotoFile);
                    uploadRequest(mCameraPhotoFile.getAbsolutePath());
                }
            } else if (requestCode == GALLERY_PHOTO_REQUEST) {
                if (data != null && data.getData() != null) {
                    uploadRequest(FileUtils.getFilePathFromUri(mContext, data.getData()));

                    LOGD(TAG, "onActivityResult : " + data.getData());
                }
            } else if (requestCode == OTP_VERIFICATION_REQUEST) {
                LogUtils.LOGD(TAG, "onActivityResult :  OTP Verification ");
                if (data != null && data.getExtras() != null) {
                    LogUtils.LOGD(TAG, "onActivityResult :  OTP Verification data not null");
                    LoggedInUserDetails userDetails =
                      data.getExtras().getParcelable(OTPVerificationContract.PARAM_RESULT_USER);
                    String token = data.getExtras().getString(OTPVerificationContract.PARAM_RESULT_TOKEN);
                    if (userDetails != null && !TextUtils.isEmpty(token)) {
                        LOGD(TAG, " user verified.");
                        mUserSession.saveLoggedInUserDetails(userDetails);
                        mUserSession.setUserToken(token);
                        setDataAndFinish(true);
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @DebugLog
    private void uploadRequest(String path) {
        LogUtils.LOGD(TAG, "uploadRequest : ");
        Map<String, String> map = new HashMap<>();
        map.put(AppConstants.IMAGE_UPLOAD.PARAM_TITLE, String.valueOf(System.currentTimeMillis()));
        map.put(AppConstants.IMAGE_UPLOAD.PARAM_CATEGORY, AppConstants.IMAGE_UPLOAD_VALUES.VALUE_CATEGORY);
        map.put(AppConstants.IMAGE_UPLOAD.PARAM_CLIENT_ID, AppConstants.IMAGE_UPLOAD_VALUES.VALUE_CLIENT_ID);
        if (mFileUploadRequest != null) {
            mFileUploadRequest.cancel();
        }

        mFileUploadRequest = new MultiPartFileUploadRequest(mContext, AppUrls.URL_UPLOAD_IMAGE, null, path, map,
          AppConstants.IMAGE_UPLOAD.PARAM_IMAGE, "image*//*");
        mFileUploadRequest.setResponseCallBack(this);
        mFileUploadRequest.execute();
    }

    @Override
    public void onResponse(Object obj, String response, int requestCode) {
        LOGD(TAG, "onResponse : " + response);
        if (isViewAttached()) {
            getMvpView().hideProgressBar();
            ImageUploadReponse imgResponse = new Gson().fromJson(response, ImageUploadReponse.class);
            if (imgResponse != null && "success".equals(imgResponse.getStatus())) {

                LogUtils.LOGD(TAG, "onResponse : " + imgResponse.getFilename());
                getMvpView().updateImageView(AppUrls.URL_UPLOAD_IMAGE + imgResponse.getFilename());
                updatePicture(imgResponse.getFilename());
            } else {
                LogUtils.LOGD(TAG, "onResponse : Image Upload Fails ");
            }
        }
    }

    private void updateProfileDatas() {
        LogUtils.LOGD(TAG, "updateProfileDatas : ");

        if (mUpdateRequest != null) {
            mUpdateRequest.cancel();
        }

        getMvpView().showProgressBar("");
        mUpdateRequest = mDataManager.getApiManager()
          .updateProfile(mEditedConsumer, mDataManager.getDataProcessor().getUpdatedSpecialDates(mSpecialDateList),
            new QuikrNetworkRequest.Callback<APIGenericResponse>() {
                @Override
                public void onSuccess(APIGenericResponse response) {
                    LogUtils.LOGD(TAG, "onSuccess : ");
                    if (!isViewAttached()) return;

                    getMvpView().hideProgressBar();
                    if (response != null && response.isSuccess() && response.getData() != null) {
                        if (response.getData().getSStatus()
                          == AppConstants.APICodes.ERROR_CODE.UPDATE_MOBILE_WITH_OTP.getErrorCode()) {
                            launchOTPVerification(mEditedConsumer);
                            LogUtils.LOGD(TAG, "onSuccess : Launch OTP");
                        } else {
                            LogUtils.LOGD(TAG, "onSuccess : Set Result  true");
                            setDataAndFinish(true);
                        }
                        //Display Success message
                        if (!TextUtils.isEmpty(response.getData().getMessage())) {
                            ToastSingleton.getInstance().showToast(response.getData().getMessage());
                        }
                    } else {
                        LogUtils.LOGD(TAG, "onSuccess : Api Error  ");
                        if (response.getError() != null && !TextUtils.isEmpty(response.getError().getMessage())) {
                            ToastSingleton.getInstance().showToast(response.getError().getMessage());
                        }
                        mDataManager.getAnalyticsManager()
                          .trackGAEventForAPIFail(AppUrls.URL_UPDATE_PROFILE, response.getError());
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    LogUtils.LOGD(TAG, "onError : ");
                    if (!isViewAttached()) return;
                    onNetworkError(errorCode, errorMessage, new WeakReference<>(mUpdateRequest), true);
                }
            });

        mUpdateRequest.execute();
    }

    private void updatePicture(String name) {
        LogUtils.LOGD(TAG, "updatePicture : ");

        if (mUpdatePicRequest != null) {
            mUpdatePicRequest.cancel();
        }
        mIsImageUpdated = false;
        getMvpView().showProgressBar("");
        mUpdatePicRequest = mDataManager.getApiManager()
          .updateProfilePicture(name, new QuikrNetworkRequest.Callback<APIImageUploadResponse>() {
              @Override
              public void onSuccess(APIImageUploadResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;

                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.isSuccess() && response.getData() != null && !TextUtils.isEmpty(
                        response.getData().getImage())) {
                          LogUtils.LOGD(TAG, "onSuccess : Update Profile Pic to Server");
                          mIsImageUpdated = true;
                          getMvpView().updateImageView(response.getData().getImage());
                          //Display Success message
                          if (!TextUtils.isEmpty(response.getData().getMessage())) {
                              ToastSingleton.getInstance().showToast(response.getData().getMessage());
                          }
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_UPDATE_PROFILE_PICTURE, response.getError());
                          if (response.getError() != null && !TextUtils.isEmpty(response.getError().getMessage())) {
                              ToastSingleton.getInstance().showToast(response.getError().getMessage());
                          }
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mUpdatePicRequest));
              }
          });

        mUpdatePicRequest.execute();
    }

    private void launchCameraIntent() {
        LogUtils.LOGD(TAG, "launchCameraIntent : ");
        try {
            mCameraPhotoFile = FileUtils.createFile(mContext, false);
            Uri uri = Uri.fromFile(mCameraPhotoFile);
            Intent intent = AndroidHelper.getCameraIntent(uri);
            getMvpView().launchActivityForResult(intent, CAMERA_PHOTO_REQUEST);
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "launchCameraIntent : Exception ");
        }
    }

    private void launchOTPVerification(Consumer consumer) {
        LogUtils.LOGD(TAG, "launchOTPVerification : ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(OTPVerificationContract.PARAM_CONSUMER, consumer);
        bundle.putBoolean(OTPVerificationContract.PARAM_IS_UPDATE_MOBILE, true);
        getMvpView().launchActivityForResult(bundle, OTPVerificationActivity.class, OTP_VERIFICATION_REQUEST);
    }

    @Override
    public void detachView() {
        LogUtils.LOGD(TAG, "detachView : ");
        if (mFileUploadRequest != null) {
            mFileUploadRequest.cancel();
        }

        if (mUpdateRequest != null) {
            mUpdateRequest.cancel();
        }

        if (mUpdatePicRequest != null) {
            mUpdatePicRequest.cancel();
        }
        super.detachView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.LOGD(TAG, "onRequestPermissionsResult : ");
        mPermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onSaveInstance(Bundle instance) {
        LogUtils.LOGD(TAG, "onSaveInstance : ");
        if (mCameraPhotoFile != null) {
            instance.putString(EditProfileContract.KEY_CAMERA_FILE_PATH, mCameraPhotoFile.getAbsolutePath());
        }
    }

    @DebugLog
    @Override
    public void onActivityRecreate(Bundle b) {
        LogUtils.LOGD(TAG, "onActivityRecreate : ");
        if (b != null) {
            String filePath = b.getString(EditProfileContract.KEY_CAMERA_FILE_PATH);
            if (!TextUtils.isEmpty(filePath)) {
                LogUtils.LOGD(TAG, "onActivityRecreate : upload file ");
                uploadRequest(filePath);
            }
        }
    }

    public void setDataAndFinish(boolean isUpdated) {
        LogUtils.LOGD(TAG, "setDataAndFinish : ");
        if (isViewAttached()) {
            Bundle data = new Bundle();
            data.putBoolean(EditProfileContract.KEY_PROFILE_UPDATE_FLAG, isUpdated);
            getMvpView().finishViewWithResult(RESULT_OK, data);
        }
    }

    @Override
    public void onSpecialDateChange(SpecialOfferDateModel changeItem, ArrayList<SpecialOfferDateModel> list) {
        LogUtils.LOGD(TAG, "onSpecialDateChange : ");
        mSpecialDateList = list;
    }
}
