package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.data.models.remote.ConfirmBookingResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.BookingSuccessContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.bookingpage.BookingDetailsActivity;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UiUtils;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public class BookingSuccessPresenter extends BasePresenter<BookingSuccessContract.View>
  implements BookingSuccessContract.Presenter {

    private static final String TAG = LogUtils.makeLogTag(BookingSuccessPresenter.class.getSimpleName());
    private final int PAYMENT_REQUEST_CODE = 1003;
    private ConfirmBookingResponse mResponse;
    private Bundle mBundle;

    public BookingSuccessPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull BookingSession bookingSession, Bundle bundle) {
        super(dataManager, context, bookingSession);
        LogUtils.LOGD(TAG, "BookingSuccessPresenter : ");
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull BookingSuccessContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        if (mBundle != null && mBundle.containsKey(BookingSuccessContract.PARAM_RESPONSE)) {
            mResponse = mBundle.getParcelable(BookingSuccessContract.PARAM_RESPONSE);
        }

        if (!TextUtils.isEmpty(mDataManager.getPrefManager().getUserDetails().getName())) {
            getMvpView().updateTitle(mDataManager.getPrefManager().getUserDetails().getName());
        }

        if (mResponse != null && mResponse.isSuccess() && mResponse.getData() != null) {
            getMvpView().updateBookingsList(mResponse.getData().getBucketInfo());
            getMvpView().updateFooterInfoList(mResponse.getData().getBookingFooters());
            getMvpView().updateHeaderInfoList(mResponse.getData().getBookingHeaders());
            getMvpView().updateBookingExtraInfoList(mResponse.getData().getBookingExtraInfo());
            if (mResponse.getData().getBucketInfo() != null
              && !mResponse.getData().getBucketInfo().isEmpty()
              && !TextUtils.isEmpty(mResponse.getData().getPaymentUrl())) {
                getMvpView().updateTotalAmountView(mContext.getString(R.string.total_amt_payable,
                  UiUtils.getFormattedAmountByString(mContext, mResponse.getData().getTotal())));
            } else {
                getMvpView().updateTotalAmountView(null);
            }
        }
    }

    @DebugLog
    @Override
    public void onBookingItemClick(ConfirmBookingInfo info) {
        LogUtils.LOGD(TAG, "onBookingItemClick : ");
        Bundle b = new Bundle();
        if (info != null) {
            b.putString(BookingDetailsContract.PARAM_BOOKING_ID, info.getBookingId());
            getMvpView().launchActivity(b, BookingDetailsActivity.class);
        } else {
            LogUtils.LOGD(TAG, "onBookingItemClick : Booking List is empty");
        }
    }

    @Override
    public void onHomeClick() {
        LogUtils.LOGD(TAG, "onHomeClick : ");
        launchHomePage();
    }

    private void launchHomePage() {
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.putExtra(AHDMainViewContract.PARAM_NAVIGATION_TAG, mContext.getString(R.string.home));
        intent.putExtra(AHDMainViewContract.PARAM_RELOAD_HOME_PAG, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
    }

    private void launchMyBookings() {
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.putExtra(AHDMainViewContract.PARAM_NAVIGATION_TAG, mContext.getString(R.string.booking));
        intent.putExtra(AHDMainViewContract.PARAM_RELOAD_HOME_PAG, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (requestCode == PAYMENT_REQUEST_CODE) {
            launchMyBookings();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPayNowClick() {
        LogUtils.LOGD(TAG, "onPayNowClick : ");
        if (mResponse != null && mResponse.getData() != null && !TextUtils.isEmpty(mResponse.getData().getPaymentUrl())) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.pay_online_cta));
            bundle.putString(WebViewFragment.PARAM_URL, mResponse.getData().getPaymentUrl());
            bundle.putString(WebViewFragment.PARAM_INTERCEPT_URL, mResponse.getData().getVerifyPaymentUrl());
            bundle.putLong(WebViewFragment.PARAM_REDIRECTION_TIME, AppConstants.PAYMENT_REDIRECT_DURATION);
            bundle.putString(WebViewFragment.PARAM_REDIRECTION_MSG,
              mContext.getString(R.string.payment_success_redirect_msg));
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }
}
