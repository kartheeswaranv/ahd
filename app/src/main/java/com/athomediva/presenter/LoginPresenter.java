package com.athomediva.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.AHDApplication;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.remote.APISignInResponse;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.UserSession;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.injection.ApplicationContext;
import com.athomediva.mvpcontract.LoginPageContract;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.athomediva.mvpcontract.SignUpPageContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.login.SignUpActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 17/04/17.
 * Functionality:
 * 1. Show error when mobile not valid when characters less than 10 or mobile number is not valid
 * 2. Contact us triggers mail to athomediva.com.
 * 3. login pressed shows dialog hits one api and takes to user otp verification screen
 * 4. if user not present launches user registration form
 * 5. Shows user blocked or any other error dialog
 * 6. Skip menu visibility only for direct screen
 */

public class LoginPresenter extends AbstractLoginPresenter<LoginPageContract.View> implements LoginPageContract.Presenter {
    private final String TAG = LoginPresenter.class.getSimpleName();
    private final int REQUEST_CODE_OTP_VERIFICATION = 1001;
    private final int REQUEST_CODE_SIGN_UP = 1002;
    private final UserSession mUserSession;
    private final boolean isFromLauncher;
    private QuikrNetworkRequest mLoginRequest;
    private PermissionManager mPermissionHelper;

    @Inject
    public LoginPresenter(@NonNull DataManager dataManager, @NonNull @ApplicationContext Context context,
      @NonNull PermissionManager permissionHelper, @NonNull Bundle bundle, @NonNull UserSession userSession) {
        super(dataManager, context);
        mPermissionHelper = permissionHelper;
        mUserSession = userSession;
        isFromLauncher = bundle.getBoolean(LoginPageContract.PARAM_FROM_LAUNCHER, false);
    }

    @DebugLog
    @Override
    public void attachView(@NonNull LoginPageContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        getMvpView().updatePromoCarousel(AppConstants.getLoginPromoItem(mContext));
        getMvpView().updateSkipMenuVisibility(isFromLauncher);
        getMvpView().updateNavigationButtonVisibility(!isFromLauncher);
    }

    @DebugLog
    @Override
    public void login(@NonNull String phoneNumber) {
        LOGD(TAG, "login: phone number =" + phoneNumber);
        if (!isViewAttached()) return;

        if (mLoginRequest != null) {
            mLoginRequest.cancel();
        }

        if (!DataValidator.isPhoneNumberValid(phoneNumber)) {
            LOGE(TAG, "login: phone number not valid");
            getMvpView().showValidationError(true, mContext.getResources().getString(R.string.msg_validation_num_not_valid));
            return;
        }

        final Consumer consumer = new Consumer();
        consumer.setMobileNumber(phoneNumber);
        consumer.setDevice(mUserSession.getDevice());

        ArrayList<Permission> list = new ArrayList<>();
        list.add(new Permission(Manifest.permission.READ_PHONE_STATE));
        mPermissionHelper.checkAndRequestForPermissions(list,
          (isAllAreGranted, grantedPermission, deniedPermission) -> addDetailsToConsumerInfo(consumer));
    }

    @Override
    public void contactUs() {
        LOGD(TAG, "contactUs: ");
        String[] to = { AppConstants.AHD_EMAIL_ID };
        AndroidHelper.sendEmail(mContext, mContext.getString(R.string.change_mobile_number), to);
    }

    @Override
    public void skip() {
        LOGD(TAG, "skip: ");
        mDataManager.getPrefManager().setValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_LOGIN, true);
        launchHomePage();
    }

    private void launchHomePage() {
        getMvpView().launchActivity(null, AHDMainActivity.class);
        getMvpView().finish();
    }

    @Override
    public void onNavigationClose() {
        LOGD(TAG, "onNavigationClose: ");
        if (isFromLauncher) {
            launchHomePage();
        } else {
            getMvpView().finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult:");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void loginRequest(Consumer consumer) {
        LOGD(TAG, "loginRequest: ");
        ArrayList<Permission> list = new ArrayList<>();
        list.add(new Permission(Manifest.permission.READ_SMS));
        list.add(new Permission(Manifest.permission.RECEIVE_SMS));
        mPermissionHelper.checkAndRequestForPermissions(list,
          (isAllAreGranted, grantedPermission, deniedPermission) -> loginApiRequest(consumer));
    }

    @DebugLog
    private void loginApiRequest(final Consumer consumer) {
        LOGD(TAG, "loginApiRequest : ");
        if (!isViewAttached()) return;
        getMvpView().showProgressBar("");

        mLoginRequest = mDataManager.getApiManager().signIn(consumer, new QuikrNetworkRequest.Callback<APISignInResponse>() {
            @Override
            public void onSuccess(APISignInResponse response) {
                LOGD(TAG, "onSuccess: ");
                if (!isViewAttached()) return;

                getMvpView().hideProgressBar();
                handleSignInResponse(response, consumer);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError: errorCode =" + errorCode + " errorMessage =" + errorMessage);
                onNetworkError(errorCode, errorMessage, new WeakReference<>(mLoginRequest), true);
            }
        });
        mLoginRequest.execute();
    }

    @DebugLog
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: ");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_OTP_VERIFICATION) {
                LOGD(TAG, "onActivityResult: otp result");
                LoggedInUserDetails userDetails = data.getExtras().getParcelable(OTPVerificationContract.PARAM_RESULT_USER);
                String token = data.getExtras().getString(OTPVerificationContract.PARAM_RESULT_TOKEN);
                if (userDetails != null && !TextUtils.isEmpty(token)) {
                    LOGD(TAG, " user verified.");

                    if (AHDApplication.get(mContext).getComponent().getSession().isEncryptionDisabled()) {
                        mDataManager.getPrefManager().setValue(PreferenceManager.SESSION.KEY_ENCRYPTION_ENABLE, false);
                    } else {
                        mDataManager.getPrefManager().setValue(PreferenceManager.SESSION.KEY_ENCRYPTION_ENABLE, true);
                    }
                    mDataManager.getPrefManager().saveInstanceId();
                    mUserSession.saveLoggedInUserDetails(userDetails);
                    mUserSession.setUserToken(token);

                    if (isFromLauncher) {
                        getMvpView().launchActivity(null, AHDMainActivity.class);
                        getMvpView().finish();
                    } else {
                        getMvpView().finishViewWithResult(Activity.RESULT_OK);
                    }
                } else {
                    if (!isFromLauncher) {
                        getMvpView().finishViewWithResult(Activity.RESULT_CANCELED);
                    }
                }
            } else if (requestCode == REQUEST_CODE_SIGN_UP) {
                LOGD(TAG, "onActivityResult: sign up result");
                Consumer consumer = data.getExtras().getParcelable(SignUpPageContract.PARAM_CONSUMER);
                proceedToOtpVerification(consumer, REQUEST_CODE_OTP_VERIFICATION);
            }
        }
    }

    @DebugLog
    private void handleSignInResponse(APISignInResponse response, Consumer consumer) {
        LOGE(TAG, "handleSignInResponse: ");
        if (!isViewAttached()) return;
        if (response == null) {
            LOGE(TAG, "handleSignInResponse: null***");
            return;
        }

        LOGD(TAG, "handleSignInResponse: process the result");
        if (response.getSuccess()) {
            proceedToOtpVerification(consumer, REQUEST_CODE_OTP_VERIFICATION);
        } else {
            mDataManager.getAnalyticsManager().trackGAEventForAPIFail(AppUrls.URL_USER_SIGN_IN, response.getError());
            if (response.getError() != null) {
                if (response.getError().getCode() == AppConstants.APICodes.ERROR_CODE.NUMBER_NOT_REGISTERED.getErrorCode()) {
                    proceedToSignUpPage(consumer);
                } else {
                    getMvpView().showValidationError(true, response.getError().getMessage());
                }
            } else {
                ToastSingleton.getInstance().showToast(mContext.getResources().getString(R.string.try_again));
            }
        }
    }

    private void proceedToSignUpPage(Consumer consumer) {
        LOGD(TAG, "proceedToSignUpPage: ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(SignUpPageContract.PARAM_CONSUMER, consumer);
        getMvpView().launchActivityForResult(bundle, SignUpActivity.class, REQUEST_CODE_SIGN_UP);
    }

    @Override
    public void detachView() {
        LOGD(TAG, "destroy: ");
        if (mLoginRequest != null) {
            mLoginRequest.cancel();
        }
        mLoginRequest = null;
        mPermissionHelper = null;
        super.detachView();
    }
}
