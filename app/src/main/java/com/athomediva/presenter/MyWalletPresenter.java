package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIGetWalletStatsResponse;
import com.athomediva.data.session.UserSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MyWalletContract;
import com.athomediva.utils.UiUtils;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import javax.inject.Inject;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.mvpcontract.MyWalletContract.PARAM_AVAILABLE_BALANCE;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class MyWalletPresenter extends BasePresenter<MyWalletContract.View> implements MyWalletContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(MyWalletPresenter.class.getSimpleName());
    private QuikrNetworkRequest mWalletInfoRequest;

    @Inject
    public MyWalletPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context);
    }

    @Override
    public void attachView(@NonNull MyWalletContract.View mvpView) {
        super.attachView(mvpView);
        getWalletBreakUp();
    }

    private void getWalletBreakUp() {
        LOGD(TAG, "getWalletBreakUp: ");
        if (mWalletInfoRequest != null) {
            mWalletInfoRequest.cancel();
        }

        getMvpView().showProgressBar("");
        mWalletInfoRequest =
          mDataManager.getApiManager().getMyWalletDetails(new QuikrNetworkRequest.Callback<APIGetWalletStatsResponse>() {
              @Override
              public void onSuccess(APIGetWalletStatsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;

                  getMvpView().hideProgressBar();
                  if (response != null && response.getData() != null) {
                      if (response.getData().getWallet() != null) {
                          getMvpView().updateWalletBreakupViews(
                            UiUtils.getFormattedAmount(mContext, response.getData().getTotalWalletAmt()),
                            AppConstants.getWalletBreakUpInfo(mContext, response.getData().getWallet()));
                      } else {
                          getMvpView().updateWalletBreakupViews(
                            UiUtils.getFormattedAmount(mContext, response.getData().getTotalWalletAmt()), null);
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError:");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mWalletInfoRequest), true);
              }
          });

        mWalletInfoRequest.execute();
    }

    @Override
    public void detachView() {
        if (mWalletInfoRequest != null) {
            mWalletInfoRequest.cancel();
        }
        super.detachView();
    }
}
