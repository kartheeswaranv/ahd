package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataObservableHelper;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.helper.DataHelper;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.athomediva.mvpcontract.MapLocationPickerContract;
import com.athomediva.ui.map.LocationPickerActivity;
import com.google.android.gms.maps.model.LatLng;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.util.List;
import javax.inject.Inject;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class MapLocationPickerPresenter extends BaseGPSPresenter<MapLocationPickerContract.View>
  implements MapLocationPickerContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(MapLocationPickerPresenter.class.getSimpleName());
    private final int LOCATION_PICKER_REQ_CODE = 1001;
    private final List<LatLng> mBounds;
    private QuikrNetworkRequest mCheckServiceReq;

    @Inject
    public MapLocationPickerPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull PermissionManager permissionHelper, Bundle bundle) {
        super(dataManager, context, permissionHelper);
        if (bundle == null) {
            mBounds = null;
        } else {
            mBounds = bundle.getParcelableArrayList(MapLocationPickerContract.PARAM_BOUNDS);
        }
    }

    @Override
    public void attachView(@NonNull MapLocationPickerContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
    }

    @Override
    public void onGetCurrentLocationClick() {
        LOGD(TAG, "onGetCurrentLocationClick: ");
        getLocationFromGPS(true);
    }

    @Override
    public void onCameraFocusedForLocation(LatLng latLng) {
        LOGD(TAG, "onCameraFocusedForLocation: ");
        if (latLng != null) {
            getMvpView().updateErrorView(false);
            DataObservableHelper.getAddressFromLocation(mContext, latLng.latitude, latLng.longitude)
              .observeOn(AndroidSchedulers.mainThread())
              .subscribeOn(Schedulers.io())
              .subscribe(address -> {
                  if (!isViewAttached()) return;
                  if (address != null) {
                      Geolocation geolocation = new Geolocation(latLng.latitude, latLng.longitude);
                      String pincode = DataHelper.getPinCodeFromAddress(address);
                      geolocation.setPinCode(pincode);
                      address = DataHelper.clearAddressPinCodePrefix(address, pincode);
                      geolocation.setAddress(address);
                      getMvpView().updateSearchView(geolocation);
                  } else {
                      onLocationFailed();
                  }
              }, error -> onLocationFailed());
        }
    }

    @Override
    public void onSearchClicked() {
        LOGD(TAG, "onSearchClicked: ");
        Bundle bundle = new Bundle();
        bundle.putBoolean(LocationPickerContract.PARAM_SHOW_USER_ADDRESS_LIST, false);
        bundle.putBoolean(LocationPickerContract.PARAM_SHOW_CURRENT_LOCATION, false);
        getMvpView().launchActivityForResult(bundle, LocationPickerActivity.class, LOCATION_PICKER_REQ_CODE);
    }

    @Override
    public void onUseThisLocationClick(Geolocation geolocation) {
        LOGD(TAG, "onUseThisLocationClick: ");
        if (geolocation != null) {
            checkServiceAvailable(geolocation);
            /*boolean isInsideBound = DataValidator.isCoordinatesInsideBound(geolocation.getLocation(), mBounds);
            if (isInsideBound) {
                updateResultWithGeolocation(geolocation);
            } else {
                getMvpView().updateErrorView(true);
                LOGD(TAG, "onUseThisLocationClick: not inside bound");
            }*/
        }
    }

    @Override
    public void onMapReady() {
        LOGD(TAG, "onMapReady: ");
        getLocationFromGPS(true);
        getMvpView().initializeBounds(AppConstants.getIndiaLatLngBounds());
        /*if (mBounds != null) {
            getMvpView().drawPolygonBounds(mBounds);
        }*/
        getMvpView().updateCurrentLocation(true);
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        super.detachView();
    }

    @Override
    protected void onCurrentLocationObtained(Geolocation geolocation) {
        LOGD(TAG, "onCurrentLocationObtained: ");
        updateViewWithGeolocation(geolocation);
    }

    private void updateViewWithGeolocation(Geolocation geolocation) {
        LOGD(TAG, "updateViewWithGeolocation: ");
        if (!isViewAttached()) return;
        getMvpView().updateErrorView(false);
        getMvpView().updateSearchView(geolocation);
        getMvpView().moveCameraToPosition(geolocation);
    }

    @Override
    protected void onLocationFailed() {
        LOGD(TAG, "onLocationFailed:");
    }

    @Override
    protected void onGPSLocationInProgress() {
        LOGD(TAG, "onGPSLocationInProgress:");
        getMvpView().updateCurrentLocation(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOGD(TAG, "onActivityResult: requestCode" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == LOCATION_PICKER_REQ_CODE && data != null) {
            LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE");
            Geolocation geolocation = data.getParcelableExtra(LocationPickerContract.PARAM_SELECTED_GEOLOCATION);
            if (geolocation != null) {
                LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE address =" + geolocation.getAddress());
                updateViewWithGeolocation(geolocation);
            } else {
                LOGE(TAG, "onActivityResult: geolocation is null ");
            }
        }
    }

    private void updateResultWithGeolocation(Geolocation geolocation) {
        LOGD(TAG, "updateResultWithGeolocation: " + geolocation);
        Bundle bundle = new Bundle();
        bundle.putParcelable(MapLocationPickerContract.PARAM_SELECTED_GEOLOCATION, geolocation);
        getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
    }

    private void checkServiceAvailable(Geolocation location) {
        LogUtils.LOGD(TAG, "checkServiceAvailable: " + location);
        if (!isViewAttached() || location == null || location.getLatLng() == null) return;

        if (mCheckServiceReq != null) mCheckServiceReq.cancel();

        getMvpView().showProgressBar("");

        mCheckServiceReq = mDataManager.getApiManager()
          .checkServiceAvailability(String.valueOf(location.getLatLng().latitude),
            String.valueOf(location.getLatLng().longitude), new QuikrNetworkRequest.Callback<APIGenericResponse>() {
                @Override
                public void onSuccess(APIGenericResponse response) {
                    LogUtils.LOGD(TAG, "onSuccess: ");
                    if (!isViewAttached()) return;
                    getMvpView().hideProgressBar();
                    if (response != null && response.isSuccess()) {
                        LogUtils.LOGD(TAG, "onSuccess: " + response.isSuccess());
                        updateResultWithGeolocation(location);
                    } else {
                        LogUtils.LOGD(TAG, "onSuccess: " + response.isSuccess());
                        getMvpView().updateErrorView(true);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    LogUtils.LOGD(TAG, "onError: ");
                    if (!isViewAttached()) return;
                    getMvpView().hideProgressBar();
                }
            });

        mCheckServiceReq.execute();
    }
}
