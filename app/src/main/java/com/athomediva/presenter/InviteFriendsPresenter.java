package com.athomediva.presenter;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.InviteFriendsContract;
import com.athomediva.ui.social.ContactSyncActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 * "ifTitle": "Refer a friend for earning credits. Friends get &#8377; 500" its breaking the ui
 * let the world know use case?
 */

public class InviteFriendsPresenter extends BasePresenter<InviteFriendsContract.View>
  implements InviteFriendsContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(InviteFriendsPresenter.class.getSimpleName());
    private final UserSession mUserSession;
    private PermissionManager mPermissionManager;

    @Inject
    public InviteFriendsPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull UserSession userSession, @NonNull PermissionManager permissionManager) {
        super(dataManager, context);
        mUserSession = userSession;
        mPermissionManager = permissionManager;
    }

    @Override
    public void attachView(@NonNull InviteFriendsContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        updateViews();
    }

    @Override
    public void onMessageSharingClick(SocialSharingItem item) {
        LOGD(TAG, "onMessageSharingClick: ");
        AndroidHelper.launchViewForAction(mContext, item.getLaunchIntent());
    }

    @Override
    public void onSocialSharingClick(SocialSharingItem item) {
        LOGD(TAG, "onSocialSharingClick: ");
        AndroidHelper.launchViewForAction(mContext, item.getLaunchIntent());
    }

    @Override
    public void onCouponCodeClicked() {
        LOGD(TAG, "onCouponCodeClicked: ");
        if (mUserSession.getUser() != null && mUserSession.getUser().getReferral() != null) {
            AndroidHelper.copyTextToClipboard(mContext, mUserSession.getUser().getReferral().getIfReferal());
        }
        ToastSingleton.getInstance().showToast(mContext.getString(R.string.referral_code_copied_toast));
    }

    private void updateViews() {
        LOGD(TAG, "updateViews: ");
        if (mUserSession.getUser() != null && mUserSession.getUser().getReferral() != null) {
            getMvpView().updateCouponCodeView(mUserSession.getUser().getReferral().getIfReferal());
            getMvpView().updateSharingMessageView(mUserSession.getUser().getReferral().getIfTitle());
            List<SocialSharingItem> itemList = AndroidHelper.getMessageApps(mContext, mUserSession.getUser().getReferral());
            getMvpView().updateMessageSharingViews(itemList);
        } else {
            LogUtils.LOGE(TAG, "updateViews: referral is null *******");
        }
        getMvpView().updateSocialSharingViews(AppConstants.getSocialLinks(mContext));
    }

    @Override
    public void onReferalMoreClick() {
        LOGD(TAG, "onReferalMoreClick : ");
        if (mUserSession.getUser() != null && mUserSession.getUser().getReferral() != null) {
            getMvpView().showReferalInfoDialog(mUserSession.getUser().getReferral().getIfTitle(),
              mUserSession.getUser().getReferral().getIfDesc());
        }
    }

    @Override
    public void onSyncContactClick() {
        LogUtils.LOGD(TAG, "onSyncContactClick : ");
        ArrayList<Permission> list = new ArrayList<>();
        list.add(new Permission(Manifest.permission.READ_CONTACTS));
        mPermissionManager.checkAndRequestForPermissions(list, (isAllAreGranted, grantedPermission, deniedPermission) -> {
            if (isAllAreGranted) {
                launchContactList();
            } else {
                LogUtils.LOGD(TAG, "onSyncContactClick : Permission Denied ");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void launchContactList() {
        LogUtils.LOGD(TAG, "launchContactList : ");
        getMvpView().launchActivity(new Bundle(), ContactSyncActivity.class);
    }
}
