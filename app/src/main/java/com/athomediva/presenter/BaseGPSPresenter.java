package com.athomediva.presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import com.athomediva.data.DataObservableHelper;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 27/04/17.
 */

public abstract class BaseGPSPresenter<T extends MVPView> extends BasePresenter<T> {
    private final String TAG = LogUtils.makeLogTag(BaseGPSPresenter.class.getSimpleName());
    private final int GPS_SETTINGS_REQ_CODE = 2001;
    private final PermissionManager mPermissionHelper;

    BaseGPSPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull PermissionManager permissionHelper) {
        super(dataManager, context);
        mPermissionHelper = permissionHelper;
    }

    BaseGPSPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull BookingSession bookingSession,
      @NonNull PermissionManager permissionHelper) {
        super(dataManager, context, bookingSession);
        mPermissionHelper = permissionHelper;
    }

    /**
     * @param launchSettingPageIfNotEnabled true if you want open settings page directly
     */
    void getLocationFromGPS(boolean launchSettingPageIfNotEnabled) {
        LOGD(TAG, "getLocationFromGPS: ");
        if (!AndroidHelper.isLocationEnabledInSettings(mContext)) {
            LOGD(TAG, "getLocationFromGPS: location not enable");
            if (launchSettingPageIfNotEnabled) {
                launchSettingsPageForResult(GPS_SETTINGS_REQ_CODE);
            } else {
                onLocationFailed();
            }
        } else {
            checkForLocationPermissionsAndProceed();
        }
    }

    /**
     * Please make sure before calling this that the setting gps is enabled
     */
    private void checkForLocationPermissionsAndProceed() {
        if (!AndroidHelper.isLocationPermissionGranted(mContext)) {
            LOGD(TAG, "getLocationFromGPS: permissions not granted");
            mPermissionHelper.checkAndRequestForPermissions(AndroidHelper.getLocationPermissionList(mContext, false),
              (isAllAreGranted, grantedPermission, deniedPermission) -> {
                  LOGD(TAG, "getLocationFromGPS: location enable " + isAllAreGranted);
                  if (isAllAreGranted) {
                      getCurrentLocation();
                  } else {
                      onLocationFailed();
                  }
              });
        } else {
            LOGD(TAG, "getLocationFromGPS: locations and permissions are granted ");
            getCurrentLocation();
        }
    }

    /**
     * If you want to disable auto launch override the method
     */
    private void launchSettingsPageForResult(int requestCode) {
        getMvpView().launchActivityForResult(AndroidHelper.getGPSSettingsIntent(mContext), requestCode);
    }

    protected abstract void onCurrentLocationObtained(Geolocation geolocation);

    protected abstract void onLocationFailed();

    protected abstract void onGPSLocationInProgress();

    /**
     * get the current location. Make sure it has location permissions
     * Call only if you want to fetch the location silently without any permission or action
     */
    void getCurrentLocation() {
        LOGD(TAG, "getCurrentLocation: ");
        onGPSLocationInProgress();
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
          != PackageManager.PERMISSION_GRANTED
          && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
          != PackageManager.PERMISSION_GRANTED) {
            LOGD(TAG, "getCurrentLocation: don't have permission");
            onLocationFailed();
        } else {
            DataObservableHelper.getCurrentAddressObservable(mContext)
              .observeOn(AndroidSchedulers.mainThread())
              .subscribeOn(Schedulers.io())
              .subscribe(geolocation -> {
                  if (!isViewAttached()) return;
                  if (geolocation != null) {
                      onCurrentLocationObtained(geolocation);
                  } else {
                      onLocationFailed();
                  }
              }, error -> onLocationFailed());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOGD(TAG, "onActivityResult: requestCode" + requestCode);
        if (requestCode == GPS_SETTINGS_REQ_CODE) {
            if (AndroidHelper.isLocationEnabledInSettings(mContext)) {
                LOGD(TAG, "onActivityResult: settings enabled. Check for permission now and get location permission");
                checkForLocationPermissionsAndProceed();
            } else {
                LOGD(TAG, "onActivityResult: settings still not enabled");
                onLocationFailed();
            }
        }
        mPermissionHelper.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * this is needed to pass the permission request result to permission helper class
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult: ");
        mPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
