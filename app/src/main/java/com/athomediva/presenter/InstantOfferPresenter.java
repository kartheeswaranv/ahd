package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.utils.UiUtils;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 15/06/18.
 */

public class InstantOfferPresenter extends BasePresenter<InstantCouponContract.View>
  implements InstantCouponContract.Presenter {
    public static final String TAG = LogUtils.makeLogTag(InstantOfferPresenter.class.getSimpleName());

    private InstantCoupon mInstantCoupon;

    public InstantOfferPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context);
        LOGD(TAG, "ReviewOrderPresenter : ");
        getExtras(bundle);
    }

    @Override
    public void attachView(@NonNull InstantCouponContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView : ");

        if (mInstantCoupon != null) {
            getMvpView().updateCouponView(mInstantCoupon.getCouponCode());
            String percentage = mInstantCoupon.getPercentage() + "% ";
            String amount = UiUtils.getFormattedAmount(mContext, mInstantCoupon.getAmount());
            getMvpView().updateOfferView(percentage, amount);
            getMvpView().updateValidationMsg(mInstantCoupon.getValidityMsg());
        } else {
            LogUtils.LOGD(TAG, "attachView: Instant Coupon is Empty");
        }
    }

    private void getExtras(Bundle bundle) {
        if (bundle != null) {
            mInstantCoupon = bundle.getParcelable(InstantCouponContract.PARAM_COUPON_OBJ);
        }

        if (mInstantCoupon == null) getMvpView().finish();
    }

    @Override
    public void onCouponCodeClick() {
        LogUtils.LOGD(TAG, "onCouponCodeClick: ");
        if (mInstantCoupon != null && !TextUtils.isEmpty(mInstantCoupon.getCouponCode())) {
            AndroidHelper.copyTextToClipboard(mContext, mInstantCoupon.getCouponCode());
            ToastSingleton.getInstance().showToast(mContext.getString(R.string.coupan_code_copied_toast));
            //As per requirement from product team we stay on this
            //getMvpView().finish();
        }
    }

    @Override
    public void onCancelClick() {
        LogUtils.LOGD(TAG, "onCancelClick: ");
        getMvpView().finish();
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
