package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.busevents.CartUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.PackagesContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.util.List;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.mvpcontract.PackagesContract.PARAM_PACKAGE_LIST;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class PackagePresenter extends BasePresenter<PackagesContract.View> implements PackagesContract.Presenter {

    private static final String TAG = LogUtils.makeLogTag(PackagePresenter.class.getSimpleName());

    private List<Services> mPackageList;
    private Bundle mBundle;
    private CartUpdationTask mCartUpdateTask;

    public PackagePresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull CartUpdationTask task, @NonNull Bundle b) {
        super(dataManager, context, session);
        LogUtils.LOGD(TAG, "PackagePresenter : ");
        mCartUpdateTask = task;
        mBundle = b;
    }

    @Override
    public void attachView(@NonNull PackagesContract.View mvpView) {
        super.attachView(mvpView);
        mDataManager.getBusManager().register(this);
        LogUtils.LOGD(TAG, "attachView : ");
        if (mBundle != null && mBundle.getParcelableArrayList(PARAM_PACKAGE_LIST) != null) {
            mPackageList = mBundle.getParcelableArrayList(PARAM_PACKAGE_LIST);
            getMvpView().updatePackages(mPackageList);
        } else {
            LogUtils.LOGD(TAG, "attachView :  Packages is empty");
        }
        onEventMainThread(new CartUpdateEvent());
    }

    @DebugLog
    @Override
    public void onPlusClick(Services item) {
        LogUtils.LOGD(TAG, "onPlusClick : ");
        createOrUpdateCart(AppConstants.CART_ACTION.ADD_SERVICE, item);
    }

    @DebugLog
    @Override
    public void onMinusClick(Services item) {
        LogUtils.LOGD(TAG, "onMinusClick : ");
        createOrUpdateCart(AppConstants.CART_ACTION.DELETE_SERVICE, item);
    }

    @DebugLog
    @Override
    public void onInfoClick(Services item) {
        LogUtils.LOGD(TAG, "onInfoClick : ");
        if (item != null && item.getInfoList() != null) {
            getMvpView().showInfoDialog(item.getName(), item.getInfoList());
        }
    }

    private void createOrUpdateCart(AppConstants.CART_ACTION action, Services service) {
        LogUtils.LOGD(TAG, "createOrUpdateCart : ");
        if (service == null || !isViewAttached() || mBookingSession.getCart() == null) return;

        if (mCartUpdateTask != null) mCartUpdateTask.cancel();

        BookingRequestData data = mBookingSession.getCart().updateServices(action.getAction(), service.getServiceId());

        getMvpView().showProgressBar("");

        mCartUpdateTask.execute(data, new QuikrNetworkRequest.Callback<APICartResponse>() {
            @Override
            public void onSuccess(APICartResponse response) {
                LogUtils.LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LogUtils.LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                //onNetworkError(errorCode, errorMessage, null);
            }
        }, this);
    }

    public void updateServices() {
        if (mPackageList != null && !mPackageList.isEmpty() || mBookingSession.getCart() != null) {
            CoreLogic.updateServices(mBookingSession.getCart().getServicesMap(), mPackageList);
            getMvpView().updatePackages(mPackageList);
        }
    }

    @Override
    public void detachView() {
        if (mCartUpdateTask != null) {
            mCartUpdateTask.cancel();
        }
        mDataManager.getBusManager().unregister(this);
        super.detachView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(final CartUpdateEvent event) {
        LOGD(TAG, "onEvent CartUpdateEvent Event");
        if (event != null && isViewAttached() && mBookingSession.getCart() != null) {
            updateServices();
        }
    }
}
