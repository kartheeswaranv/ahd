package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.busevents.ForceUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.network.NetworkErrorListener;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPPresenter;
import com.athomediva.mvpcontract.MVPView;
import com.athomediva.ui.NoNetworkActivity;
import com.quikrservices.android.network.Constants;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.greenrobot.eventbus.Subscribe;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * created by mohit kumar
 * Base class that implements the Presenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 */
abstract class BasePresenter<T extends MVPView> implements MVPPresenter<T>, NetworkErrorListener {
    protected final Context mContext;
    protected final DataManager mDataManager;
    private final String TAG = LogUtils.makeLogTag(BasePresenter.class.getSimpleName());
    protected BookingSession mBookingSession;
    private WeakReference<T> mMvpView;
    private final int NO_NETWORK_REQUEST_CODE = 3322;
    private List<WeakReference<QuikrNetworkRequest>> mApiRequests = new ArrayList<>();

    public BasePresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        mDataManager = dataManager;
        mContext = context;
    }

    public BasePresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull BookingSession bookingSession) {
        mDataManager = dataManager;
        mContext = context;
        mBookingSession = bookingSession;
    }

    @CallSuper
    @Override
    public void attachView(@NonNull T mvpView) {
        LOGD(TAG, "attachView: ");
        mMvpView = new WeakReference<>(mvpView);
    }

    @CallSuper
    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        mMvpView = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: ");
        if (resultCode == RESULT_OK && requestCode == NO_NETWORK_REQUEST_CODE) {
            retryNetworkConnection();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult:");
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public T getMvpView() {
        return mMvpView == null ? null : mMvpView.get();
    }

    @Subscribe
    public void onEventMainThread(ForceUpdateEvent event) {
        LOGD(TAG, "onEvent ResetBookingEvent");
    }

    public void onNetworkError(int errorCode, String errorMessage, WeakReference<QuikrNetworkRequest> networkRequest) {
        LOGD(TAG, "onNetworkError: ");
        onNetworkError(errorCode, errorMessage, networkRequest, false);
    }

    public void onNetworkError(int errorCode, String errorMessage, WeakReference<QuikrNetworkRequest> networkRequest,
      boolean launchNoNetwork) {
        LOGD(TAG, "onNetworkError: ");
        if (isViewAttached()) {
            getMvpView().hideProgressBar();
            if (errorCode == Constants.ERROR_CODES.NO_NETWORK) {
                if (launchNoNetwork) {
                    launchNoNetworkActivity(networkRequest);
                } else {
                    ToastSingleton.getInstance().showToast(R.string.NoInternet);
                }
            } else if (!TextUtils.isEmpty(errorMessage)) {
                ToastSingleton.getInstance().showToast(errorMessage);
            } else {
                ToastSingleton.getInstance().showToast(R.string.Connectionissue);
            }
        }
    }

    private void launchNoNetworkActivity(WeakReference<QuikrNetworkRequest> request) {
        LogUtils.LOGD(TAG, "launchNoNetworkActivity : ");
        if (request != null && request.get() != null) {
            mApiRequests.add(request);
            Intent intent = new Intent(mContext, NoNetworkActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getMvpView().launchActivityForResult(intent, NO_NETWORK_REQUEST_CODE);
        }
    }

    /**
     * Handles the retry action if network is connected
     * otherwise this method will not execute
     */
    private void retryNetworkConnection() {
        LogUtils.LOGD(TAG, "retryNetworkConnection : ");
        if (mApiRequests != null && !mApiRequests.isEmpty()) {
            for (Iterator<WeakReference<QuikrNetworkRequest>> iterator = mApiRequests.iterator(); iterator.hasNext(); ) {
                WeakReference<QuikrNetworkRequest> request = iterator.next();
                if (request != null && request.get() != null) {
                    request.get().execute();
                    iterator.remove();
                }
            }
        } else {
            LogUtils.LOGD(TAG, "retryNetworkConnection : No Api Request is pending");
        }
    }
}

