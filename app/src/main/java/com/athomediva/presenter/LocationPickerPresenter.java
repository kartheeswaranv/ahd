package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGoogleAutoSuggestResponse;
import com.athomediva.data.models.remote.APIGooglePlaceByIDResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.Predictions;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.DataHelper;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import javax.inject.Inject;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 10/04/17.
 * // 1. on add address button on click behaviour not clear since adding address depends on circle
 * // 2. primary and secondary address not there in address list
 * 3. Edit address option not there with address item
 * //TODO API OVER_QUERY_LIMIT
 */

public class LocationPickerPresenter extends BaseGPSPresenter<LocationPickerContract.View>
  implements LocationPickerContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(LocationPickerPresenter.class.getSimpleName());
    private final UserSession mUserSession;
    private QuikrNetworkRequest mGoogleSuggestedLocationRequest;
    private QuikrNetworkRequest mGooglePlaceByIdRequest;
    private QuikrNetworkRequest mGetUserDetails;
    private boolean enableCurrentLocation = true;

    @Inject
    public LocationPickerPresenter(@NonNull DataManager dataManager, @NonNull Context context, UserSession userSession,
      @NonNull PermissionManager permissionHelper, Bundle bundle) {
        super(dataManager, context, permissionHelper);
        mUserSession = userSession;
        if (bundle != null) {
            enableCurrentLocation = bundle.getBoolean(LocationPickerContract.PARAM_SHOW_CURRENT_LOCATION, true);
        }
    }

    @Override
    public void attachView(@NonNull LocationPickerContract.View mvpView) {
        super.attachView(mvpView);

        if (DataValidator.isUserSessionExist(mUserSession) && enableCurrentLocation) {
            updateSavedAddressView();
            getCompleteUserDetails();
        }
        getMvpView().updateCurrentLocationVisibility(enableCurrentLocation);
        //if coming only from map open the key board
        if (!enableCurrentLocation) {
            getMvpView().openKeyBoard();
        }
    }

    @Override
    public void onGetCurrentLocationClick() {
        LOGD(TAG, "onGetCurrentLocationClick: ");
        getLocationFromGPS(true);
    }

    @Override
    public void onSearchQuery(String query) {
        LOGD(TAG, "onSearchQuery: query" + query);
        if (mGoogleSuggestedLocationRequest != null) {
            mGoogleSuggestedLocationRequest.cancel();
        }

        if (TextUtils.isEmpty(query)) {
            getMvpView().updateSuggestedAddressVisibility(false);
            return;
        }

        if (query.length() < AppConstants.GOOGLE_AUTOSUGGEST_QUERY_CHAR_THRESHOLD) {
            LOGD(TAG, "onSearchQuery: less that threshold. so not hitting the api");
            getMvpView().updateAutoCompleteTextViewResult(null);
            getMvpView().updateEmptyPageVisibility(null, false);
            return;
        }

        mGoogleSuggestedLocationRequest = mDataManager.getApiManager()
          .getSuggestedPlaces(query, new QuikrNetworkRequest.Callback<APIGoogleAutoSuggestResponse>() {
              @Override
              public void onSuccess(APIGoogleAutoSuggestResponse response) {
                  LOGD(TAG, "getSuggestedPlaces onSuccess:");
                  if (!isViewAttached()) return;
                  if ("OK".equalsIgnoreCase(response.getStatus()) || "ZERO_RESULTS".equalsIgnoreCase(response.getStatus())) {
                      if (!response.getPredictions().isEmpty()) {
                          getMvpView().updateEmptyPageVisibility(null, false);
                          getMvpView().updateAutoCompleteTextViewResult(response.getPredictions());
                      } else {
                          getMvpView().updateEmptyPageVisibility(AppConstants.getEmptySuggestedAddressItem(mContext, query),
                            true);
                      }
                  }

                  if (response.getStatus() != null) {
                      LOGD(TAG, "getSuggestedPlaces onSuccess: status =" + response.getStatus());
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "getSuggestedPlaces onError: ");
              }
          });
        mGoogleSuggestedLocationRequest.execute();
    }

    @Override
    public void onSearchItemSelected(Predictions address) {
        LOGD(TAG, "onSearchItemSelected: ");
        getPlaceByID(address.getPlaceId());
    }

    @Override
    public void onUserAddressClick(Address address) {
        LOGD(TAG, "onUserAddressClick: ");
        Geolocation geolocation = new Geolocation(address.getLat(), address.getLon());
        geolocation.setAddress(address.getAddress());
        geolocation.setUserAddressId(address.getAddressID());
        Bundle bundle = new Bundle();
        bundle.putParcelable(LocationPickerContract.PARAM_SELECTED_GEOLOCATION, geolocation);
        getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
    }

    private void updateSavedAddressView() {
        LOGD(TAG, "updateSavedAddressView: ");

        if (mUserSession.getAddressList() != null && !mUserSession.getAddressList().isEmpty()) {
            LOGD(TAG, "updateSavedAddressView: show saved address list");
            getMvpView().updateSavedAddressVisibility(true);
            getMvpView().updateAddressView(mUserSession.getAddressList());
        } else {
            getMvpView().updateSavedAddressVisibility(false);
            LOGD(TAG, "updateSavedAddressView: hide saved address list");
        }
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        if (mGoogleSuggestedLocationRequest != null) {
            mGoogleSuggestedLocationRequest.cancel();
        }

        if (mGooglePlaceByIdRequest != null) {
            mGooglePlaceByIdRequest.cancel();
        }

        if (mGetUserDetails != null) {
            mGetUserDetails.cancel();
        }

        super.detachView();
    }

    private void getPlaceByID(String placeId) {
        LOGD(TAG, "getPlaceByID: ");
        if (mGooglePlaceByIdRequest != null) {
            mGooglePlaceByIdRequest.cancel();
        }

        if (TextUtils.isEmpty(placeId)) {
            return;
        }

        getMvpView().showProgressBar("");
        mGooglePlaceByIdRequest =
          mDataManager.getApiManager().getPlaceByID(placeId, new QuikrNetworkRequest.Callback<APIGooglePlaceByIDResponse>() {
              @Override
              public void onSuccess(APIGooglePlaceByIDResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.getResult() != null && "OK".equalsIgnoreCase(response.getStatus())) {
                      Geolocation geolocation = new Geolocation(response.getResult().getGeometry().getLocation().getLat(),
                        response.getResult().getGeometry().getLocation().getLng());
                      geolocation.setAddress(response.getResult().getFormatted_address());
                      String pincode =
                        DataHelper.getPinCodeFromAddressComponent(response.getResult().getAddressComponents());
                      geolocation.setPinCode(pincode);
                      Bundle bundle = new Bundle();
                      bundle.putParcelable(LocationPickerContract.PARAM_SELECTED_GEOLOCATION, geolocation);
                      getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
                  } else {
                      LOGE(TAG, "getPlaceByID:  error");
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGooglePlaceByIdRequest));
              }
          });

        mGooglePlaceByIdRequest.execute();
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mGetUserDetails != null) {
            mGetUserDetails.cancel();
        }
        mGetUserDetails =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  mUserSession.updateDataFromResponse(response);
                  updateSavedAddressView();
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
              }
          });

        mGetUserDetails.execute();
    }

    @Override
    protected void onCurrentLocationObtained(Geolocation geolocation) {
        LOGD(TAG, "onCurrentLocationObtained: ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(LocationPickerContract.PARAM_SELECTED_GEOLOCATION, geolocation);
        getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
    }

    @Override
    protected void onLocationFailed() {
        LOGD(TAG, "onLocationFailed:");
    }

    @Override
    protected void onGPSLocationInProgress() {
        LOGD(TAG, "onGPSLocationInProgress:");
    }
}
