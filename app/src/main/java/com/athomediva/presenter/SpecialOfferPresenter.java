package com.athomediva.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.SpecialOfferContract;
import com.athomediva.ui.home.SpecialDatesFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public class SpecialOfferPresenter extends BasePresenter<SpecialOfferContract.View>
  implements SpecialOfferContract.Presenter, SpecialDatesFragment.SpecialDatesChangeListener {
    private static final String TAG = LogUtils.makeLogTag(SpecialOfferPresenter.class.getSimpleName());

    private QuikrNetworkRequest mSubmitRequest;
    private QuikrNetworkRequest mUserDetails;
    private ArrayList<SpecialOfferDateModel> mSpecialDateList;
    private UserSession mUserSession;

    public SpecialOfferPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull UserSession userSession) {
        super(dataManager, context);
        LogUtils.LOGD(TAG, "SpecialOfferPresenter : ");
        mUserSession = userSession;
    }

    @Override
    public void attachView(@NonNull SpecialOfferContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        mSpecialDateList = mDataManager.getDataProcessor().createSpecialDatesFromUserDetails(mUserSession.getUser());
        getMvpView().initSpecialOfferView(mSpecialDateList, this);
    }

    @Override
    public void onSubmitClick() {
        LogUtils.LOGD(TAG, "onSubmitClick : ");
        submitData();
    }

    @Override
    public void onSkipClick() {
        LogUtils.LOGD(TAG, "onSkipClick : ");
        if (isViewAttached()) getMvpView().dismissDialog();
    }

    private void submitData() {
        LogUtils.LOGD(TAG, "submitData : ");
        if (mSubmitRequest != null) mSubmitRequest.cancel();

        ArrayList<SpecialOfferDateModel> updatedList =
          mDataManager.getDataProcessor().getUpdatedSpecialDates(mSpecialDateList);

        if (updatedList == null || updatedList.isEmpty()) {
            LogUtils.LOGD(TAG, "submitData : Updated Date is empty");
            return;
        }

        getMvpView().showProgressBar("");

        mSubmitRequest = mDataManager.getApiManager()
          .updateSpecialDates(updatedList, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
              @Override
              public void onSuccess(APIGenericResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;

                  if (response != null && response.isSuccess()) {
                      //Update the user details otherwise userdetails in preference is not getting updated
                      // this will launch again if it is not updated
                      if (response.getData() != null && !TextUtils.isEmpty(response.getData().getMessage())) {
                          ToastSingleton.getInstance().showToast(response.getData().getMessage());
                      }
                      getCompleteUserDetails();
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  getMvpView().dismissDialog();
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mSubmitRequest));
              }
          });

        mSubmitRequest.execute();
    }

    @Override
    public void onSpecialDateChange(SpecialOfferDateModel changeItem, ArrayList<SpecialOfferDateModel> list) {
        LogUtils.LOGD(TAG, "onSpecialDateChange : ");
        mSpecialDateList = list;
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mUserDetails != null) {
            mUserDetails.cancel();
        }

        mUserDetails =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.getSuccess()) {
                      mUserSession.updateDataFromResponse(response);
                  }
                  getMvpView().dismissDialog();
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mUserDetails));
                  getMvpView().dismissDialog();
              }
          });

        mUserDetails.execute();
    }

    @Override
    public void detachView() {
        if (mSubmitRequest != null) mSubmitRequest.cancel();
        if (mUserDetails != null) mUserDetails.cancel();
        super.detachView();
    }
}
