package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.EditProfileContract;
import com.athomediva.mvpcontract.MyAccountPageContract;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.mvpcontract.MyWalletContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.address.MyAddressFragment;
import com.athomediva.ui.myaccount.EditProfileActivity;
import com.athomediva.ui.mywallet.MyWalletActivity;
import com.athomediva.ui.offers.OffersAndCouponsActivity;
import com.athomediva.ui.ratings.RateUsActivity;
import com.athomediva.ui.social.InviteFriendActivity;
import com.athomediva.ui.support.CustomerCareFragment;
import com.athomediva.ui.webview.WebViewFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 * show Progress bar
 * Hit Get User Details API to fetch the updated user information
 * update the view based on that
 */

public class MyAccountsPresenter extends BasePresenter<MyAccountPageContract.View>
  implements MyAccountPageContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(MyAccountsPresenter.class.getSimpleName());
    private final UserSession mUserSession;
    private QuikrNetworkRequest mUserDetails;
    private QuikrNetworkRequest mLogoutRequest;
    private APIFullUserDetailsResponse mFullUserResponse;

    private static final int EDIT_PROFILE_REQUEST = 1201;

    @Inject
    public MyAccountsPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull UserSession userSession,
      @NonNull BookingSession bookingSession) {
        super(dataManager, context, bookingSession);
        mUserSession = userSession;
    }

    @Override
    public void attachView(@NonNull MyAccountPageContract.View mvpView) {
        super.attachView(mvpView);
        if (mUserSession == null || !DataValidator.isUserSessionExist(mUserSession)) {
            getMvpView().finish();
            return;
        }
        // Load the header Data from User session locally
        // and then update this data from api
        if (mUserSession != null && mUserSession.getUser() != null) {
            getMvpView().updateUserProfileView(mUserSession.getUser());
        }
        getCompleteUserDetails();
    }

    private void updateView(APIFullUserDetailsResponse response) {
        LOGD(TAG, "updateView: ");
        if (!isViewAttached() || response == null) return;
        List<TitleDescImageItem> list = AppConstants.getAccountsItem(mContext, response);
        getMvpView().initializeActionSections(list);
        getMvpView().updateUserProfileView(mUserSession.getUser());
    }

    @Override
    public void onItemClick(String action) {
        LOGD(TAG, "onItemClick: ");
        if (!isViewAttached()) return;
        if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_my_wallet))) {
            Bundle bundle = new Bundle();
            if (mFullUserResponse != null
              && mFullUserResponse.getData() != null
              && mFullUserResponse.getData().getWallet() != null) {
                bundle.putDouble(MyWalletContract.PARAM_AVAILABLE_BALANCE,
                  mFullUserResponse.getData().getWallet().getTotalAmount());
            }
            //bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, MyWalletActivity.TAG);
            getMvpView().launchActivity(bundle, MyWalletActivity.class);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.my_addresses))) {

            if (mFullUserResponse != null && mFullUserResponse.getData() != null) {
                Bundle bundle = new Bundle();
                bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, MyAddressFragment.TAG);
                bundle.putBoolean(MyAddressPageContract.PARAM_IS_FROM_ACCOUNT, true);
                getMvpView().launchActivity(bundle, FragmentContainerActivity.class);
            }
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_offers_coupans))) {
            Bundle bundle = new Bundle();
            //bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, OffersAndCouponsActivity.TAG);
            getMvpView().launchActivity(bundle, OffersAndCouponsActivity.class);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_invite_a_friend))) {
            Bundle bundle = new Bundle();
            //bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, InviteFriendFragment.TAG);
            getMvpView().launchActivity(bundle, InviteFriendActivity.class);
            mDataManager.getAnalyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.REFERAL,
                GATrackerContext.Label.REFERAL_PAGE_LOAD);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_customer_service))) {
            Bundle bundle = new Bundle();
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, CustomerCareFragment.TAG);
            getMvpView().launchActivity(bundle, FragmentContainerActivity.class);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_cancellation_policy))) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.cancellation_policy));
            bundle.putString(WebViewFragment.PARAM_URL, AppUrls.URL_VIEW_TERMS_OF_SERVICE);
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivity(bundle, FragmentContainerActivity.class);
            mDataManager.getAnalyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.CANCELLATION_POLICY,
                GATrackerContext.Label.CANCELLATION_POLICY_PAGELOAD);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_rate_this_app))) {
            mDataManager.getAnalyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, GATrackerContext.Action.RATE_APP,
                GATrackerContext.Label.RATE_APP_EVENT);
            Bundle bundle = new Bundle();
            getMvpView().launchActivity(bundle, RateUsActivity.class);
        } else if (action.equalsIgnoreCase(mContext.getString(R.string.ahd_logout))) {
            logoutUser();
        }
    }

    @Override
    public void onEditClicked() {
        LOGD(TAG, "onEditClicked: ");
        launchProfileEdit();
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mUserDetails != null) {
            mUserDetails.cancel();
        }

        getMvpView().showProgressBar(mContext.getString(R.string.fetching_details));
        mUserDetails =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  mUserSession.updateDataFromResponse(response);
                  mFullUserResponse = response;
                  updateView(response);
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mUserDetails), true);
              }
          });

        mUserDetails.execute();
    }

    private void launchProfileEdit() {
        LogUtils.LOGD(TAG, "launchProfileEdit : ");
        Bundle b = new Bundle();
        Consumer consumer = new Consumer();
        consumer.setUserId(mUserSession.getUserToken());
        consumer.setMobileNumber(mUserSession.getUser().getMobile());
        consumer.setEmailID(mUserSession.getUser().getEmail());
        consumer.setGender(mUserSession.getUser().getGender());
        consumer.setUserName(mUserSession.getUser().getName());
        consumer.setImage(mUserSession.getUser().getImage());
        b.putParcelable(EditProfileContract.PARAM_CONSUMER, consumer);
        getMvpView().launchActivityForResult(b, EditProfileActivity.class, EDIT_PROFILE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_PROFILE_REQUEST && resultCode == RESULT_OK) {
            if (data.getExtras() != null
              && data.hasExtra(EditProfileContract.KEY_PROFILE_UPDATE_FLAG)
              && data.getBooleanExtra(EditProfileContract.KEY_PROFILE_UPDATE_FLAG, false)) {
                LogUtils.LOGD(TAG, "onActivityResult : Profile is updated So refresh the data");
                getCompleteUserDetails();
            } else {
                LogUtils.LOGD(TAG, "onActivityResult : Profile is not updated Keep old data");
            }
        }
    }

    private void logoutUser() {
        LOGD(TAG, "logoutUser: ");

        if (mLogoutRequest != null) mLogoutRequest.cancel();
        getMvpView().showProgressBar("");
        mLogoutRequest = mDataManager.getApiManager().logoutUser(new QuikrNetworkRequest.Callback<APIGenericResponse>() {
            @Override
            public void onSuccess(APIGenericResponse response) {
                LOGD(TAG, "onSuccess: ");

                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                if (response != null) {
                    if (response.isSuccess()) {
                        mUserSession.logout();
                        mBookingSession.resetSession();
                    } else if (response.getError() != null) {
                        ToastSingleton.getInstance().showToast(response.getError().getMessage());
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError: ");
                onNetworkError(errorCode, errorMessage, new WeakReference<>(mLogoutRequest), true);
            }
        });

        mLogoutRequest.execute();
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        if (isViewAttached()) {
            getMvpView().hideProgressBar();
        }
        if (mUserDetails != null) {
            mUserDetails.cancel();
        }

        if (mLogoutRequest != null) {
            mLogoutRequest.cancel();
        }
        super.detachView();
    }
}
