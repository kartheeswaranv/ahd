package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AddAddressPageContract;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.address.AddAddressFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.List;
import javax.inject.Inject;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 10/04/17.
 * // 1. on add address button on click behaviour not clear since adding address depends on circle
 * // 2. primary and secondary address not there in address list
 * 3. Edit address option not there with address item
 */

public class MyAddressPresenter extends BasePresenter<MyAddressPageContract.View>
  implements MyAddressPageContract.Presenter {
    private static final int ADD_ADDRESS_REQUEST_CODE = 201;
    private final String TAG = LogUtils.makeLogTag(MyAddressPresenter.class.getSimpleName());
    private final Bundle mBundle;
    private QuikrNetworkRequest mGetUserDetails;
    private QuikrNetworkRequest mDeleteAddress;
    private UserSession mUserSession;
    private String mCircleName;
    private boolean mIsFromAccountPage;

    @Inject
    public MyAddressPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull Bundle bundle,
      @NonNull UserSession userSession) {
        super(dataManager, context);
        mBundle = bundle;
        mUserSession = userSession;
    }

    @Override
    public void attachView(@NonNull MyAddressPageContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        mCircleName = mBundle.getString(MyAddressPageContract.PARAM_CIRCLE_NAME);
        mIsFromAccountPage = mBundle.getBoolean(MyAddressPageContract.PARAM_IS_FROM_ACCOUNT);
        if (!mUserSession.isNoAddressAddedForUser()) {
            updateAddressView();
        } else {
            getCompleteUserDetails();
        }
    }

    private void updateAddressView() {
        LOGD(TAG, "updateAddressView: ");
        if (TextUtils.isEmpty(mCircleName)) {
            updateAddressList(mUserSession.getAddressList());
        } else {
            updateAddressList(CoreLogic.getFilterAddressByCircle(mUserSession.getAddressList(), mCircleName));
        }
    }

    @Override
    public void onOptionItemClick(Address addressItem) {
        LOGD(TAG, "onOptionItemClick: ");
        //We know that if circle name is present the view has been launched for selection
        //if (!TextUtils.isEmpty(mCircleName)) {
        if (addressItem != null) {
            Bundle b = new Bundle();
            b.putParcelable(MyAddressPageContract.PARAM_SELECTED_ADDRESS, addressItem);
            getMvpView().finishViewWithResult(RESULT_OK, b);
        }
        //}
    }

    @Override
    public void onAddAddressClick() {
        LOGD(TAG, "onUserAddressClick: ");
        Bundle bundle = new Bundle();
        bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, AddAddressFragment.TAG);
        bundle.putString(AddAddressPageContract.PARAM_CIRCLE_NAME, mCircleName);
        getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, ADD_ADDRESS_REQUEST_CODE);
    }

    @Override
    public void onDeleteAddressClick(Address address) {
        LOGD(TAG, "onDeleteAddressClick: ");
        deleteAddress(address);
    }

    private void updateAddressList(List<Address> addressList) {
        LOGD(TAG, "updateAddressList: ");
        if (addressList != null && !addressList.isEmpty()) {
            getMvpView().updateAddressView(addressList, mIsFromAccountPage);
            getMvpView().updateEmptyPageVisibility(false);
        } else {
            getMvpView().updateEmptyPageVisibility(true);
        }
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mGetUserDetails != null) {
            mGetUserDetails.cancel();
        }

        getMvpView().showProgressBar(mContext.getString(R.string.fetching_details));
        mGetUserDetails =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  mUserSession.updateDataFromResponse(response);
                  updateAddressView();
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetUserDetails));
              }
          });

        mGetUserDetails.execute();
    }

    private void deleteAddress(Address address) {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mDeleteAddress != null) {
            mDeleteAddress.cancel();
        }

        getMvpView().showProgressBar(mContext.getString(R.string.deleting_Address));
        mDeleteAddress =
          mDataManager.getApiManager().deleteAddress(address, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
              @Override
              public void onSuccess(APIGenericResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.isSuccess()) {
                          getCompleteUserDetails();
                      } else if (response.getError() != null) {
                          ToastSingleton.getInstance().showToast(response.getError().getMessage());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mDeleteAddress));
              }
          });

        mDeleteAddress.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_ADDRESS_REQUEST_CODE && resultCode == RESULT_OK) {
            getCompleteUserDetails();
        }
    }

    @Override
    public void detachView() {
        if (mGetUserDetails != null) mGetUserDetails.cancel();
        if (mDeleteAddress != null) mDeleteAddress.cancel();
        super.detachView();
    }
}
