package com.athomediva.presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.remote.CreateOrderError;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.PaymentPendingContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UiUtils;
import java.util.ArrayList;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public class PaymentPendingPresenter extends BasePresenter<PaymentPendingContract.View>
  implements PaymentPendingContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(PaymentPendingPresenter.class.getSimpleName());
    private static final int PAYMENT_REQUEST_CODE = 1220;
    private Bundle mBundle;
    private PermissionManager mPermissionMgr;
    private CreateOrderError mResponse;

    public PaymentPendingPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull PermissionManager permissionManager, @NonNull Bundle bundle, @NonNull BookingSession bookingSession) {
        super(dataManager, context, bookingSession);
        mBundle = bundle;
        mPermissionMgr = permissionManager;
        LogUtils.LOGD(TAG, "PaymentPendingPresenter : ");
    }

    @Override
    public void attachView(@NonNull PaymentPendingContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");

        if (mBundle != null && mBundle.containsKey(PaymentPendingContract.PARAM_RESPONSE)) {
            mResponse = mBundle.getParcelable(PaymentPendingContract.PARAM_RESPONSE);
        }

        if (!TextUtils.isEmpty(mDataManager.getPrefManager().getUserDetails().getName())) {
            getMvpView().updateTitle(mDataManager.getPrefManager().getUserDetails().getName());
        }

        if (mResponse != null && mResponse.getData() != null) {
            getMvpView().updateFooterView(mResponse.getData().getFooterInfo());
            getMvpView().updateHeaderView(mResponse.getData().getHeaderInfo());
            getMvpView().updateBookingListView(mResponse.getData().getList());
            String amt = UiUtils.getFormattedAmount(mContext, mResponse.getData().getPendingAmt());
            String paymentCta = mContext.getString(R.string.proceed_to_pay, amt);
            getMvpView().updateCTA(paymentCta);
        }
    }

    @Override
    public void detachView() {
        super.detachView();
        LogUtils.LOGD(TAG, "detachView : ");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionMgr.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LogUtils.LOGD(TAG, "onRequestPermissionsResult : ");
    }

    @Override
    public void onCallClick() {
        LogUtils.LOGD(TAG, "onCallClick : ");
        if (mBookingSession != null
          && mBookingSession.getMetaData() != null
          && mBookingSession.getMetaData().getData() != null
          && !TextUtils.isEmpty(mBookingSession.getMetaData().getData().getCustomerCareNo())) {
            callCustomerCare(mBookingSession.getMetaData().getData().getCustomerCareNo());
        } else {
            LogUtils.LOGD(TAG, "onCallClick : MetaData has no Customer Care number ");
        }
    }

    @Override
    public void onProceedClick() {
        LogUtils.LOGD(TAG, "onProceedClick : ");
        if (mResponse != null && mResponse.getData() != null && !TextUtils.isEmpty(mResponse.getData().getPaymentUrl())) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.pay_online_cta));
            bundle.putString(WebViewFragment.PARAM_URL, mResponse.getData().getPaymentUrl());
            bundle.putString(WebViewFragment.PARAM_INTERCEPT_URL, mResponse.getData().getVerifyPaymentUrl());
            bundle.putLong(WebViewFragment.PARAM_REDIRECTION_TIME, AppConstants.PAYMENT_REDIRECT_DURATION);
            bundle.putString(WebViewFragment.PARAM_REDIRECTION_MSG,
              mContext.getString(R.string.payment_success_redirect_msg));
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }

    private void callCustomerCare(String mobileno) {
        if (TextUtils.isEmpty(mobileno)) {
            LogUtils.LOGD(TAG, "callCustomerCare : Mobileno is empty");
            return;
        }
        ArrayList<Permission> list = new ArrayList<>();
        list.add(new Permission(Manifest.permission.CALL_PHONE));
        mPermissionMgr.checkAndRequestForPermissions(list, (isAllAreGranted, grantedPermission, deniedPermission) -> {
            if (isAllAreGranted) {
                LogUtils.LOGD(TAG, "onCustomerCareClick : Permission Granted");
                Intent intent = AndroidHelper.getCallIntent(mobileno);
                getMvpView().launchActivity(intent);
            } else {
                LogUtils.LOGD(TAG, "onCustomerCareClick : Permission Denied");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (resultCode == RESULT_OK && requestCode == PAYMENT_REQUEST_CODE) {
            LogUtils.LOGD(TAG, "onActivityResult : Payment Success ");
            getMvpView().finishViewWithResult(RESULT_OK);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
