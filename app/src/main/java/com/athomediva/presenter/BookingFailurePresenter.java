package com.athomediva.presenter;

/**
 * Created by kartheeswaran on 30/06/17.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.data.models.remote.ConfirmBookingError;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.BookingFailureContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.bookingpage.BookingDetailsActivity;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UiUtils;

import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import www.zapluk.com.R;

public class BookingFailurePresenter extends BasePresenter<BookingFailureContract.View>
        implements BookingFailureContract.Presenter {

    private static final String TAG = LogUtils.makeLogTag(BookingSuccessPresenter.class.getSimpleName());
    private static final int PAYMENT_REQUEST_CODE = 1003;
    private ConfirmBookingError mResponse;
    private QuikrNetworkRequest mGetCartDetails;
    private Bundle mBundle;

    public BookingFailurePresenter(@NonNull DataManager dataManager, @NonNull Context context,
                                   @NonNull BookingSession bookingSession, Bundle bundle) {
        super(dataManager, context, bookingSession);
        LogUtils.LOGD(TAG, "BookingSuccessPresenter : ");
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull BookingFailureContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        if (mBundle != null && mBundle.containsKey(BookingFailureContract.PARAM_RESPONSE)) {
            mResponse = mBundle.getParcelable(BookingFailureContract.PARAM_RESPONSE);
        }

        if (!TextUtils.isEmpty(mDataManager.getPrefManager().getUserDetails().getName())) {
            getMvpView().updateTitle(mDataManager.getPrefManager().getUserDetails().getName());
        }

        if (mResponse != null && mResponse.getData() != null) {
            getMvpView().updateBookingHeaderInfoList(mResponse.getData().getBookingHeaders());
            getMvpView().updateBookingsList(mResponse.getData().getBucketInfo());
            getMvpView().updateBookingFailureInfo(mResponse.getData().getBookingFooters());
            getMvpView().updateBookingExtraInfoList(mResponse.getData().getBookingExtraInfo());

            if (mResponse.getData().getBucketInfo() == null || mResponse.getData().getBucketInfo().isEmpty()) {
                // If no success booking then we need to change the icon and title text
                getMvpView().updateDesc(mResponse.getMessage(), true);
            } else {
                getMvpView().updateDesc(null, false);
            }

            if (mResponse.getData().getBucketInfo() != null && !mResponse.getData().getBucketInfo().isEmpty()
                    && !TextUtils.isEmpty(mResponse.getData().getPaymentUrl())) {
                getMvpView().updateTotalAmountView(mContext.getString(R.string.total_amt_payable,
                        UiUtils.getFormattedAmountByString(mContext, mResponse.getData().getTotal())));
            } else {
                getMvpView().updateTotalAmountView(null);
            }
        }
        // To Reset the cart Details and Cart Count We need to refresh the Cart Details
        getCartDetails();
    }

    @DebugLog
    @Override
    public void onBookingItemClick(ConfirmBookingInfo info) {
        LogUtils.LOGD(TAG, "onBookingItemClick : ");
        Bundle b = new Bundle();
        if (info != null) {
            b.putString(BookingDetailsContract.PARAM_BOOKING_ID, info.getBookingId());
            getMvpView().launchActivity(b, BookingDetailsActivity.class);
        } else {
            LogUtils.LOGD(TAG, "onBookingItemClick : Booking List is empty");
        }
    }

    @Override
    public void onHomeClick() {
        LogUtils.LOGD(TAG, "onHomeClick : ");
        launchHomePage();
    }

    @Override
    public void onRescheduleClick(BookingDescInfo info) {
        LogUtils.LOGD(TAG, "onRescheduleClick : ");
        launchReschedulePage();
    }

    private void launchHomePage() {
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.putExtra(AHDMainViewContract.PARAM_NAVIGATION_TAG, mContext.getString(R.string.booking));
        getMvpView().launchActivity(intent);
        getMvpView().finish();
    }

    private void launchReschedulePage() {
        LogUtils.LOGD(TAG, "launchReschedulePage : ");
        mBookingSession.resetCategorySelection();

        if (mBookingSession.getCart() != null) {
            Intent intent = new Intent(mContext, MyCartActivity.class);
            intent.putExtra(MyCartContract.PARAM_CART_ID, mBookingSession.getCart().getCartID());
            getMvpView().launchActivity(intent);
            getMvpView().finish();
        } else {
            LogUtils.LOGD(TAG, "launchReschedulePage : Cart is Empty");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.LOGD(TAG, "onActivityResult : ");
        if (requestCode == PAYMENT_REQUEST_CODE) {
            launchHomePage();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPayNowClick() {
        LogUtils.LOGD(TAG, "onPayNowClick : ");
        if (mResponse != null && mResponse.getData() != null && !TextUtils.isEmpty(mResponse.getData().getPaymentUrl())) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.pay_online_cta));
            bundle.putString(WebViewFragment.PARAM_URL, mResponse.getData().getPaymentUrl());
            bundle.putString(WebViewFragment.PARAM_INTERCEPT_URL, mResponse.getData().getVerifyPaymentUrl());
            bundle.putLong(WebViewFragment.PARAM_REDIRECTION_TIME, AppConstants.PAYMENT_REDIRECT_DURATION);
            bundle.putString(WebViewFragment.PARAM_REDIRECTION_MSG,
                    mContext.getString(R.string.payment_success_redirect_msg));
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }

    // Fetch the Cart Details and Reset the Category Section  to remove the success
    // booking details from cart 
    private void getCartDetails() {
        LogUtils.LOGD(TAG, "getCartDetails : ");
        mBookingSession.resetCategorySelection();
        String cartId = null;
        if (mBookingSession.getCart() != null) {
            cartId = mBookingSession.getCart().getCartID();
        }
        if (!isViewAttached() || TextUtils.isEmpty(cartId) || !shouldRefreshCartDetails()) {
            LogUtils.LOGD(TAG, "getCartDetails : Cart is Empty ");
            return;
        }

        if (mGetCartDetails != null) mGetCartDetails.cancel();

        getMvpView().showProgressBar("");

        mGetCartDetails =
          mDataManager.getApiManager().getCartDetails(cartId, new QuikrNetworkRequest.Callback<APICartResponse>() {
              @Override
              public void onSuccess(APICartResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.isSuccess()) {
                          mBookingSession.updateCartResponse(response);
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GET_CART_DETAILS, response.getError().getCode(),
                              response.getError().getMessage());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
              }
          });
        mGetCartDetails.execute();
    }

    /**
     * This method is used to clear the cart details
     * if Any one booking is confirmed return true,otherwise return false
     * to refresh the cart details
     */
    private boolean shouldRefreshCartDetails() {
        if (mResponse != null
          && mResponse.getData() != null
          && mResponse.getData().getBucketInfo() != null
          && !mResponse.getData().getBucketInfo().isEmpty()) {
            return true;
        }

        return false;
    }

    @Override
    public void detachView() {
        LogUtils.LOGD(TAG, "detachView : ");
        if (mGetCartDetails != null) mGetCartDetails.cancel();
        super.detachView();
    }
}
