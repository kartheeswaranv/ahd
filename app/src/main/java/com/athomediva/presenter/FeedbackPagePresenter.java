package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.RatingsModel;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.mvpcontract.FeedbackPageContract;
import com.athomediva.ui.feedback.FeedbackActivity;
import com.athomediva.ui.feedback.FeedbackSuccessFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.List;
import www.zapluk.com.R;

import static com.athomediva.ui.feedback.FeedbackActivity.FRAG_TAG_NAME;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public class FeedbackPagePresenter extends BasePresenter<FeedbackPageContract.View>
  implements FeedbackPageContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(FeedbackPagePresenter.class.getSimpleName());

    private Bundle mBundle;
    private String mBookingId;
    private String mBucketName;
    private QuikrNetworkRequest mFeedbackSubmitReq;

    private List<RatingsModel> mRatingList;

    public FeedbackPagePresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull Bundle bundle) {
        super(dataManager, context, session);
        mBundle = bundle;
        getExtras();
        LogUtils.LOGD(TAG, "FeedbackPagePresenter : ");
    }

    @Override
    public void attachView(@NonNull FeedbackPageContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        getMvpView().updateSubTitle(mBucketName);
        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            mRatingList = mDataManager.getDataProcessor()
              .convertConfigToRating(mBookingSession.getMetaData().getData().getRatingsConfig());
            getMvpView().updateRatingListView(mRatingList);
        }
    }

    @DebugLog
    @Override
    public void onSubmitClick(String comments) {
        LogUtils.LOGD(TAG, "onSubmitClick : ");
        if (TextUtils.isEmpty(getValidationError())) {
            submitFeedback(comments);
        } else {
            getMvpView().showValidationMessage(getValidationError());
        }
    }

    private void getExtras() {
        LogUtils.LOGD(TAG, "getExtras : ");
        if (mBundle != null) {
            mBucketName = mBundle.getString(FeedbackActContract.PARAM_SUBTITLE);
            mBookingId = mBundle.getString(FeedbackActContract.PARAM_BOOKING_ID);
        }
    }

    private String getValidationError() {
        LogUtils.LOGD(TAG, "getValidationError : ");
        if (mRatingList != null && mRatingList.size() > 0) {
            for (RatingsModel model : mRatingList) {
                if (!model.isOptional() && model.getRating() == 0) {
                    String rating_error = mContext.getString(R.string.rating_validation_error, model.getRatingKey());
                    return rating_error;
                }
            }
        }

        return null;
    }

    private void launchSuccessPage() {
        LogUtils.LOGD(TAG, "launchSuccessPage : ");
        Bundle b = new Bundle();
        b.putString(FRAG_TAG_NAME, FeedbackSuccessFragment.FRAG_TAG);
        if (DataValidator.hasAverageRating(mRatingList)) {
            b.putBoolean(FeedbackActContract.PARAM_IS_ENABLE_RATE, true);
        } else {
            b.putBoolean(FeedbackActContract.PARAM_IS_ENABLE_RATE, false);
        }
        b.putString(FeedbackActContract.PARAM_SUBTITLE, mBucketName);
        b.putString(FeedbackActContract.PARAM_BOOKING_ID, mBookingId);
        getMvpView().launchActivity(b, FeedbackActivity.class);
    }

    private void submitFeedback(String comments) {
        LogUtils.LOGD(TAG, "submit : ");
        if (!isViewAttached()) return;

        if (mFeedbackSubmitReq != null) {
            mFeedbackSubmitReq.cancel();
        }

        getMvpView().showProgressBar("");

        mFeedbackSubmitReq = mDataManager.getApiManager()
          .submtiFeedback(mBookingId, comments, mRatingList, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
              @Override
              public void onSuccess(APIGenericResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {
                      if (response.isSuccess()) {
                          launchSuccessPage();
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_FEEDBACK_SUBMIT, response.getError());

                          if (response.getError() != null) {
                              ToastSingleton.getInstance().showToast(response.getError().getMessage());
                          }
                          // If Api Fails We need to enable the back press option
                          getMvpView().enableBackPress(true);
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  // If Api Fails We need to enable the back press option
                  getMvpView().enableBackPress(true);
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mFeedbackSubmitReq), true);
              }
          });

        mFeedbackSubmitReq.execute();
    }
}
