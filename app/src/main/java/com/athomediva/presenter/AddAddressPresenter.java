package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.DataValidator;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.AddAddressItem;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.models.remote.Location;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AddAddressPageContract;
import com.athomediva.mvpcontract.LocationPickerContract;
import com.athomediva.mvpcontract.MapLocationPickerContract;
import com.athomediva.ui.map.MapLocationPickerActivity;
import com.google.android.gms.maps.model.LatLng;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;
import static com.athomediva.mvpcontract.AddAddressPageContract.PARAM_CIRCLE_NAME;

/**
 * Created by mohitkumar on 10/04/17.
 */

public class AddAddressPresenter extends BasePresenter<AddAddressPageContract.View>
  implements AddAddressPageContract.Presenter {
    private final String TAG = LogUtils.makeLogTag(AddAddressPresenter.class.getSimpleName());
    private final int MAP_LOCATION_PICKER_REQ_CODE = 1001;
    private final AddAddressItem item = new AddAddressItem();
    private final Bundle mBundle;
    private QuikrNetworkRequest mGetBoundsReq;
    private QuikrNetworkRequest mAddAddress;

    @Inject
    public AddAddressPresenter(@NonNull DataManager dataManager, @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context);
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull AddAddressPageContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        updateView();
    }

    @Override
    public void onGetCurrentLocationClick() {
        LOGD(TAG, "onGetCurrentLocationClick: ");
        String circle = mBundle.getString(PARAM_CIRCLE_NAME);
        if (!TextUtils.isEmpty(circle)) {
            fetchCircleBounds(circle);
        } else {
            launchMapLocationPicker(null);
        }
    }

    @Override
    public void onAddressTypeClick(String menu) {
        LOGD(TAG, "onAddressTypeClick: item =" + menu);
        item.getAddressTypeEntity().setData(menu);
        item.getAddressTypeEntity().setValid(true);
        item.getCustomAddressEntity().setData("");
        updateView();
        if (menu.equals(mContext.getResources().getString(R.string.address_custom))) {
            LOGD(TAG, "onMenuItemClick: custom name");
            getMvpView().updateCustomViewVisibility(true);
        } else {
            getMvpView().updateCustomViewVisibility(false);
        }
    }

    @Override
    public void onSaveClick() {
        LOGD(TAG, "onSaveClick: ");
        if (DataValidator.validateAddAddressDetails(mContext, item)) {
            onSaveAddress();
        } else {
            LOGE(TAG, "onSaveClick: *** data validation failed ");
        }
        updateView();
    }

    private void updateView() {
        LOGD(TAG, "updateView: ");
        if (!isViewAttached()) {
            LOGE(TAG, "updateView:  *****");
            return;
        }
        getMvpView().updateViews(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOGD(TAG, "onActivityResult: requestCode" + requestCode);
        if (resultCode == RESULT_OK && requestCode == MAP_LOCATION_PICKER_REQ_CODE && data != null) {
            LOGD(TAG, "onActivityResult: LOCATION_PICKER_REQ_CODE");
            Geolocation geolocation = data.getParcelableExtra(MapLocationPickerContract.PARAM_SELECTED_GEOLOCATION);
            if (geolocation != null) {
                item.getFullAddressEntity().setValid(true);
                item.getFullAddressEntity().setData(geolocation);
                updateView();
                LOGD(TAG, "onActivityResult: MAP_LOCATION_PICKER_REQ_CODE address = " + geolocation);
            } else {
                LOGE(TAG, "onActivityResult: geolocation is null ");
            }
        }
    }

    private void onSaveAddress() {
        LOGD(TAG, "onSaveAddress: ");

        if (mAddAddress != null) mAddAddress.cancel();
        getMvpView().showProgressBar("");
        mAddAddress = mDataManager.getApiManager().addAddress(item, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
            @Override
            public void onSuccess(APIGenericResponse response) {
                if (!isViewAttached()) return;
                LOGD(TAG, "onSuccess: response =" + response);
                getMvpView().hideProgressBar();

                if (response != null) {
                    Bundle bundle = new Bundle();
                    if (response.isSuccess()) {
                        LOGD(TAG, "onSuccess: address added");
                        getMvpView().finishViewWithResult(RESULT_OK, bundle);
                    } else {
                        if (response.getError() != null) {
                            LOGD(TAG, "onSuccess: address added failed " + response.getError().getMessage());
                            ToastSingleton.getInstance().showToast(response.getError().getMessage());
                            getMvpView().finishViewWithResult(RESULT_CANCELED, bundle);
                        }
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError: ");
                onNetworkError(errorCode, errorMessage, new WeakReference<>(mAddAddress), true);
            }
        });
        mAddAddress.execute();
    }

    private void fetchCircleBounds(String circleName) {
        LOGD(TAG, "fetchCircleBounds : ");
        /*if (mGetBoundsReq != null) mGetBoundsReq.cancel();

        if (!isViewAttached()) return;

        getMvpView().showProgressBar("");

        mGetBoundsReq =
          mDataManager.getApiManager().getBoundsByCircle(circleName, new QuikrNetworkRequest.Callback<BoundsResponse>() {
              @Override
              public void onSuccess(BoundsResponse response) {
                  LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;

                  getMvpView().hideProgressBar();

                  if (response != null && response.getBounds() != null) {
                      launchMapLocationPicker(response.getBounds().get(0));
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError : ");
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetBoundsReq));
              }
          });
        mGetBoundsReq.execute();*/
        launchMapLocationPicker(null);
    }

    private void launchMapLocationPicker(ArrayList<Location> list) {
        LOGD(TAG, "launchMapLocationPicker : ");
        Bundle bundle = new Bundle();
        bundle.putBoolean(LocationPickerContract.PARAM_SHOW_USER_ADDRESS_LIST, false);
        if (list != null && !list.isEmpty()) {
            bundle.putParcelableArrayList(MapLocationPickerContract.PARAM_BOUNDS,
              (ArrayList<LatLng>) mDataManager.getDataProcessor().convertLocObjToLatLng(list));
        }
        getMvpView().launchActivityForResult(bundle, MapLocationPickerActivity.class, MAP_LOCATION_PICKER_REQ_CODE);
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        if (mGetBoundsReq != null) mGetBoundsReq.cancel();
        if (mAddAddress != null) mAddAddress.cancel();
        super.detachView();
    }
}

