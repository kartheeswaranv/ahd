package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.remote.APIGetMetaDataResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.ui.feedback.FeedbackFragment;
import com.athomediva.ui.feedback.FeedbackSuccessFragment;
import com.quikrservices.android.network.QuikrNetworkRequest;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;
import static com.athomediva.ui.feedback.FeedbackActivity.FRAG_TAG_NAME;

/**
 * Created by kartheeswaran on 10/07/17.
 */

public class FeedbackActPresenter extends BasePresenter<FeedbackActContract.View> implements FeedbackActContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(FeedbackActPresenter.class.getSimpleName());

    private String mSubTitle;
    private String mBookingId;

    private Bundle mBundle;
    private String mTagName;
    private boolean mIsMandatory;

    private QuikrNetworkRequest mMetaDataRequest;

    public FeedbackActPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull Bundle bundle) {
        super(dataManager, context, session);
        mBundle = bundle;
        LogUtils.LOGD(TAG, "FeedbackPagePresenter : ");
    }

    @Override
    public void attachView(@NonNull FeedbackActContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");

        if (mDataManager.getPrefManager() != null && !mDataManager.getPrefManager().isUserLoggedIn()) {
            LogUtils.LOGD(TAG, "attachView : User is not logged in");
            getMvpView().finish();
        }
        handleBundle(mBundle);
    }

    @Override
    public void onNewIntent(Bundle b) {
        LogUtils.LOGD(TAG, "onNewIntent : ");
        handleBundle(b);
    }

    private void handleBundle(Bundle bundle) {
        LogUtils.LOGD(TAG, "handleBundle : ");
        mIsMandatory = false;
        if (bundle != null) {
            if (bundle.containsKey(DeeplinkConstants.KEY_ACTION) && TextUtils.equals(
              bundle.getString(DeeplinkConstants.KEY_ACTION), Intent.ACTION_VIEW)) {
                Uri uri = bundle.getParcelable(DeeplinkConstants.KEY_DEEPLINK_URL);
                mBookingId = uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.ORDER_ID);
                mTagName = FeedbackFragment.FRAG_TAG;
                mBundle.putString(FeedbackActContract.PARAM_BOOKING_ID, mBookingId);
            } else if (bundle.containsKey(FeedbackActContract.PARAM_BOOKING_ID) && !TextUtils.isEmpty(
              bundle.getString(FeedbackActContract.PARAM_BOOKING_ID))) {
                LogUtils.LOGD(TAG, "attachView : Booking Id is not empty");
                mTagName = bundle.getString(FRAG_TAG_NAME);
                mBookingId = bundle.getString(FeedbackActContract.PARAM_BOOKING_ID);
                mSubTitle = bundle.getString(FeedbackActContract.PARAM_SUBTITLE);
                mIsMandatory = bundle.getBoolean(FeedbackActContract.PARAM_MANDATORY);
            } else {
                LogUtils.LOGD(TAG, "attachView : Booking Id is missing");
                getMvpView().finish();
            }
        }

        if (TextUtils.isEmpty(mTagName) || TextUtils.isEmpty(mBookingId)) {
            getMvpView().finish();
            return;
        }

        getMvpView().enableHomeBtn(!mIsMandatory);
        updateHeader(mContext.getString(R.string.rating_page_title), mSubTitle);
        if (mBookingSession.getMetaData() != null) {
            getMvpView().launchFragment(mTagName, bundle);
        } else {
            getHomePageMetaData();
        }
    }

    private void updateHeader(String title, String subTitle) {
        LogUtils.LOGD(TAG, "updateHeader : ");
        getMvpView().updateTitle(title);
        getMvpView().updateSubTitle(subTitle);
    }

    @Override
    public void onBackPressed() {
        LogUtils.LOGD(TAG, "onBackPressed : ");
        if (!mIsMandatory) {
            if (!TextUtils.equals(mTagName, FeedbackSuccessFragment.FRAG_TAG)) {
                getMvpView().finish();
            } else {
                getMvpView().finishViewWithResult(RESULT_OK);
            }
        } else {
            LogUtils.LOGD(TAG, "onBackPressed : Back press disable if rating is mandatory");
        }
    }

    private void getHomePageMetaData() {
        LogUtils.LOGD(TAG, "getHomePageMetaData : ");
        if (mMetaDataRequest != null) {
            mMetaDataRequest.cancel();
        }
        getMvpView().showProgressBar("");

        mMetaDataRequest =
          mDataManager.getApiManager().getAppMetaData(new QuikrNetworkRequest.Callback<APIGetMetaDataResponse>() {
              @Override
              public void onSuccess(APIGetMetaDataResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  mBookingSession.setMetaData(response);
                  getMvpView().launchFragment(mTagName, mBundle);
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGE(TAG, "onError: ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
              }
          });
        mMetaDataRequest.execute();
    }

    @Override
    public void onHomeBtnStatusChange(boolean enable) {
        LogUtils.LOGD(TAG, "onHomeBtnStatusChange : ");
        // If Api Failure we should  make mandatory flag as false
        // This flag should update while changing the status
        mIsMandatory = !enable;
        // Clear the Orders to review if it is not mandatory
        if (!mIsMandatory) {
            clearOrdersToReview();
        }
    }

    @Override
    public void detachView() {
        LogUtils.LOGD(TAG, "detachView : ");
        if (mMetaDataRequest != null) mMetaDataRequest.cancel();
        super.detachView();
    }

    private Bundle getBundle() {
        Bundle b = new Bundle();
        b.putString(FeedbackActContract.PARAM_SUBTITLE, mSubTitle);
        b.putString(FeedbackActContract.PARAM_BOOKING_ID, mBookingId);

        return b;
    }

    private void clearOrdersToReview() {
        LogUtils.LOGD(TAG, "clearOrdersToReview : ");
        // Clear the OrdersToReview in Meta Data while feedback is done or cancel
        mBookingSession.clearOrdersToReview();
    }
}
