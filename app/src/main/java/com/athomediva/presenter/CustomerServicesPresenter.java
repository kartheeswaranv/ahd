package com.athomediva.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.CustomerServicesContract;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class CustomerServicesPresenter extends BasePresenter<CustomerServicesContract.View>
  implements CustomerServicesContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(CustomerServicesPresenter.class.getSimpleName());

    public CustomerServicesPresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        super(dataManager, context);
    }

    @Override
    public void attachView(@NonNull CustomerServicesContract.View mvpView) {
        super.attachView(mvpView);
        updateView();
    }

    @Override
    public void onPhoneServicesClick() {
        LOGD(TAG, "onPhoneServicesClick: ");
        AndroidHelper.callToPhoneNumber(mContext, AppConstants.AHD_CUSTOMER_CARE_NUMBER);
    }

    @Override
    public void onEmailServicesClick() {
        LOGD(TAG, "onEmailServicesClick: ");
        AndroidHelper.sendSupportEmail(mContext);
    }

    @Override
    public void onSocialServiceClick(SocialSharingItem item) {
        LOGD(TAG, "onSocialServiceClick: item =" + item.getTitle());
        AndroidHelper.launchViewForAction(mContext, item.getLaunchIntent());
    }

    private void updateView() {
        TitleDescImageItem callUsItem =
          new TitleDescImageItem(R.drawable.ahd_ic_call, mContext.getString(R.string.call_us_on),
            AppConstants.AHD_CUSTOMER_CARE_NUMBER);
        getMvpView().updateCallUsView(callUsItem);
        TitleDescImageItem emailItem =
          new TitleDescImageItem(R.drawable.ahd_ic_mail_blue, mContext.getString(R.string.write_us),
            AppConstants.AHD_EMAIL_ID);
        getMvpView().updateEmailView(emailItem);
        getMvpView().updateSocialNetworkingServices(mContext.getString(R.string.follow_us_on),
          AppConstants.getSocialLinks(mContext));
    }
}
