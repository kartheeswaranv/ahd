package com.athomediva.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.remote.APIGenerateOtpResponse;
import com.athomediva.data.models.remote.APIVerifyOTPResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.helper.AndroidHelper;
import com.athomediva.helper.SMSReader;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.inject.Inject;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 04/04/17.
 * Check for permission
 * if given show auto verify register for sms broadcast
 * increment auto verify every second
 * hit send otp api
 *
 * manual click remove the sms broadcast
 * hide manual click view
 * on fill hit api
 */

public class OTPVerificationPresenter extends BasePresenter<OTPVerificationContract.View>
  implements OTPVerificationContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(OTPVerificationPresenter.class.getSimpleName());
    private final PermissionManager mPermissionHelper;
    private final Consumer mConsumer;
    private QuikrNetworkRequest mOtpVerRequest, mResendOTP;
    private boolean mIsUpdateMobile;

    private final SMSReader mSmsReceiver = new SMSReader() {
        @Override
        protected void GotVerifyCode(String code) {
            LOGD(TAG, "GotVerifyCode: code =" + code);
            if (!isViewAttached()) return;
            getMvpView().fillOTPAutomatically(code);
            startVerification(code);
        }
    };

    @Inject
    public OTPVerificationPresenter(@NonNull DataManager dataManager, @NonNull Context context,
      @NonNull PermissionManager permissionHelper, @NonNull Bundle bundle) {
        super(dataManager, context);
        mPermissionHelper = permissionHelper;
        mConsumer = bundle.getParcelable(OTPVerificationContract.PARAM_CONSUMER);
        mIsUpdateMobile = bundle.getBoolean(OTPVerificationContract.PARAM_IS_UPDATE_MOBILE);
    }

    @Override
    public void otpEntered(@NonNull String otp) {
        LOGD(TAG, "otpEntered: ");
        if (otp == null || otp.length() < 6) {
            LOGE(TAG, "otpEntered *** invalid entered otp ");
            return;
        }
        startVerification(otp);
    }

    @Override
    public void attachView(@NonNull OTPVerificationContract.View mvpView) {
        LOGD(TAG, "attachView: ");
        super.attachView(mvpView);
        getMvpView().updateMobileNumberView(mConsumer.getUserNumber());
        setUpViewState();
    }

    @DebugLog
    private void setUpViewState() {
        LOGD(TAG, "setUpViewState: ");
        if (AndroidHelper.isOTPPermissionGranted(mContext)) {
            getMvpView().updateAutoVerificationView(true);
            getMvpView().enableOTPMode(false);
            startAutoVerification();
        } else {
            enterManualClick();
        }
    }

    @Override
    public void enterManualClick() {
        LOGD(TAG, "enterManualClick: ");
        getMvpView().enableOTPMode(true);
        getMvpView().updateAutoVerificationView(false);
        stopAutoVerification();
    }

    @Override
    public void editNumberClick() {
        LOGD(TAG, "editNumberClick: ");
        getMvpView().finish();
    }

    @Override
    public void resendOTPClick() {
        LOGD(TAG, "resendOTPClick: ");
        ArrayList<Permission> smsPermissions = new ArrayList<>();
        smsPermissions.add(new Permission(Manifest.permission.READ_SMS));
        smsPermissions.add(new Permission(Manifest.permission.RECEIVE_SMS));
        mPermissionHelper.checkAndRequestForPermissions(smsPermissions,
          (isAllAreGranted, grantedPermission, deniedPermission) -> resendOTPApiRequest());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LOGD(TAG, "onRequestPermissionsResult: ");
        mPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void StopSMSBroadCast() {
        LOGD(TAG, "StopSMSBroadCast: ");
        if (mSmsReceiver != null && mContext != null) {
            try {
                mContext.unregisterReceiver(this.mSmsReceiver);
            } catch (IllegalArgumentException e) {
                LOGE(TAG, "StopSMSBroadCast  Exception : Receiver is not registered");
            }
        }
    }

    private void registerForSmsBroadcast() {
        LOGD(TAG, "registerForSmsBroadcast: ");
        if (!isViewAttached()) return;
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(99999);
        mContext.registerReceiver(mSmsReceiver, intentFilter);
    }

    private void resendOTPApiRequest() {
        LOGD(TAG, "resendOTPApiRequest : ");
        if (!isViewAttached()) return;
        setUpViewState();

        if (mResendOTP != null) {
            mResendOTP.cancel();
        }

        getMvpView().showProgressBar(mContext.getString(R.string.sending_otp));

        mResendOTP = mDataManager.getApiManager()
          .genOTP(mConsumer, mIsUpdateMobile, new QuikrNetworkRequest.Callback<APIGenerateOtpResponse>() {
              @Override
              public void onSuccess(APIGenerateOtpResponse response) {
                  LOGD(TAG, "onSuccess: resendOTP");
                  if (!isViewAttached()) return;

                  getMvpView().hideProgressBar();
                  getMvpView().showOtpError(false, "");
                  if (response != null) {
                      if (response.getSuccess()) {
                          ToastSingleton.getInstance().showToast(R.string.otp_sent);
                      } else {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GEN_OTP, response.getError());
                          if (response.getError() != null) {
                              getMvpView().showOtpError(true, response.getError().getMessage());
                          } else {
                              ToastSingleton.getInstance().showToast(R.string.try_again);
                          }
                      }
                  } else {
                      ToastSingleton.getInstance().showToast(R.string.try_again);
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: errorCode =" + errorCode + " errorMessage =" + errorMessage);
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mResendOTP), true);
              }
          });

        mResendOTP.execute();
    }

    private void startAutoVerification() {
        LOGD(TAG, "startAutoVerification: ");
        stopAutoVerification();
        registerForSmsBroadcast();
    }

    private void stopAutoVerification() {
        LOGD(TAG, "stopAutoVerification: ");
        StopSMSBroadCast();
    }

    @Override
    public void detachView() {
        LOGD(TAG, "detachView: ");
        if (mOtpVerRequest != null) {
            mOtpVerRequest.cancel();
        }

        if (mResendOTP != null) {
            mResendOTP.cancel();
        }

        stopAutoVerification();
        super.detachView();
    }

    private void handleVerificationResponse(APIVerifyOTPResponse response) {
        LOGD(TAG, "handleVerificationResponse: ");
        if (!isViewAttached()) return;

        if (response == null) {
            LOGE(TAG, "handleVerificationResponse: null ***");
            return;
        }

        if (response.getSuccess()
          && response.getData() != null
          && response.getData().getUserDetails() != null
          && response.getData().getToken() != null) {
            LOGD(TAG, "handleVerificationResponse: success");
            LOGD(TAG, "token == " + response.getData().getToken());
            getMvpView().updateVerificationStatusView(true);
            Bundle bundle = new Bundle();
            bundle.putParcelable(OTPVerificationContract.PARAM_RESULT_USER, response.getData().getUserDetails());
            bundle.putString(OTPVerificationContract.PARAM_RESULT_TOKEN, response.getData().getToken());
            getMvpView().finishViewWithResult(Activity.RESULT_OK, bundle);
        } else {
            LOGE(TAG, "handleVerificationResponse: failed");
            mDataManager.getAnalyticsManager().trackGAEventForAPIFail(AppUrls.URL_VERIFY_OTP, response.getError());
            if (response.getError() != null) {
                getMvpView().showOtpError(true, response.getError().getMessage());
            } else {
                ToastSingleton.getInstance().showToast(R.string.try_again);
            }
        }
    }

    private void startVerification(String otp) {
        LOGD(TAG, "startVerification: otp =" + otp);
        if (!isViewAttached()) return;

        if (mOtpVerRequest != null) {
            mOtpVerRequest.cancel();
        }

        if (mConsumer == null || otp == null) {
            LOGE(TAG, "startVerification: consumer or otp is null ****");
            return;
        }

        if (otp.length() != AppConstants.OTP_LENGTH) {
            LOGE(TAG, "startVerification: otp length is not valid");
            getMvpView().showOtpError(true, mContext.getString(R.string.msg_otp_validation_error));
            return;
        }

        mOtpVerRequest = mDataManager.getApiManager()
          .verifyOTP(mConsumer, otp, mIsUpdateMobile, new QuikrNetworkRequest.Callback<APIVerifyOTPResponse>() {
              @Override
              public void onSuccess(APIVerifyOTPResponse response) {
                  LOGD(TAG, "onSuccess: verify otp");
                  if (!isViewAttached()) return;

                  getMvpView().hideProgressBar();
                  if (response != null) {
                      handleVerificationResponse(response);
                  } else {
                      ToastSingleton.getInstance().showToast(R.string.try_again);
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGE(TAG, "onError: errorCode =" + errorCode + " errorMessage =" + errorMessage);
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mOtpVerRequest));
              }
          });

        mOtpVerRequest.execute();
    }
}
