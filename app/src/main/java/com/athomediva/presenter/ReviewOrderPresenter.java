package com.athomediva.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartData;
import com.athomediva.data.models.remote.ConfirmBookingData;
import com.athomediva.data.models.remote.ConfirmBookingError;
import com.athomediva.data.models.remote.ConfirmBookingResponse;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.BookingFailureContract;
import com.athomediva.mvpcontract.BookingSuccessContract;
import com.athomediva.mvpcontract.InstantCouponContract;
import com.athomediva.mvpcontract.MyAddressPageContract;
import com.athomediva.mvpcontract.MyCartContract;
import com.athomediva.mvpcontract.ReviewOrderContract;
import com.athomediva.ui.AHDMainActivity;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.address.MyAddressFragment;
import com.athomediva.ui.bookingpage.BookingFailureFragment;
import com.athomediva.ui.bookingpage.BookingSuccessFragment;
import com.athomediva.ui.mycart.MyCartActivity;
import com.athomediva.ui.offers.InstantCouponActivity;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.ui.FragmentContainerActivity.PARAM_FRAG_TAG_NAME;

/**
 * Created by kartheeswaran on 04/05/17.
 */

public class ReviewOrderPresenter extends BasePresenter<ReviewOrderContract.View> implements ReviewOrderContract.Presenter {

    private static final String TAG = LogUtils.makeLogTag(ReviewOrderPresenter.class.getSimpleName());
    private static final int ADDRESS_REQUEST_CODE = 301;
    private QuikrNetworkRequest mWalletInfoRequest;
    private CartData mCartData;

    private CartUpdationTask mCartUpdateTask;
    private QuikrNetworkRequest mConfirmBookingReq;
    private Bundle mBundle;

    public ReviewOrderPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull CartUpdationTask task, Bundle bundle) {
        super(dataManager, context, session);
        LOGD(TAG, "ReviewOrderPresenter : ");
        mCartUpdateTask = task;
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull ReviewOrderContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView : ");
        if (mBundle != null && mBundle.containsKey(ReviewOrderContract.PARAM_CART_RESPONSE)) {
            handleCartResponse(mBundle.getParcelable(ReviewOrderContract.PARAM_CART_RESPONSE));
        } else {
            LogUtils.LOGD(TAG, "attachView : Response is empty");
        }

        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            LogUtils.LOGD(TAG, "attachView:Update Instant Coupon ");
            getMvpView().updateInstantCouponView(mBookingSession.getMetaData().getData().getInstantCoupon(), false);
        }
    }

    @Override
    public void onCartEditClick() {
        LOGD(TAG, "onCartEditClick : ");
        launchReschedulePage();
    }

    @Override
    public void onCouponApplyClick(String couponCode) {
        LogUtils.LOGD(TAG, "onCouponApplyClick : ");
        if (mBookingSession.getCart() != null) {
            BookingRequestData data =
              mBookingSession.getCart().updateCoupon(AppConstants.CART_ACTION.UPDATE_COUPON.getAction(), couponCode);
            updateCart(data);
        } else {
            LogUtils.LOGD(TAG, "onCouponApplyClick : Cart is null");
        }
    }

    @Override
    public void onCouponClearClick() {
        LogUtils.LOGD(TAG, "onCouponClearClick : ");
        if (mBookingSession.getCart() != null) {
            BookingRequestData data =
              mBookingSession.getCart().removeCoupon(AppConstants.CART_ACTION.REMOVE_COUPON.getAction());
            updateCart(data);
        } else {
            LogUtils.LOGD(TAG, "onCouponClearClick : Cart is null");
        }
    }

    @Override
    public void onWalletInfoClick() {
        LOGD(TAG, "onWalletInfoClick : ");
        getWalletBreakUp();
    }

    @Override
    public void onAddressChangeClick() {
        LOGD(TAG, "onAddressChangeClick : ");
        //launchAddressPage();
    }

    private void launchAddressPage() {
        LOGD(TAG, "launchAddressPage : ");
        if (mCartData != null && mCartData.getAddress() != null) {
            Bundle b = new Bundle();
            b.putString(PARAM_FRAG_TAG_NAME, MyAddressFragment.TAG);
            //b.putString(MyAddressPageContract.PARAM_CIRCLE_NAME, mCartData.getAddress().getCircle());
            getMvpView().launchActivityForResult(b, FragmentContainerActivity.class, ADDRESS_REQUEST_CODE);
        }
    }

    @Override
    public void onProceedClick() {
        LOGD(TAG, "onProceedClick : ");
        if (mBookingSession.getCart() != null) {
            confirmBooking(mBookingSession.getCart().getCartID());
        } else {
            LogUtils.LOGD(TAG, "onProceedClick : Cart is null");
        }
    }

    @Override
    public void onRecheduleConfirm() {
        LogUtils.LOGD(TAG, "onRecheduleConfirm : ");
        launchReschedulePage();
    }

    public void onRescheduleCancel(boolean clearCart) {
        LogUtils.LOGD(TAG, "onRescheduleCancel : ");
        if (clearCart) {
            LogUtils.LOGD(TAG, "onRescheduleCancel : Reset the session ");
            // If it is fails reset the category selection
            // but keep the cart id and circlename
            mBookingSession.resetSession();
            launchHomePage();
        }
    }

    private void getWalletBreakUp() {
        LOGD(TAG, "getWalletBreakUp: ");
        if (mCartData != null && mCartData.getWallet() != null) {
            getMvpView().showWalletInfoDialog(mCartData.getWallet());
        }
    }

    @Override
    public void detachView() {
        if (mWalletInfoRequest != null) {
            mWalletInfoRequest.cancel();
        }
        if (mCartUpdateTask != null) {
            mCartUpdateTask.cancel();
        }

        if (mConfirmBookingReq != null) {
            mConfirmBookingReq.cancel();
        }
        super.detachView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ADDRESS_REQUEST_CODE) {
            if (isViewAttached()) {
                Address address = (Address) data.getParcelableExtra(MyAddressPageContract.PARAM_SELECTED_ADDRESS);
                if (address != null) {
                    updateAddressInCart(address);
                }
            }
        }
    }

    private void handleCartResponse(CartData response) {
        LOGD(TAG, "handleCartResponse : ");
        if (response != null) {
            mCartData = response;
            getMvpView().updateServicesView(response.getBuckets());
            getMvpView().updateAddressView(response.getAddress());

            if (!TextUtils.isEmpty(response.getCouponErrorMsg())) {
                getMvpView().setCouponError(response.getCouponErrorMsg());
            }
            if (response.getCoupon() != null) {
                getMvpView().updateCouponView(response.getCoupon());
            }
            if (response.getWallet() != null) {
                getMvpView().updateWalletView(response.getWallet().getTotalWalletAmt(), response.getWallet().getColor());
            } else {
                // To Disable the Views if is empty
                getMvpView().updateWalletView(0, null);
            }

            getMvpView().updatePaymentPendingView(response.isPendingStatus(), response.getPaymentPending(),
              response.getPendingMsg());

            if (response.getCoupon() != null) {
                getMvpView().updateCouponCashbackView(response.getCoupon().getCouponMsg());
            } else {
                getMvpView().updateCouponCashbackView(null);
            }

            getMvpView().updatePaymentInfoView(response.getTotal(), response.getSubTotal());
        }
    }

    private void updateCart(BookingRequestData data) {
        LOGD(TAG, "createOrUpdateCart : ");
        if (data == null || !isViewAttached()) return;

        if (mCartUpdateTask != null) mCartUpdateTask.cancel();

        getMvpView().showProgressBar("", true);

        mCartUpdateTask.execute(data, new QuikrNetworkRequest.Callback<APICartResponse>() {
            @Override
            public void onSuccess(APICartResponse response) {
                LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                if (response != null && response.isSuccess() && response.getData() != null) {
                    mCartData = response.getData();
                    handleCartResponse(mCartData);
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                //onNetworkError(errorCode, errorMessage, null);
            }
        }, this);
    }

    private void confirmBooking(String cartId) {
        LOGD(TAG, "ConfirmBooking : ");
        if (!isViewAttached()) return;

        if (mConfirmBookingReq != null) {
            mConfirmBookingReq.cancel();
        }

        getMvpView().showProgressBar("", true);
        mConfirmBookingReq =
          mDataManager.getApiManager().confirmBooking(cartId, new QuikrNetworkRequest.Callback<ConfirmBookingResponse>() {
              @Override
              public void onSuccess(ConfirmBookingResponse response) {
                  LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null) {

                      if (response.isSuccess() && response.getData() != null) {
                          // After success clear all Category Response
                          // fetch it from home page
                          mBookingSession.resetSession();
                          updateBookingGA(response.getData());
                          launchSuccessPage(response);
                      } else if (response != null && response.getError() != null) {
                          if (response.getError().getCode()
                            == AppConstants.APICodes.ERROR_CODE.BOOKING_CONFIRM_FAILED.getErrorCode()) {
                              updateBookingGA(response.getData());
                              launchBookingFailurePage(response.getError());
                          } else {
                              ToastSingleton.getInstance().showToast(response.getError().getMessage());
                          }
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mConfirmBookingReq), true);
              }
          });

        mConfirmBookingReq.execute();
    }

    private void updateAddressInCart(Address address) {
        LogUtils.LOGD(TAG, "updateAddressInCart : ");
        if (mBookingSession.getCart() == null) {
            LogUtils.LOGD(TAG, "updateAddressInCart : Cart is null");
            return;
        }
        if (address != null) {
            BookingRequestData data = mBookingSession.getCart()
              .updateAddress(AppConstants.CART_ACTION.UPDATE_ADDRESS.getAction(), address.getAddressID());
            //data.setBookingResNeeded(true);
            updateCart(data);
        } else {
            LogUtils.LOGD(TAG, "updateAddressInCart : Address is empty");
        }
    }

    @Override
    public void onInstantCouponClick(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "onInstantCouponClick: ");
        launchInstantCouponPage(coupon);
    }

    private void launchInstantCouponPage(InstantCoupon coupon) {
        LogUtils.LOGD(TAG, "launchInstantCouponPage: ");
        Bundle bundle = new Bundle();
        bundle.putParcelable(InstantCouponContract.PARAM_COUPON_OBJ, coupon);
        getMvpView().launchActivity(bundle, InstantCouponActivity.class);
    }

    private void launchSuccessPage(ConfirmBookingResponse response) {
        LOGD(TAG, "launchSuccessPage : ");
        Bundle b = new Bundle();
        b.putParcelable(BookingSuccessContract.PARAM_RESPONSE, response);

        b.putString(PARAM_FRAG_TAG_NAME, BookingSuccessFragment.FRAG_TAG);
        getMvpView().launchActivity(b, FragmentContainerActivity.class);
        getMvpView().finish();
    }

    private void launchBookingFailurePage(ConfirmBookingError error) {
        LOGD(TAG, "launchSuccessPage : ");
        Bundle b = new Bundle();
        b.putParcelable(BookingFailureContract.PARAM_RESPONSE, error);

        b.putString(PARAM_FRAG_TAG_NAME, BookingFailureFragment.FRAG_TAG);
        getMvpView().launchActivity(b, FragmentContainerActivity.class);
        getMvpView().finish();
    }

    private void launchReschedulePage() {
        LogUtils.LOGD(TAG, "launchReschedulePage : ");
        //TODO Launch Via Deeplink
        Intent intent = new Intent(mContext, MyCartActivity.class);
        if (mBookingSession.getCart() != null) {
            intent.putExtra(MyCartContract.PARAM_CART_ID, mBookingSession.getCart().getCartID());
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
    }

    private void launchHomePage() {
        //TODO Launch Via Deeplink
        LogUtils.LOGD(TAG, "launchHomePage : ");
        Intent intent = new Intent(mContext, AHDMainActivity.class);
        intent.putExtra(AHDMainViewContract.PARAM_NAVIGATION_TAG, mContext.getString(R.string.booking));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getMvpView().launchActivity(intent);
    }

    private void updateBookingGA(ConfirmBookingData data) {
        if (data != null) {
            String ids = CoreLogic.getbookingIdsForGA(data.getBucketInfo());
            if (!TextUtils.isEmpty(ids)) {
                mDataManager.getAnalyticsManager()
                  .trackEventGA(GATrackerContext.Category.BOOKINGS, GATrackerContext.Action.CONFIRM_BOOKING_IDS, ids);
            }
        }
    }
}
