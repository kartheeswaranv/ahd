package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.busevents.CartUpdateEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.BookingRequestData;
import com.athomediva.data.models.remote.APICartResponse;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.network.CartUpdationTask;
import com.athomediva.data.session.BookingSession;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.ServicesContract;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.athomediva.logger.LogUtils.LOGD;

/**
 * Created by kartheeswaran on 17/04/17.
 */

public class ServicesPagePresenter extends BasePresenter<ServicesContract.View> implements ServicesContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(HomePagePresenter.class.getSimpleName());

    private int mSelectedPosition;
    private ArrayList<Services> mList;
    private QuikrNetworkRequest mCreateCartReq;
    private CartUpdationTask mCartUpdateTask;

    public ServicesPagePresenter(@NonNull DataManager dataManager, @NonNull BookingSession session, @NonNull Context context,
      @NonNull CartUpdationTask task, @NonNull Bundle b) {
        super(dataManager, context, session);
        mDataManager.getBusManager().register(this);
        mCartUpdateTask = task;
        LogUtils.LOGD(TAG, "ServicesPagePresenter : ");
        if (b != null) {
            mSelectedPosition = b.getInt(ServicesContract.PARAM_SELECT_POSITION);
            mList = b.getParcelableArrayList(ServicesContract.PARAM_SELECTED_SERVICES);
        }
    }

    @Override
    public void attachView(@NonNull ServicesContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        onEventMainThread(new CartUpdateEvent());
        getMvpView().updateServices(mList);
    }

    @DebugLog
    @Override
    public void onInfoClick(Services model) {
        LogUtils.LOGD(TAG, "onInfoClick : ");
        if (model != null && model.getInfo() != null) {
            getMvpView().showInfoDialog(model.getName(), model.getInfo());
        }
    }

    @DebugLog
    @Override
    public void onMinusClick(Services model) {
        LogUtils.LOGD(TAG, "onMinusClick : ");
        createOrUpdateCart(AppConstants.CART_ACTION.DELETE_SERVICE, model);
    }

    @DebugLog
    @Override
    public void onPlusClick(Services model) {
        LogUtils.LOGD(TAG, "onPlusClick : ");
        createOrUpdateCart(AppConstants.CART_ACTION.ADD_SERVICE, model);
    }

    private void createOrUpdateCart(AppConstants.CART_ACTION action, Services service) {
        LogUtils.LOGD(TAG, "createOrUpdateCart : ");
        if (service == null || !isViewAttached() || mBookingSession.getCart() == null) return;

        if (mCartUpdateTask != null) mCartUpdateTask.cancel();

        BookingRequestData data = mBookingSession.getCart().updateServices(action.getAction(), service.getServiceId());

        getMvpView().showProgressBar("");

        mCartUpdateTask.execute(data, new QuikrNetworkRequest.Callback<APICartResponse>() {
            @Override
            public void onSuccess(APICartResponse response) {
                LogUtils.LOGD(TAG, "onSuccess : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                LogUtils.LOGD(TAG, "onError : ");
                if (!isViewAttached()) return;
                getMvpView().hideProgressBar();
                //onNetworkError(errorCode, errorMessage, new WeakReference<>(mSignUpRequest));
            }
        }, this);
    }

    public void updateServices() {
        LogUtils.LOGD(TAG, "updateServices : ");
        if (mBookingSession.getCart() == null) {
            LogUtils.LOGD(TAG, "updateServices : Cart is null");
            return;
        }
        if (mList != null && !mList.isEmpty()) {
            CoreLogic.updateServices(mBookingSession.getCart().getServicesMap(), mList);
            getMvpView().updateServices(mList);
        }
    }

    @Override
    public void detachView() {
        if (mCartUpdateTask != null) {
            mCartUpdateTask.cancel();
        }
        mDataManager.getBusManager().unregister(this);
        super.detachView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(final CartUpdateEvent event) {
        LOGD(TAG, "onEvent CartUpdateEvent Event");
        if (event != null && isViewAttached() && mBookingSession.getCart() != null) {
            updateServices();
        }
    }
}

