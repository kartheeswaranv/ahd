package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.CoreLogic;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.OptionModel;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.CancelBookingContract;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import www.zapluk.com.R;

/**
 * Created by kartheeswaran on 28/05/17.
 */

public class CancelBookingPresenter extends BasePresenter<CancelBookingContract.View>
  implements CancelBookingContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(CancelBookingPresenter.class.getSimpleName());

    private Bundle mBundle;
    private List<OptionModel> mOptionList;

    public CancelBookingPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session,
      @NonNull Context context, @NonNull Bundle bundle) {
        super(dataManager, context, session);
        LogUtils.LOGD(TAG, "CancelBookingPresenter : ");
        mBundle = bundle;
    }

    @Override
    public void attachView(@NonNull CancelBookingContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");
        getMvpView().updateCTAView(false);
        if (mBookingSession.getMetaData() != null
          && mBookingSession.getMetaData().getData() != null
          && mBookingSession.getMetaData().getData().getCancelReasons() != null) {
            mOptionList = createOptionList(mBookingSession.getMetaData().getData().getCancelReasons());

            if (mOptionList != null && mOptionList.size() > 0) {

                mOptionList.get(mOptionList.size() - 1).setEditable(true);
                getMvpView().updateOptionsView(Collections.unmodifiableList(mOptionList));
            }
        } else {
            LogUtils.LOGD(TAG, "attachView : There is no Cancel Reasons");
            getMvpView().finish();
        }
    }

    @Override
    public void onCancelClick() {
        LogUtils.LOGD(TAG, "onCancelClick : ");
        int selectedPos = CoreLogic.getSelectedOption(mOptionList);
        if (selectedPos > -1) {
            if (selectedPos == mOptionList.size() - 1 && TextUtils.isEmpty(mOptionList.get(selectedPos).getText())) {
                ToastSingleton.getInstance().showToast("Please give valid input");
            } else {
                Bundle b = new Bundle();
                b.putParcelable(BookingDetailsContract.PARAM_SELECTED_OPTION, mOptionList.get(selectedPos));
                getMvpView().finishViewWithResult(Activity.RESULT_OK, b);
            }
        } else {

        }
    }

    @Override
    public void onOptionItemClick(int position) {
        LogUtils.LOGD(TAG, "onOptionItemClick : ");
        handleOptionSelection(position);
        getMvpView().refreshAdapterView();
        getMvpView().updateCTAView(true);
        if (mOptionList.get(position).isEditable()) {
            getMvpView().showEditOptionView(mOptionList.get(position));
        } else {
            getMvpView().hideEditOptionView();
        }
    }

    private List<OptionModel> createOptionList(ArrayList<OptionModel> optionList) {
        LogUtils.LOGD(TAG, "getList : ");
        List<OptionModel> list = new ArrayList<>();
        for (int index = 0; index < optionList.size(); index++) {
            OptionModel model;
            if (index == optionList.size() - 1) {
                model = new OptionModel(optionList.get(index).getId(), optionList.get(index).getTitle(), false, true);
            } else {
                model = new OptionModel(optionList.get(index).getId(), optionList.get(index).getTitle(), false, false);
            }
            list.add(model);
        }
        return list;
    }

    private void handleOptionSelection(int pos) {
        LogUtils.LOGD(TAG, "handleOptionSelection : ");
        for (int index = 0; index < mOptionList.size(); index++) {
            if (pos == index) {
                mOptionList.get(index).setSelected(true);
            } else {
                mOptionList.get(index).setSelected(false);
                // Clear the text when selection change
                mOptionList.get(index).setText("");
            }
        }
    }
}
