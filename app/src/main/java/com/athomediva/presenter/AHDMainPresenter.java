package com.athomediva.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DataValidator;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.busevents.CategoryLoadEvent;
import com.athomediva.data.busevents.UserLogoutEvent;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.remote.APIFullUserDetailsResponse;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.session.UserSession;
import com.athomediva.helper.DeeplinkHandler;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.AHDMainViewContract;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.ui.launcher.AppLauncherActivity;
import com.quikrservices.android.network.QuikrNetworkRequest;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import org.greenrobot.eventbus.Subscribe;
import www.zapluk.com.R;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;
import static com.athomediva.mvpcontract.AHDMainViewContract.PARAM_NAVIGATION_TAG;

/**
 * Created by mohitkumar on 04/04/17.
 */

public class AHDMainPresenter extends BasePresenter<AHDMainViewContract.View>
  implements AHDMainViewContract.Presenter, DeeplinkHandler.DeeplinkCallbacks {
    private static final String TAG = LogUtils.makeLogTag(AHDMainPresenter.class.getSimpleName());
    private final int LOGIN_REQ_CODE_FOR_BOOKING = 1001;
    private final int LOGIN_REQ_CODE_FOR_ACCOUNT = 1002;
    private final UserSession mUserSession;
    private Bundle mBundle;
    private QuikrNetworkRequest mGetUserDetailsReq;
    private long mBackBtnPressTimeStamp = 0;
    private String mNavigationTag;
    private DeeplinkHandler mDeeplinkHandler;

    @Inject
    public AHDMainPresenter(@NonNull WeakReference<Activity> activityWeakReference, @NonNull DataManager dataManager,
      @NonNull Context context, @NonNull BookingSession bookingSession, @NonNull UserSession userSession, Bundle bundle,
      Bundle saveInstance) {
        super(dataManager, context, bookingSession);
        mUserSession = userSession;
        mBundle = bundle;
        // Replace the bundle with saveInstance
        if (saveInstance != null) {
            mBundle = saveInstance;
        }
        mDeeplinkHandler =
          new DeeplinkHandler(activityWeakReference, mDataManager, mBookingSession, bundle, userSession, this);
    }

    @Override
    public void attachView(@NonNull AHDMainViewContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        mDataManager.getBusManager().register(this);
        handleBundle(mBundle);
        mDeeplinkHandler.handleDeeplinkBundle(mBundle);
        if (mDataManager.getPrefManager().isUserLoggedIn()) {
            getCompleteUserDetails();
        }
    }

    private void handleBundle(Bundle bundle) {
        LOGD(TAG, "handleBundle: ");
        if (bundle != null) {
            String tag = bundle.getString(PARAM_NAVIGATION_TAG, mContext.getString(R.string.home));
            if (mContext.getString(R.string.home).equalsIgnoreCase(tag)) {
                getMvpView().showHomePage(bundle.getBoolean(AHDMainViewContract.PARAM_RELOAD_HOME_PAG, false));
            } else {
                onNavigationItemClicked(tag);
            }
        }
    }

    @Override
    public void onNavigationItemClicked(String title) {
        LOGD(TAG, "onNavigationItemClicked: title =" + title);
        mNavigationTag = title;
        if (TextUtils.isEmpty(title)) {
            LOGE(TAG, "onNavigationItemClicked: title is empty");
            return;
        }
        if (title.equalsIgnoreCase(mContext.getString(R.string.home))) {
            getMvpView().showHomePage(false);
        } else if (title.equalsIgnoreCase(mContext.getString(R.string.booking))) {
            if (!DataValidator.isUserSessionExist(mUserSession)) {
                LOGD(TAG, "onNavigationItemClicked: user not logged in");
                launchLoginPage(LOGIN_REQ_CODE_FOR_BOOKING);
            } else {
                getMvpView().showBookingsPage();
            }
        } else if (title.equalsIgnoreCase(mContext.getString(R.string.my_account))) {
            if (!DataValidator.isUserSessionExist(mUserSession)) {
                LOGD(TAG, "onNavigationItemClicked: user not logged in");
                launchLoginPage(LOGIN_REQ_CODE_FOR_ACCOUNT);
            } else {
                getMvpView().showMyAccountsPage();
            }
        }
    }

    @Override
    public void onNewIntent(Bundle bundle) {
        LOGD(TAG, "onNewIntent: ");
        mDeeplinkHandler.handleDeeplinkBundle(bundle);
        // Handle the Intent Extras on NewIntent
        handleBundle(bundle);
    }

    @Override
    public void detachView() {
        super.detachView();
        mDeeplinkHandler.destroy();
        mDataManager.getBusManager().unregister(this);
        LOGD(TAG, "detachView: ");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOGD(TAG, "onActivityResult: ");

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LOGIN_REQ_CODE_FOR_BOOKING) {
                LOGD(TAG, "onActivityResult: booking result obtained");
                if (mUserSession.getUser() != null) {
                    LOGD(TAG, "onActivityResult: user found");
                    getMvpView().showBookingsPage();
                }
            } else if (requestCode == LOGIN_REQ_CODE_FOR_ACCOUNT) {
                LOGD(TAG, "onActivityResult: account result obtained");
                if (mUserSession.getUser() != null) {
                    LOGD(TAG, "onActivityResult: user found");
                    getMvpView().showMyAccountsPage();
                }
            }
        }
        mDeeplinkHandler.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void launchLoginPage(int requestCode) {
        LOGD(TAG, "launchLoginPage: ");
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppLauncherContract.PARAM_LAUNCHED_FOR_LOGIN, true);
        getMvpView().launchActivityForResult(bundle, AppLauncherActivity.class, requestCode);
    }

    @Subscribe
    public void onEventMainThread(UserLogoutEvent event) {
        LOGD(TAG, "onEvent ResetBookingEvent");
        onNavigationItemClicked(mContext.getString(R.string.home));
    }

    private void getCompleteUserDetails() {
        LOGD(TAG, "getCompleteUserDetails: ");
        if (mGetUserDetailsReq != null) {
            mGetUserDetailsReq.cancel();
        }

        mGetUserDetailsReq =
          mDataManager.getApiManager().getUserDetails(new QuikrNetworkRequest.Callback<APIFullUserDetailsResponse>() {
              @Override
              public void onSuccess(APIFullUserDetailsResponse response) {
                  LOGD(TAG, "onSuccess: ");
                  if (!isViewAttached()) return;
                  mUserSession.updateDataFromResponse(response);
                  if (!DataValidator.isSpecialDatesUpdated(mDataManager.getPrefManager().getUserDetails())) {
                      mBookingSession.setSpecialDatePopupEnabled(true);
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LOGD(TAG, "onError: ");
              }
          });

        mGetUserDetailsReq.execute();
    }

    @Subscribe
    public void onEventMainThread(CategoryLoadEvent event) {
        LOGD(TAG, "onEventMainThread : Category Load Event");
        // Get the deeplink datas on CategoryLoad if category data is not available
        if (isViewAttached()) {
            mDeeplinkHandler.onEventMainThread(event);
        }
    }

    @Override
    public void onBackPressed() {

        if (System.currentTimeMillis() - mBackBtnPressTimeStamp > AppConstants.BACK_PRESS_DURATION) {
            LogUtils.LOGD(TAG, "onBackPressed : First Time Press");
            ToastSingleton.getInstance().showToast(mContext.getString(R.string.back_button_exit));
            mBackBtnPressTimeStamp = System.currentTimeMillis();
            return;
        }

        LogUtils.LOGD(TAG, "onBackPressed : Finish");
        mDataManager.getAnalyticsManager()
          .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, "", GATrackerContext.Label.SESSION_END);

        getMvpView().finish();
    }

    @Override
    public void onSaveInstance(Bundle bundle) {
        LogUtils.LOGD(TAG, "onSaveInstance : ");
        bundle.putString(PARAM_NAVIGATION_TAG, mNavigationTag);
    }

    @Override
    public void onSuccess(DeeplinkConstants.DEEPLINK_TYPE type, Uri uri) {
        LogUtils.LOGD(TAG, "onSuccess : ");
        if (!isViewAttached()) return;
        if (type == DeeplinkConstants.DEEPLINK_TYPE.HOME_PAGE) {
            LogUtils.LOGD(TAG, "onSuccess : Deepling HomePage trigger ");
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.MY_ORDERS) {
            LogUtils.LOGD(TAG, "onSuccess : Deepling MyOrders trigger ");
            mNavigationTag = mContext.getString(R.string.booking);
            onNavigationItemClicked(mContext.getString(R.string.booking));
            LogUtils.LOGD(TAG, "onSuccess : Is View Attach");
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.MY_ACCOUNTS) {
            LogUtils.LOGD(TAG, "onSuccess : Deepling MyAccounts trigger ");
            mNavigationTag = mContext.getString(R.string.my_account);
            onNavigationItemClicked(mContext.getString(R.string.my_account));
            LogUtils.LOGD(TAG, "onSuccess : Is View Attach");
        } else if (type == DeeplinkConstants.DEEPLINK_TYPE.AVAILABILITY_CONFIRM) {
            if (getMvpView() != null) getMvpView().showAvailabilityConfirmDialog();
        }
    }
}
