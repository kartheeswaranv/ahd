package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import com.athomediva.data.AppConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.Geolocation;
import com.athomediva.data.models.remote.APIListCategoriesResponse;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.data.sharedpref.PreferenceManager;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.injection.ApplicationContext;
import com.athomediva.mvpcontract.AppLauncherContract;
import com.athomediva.mvpcontract.LoginPageContract;
import com.athomediva.ui.AHDMainActivity;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import javax.inject.Inject;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 17/04/17.
 * Functionality:
 * 1. Can be launched directly with app launch or when user has to login
 * 2. Get the location and categories information from api and then navigate to home page
 * 3. If user has not clicked on skip then stay in same page.
 * 4. Check for all the permissions to get the location.
 * 5. On any error scenario navigate to home page if user has not clicked on skip
 * //TODO network handle error scenario
 */

public class AppLauncherPresenter extends BaseGPSPresenter<AppLauncherContract.View>
  implements AppLauncherContract.Presenter {
    private static final String TAG = AppLauncherPresenter.class.getSimpleName();
    private static final int CLOSE_SPLASH = 1;
    private static final int LAUNCH_LOGIN = 2;
    private static final int COUNT_DOWN_TIME = 3;
    private static final long ONE_SECOND = 1000;
    private QuikrNetworkRequest mCategoriesRequest;
    private boolean isLaunchedForLoginResult;
    private long mCountDownTimeInMillSeconds = 0;
    private final Handler handler = new Handler(msg -> {
        LOGD(TAG, ": handler message obtained");
        if (!isViewAttached()) return false;
        if (msg.what == CLOSE_SPLASH) {
            getMvpView().launchActivity(null, AHDMainActivity.class);
            getMvpView().finish();
        } else if (msg.what == LAUNCH_LOGIN) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(LoginPageContract.PARAM_FROM_LAUNCHER, !isLaunchedForLoginResult);
            getMvpView().showLoginPageView(bundle);
        } else if (msg.what == COUNT_DOWN_TIME) {
            mCountDownTimeInMillSeconds += ONE_SECOND;
            LOGD(TAG, ": time spent = " + System.currentTimeMillis());
            sendCountDownMessage();
        }
        return false;
    });

    @Inject
    public AppLauncherPresenter(@NonNull DataManager dataManager, @NonNull @ApplicationContext Context context,
      @NonNull PermissionManager permissionHelper, Bundle bundle, BookingSession bookingSession) {
        super(dataManager, context, bookingSession, permissionHelper);
        if (bundle != null) {
            isLaunchedForLoginResult = bundle.getBoolean(AppLauncherContract.PARAM_LAUNCHED_FOR_LOGIN, false);
        }
        mBookingSession.setSpecialDatePopupEnabled(true);
    }

    private void sendCountDownMessage() {
        LOGD(TAG, "sendCountDownMessage: ");
        handler.removeMessages(COUNT_DOWN_TIME);
        handler.sendEmptyMessageDelayed(COUNT_DOWN_TIME, ONE_SECOND);
    }

    @DebugLog
    @Override
    public void attachView(@NonNull AppLauncherContract.View mvpView) {
        super.attachView(mvpView);
        LOGD(TAG, "attachView: ");
        //This page will be opened for user login page only when user has to login
        if (isLaunchedForLoginResult) {
            LOGD(TAG, "attachView: launched for login result");
            handler.sendEmptyMessage(LAUNCH_LOGIN);
        } else {
            mDataManager.getAnalyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, "", GATrackerContext.Label.SESSION_INIT);
            //Normal app launch flow
            //if user has skipped or logged in do not open launcher page
            sendCountDownMessage();
            if (shouldShowLoginPage()) {
                LOGD(TAG, "attachView: user not logged in or skipped so add user login view");
                handler.sendEmptyMessageDelayed(LAUNCH_LOGIN, AppConstants.LOGIN_SPLASH_LAUNCH_DELAY_TIME);
            }
            //get the location by asking generic permissions
            getLocationFromGPS(false);
        }
    }

    @Override
    public void detachView() {
        LOGD(TAG, "destroy: ");
        //cleanups
        if (mCategoriesRequest != null) {
            mCategoriesRequest.cancel();
        }

        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        super.detachView();
    }

    @Override
    protected void onCurrentLocationObtained(Geolocation geolocation) {
        LOGD(TAG, "onCurrentLocationObtained: ");
        mBookingSession.resetCachedResponse();
        mBookingSession.setCurrentGeolocation(geolocation);
        //got the location so now lets call home page api here itself
        getCategories();
    }

    @Override
    protected void onLocationFailed() {
        LOGD(TAG, "onLocationFailed: ");
        //if user has not skipped login finish the page
        if (!shouldShowLoginPage()) {
            closeLauncherView();
        }
    }

    @Override
    protected void onGPSLocationInProgress() {
        LOGD(TAG, "onGPSLocationInProgress: ");
    }

    @Override
    public void onReadyForPlayback() {
        LOGD(TAG, "onReadyForPlayback: ");
        try {
            getMvpView().startVideoPlayback(AppConstants.SPLASH_VIDEO_FILE_NAME);
        } catch (Exception e) {
            LOGE(TAG, "attachView:" + e);
            handler.sendEmptyMessage(CLOSE_SPLASH);
        }
    }

    @Override
    public void onBackPressed() {
        LOGD(TAG, "onBackPressed: ");
        if (!isLaunchedForLoginResult) {
            mDataManager.getAnalyticsManager()
              .trackEventGA(GATrackerContext.Category.ATHOMEDIVA, "", GATrackerContext.Label.SESSION_END);
        }
    }

    /**
     * Ge the categories api response
     */
    private void getCategories() {
        LOGD(TAG, "getCategories: ");
        if (mCategoriesRequest != null) {
            mCategoriesRequest.cancel();
        }

        mCategoriesRequest = mDataManager.getApiManager()
          .getAllCategories(mBookingSession.getCurrentGeolocation().getLocation().latitude,
            mBookingSession.getCurrentGeolocation().getLocation().longitude, false,
            new QuikrNetworkRequest.Callback<APIListCategoriesResponse>() {
                @Override
                public void onSuccess(APIListCategoriesResponse response) {
                    LOGD(TAG, "onSuccess: ");
                    if (!isViewAttached()) return;

                    if (response.isSuccess() && response.getData() != null) {
                        //Pre store the data in session
                        mBookingSession.updateLocationWithCategoriesLatLng(response.getData().getMeta());
                        if (response.getData().getMeta() == null) {
                            mDataManager.getAnalyticsManager()
                              .trackGAEventForAPIFail(AppUrls.URL_GET_CATEGORIES, GATrackerContext.Label.META_DATA_IS_EMPTY);
                        }
                        mBookingSession.setCategoryData(response);
                        if (!shouldShowLoginPage()) {
                            LOGD(TAG, "attachView: user logged in or user has skipped");
                            closeLauncherView();
                        }
                    } else {
                        closeLauncherView();
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    LOGE(TAG, "onError: ");
                    onNetworkError(errorCode, errorMessage, new WeakReference<>(mCategoriesRequest));
                    if (!isViewAttached()) return;
                    closeLauncherView();
                }
            });

        mCategoriesRequest.execute();
    }

    /**
     * will be called in case of any error scenario.We should navigate to home s
     */
    private void closeLauncherView() {
        LOGD(TAG, "closeLauncherView: spent time  =" + mCountDownTimeInMillSeconds);
        handler.removeMessages(CLOSE_SPLASH);
        handler.sendEmptyMessageDelayed(CLOSE_SPLASH, mCountDownTimeInMillSeconds > AppConstants.SPLASH_CLOSE_TIME ? 0
          : AppConstants.SPLASH_CLOSE_TIME - mCountDownTimeInMillSeconds);
    }

    private boolean shouldShowLoginPage() {
        boolean shouldShow = !(mDataManager.getPrefManager().isUserLoggedIn() || mDataManager.getPrefManager()
          .getBooleanValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_LOGIN));
        LOGD(TAG, "shouldShowLoginPage: shouldShow = " + shouldShow);
        return !(mDataManager.getPrefManager().isUserLoggedIn() || mDataManager.getPrefManager()
          .getBooleanValue(PreferenceManager.SESSION.KEY_USER_SKIPPED_LOGIN));
    }
}
