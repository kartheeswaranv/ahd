package com.athomediva.presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.athomediva.data.AppConstants;
import com.athomediva.data.DeeplinkConstants;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.managers.GATrackerContext;
import com.athomediva.data.models.local.OptionModel;
import com.athomediva.data.models.local.Permission;
import com.athomediva.data.models.local.StatusModel;
import com.athomediva.data.models.remote.APIGenericResponse;
import com.athomediva.data.models.remote.APIOldResponse;
import com.athomediva.data.models.remote.BookingDetails;
import com.athomediva.data.network.AppUrls;
import com.athomediva.data.session.BookingSession;
import com.athomediva.helper.ToastSingleton;
import com.athomediva.helper.permission.PermissionManager;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.BookingDetailsContract;
import com.athomediva.mvpcontract.FeedbackActContract;
import com.athomediva.ui.FragmentContainerActivity;
import com.athomediva.ui.bookingpage.BookingCancelFragment;
import com.athomediva.ui.feedback.FeedbackActivity;
import com.athomediva.ui.feedback.FeedbackFragment;
import com.athomediva.ui.webview.WebViewFragment;
import com.athomediva.utils.UiUtils;
import com.quikrservices.android.network.QuikrNetworkRequest;
import hugo.weaving.DebugLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import www.zapluk.com.R;

import static android.app.Activity.RESULT_OK;
import static com.athomediva.mvpcontract.BookingDetailsContract.PARAM_BOOKING_ID;
import static com.athomediva.ui.FragmentContainerActivity.PARAM_FRAG_TAG_NAME;
import static com.athomediva.ui.feedback.FeedbackActivity.FRAG_TAG_NAME;

/**
 * Created by kartheeswaran on 18/05/17.
 */

public class BookingDetailsPresenter extends BasePresenter<BookingDetailsContract.View>
  implements BookingDetailsContract.Presenter {
    private static final String TAG = LogUtils.makeLogTag(BookingDetailsPresenter.class.getSimpleName());

    private final int REQUEST_CODE_CALL = 1001;
    private final int OPTIONS_REQUEST_CODE = 1002;
    private final int PAYMENT_REQUEST_CODE = 1003;
    private final int FEEDBACK_REQUEST_CODE = 1004;

    private PermissionManager mPermissionManager;
    private QuikrNetworkRequest mGetBookingDetReq;
    private QuikrNetworkRequest mCancelBookingReq;
    private QuikrNetworkRequest mMessagePostReq;
    private String mReqMessage;
    private String mBookingId;
    private Bundle mBundle;
    private BookingDetails.BookingDetailsData mBookingData;
    private int mMaxProgressStep;

    public BookingDetailsPresenter(@NonNull DataManager dataManager, @NonNull BookingSession session,
      @NonNull Context context, @NonNull PermissionManager permissionManager, @NonNull Bundle bundle) {
        super(dataManager, context, session);
        mBundle = bundle;
        mPermissionManager = permissionManager;
        if (mBookingSession.getMetaData() != null && mBookingSession.getMetaData().getData() != null) {
            mMaxProgressStep = mBookingSession.getMetaData().getData().getProgressSteps();
        }

        LogUtils.LOGD(TAG, "BookingDetailsPresenter : ");
    }

    @Override
    public void attachView(@NonNull BookingDetailsContract.View mvpView) {
        super.attachView(mvpView);
        LogUtils.LOGD(TAG, "attachView : ");

        if (mBundle != null) {
            if (mBundle.containsKey(DeeplinkConstants.KEY_ACTION) && TextUtils.equals(
              mBundle.getString(DeeplinkConstants.KEY_ACTION), Intent.ACTION_VIEW)) {
                Uri uri = mBundle.getParcelable(DeeplinkConstants.KEY_DEEPLINK_URL);
                if (uri != null) {
                    mBookingId = uri.getQueryParameter(DeeplinkConstants.DEEPLINK_PARAMS.ORDER_ID);
                }
            } else if (mBundle.containsKey(PARAM_BOOKING_ID) && !TextUtils.isEmpty(mBundle.getString(PARAM_BOOKING_ID))) {
                LogUtils.LOGD(TAG, "attachView : Booking Id is not empty");
                mBookingId = mBundle.getString(PARAM_BOOKING_ID);
            } else {
                LogUtils.LOGD(TAG, "attachView : Booking Id is missing");
                getMvpView().finish();
            }
        }
        if (TextUtils.isEmpty(mBookingId)) {
            getMvpView().finish();
        } else {

            fetchBookingDetails(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onSpecificReqSendClick(String msg) {
        LogUtils.LOGD(TAG, "onSpecificReqSendClick : ");
        mReqMessage = msg;
        postOrderMessage(mReqMessage);
    }

    @Override
    public void onAddressChangeClick() {
        LogUtils.LOGD(TAG, "onAddressChangeClick : ");
    }

    @Override
    public void onCancelClick() {
        LogUtils.LOGD(TAG, "onCancelClick : ");
        if (mBookingData != null && !TextUtils.isEmpty(mBookingData.getBucketName())) {
            String title = mContext.getString(R.string.booking_cancel_dialog_title);
            String msg = mContext.getString(R.string.booking_cancel_msg, mBookingData.getBucketName());

            if (!TextUtils.isEmpty(mBookingData.getCancelPenaltyMsg())) {
                msg = mBookingData.getCancelPenaltyMsg();
            }
            getMvpView().showCancelDialog(title, msg);
        }
    }

    @Override
    public void onCustomerCareClick() {
        LogUtils.LOGD(TAG, "onCustomerCareClick : ");
        ArrayList<Permission> list = new ArrayList<>();
        list.add(new Permission(Manifest.permission.CALL_PHONE));
        mPermissionManager.checkAndRequestForPermissions(list, (isAllAreGranted, grantedPermission, deniedPermission) -> {
            if (isAllAreGranted) {
                LogUtils.LOGD(TAG, "onCustomerCareClick : Permission Granted");
                if (mBookingSession != null
                  && mBookingSession.getMetaData() != null
                  && mBookingSession.getMetaData().getData() != null
                  && !TextUtils.isEmpty(mBookingSession.getMetaData().getData().getCustomerCareNo())) {
                    callCustomerCare(mBookingSession.getMetaData().getData().getCustomerCareNo());
                } else {
                    LogUtils.LOGD(TAG, "onCustomerCareClick : MetaData has no Customer Care number ");
                }
            } else {
                LogUtils.LOGD(TAG, "onCustomerCareClick : Permission Denied");
            }
        });
    }

    private void callCustomerCare(String mobileno) {
        LogUtils.LOGD(TAG, "callCustomerCare : ");

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + mobileno));
        getMvpView().launchActivityForResult(intent, REQUEST_CODE_CALL);
    }

    @Override
    public void onRateServiceClick() {
        LogUtils.LOGD(TAG, "onRateServiceClick : ");
        Bundle b = new Bundle();
        b.putString(FRAG_TAG_NAME, FeedbackFragment.FRAG_TAG);
        b.putString(FeedbackActContract.PARAM_SUBTITLE, mBookingData.getRateServiceTitle());
        b.putString(FeedbackActContract.PARAM_BOOKING_ID, mBookingData.getBookingId());
        getMvpView().launchActivityForResult(b, FeedbackActivity.class, FEEDBACK_REQUEST_CODE);
    }

    @Override
    public void onPayonlineClick() {
        LogUtils.LOGD(TAG, "onPayonlineClick : ");
        if (mBookingData != null && !TextUtils.isEmpty(mBookingData.getPaymentLink())) {
            Bundle bundle = new Bundle();
            bundle.putString(WebViewFragment.PARAM_TITLE, mContext.getString(R.string.pay_online_cta));
            bundle.putString(WebViewFragment.PARAM_URL, mBookingData.getPaymentLink());
            bundle.putString(WebViewFragment.PARAM_INTERCEPT_URL, mBookingData.getVerifyPaymentUrl());
            bundle.putLong(WebViewFragment.PARAM_REDIRECTION_TIME, AppConstants.PAYMENT_REDIRECT_DURATION);
            bundle.putString(WebViewFragment.PARAM_REDIRECTION_MSG,
              mContext.getString(R.string.payment_success_redirect_msg));
            bundle.putString(FragmentContainerActivity.PARAM_FRAG_TAG_NAME, WebViewFragment.TAG);
            getMvpView().launchActivityForResult(bundle, FragmentContainerActivity.class, PAYMENT_REQUEST_CODE);
        }
    }

    @Override
    public void onCancelConfirmClick() {
        LogUtils.LOGD(TAG, "onCancelConfirmClick : ");
        /*Bundle b = new Bundle();
        b.putString(PARAM_FRAG_TAG_NAME, BookingCancelFragment.FRAG_TAG);
        getMvpView().launchActivityForResult(b, FragmentContainerActivity.class, OPTIONS_REQUEST_CODE);*/
        cancelOrder();
    }

    private void handleBookingDetailsRepsonse(BookingDetails.BookingDetailsData details) {
        LogUtils.LOGD(TAG, "handleBookingDetailsRepsonse : ");
        if (details != null) {
            mBookingData = details;
            getMvpView().showContainerView();
            getMvpView().updateSubTitleView(details.getBucketName());
            if (details.getServiceGroupList() != null && !details.getServiceGroupList().isEmpty()) {
                getMvpView().updateServicesView(
                  mDataManager.getDataProcessor().convertToCartService(details.getServiceGroupList()));
            }
            if (details.getPaymentInfoList() != null && !details.getPaymentInfoList().isEmpty()) {
                getMvpView().updatePaymentInfo(details.getPaymentInfoList(), details.getPaymentType());
            }
            getMvpView().updateStatusView(createStatusModel(details));
            getMvpView().updateAddressView(details.getAddress());
            getMvpView().updateSpecificReqView(details.getSpecReqMsg(), details.isSpecialReqEnable());
            getMvpView().showCTA(details.isCancelEnable(), details.isCallEnable());
            getMvpView().updateHeaderView(details.getOrderDate(), details.getDuration(), details.getOrderIdTxt(),
              details.getAuthCode());
        }
    }

    @DebugLog
    private void cancelOrder(OptionModel model) {
        LogUtils.LOGD(TAG, "cancelOrder : " + model);

        if (!isViewAttached() || model == null) return;

        if (mCancelBookingReq != null) {
            mCancelBookingReq.cancel();
        }

        getMvpView().showProgressBar("");

        String msg;
        if (TextUtils.isEmpty(model.getText())) {
            msg = model.getTitle();
        } else {
            msg = model.getText();
        }
        LogUtils.LOGD(TAG, "cancelOrder : Cancel Reason " + msg);
        mCancelBookingReq = mDataManager.getApiManager()
          .cancelBooking(mBookingId, msg, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
              @Override
              public void onSuccess(APIGenericResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.isSuccess() && response.getData() != null) {
                      mDataManager.getAnalyticsManager()
                        .trackEventGA(GATrackerContext.Category.BOOKINGS, GATrackerContext.Action.CANCEL_BOOKING_ID,
                          mBookingId);
                      getMvpView().showCancelStatusDialog(response.getData().getMessage());
                  } else if (response != null && response.getError() != null) {
                      if (response.getError().getCode()
                        == AppConstants.APICodes.ERROR_CODE.CANCEL_BOOKING_FAILURE.getErrorCode()) {
                          getMvpView().showCancelStatusDialog(response.getError().getMessage());
                      } else {
                          ToastSingleton.getInstance().showToast(response.getError().getMessage());
                      }
                      mDataManager.getAnalyticsManager()
                        .trackGAEventForAPIFail(AppUrls.URL_CANCEL_BOOKING, response.getError());
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mCancelBookingReq));
              }
          });

        mCancelBookingReq.execute();
    }

    @DebugLog
    private void cancelOrder() {
        LogUtils.LOGD(TAG, "cancelOrder : ");

        if (!isViewAttached()) return;

        if (mCancelBookingReq != null) {
            mCancelBookingReq.cancel();
        }

        getMvpView().showProgressBar("");

        //LogUtils.LOGD(TAG, "cancelOrder : Cancel Reason " + msg);
        mCancelBookingReq = mDataManager.getApiManager()
          .cancelBooking(mBookingId, null, new QuikrNetworkRequest.Callback<APIGenericResponse>() {
              @Override
              public void onSuccess(APIGenericResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.isSuccess() && response.getData() != null) {
                      mDataManager.getAnalyticsManager()
                        .trackEventGA(GATrackerContext.Category.BOOKINGS, GATrackerContext.Action.CANCEL_BOOKING_ID,
                          mBookingId);
                      getMvpView().showCancelStatusDialog(response.getData().getMessage());
                  } else if (response != null && response.getError() != null) {
                      if (response.getError().getCode()
                        == AppConstants.APICodes.ERROR_CODE.CANCEL_BOOKING_FAILURE.getErrorCode()) {
                          getMvpView().showCancelStatusDialog(response.getError().getMessage());
                      } else {
                          ToastSingleton.getInstance().showToast(response.getError().getMessage());
                      }
                      mDataManager.getAnalyticsManager()
                        .trackGAEventForAPIFail(AppUrls.URL_CANCEL_BOOKING, response.getError());
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mCancelBookingReq));
              }
          });

        mCancelBookingReq.execute();
    }

    private void postOrderMessage(String message) {
        LogUtils.LOGD(TAG, "postOrderMessage :");
        if (!isViewAttached() || TextUtils.isEmpty(message)) return;

        if (mMessagePostReq != null) {
            mMessagePostReq.cancel();
        }

        getMvpView().showProgressBar("");
        mMessagePostReq = mDataManager.getApiManager()
          .updateBookingMessage(mBookingId, message, new QuikrNetworkRequest.Callback<APIOldResponse>() {
              @Override
              public void onSuccess(APIOldResponse response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  getMvpView().hideProgressBar();
                  if (response != null && response.getStatus() == 1) {
                      getMvpView().updateSpecificReqView(message, true);
                  } else if (response != null && response.getMessage() != null) {
                      mDataManager.getAnalyticsManager()
                        .trackGAEventForAPIFail(AppUrls.URL_UPDATE_BOOKING_MSG, response.getMessage());
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");
                  if (!isViewAttached()) return;
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mMessagePostReq));
              }
          });

        mMessagePostReq.execute();
    }

    private void fetchBookingDetails(boolean pullToRefresh) {
        LogUtils.LOGD(TAG, "fetchBookingDetails : ");
        if (!isViewAttached() || TextUtils.isEmpty(mBookingId)) return;

        if (mGetBookingDetReq != null) {
            mGetBookingDetReq.cancel();
        }

        if (!pullToRefresh) {
            getMvpView().showProgressBar("");
        }

        mGetBookingDetReq =
          mDataManager.getApiManager().getBookingDetails(mBookingId, new QuikrNetworkRequest.Callback<BookingDetails>() {
              @Override
              public void onSuccess(BookingDetails response) {
                  LogUtils.LOGD(TAG, "onSuccess : ");
                  if (!isViewAttached()) return;
                  hideProgressBar(pullToRefresh);
                  if (response != null) {
                      if (response.isSuccess() && response.getData() != null) {
                          handleBookingDetailsRepsonse(response.getData());
                      } else if (response.getError() != null) {
                          mDataManager.getAnalyticsManager()
                            .trackGAEventForAPIFail(AppUrls.URL_GET_BOOKING_DETAILS, response.getError());
                          ToastSingleton.getInstance().showToast(response.getError().getMessage());
                      }
                  }
              }

              @Override
              public void onError(int errorCode, String errorMessage) {
                  LogUtils.LOGD(TAG, "onError : ");

                  if (!isViewAttached()) return;
                  hideProgressBar(pullToRefresh);
                  onNetworkError(errorCode, errorMessage, new WeakReference<>(mGetBookingDetReq), true);
              }
          });
        mGetBookingDetReq.execute();
    }

    @Override
    public void onRefreshClick() {
        LogUtils.LOGD(TAG, "onRefreshClick : ");
        fetchBookingDetails(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == OPTIONS_REQUEST_CODE) {
                LogUtils.LOGD(TAG, "onActivityResult : " + OPTIONS_REQUEST_CODE);
                OptionModel model = data.getExtras().getParcelable(BookingDetailsContract.PARAM_SELECTED_OPTION);
                cancelOrder(model);
            } else if (requestCode == PAYMENT_REQUEST_CODE) {
                LogUtils.LOGD(TAG, "onActivityResult : Payment Done ");
                fetchBookingDetails(false);
            } else if (requestCode == FEEDBACK_REQUEST_CODE) {
                fetchBookingDetails(false);
            }
        }
    }

    private StatusModel createStatusModel(BookingDetails.BookingDetailsData details) {
        LogUtils.LOGD(TAG, "createStatusModel : ");
        if (details != null) {
            String titleTxt = details.getUnverifyMsg();
            String descTxt = details.getOrderDisplayStatus();
            if (TextUtils.isEmpty(titleTxt)) {
                titleTxt = descTxt;
                descTxt = null;
            }
            StatusModel model = UiUtils.generateStatusModel(mContext, details.getProgressSteps(), titleTxt, mMaxProgressStep,
              details.getStatusColor());
            model.setRated(details.isRated());
            model.setTitleText(descTxt);
            // Enable the call Button if it is unverify booking
            model.setCallEnable(!TextUtils.isEmpty(details.getUnverifyMsg()));
            model.setPayOnlineEnable(!TextUtils.isEmpty(details.getPaymentLink()));
            model.setRating(details.getRating());

            return model;
        }
        return null;
    }

    private void hideProgressBar(boolean pullToRefresh) {
        LogUtils.LOGD(TAG, "hideProgressBar : ");
        if (pullToRefresh) {
            getMvpView().hideSwipeRefreshView();
        } else {
            getMvpView().hideProgressBar();
        }
    }
}
