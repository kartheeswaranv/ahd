package com.athomediva.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import com.athomediva.data.managers.DataManager;
import com.athomediva.data.models.local.Consumer;
import com.athomediva.logger.LogUtils;
import com.athomediva.mvpcontract.MVPView;
import com.athomediva.mvpcontract.OTPVerificationContract;
import com.athomediva.ui.verification.OTPVerificationActivity;
import hugo.weaving.DebugLog;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.athomediva.logger.LogUtils.LOGD;
import static com.athomediva.logger.LogUtils.LOGE;

/**
 * Created by mohitkumar on 15/10/16.
 */

public abstract class AbstractLoginPresenter<T extends MVPView> extends BasePresenter<T> {
    private final String TAG = LogUtils.makeLogTag(AbstractLoginPresenter.class.getSimpleName());
    private Subscription mGetConsumerInfoSubscription;
    private Single<Consumer> mConsumerInfoObservable;
    private SingleSubscriber<Consumer> mGetConsumerInfoSubscriber;

    AbstractLoginPresenter(@NonNull DataManager dataManager, @NonNull Context context) {
        super(dataManager, context);
    }

    protected abstract void loginRequest(final Consumer consumer);

    @DebugLog
    void addDetailsToConsumerInfo(final Consumer consumer) {
        LOGD(TAG, "addDetailsToConsumerInfo: ");
        if (mGetConsumerInfoSubscription != null && !mGetConsumerInfoSubscription.isUnsubscribed()) {
            mGetConsumerInfoSubscription.unsubscribe();
        }

        if (mGetConsumerInfoSubscriber != null && !mGetConsumerInfoSubscriber.isUnsubscribed()) {
            mGetConsumerInfoSubscriber.unsubscribe();
        }

        mConsumerInfoObservable = mDataManager.getDataProcessor().getConsumerInfoObservable(consumer);

        mGetConsumerInfoSubscriber = new SingleSubscriber<Consumer>() {
            @Override
            public void onSuccess(Consumer consumer) {
                LOGD(TAG, "mConsumerInfoObservable success current thread =" + Thread.currentThread().getName());
                loginRequest(consumer);
            }

            @Override
            public void onError(Throwable error) {
                LOGE(TAG, "onError: GCM not registered******");
                loginRequest(consumer);
            }
        };
        mGetConsumerInfoSubscription = mConsumerInfoObservable.observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.io())
          .subscribe(mGetConsumerInfoSubscriber);
    }

    void proceedToOtpVerification(Consumer consumer, int reqCode) {
        LOGD(TAG, "proceedToOtpVerification: ");
        if (!isViewAttached()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(OTPVerificationContract.PARAM_CONSUMER, consumer);
        getMvpView().launchActivityForResult(bundle, OTPVerificationActivity.class, reqCode);
    }

    @DebugLog
    @CallSuper
    @Override
    public void detachView() {
        if (mGetConsumerInfoSubscription != null && !mGetConsumerInfoSubscription.isUnsubscribed()) {
            mGetConsumerInfoSubscription.unsubscribe();
        }

        if (mGetConsumerInfoSubscriber != null && !mGetConsumerInfoSubscriber.isUnsubscribed()) {
            mGetConsumerInfoSubscriber.unsubscribe();
        }
        super.detachView();
    }
}
