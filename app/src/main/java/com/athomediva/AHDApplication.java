package com.athomediva;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.athomediva.data.AppConstants;
import com.athomediva.data.network.AHDAPIGATracker;
import com.athomediva.data.network.NetworkContext;
import com.athomediva.injection.component.ApplicationComponent;
import com.athomediva.injection.component.DaggerApplicationComponent;
import com.athomediva.injection.module.ApplicationModule;
import com.google.firebase.messaging.FirebaseMessaging;
import com.newrelic.agent.android.NewRelic;
import com.quikrservices.android.network.QuikrServicesNetwork;
import www.zapluk.com.BuildConfig;

/**
 * Created by mohitkumar on 07/09/16.
 */
public class AHDApplication extends MultiDexApplication {

    public static Context sContext;
    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        //if (LeakCanary.isInAnalyzerProcess(this)) {
        //    // This process is dedicated to LeakCanary for heap analysis.
        //    // You should not init your app in this process.
        //    return;
        //}
        //LeakCanary.install(this);
        setApplicationModule(new ApplicationModule(this));

        if (!BuildConfig.DEBUG) NewRelic.withApplicationToken(BuildConfig.NEW_RELIC_TOKEN).start(this);
        QuikrServicesNetwork.initialize(
          new NetworkContext(getApplicationContext(), getComponent().preferencesManager(), getComponent().userSession(),
            new AHDAPIGATracker(getComponent().analyticsManager())));
        getComponent().getSession().resetSession();
        FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.FCM_TOPIC_ALL_DEVICES);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    public void setApplicationModule(ApplicationModule module) {
        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(module).build();
    }

    public static AHDApplication get(Context context) {
        return (AHDApplication) context.getApplicationContext();
    }
}
