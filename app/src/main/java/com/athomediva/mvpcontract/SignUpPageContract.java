package com.athomediva.mvpcontract;

import android.support.v4.app.ActivityCompat;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.ui.home.SpecialDatesFragment;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface SignUpPageContract {
    String PARAM_CONSUMER = "param_consumer";

    interface Presenter extends MVPPresenter<View>, ActivityCompat.OnRequestPermissionsResultCallback {
        void signUpClick(SignUpItem signUpItem);

        void termsOfServiceClick();

        void privacyPolicyClick();
    }

    interface View extends MVPView {
        void updateView(SignUpItem signUpItem);

        void initiateSpecialOfferView(ArrayList<SpecialOfferDateModel> list,
          SpecialDatesFragment.SpecialDatesChangeListener listener);
    }
}