package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.SocialSharingItem;
import com.athomediva.data.models.local.TitleDescImageItem;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface CustomerServicesContract {

    interface Presenter extends MVPPresenter<CustomerServicesContract.View> {
        void onPhoneServicesClick();

        void onEmailServicesClick();

        void onSocialServiceClick(SocialSharingItem item);
    }

    interface View extends MVPView {
        void updateCallUsView(TitleDescImageItem callItem);

        void updateEmailView(TitleDescImageItem emailItem);

        void updateSocialNetworkingServices(String title, List<SocialSharingItem> itemList);
    }
}
