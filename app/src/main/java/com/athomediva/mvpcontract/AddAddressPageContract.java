package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.AddAddressItem;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface AddAddressPageContract {

    String PARAM_CIRCLE_NAME = "param_circle_name";
    String PARAM_ADDRESS_SELECT = "param_select_address";

    interface Presenter extends MVPPresenter<AddAddressPageContract.View> {
        void onGetCurrentLocationClick();

        void onAddressTypeClick(String item);

        void onSaveClick();
    }

    interface View extends MVPView {
        void updateViews(AddAddressItem item);

        void updateCustomViewVisibility(boolean show);
    }
}
