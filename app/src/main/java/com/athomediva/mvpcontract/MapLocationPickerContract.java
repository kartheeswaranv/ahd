package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.Geolocation;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface MapLocationPickerContract {
    String PARAM_SELECTED_GEOLOCATION = "param_selected_geolocation";
    String PARAM_BOUNDS = "param_bounds";

    interface Presenter extends MVPPresenter<MapLocationPickerContract.View> {
        void onGetCurrentLocationClick();

        void onCameraFocusedForLocation(LatLng latLng);

        void onSearchClicked();

        void onUseThisLocationClick(Geolocation geolocation);

        void onMapReady();
    }

    interface View extends MVPView {

        void updateSearchView(Geolocation geolocation);

        void updateErrorView(boolean show);

        void moveCameraToPosition(Geolocation latLng);

        void initializeBounds(LatLngBounds bounds);

        void updateCurrentLocation(boolean currentLocationEnabled);

        void drawPolygonBounds(List<LatLng> bounds);
    }
}
