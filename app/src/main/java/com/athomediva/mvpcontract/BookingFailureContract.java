package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.data.models.remote.BookingExtraInfo;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 30/06/17.
 */

public interface BookingFailureContract {
    String PARAM_RESPONSE = "param_failure_response";

    interface Presenter extends MVPPresenter<View> {
        void onBookingItemClick(ConfirmBookingInfo info);

        void onHomeClick();

        void onRescheduleClick(BookingDescInfo info);

        void onPayNowClick();
    }

    interface View extends MVPView {
        void updateTitle(String userName);

        void updateDesc(String desc, boolean isAllFailed);

        void updateBookingsList(ArrayList<ConfirmBookingInfo> bookingList);

        void updateBookingFailureInfo(ArrayList<BookingDescInfo> bookingFailedList);

        void updateBookingHeaderInfoList(ArrayList<BookingDescInfo> bookingDescInfos);

        void updateTotalAmountView(String amount);

        void updateBookingExtraInfoList(ArrayList<BookingExtraInfo> list);
    }
}
