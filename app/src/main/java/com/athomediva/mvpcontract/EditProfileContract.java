package com.athomediva.mvpcontract;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import com.athomediva.data.models.local.SignUpItem;
import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.ui.home.SpecialDatesFragment;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 06/06/17.
 */

public interface EditProfileContract {

    String PARAM_CONSUMER = "param_consumer";
    String KEY_CAMERA_FILE_PATH = "key_file_path";
    String KEY_PROFILE_UPDATE_FLAG = "key_profile_update";

    interface Presenter extends MVPPresenter<View>, ActivityCompat.OnRequestPermissionsResultCallback {
        void updateClick(SignUpItem signUpItem);

        void onEditImgClick(int option);

        void onActivityRecreate(Bundle b);

        void onSaveInstance(Bundle instance);
    }

    interface View extends MVPView {
        void updateView(SignUpItem signUpItem);

        void updateImageView(String url);

        void initSpecialOfferView(boolean titleEnable, ArrayList<SpecialOfferDateModel> list,
          SpecialDatesFragment.SpecialDatesChangeListener listener);
    }
}
