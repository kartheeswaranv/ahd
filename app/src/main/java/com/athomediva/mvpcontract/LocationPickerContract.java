package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.Predictions;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface LocationPickerContract {

    String PARAM_SELECTED_GEOLOCATION = "param_selected_geolocation";
    String PARAM_SHOW_USER_ADDRESS_LIST = "param_show_user_address";
    String PARAM_SHOW_CURRENT_LOCATION = "param_show_current_location";

    interface Presenter extends MVPPresenter<LocationPickerContract.View> {
        void onGetCurrentLocationClick();

        void onSearchQuery(String query);

        void onSearchItemSelected(Predictions address);

        void onUserAddressClick(Address address);
    }

    interface View extends MVPView {
        void updateAddressView(List<Address> itemList);

        void updateSavedAddressVisibility(boolean show);

        void updateCurrentLocationVisibility(boolean show);

        void updateAutoCompleteTextViewResult(List<Predictions> addresses);

        void updateSuggestedAddressVisibility(boolean show);

        void updateEmptyPageVisibility(EmptySectionItem item, boolean show);

        void openKeyBoard();
    }
}
