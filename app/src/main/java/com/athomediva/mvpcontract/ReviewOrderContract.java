package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.Coupon;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.WalletDetails;
import java.util.List;

/**
 * Created by kartheeswaran on 04/05/17.
 */

public interface ReviewOrderContract {

    String PARAM_CART_RESPONSE = "param_cart_response";

    interface Presenter extends MVPPresenter<View> {
        void onCartEditClick();

        void onCouponApplyClick(String couponCode);

        void onCouponClearClick();

        void onWalletInfoClick();

        void onAddressChangeClick();

        void onProceedClick();

        void onRecheduleConfirm();

        void onRescheduleCancel(boolean clearCart);

        void onInstantCouponClick(InstantCoupon coupon);
    }

    interface View extends MVPView {
        void updateAddressView(Address address);

        void updateCouponView(Coupon coupon);

        void setCouponError(String error);

        void updateWalletView(double value, String color);

        void updatePaymentInfoView(String total, String subTotal);

        void updateServicesView(List<CartBucketItem> list);

        void showWalletInfoDialog(WalletDetails data);

        void showRescheduleDialog(String msg, boolean clearCart);

        void updateWalletError(String msg);

        void updatePaymentPendingView(boolean pendingStatus, double value, String msg);

        void updateCouponCashbackView(String cashback);

        void updateInstantCouponView(InstantCoupon coupon, boolean disableShadow);
    }
}
