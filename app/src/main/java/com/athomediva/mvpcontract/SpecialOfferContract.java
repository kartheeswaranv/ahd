package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.SpecialOfferDateModel;
import com.athomediva.ui.home.SpecialDatesFragment;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 31/07/17.
 */

public interface SpecialOfferContract {

    interface Presenter extends MVPPresenter<SpecialOfferContract.View> {
        void onSubmitClick();

        void onSkipClick();
    }

    interface View extends MVPView {
        void dismissDialog();

        void initSpecialOfferView(ArrayList<SpecialOfferDateModel> list,
          SpecialDatesFragment.SpecialDatesChangeListener listener);
    }
}
