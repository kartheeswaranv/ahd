package com.athomediva.mvpcontract;

import android.os.Bundle;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface AHDMainViewContract {
    String PARAM_RELOAD_HOME_PAG = "param_reload_home_page";
    String PARAM_NAVIGATION_TAG = "param_navigation_tag";

    interface Presenter extends MVPPresenter<View> {
        void onNavigationItemClicked(String number);

        void onNewIntent(Bundle bundle);

        void onBackPressed();

        void onSaveInstance(Bundle bundle);
    }

    interface View extends MVPView {
        void showHomePage(boolean reload);

        void showBookingsPage();

        void showMyAccountsPage();

        void showAvailabilityConfirmDialog();
    }
}

