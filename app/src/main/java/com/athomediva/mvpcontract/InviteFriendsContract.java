package com.athomediva.mvpcontract;

import android.support.v4.app.ActivityCompat;
import com.athomediva.data.models.local.SocialSharingItem;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface InviteFriendsContract {

    interface Presenter extends MVPPresenter<InviteFriendsContract.View>, ActivityCompat.OnRequestPermissionsResultCallback {
        void onMessageSharingClick(SocialSharingItem item);

        void onSocialSharingClick(SocialSharingItem item);

        void onCouponCodeClicked();

        void onReferalMoreClick();

        void onSyncContactClick();
    }

    interface View extends MVPView {

        void updateSharingMessageView(String amount);

        void updateCouponCodeView(String couponCode);

        void updateMessageSharingViews(List<SocialSharingItem> itemList);

        void updateSocialSharingViews(List<SocialSharingItem> itemList);

        void showReferalInfoDialog(String title, String desc);
    }
}
