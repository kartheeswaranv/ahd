package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.EmptySectionItem;
import com.athomediva.data.models.remote.Booking;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.Offers;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import com.athomediva.data.models.remote.Testimonials;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface HomePageContract {
    interface Presenter extends MVPPresenter<View> {
        void onCurrentLocationClick();

        void onCategoryItemClick(Categories cat, SubCategories subCat);

        void onCodeCopyClick(String code);

        void onViewCartClicked();

        void onEmptyWidgetActionButtonClick(EmptySectionItem item);

        void onPlusClick(Services item);

        void onMinusClick(Services item);

        void onViewAllClick();

        void onRateServiceClick(Booking booking);

        void onPayonlineClick(Booking booking);

        void onBookingClick(Booking booking);
    }

    interface View extends MVPView {

        void updateOffersView(List<Offers> offerList);

        void updateCurrentLocationView(String locationName);

        void updateLocationViewVisibility(boolean show);

        void updateTestimonialsView(List<Testimonials> testimonialList, String status);

        void updateCartMenu(int count);

        void resetAllServicesContainer();

        void addServicesSection(List<Categories> categoryList);

        void addPackagesSection(ArrayList<Services> packageItem);

        void updatePackagesView(Services item);

        void refreshPackages(List<Services> packages);

        void addEmptyWidgetSection(EmptySectionItem item);

        void showForceUpdateView(boolean isOptional, String message);

        void showCartEmptyWarningView(String title, String description, android.view.View.OnClickListener listener);

        void updateRecentBookingsView(List<Booking> list);

        void launchSpecialDateOffer();
    }
}