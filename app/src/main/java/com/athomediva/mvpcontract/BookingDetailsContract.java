package com.athomediva.mvpcontract;

import android.support.v4.app.ActivityCompat;
import com.athomediva.data.models.local.StatusModel;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.PaymentInfo;
import com.athomediva.data.models.remote.Services;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 17/05/17.
 */

public interface BookingDetailsContract {

    String PARAM_SELECTED_OPTION = "param_selected_option";
    String PARAM_BOOKING_ID = "param_booking_id";

    interface Presenter extends MVPPresenter<View>, ActivityCompat.OnRequestPermissionsResultCallback {

        void onSpecificReqSendClick(String msg);

        void onAddressChangeClick();

        void onCancelClick();

        void onCustomerCareClick();

        void onCancelConfirmClick();

        void onRateServiceClick();

        void onPayonlineClick();

        void onRefreshClick();
    }

    interface View extends MVPView {
        void updateSubTitleView(String title);

        void updateServicesView(List<Services> services);

        void updatePaymentInfo(ArrayList<PaymentInfo> payment, String paymentMode);

        void updateAddressView(Address address);

        void updateStatusView(StatusModel model);

        void updateSpecificReqView(String reqMsg, boolean enable);

        void showCancelDialog(String title, String msg);

        void showCTA(boolean cancel, boolean customer);

        void updateHeaderView(String date, String duration, String bookingId, String authCode);

        void showCancelStatusDialog(String msg);

        void hideSwipeRefreshView();

        void showContainerView();
    }
}
