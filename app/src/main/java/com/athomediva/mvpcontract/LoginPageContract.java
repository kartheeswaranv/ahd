package com.athomediva.mvpcontract;

import android.support.v4.app.ActivityCompat;
import com.athomediva.data.models.local.TitleDescImageItem;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface LoginPageContract {
    String PARAM_FROM_LAUNCHER = "param_key_from_launcher";

    interface Presenter extends MVPPresenter<View>, ActivityCompat.OnRequestPermissionsResultCallback {
        void login(String phoneNumber);

        void contactUs();

        void skip();

        void onNavigationClose();
    }

    interface View extends MVPView {
        void updatePromoCarousel(List<TitleDescImageItem> itemList);

        void updateSkipMenuVisibility(boolean show);

        void updateNavigationButtonVisibility(boolean show);

        void showValidationError(boolean show, String message);
    }
}