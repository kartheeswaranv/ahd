package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.RatingsModel;
import java.util.List;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public interface FeedbackPageContract {

    interface Presenter extends MVPPresenter<FeedbackPageContract.View> {
        void onSubmitClick(String comments);
    }

    interface View extends MVPView {
        void showValidationMessage(String msg);

        void updateSubTitle(String title);

        void updateRatingListView(List<RatingsModel> list);

        void enableBackPress(boolean enable);
    }
}
