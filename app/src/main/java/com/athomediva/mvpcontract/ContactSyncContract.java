package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.Contact;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 04/09/17.
 */

public interface ContactSyncContract {

    interface Presenter extends MVPPresenter<ContactSyncContract.View> {
        void onSingleRequestClick(Contact contact);

        void onMultiRequestClick();

        void onItemSelect(Contact contact);
    }

    interface View extends MVPView {
        void updateContactCountView(int count);

        void updateData(List<Contact> list);

        void refreshItems(ArrayList<Contact> list);

        void enableInviteView(boolean enable);

        void showContactProgressView();

        void hideContactProgressView();

        void updateInviteMsgView(String msg);
    }
}
