package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.CartModel;
import com.athomediva.data.models.remote.Categories;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.data.models.remote.Services;
import com.athomediva.data.models.remote.SubCategories;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 05/04/17.
 */

public interface SubcatSectionContract {
    String PARAM_SELECTED_POS = "param_selected_pos";
    String PARAM_SELECTED_SUBCAT = "param_selected_subcat";
    String PARAM_IS_PACKAGE = "param_is_package";

    interface Presenter extends MVPPresenter<View> {
        void onSubCatClicked(int group);

        void onCategoryClick(Categories category);

        void onToolbarClick();

        void onServicesUpdate();

        void onViewCartClick();

        void onInstantCouponClick(InstantCoupon coupon);
    }

    interface View extends MVPView {
        void launchPackageFragment(ArrayList<Services> list);

        void updateSubCats(List<SubCategories> serviceList, int selectedSubCatPos);

        void showCategories(List<Categories> list);

        void hideCategories();

        void updateToolbar(String title);

        void updateTabs();

        void updateCart(CartModel model);

        void updateMinOrderError(MinimumOrderError error);

        void updateInstantCouponView(InstantCoupon instantCoupon, boolean disableShadow);
    }
}
