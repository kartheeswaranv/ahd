package com.athomediva.mvpcontract;

import android.content.DialogInterface;
import com.athomediva.data.models.remote.APIDateTimeSlotResponse;
import com.athomediva.data.models.remote.Address;
import com.athomediva.data.models.remote.CartBucketItem;
import com.athomediva.data.models.remote.CartHeaderDescription;
import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.InstantCoupon;
import com.athomediva.data.models.remote.MinimumOrderError;
import com.athomediva.data.models.remote.Services;
import java.util.List;

/**
 * Created by kartheeswaran on 26/04/17.
 */

public interface MyCartContract {
    String PARAM_CART_ID = "param_cart_id";

    interface Presenter extends MVPPresenter<MyCartContract.View> {

        void onScheduleClick(CartBucketItem model);

        void onScheduleConfirm(CartBucketItem model, DateSlot slot);

        void onMinusClick(Services model);

        void onPlusClick(Services model);

        void onAddAddressClick();

        void onChangeAddressClick();

        void onProceedClick();

        void onExploreServiceClick();

        void onInstantCouponClick(InstantCoupon coupon);
    }

    interface View extends MVPView {
        void updateAddress(Address address);

        void updateBuckets(List<CartBucketItem> list);

        void updateHeaderInfo(String title, CartHeaderDescription description);

        void updateAdapter();

        void updateEmptyPageVisibility(boolean show);

        void showScheduleDialog(CartBucketItem model, APIDateTimeSlotResponse response);

        void enableProceedView(boolean enable);

        void showOrHideMinOrderError(MinimumOrderError error);

        void showErrorMessage(String message);

        void showErrorMessage(String message, DialogInterface.OnDismissListener listener);

        void showContainerView();

        void updateInstantCouponView(InstantCoupon instantCoupon, boolean disableShadow);
    }
}
