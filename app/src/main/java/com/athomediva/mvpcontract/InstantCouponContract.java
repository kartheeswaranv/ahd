package com.athomediva.mvpcontract;

/**
 * Created by kartheeswaran on 15/06/18.
 */

public interface InstantCouponContract {

    String PARAM_COUPON_OBJ = "param_instant_coupon";

    interface Presenter extends MVPPresenter<View> {
        void onCouponCodeClick();

        void onCancelClick();
    }

    interface View extends MVPView {

        void updateOfferView(String percentage, String amount);

        void updateCouponView(String couponCode);

        void updateValidationMsg(String msg);
    }
}
