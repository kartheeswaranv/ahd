package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.Address;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface MyAddressPageContract {
    String PARAM_CIRCLE_NAME = "param_circle_name";
    String PARAM_SELECTED_ADDRESS = "param_selected_address";
    String PARAM_IS_FROM_ACCOUNT = "param_is_from_account";

    interface Presenter extends MVPPresenter<MyAddressPageContract.View> {
        void onOptionItemClick(Address addressItem);

        void onAddAddressClick();

        void onDeleteAddressClick(Address address);
    }

    interface View extends MVPView {
        void updateAddressView(List<Address> itemList, boolean enableOption);

        void updateEmptyPageVisibility(boolean show);
    }
}
