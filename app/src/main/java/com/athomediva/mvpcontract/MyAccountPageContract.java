package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.TitleDescImageItem;
import com.athomediva.data.models.remote.LoggedInUserDetails;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface MyAccountPageContract {
    interface Presenter extends MVPPresenter<View> {
        void onItemClick(String action);

        void onEditClicked();
    }

    interface View extends MVPView {
        void initializeActionSections(List<TitleDescImageItem> sectionItemList);

        void updateUserProfileView(LoggedInUserDetails user);
    }
}