package com.athomediva.mvpcontract;

import android.support.v4.app.ActivityCompat;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface OTPVerificationContract {
    String PARAM_CONSUMER = "param_consumer";
    String PARAM_RESULT_USER = "param_user";
    String PARAM_RESULT_TOKEN = "param_token";
    String PARAM_IS_UPDATE_MOBILE= "param_is_update_mobile";

    interface Presenter extends MVPPresenter<View>, ActivityCompat.OnRequestPermissionsResultCallback {
        void otpEntered(String otp);

        void enterManualClick();

        void editNumberClick();

        void resendOTPClick();
    }

    interface View extends MVPView {
        void updateVerificationStatusView(boolean show);

        void fillOTPAutomatically(String otp);

        void showOtpError(boolean isError, String otpErrorMessage);

        void updateAutoVerificationView(boolean start);

        void enableOTPMode(boolean isManual);

        void updateMobileNumberView(String number);
    }
}