package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.OptionModel;
import java.util.List;

/**
 * Created by kartheeswaran on 28/05/17.
 */

public interface CancelBookingContract {

    interface Presenter extends MVPPresenter<View> {
        void onCancelClick();

        void onOptionItemClick(int pos);
    }

    interface View extends MVPView {
        void updateOptionsView(List<OptionModel> list);

        void refreshAdapterView();

        void updateCTAView(boolean enable);

        void showEditOptionView(OptionModel model);

        void hideEditOptionView();
    }
}
