package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.Services;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 16/04/17.
 */

public interface ServicesContract {
    String PARAM_SELECT_POSITION = "param_select_position";
    String PARAM_SELECTED_SERVICES = "param_selected_services";

    interface Presenter extends MVPPresenter<ServicesContract.View> {
        void onMinusClick(Services model);

        void onPlusClick(Services model);

        void onInfoClick(Services model);
    }

    interface View extends MVPView {
        void updateServiceItem(Services model);

        void updateServices(List<Services> list);

        void showInfoDialog(String title, String msg);
    }
}
