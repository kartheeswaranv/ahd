package com.athomediva.mvpcontract;

import com.athomediva.data.models.local.TitleDescImageItem;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface MyWalletContract {
    String PARAM_AVAILABLE_BALANCE = "param_available_balance";

    interface Presenter extends MVPPresenter<MyWalletContract.View> {
    }

    interface View extends MVPView {

        void updateWalletBreakupViews(String amount, List<TitleDescImageItem> walletBreakupList);
    }
}
