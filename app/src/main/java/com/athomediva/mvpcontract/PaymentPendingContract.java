package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.PaymentPendingBookingDetails;
import java.util.ArrayList;

/**
 * Created by kartheeswaran on 05/07/17.
 */

public interface PaymentPendingContract {
    String PARAM_RESPONSE = "param_response";

    interface Presenter extends MVPPresenter<View> {
        void onCallClick();

        void onProceedClick();
    }

    interface View extends MVPView {
        void updateBookingListView(ArrayList<PaymentPendingBookingDetails> list);

        void updateCTA(String cta);

        void updateFooterView(String footerMsg);

        void updateHeaderView(String headerMsg);

        void updateTitle(String title);
    }
}
