package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.Booking;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface MyBookingsPageContract {
    interface Presenter extends MVPPresenter<View> {
        void onItemClick(Booking booking);

        void onRateMyServiceClick(Booking booking);

        void onRefreshClick();
    }

    interface View extends MVPView {

        void hideSwipeRefreshView();

        void updateBookingsView(ArrayList<String> titleList, ArrayList<ArrayList<Booking>> bookingList);
    }
}