package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.SubCategories;
import java.util.List;

/**
 * Created by kartheeswaran on 25/05/17.
 */

public interface FeedbackSuccessContract {

    interface Presenter extends MVPPresenter<FeedbackSuccessContract.View> {
        void onSubcatItemClick(SubCategories model);

        void onHomeClick();

        void onFacebookShareClick();

        void onTwitterShareClick();

        void onCopyToClipboardClick();

        void onRateOnPlaystoreClick();
    }

    interface View extends MVPView {
        void updateSubCatView(List<SubCategories> list);

        void updateReferalView(String referralCode);

        void updateSubTitle(String bucketName);

        void enableRateUsView(boolean enable);

        void hideSubCatView();
    }
}
