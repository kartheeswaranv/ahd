package com.athomediva.mvpcontract;

import android.content.Intent;
import android.os.Bundle;

/**
 * Base interface that any class that wants to act as a View in the MVP (Model View Presenter)
 * pattern must implement. Generally this interface will be extended by a more specific interface
 * that then usually will be implemented by an Activity or Fragment.
 */
public interface MVPView {

    void launchActivity(Bundle bundle, Class className);

    void launchActivity(Intent intent);

    void launchActivityForResult(Bundle bundle, Class className, int resultCode);

    void launchActivityForResult(Intent intent, int reqCode);

    void finishViewWithResult(int resultCode, Bundle data);

    void finishViewWithResult(int resultCode);

    void showProgressBar(String desc);

    void showProgressBar(String desc, boolean disableCancel);

    void hideProgressBar();

    void finish();
}
