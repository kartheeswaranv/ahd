package com.athomediva.mvpcontract;

/**
 * Created by kartheeswaran on 07/07/17.
 */

public interface RateUsContract {

    interface Presenter extends MVPPresenter<View> {
        void onSubmitClick(float rating, String comments);

        void onRatingChange(float rating);

        void onPlayStoreClick();
    }

    interface View extends MVPView {
        void updateView();

        void enableLowRatingOptions(boolean enable);
    }
}
