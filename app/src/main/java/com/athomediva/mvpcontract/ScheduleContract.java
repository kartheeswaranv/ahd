package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.DateSlot;
import com.athomediva.data.models.remote.TimeSlot;
import com.athomediva.ui.mycart.ScheduleDialogFragment;
import java.util.List;

/**
 * Created by kartheeswaran on 02/05/17.
 */

public interface ScheduleContract {
    String PARAM_RESPONSE = "param_response";
    String PARAM_SELECTED_SLOT = "param_selected_slot";

    interface Presenter extends MVPPresenter<ScheduleContract.View> {

        void onScheduleClick(ScheduleDialogFragment.DateSelectListener listener);

        void onDateSelection(DateSlot selectedDate);

        void onTimeSelection(TimeSlot slot);
    }

    interface View extends MVPView {
        void updateMenu(List<TimeSlot> slots);

        void updateDateList(List<DateSlot> list);

        void enableScheduleBtn(boolean enable);

        void refreshDateList();

        void updateTimeView(TimeSlot slot);

        void dismissDialog();
    }
}
