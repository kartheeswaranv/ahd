package com.athomediva.mvpcontract;

import android.os.Bundle;

/**
 * Created by kartheeswaran on 10/07/17.
 */

public interface FeedbackActContract {
    String PARAM_BOOKING_ID = "param_booking_id";
    String PARAM_SUBTITLE = "param_sub_title";
    String PARAM_MANDATORY = "param_is_mandatory";
    String PARAM_IS_ENABLE_RATE = "param_is_enable_rate";

    interface Presenter extends MVPPresenter<View> {
        void onBackPressed();

        void onHomeBtnStatusChange(boolean enable);

        void onNewIntent(Bundle b);
    }

    interface View extends MVPView {
        void updateTitle(String title);

        void updateSubTitle(String subTitle);

        void launchFragment(String tagName, Bundle b);

        void enableHomeBtn(boolean enable);
    }
}
