package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.BookingDescInfo;
import com.athomediva.data.models.remote.BookingExtraInfo;
import com.athomediva.data.models.remote.ConfirmBookingInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartheeswaran on 23/05/17.
 */

public interface BookingSuccessContract {

    String PARAM_RESPONSE = "param_response";

    interface Presenter extends MVPPresenter<View> {
        void onBookingItemClick(ConfirmBookingInfo info);

        void onHomeClick();

        void onPayNowClick();
    }

    interface View extends MVPView {
        void updateTitle(String userName);

        void updateHeaderInfoList(ArrayList<BookingDescInfo> desc);

        void updateFooterInfoList(ArrayList<BookingDescInfo> desc);

        void updateBookingsList(List<ConfirmBookingInfo> bookingList);

        void updateTotalAmountView(String amount);

        void updateBookingExtraInfoList(ArrayList<BookingExtraInfo> list);
    }
}
