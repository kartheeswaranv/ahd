package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.OffersItem;
import java.util.ArrayList;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface OffersViewContract {

    interface Presenter extends MVPPresenter<OffersViewContract.View> {
        void onOffersCopyToClipboardClick(OffersItem offer);

        void onEmptyPageActionBtnClick();
    }

    interface View extends MVPView {
        void updateOffersView(ArrayList<OffersItem> offersList);

        void updateEmptyPageVisibility(boolean show);
    }
}
