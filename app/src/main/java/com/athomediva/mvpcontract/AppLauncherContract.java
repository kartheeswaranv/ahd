package com.athomediva.mvpcontract;

import android.os.Bundle;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface AppLauncherContract {
    String PARAM_LAUNCHED_FOR_LOGIN = "param_launched_for_login";

    interface Presenter extends MVPPresenter<AppLauncherContract.View> {
        void onReadyForPlayback();
        void onBackPressed();
    }

    interface View extends MVPView {
        void startVideoPlayback(String file) throws Exception;

        void showLoginPageView(Bundle bundle);
    }
}
