package com.athomediva.mvpcontract;

import com.athomediva.data.models.remote.Services;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohitkumar on 04/04/17.
 */

public interface PackagesContract {
    String PARAM_PACKAGE_LIST = "param_package_list";

    interface Presenter extends MVPPresenter<View> {
        void onPlusClick(Services item);

        void onMinusClick(Services item);

        void onInfoClick(Services item);
    }

    interface View extends MVPView {
        void updatePackages(List<Services> packageList);

        void updatePackageItem(Services item);

        void showInfoDialog(String title, ArrayList<String> list);
    }
}
